<div class="container">



	<div class="container-fluid">
		
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		    <span class="sr-only">Toggle navigation</span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		  </button>
		  
		</div>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
      <!-- .dropdown -->  
		    <li class="dropdown">
			   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Is It AFS item? <span class="caret"></span></a>
			  
			</li> <!-- .dropdown -->						

       <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Supply Ord guide <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Grocery <i class="fa fa-chevron-right"></i></a>
          </li>                 
          <li><a href="#">Meat</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">FSD <i class="fa fa-chevron-right"></i></a>
             
          </li> 
           <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Bakery <i class="fa fa-chevron-right"></i></a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Produce <i class="fa fa-chevron-right"></i></a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Natural Food GM <i class="fa fa-chevron-right"></i></a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Hot work <i class="fa fa-chevron-right"></i></a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Floral<i class="fa fa-chevron-right"></i></a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Pharmacy<i class="fa fa-chevron-right"></i></a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Unknown<i class="fa fa-chevron-right"></i></a>
             
          </li> 
          
        </ul>
      </li> <!-- .dropdown -->
       <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Out of Stock <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Grocery </a>
          </li>                 
          <li><a href="#">Meat</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">FSD </a>
             
          </li> 
           <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Bakery </a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Produce </a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Natural Food GM </a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Hot work </a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Floral</a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Pharmacy</a>
             
          </li> 
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Unknown</a>
             
          </li> 
          
        </ul>
      </li> <!-- .dropdown -->
      
      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Billable Query<span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Printer/Output Menu </a>
          </li>                 
                  </ul>
      </li> <!-- .dropdown -->
      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">RX Inventory<span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Annual Inventory</a>
          </li>  
           <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Re-Count/Spot Audit</a>
          </li>                 
                  </ul>
      </li> <!-- .dropdown -->
            <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Item Lookup<span class="caret"></span></a>
       
      </li> <!-- .dropdown -->
      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pull List<span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Printer/ Output menu</a>
          </li>  
                         
                  </ul>
      </li> <!-- .dropdown -->
      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Cool labels<span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
         
           <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Printer/ Output menu<i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Country of Origin</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistertotal')}}">31 Countrys</a></li>
              </ul>
          </li> 
                         
                  </ul>
      </li> <!-- .dropdown -->
     
      <li><a href="#">Item Sales</a></li>
      <li><a href="#">Product Checkin</a></li>
       <li><a href="#">Rtl Comp Audit</a></li>
        <li><a href="#">ARM</a></li>
         <li><a href="#">RIP Donations</a></li>
      
          <li><a href="{{URL::route('socscn01-returnhome')}}">Exit</a></li>
     
    
     
       </ul> <!-- .nav .navbar-nav -->		 
		</div><!-- /.navbar-collapse -->
		
	</div><!-- /.container-fluid -->

 
</div>  

<style>
.dropdown-submenu {
  position: relative;
}
.dropdown-submenu > .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
}
.dropdown-submenu:hover > .dropdown-menu {
  display: block;
}
.dropdown-submenu:hover > a:after {
  border-left-color: #fff;
}
.dropdown-submenu.pull-left {
  float: none;
}
.dropdown-submenu.pull-left > .dropdown-menu {
  left: -100%;
  margin-left: 10px;
}

</style>