<div class="container">



	<div class="container-fluid">
		
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		    <span class="sr-only">Toggle navigation</span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		  </button>
		  
		</div>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
       <li><a href="#">Receiver Scan</a></li>
        <li><a href="#">DEX Check-in</a></li>
      

     					

       <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Review <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        <li><a href="#">By Invoice Number</a></li>
                         
          <li><a href="#">By Delivery Return Number</a></li>
          <li><a href="#">DEX In Progress</a></li>
          <li><a href="#">DEX Transmissions</a></li>
          <li><a href="#">Vendor Defaults</a></li>
         </ul>
      </li> <!-- .dropdown -->
       <!-- .dropdown -->  
       
       <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Reports <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Wkly Receiving <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">GROCERY</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistertotal')}}">LIQUOR</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">MEAT</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">DELI</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">FSD</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">BAKERY</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">PRODUCE</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">NAT FDS</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">GM</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">HOT WOK</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">FLORAL</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">COFFEE SHOP</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">CSO</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">SEAFOOD</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">PHARMACY</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">ALL</a></li>
              </ul>
          </li>                 
          
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Offlined Invoices <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">GROCERY</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">LIQUOR</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">MEAT</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">DELI</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">FSD</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">BAKERY</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">PRODUCE</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">NAT FDS</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">GM</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">HOT WOK</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">FLORAL</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">COFFEE SHOP</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">CSO</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">SEAFOOD</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">PHARMACY</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">ALL</a></li>

             
              </ul>
          </li> 
          <li><a href="#">Cost Discrepcy</a></li>
           <li><a href="#">Vendor Costs</a></li>
            <li><a href="#">Transporters</a></li>
             <li><a href="#">Transporter Form</a></li>

        </ul>
      </li> <!-- .dropdown -->
      
           <li><a href="{{URL::route('socscn01-returnhome')}}">Exit</a></li>
     
    
     
       </ul> <!-- .nav .navbar-nav -->		 
		</div><!-- /.navbar-collapse -->
		
	</div><!-- /.container-fluid -->

 
</div>  

<style>
.dropdown-submenu {
  position: relative;
}
.dropdown-submenu > .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
}
.dropdown-submenu:hover > .dropdown-menu {
  display: block;
}
.dropdown-submenu:hover > a:after {
  border-left-color: #fff;
}
.dropdown-submenu.pull-left {
  float: none;
}
.dropdown-submenu.pull-left > .dropdown-menu {
  left: -100%;
  margin-left: 10px;
}

</style>