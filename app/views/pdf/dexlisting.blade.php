<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
        <h1 style="color:#ce0002;">Dexlog Invoice Report</h1>
    <table>
        <tr>
            <th>Invoice No</th>
            <th>Type</th>
            <th>Comm ID</th>
            <th>Message</th>
            <th>Status</th>
            <th>Created Date/Time</th>
          </tr>
        @foreach ($results as $rs)
          <tr>
             <td>{{ $rs['invoice_number'] }}</td>
            <td>{{ $rs['type_code'] }}</td>
            <td>{{ $rs['commid'] }}</td>
            <td>{{ $rs['message'] }}</td>
            <td>{{ $rs['status_code'] }}</td>
            <td>{{ date("m/d/Y  g:i A", strtotime($rs['create_datetime'])) }}</td>
          </tr>
        @endforeach
    </table>
</div>  

</body>
</html>