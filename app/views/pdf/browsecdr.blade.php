<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
    <table>
        <tr>
             <th>Invoice No</th>
            <th>Invoice Date</th>
            <th>Store Number</th>
            <th>Method</th>
            <th>Type</th>
            <th>Department</th>
            <th>Delivery Unit</th>
            <th>Vendor Number</th>
            <th>Cost Discrepency</th>
            <th>Status</th>
          </tr>
        @foreach ($results as $rs)
          <tr>
            <td>{{ $rs['id'] }}</td>
            <td>{{ date("m/d/Y", strtotime($rs['invoice_date'])) }}</td>
            <td>{{ $rs['store_number'] }}</td>
            <td>{{ $rs['method_rcvd'] }}</td>
            <td>{{ $rs['type_code'] }}</td>
            <td>{{ $rs['dept_number'] }}</td>
            <td>{{ $rs['delivery_unit'] }}</td>
            <td>{{ $rs['vendor_number'] }}</td>
            <td>{{ $rs['cost_discrep_sw'] }}</td>
            <td>{{ $rs['status_code'] }}</td>
          </tr>
        @endforeach
    </table>
</div>  

</body>
</html>