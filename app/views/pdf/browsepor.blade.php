<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
    <table>
        <tr>
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th>Name</th>
            <th>Type</th>
            <th>Total Cost</th>
          </tr>
        @foreach ($results as $rs)
          <tr>
          <td>{{ $rs['id'] }}</td>
            <td>{{ date("m/d/Y", strtotime($rs['invoice_date'])) }}</td>
            <td>{{ $rs['name'] }}</td>
            @if($rs['type_code'] == 'D') 
            <td>Delivery</td>
            @elseif ($rs['type_code'] == 'R')
            <td>Return</td>
            @elseif ($rs['type_code'] == 'S')
            <td>Store Supplies</td>
            @else
            <td>Unknown</td>
            @endif
             <td>{{ $rs['tot_vend_cost'] }}</td>
          </tr>
        @endforeach
    </table>
</div>  

</body>
</html>