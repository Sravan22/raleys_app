<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
      <h1 style="color:#ce0002;">Transporter List</h1>
    <table>
        <tr>
            <th>Transporter Number</th>
            <th>Transporter Name</th>
            <th>City</th>
            <th>State</th>
            <th>Zipcode</th>
            <th>Phone</th>
          </tr>
       @foreach ($transports as $trns)
          <tr>
            <td>{{ $trns->trns_number }}</td>
            <td>{{ $trns->name }}</td>
            <td>{{ $trns->city }}</td>
            <td>{{ $trns->state }}</td>
            <td>{{ $trns->zip_code }}</td>
            <td>{{ $trns->phone }}</td>
          </tr>
        @endforeach
    </table>
</div>  

</body>
</html>