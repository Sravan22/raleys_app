<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}
.center{
    text-align: center;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
        <h1 style="color:#ce0002;">Shrink Capture Department Report</h1>
    <table>
        <tr>
             <th>Shrink Date</th>
             <th>Shrink Number</th>
             <th>Employee Id</th>
             <th>Total Line Items</th>
             <th>Total Value</th>
             <th>Status</th>
          </tr>

          @for ($i = 0; $i < count($shrinkcapturedepart); $i++)
         <?php 
              $params = array(
                        'shrink_date' =>  date('m/d/Y',strtotime($shrinkcapturedepart[$i]->shrink_date)),
                        'shrink_number' =>  $shrinkcapturedepart[$i]->shrink_number,
                        'employee_id' =>  $shrinkcapturedepart[$i]->employee_id,
                        'tot_line_items' =>  $shrinkcapturedepart[$i]->tot_line_items,
                        'tot_value' =>  $shrinkcapturedepart[$i]->tot_value,
                        'seq_number' =>  $shrinkcapturedepart[$i]->seq_number,
                        'status' =>  $shrinkcapturedepart[$i]->status
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>{{ date('m/d/Y',strtotime($shrinkcapturedepart[$i]->shrink_date)) }}</td>
            <td >{{  $shrinkcapturedepart[$i]->shrink_number }}</td>
           
            <td>{{ $shrinkcapturedepart[$i]->employee_id }}</td>
            <td>{{ $shrinkcapturedepart[$i]->tot_line_items }}</td>
            <td>${{ $shrinkcapturedepart[$i]->tot_value }}</td>
            @if($shrinkcapturedepart[$i]->status == "O")
            <td>Open</td>
            @elseif($shrinkcapturedepart[$i]->status == "V")
            <td>Void</td>
            @elseif($shrinkcapturedepart[$i]->status == "D")
            <td>Deleted</td>
            @elseif($shrinkcapturedepart[$i]->status == "C")
            <td>Closed</td>
            @elseif($shrinkcapturedepart[$i]->status == "S")
            <td>Sent</td>
            @else
            <td>Unknown</td>
            @endif
            {{-- <td>{{ $shrinkcapturedepart[$i]->status }}</td> --}}
         </tr>
         
          @endfor
            </table>
</div>  

</body>
</html>