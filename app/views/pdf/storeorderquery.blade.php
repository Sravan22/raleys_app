<!DOCTYPE html>
<html>
   <head>
      <style>
         table {
         font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
         font-size: 9px;
         color: #333;
         border-collapse: collapse;
         width: 100%;
         }
         td, th {
         border: 1px solid #dddddd;
         text-align: left;
         padding: 5px;
         }
         .center{
         text-align: center;
         }
         tr:nth-child(even) {
         background-color: #dddddd;
         }
      </style>
   </head>
   <body>
      <div class="row">
         <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
            <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
            </a>
         </div>
         <h1 style="color:#ce0002;">Print Order Browse</h1>
         <table>
            <tr>
               <th>Dept No</th>
               <th>Order Date</th>
               <th>Type</th>
               <th>Last Updated</th>
               
            </tr>
@for ($i = 0; $i < count($print_order_result_array); $i++)   
         <tr>
          
            
            <?php 
            $seq_number = DB::select('SELECT * FROM sotrnhdr WHERE seq_number = "'.$print_order_result_array[$i]['seq_number'].'" ' );
            $seq_number_array = json_decode(json_encode($seq_number), true);
            //echo '<pre>'; print_r($seq_number_array);exit;
            $description = DB::select('select * from sotrncfg where type_code= "'.$seq_number_array[0]['type_code'].'" and subtype_code = "'.$seq_number_array[0]['subtype_code'].'" ' );
            $description_array = json_decode(json_encode($description), true);
            ?>
            <?php 
              $params = array(
                        'order_date' =>  date('m/d/Y',strtotime($print_order_result_array[$i]['order_date'])),
                        'seq_number' =>  $print_order_result_array[$i]['seq_number'],
                        'description' => $description_array[0]['description'],
                        'status_code' => $seq_number_array[0]['status_code']
                        ); 
              $queryString = http_build_query($params);
            ?>
            <?php  $order_date = date('m/d/Y',strtotime($print_order_result_array[$i]['order_date']));  ?>
          <td>{{ $print_order_result_array[$i]['dept_number'] }}</td>
          <td>{{$order_date}}</td>
            
            <td>{{ $description_array[0]['description'] }}</td>
            <td>{{ $print_order_result_array[$i]['last_update'] }} </td>
         </tr>
          @endfor
         </table>
         
      </div>
   </body>
</html>