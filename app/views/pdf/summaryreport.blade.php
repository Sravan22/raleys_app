<?php

?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">Summary Report</h1>
    <table>
        <tr>
            <th> Userid</th>
            <th>Username</th>
            <th> Update Date and Time </th>
            <th>Seq No</th>
            <th>Main Type</th>
            <th>Action CD</th>
            <th>Item Code</th>
            <th>Item Desc</th>
            <th>Item Dept</th>
            <th>Master Field No</th>
            <th>Original Value</th>
            <th>New Value</th>
        </tr>
        @for ($i = 0; $i < count($summary_report_query_array); $i++)
        <tr>  

            <td>{{ $summary_report_query_array[0]['userid'] }}</td>
            <td>{{ $username }}</td>
            <td>{{ date('m/d/Y',strtotime($summary_report_query_array[0]['update_date'])) }} {{ $summary_report_query_array[0]['update_time'] }}</td>
            <td>{{ $summary_report_query_array[0]['seq_no'] }}</td>
            <td>{{ $summary_report_query_array[0]['maint_type_cd'] }}</td>
            <td>{{ $summary_report_query_array[0]['action_cd'] }}</td>
            <td>{{ $summary_report_query_array[0]['item_code'] }}</td>
            <td>{{ $summary_report_query_array[0]['item_desc'] }}</td>
            <td>{{ $summary_report_query_array[0]['item_dept'] }}</td>
            <td>{{ $summary_report_query_array[0]['mstr_fld_no'] }}</td>
            <td>{{ $summary_report_query_array[0]['orig_value'] }}</td>
            <td>{{ $summary_report_query_array[0]['new_value'] }}</td>
            
          </tr>
       
        @endfor 
    </table>
</div>  

</body>
</html>