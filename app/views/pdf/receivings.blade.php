<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
      <h1 style="color:#ce0002;">Received Browse</h1>
    @if($receivings) 
    <table>
          <tr>
            <th>Invoice Created Date/Time</th>
            <th>Invoice Number</th>
            <th>Vendor Name</th>
            <th>Invoice Type</th>
            <th>Status</th>
            <th>Method Received</th>
          </tr>
      
        @foreach ($receivings as $vnd)
          <tr>
            <td>{{ date("m/d/Y g:i A", strtotime($vnd['create_datetime'])) }}</td>
            <td><u>{{ HTML::link(URL::route('mktmgr-view-seletedrow',['seq_number' => $vnd['seq_number']]), $vnd['id']) }}</u></td>
            <td>{{ $vnd['vname'] }}</td>
            @if ($vnd['type_code'] == 'D')
            <td>Delivery</td>
            @elseif ($vnd['type_code'] == 'R')
            <td>Return</td>
            @else
            <td>Unknown</td>
            @endif
            @if ($vnd['status_code'] == 'A') 
            <td>Accepted</td>
            @elseif ($vnd['status_code'] == 'H')
            <td>On Hold</td>
            @elseif ($vnd['status_code'] == 'I')
            <td>Incomplete</td>
            @elseif ($vnd['status_code'] == 'O')
            <td>Open</td>
            @elseif ($vnd['status_code'] == 'R')
            <td>Reviewed</td>
            @elseif ($vnd['status_code'] == 'U')
            <td>Uploaded</td>
            @else
            <td>Voided</td>
            @endif
            @if($vnd['method_rcvd'] == 'D') 
            <td>Dex</td>
            @elseif ($vnd['method_rcvd'] == 'R')
            <td>Receiver Scan</td>
            @elseif ($vnd['method_rcvd'] == 'O')
            <td>Offline</td>
            @elseif ($vnd['method_rcvd'] == 'N')
            <td>NEX - EDI</td>
            @else
            <td>UNKNOWN</td>
            @endif
            </tr>
        @endforeach
    </table>  
     @else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Receving Report meet Query criteria.
            </div>
    @endif
</div>  

</body>
</html>