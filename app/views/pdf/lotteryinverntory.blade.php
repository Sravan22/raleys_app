<!DOCTYPE html>
<html>
   <head>
      <style>
         table {
         font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
         font-size: 9px;
         color: #333;
         border-collapse: collapse;
         width: 100%;
         }
         .center {
         text-align: center;
         }
         td, th {
         border: 1px solid #dddddd;
         text-align: left;
         padding: 5px;
         }
         .center{
         text-align: center;
         }
         tr:nth-child(even) {
         background-color: #dddddd;
         }
      </style>
   </head>
   <body>
      
      <div class="row">
      <div class="col-md-6">
         <a class="custom-input-width pull-left" href="{{ ('/') }}">
         <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
         </a>
      </div>
      <h1 style="color:#ce0002;">Lottery Inventory Report</h1>
         <table>
            <tr>
               <th>Game</th>
               <th>Price</th>
               <th>Begin</th>
               <th>Del</th>
               <th>Returns</th>
               <th>End Inv</th>
               <th>Sales</th>
               <th>Unity</th>
               <th>Diff</th>
               </tr>
            @foreach($work_data  as $key => $value)
                        <tr>
                           <td>{{$value['game_no']}}</td>
                           <td>{{$value['ticket_value']}}</td>
                           <td>{{$value['begin_inv']}}</td>
                           <td>{{$value['deliveries']}}</td>
                           <td>{{$value['returns']}}</td>
                           <td>{{$value['end_inv']}}</td>
                           <td>{{$value['calc_sales']}}</td>
                           <td>{{$value['sales_unity']}}</td>
                           <td>{{$value['sales_diff']}}</td>
                        </tr>
                        @endforeach
         </table>
      </div>
   </body>
</html>