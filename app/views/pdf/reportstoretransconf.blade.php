<?php

?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">Store Transfer Configuration Manager Report</h1>
    <table>
        <tr>
            <th>Active Switch</th>
            <th>Type Code</th>
            <th>Sub-Type Code</th>
            <th>Description</th>
            <th>Partition Code</th>
            <th>Dep Number</th>
        </tr>
        @foreach ($confbrowser as $rs)
          <tr>
            <td>{{ $rs->active_switch }}</td>
            <td>{{ $rs->type_code }}</td>
            <td>{{ $rs->subtype_code }}</td>
            <td>{{ $rs->description }}</td>
            <td>{{ $rs->partition_code }}</td>
            <td>{{ $rs->dept_number }}</td>
          </tr>
        @endforeach
    </table>
</div>  

</body>
</html>