<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">Non Perishable Inventory Prep Totals Report</h1>
    <table class="table table-striped">
        <tr>
               <th></th>
               <th></th>
               <th>Total</th>
               <th>Total</th>
               <th>Total</th>
               <th colspan="4">Current Inventories with a STATUS of</th>
               <th></th>
               <th></th>
            </tr>
            <tr>
               <th>Dept Number</th>
               <th>Dept Desc</th>
               <th>Value at cost</th>
               <th>Value at RT1</th>
               <th>CRV at RT1</th>
               <th>O</th>
               <th>C</th>
               <th>V</th>
               <th>A</th>
               <th>R</th>
               <th>S</th>
            </tr>
         </thead>
         <tbody>
         @foreach($work_data->dept_sodinv_rec as $key => $value)
        
            <tr>
               
               <td>{{$value['dept_no']}}</td>
               <td>{{$value['desc']}}</td>
               <td>{{$value['tot_value_cst']}}</td>
               <td>${{$value['tot_value_rtl']}}</td>
               <td>${{$value['tot_crv_rtl']}}</td>
               <td>{{$value['num_of_invs_O']}}</td>
               <td>{{$value['num_of_invs_C']}}</td>
               <td>{{$value['num_of_invs_V']}}</td>
               <td>{{$value['num_of_invs_A']}}</td>
               <td>{{$value['num_of_invs_R']}}</td>
               <td>{{$value['num_of_invs_S']}}</td>
            </tr>
            @endforeach
         </tbody>
         </table>
</div>  

</body>
</html>