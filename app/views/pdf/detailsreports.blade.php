<?php
//echo '<pre>',print_r($work_data);exit();
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}


</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">Detail Report</h1>
      <h2 style="text-align: center;">Detail</h2>
    <table class="table table-striped">
        <tr>
               <th>Update Date</th>
                <th>Upadate Time</th>
                <th>User</th>
                <th>UPC / PLU / SKU</th>
                <th>Description</th>
                <th>Action</th>
                <th>Field Number</th>
                <th>Old</th>
                <th>New</th>
                <th>Maint Type</th>
               
               
              
            </tr>
            
         <tbody>
        @foreach($work_data->results as $row)
        
            <tr>
               
               <td>{{date("m-d-Y", strtotime($row->update_date));}}</td>
                        <td>{{$row->update_time}}</td>
                        <td>{{$row->userid}} {{ $row->user_name}}</td>
                        <td>{{$row->item_code}}</td>
                        <td>{{$row->item_desc}}</td>
                        <td>{{$row->action_desc}}</td>
                        
                        <td>{{$row->mstr_fld_no}} {{$row->mstr_fld_desc}}</td>
                        <td>@if($row->action_cd=='A' OR $row->action_cd=='F') {{$row->orig_value}} @endif</td>
                        <td>{{$row->new_value}}</td>
                        <td>@if($row->maint_type_cd == "B") {{'BATCH'}} @endif</td>
            </tr>
            @endforeach
         </tbody>
         </table>
         
         <br> 
           <h2 style="text-align: center;">Summary</h2>
      
       
         <table>
                
                    
                    <tr>
                        <td>Total Items Scanned: </td>
                        <td> {{$work_data->tot_scanned}}</td>
                    </tr>
                    <tr>
                        <td>Total Aisle Scanned:</td>
                        <td> {{$work_data->tot_aisle_scanned}}</td>
                    </tr>

                    <tr>
                        <td>Items Not Found: </td>
                        <td> {{$work_data->tot_notfound}} </td>

                    </tr>
                    <tr>
                        <td> Items Added:</td>
                        <td> {{$work_data->tot_add}}</td>

                    </tr>
                    <tr>
                        <td>  Items Deleted:</td>
                        <td> {{$work_data->tot_del}}</td>

                    </tr>
                    <tr>
                        <td>  Price Changes:</td>
                        <td>{{$work_data->tot_updprice}}</td>


                    
                    </tr>
                    <tr>
                        <td>    Updated Items: </td>
                        <td>{{$work_data->tot_upd}}</td>

                    </tr>
                    <tr>
                        <td>   Total FM Notes: </td>
                        <td> {{$work_data->tot_fm_notes}}</td>

                    </tr>
                    <tr>
                        <td>  Total Signin's:</td>
                        <td> {{$work_data->tot_signin}}</td>
        
                    </tr>
                    <tr>
                        <td>  Total Signout's: </td>
                        <td> {{$work_data->tot_signout}}</td>

                    </tr>
                    <tr>
                        <td>  Total Unknown:</td>
                        <td> {{$work_data->tot_unknown}}</td>
                    </tr>

                
            </table>
</div>  

</body>
</html>