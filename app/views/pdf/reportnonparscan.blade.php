

<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;"> Item Scan Detail Report</h1>
      <table>
                <tr>
                    <td>Inventory No:</td>
                    <td>{{$work_data->sodinv_hdrs_rec['inventory_number']}}</td>
                    <td>Total Line Items:</td>
                    <td>{{$work_data->sodinv_hdrs_rec['total_scans']}}</td>
                </tr>
                <tr>
                    <td>Inventory Date:</td>

                    <td>{{date("m-d-Y", strtotime($work_data->sodinv_hdrs_rec['create_date']))}}</td>
                    <td>Total Cst Value:</td>
                    <td>${{$work_data->sodinv_hdrs_rec['total_value_cst']}}</td>
                </tr>
                <tr>
                    <td>Employee ID:</td>
                    <td>{{$work_data->sodinv_hdrs_rec['employee_id']}}</td>
                    <td>Total Rtl Value:</td>
                    <td>${{$work_data->sodinv_hdrs_rec['total_value_rtl']}}</td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td>{{$work_data->sodinv_hdrs_rec['hdr_status']}}</td>
                    <td>Total CRV Value:</td>
                    <td>${{$work_data->sodinv_hdrs_rec['total_crv_rtl']}}</td>
                </tr>
            </table>
            <br>

    <table class="table table-striped">
    
                    <tr>        
                        <th>Scn Number</th>
                        <th>Description</th>
                        <th>Itm Cst</th>
                        <th>Itm Rt1</th>
                        <th>Qty</th>
                        <th>Ext Cost</th>
                        <th>Ext Rt1</th>
                    </tr>
               
                
                @foreach($work_data->sodinv_dtls_rec as $key => $value)

                    <tr>

                        <td>{{$value['seq_number']}}</td>
                        <td>{{$value['item_desc']}}</td>
                        <td>${{$value['upc_cost']}}</td>
                        <td>${{$value['rtl_amt']}}</td>
                        <td>{{$value['quantity']}}</td>
                        <td>${{$value['ext_value_cst']}}</td>
                        <td>${{$value['ext_value_rtl']}}</td>
                    </tr>
                    @endforeach


                
            </table>
</div>  

</body>
</html>