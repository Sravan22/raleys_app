<!DOCTYPE html>
<html>
   <head>
      <style>
         table {
         font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
         font-size: 9px;
         color: #333;
         border-collapse: collapse;
         width: 100%;
         }
         .center {
          text-align: center;
         }
         td, th {
         border: 1px solid #dddddd;
         text-align: left;
         padding: 5px;
         }
         .center{
         text-align: center;
         }
         tr:nth-child(even) {
         background-color: #dddddd;
         }
      </style>
   </head>
   <body>
      <div class="row">
      <div class="col-md-6">
         <a class="custom-input-width pull-left" href="{{ ('/') }}">
         <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
         </a>
      </div>
      <h1 style="color:#ce0002;">Print Order Details Report</h1>
      <div class="container">
         <div class="row">
            <div class="col-md-4"><b>Order Date</b> : {{ $order_date }}</div>
            <div class="col-md-4">&nbsp;</div>
            <div class="col-md-4">&nbsp;</div>
         </div>
         <div class="row">
            <div class="col-md-8">
              <b> Status : </b>
               @if($status_code == "O")
               OPEN
               @elseif($status_code == "T")
               TRANSMITTED
               @elseif($status_code == "S")
               SENDING
               @elseif($status_code == "H")
               HOLD
               @elseif($status_code == "V")
               VOID
               @elseif($status_code == "R")
               RELEASED
               @elseif($status_code == "F")
               PLUSSED OUT
               @else
               UNKNOWN
               @endif
                  
               <span style="margin-left: 150px">{{ $description }}</span>
            </div>
            <div class="col-md-4">&nbsp;</div>
         </div>
         <br>
      </div>
      <div class="container">
         @if ($seq_number_result_array)
         <table>
            <tr>
               <th>SKU Number</th>
               <th>Item Number</th>
               <th>Description</th>
               <th>Pack Size</th>
               <th>Container Size</th>
               <th>Qty</th>
            </tr>
            @for ($i = 0; $i < count($seq_number_result_array); $i++)
            <tr>
               <td>{{ $seq_number_result_array[$i]['sku_number'] }}</td>
               <td>{{ $seq_number_result_array[$i]['item_number'] }}</td>
               <?php 
                  $soitem_result = DB::select('select * from soitem WHERE sku_number  = "'.$seq_number_result_array[$i]['sku_number'].'"');
                  $soitem_array = json_decode(json_encode($soitem_result), true);
                  ?>
               @if(!empty($soitem_array))
               <td>{{ $soitem_array[$i]['description'] }}</td>
               <td>{{ $soitem_array[$i]['case_pack'] }}</td>
               <td>{{ $soitem_array[$i]['cont_size'] }}</td>
               @else
               <td>ITEM DETAIL NOT FOUND</td>
               <td>0</td>
               <td>0</td>
               @endif 
               <td>{{ $seq_number_result_array[$i]['quantity'] }}</td>
            </tr>
            @endfor
            @endif
         </table>
      </div>
   </body>
</html>