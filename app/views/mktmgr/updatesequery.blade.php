

@extends('layout.dashboard')

@section('page_heading','Receivings')
@section('content')
@section('section')

<header class="row">
  @include('mktmgr.recivermenu')
</header>
<div class="container">
  <!-- <p id="menulist">
      <a href="{{ URL::route('mktmgr-receivings')}}" >Receivings</a> >> Store Expenses >>
      <a href="{{ URL::route('mktmgr-receivings-sequery')}}" >Query</a>
  </p> -->
          <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >Store Expenses </div>
                       </div> 

                 <div style="padding-top:5px" class="panel-body" >

            
                <form action="{{URL::route('mktmgr-post-receivings-sequery')}}" class="form-horizontal" method="post" role="form" style="display: block;">

                <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Store</label>
            <div class="col-sm-8">
                <input type="text" readonly="" class="form-control" id="store" placeholder="Store" name="store" value="{{ $rcv_data->store_number }}">
                 @if($errors->has('store'))
            {{ $errors->first('store')}}
            @endif
            </div>
        </div> 

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Vendor</label>
            <div class="col-sm-8">
            <input type="text" readonly="" class="form-control" id="vendor" placeholder="vendor" name="vendor" value="{{ $rcv_data->name }}">
               <span></span> 
                {{-- <input type="text" class="form-control" id="vendorname" placeholder="Vendor Number" name="vendorname" value="{{ $rcv_data->name }}">
                 @if($errors->has('vendorname'))
            {{ $errors->first('vendorname')}}
            @endif --}}
              
            </div>
        </div>  

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Vendor Number</label>
            <div class="col-sm-8">
                <input type="text" readonly="" class="form-control" id="vendornum" placeholder="Vendor Number" name="vendornum" value="{{ $rcv_data->vendor_number }}">
                 @if($errors->has('vendornum'))
            {{ $errors->first('vendornum')}}
            @endif
            </div>
        </div>
        <div class="focusguard" id="focusguard-1" tabindex="1"></div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Invoice Date</label>
            <div class="col-sm-8">
                <input type="date" class="form-control" id="invdate" placeholder="Invoice Date" name="invdate" value="{{ $rcv_data->invoice_date }}" autofocus=""  tabindex="2" />
                 @if($errors->has('invdate'))
            {{ $errors->first('invdate')}}
            @endif
            </div>
        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Invoice Number</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="invoice_number" placeholder="Invoice Number" name="invoice_number" value="{{ $rcv_data->id }}" tabindex="3" />
                 @if($errors->has('invoice_number'))
            {{ $errors->first('invoice_number')}}
            @endif
            </div>
        </div>

         <!-- <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Expense Type</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="expensetype" placeholder="Expense Type" name="expensetype" value="{{ $rcv_data->type_code }}">
                 @if($errors->has('expensetype'))
            {{ $errors->first('expensetype')}}
            @endif
            </div>
                 </div> -->
         <div class="form-group">
                <label for="#" class="col-sm-4 control-label">Expense Type</label>
                  <div class="col-sm-8">
                  <select class="form-control " id="expensetype" name="expensetype" tabindex="4">
                   <option <?php if($rcv_data->type_code == 'G') echo 'selected'; ?> value="G">G - General Repair </option>
                  <option <?php if($rcv_data->type_code == 'S') echo 'selected'; ?> value="S">S - Supply</option>
                  <option <?php if($rcv_data->type_code == 'V') echo 'selected'; ?> value="V">V-Service</option>
                     @if($errors->has('expensetype'))
            {{ $errors->first('expensetype')}}
            @endif
                               
                            </select>
                             
                           </div>
                        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Amount</label>
            <div class="col-sm-8">
                <input type="text" onKeyPress="return StopNonNumeric(this,event)" class="form-control float" id="amount" placeholder="Amount" name="amount" tabindex="5"  value="{{ number_format($rcv_data->tot_vend_cost,2); }}"  />
                 <span class="control-label"></span>
                 @if($errors->has('amount'))
            {{ $errors->first('amount')}}
            @endif 
            </div>
        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Status</label>
            <div class="col-sm-8">
                <select class="form-control"  id="status" name="status" tabindex="6">
                    <option <?php if($rcv_data->status_code == 'O') echo 'selected'; ?> value="O">O - Open</option>
                    <option <?php if($rcv_data->status_code == 'V') echo 'selected'; ?> value="V">V - Void</option>
                </select>
                 @if($errors->has('status'))
            {{ $errors->first('status')}}
            @endif
            </div>
        </div>  
        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Created</label>
            <div class="col-sm-8">
               <input type="text" readonly="" class="form-control" id="create_datetime" placeholder="Amount" name="create_datetime" value="{{date("m-d-Y H:i:s", strtotime($rcv_data->create_datetime)); }}">
                 @if($errors->has('status'))
            {{ $errors->first('status')}}
            @endif
            </div>
        </div>    

                  
          <div class="form-group">
            <div class="row">
               <div class="col-sm-12">
    <input type="submit" name="login-submit" style="margin-left: 43%" id="submit" tabindex="7" value="Update" class="btn">
       
        <input type="button" tabindex="8"  id="btn" class="btn" value="Cancel" onclick="history.go(-1);" />
                                        {{ Form::token()}}
              </div>
            </div>
          </div>
          
<div class="focusguard" id="focusguard-2" tabindex="9"></div>
        </form>
                
              
          </div>
        </div>
      </div>
</div>
@stop

<script type="text/javascript" src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>

<script type="text/javascript">
   $(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
   
    var day = dtToday.getDate();
    //alert(day);
    var year = dtToday.getFullYear();
    //alert(year);
   
    if(month < 10)
        month = '0' + month.toString();
   
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //var newdate = new Date(maxDate);
    //newdate.setDate(newdate.getDate() - 180);
    //alert(newdate);*/
    //var minDate = new Date(newdate);
   
    $('#invdate').attr('max', maxDate);
    
   }); 

   jQuery(document).ready(function($) {


      $('#focusguard-2').on('focus', function() {
      $('#invdate').focus();
      });

      $('#focusguard-1').on('focus', function() {
      $('#btn').focus();
      });

     $('#amount').on("click",function (){
       $(this).select();
       });
      $('#amount').on('keyup',function(){
            var num = $(this).val(); 
           console.log(num)
           if(num<=0) {
           $(this).parent().addClass('has-error');
           $(this).next('.control-label').text('Expense Amount can not be zero')
           $('#submit').addClass('disabled');
           }
           
           else {
           $(this).parent().addClass('has-success');
            $(this).parent().removeClass('has-error');
            
             $(this).next('.control-label').text('');
              $('#submit').removeClass('disabled');
           }
      })
      $(".float").blur(function(){  
       var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
   
     });
      
    }); 
   function StopNonNumeric(el, evt)
       {
           var charCode = (evt.which) ? evt.which : event.keyCode;
           var number = el.value.split('.');
           if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
               return false;
           }
           //just one dot (thanks ddlab)
           if(number.length>1 && charCode == 46){
                return false;
           }
           //get the carat position
           var dotPos = el.value.indexOf(".");
           if( dotPos>-1 && (number[1].length > 3)){
               return false;
           }
           return true;
       }
   
   
   </script>