<?php 
   //echo '<pre>',print_r($out_mainrpt1);exit();
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="panel">
         <div class="" style="margin-left: 78px;margin-bottom:-23px;font-weight:bold;">
            {{--  
            <div class="panel-title">Weekly Sales Recap</div>
            --}}
         </div>
         <div class="" style=" text-align:right; font-weight:bold;">
            <div class="panel-title col-md-6" style="font-size: 20px;">
               Raley's / Bel Air
            </div>
            <div class="col-md-6">
               <span class="pull-right">
               <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
                  {{--  <a href="#" target="_blank">
                   <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
                   </a> --}}
               </span>

            </div>
         </div>
         <br>
         <div style="padding-top:30px" class="panel-body">
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">PERIOD</th>
                        <th class="text-center">WEEK-ENDING DATE</th>
                        <th class="text-center">MARKET NUMBER</th>
                        <th class="text-center">DRUG NUMBER</th>
                        <th class="text-center">COMPETITOR COUPON</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td class="text-center"><b> {{ $pwmdc['per'] }} </b></td>
                        <td class="text-center"><b> {{ $pwmdc['end_date'] }} </b></td>
                        <td class="text-center"><b> {{ $pwmdc['str_num'] }} </b></td>
                        <td class="text-center"><b> {{ $pwmdc['drug_num'] }} </b></td>
                        <td class="text-center"><b>.00 </b></td>
                     </tr>
                  </tbody>
               </table>
            </div>
         
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">CUSTOMER COUNT</th>
                        <th class="text-center">1</th>
                        <th class="text-center" colspan="6" style="display: none;">&nbsp;</th>
                     </tr>
                     <tr>
                        <th class="text-center">DEPARTMENT</th>
                        <th class="text-center">SALES</th>
                        <th class="text-center">FISH & GAME</th>
                        <th class="text-center">A/R ADJ</th>
                        <th class="text-center">MISC ADJ</th>
                        <th class="text-center">ADJ SALES</th>
                        <th class="text-center">CUST CT</th>
                        <th class="text-center">ACCT NUMBER</th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_sales = $fishgame_total = $aradj_total = $misc_total = $rowTotal = $cust_cnt_total = 0;
                  for($i=0;$i<count($out_mainrpt1);$i++)
                  {
                   $total_sales = $total_sales + $out_mainrpt1[$i]['dept_sales'];
                   $fishgame_total = $fishgame_total + $out_mainrpt1[$i]['fishgame'];
                   $aradj_total = $aradj_total + $out_mainrpt1[$i]['aradj'];
                   $misc_total = $misc_total + $out_mainrpt1[$i]['misc'];
                   $rowTotal = $rowTotal + $out_mainrpt1[$i]['rowTotal'];
                   $cust_cnt_total = $cust_cnt_total + $out_mainrpt1[$i]['cust_cnt'];
                  ?>
                     <tr>
                        <td class="text-center"> {{ $out_mainrpt1[$i]['item_desc'] }} </td>
                        <td class="text-center">{{ number_format((float)$out_mainrpt1[$i]['dept_sales'], 2, '.', ''); }} </td>
                        <td class="text-center"> {{ number_format((float)$out_mainrpt1[$i]['fishgame'], 2, '.', ''); }}</td>
                        <td class="text-center">{{ number_format((float)$out_mainrpt1[$i]['aradj'], 2, '.', ''); }} </td>
                        <td class="text-center">{{ number_format((float)$out_mainrpt1[$i]['misc'], 2, '.', ''); }} </td>
                        <td class="text-center">{{ number_format((float)$out_mainrpt1[$i]['rowTotal'], 2, '.', ''); }} </td>
                        <td class="text-center">{{ $out_mainrpt1[$i]['cust_cnt'] }}  </td>
                        <td class="text-center">{{ $out_mainrpt1[$i]['acctnum'] }} </td>
                     </tr>
                     <?php 
                        }
                     ?>
                     <tr bgcolor="#ada4a4">
                     <th class="text-center">Total Sales Transactions</th>
                        <td class="text-center">
                           {{ number_format((float)$total_sales, 2, '.', ''); }}
                        </td>
                        <td class="text-center">
                           {{ number_format((float)$fishgame_total, 2, '.', ''); }}
                        </td>
                        <td class="text-center">
                           {{ number_format((float)$aradj_total, 2, '.', ''); }}
                        </td>
                        <td class="text-center">
                           {{ number_format((float)$misc_total, 2, '.', ''); }}
                        </td>
                        <td class="text-center">
                           {{ number_format((float)$aradj_total, 2, '.', ''); }}
                        </td>
                        <td class="text-center">
                           {{  $cust_cnt_total  }}
                        </td>
                        <td>&nbsp;</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <h4 class="text-center"><b>Weekly Sales Recap of Net Safe</b></h4>
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">ITEM DESCRIPTION</th>
                        <th class="text-center">ITEM AMOUNT</th>
                        <th class="text-center">OFFICE ADJ</th>
                        <th class="text-center">ACCOUNT NUMBER</th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                  $netsafe_total = 0;
                  for($i=0;$i<count($out_netsaferpt);$i++)
                  {
                   $netsafe_total = $netsafe_total + $out_netsaferpt[$i]['dept_sales'];
                  ?>
                     <tr>
                        <td class="text-center">{{ $out_netsaferpt[$i]['item_desc'] }} </td>
                        <td class="text-center">{{ number_format((float)$out_netsaferpt[$i]['dept_sales'], 2, '.', ''); }}</td>
                        <td class="text-center">  &nbsp; </td>
                        <td class="text-center"> {{ $out_netsaferpt[$i]['acctnum'] }} </td>
                     </tr>
                     <?php 
                        }
                     ?>
                      <tr bgcolor="#ada4a4">
                        <th class="text-center">Net Safe Activity</th>
                        <th class="text-center">{{ number_format((float)$netsafe_total, 2, '.', ''); }} </th>
                        <th class="text-center" colspan="2"></th>
                     </tr>
                  </tbody>
               </table>
            </div>


            <h4 class="text-center"><b>Weekly Sales Recap of Deposits</b></h4>
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">DAY</th>
                        <th class="text-center">DATE</th>
                        <th class="text-center">ITEM DESCRIPTION</th>
                        <th class="text-center">ITEM AMOUNT</th>
                        <th class="text-center">OFFICE ADJ</th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                     $deposits_final_total = 0;
                     for($i=0;$i<count($out_deprpt);$i++)
                     {
                        $deposits_total = 0;
                        for($j=0;$j<count($out_deprpt[$i]);$j++)
                        {
                        $deposits_total = $deposits_total + $out_deprpt[$i][$j]['item_amt'];

                        $weekday = date('l', strtotime($out_deprpt[$i][$j]['wk_end_date']));
                     ?>
                     <tr>
                        <td class="text-center"> {{ $weekday }} </td>
                        <td class="text-center">{{ date('m/d/Y',strtotime($out_deprpt[$i][$j]['wk_end_date'])) }} </td>
                        <td class="text-center"> {{ $out_deprpt[$i][$j]['item_desc'] }} </td>
                        <td class="text-center"> {{ number_format((float)$out_deprpt[$i][$j]['item_amt'], 2, '.', ''); }} </td>
                        <td class="text-center"></td>
                     </tr>
                     <?php
                        }
                        $deposits_final_total = $deposits_final_total + $deposits_total;
                        ?>
                        <tr bgcolor="#ada4a4">
                        <th class="text-center" colspan="2"></th>
                        <th class="text-center">TOTAL</th>
                        <th class="text-center"> {{ number_format((float)$deposits_total, 2, '.', ''); }} </th>
                        <td class="text-center"></td>
                     </tr>
                     <?php }
                     ?>
                     <tr>
                        <th class="text-center">&nbsp;</th>
                        <th class="text-center">&nbsp;</th>
                        <th class="text-center">&nbsp;</th>
                        <th class="text-center">&nbsp;</th>
                        <th class="text-center">&nbsp;</th>
                     </tr>
                     <tr bgcolor="#ada4a4">
                        <th class="text-center" colspan="2"></th>
                        <th class="text-center">Total Of Deposits</th>
                        <th class="text-center">{{ $deposits_final_total }}</th>
                        <td class="text-center"></td>
                     </tr>
                  </tbody>
               </table>
            </div>


            <h4 class="text-center"><b>Weekly Sales Recap of Store Transactions</b></h4>
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">ITEM DESCRIPTION</th>
                        <th class="text-center">ITEM AMOUNT</th>
                        <th class="text-center">OFFICE ADJ</th>
                        <th class="text-center">ACCOUNT NUMBER</th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                  $st_total = 0;
                  for($i=0;$i<count($out_strpt);$i++)
                  {
                   $st_total = $st_total + $out_strpt[$i]['item_amt'];
                  ?>
                     <tr>
                        <td class="text-center">{{ $out_strpt[$i]['item_desc'] }}</td>
                        <td class="text-center"> {{ number_format((float)$out_strpt[$i]['item_amt'], 2, '.', ''); }} </td>
                        <td class="text-center">  &nbsp; </td>
                        <td class="text-center"> {{ $out_strpt[$i]['acctnum'] }}  </td>
                     </tr>
                     <?php 
                        }
                     ?>
                     <tr bgcolor="#ada4a4">
                        <th class="text-center">Total Store Transactions</th>
                        <th class="text-center">{{ number_format((float)$st_total, 2, '.', ''); }}</th>
                        <th class="text-center" colspan="2"></th>
                        
                     </tr>
                  </tbody>
               </table>
            </div>

            <h4 class="text-center"><b>Weekly Sales Recap of Paid Out Types</b></h4>
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">ITEM DESCRIPTION</th>
                        <th class="text-center">ITEM AMOUNT</th>
                        <th class="text-center">OFFICE ADJ</th>
                        <th class="text-center">ACCOUNT NUMBER</th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                  $pot_total = 0;
                  for($i=0;$i<count($out_porpt);$i++)
                  {
                   $pot_total = $pot_total + $out_porpt[$i]['item_amt'];
                  ?>
                     <tr>
                        <td class="text-center"> {{ $out_porpt[$i]['item_desc'] }} </td>
                        <td class="text-center"> {{ number_format((float)$out_porpt[$i]['item_amt'], 2, '.', ''); }} </td>
                        <td class="text-center">  &nbsp; </td>
                        <td class="text-center"> {{ $out_porpt[$i]['acctnum'] }}  </td>
                     </tr>
                     <?php 
                        }
                     ?>
                     <tr bgcolor="#ada4a4">
                        <th class="text-center">Grand Total of Paid Outs</th>
                        <th class="text-center">{{ number_format((float)$pot_total, 2, '.', ''); }}</th>
                        <th class="text-center" colspan="2"></th>
                     </tr>
                     <tr bgcolor="#ada4a4">
                        <th class="text-center">Net Store/Paid Outs</th>
                        <th class="text-center">{{ number_format((float)$get_netstore, 2, '.', ''); }}</th>
                        <th class="text-center" colspan="2"></th>
                     </tr>
                  </tbody>
               </table>
            </div>

            <h4 class="text-center"><b>Summary of Weekly Sales Recap </b></h4>
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">ITEM DESCRIPTION</th>
                        <th class="text-center">ITEM AMOUNT</th>
                        <th class="text-center">OFFICE ADJ</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <th class="text-center">Total Sales & Tax </th>
                        <td class="text-center">{{ number_format((float)$out_summaryrpt['totalsales'], 2, '.', ''); }} </td>
                        <td class="text-center">  &nbsp; </td>
                     </tr>
                      <tr>
                        <th class="text-center"> Net Safe Activity </th>
                        <td class="text-center"> {{ number_format((float)$out_summaryrpt['netsafe'], 2, '.', ''); }} </td>
                        <td class="text-center">  &nbsp; </td>
                     </tr>
                     <tr>
                        <th class="text-center"> Grand Total of Deposits </th>
                        <td class="text-center"> {{ number_format((float)$out_summaryrpt['deposits'], 2, '.', ''); }}</td>
                        <td class="text-center">  &nbsp; </td>
                     </tr>
                     <tr>
                        <th class="text-center"> Net Transfers & Deliveries </th>
                        <td class="text-center"> {{ number_format((float)$out_summaryrpt['del_tranfs'], 2, '.', ''); }} </td>
                        <td class="text-center">  &nbsp; </td>
                     </tr>
                     <tr>
                        <th class="text-center"> Net Store/Paid Out Transactions </th>
                        <td class="text-center"> {{ number_format((float)$out_summaryrpt['po_types'], 2, '.', ''); }} </td>
                        <td class="text-center">  &nbsp; </td>
                     </tr>
                     <tr>
                        <th class="text-center">&nbsp;</th>
                        <th class="text-center">&nbsp;</th>
                        <th class="text-center">&nbsp;</th>
                     </tr>
                     <tr bgcolor="#ada4a4">
                        <th class="text-center">Total Should Be Zero '0'</th>
                        <th class="text-center">{{ number_format((float)$out_summaryrpt['salessum'], 2, '.', ''); }}</th>
                        <th class="text-center">&nbsp;</th>
                     </tr>
                  </tbody>
               </table>
            </div>

            <h4 class="text-center"><b>Weekly Comments</b></h4>
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">DATE</th>
                        <th class="text-center">COMMENT</th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                     for($i=0;$i<count($out_comments);$i++)
                     {
                  ?>
                     <tr>
                        <td class="text-center">{{ date('m/d/Y',strtotime($out_comments[$i]['date_stamp'])) }}</td>
                        <td class="text-center"> {{ $out_comments[$i]['comment_line'] }}</td>
                     </tr>
                     <?php 
                        }
                     ?>
                  </tbody>
               </table>
            </div>

         </div>
      </div>
   </div>
</div>
@stop