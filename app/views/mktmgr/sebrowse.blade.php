

@extends('layout.dashboard')
@section('page_heading','Receivings >> Printer Selection Menu')
@section('content')
@section('section')
<style>
@media print 
{
  a[href]:after { content: none !important; }
  img[src]:after { content: none !important; }
}
</style>
<header class="row">
        @include('mktmgr.recivermenu')
    </header>
<div class="container">
<div class="col-md-6">
          <div class="menu_search_line">
      <div class="dropdown sub">
         <a  href="{{ URL::route('mktmgr-receivings-sequery')}}" >
         Exit
         </a>
      </div>
   </div>
      </div>
<div class="col-md-6 text-right">
           @if(count($receivings) > 0)
          <a href="" onclick="printPage(); return false"> 
            <i class="fa fa-print fa-fw iconsize"></i> 
          </a>
          {{-- <a href="{{ route('pdf-expensesreport',['download'=>'pdf', 'frmdt' => $frmdt, 'todt' => $todt]) }}" media="print" target="_blank"> --}}
            {{-- <i class="fa fa-file-pdf-o fa-fw iconsize"></i> --}}
          </a>
          @endif
        </div>
        
    <table class="table table-striped">
    <thead>
      <tr>
        <th>Invoice Date</th>
        <th>Invoice Number</th>
        <th>Vendor Name</th>
        <th>Type</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    @if(count($receivings) > 0)
    @foreach ($receivings as $rcv)
      <tr>
        <td><u>{{ HTML::link(URL::route('mktmgr-view-storeexpenses',['id' => $rcv->seq_number]), date("m/d/Y", strtotime($rcv->invoice_date))) }}</u></td>
        <td><u>{{ HTML::link(URL::route('mktmgr-update-receivings-sequery',['id' => $rcv->seq_number]), $rcv->id) }}</u></td>
        <td>{{ $rcv->vname }}</td>
        @if($rcv->type_code =='G')
        <td>General</td>
        @elseif($rcv->type_code =='S')
        <td>Supply</td>
        @elseif($rcv->type_code =='V')
        <td>Service</td>
        @endif
       
        {{-- <td>{{ $rcv->status_code }}</td> --}}
            @if($rcv->status_code == 'A') 
            <td>Accepted</td>
            @elseif ($rcv->status_code == 'H')
            <td>On Hold</td>
            @elseif ($rcv->status_code == 'I')
            <td>Incomplete</td>
            @elseif ($rcv->status_code == 'O')
            <td>Open</td>
            @elseif ($rcv->status_code == 'R')
            <td>Reviewed</td>
            @elseif ($rcv->status_code == 'U')
            <td>Uploaded</td>
            @elseif ($rcv->status_code == '')
            <td>Open</td>
            @elseif ($rcv->status_code == 'V')
            <td>Voided</td>
            @endif
      </tr>
     @endforeach
     @else
      <tr><td colspan="6" align="center">No records found!</td></tr> 
    @endif
    </tbody>
  </table> 
  <input type="button" class="btn" style="margin-left: 48%" name="" onclick="history.go(-1);" value="Back">         
 
</div>
@stop
