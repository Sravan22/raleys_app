<?php
  // echo '<pre>';print_r($sotrnhdr_rec);exit; 
?>
@extends('layout.dashboard')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<div class="container">
   <div class="row">
      <div class="col-md-6">
         <h3>Planned and Recent Plus Outs...</h3>
      </div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
        <a href="{{ route('pdf-report-plusout',['download'=>'pdf'])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
<div class="container">
{{-- @if ($print_order_result_array) --}}
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Ship Date</th>
            <th>Order Type</th>
            <th>Total Lines</th>
            <th>Total Units</th>
         </tr>
        </thead> 
        <tbody>
         @for ($i = 0; $i < count($sotrnhdr_rec); $i++)   
         <tr>
        <?php 
           $order_date =  DB::select('SELECT order_date  FROM soordhdr   WHERE seq_number = "'.$sotrnhdr_rec[$i]->seq_number.'" ');
           $order_date_array = json_decode(json_encode($order_date), true);
           if(!empty($order_date_array))
           {  
               $params = array(
                        'seq_number' =>  $sotrnhdr_rec[$i]->seq_number
                        ); 
              $queryString = http_build_query($params);
                $order_date = date("m/d/Y", strtotime($order_date_array[0]['order_date']));
               
                echo '<td><u>'.HTML::link(URL::route('mktmgr-plusoutsitems',$queryString), $order_date).'</u></td>';
           }
           $description =  DB::select('SELECT description FROM sotrncfg  WHERE type_code    = "'.$sotrnhdr_rec[$i]->type_code.'"  AND subtype_code = "'.$sotrnhdr_rec[$i]->subtype_code.'" ');
           $description_array = json_decode(json_encode($description), true);
            if(!empty($description_array))
           {

                echo '<td>'.$description_array[0]['description'].'</td>';
           }
             $tot_units_tot_lines =  DB::select('SELECT count(*) as tot_lines, sum(quantity) as tot_units  FROM soorditm  WHERE seq_number = "'.$sotrnhdr_rec[$i]->seq_number.'"');
             $tot_units_tot_lines_array = json_decode(json_encode($tot_units_tot_lines), true);
             if(!empty($tot_units_tot_lines_array))
            {
                echo '<td>'.$tot_units_tot_lines_array[0]['tot_lines'].'</td>';
                echo '<td>'.$tot_units_tot_lines_array[0]['tot_units'].'</td>';
             }

           // $description =  DB::select('SELECT description  FROM sotrncfg WHERE type_code = "'.$sotrnhdr_rec[$i]->type_code.'"  AND subtype_code = "'.$sotrnhdr_rec[$i]->subtype_code.'" ');
           //  $description_array = json_decode(json_encode($description), true);
           //  echo '<pre>';print_r($description_array);
           //  if(!empty($description_array))
           // {
           //  //echo '<pre>';print_r($order_date_array);
           //       echo '<td>'.$description_array[0]['description'].'</td>';
           // }
            //$order_date_array = json_decode(json_encode($order_date), true);
        ?>
             {{--  <td>GROCERY SSI DRY PLUS OUT</td>
              <td>1</td>
              <td>1</td> --}}
         </tr>
        
         @endfor
{{--
        @else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Store Transfers meet Query criteria.
            </div>
        @endif --}}
               
      </tbody>
   </table>
<input type="button" style="margin-left: 50%" class="btn" name="" onclick="window.location.href='{{URL::route('mktmgr-storeorderview')}}'" value="Back"/>  
</div>
@stop