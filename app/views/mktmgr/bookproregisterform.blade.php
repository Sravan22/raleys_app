<?php
  //echo $dayIsLocked;exit;
 //echo '<pre>';print_r($beginloandata);exit;

?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style type="text/css">
  .form-horizontal .control-label {
    text-align: right !important;
    /* padding-left: 60px; */
}
</style>
<div class="container">
   <div class="flash-message">
   @if($dayIsLocked == 'Y')
   <p class="alert alert-info">Data is locked for {{ date("m/d/Y", strtotime($loandate)) }}. Any changes won't be saved <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
   @endif
</div>
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >BEGIN LOAN DETAIL &nbsp;&nbsp;&nbsp; Register {{ $reg_number }} &nbsp;&nbsp;&nbsp;{{ date('m/d/Y',strtotime($loandate)) }}</div>
         </div>
         <form action="{{URL::route('mktmgr-postbeginloanreg-update')}}" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;">
            <div class="form-group">
               <label for="inputEmail" class="control-label col-sm-4">Food Stamps</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="food_stamps" placeholder="0.00" autofocus="" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['food_stamps'] }}" name="food_stamps">
                  @if($errors->has('food_stamps'))
                  {{ $errors->first('food_stamps')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Ones</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="ones"  name="ones" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['ones'] }}" >
                  @if($errors->has('ones'))
                  {{ $errors->first('ones')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Fives</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="fives"  name="fives" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['fives'] }}">
                  @if($errors->has('fives'))
                  {{ $errors->first('fives')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Tens</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="tens"  name="tens" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['tens'] }}">
                  @if($errors->has('tens'))
                  {{ $errors->first('tens')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Twenties</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="twenties"  name="twenties" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['twenties'] }}">
                  @if($errors->has('twenties'))
                  {{ $errors->first('twenties')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Rolled Coins</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="rolled_coins"  name="rolled_coins" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> placeholder="0.00" value="{{ $beginloandata['rolled_coins'] }}">
                  @if($errors->has('rolled_coins'))
                  {{ $errors->first('rolled_coins')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Quarters</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="quarters"  name="quarters" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['quarters'] }}">
                  @if($errors->has('quarters'))
                  {{ $errors->first('quarters')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Dimes</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="dimes"  name="dimes" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['dimes'] }}">
                  @if($errors->has('dimes'))
                  {{ $errors->first('dimes')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Nickles</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="nickles"  name="nickles" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['nickles'] }}">
                  @if($errors->has('nickles'))
                  {{ $errors->first('nickles')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Pennies</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="pennies"  name="pennies" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['pennies'] }}">
                  @if($errors->has('pennies'))
                  {{ $errors->first('pennies')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Misc</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="misc"  name="misc" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ $beginloandata['misc'] }}">
                  @if($errors->has('misc'))
                  {{ $errors->first('misc')}}
                  @endif
               </div>
            </div>
            <hr style="margin-right: 105px;">
            <div class="form-group">
               <label for="inputPassword" class="col-sm-4 text-right">Total</label>
               <div class="col-sm-8">
               <b><span class="totalvalue">0.00</span></b>
                  <input type="hidden" class="form-control totalvalue" id="bl_total"  name="bl_total" readonly="readonly" {{ $beginloandata['bl_total'] }}>
                  {{-- <input type="hidden" name="querymake" id="querymake" value="" /> --}}
                  @if($errors->has('bl_total'))
                  {{ $errors->first('bl_total')}}
                  @endif
               </div>
            </div>
            <hr style="margin-right: 105px;">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
               @if($dayIsLocked == 'Y')
               <div class="col-sm-12" align="center">
                  <input type="button" style="margin-right:90px;" value="Go Back" class="btn" id="cancel" name="cancel" onclick="goBack()">
                  {{ Form::token()}}
               </div>
               @else
               <div class="col-sm-12" align="center">
                  <input type="submit" style="margin-left:-51px;" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeperregister')}}'" />
                  {{ Form::token()}}
               </div>
               @endif                
            </div>
            </div>
          <br>
         </form>
      </div>
   </div>
</div>


<style type="text/css">
  @media (min-width: 992px){
.col-md-offset-1 {
    margin-left: 18.333333% !important;
}
}
</style>
<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>     
<script>
//   $(document).ready(function() {
//     $(".alltotal").each(function() {
//          $(this).keyup(function() {
//          calculateSum ();
//         });
//     });
// });

// function calculateSum() {
//     var sum = 0;
//     $(".alltotal").each(function() {
//         if (!isNaN(this.value) && this.value.length != 0) {
//             sum += parseFloat(this.value);
//          }
//     });
//     $(".totalvalue").val(sum.toFixed(2));
//     $(".totalvalue").text(parseFloat(sum.toFixed(2)));
//  }


 $(document).ready(function() {

  $(".form-control").keydown(function (e) {
   // Allow: backspace, delete, tab, escape, enter 
   
   if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl/cmd+A
       (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
       (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
       (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
       (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
   }

   // Ensure that it is a number and stop the keypress
   if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
       e.preventDefault();
   }

 
   });
    $(".alltotal").on("click", function () {
      $(this).select();
   });
 });

 /*$(".alltotal").one('click',function(){  
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval*100));
        $(".totalvalue").text(sum.toFixed(2));
       return true;
    });

 $(".alltotal").one('blur',function(){  
        var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
        $('.alltotal').each(function() { 
         sum += parseFloat($(this).val()); 
        });
         $(".totalvalue").val(sum.toFixed(2));
       $(".totalvalue").text(sum.toFixed(2));

       return true;
    });  */
 $(".alltotal").focus(function(event) {
      var sum = 0; 
      selfield = this.id;
      //this.select();
      // $("#safecoinsubmit").removeAttr('disabled');
      selfieldval=$("#"+selfield).val(); 
      $("#"+selfield).val(parseFloat(selfieldval*100));
      this.select();
      });
    
$(".alltotal").blur(function(){  
       var sum = 0; 
        selfield = this.id;
       
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
        $('.alltotal').each(function() { 
         sum += parseFloat($(this).val()); 
        });
         $(".totalvalue").val(sum.toFixed(2));
        $(".totalvalue").text(sum.toFixed(2));
               return true;
     });
     

   function goBack() {
       window.history.back();
   }
</script>  
@stop
