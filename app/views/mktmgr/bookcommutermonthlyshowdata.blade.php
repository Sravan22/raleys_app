@extends('layout.dashboardbookkeepermarket')
@section('page_heading','Commuter')
@section('content')
@section('section')

<header class="row">
    @include('mktmgr.bookkepermenu')
</header>
<style type="text/css">
 /* .form-horizontal .control-label {
    text-align: left !important; 
}*/
</style>
<div class="container">

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div id="loginbox" style="margin-top:10px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >MTHLY COMMUTER INVENTORY</div>
            </div> 

            <div style="padding-top:10px" class="panel-body" >


                <form action="{{URL::route('mktmgr-bookcommutermonthlypost')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                    
                   <input type="hidden" class="form-control" id="inv_type" placeholder="" name="inv_type" value="{{$data->inv_type}}" >
                      <input type="hidden" class="form-control" id="" placeholder="" name="desc" value="{{$data->desc}}" >
                       <input type="hidden" class="form-control" id="" placeholder="" name="comm_month" value="{{$data->comm_month}}" >
                        <input type="hidden" class="form-control" id="comm_date" placeholder="" name="comm_date" value="{{date('Y-m-d',strtotime($data->dateofinfo))}}" >
                        
                    <div class="form-group">
                      
                        <div class="col-sm-3 text-center">
                            <b>{{$data->inv_desc}}</b>
                            {{-- <input type="text" class="form-control" id="" placeholder="" name="" value="{{$data->inv_desc}}" readonly="">
                          
                            
                            @if($errors->has('inv_type'))
                            {{ $errors->first('inv_type')}}
                            @endif --}}
                        </div>
                         <div class="col-sm-3 text-center">
                         <b>{{$data->desc_desc}}</b>
                            {{-- <input type="text" class="form-control" id="" placeholder="" name="" value="{{$data->desc_desc}}" readonly="">
                           
                            @if($errors->has('desc'))
                            {{ $errors->first('desc')}}
                            @endif --}}
                        </div>
                        
                        <div class="col-sm-2 text-center">
                        <b>{{ date('F', mktime(0, 0, 0, $data->comm_month, 10)); }}</b>
                            {{-- <input type="text" class="form-control" id="" placeholder="" name="" value="{{ date('F', mktime(0, 0, 0, $data->comm_month, 10)); }}" readonly="">
                            
                            @if($errors->has('comm_month'))
                            {{ $errors->first('comm_month')}}
                            @endif --}}
                        </div>
                        
                        <div class="col-sm-4 text-center">
                        <b>{{date('m/d/Y',strtotime($data->dateofinfo))}}</b>
                            {{-- <input type="text" class="form-control" id="" placeholder="" name="" value="{{date('m/d/Y',strtotime($data->dateofinfo))}}" readonly="">
                            
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif --}}
                        </div>
                    </div>
                    
                    
                    
                    <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-9">Price</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="price" placeholder="" tabindex="1" name="price" value="{{$data->price}}">
                            @if($errors->has('price'))
                            {{ $errors->first('price')}}
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-9">Delivery or Store Transfer Number 1</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="trf_no_1" tabindex="2" placeholder="" name="trf_no_1" value="{{$data->trf_no_1}}">
                            @if($errors->has('trf_no_1'))
                            {{ $errors->first('trf_no_1')}}
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-9">Delivery or Store Transfer Number 2</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" tabindex="3" id="trf_no_2" placeholder="" name="trf_no_2" value="{{$data->trf_no_2}}">
                            @if($errors->has('trf_no_2'))
                            {{ $errors->first('trf_no_2')}}
                            @endif
                        </div>
                    </div>
                    
                    
                    
                    <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-6">&nbsp;</label>
                        
                         <label for="inputPassword" class="control-label col-sm-3" style="margin-left: -43px">COUNT</label>
                         <label for="inputPassword" class="control-label col-sm-3" style="margin-left: 0px">AMOUNT</label>
                    </div>
                    
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-6">Beginning Inventory</label>
                        <div class="col-sm-3">
                             <input type="text" class="form-control" id="begin_inv" placeholder="" name="begin_inv" value="{{$data->begin_inv}}" readonly="">
                            @if($errors->has('begin_inv'))
                            {{ $errors->first('begin_inv')}}
                            @endif
                            
                        </div>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="begin_inv_amt" placeholder="" name="" value="{{$data->begin_inv_amt}}" readonly="">
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-6">Deliveries</label>
                       
                        <div class="col-sm-3">
                            <input type="text" class="form-control" tabindex="4" id="deliveries" placeholder="" name="deliveries" value="{{$data->deliveries}}">
                            @if($errors->has('deliveries'))
                            {{ $errors->first('deliveries')}}
                            @endif
                        </div>
                         <div class="col-sm-3">
                             <input type="text" class="form-control" id="deliveries_amt" placeholder="" name="" value="{{$data->deliveries_amt}}" readonly="">
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-6">Returns</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" tabindex="5" id="returns" placeholder="" name="returns" value="{{$data->returns}}">
                            @if($errors->has('returns'))
                            {{ $errors->first('returns')}}
                            @endif
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="returns_amt" placeholder="" name="" value="{{$data->returns_amt}}" readonly="">
                           
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-6">Ending Inventory</label>
                        
                         <div class="col-sm-3">
                             <input type="text" class="form-control" tabindex="6" id="end_inv" placeholder="" name="end_inv" value="{{$data->end_inv}}">
                            @if($errors->has('end_inv'))
                            {{ $errors->first('end_inv')}}
                            @endif
                        </div>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="end_inv_amt" placeholder="" name="" value="{{$data->end_inv_amt}}" readonly="">
                           
                        </div>
                       
                    </div>
                    
                    
                    
                    <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-9">Sales</label>
                        <div class="col-sm-3">
                        {{-- <span id="sales_amt"><b>{{$data->sales_amt}}</b></span> --}}
                            <input type="text" readonly="" class="form-control" id="sales_amt" placeholder="" name="sales_amt" value="{{$data->sales_amt}}">
                           
                        </div>
                      
                    </div>


                    

                    <div class="form-group" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <input type="submit" name="accept-submit" id="submit" tabindex="7" value="Accept" class="btn">  
                              <input type="submit" name="login-submit" id="submit" tabindex="8" value="Cancel" class="btn">
                                {{ Form::token()}}
                            </div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>
</div>
</div>

  

@section('jssection')
 @parent
 <script>
     function convertnulltozero(val){
         
          if(isNaN(val)==true || val==NaN || val==null || val===undefined || val=='NaN' || typeof(val)=='undefined' || val==''){
             
                val=0;
                
            } else {
                val=val.toFixed(2);
            }
            
          return val;
      }
      
      function isFloat(num){
         
        if(num.toString().indexOf(".")==-1){
            return false;
        }
        return true;
    }
       jQuery(document).ready(function($) {
           
            $("#price, #trf_no_1, #trf_no_2,#begin_inv,#deliveries,#returns,#end_inv").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
           $( "#price" ).on( "blur", function() {
             if(isFloat($(this).val())==true){
                   
               } else {
                var nw=parseFloat($(this).val())/100;

                 $(this).val(convertnulltozero(nw));
                }
            });
              
          $( "#price, #trf_no_1, #trf_no_2,#begin_inv,#deliveries,#returns,#end_inv" ).on( "change keyup", function() {              
                calculateSalesDiff();
          });
          
          function calculateSalesDiff(){
              
              var price=parseFloat($("#price").val());
              var trf_no_1=parseFloat($("#trf_no_1").val());
              var trf_no_2=parseFloat($("#trf_no_2").val());
              var begin_inv=parseFloat($("#begin_inv").val());
              var deliveries=parseFloat($("#deliveries").val());
              var returns=parseFloat($("#returns").val());
              var end_inv=parseFloat($("#end_inv").val());
             
             
             
              var sales_amt=0;

              var deliveries_amt = (deliveries * price);
              $("#deliveries_amt").val(convertnulltozero(deliveries_amt));
              
              
              var returns_amt = (returns * price);
              $("#returns_amt").val(convertnulltozero(returns_amt));
              
              var end_inv_amt = (end_inv * price);
              $("#end_inv_amt").val(convertnulltozero(end_inv_amt));
              
              var begin_inv_amt = (begin_inv * price);
              $("#begin_inv_amt").val(convertnulltozero(begin_inv_amt));
                    
                  
              
               sales_amt =   (begin_inv_amt + deliveries_amt - returns_amt - end_inv_amt);
                 
              
                if(sales_amt==NaN || sales_amt==undefined){
                    sales_amt=0;
                }
                
               
                $("#sales_amt").val(convertnulltozero(sales_amt));
                
          }
           
        });
    </script>
        @endsection


@stop
