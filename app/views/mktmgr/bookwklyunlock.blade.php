@extends('layout.dashboardbookkeepermarket')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php 
   $d=strtotime("last Saturday"); $lastsaturday = date("Y-m-d", $d);
   
   ?>
<div class="container">
   {{-- <div class="flash-message" id="recapmsg">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      </p>
      @endif
      @endforeach
   </div> --}}
   <!-- end .flash-message -->
   <div id="yesnomsg">
      <p class="alert alert-info">This process will unlock the files, do you want to continue 
         <a href="#" id="yescallrecapdate" style="color:red;">Yes</a><a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
      </p>
   </div>
   <div id="datetounlock" style="margin-top:5px;" class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-2">
      <!--   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-2">   -->                  
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >RECAP REPORT</div>
         </div>
         <div class="panel-body" style="padding-top: 20px;">
            <form action="{{URL::route('mktmgr-unlocakwekly')}}" method="post" class="form-horizontal" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-7" style="text-align: right;">Please enter the Date of the Information</label>
                  <div class="col-sm-5">
                     <input type="date" class="form-control lastdate" id="dateofinfo" placeholder="" name="dateofinfo" value ="<?php echo $lastsaturday; ?>">
                     @if($errors->has('dateofinfo'))
                     {{ $errors->first('dateofinfo')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>        
<script type="text/javascript">
   $(document).ready(function(){ 
       $("#datetounlock").hide(); //$("#pwdform").hide();
       $("#yescallrecapdate").click(function(){
           $("#yesnomsg").hide();
           $("#datetounlock").show();
           return false;
       });
   });
</script>
@stop