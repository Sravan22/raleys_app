<?php 
    //echo '<pre>';print_r($safe_bal);exit;
   //echo count($reg_array_final1[0]);exit;
   //echo '<pre>',print_r($reg_array_final1);exit();
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
         {{--  <a href="" target="_blank">
         <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
         </a> --}}
         </span>
      </div>
   </div>
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="panel">
         <div class="" style="margin-left: 78px;margin-bottom:-23px;font-weight:bold;">
            <div class="panel-title">{{-- {{  date("m/d/Y", strtotime($txDate)) }} --}}</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Raley's / Bel Air</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">SAFE REPORT</div>
         </div>
         <div class="col-md-4"></div>
         <div class="col-md-4" style=" text-align:center; font-weight:bold;">
            <div class="panel-title"> Market Number : {{ $str_num }} &nbsp;&nbsp;&nbsp;Drug Number : {{ $drug_num }} &nbsp;&nbsp;&nbsp; DATE : {{ date('m/d/Y',strtotime($rpt_date)) }}</div>
         </div>
         <div class="col-md-4"></div>

         {{-- @if($reg_array_final1) --}}
         <div style="padding-top:30px" class="panel-body">
            <div class="row">
              <div class="col-md-4" style="margin-right:4px;">&nbsp;</div>
              <div class="col-md-4">
                <table class="table table-bordered table-colored" style="width: 0%">
               <thead>
                  <tr>
                     <th colspan="2" class="text-center">CURRENCY SAFE</th>
                  </tr> 
                    
               </thead>
               <tbody>
                   <tr>
                     <td><strong>BEGINNING SAFE</strong></td>
                     <td class="text-right">{{ number_format((float)$safe_bal[0]['begin_safe']/100, 2, '.', ''); }}</td>
                   </tr>
                   @foreach($rpt_safe_array as $row)
                    <tr>
                     <td><strong>{{ $row['item_desc'] }}</strong></td>
                     <td class="text-right">{{ number_format((float)$row['item_amt']/100, 2, '.', ''); }}</td>
                   </tr>
                   @endforeach
                   <tr>
                     <td><strong>TOTAL TO ACCOUNT FOR</strong></td>
                     <td class="text-right">{{ number_format((float)$safe_bal[0]['tot_acct']/100, 2, '.', ''); }}</td>
                   </tr>
                   <tr>
                     <td><strong>SAFE COUNT TOTAL</strong></td>
                     <td class="text-right">{{ number_format((float)$safe_bal[0]['safe_cnt']/100, 2, '.', ''); }}</td>
                   </tr>
                   <tr>
                     <td><strong>SAFE OVER <<span>Short</span>></strong></td>
                     <td class="text-right">{{ number_format((float)$safe_bal[0]['safe_os']/100, 2, '.', ''); }}</td>
                   </tr>
                   <tr>
                     <td><strong>GROCERY OVER <<span>Short</span>></strong></td>
                     <td class="text-right">{{ number_format((float)$safe_bal[0]['groc_os']/100, 2, '.', ''); }}</td>
                   </tr>
                   <tr>
                     <td><strong>DRUG OVER <<span>Short</span>></strong></td>
                     <td class="text-right">{{ number_format((float)$safe_bal[0]['drug_os']/100, 2, '.', ''); }}</td>
                   </tr>
                   <tr>
                     <td><strong>STORE TOTAL OVER <<span>Short</span>></strong></td>
                     <td class="text-right">{{ number_format((float)$safe_bal[0]['total']/100, 2, '.', ''); }}</td>
                   </tr>
                </tbody>
            </table>
              </div>
              <div class="col-md-4">&nbsp;</div>
            </div>

             <h4 class="text-center"><b>Weekly Comments</b></h4>
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">DATE</th>
                        <th class="text-center">COMMENT</th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                     for($i=0;$i<count($out_comments);$i++)
                     {
                  ?>
                     <tr>
                        <td class="text-center">{{ date('m/d/Y',strtotime($out_comments[$i]['date_stamp'])) }}</td>
                        <td class="text-center"> {{ $out_comments[$i]['comment_line'] }}</td>
                     </tr>
                     <?php 
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
          
        {{--  @endif    --}}


        
      </div>
   </div>
</div>
@stop