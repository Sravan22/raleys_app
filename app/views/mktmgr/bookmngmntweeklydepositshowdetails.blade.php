        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >{{ $title}} - {{date('m/d/Y',strtotime($date_stamp))}}</div>
            </div> 
            <div style="padding-top:10px" class="panel-body">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Day</th>
                        <th>Date</th>
                        <th>ID</th>
                        <th>Description</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($result as $rs)
                      <tr>
                        <td>{{ $rs->wkday }}</td>
                        <td>{{ $rs->recap_date }}</td>
                        <td>{{ $rs->item_id }}</td>
                        <td>{{ $rs->desc }}</td>
                        <td><?php echo number_format($rs->amt,2); ?></td>
                      </tr>
                    @endforeach
                    <tr><td colspan="5">&nbsp;</td></tr>
                    <tr><td colspan="3">&nbsp;</td><td>Total</td><td><?php echo number_format($total,2); ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-group" style="margin-top: 20px;">
            <div class="row">
                <div class="col-sm-12" align="center">
                    <input type="button" name="cancel" value="Cancel" class="btn" onclick="clickCancel()">
                </div>
            </div>
        </div>
        