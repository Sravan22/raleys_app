
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
</header>



<div class="container">

   <!-- Start flash-message -->
   <div class="flash-message" id="recapmsg" style="margin-top:55px;margin-left: 26%;width: 48%;">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
        <!--  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
      </p>
      @endif
      @endforeach
   </div>
  
   
      
   </div>


<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>
<script type="text/javascript">

    jQuery(document).ready(function($) {
        $("#cancelrecap").click(function(){

        msg='<p class="alert alert-danger">Recap calculation aborted at user request.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></p>';
           $("#recapmsg").html(msg);
         
            setTimeout(function() {document.location.href='wklyrecap' }, 1600);
        });
        $('#continue').click(function(event) {
            var calcdate = '{{ $calcdate }}';
            
              msg='<p class="alert alert-success">Processing weekly recap ...<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></p>';
             $("#recapmsg").html('');
             $("#recapmsg").html(msg);
           
           $.post("runprocessweekly", {calcdate: calcdate}, function(result){
          
             $("#recapmsg").html('');
             $("#recapmsg").html(result);
             setTimeout(function() {document.location.href='wklyrecap' }, 1600);
           });
         });
    });
</script>
@stop