@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.storeordersmenu')
    </header>
<div class="container" style="padding-top:10px;">
<table class="table table-striped">
    <thead>
      <tr>
        <th>Description</th>
        <th>Ad Retail</th>
        <th>Retail</th>
        <th>Sold</th>
        <th>Cases</th>
        <th>PO Qty</th>
        <th>Ord Qty</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="7" style="text-align:center;">No data found!</td>
      </tr>
    </tbody>
  </table>
</div>
@stop
