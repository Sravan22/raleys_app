<div class="container">
   <div class="menu_search_line">
      <ul class="navbar dib_float_left">
         <!-- <li id="main-home-link" class="menu_choice home_menu_choice active">
            Bookkeeper
            </li> -->
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">Reg<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Begin-Loans</a>
                  <ul class="dropdown-menu">
                     <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Register (Alt+a)</a></li>
                     <li><a href="{{URL::route('mktmgr-bookkeeperregistertotal')}}">Totals (Alt+b)</a></li>
                     <li><a href="{{URL::route('mktmgr-commentlist')}}">Comments (Alt+c)</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Loans-to-Reg</a>
                  <ul class="dropdown-menu">
                     {{-- <li><a href="{{URL::route('mktmgr-bookloanrregisterreg')}}">Register (Alt+d)</a></li>
                     <li><a href="{{URL::route('mktmgr-bookloanrregtotal')}}">Totals (Alt+e)</a></li> --}}
                     {{-- <li><a href="{{URL::route('mktmgr-loantoregregister')}}">Register (Alt+d)</a></li> --}}
                     <li><a href="{{URL::route('mktmgr-loantoregregister')}}">Register (Alt+d)</a></li>
                     <li><a href="{{URL::route('mktmgr-loantoregtotal')}}">Totals (Alt+e)</a></li>
                     <li><a href="{{URL::route('mktmgr-commentlist')}}">Comments (Alt+c)</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Reg-Check-Out</a>
                  <ul class="dropdown-menu">
                     {{-- <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Item (Alt+f)</a></li>
                     <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Register (Alt+g)</a></li>
                     <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Total (Alt+h)</a></li> --}}
                     <li><a href="{{URL::route('mktmgr-regcheckoutitem')}}">Item (Alt+f)</a></li>
                     <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Register (Alt+g)</a></li>
                     <li><a href="{{URL::route('mktmgr-regcheckouttotl')}}">Total (Alt+h)</a></li>
                    <li><a href="{{URL::route('mktmgr-regcheckoutprint')}}">Print</a></li>
                     <li><a href="{{URL::route('mktmgr-commentlist')}}">Comment (Alt+c)</a></li>
                  </ul>
               </li>
               <li><a href="{{URL::route('mktmgr-commentlist')}}">Comments (Alt+c)</a></li>
            </ul>
         </div>


         







         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
            Safe <span class="caret mainheadlink"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Total-Safe-Count</a>
                  <ul class="dropdown-menu">
                     {{-- <li><a href="{{URL::route('mktmgr-booksafecnt')}}">Old Detail (Alt+j)</a></li> --}}
                     <li><a href="{{URL::route('bookspro-safecount')}}">Detail (Alt+j)</a></li>
                     <li><a href="{{URL::route('bookspro-safecountprintdate')}}">Print</a></li> 
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Safe-Report</a>
                  <ul class="dropdown-menu">
                     {{-- <li><a href="{{URL::route('mktmgr-booksafereport')}}"> Old Safe-Report (Alt+k)</a></li> --}}
                     <li><a href="{{URL::route('bookspro-safereportdate')}}"> Safe-Report (Alt+k)</a></li>
                     <li><a href="{{URL::route('mktmgr-commentlist')}}">Comments (Alt+c)</a></li>
                     <li><a href="{{URL::route('bookspro-safereportprintdate')}}">Print</a></li>
                  </ul>
               
               </li>
               <li><a href="{{URL::route('mktmgr-commentlist')}}">Comments (Alt+c)</a></li>
            </ul>
         </div>
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
            Wkly-recap <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               {{-- <li><a href="{{URL::route('mktmgr-bookwklysales')}}">Old Sales (Alt+m)</a></li> --}}
               <li><a href="{{URL::route('bookspro_weeklysalesdate')}}"> Sales (Alt+m)</a></li>
               {{-- <li><a href="{{URL::route('mktmgr-bookwklyrecap')}}">Old Recap (Alt+n)</a></li> --}}
                <li><a href="{{URL::route('mktmgr-wklyrecap')}}"> Recap (Alt+n)</a></li>
               <li><a href="{{URL::route('mktmgr-bookwklyview')}}">View (Alt+o)</a></li>
                <li><a href="{{URL::route('bookspro-weeklysalesprint')}}">Print</a></li> 
               <li><a href="{{URL::route('mktmgr-bookwklyunlock')}}">Unlock (Alt+p)</a></li>
               <li><a href="{{URL::route('mktmgr-commentlist')}}">Comments (Alt+c)</a></li>
            </ul>
         </div>
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
            Inv <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Postage</a>
                  <ul class="dropdown-menu">
                     <li><a href="{{URL::route('mktmgr-bookpostageadd')}}">Add (Alt+r)</a></li>
                      <li><a href="{{URL::route('mktmgr-bookpostageprnt')}}">Print (Alt+s)</a></li> 
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Lottery</a>
                  <ul class="dropdown-menu">
                     <li><a href="{{URL::route('mktmgr-booklotteryadd')}}">Add (Alt+t)</a></li>
                     <li><a href="{{URL::route('mktmgr-booklotterytotl')}}">Totals (Alt+u)</a></li>
                     <!--   <li><a href="{{URL::route('mktmgr-booklotteryprnt')}}">Print</a></li> -->
                     <li><a href="{{URL::route('mktmgr-booklotteryview')}}">View (Alt+v)</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Commuter</a>
                  <ul class="dropdown-menu">
                     <li><a href="{{URL::route('mktmgr-bookcommuteradd')}}">Add (Alt+w)</a></li>
                     <li><a href="{{URL::route('mktmgr-bookcommuterprnt')}}">Print</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Mthly-Commuter</a>
                  <ul class="dropdown-menu">
                     <li><a href="{{URL::route('mktmgr-bookcommutermonthlyadd')}}">Add (Alt+x)</a></li>
                       <li><a href="{{URL::route('mktmgr-bookcommutermonthlyprnt')}}">Print</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">pHone</a>
                  <ul class="dropdown-menu">
                     <li><a href="{{URL::route('mktmgr-bookphonecardadd')}}">Add (Alt+y)</a></li>
                     <li><a href="{{URL::route('mktmgr-bookphonecardprnt')}}">Print</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">epS</a>
                  <ul class="dropdown-menu">
                     <li><a href="{{URL::route('mktmgr-bookepsadd')}}">Add (Ctrl+a)</a></li>
                     <li><a href="{{URL::route('mktmgr-bookepsprint')}}">Print</a></li>
                  </ul>
               </li>
               <li><a href="{{URL::route('mktmgr-commentlist')}}">Comments (Alt+c)</a></li>
            </ul>
         </div>
         
         {{-- <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
            Fuel <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="#">N/A for store 305 (Ctrl+b)</a></li>
            </ul>
         </div> --}}

            <?php  $storeType = Session::get('storeType');
           // echo $storeType;exit;
             ?>
             @if($storeType == 'FUEL')
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">Fuel<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Delivery</a>
                  <ul class="dropdown-menu">
                     <li><a href="{{ URL::route('bookspro-deliveryadd_update') }}">Add/Update</a></li>
                     <li><a href="{{ URL::route('bookspro-deliverylist') }}">List</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Sales</a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ URL::route('bookspro-salesdate') }}">Add/Update</a></li>
                     <li><a href="{{ URL::route('bookspro-saleslist') }}">List</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Ending-Inventory</a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ URL::route('bookspro-endinvdate') }}">Add/Update</a></li>
                     <li><a href="{{ URL::route('bookspro-endinginvlist') }}">List</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Reports</a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ URL::route('bookspro-dailyreportdate') }}">Daily</a></li>
                     <li><a href="{{ URL::route('bookspro-fuel_weekly_report_date') }}">Weekly</a></li>
                     <li><a href="">Daily-print</a></li>
                     <li><a href="">Weekly-print</a></li>
                  </ul>
               </li>
            </ul>
         </div>
         @endif
         <?php $username = Auth::user()->username; //echo $username; //exit;
         if ($username === 'mktmgr') { ?>
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
            Mgmt <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              {{--  <li><a href="{{URL::route('mktmgr-bookmngmntdaily')}}">Old Daily (Ctrl+c)</a></li> --}}
               <li><a href="{{URL::route('bookspro-mgmtdailydate')}}"> Daily (Ctrl+c)</a></li>
               {{-- <li><a href="{{URL::route('mktmgr-bookmngmntweekly')}}">Weekly (Ctrl+d)</a></li> --}}
               <li><a href="{{URL::route('bookspro-mgmtweeklydate')}}">Weekly (Ctrl+d)</a></li>
               <li><a href="{{URL::route('mktmgr-commentlist')}}">Comments (Alt+c)</a></li>
            </ul>
         </div>
         <?php } ?>
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
            cHg-regs <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="{{URL::route('mktmgr-bookchgquery')}}">Query (Ctrl+e)</a></li>
               <li><a href="{{URL::route('mktmgr-bookchgadd')}}">Insert (Ctrl+f)</a></li>
               <!--  <li><a href="{{URL::route('mktmgr-bookchgupdate')}}">Update</a></li>
                  <li><a href="{{URL::route('mktmgr-bookchgdelete')}}">Delete</a></li> -->
               <!-- <li><a href="">Update</a></li>
                  <li><a href="">Delete</a></li> -->
            </ul>
         </div>
         <div class="dropdown sub">
            <a id="dLabel" href="{{ URL::route('mktmgr-bookutilitypymntcnt')}}" role="button" 
               class="menu_choice" data-target="#">
            Utility (Ctrl+g)
            </a>
         </div>
         {{--   
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
            Comments <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="{{URL::route('mktmgr-bookcommentlist')}}">Comment List (Alt+c)</a></li>
               <li><a href="{{URL::route('mktmgr-commentlist')}}">Comment List (Alt+c)</a></li>
               --}}
               {{-- 
               <li><a href="{{URL::route('mktmgr-bookcommentadd')}}">Add Comment (Alt+i)</a></li>
               <li><a href="{{URL::route('mktmgr-commentadd')}}">Add Comment (Alt+i)</a></li>
            </ul>
         </div>
         --}}
         <div class="dropdown sub">
            <a id="dLabel" href="{{ URL::route('mktmgr-commentlist')}}" role="button" 
               class="menu_choice" data-target="#">
            Comments (Alt+c)
            </a>
         </div>
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
            Others <span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="#">N/A Supervisor only (Ctrl+h)</a></li>
            </ul>
         </div>
         <div class="dropdown sub">
             
             
             <?php $username = Auth::user()->username; //exit;
         if ($username === 'bkpfst' || $username === 'bkpsnd' || $username === 'bkpthd') { ?>
              <a id="dLabel" href="{{ URL::route('home')}}" role="button" 
               class="menu_choice" data-target="#">
        <?php } else { ?>
            <a id="dLabel" href="{{ URL::route('mktmgr-returnhome')}}" role="button" 
               class="menu_choice" data-target="#">
        <?php } ?>
                
            Exit (Alt+z)
            </a>
            <!-- <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="#">Warning Limits</a></li>
               </ul> -->
         </div>
      </ul>
   </div>
</div>