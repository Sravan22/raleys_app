    <div id="mainbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >Non Perishable Inv Prep <?php echo $data->sodinv_dtl['hdr_seq_number']; ?>: Item Scan <?php echo $data->sodinv_dtl['seq_number']; ?> Detail</div>
            </div> 
            
            <div style="padding-top:30px" class="panel-body" >

<form action="" class="form-horizontal" method="post" role="form" style="display: block;">
                
                <div class="form-group clear">
                        <label for="inputPassword" class="control-label col-sm-4">Inventory Number:</label>                        
                        <div class="control-label col-sm-3">
                           <?php echo $data->sodinv_dtl['hdr_seq_number']; ?>
                        </div>
                        
                        <label for="inputPassword" class="control-label col-sm-2">Item Is:</label>                        
                        <div class="control-label col-sm-3">
                            <?php echo $data->sodinv_dtl['itm_status']; ?>
                        </div>
                    </div>


                    <div class="form-group clear">
                        <label for="inputPassword" class="control-label col-sm-4">Scan Number:</label>                        
                        <div class="control-label col-sm-8">
                            <?php echo $data->sodinv_dtl['seq_number']; ?>
                        </div>
                    </div>

                    <div class="form-group clear">
                        <label for="inputPassword" class="control-label col-sm-4">UPC Number:</label>                        
                        <div class="control-label col-sm-8">
                            <?php echo $data->sodinv_dtl['upc_number']; ?>
                        </div>
                    </div>

    <div class="form-group clear" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-4">SKU Number:</label>                        
                        <div class="control-label col-sm-8">
                            <?php echo $data->sodinv_dtl['item_number']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-4">Item In GL Dept:</label>                        
                        <div class="control-label col-sm-8">
                            <?php echo $data->sodinv_dtl['gl_dept']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-4">Cost:</label>                        
                        <div class="control-label col-sm-8">
                           $<?php echo $data->sodinv_dtl['upc_cost']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-4">Retail:</label>                        
                        <div class="control-label col-sm-8">
                            $<?php echo $data->sodinv_dtl['rtl_amt']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-4">Scale ID:</label>                        
                        <div class="control-label col-sm-8">
                            <?php echo $data->sodinv_dtl['scale_id']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-4">Case Pack:</label>                        
                        <div class="control-label col-sm-8">
                           <?php echo $data->sodinv_dtl['case_pack']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-4">Container Size:</label>                        
                        <div class="control-label col-sm-8">
                            <?php echo $data->sodinv_dtl['cont_size']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-4">Item Description:</label>                        
                        <div class="control-label col-sm-8">
                            <?php echo $data->sodinv_dtl['item_desc']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-4">Quantity:</label>                        
                        <div class="control-label col-sm-8">
                            <?php echo $data->sodinv_dtl['quantity']; ?>
                        </div>
                    </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12" align="center">
                            <input type="button" name="cancel-submit" id="cancel-submit" onclick="clickCancel()" tabindex="4" value="Back" class="btn">                          
                        </div>
                    </div>
                </div>


</form>

            </div>
        </div>
    </div>