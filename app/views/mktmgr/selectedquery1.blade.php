@extends('layout.dashboard')
@section('page_heading','Receivings >> Printer Selection Menu')
@section('content')
@section('section')
<style type="text/css">
   .error{
   float: left;
   }
   .store{
   margin-left: -57px;
   }
   .span{
   padding-right: 133px;
   }
   .spanv{
   padding-right: 50px
   }
   .select2-container
   {
   width: 76%;
   float: left;
   }
</style>
<header class="row">
   @include('mktmgr.recivermenu')
</header>
@if(!empty($receivings))
@foreach ($receivings as $rcv)
<form class="form-horizontal" id="inputupdate" role="form" method="post" action="{{URL::route('mktmgr-post-updatequeryresult')}}">
   <div class="container" style="">
      <div class="flash-message" style="padding-bottom: 22px;">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Query</div>
            </div>
            <div style="padding-top:15px" class="panel-body" >
               <div class="row vertical-divider">
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Store</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="storeid" name="storeid" placeholder="Store ID" value="{{{ Session::get('storeid') }}}" readonly />
                              <input type="hidden" class="form-control" id="seq_number" name="seq_number" value="{{  $rcv->seq_number }}" readonly />
                           </div>
                        </div>
                        <div class="focusguard" id="focusguard-1" tabindex="1"></div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Department Name</label>
                           <div class="col-sm-8">
                              <select class="form-control keyfirst" id="dept_name_number" name="dept_name_number" value="dept_name_number" autofocus="" tabindex="2" >
                                 <option value="{{$rcv->retail_dept}}">{{$rcv->retail_dept}} : {{  $rcv->name }}</option>
                                 @for ($i = 0; $i < count($dept_list); $i++) 
                                 @if($rcv->retail_dept !== $dept_list[$i]->retail_dept)
                                 {
                                 <option value="{{$dept_list[$i]->retail_dept }}">{{$dept_list[$i]->retail_dept }} : {{ $dept_list[$i]->name }}</option>
                                 }
                                 @endif
                                 @endfor
                              </select>
                              <!--  <input type="text" class="form-control" id="DepartmentNumber" name="DepartmentNumber" 
                                 placeholder="Department Number" value="{{  $rcv->name }}" readonly /> -->
                           </div>
                        </div>
                        <!--- dept list -->
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Vendor</label>
                           <div class="col-sm-8">
                              <select class="form-control" id="vendor" name="vendor" value="vendor" disabled="true">
                                 <option value="" >{{ $rcv->vname }}</option>
                              </select>
                              @if($errors->has('vendor'))
                              {{ $errors->first('vendor')}}
                              @endif                 
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Number</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="vendornumber" name="vendornumber"{{ (Input::old('vendornumber'))?'
                              value="'.Input::old('vendornumber').'"':''}}  placeholder="Number" value ="{{ $rcv->vendor_number}}"readonly />
                              <!--  @if($errors->has('vendornumber'))
                                 {{ $errors->first('vendornumber')}}
                                 @endif  -->
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Transporter</label>
                           <div class="col-sm-8 transporter">
                              <input type="text" class="form-control" id="transporter" name="transporter" placeholder="" value ="{{ $rcv->vname2 }}" readonly>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Number</label>
                           <div class="col-sm-8">
                              <input type="number" class="form-control" id="number_2" name="number_2" value="{{  $rcv->trns_number }}"  
                                 placeholder="" readonly="readonly" />
                           </div>
                        </div>
                        <!-- <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Vendor</label>
                             <div class="col-sm-8">
                             <select class="form-control select2" id="vendor" name="vendor" value="vendor">
                                         <option value="">{{ $rcv->vname }}</option>
                                          </select>
                                          @if($errors->has('vendor'))
                                          {{ $errors->first('vendor')}}
                                          @endif                 
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="#" class="col-sm-4 control-label">Number</label>
                                       <div class="col-sm-8">
                                          <input type="text" class="form-control" id="vendornumber" name="vendornumber"{{ (Input::old('vendornumber'))?'
                                          value="'.Input::old('vendornumber').'"':''}}  placeholder="Number" readonly value="{{$rcv->vendor_number}}" />
                                          @if($errors->has('vendornumber'))
                                             {{ $errors->first('vendornumber')}}
                                             @endif
                                       </div>
                                    </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Invoice Date</label>
                           <div class="col-sm-8">
                              <input type="date" tabindex="3" class="form-control" id="invoice_date" name="invoice_date"  value="{{  $rcv->invoice_date }}"{{ (Input::old('invoice_date'))?'
                              value="'.Input::old('invoice_date').'"':''}}   placeholder="MM-DD-YY" <?php if($rcv->method_rcvd != 'O'){  echo 'readonly'; } ?>/>
                              @if($errors->has('invoice_date'))
                              {{ $errors->first('invoice_date')}}
                              @endif 
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Invoice Number</label>
                           <div class="col-sm-8">
                              <input type="text" tabindex="4" class="form-control" name="invoice_number"{{ (Input::old('invoice_number'))?' 
                              value="'.Input::old('invoice_number').'"':''}} 
                              id="invoice_number" placeholder="Invoice Number" value="{{$rcv->id }}" />
                              @if($errors->has('invoice_number'))
                              {{ $errors->first('invoice_number')}}
                              @endif 
                           </div>
                        </div>
                        <!-- <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Invoice Type</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="invoice_type" name="invoice_type" value="<?php 
                              if($rcv->type_code == 'D') echo 'D - Delivery';
                               elseif($rcv->type_code == 'R') { echo 'R - Return'; } 
                               else { echo 'S - Store Supplies'; }
                              ?>" readonly/>
                           </div>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Invoice Type</label>
                           <div class="col-sm-8">
                              <select class="form-control " id="invoice_type" name="invoice_type" tabindex="5">
                                 <option <?php if($rcv->type_code == 'D') echo 'selected'; ?> value="D">D - Delivery</option>
                                 <option <?php if($rcv->type_code == 'R') echo 'selected'; ?> value="R">R - Return</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Status</label>
                           <div class="col-sm-8">
                              <select class="form-control " id="status" name="status" tabindex="6">
                                 <?php if($rcv->method_rcvd == 'D' || $rcv->method_rcvd == 'N') 
                                    {
                                    ?>
                                 <option <?php if($rcv->status_code == 'A') echo 'selected'; ?> value="A">A - Accept</option>
                                 <option <?php if($rcv->status_code == 'R') echo 'selected'; ?> value="R">R - Reviewed</option>
                                 <option <?php if($rcv->status_code == 'V') echo 'selected'; ?> value="V">V - Voided</option>
                                 <?php   
                                    }
                                    elseif ($rcv->method_rcvd == 'O' || $rcv->method_rcvd == 'E' ) {
                                     ?>
                                 <option <?php if($rcv->status_code == 'O') echo 'selected'; ?> value="O">O - Open</option>
                                 <option <?php if($rcv->status_code == 'V') echo 'selected'; ?> value="V">V - Voided</option>
                                 <?php
                                    }
                                    elseif($rcv->method_rcvd == 'R')
                                    {
                                    ?>
                                 <option <?php if($rcv->status_code == 'O') echo 'selected'; ?> value="O">O - Open</option>
                                 <option <?php if($rcv->status_code == 'I') echo 'selected'; ?> value="I">I - Incompleted</option>
                                 <option <?php if($rcv->status_code == 'V') echo 'selected'; ?> value="V">V - Voided</option>
                                 <?php
                                    }
                                    ?>
                              </select>
                           </div>
                        </div>
                        <!-- <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Status</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="status" name="status" value="<?php 
                              if($rcv->status_code == 'O') echo 'O - Open';
                               elseif($rcv->status_code == 'A') { echo 'A - Accept'; }
                                elseif($rcv->status_code == 'I') { echo 'I - Incompleted'; } elseif($rcv->status_code == 'R') { echo 'R - Reviewed'; } elseif($rcv->status_code == 'V') { echo 'V - Voided'; } 
                                else { echo 'U - Unknown'; }
                              ?>" readonly/>
                              <select class="form-control" id="status" name="status" placeholder="Status" >
                                 <option value="A">A</option>
                                 <option value="B">B</option>
                                 <option value="I">I</option>
                                 <option value="O" selected="">O - Open</option>
                                 <option value="P">P</option>
                                 <option value="R">R</option>
                                 <option value="U">U</option>
                                 <option value="V">V</option>
                                 </select>
                           </div>
                           </div> -->
                     </div>
                     <div class="form-horizontal">
                        <!-- <div class="col-lg-6"> -->
                        <!-- Transporter -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Cost Discrepancies</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="cost_discrepancies" name="cost_discrepancies" value="{{  $rcv->cost_discrep_sw }}" readonly/>
                              <!-- <select class="form-control" id="cost_discrepancies" name="cost_discrepancies" 
                                 value="'.Input::old('cost_discrepancies').'"':''}}   placeholder="Cost Discrepancies" >
                                                 <option value="N">N</option>
                                                 <option value="Y">Y</option>
                                             </select> -->
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Method Received</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="method_received" name="method_received" value="<?php 
                                 if($rcv->method_rcvd == 'D') echo 'D - Dex';
                                 elseif($rcv->method_rcvd == 'R') { echo 'R - Receiver Scan'; } elseif($rcv->method_rcvd == 'O') { echo 'O - Offline'; }
                                 elseif($rcv->method_rcvd == 'N') echo 'NEX - EDI'; 
                                 else { 'UNKNOWN'; }
                                 ?>"  readonly="readonly" />
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <div class="col-xs-6">
                     <div class="form-horizontal " >
                        <div class="form-group" style="padding-top: 15px;">
                           <label for="#" class="col-sm-7 control-label" style="padding-left: 195px">-------Vendor-------</label>
                           <label for="#" class="col-sm-5 control-label" style="">--------Store--------</label> 
                        </div>
                        <div class="form-group" style="padding-top: 30px">
                           <label for="#" class="col-sm-4 control-label">Qty</label>
                           <div class="col-sm-4">
                              <input type="text"  class="form-control dec select" id="tot_vend_qty"  name="tot_vend_qty" value="<?php if($rcv->tot_vend_qty){echo $rcv->tot_vend_qty;} else{echo '0.00';}?>"  />
                              <!--  <b><span class="spanv"  name="tot_vend_qty">{{$rcv->tot_vend_qty }}</span></b> -->
                           </div>
                           <div class="col-sm-4">
                              <b><span class="span store_qty">{{$rcv->tot_vend_qty }}</span></b>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label txt">Store Inventory</label>
                           <div class="col-sm-4">
                              <!-- <b><span class=" spanv" name="tot_vend_cost">{{  number_format($rcv->tot_vend_cost, 2, '.', '')  }}</span></b> -->
                              <input type="text" class="form-control select dec vendor txt" id="tot_vend_cost" name="tot_vend_cost" placeholder="$0.00"
                                 value="{{  number_format($rcv->tot_vend_cost, 2, '.', '')  }}" />
                              @if($errors->has('tot_vend_cost'))
                              {{ $errors->first('tot_vend_cost')}}
                              @endif 
                           </div>
                           <div  class="col-sm-4">
                              <b><span  class="span tot_vend_cost_store" >{{  number_format($rcv->tot_vend_cost - $rcv->tot_vend_crv, 2, '.', '') }}</span></b>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">CRV</label>
                           <div class="col-sm-4">
                              <!-- <input type="text" class="form-control" id="crv" 
                                 name="crv" value="{{  number_format($rcv->tot_vend_crv, 2, '.', '')  }}" placeholder="$0.00" /> -->
                           </div>
                           <div  class="col-sm-4">
                              <?php 
                                 if($ws_crv_switch == 'Y'){?> 
                              <input type="text"  class="form-control store select txt" id="tot_vend_crv" name="tot_vend_crv" value="{{ number_format($rcv->tot_vend_crv, 2, '.', '')  }} " onKeyPress="return StopNonNumeric(this,event)"/> 
                              <?php }
                                 else{
                                   ?>
                              <input type="hidden" name="tot_vend_crv" value="{{ number_format($rcv->tot_vend_crv, 2, '.', '')  }} "/>
                              <b><span  class="span" name="tot_vend_crv">{{  number_format($rcv->tot_vend_crv, 2, '.', '')  }}</span></b>
                              <?php
                                 }
                                 ?>
                              @if($errors->has('tot_vend_crv'))
                              {{ $errors->first('tot_vend_crv')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Allowance</label>
                           <div class="col-sm-4">
                              <!-- <input type="text" class="form-control" id="allowances" 
                                 name="allowances" value="{{  number_format($rcv->tot_vend_allow, 2, '.', '')  }}" placeholder="$0.00" /> -->
                              @if($errors->has('allowances'))
                              {{ $errors->first('allowances')}}
                              @endif 
                           </div>
                           <div  class="col-sm-4">
                              <!-- <input type="number"  class="form-control store select" id="tot_store_allow" name="tot_store_allow" value="" readonly /> -->
                              <b><span  class="span tot_vend_allow">{{  number_format($rcv->tot_vend_allow, 2, '.', '')  }}</span></b>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Selling Supplies</label>
                           <div class="col-sm-4">
                              <input type="text" class="form-control dec select vendor txt" id="tot_sell_supply" 
                                 name="tot_sell_supply" value="{{  number_format($rcv->tot_sell_supply, 2, '.', '')}}" />
                              <!--   <b><span class="spanv" name="tot_sell_supply" >{{  number_format($rcv->tot_sell_supply, 2, '.', '')}}</span></b> -->
                           </div>
                           <div  class="col-sm-4">
                              <!-- <input type="number"  class="form-control store txt1" id="ws_tot_sell_supply" name="ws_tot_sell_supply" value="" readonly="" /> -->
                              <b><span class="span ws_tot_sell_supply">{{  number_format($rcv->tot_sell_supply, 2, '.', '')}}</span></b>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Store Supplies</label>
                           <div class="col-sm-4">
                              <input type="text" class="form-control dec select vendor txt" id="tot_store_supply" 
                                 name="tot_store_supply" value="{{  number_format($rcv->tot_store_supply, 2, '.', '') }}" />
                              <!--   <b><span class="spanv" name="tot_store_supply">{{  number_format($rcv->tot_store_supply, 2, '.', '') }}</span></b> -->
                           </div>
                           <div  class="col-sm-4">
                              <!-- <input type="number"  class="form-control store select txt1" id="ws_tot_store_supply" name="ws_tot_store_supply" value="" readonly/> -->
                              <b><span class="span ws_tot_store_supply">{{  number_format($rcv->tot_store_supply, 2, '.', '') }}</span></b>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Net</label>
                           <div class="col-sm-4">
                              <!-- <input type="number" class="form-control sum" id="ws_vendor_net" name="ws_vendor_net"
                                 value="{{  number_format($rcv->tot_vend_cost+$rcv->tot_sell_supply+$rcv->tot_store_supply, 2, '.', '') }}" readonly/> -->
                              <b><span class="spanv ws_vendor_net" id="ws_vendor_net" name="ws_vendor_net" >{{  number_format($rcv->tot_vend_cost+$rcv->tot_sell_supply+$rcv->tot_store_supply, 2, '.', '') }}</span></b>
                           </div>
                           <div  class="col-sm-4">
                              <b><span class="span   ws_vendor_net"  name="ws_vendor_net" >{{  number_format($rcv->tot_vend_cost+$rcv->tot_sell_supply+$rcv->tot_store_supply, 2, '.', '') }}</span></b>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Created Date</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="create_datetime" name="create_datetime" placeholder="Created Date" 
                                 value="{{  date('m/d/Y G:i:s',strtotime(trim($rcv->create_datetime)));  }}"  readonly/>
                              <input type="hidden" name="created_date_to_update" value="{{  date('Y-m-d',strtotime(trim($rcv->create_datetime)));  }}">
                           </div>
                           <label for="#" class="col-sm-4 control-label">Updated Date</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="last_update" name="last_update" placeholder="Updated Date" 
                                 value="{{ date('m/d/Y G:i:s'); }}" readonly/>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">By</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="last_updated_by" name="last_updated_by" value="{{$rcv->last_updated_by}}" readonly="readonly" />
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-12 control-label" style="text-align: center;"> 
                           {{--  <u>{{ HTML::link(URL::route('/get-itemsbyid/'.$rcv->seq_number), 'Items List') }}</u> --}}
                           <u><a href="{{ url('/mktmgr/get-itemsbyid/'.$rcv->seq_number) }}">Items List</a></u></label>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-12 topspace" align="center">
                           <input type="submit" name="login-submit" id="submit" value="Update" class="btn" tabindex="7">
                           <input type="button" class="btn keylast" value="Back" id="back" onclick="history.go(-1);" tabindex="8" />
                           {{ Form::token()}}
                        </div>
                     </div>
                  </div>
                  <div class="focusguard" id="focusguard-2" tabindex="9"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>
@endforeach
@else
  <p class="alert alert-info">Data is Empty <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
@endif
<script type="text/javascript">
   $(function(){
   
   
     $('.dec').blur(function(event) {
        selfield = this.id;
        selfieldval=$("#"+selfield).val();
        $("#"+selfield).val((parseFloat(selfieldval/100)*100).toFixed(2));
     });   
   
   
    /*frontend validations*/
    $('#inputupdate').validate({
   
         rules:{
            dept_name_number:{
               required:true
            },
            vendor:{
               required:true
            },
            invoice_date:{
              required : true,
              date : true,
            
            },
            invoice_number:"required",
           
            tot_vend_qty :{
               required:true,
               min:0,
               max:6000,
               number:true
            }
         },
   
         messages:{
            dept_name_number: "Department Name is required",
            vendor: "Vendor Name is required",
            invoice_date:{
              required:"Invoice date required(mm/dd/yyyy)",
              date:"Date is invalid",
              max:"Invoice Date cannot be future date"
            },
            invoice_number:"Invoice Number is required",
            
            tot_vend_qty:{
                required: "Total Qty is required",
                min:"Total Qty is greater than {0}",
                max:"Total Qty is less than {0}",
                number:"Enter valid number"
            }
         }
      });
    
   
   
    $(".vendor").on("keydown keyup change", function() {
    calculateSum();
    });
   
    function calculateSum() {
    var sum = 0;
   
    $(".vendor").each(function() {
    if (!isNaN(this.value) && this.value.length != 0) {
      sum += parseFloat(this.value);   
    } 
    });
    sum = sum.toFixed(2);
    //$('#ws_vendor_net').text(sum);
    $('.ws_vendor_net').text(sum);
   
    }
   
    $('#tot_vend_qty').on("blur", function() {
      
     var store = $(this).val();
     $(".store_qty").text(store);
    });
   
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
   
    var day = dtToday.getDate();
    //alert(day);
    var year = dtToday.getFullYear();
    //alert(year);
   
    if(month < 10)
        month = '0' + month.toString();
   
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //var newdate = new Date(maxDate);
    //newdate.setDate(newdate.getDate() - 180);
    //alert(newdate);*/
    //var minDate = new Date(newdate);
   
    $('#invoice_date').attr('max', maxDate);
    
   });  
   
   jQuery(document).ready(function($) {
   
   $('.select').on("click",function (){
   $(this).select();
   });
   /*
   $('.store_qty').change(function() {
   alert('hii');
   $('#ws_store_net').val($(this).val());
   });
   if($('.store_qty').val() > 0 )
   */
   /*  $(".txt,#tot_vend_qty").blur(function(){  
   var sum = 0; 
   selfield = this.id;
   
   selfieldval=$("#"+selfield).val(); 
   $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
   })*/;
   
   $('#tot_vend_qty').on("blur", function() {
   
   var store = $(this).val();
   
   $(".store_qty").text(store);
   });
   
   $('#tot_vend_cost').on("blur", function() {
   
   var store = $(this).val();
   
   $(".tot_vend_cost_store").text(store);
   });
   
   $('#tot_vend_cost,#tot_vend_crv').on("blur", function() {
             var crv = 0 ;
             var diff = 0;
             var store = $('#tot_vend_cost').val();
             var crv = $('#tot_vend_crv').val() ? $('#tot_vend_crv').val() : 0.00;
             
             diff = store - crv;
             diff = diff.toFixed(2);
             $(".tot_vend_cost_store").text(diff);
    });  
   
   
   $('#tot_sell_supply').on("blur", function() {
   
   var store = $(this).val();
   
   $('.ws_tot_sell_supply').text(store);
   });
   
   
   $('#tot_store_supply').on("blur", function() {
   
   var store = $(this).val();
   
   $(".ws_tot_store_supply").text(store);
   });
   
   $(".txt,.txt1").on("keydown keyup change", function() {
   calculateSum();
   //   passtostore();
   //calculateSum1();
   
   });
   
   
    $("#dept_name_number").on('change',function (e) {
   e.preventDefault(); 
   
   $('#vendor').removeAttr('disabled');
   deptNumber = $("#dept_name_number").val();
   
   if(deptNumber != ''){
     $('#vendor option').remove();
   //$('#vendor').val() == '';
   $.post("vendorlist", 
   {deptnumber: deptNumber},
   function(result){ 
   
   var arrayvendorlist = result.split('+');
   
   $.each(arrayvendorlist, function (index, value) {
   if(value != '')
   {
   var vendor = value.split('_');
   
   $('#vendor').append($("<option></option>")
   .attr("value",vendor[1]+'_'+vendor[0])
   .text(vendor[0])); 
   }
   });
   
   
   });
   }
   
   });
   
     $("#vendor").on('change',function(e){ 
   e.preventDefault(); 
   vendorname_number = $("#vendor").val(); 
   //alert(vendorname_number);
   var vendo0r_numname = vendorname_number.split('_');
   // alert(vendo0r_numname[1]);
   $("#vendornumber").val(vendo0r_numname[0]);
   
   vendornumber = vendo0r_numname[0];
   //alert(vendornumber);
   
   
   if(vendornumber != ' '){
   
   
   $.post("transporter_list", 
   {vendornumber: vendornumber}, 
   function(result){ 
   // alert(result);
   var transporter_data = result.split('_');
   //alert(transporter_data);
   if(result == 'empty')
   {
   $("#transporter").attr('disabled', 'disabled').val('');
   $("#number_2").val('');
   //$("#transporter").removeAttr("name");
   //$("img").attr("name", "sravan");
   $(".showtransporter").show();
   $(".showtransporter").attr("name", "transporter");
   //$(".showtransporter").removeAttr("id");
   }
   else
   {
   var vendorname = transporter_data[0];
   var vendornumber = transporter_data[1]; 
   //alert(vendorname);
   
   var res = transporter_data[1].slice(0, 6);
   //alert(res);return false;
   $("#transporter").val(vendorname);
   //$(".showtransporter").removeAttr("name");
   
   $("#number_2").val(res);
   }
   
   
   });
   
   }
   
   
   });
   
   
   });
   
   
   function calculateSum() {
   var sum = 0;
   //iterate through each textboxes and add the values
   $(".txt").each(function() {
   //add only if the value is number
   if (!isNaN(this.value) && this.value.length != 0) {
   sum += parseFloat(this.value);   
   } 
   });
   // $("input.sum").val(sum.toFixed(2));
   sum = sum.toFixed(2);
   $('.sum').text(sum);
   }
   
   
   
   $(document).ready(function(){
   
   
   
   
   
   /*$("#dept_name_number").change(function (e) {
   e.preventDefault(); deptNumber = $("#dept_name_number").val(); //alert(deptNumber);
   if(deptNumber != '')
   {
   $.post("vendorlist", {deptnumber: deptNumber}, function(result){ //alert(deptNumber);
   var arrayvendorlist = result.split(',');
   $makedropdown ='<select class="form-control" id="vendorname" name="vendorname"><option value="">Select Vendor Name</option>';
   $.each(arrayvendorlist, function (index, value) {
   var vendor = value.split('_');
   $makedropdown +='<option value='+vendor[1]+'_'+vendor[0]+'>'+vendor[0]+'</option>';
   });
   
   $makedropdown +='</select>';
   
   $("#placevendorlist").html($makedropdown);
   });
   }
   
   });*/
   
   $("#transporter").change(function (e){
   e.preventDefault(); transporter_number = $("#transporter").val(); //alert(deptNumber);
   if(transporter_number != '')
   {
   $("#number_2").val(transporter_number); return true;
   }
   });
   
   $("#placevendorlist").change(function(e){ 
   e.preventDefault(); vendorname_number = $("#vendorname").val(); //alert(vendorname_number);
   var vendo0r_numname = vendorname_number.split('_');
   $("#vendornumber").val(vendo0r_numname[0]);
   });
   
   $("#qty_vendor").blur(function(){
   qty_vendor=$("#qty_vendor").val();
   $("#qty_store").val(qty_vendor); return true;
   });
   
   $("#store_inventory_vendor").blur(function(){
   store_inventory_vendor=parseFloat($("#store_inventory_vendor").val(),10).toFixed(2);
   $("#store_inventory_vendor").val(store_inventory_vendor); calnet(); return true;
   });
   
   $("#store_inventory_vendor1").blur(function(){
   
   store_inventory_vendor1=$("#store_inventory_vendor1").val(); calnet(); return true;
   });
   
   $("#store_inventory_vendor2").blur(function(){
   store_inventory_vendor2=$("#store_inventory_vendor2").val(); calnet(); return true;
   });
   
   
   $("#selling_supplies_vendor").blur(function(){
   selling_supplies_vendor=parseFloat($("#selling_supplies_vendor").val(),10).toFixed(2);
   $("#selling_supplies_vendor").val(selling_supplies_vendor); calnet(); return true;
   });
   
   $("#crv").blur(function(){
   crv=parseFloat($("#crv").val(),10).toFixed(2);
   $("#crv").val(crv); calnet(); return true;
   });
   
   $("#allowances").blur(function(){
   allowances=parseFloat($("#allowances").val(),10).toFixed(2);
   $("#allowances").val(allowances); calnet(); return true;
   });
   
   $("#store_supplies_vendor").blur(function(){
   store_supplies_vendor=parseFloat($("#store_supplies_vendor").val(),10).toFixed(2);
   $("#store_supplies_vendor").val(store_supplies_vendor); calnet(); return true;
   });
   
   });
   
   /* $(function() {
   //Initialize Select2 Elements
   $(".select2").select2();
   });*/
   
   function calnet() {
   store_inventory_vendor=$("#store_inventory_vendor").val();
   selling_supplies_vendor=$("#selling_supplies_vendor").val();
   store_supplies_vendor=$("#store_supplies_vendor").val();
   if(store_inventory_vendor == ''){store_inventory_vendor  = 0.00; }
   if(selling_supplies_vendor == ''){selling_supplies_vendor  = 0.00; }
   if(store_supplies_vendor == ''){store_supplies_vendor  = 0.00; }
   tot=parseFloat(store_inventory_vendor)+parseFloat(selling_supplies_vendor)+parseFloat(store_supplies_vendor);
   $("#net_vendor").val(tot.toFixed(2));
   return 1;
   }
   
        
   
   
</script>
@stop