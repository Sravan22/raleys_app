<?php 
  //echo $comment_date;exit;
?>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="" class="mainbox col-md-4 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >COMMENTS</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-post-updatecommentdata')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <input type="date" class="form-control" readonly="" name="date_stamp" value="{{ $comments[0]['date_stamp'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line1" placeholder="Enter Comment Line 1" maxlength="65" name="comment_line1" rows="3">{{ $comments[0]['comment_line1'] }}</textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line2" placeholder="Enter Comment Line 2" maxlength="65" name="comment_line2" rows="3">{{ $comments[0]['comment_line2'] }}</textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line3" placeholder="Enter Comment Line 3" maxlength="65" name="comment_line3" rows="3">{{ $comments[0]['comment_line3'] }}</textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line4" placeholder="Enter Comment Line 4" maxlength="65" name="comment_line4" rows="3">{{ $comments[0]['comment_line4'] }}</textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line5" placeholder="Enter Comment Line 5" maxlength="65" name="comment_line5" rows="3">{{ $comments[0]['comment_line5'] }}</textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line6" placeholder="Enter Comment Line 6" maxlength="65" name="comment_line6" rows="3">{{ $comments[0]['comment_line6'] }}</textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line7" placeholder="Enter Comment Line 7" maxlength="65" name="comment_line7" rows="3">{{ $comments[0]['comment_line7'] }}</textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line8" placeholder="Enter Comment Line 8" maxlength="65" name="comment_line8" rows="3">{{ $comments[0]['comment_line8'] }}</textarea>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-12 margin">
                     <textarea class="form-control" id="comment_line9" placeholder="Enter Comment Line 9" maxlength="65" name="comment_line9" rows="3">{{ $comments[0]['comment_line9'] }}</textarea>
                  </div>
               </div>

               
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-7" style="margin-left: 70px" align="center">
                        <input type="submit" name="store_submit" id="submit" tabindex="4" value="Update" class="btn">
                        <input type="button" value="Cancel" class="btn" onclick="history.go(-1);" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop
<style type="text/css">
  .margin{
        margin-left: 38px;
  }
  @media (min-width: 992px) {
.col-md-offset-3 {
    margin-left: 33% !important; 
}
}
</style>
<script type="text/javascript">
   function StopNonNumeric(el, evt)
   {
    //var r=e.which?e.which:event.keyCode;
    //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot (thanks ddlab)
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var dotPos = el.value.indexOf(".");
    if( dotPos>-1 && (number[1].length > 3)){
        return false;
    }
    return true;
   }
</script>