                  
        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >DEPOSIT DETAIL - {{ $date_stamp }}</div>
            </div> 

            <div style="padding-top:10px" class="panel-body">


                <form action="{{URL::route('mktmgr-post-bookmngmntweeklymgrok')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                <input type="hidden" name="recap_date" value="{{ $date_stamp }}">
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Currency Deposits</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('CURRENCY DEPOSITS')" name="CURRENCY DEPOSITS" tabindex="4" value="<?php echo number_format($wk_maindep_currency,2) ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                  <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Check Deposits</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('CHECK DEPOSITS')" name="CHECK DEPOSITS" tabindex="4" value="<?php echo number_format($wk_maindep_checks,2) ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">M/C - Visa Deposits</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('VISA DEPOSITS')" name="VISA DEPOSITS" tabindex="4" value="<?php echo number_format($wk_maindep_express_cred,2) ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Express Debit</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('EXPRESS DEBIT')" name="EXPRESS DEBIT" tabindex="4" value="<?php echo number_format($wk_maindep_express_deb,2) ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Discover Deposits</label>
                        <div class="col-sm-7">&nbsp;</div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">American Express</label>
                        <div class="col-sm-7">&nbsp;</div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">EBT Welfare</label>
                        <div class="col-sm-7">&nbsp;</div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Future Item 2</label>
                        <div class="col-sm-7">&nbsp;</div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">EBT WIC</label>
                        <div class="col-sm-7">&nbsp;</div>
                    </div>
                    
                    <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-5">Total</label>
                        <div class="col-sm-7">
                            <?php echo number_format($wk_maindep_total,2) ?>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Accept" class="btn">
                                <input type="button" name="cancel" value="Cancel" class="btn" onclick="clickCancel()">
                                {{ Form::token()}}
                            </div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    