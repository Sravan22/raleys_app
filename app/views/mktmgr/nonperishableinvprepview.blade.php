@extends('layout.dashboard')
@section('page_heading','Non Perishable Inventory Prep')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
</header>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <style>table a { text-decoration:underline; } </style>
   <div class="" style=" text-align:center; font-weight:bold; margin-bottom: 30px;">
      <div class="panel-title" >Non Perishable Inventory Prep Totals</div>
   </div>
   <div class="menu_search_line">
      <div class="dropdown sub">
         <a  href="{{ URL::route('mktmgr-returnhome')}}" >
         Exit
         </a>
      </div>
   </div>
</div>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-nonperishable',['download'=>'pdf'])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
<div class="container" style="padding-top:10px;">
   <form action="#" class="form-horizontal" method="post" role="form" style="display: block;">
      <table class="table table-striped">
         <thead>
            <tr>
               <th></th>
               <th></th>
               <th>Total</th>
               <th>Total</th>
               <th>Total</th>
               <th colspan="4">Current Inventories with a STATUS of</th>
               <th></th>
            </tr>
            <tr>
               <th>Dept Number</th>
               <th>Dept Desc</th>
               <th>Value at Cost</th>
               <th>Value at RT1</th>
               <th>CRV at RT1</th>
               <th>O</th>
               <th>C</th>
               <th>V</th>
               <th>A</th>
               <th>R</th>
               <th>S</th>
            </tr>
         </thead>
         <tbody>
            @foreach($data->dept_sodinv_rec as $key => $value)
            <tr>
               <td><a href="{{ URL::route('mktmgr-noninvdeptinfo',$value['dept_no']) }}">{{$value['dept_no']}}</a></td>
               <td>{{$value['desc']}}</td>
               <td>{{$value['tot_value_cst']}}</td>
               <td>${{$value['tot_value_rtl']}}</td>
               <td>${{$value['tot_crv_rtl']}}</td>
               <td>{{$value['num_of_invs_O']}}</td>
               <td>{{$value['num_of_invs_C']}}</td>
               <td>{{$value['num_of_invs_V']}}</td>
               <td>{{$value['num_of_invs_A']}}</td>
               <td>{{$value['num_of_invs_R']}}</td>
               <td>{{$value['num_of_invs_S']}}</td>
            </tr>
            @endforeach
         </tbody>
      </table>
   </form>
</div>
@stop
<style type="text/css">
   @media print {
   a[href]:after {
   content: "" !important; 
   }
   }
</style>