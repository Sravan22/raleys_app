@extends('layout.dashboard')

@section('page_heading','Non Perishable Inventory Prep')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
       
    </header>

<div class="container" style="padding-top:10px;">
         <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
<style>table a { text-decoration:underline; } </style>
  <div id="loginbox">
<div class="" style=" text-align:center; font-weight:bold; margin-bottom: 30px;">
                        <div class="panel-title" >Non Perishable Inventory Prep for Department - {{$data->dept_inp}}</div>
                       </div> 
      
                <?php 
                $deptno=$data->dept_inp;
                ?>
     
            <div class="row" style=" margin-bottom: 30px;">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <span class="pull-right">
                   <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-nonpar-department',['download'=>'pdf','deptno'=> $deptno])  }}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
                </span>
            </div>
          </div>

      

    <form action="{{URL::route('mktmgr-nonmoreinfoseldate')}}" class="form-horizontal" method="post" role="form" style="display: block;"> 
<table class="table table-striped">
    <thead>
      <tr>
       
        <th>Inv Date</th>
        <th>Inv Number</th>
        <th>Type</th>
        <th>Inv Area</th>
        <th>Sect</th>
        <th>Emp ID</th>
        <th>Tot Lns</th>
        <th>Value at Cost</th>
        <th>Value at RT1</th>
        <th>CRV at RT1</th>
        <th>Stat</th>
      </tr>
    </thead>
    <tbody>
         @foreach($data->sodinv_hdrs_rec as $key => $value)
      <tr>
        
        <td ><a href="javascript:void(0)" onclick="showdetails('{{$value['create_date']}}','{{$value['inventory_number']}}','{{$value['inventory_type']}}','{{$value['inventory_area']}}','{{$value['inventory_section']}}','{{$value['employee_id']}}','{{$value['total_scans']}}','{{$value['total_value_cst']}}','{{$value['total_value_rtl']}}','{{$value['total_crv_rtl']}}','{{$value['invhdr_status']}}','{{$data->sodinv_hdrs_non_disp_rec[$key]['inv_type_desc']}}','{{$data->sodinv_hdrs_non_disp_rec[$key]['hdr_status']}}')">{{date('m/d/Y',strtotime($value['create_date']))}}</a></td>
        <td><a href="{{URL::route('mktmgr-nonmoreinfoseldate',array('deptno'=>$data->dept_inp,'sodinvno'=>$value['inventory_number']))}}">{{$value['inventory_number']}}</a></td>
        <td>{{$value['inventory_type']}}</td>
        <td>{{$value['inventory_area']}}</td>
        <td>{{$value['inventory_section']}}</td>
        <td>{{$value['employee_id']}}</td>
        <td>{{$value['total_scans']}}</td>
        <td>${{$value['total_value_cst']}}</td>
        <td>${{$value['total_value_rtl']}}</td>
        <td>${{$value['total_crv_rtl']}}</td>
        <td>{{$value['invhdr_status']}}</td>
      </tr>
       @endforeach
      
    </tbody>
  </table>

  <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
            
            <input type="button" name="login-submit" id="submit" tabindex="4" onclick="window.location.href='{{URL::route('mktmgr-nonperishableinvprepview')}}'" value="Back" class="btn">
              {{ Form::token()}}
        </div>
        </div>
  </div>
 </form>
</div>
  

 
@section('jssection')
@parent
<script>

    parentHtml = $("#loginbox").html();
function showdetails(create_date,inventory_number,inventory_type,inventory_area,inventory_section,employee_id,total_scans,total_value_cst,total_value_rtl,total_crv_rtl,invhdr_status,inv_type_desc,hdr_status) {
    
    
    $.ajax({
        url:'{{URL::route('mktmgr-noninvshowdetail')}}',
        type:'GET',
        data: "create_date="+create_date+"&inventory_number="+inventory_number+"&inventory_type="+inventory_type+"&inventory_area="+inventory_area+"&inventory_section="+inventory_section+"&employee_id="+employee_id+"&total_scans="+total_scans+"&total_value_cst="+total_value_cst+"&total_value_rtl="+total_value_rtl+"&total_crv_rtl="+total_crv_rtl+"&invhdr_status="+invhdr_status+"&inv_type_desc="+inv_type_desc+"&hdr_status="+hdr_status,
        success:function(data){
            $("#loginbox").html(data);
        }
    });
}

function clickCancel() {
    $("#loginbox").html(parentHtml);
    //htmlStore = parentHtml;
}
function clickApprove(inventory_number) {
    
    var invhdr_status=$("#invhdr_status").val();
    var hdr_status=$("#hdr_status").val();
    
    $.ajax({
        url:'{{URL::route('mktmgr-noninvapproveinv')}}',
        type:'GET',
        dataType: "json",
        data: "inventory_number="+inventory_number+"&invhdr_status="+invhdr_status+"&hdr_status="+hdr_status,
        success:function(data){
            
            if(data.status){
                $(".hdrstatus").html(data.hdr_status);
                $("#invhdr_status").val(data.invhdr_status);
                $("#hdr_status").val(data.hdr_status);
                $(".errormsg").html(data.msg);            
                
            } else {
                $(".errormsg").html(data.msg);    
            }
        }
        });
}
function clickVoid(inventory_number) {
    
    var invhdr_status=$("#invhdr_status").val();
    var hdr_status=$("#hdr_status").val();
    
    $.ajax({
        url:'{{URL::route('mktmgr-noninvvoidinv')}}',
        type:'GET',
        dataType: "json",
        data: "inventory_number="+inventory_number+"&invhdr_status="+invhdr_status+"&hdr_status="+hdr_status,
        success:function(data){
            
            if(data.status){
                $(".hdrstatus").html(data.hdr_status);
                $("#invhdr_status").val(data.invhdr_status);
                $("#hdr_status").val(data.hdr_status);
                $(".errormsg").html(data.msg);                  
                
            } else {
                $(".errormsg").html(data.msg);    
            }
        }
        });
}
</script>
@endsection
@stop
<style type="text/css">
  

@media print {
  a[href]:after {
    content: "" !important; 
    
  }
}
</style>