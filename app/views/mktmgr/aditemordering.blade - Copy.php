@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
<div class="container">
   <div id="" class="mainbox col-md-5 col-md-offset-4 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">Weekly Ad Order for...</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('post-store-order-weekly')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label  col-sm-12" id="textright">Department</label>
                  </div>
                  <input type="hidden" name="store_number" value="{{{ Session::get('storeid') }}}">
                  <div class="col-sm-7">
                     <select id="dept_name_number" name="dept_name_number" class="form-control keyfirst" value="dept_name_number" autofocus="" tabindex="2">
                        <option value="">Select</option>
                        @for ($i = 0; $i < count($departments); $i++)
                        <option value="{{ $departments[$i]['gl_dept_number'] }}">{{ $departments[$i]['name'] }}</option>
                        @endfor
                     </select>
                     @if($errors->has('dept_name_number'))
            {{ $errors->first('dept_name_number')}}
            @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label  col-sm-12" id="textright">Order Type</label>
                  </div>
                  <div class="col-sm-7">
                     <div id="orderlist">
                        <select name="type_subtype_code" id="type_subtype_code" class="form-control" isImportant="true">
                  <option value="">Select Order type</option>
                </select>
                @if($errors->has('type_subtype_code'))
            {{ $errors->first('type_subtype_code')}}
            @endif
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12">
                        <input type="submit" style="margin-left: 49%" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/storeorderview')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
                <div class="focusguard" id="focusguard-2" tabindex="6"></div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop 
 
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>
<script type="text/javascript">
   $(document).ready(function(){
     //alert('Hiii');
     //$('select').on('change', function() {
      $('#dept_name_number').on('change', function(e) {
     //$("#dept_name_number").change(function (e) {
     e.preventDefault(); 
     deptNumber = $("#dept_name_number").val(); 
     //alert(deptNumber);
      if(deptNumber != '')
   {
   $.post("select_order_type", {deptnumber: deptNumber}, function(result){  //alert(result);
                 var arrayvendorlist = result.split(',');
                //alert(arrayvendorlist);//return false;
              $makedropdown ='<select class="form-control" id="type_subtype_code" name="type_subtype_code"><option value="">Select Order Type</option>';
              $.each(arrayvendorlist, function (index, value) {
                   var vendor = value.split('_');
                   //alert(vendor);//return false;
                   if(vendor.length==2)
                   {
                    $makedropdown +='<option value='+vendor[0]+'>'+vendor[1]+'</option>';
                   }
                   else
                   {
                    $makedropdown += '';
                   }
                  
                });
              
              $makedropdown +='</select>';
   
              $("#orderlist").html($makedropdown);
            });
   }
   
   
   
   });
   });
</script>