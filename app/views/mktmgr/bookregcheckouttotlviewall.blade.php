@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<style type="text/css">
    .makecenter
    {
        margin-left:20px;
    }
    .row.vertical-divider > div[class^="col-"]
    {
      text-align: left;
      padding: 2px;
    }
</style>
<form class="form-horizontal" role="form" method="post" action="#">
  <div class="container" style="">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
        <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >REGISTER CHECK OUT</div>
                       </div> 

                 <div style="padding-top:15px" class="panel-body" >
                  
<div class="row horizantal-divider">
  <div class="col-xs-6">    
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">DATE</label>
     <div class="col-sm-4" style="text-align: left;margin-left: -48px;">
     <input class="form-control col-sm-6" type="text" name="selecteddate" id="selecteddate" value="{{ date("m/d/Y", strtotime($dateofinfo)); }}" 
            readonly="readonly">
    </div>
  </div>
  </div>
 
 <!-- <div class="col-lg-12"> -->

  

 <div class="col-xs-6">
  <div class="form-horizontal">

        <div class="form-group">
          <label for="#" class="col-sm-2 control-label">CATEGORY</label>
          <div class="col-sm-2" style="padding-left:0px;">
            <input type="text" class="form-control col-sm-2"  name="selecteditemsid" id="selecteditemsid" value="{{ $itemid }}" readonly="readonly">
          </div>
           <div class="col-sm-8" style="text-align:right;margin-left: -48px;">  
           <input type="text" class="form-control col-sm-4" name="selecteditemsname" id="selecteditemsname" value="{{ $itemname }}" readonly="readonly">
          </div>
         </div>

    
  </div> 
 </div>
 <div class="col-xs-12"><hr></div>
 <?php $gt=0; $i=1;
      $amtarray = array('reg1'=>'00.00', 'reg2'=>'00.00', 'reg3'=>'00.00', 'reg4'=>'00.00', 'reg5'=>'00.00', 'reg6'=>'00.00', 'reg7'=>'00.00',
                        'reg8'=>'00.00', 'reg9'=>'00.00', 'reg12'=>'00.00', 'reg13'=>'00.00', 'reg14'=>'00.00', 'reg15'=>'00.00',
                        'reg17'=>'00.00', 'reg29'=>'00.00', 'reg30'=>'00.00');
 ?>
  @if(count($resultArray) != 0 )
   @foreach($resultArray as $row)
     <?php 
            $gt=$gt+$row['item_amt']; 
            /*if($i ==  $row['reg_num'])
            {*/  
              $val=number_format($row['item_amt'], 2, '.', '');
              $amtarray['reg'.$row['reg_num']]=$val;
            //}
          //$i=$i+1;
     ?>
  @endforeach
  @endif
    <div class="col-sm-4">
        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Rg/Chkr</label>
          <input class="form-control" id="rg1" name="rg1" type="text" readonly="readonly" value="1">
          <input class="form-control" id="rg4" name="rg4" type="text" readonly="readonly" value="4">
          <input class="form-control" id="rg7" name="rg7" type="text" readonly="readonly" value="7">
          <input class="form-control" id="rg12" name="rg12" type="text" readonly="readonly" value="12">
          <input class="form-control" id="rg15" name="rg15" type="text" readonly="readonly" value="15">
          <input class="form-control" id="rg30" name="rg30" type="text" readonly="readonly" value="30">
        </div>

        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Amount</label>
          <input class="form-control calall" id="amt1" name="amt1" type="text" step="0.01" tabindex="0"
             readonly=""    value={{ $amtarray['reg1'] }}>
          <input class="form-control calall" id="amt4" name="amt4" type="text" step="0.01" tabindex="2" readonly="" 
                 value={{ $amtarray['reg4']  }} >
          <input class="form-control calall" id="amt7" name="amt7" type="number" step="0.01" tabindex="3" readonly="" 
                  value={{ $amtarray['reg7']   }}  >
          <input class="form-control calall" id="amt12" name="amt12" type="number" step="0.01" tabindex="4" readonly="" 
                 value={{ $amtarray['reg12']  }} >
          <input class="form-control calall" id="amt15" name="amt15" type="number" step="0.01" tabindex="5" readonly=""  >
          <input class="form-control calall" id="amt30" name="amt30" type="number" step="0.01"  tabindex="6" readonly="" >
        </div>
    </div>
    <div class="col-sm-4">
          <div class="col-xs-6">
          <label for="ex1" class="makecenter">Rg/Chkr</label>
          <input class="form-control" id="rg2" name="rg2" type="text" readonly="readonly" value="2">
          <input class="form-control" id="rg5" name="rg5" type="text" readonly="readonly" value="5">
          <input class="form-control" id="rg8" name="rg8" type="text" readonly="readonly" value="8">
          <input class="form-control" id="rg13" name="rg13" type="text" readonly="readonly" value="13">
          <input class="form-control" id="rg17" name="rg17" type="text" readonly="readonly" value="17">
           <label for="ex1" class="makecenter">Grand Total</label>
        </div>

        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Amount</label>
          <input class="form-control calall" id="amt2" name="amt2" type="number" step="0.01" tabindex="7" readonly="" 
              value={{ $amtarray['reg2']  }}  >
          <input class="form-control calall" id="amt5" name="amt5" type="number" step="0.01" tabindex="8" readonly="" 
                value={{ $amtarray['reg5']  }}  >
          <input class="form-control calall" id="amt8" name="amt8" type="number" step="0.01" tabindex="9" readonly="" 
                 value={{ $amtarray['reg8']  }}  >
          <input class="form-control calall" id="amt13" name="amt13" type="number" step="0.01" tabindex="10" readonly="" 
                 value={{ $amtarray['reg13']  }}>
          <input class="form-control calall" id="amt17" name="amt17" type="number" step="0.01" tabindex="11" readonly=""  >
          <input type="number" class="form-control" id="gtot" name="gtot" placeholder="Grand Total" 
                 value="{{ $gt/100 }}" readonly="readonly">
        </div>
    </div>
   
    
    <div class="col-sm-4">
        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Rg/Chkr</label>
          <input class="form-control" id="rg3" name="rg3" type="text" readonly="readonly" value="3">
          <input class="form-control" id="rg6" name="rg6" type="number" readonly="readonly" value="6">
          <input class="form-control" id="rg9" name="rg9" type="text" readonly="readonly" value="9">
          <input class="form-control" id="rg14" name="rg14" type="text" readonly="readonly" value="14">
          <input class="form-control" id="rg29" name="rg29" type="text" readonly="readonly" value="29">
        </div>

        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Amount</label>
          <input class="form-control calall" id="amt3" name="amt3" type="number" step="0.01" tabindex="12" readonly="" 
                  value={{ $amtarray['reg3']  }}  >
          <input class="form-control calall" id="amt6" name="amt6" type="number" step="0.01" tabindex="12" readonly="" 
                  value={{ $amtarray['reg6']  }}  >
          <input class="form-control calall" id="amt9" name="amt9" type="number" step="0.01" tabindex="13" readonly="" 
                   value={{ $amtarray['reg9']  }}  >
          <input class="form-control calall" id="amt14" name="amt14" type="number" step="0.01" tabindex="14" readonly="" 
                   value={{ $amtarray['reg14']  }}  >
          <input class="form-control calall" id="amt29" name="amt29" type="number" step="0.01" tabindex="14" readonly="" >
        </div>
    </div>
  </div>
  
    </div>
  
  
          <div class="form-group">
                    <div class="row">
                       <div class="col-sm-12 topspace" align="center">
        <input type="submit" name="login-submit" id="submit" value="Accept" class="btn"   onclick="goBack()">
       <!-- <input type="button" name="login-submit" id="cancel" tabindex="4" value="Cancel" class="btn"  onclick="goBack()"> -->
                                                {{ Form::token()}}
                      </div>
                    </div>
                  </div>
  @if($dayIsLocked != 'Y')                
  @endif
  
</div>


 </div>
        </div>  </div>
      </div>
  </form>  
  <script>
function goBack() {
    window.history.back();
}
</script>  
@stop
