@extends('layout.dashboardbookkeepermarket')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); 
   ?>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >SAFE REPORT</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            {{-- <form action="{{URL::route('mktmgr-post-bookmngmntdaily')}}" class="form-horizontal" method="post" role="form" style="display: block;"> --}}
            <form action="{{URL::route('bookspro-post-mgmtdaily')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-7" style="text-align: right;">Please enter the date of the information</label>
                  <div class="col-sm-5">
                     <input type="date" class="form-control lastdate" id="safe_date" placeholder="" name="safe_date" value="{{ $previewsdate; }}">
                     @if($errors->has('safe_date'))
                     {{ $errors->first('safe_date')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 10px;">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop