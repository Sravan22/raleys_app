@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<div class="container">
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
    	  <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >LOAN REPORT</div>
                       </div> 
     <form action="{{URL::route('mktmgr-savebookloanreg')}}" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;">

    <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Register Number</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" id="regnum" name="regnum" readonly="readonly" value="{{ $reg_num }}">
            </div>
    </div>

    <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Register Date</label>
        <div class="col-sm-6">
           <input type="text" class="form-control" id="dateofinfo" name="dateofinfo" readonly="readonly" value="{{ $dateofinfo }}">
            </div>
    </div> 
    <?php $i=1; $gtot =0; ?>
     @foreach($alldata as $row)
        <?php 
              $dbval = '00.00'; //echo $row['allitems']; echo "<br />";
              if($row['allitems'] != 'BL' OR $row['allitems'] != 'LR')
              {

                 /*$dbval=number_format($row['item_amt']/100);
                 $dbval=number_format($dbval, 2, '.', '');*/
                 $dbval=number_format($row['item_amt']/100,2,'.',',');
                 //$dbval=number_format($row['item_amt'], 2);
                 $gtot=$gtot+$dbval;
                 $amtid = 'amt'.$i; $itemdescid = 'itemdesc'.$i; $i=$i+1;
        ?>
        <div class="form-group">
            <input type="hidden" id="{{ $itemdescid }}" name="{{ $itemdescid }}" value="{{ $row['allitems'] }}">
            <label for="inputEmail" class="control-label col-sm-6">({{ $row['allitems'] }}) {{ $row['item_desc'] }}</label>
        <div class="col-sm-6">
           <input type="number" class="form-control caltosum" id="{{ $amtid }}" name="{{ $amtid }}" value="{{ $dbval }}" step="0.01" placeholder="00.00">
            </div>
           </div> 
        <?php } ?>
     @endforeach  
     <?php $gtot=number_format($gtot, 2, '.', ''); ?>

     <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Loans To Registers Total</label>
        <div class="col-sm-6">
            <input type="number" class="form-control" id="gt" name="gt" readonly="readonly" step="0.01" value="{{ $gtot }}" placeholder="00.00">
            <input type="hidden" name="totfields" id="totfields" value="{{ $i}}">
            
            </div>
    </div>
    <div class="form-group">
            <div class="row">
            <div class="col-sm-12" align="center">
             
              <input type="button" style="width: 140px;margin-left: 75px;" value="Cancel" class="btn" id="cancel" name="cancel" onclick="goBack()">
              {{ Form::token()}}
            </div>
            </div>
        </div> 

    @if($approvalsOk == 'N')
    <div class="form-group">
	  <div class="row">
											 <div class="col-sm-12" align="center">
				<input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                <!-- <input type="submit" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn"> -->
        {{ Form::token()}}
											</div>
										</div>
									</div>
		@endif							
								</form>
								
							
					</div>
				</div>
			</div>
		</div>
<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>
 <script type="text/javascript">
 $(".setnumtot").blur(function(){ var sum = 0;
    amt=$("#"+this.id).val();
    $("#"+this.id).val(parseFloat(amt).toFixed(2));
    allsum=sumall();
    $("#gt").val(parseFloat(allsum,10).toFixed(2));
 });

$(".caltosum").blur(function(){  var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
       $('.caltosum').each(function() {
       if($(this).val()){ 
        sum += parseFloat($(this).val()); 
        }
        });
       $("#gt").val(parseFloat(sum).toFixed(2));
       return true;
    });

 function sumall()
 {
    sumallval = 0;
    tot=$("#tot").val();
    for(i=1; i< tot; i++)
    {   
        selval=$("#val"+i).val();
        if(selval != '') { sumallval = sumallval+parseInt($("#val"+i).val()); }
    }
    return sumallval;
       /* return tot=parseInt(misc)+parseInt(pennies)+parseInt(nickles)+parseInt(dimes)+parseInt(quarters)+parseInt(twenties)
         +parseInt(tens)+parseInt(fives)+parseInt(ones);*/
}
 </script>       
@stop