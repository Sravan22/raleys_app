@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>

<div class="container">
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >DEX Log by Date</div>
         </div>
         <div style="padding-top:10px" class="panel-body" >
            <form action="{{URL::route('mktmgr-post-DEX-invoicedate')}}" class="form-horizontal" method="post" role="form" id="register-form" style="display: block;">
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <label for="keydate" class="col-sm-6 control-label center" >Please key invoice date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control keyfirst" autofocus="" id="keydate" placeholder="MM-DD-YY" name="keydate" tabindex="2">
                    
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12 topspace" style="margin-left: 52%">
                        <input type="submit" name="submit" id="submit" tabindex="3" value="Search" class="btn">
                        <input type="reset" class=" btn keylast clearerrors" name="login-submit" id="btn" tabindex="4" value="Reset" >
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="5"></div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
<!-- jQuery Form Validation code -->
  <script>
  
  // When the browser is ready...
  $(function() {
  

    $('.clearerrors').on('click', function(e) {
   //alert('Hiii');
   $('.error').text('');
});
    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
            keydate: {
               required: true,
               date: true
            }
        },
        
        // Specify the validation error messages
          messages: {
            keydate: "Enter the invoice date"
          
        }
    });

  });
  
  </script>
@stop
