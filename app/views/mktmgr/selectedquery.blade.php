@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>

<style type="text/css">
   .select2-container
   {
   width: 76%;
   float: left;
   }
   .store{
   margin-left: -57px;
   }
   .span{
   padding-right: 133px;
   }
</style>
<?php $todays_date = date('Y-m-d'); ?>
@foreach ($receivings as $rcv)
<form class="form-horizontal" role="form" method="post" action="{{URL::route('mktmgr-post-updatequeryresult')}}">
   <div class="container" style="">
      <div class="flash-message">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Receiving</div>
            </div>
            <div style="padding-top:15px" class="panel-body" >
               <div class="row vertical-divider">
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Store</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="storeid" name="storeid" placeholder="Store ID" value="{{{ Session::get('storeid') }}}" readonly />
                               <input type="hidden" class="form-control" id="seq_number" name="seq_number" value="{{  $rcv->seq_number }}" readonly />
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Department</label>
                           <div class="col-sm-8">
                              <select class="form-control select2" id="dept_name_number" name="dept_name_number" value="dept_name_number">
                                 <option value="">{{  $rcv->name }}</option>
                                 @for ($i = 0; $i < count($dept_list); $i++) 
                                 <option value="{{$dept_list[$i]->retail_dept }}">{{ $dept_list[$i]->name }}</option>
                                 @endfor
                                 <!--  @foreach ($dept_list as $key => $value) 
                                    <option value="{{ $value->retail_dept }}">{{ $value->retail_dept }} : {{ $value->name }}</option>
                                    @endforeach -->
                              </select>
                              <!--  <input type="hidden" class="form-control" id="departmentnum" placeholder="departmentnum" /> -->
                           </div>
                        </div>
                        <div class="form-horizontal">
                           <!-- <div class="col-lg-6"> -->
                           <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">Vendor</label>
                              <div class="col-sm-8">
                                 <select class="form-control select2" id="vendor" name="vendor" value="vendor">
                                    <option value="">{{  $rcv->vname }}</option>
                                 </select>
                                 @if($errors->has('vendor'))
                                 {{ $errors->first('vendor')}}
                                 @endif                 
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="#" class="col-sm-4 control-label">Number</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="vendornumber" name="vendornumber"{{ (Input::old('vendornumber'))?'
                                 value="'.Input::old('vendornumber').'"':''}}  placeholder="Number" readonly />
                                 <!--  @if($errors->has('vendornumber'))
                                    {{ $errors->first('vendornumber')}}
                                    @endif  -->
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="#" class="col-sm-4 control-label">Transporter</label>
                              <div class="col-sm-8 transporter">
                                 <input type="text" class="form-control" id="transporter" name="transporter" placeholder="Transporter" readonly="readonly" />
                                 <select class="form-control showtransporter" id="transporter">
                                    <option value="">Select</option>
                                    @foreach ($transporter_list as $key => $valuedb) 
                                    <option value="{{ $valuedb->trns_number }}">{{ $valuedb->name }}</option>
                                    @endforeach
                                 </select>
                                 <!--  <input type="hidden" class="form-control" id="departmentnum" placeholder="departmentnum" /> -->
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="#" class="col-sm-4 control-label">Number</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="number_2" name="number_2" placeholder="Number" readonly="readonly" />
                                 <!--  @if($errors->has('number_2'))
                                    {{ $errors->first('number_2')}}
                                    @endif  -->
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="#" class="col-sm-4 control-label">Invoice Date</label>
                              <div class="col-sm-8">
                                 <input type="date" class="form-control" id="invoice_date" name="invoice_date" value="{{ $rcv->invoice_date }}" placeholder="MM-DD-YY" />
                                 @if($errors->has('invoice_date'))
                                 {{ $errors->first('invoice_date')}}
                                 @endif       
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="#" class="col-sm-4 control-label">Invoice Number</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" name="invoice_number"{{ (Input::old('invoice_number'))?' 
                                 value="'.Input::old('invoice_number').'"':''}} 
                                 id="invoice_number" placeholder="Invoice Number" />
                                 @if($errors->has('invoice_number'))
                                 {{ $errors->first('invoice_number')}}
                                 @endif 
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="#" class="col-sm-4 control-label">Invoice Type</label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="invoice_type" name="invoice_type">
                                    <option value="">Select Invoice Type</option>
                                    <option value="D">D - Delivery</option>
                                    <option value="R">R - Return</option>
                                    {{-- 
                                    <option value="S">S - Store Supplies</option>
                                    --}}
                                 </select>
                                 @if($errors->has('invoice_type'))
                                 {{ $errors->first('invoice_type')}}
                                 @endif
                              </div>
                           </div>
                           <!--  <div class="form-group">
                              <label for="#" class="col-sm-4 control-label">Status</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="status" name="status" />
                                 <select class="form-control" id="status" name="status" placeholder="Status" readonly >
                                    <option value="A">A - Accepted</option>
                                    {{-- 
                                    <option value="B">B</option>
                                    --}}
                                    <option value="I">I - Incomplete</option>
                                    <option value="O" selected="">O - Open</option>
                                    {{-- 
                                    <option value="P">P</option>
                                    --}}
                                    <option value="R">R - Reviewed</option>
                                    <option value="U">U - Unknown</option>
                                    <option value="V">V - Voided</option>
                                 </select>
                              </div>
                              </div>
                                                </div> -->
                           <div class="form-group">
                              <label for="#" class="col-sm-4 control-label">Status</label>
                              <div class="col-sm-8">
                                 <input type="text" class="form-control" id="status" name="status" placeholder="O - Open" readonly="" />
                              </div>
                           </div>
                        </div>
                        <!-- <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Cost Discrepancies</label>
                           <div class="col-sm-8">
                              <select class="form-control" id="cost_discrepancies" name="cost_discrepancies" 
                              value="'.Input::old('cost_discrepancies').'"':''}}   placeholder="Cost Discrepancies" >
                              <option value="N">N</option>
                              <option value="Y">Y</option>
                              </select>
                           </div>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Cost Discrepancies</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="cost_discrepancies" name="cost_discrepancies" placeholder="N" readonly="" />
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Method Received</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="method_received" name="method_received" placeholder="O - OFFLINE"  readonly="readonly" />
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <!-- <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Method Received</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="method_received" name="method_received" placeholder="O - OFFLINE"  readonly="readonly" />
                           </div>
                           </div> -->
                        <!--  <div class="form-group">
                           <label for="#" class="col-sm-4 control-label"></label>
                           <label for="#" class="col-sm-6 control-label" style="text-align: center;">Vendor</label>
                           <label for="#" class="col-sm-4 control-label" style="text-align: center;">Store</label>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-7 control-label" style="padding-left: 195px">-------Vendor-------</label>
                           <label for="#" class="col-sm-5 control-label" style="">--------Store--------</label> 
                        </div>
                        <!--  <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Qty</label>
                           <div class="col-sm-8">
                              <input type="number" class="form-control" id="qty_vendor" name="qty_vendor"{{ (Input::old('qty_vendor'))?'  
                              value="'.Input::old('qty_vendor').'"':''}} placeholder="0.00" />
                              @if($errors->has('qty_vendor'))
                              {{ $errors->first('qty_vendor')}}
                              @endif 
                           </div>
                           <div class="col-sm-4">
                              <input type="text" class="form-control" id="qty_store" 
                                     name="qty_store"{{ (Input::old('qty_store'))?' value="'.Input::old('qty_store').'"':''}}  placeholder="0.00" readonly/>
                              @if($errors->has('qty_store'))
                                            {{ $errors->first('qty_store')}}
                                            @endif 
                              </div>
                           </div>
                           -->
                        <div class="form-group" style="padding-top: 25px;">
                           <label for="#" class="col-sm-4 control-label">Qty</label>
                           <div class="col-sm-4">
                              <input type="number"  class="form-control float select" id="qty_vendor" min="0" max="6000" name="qty_vendor" {{ (Input::old('qty_vendor'))?' value="'.Input::old('qty_vendor').'"':''}} placeholder="$0.00"/>
                              @if($errors->has('qty_vendor'))
                              {{ $errors->first('qty_vendor')}}
                              @endif 
                           </div>
                           <div class="col-sm-4">
                              <b><span class=" span store_qty">$0.00</span></b>
                           </div>
                        </div>
                        <!--  <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Store Inventory</label>
                           <div class="col-sm-8">
                              <input type="number" step=".01" class="form-control" id="store_inventory_vendor" 
                              name="store_inventory_vendor"{{ (Input::old('store_inventory_vendor'))?'   
                              value="'.Input::old('store_inventory_vendor').'"':''}}  placeholder="$0.00" />
                              @if($errors->has('store_inventory_vendor'))
                              {{ $errors->first('store_inventory_vendor')}}
                              @endif 
                           </div>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label txt">Store Inventory</label>
                           <div class="col-sm-4">
                              <input type="number" class="form-control select float vendor" id="store_inventory_vendor" 
                              name="store_inventory_vendor"{{ (Input::old('store_inventory_vendor'))?' value="'.Input::old('store_inventory_vendor').'"':''}} placeholder="$0.00" />
                              @if($errors->has('store_inventory_vendor'))
                              {{ $errors->first('store_inventory_vendor')}}
                              @endif 
                           </div>
                           <div  class="col-sm-4">
                              <b><span  class="span store_inventory_store ">$0.00</span></b>
                           </div>
                        </div>
                        <!--  <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">CRV</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" name="crv" id="crv" value="0.00">
                           </div>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">CRV</label>
                           <!-- if else condition for crv -->
                           <div class="col-sm-3">
                           </div>
                           <div  class="col-sm-4">
                              <input type="number"  class="form-control float select" id="crv" name="crv" placeholder="$0.00" value="0.00"/>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Allowance</label>
                           <!-- if else condition for crv -->
                           <div class="col-sm-3">
                           </div>
                           <div  class="col-sm-4">
                              <input type="number"  class="form-control  select" id="allowances" placeholder="$0.00" name="allowances" value="0.00" readonly />
                           </div>
                        </div>
                        <!-- <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Allowance</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" name="allowances" id="allowances" value="0.00">
                           </div>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Selling Supplies</label>
                           <div class="col-sm-4">
                              <input type="number" class="form-control select float vendor" id="selling_supplies_vendor" 
                                 name="selling_supplies_vendor" placeholder="$0.00" />
                              @if($errors->has('selling_supplies_vendor'))
                              {{ $errors->first('selling_supplies_vendor')}}
                              @endif
                           </div>
                           <div  class="col-sm-4">
                              <!-- <input type="number"  class="form-control store txt1" id="ws_tot_sell_supply" name="ws_tot_sell_supply" value="" readonly="" /> -->
                              <b><span class="span ws_tot_sell_supply ">$0.00</span></b>
                           </div>
                        </div>
                        <!--  <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Selling Supplies</label>
                           <div class="col-sm-8">
                              <input type="number" step=".01" class="form-control" id="selling_supplies_vendor" 
                                 name="selling_supplies_vendor" value="0.00" placeholder="+ $0.00" />
                              @if($errors->has('selling_supplies_vendor'))
                              {{ $errors->first('selling_supplies_vendor')}}
                              @endif 
                           </div>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Store Supplies</label>
                           <div class="col-sm-4">
                              <input type="number" class="form-control select float vendor" id="store_supplies_vendor" 
                                 name="store_supplies_vendor" value="" placeholder="$0.00"/>
                              @if($errors->has('store_supplies_vendor'))
                              {{ $errors->first('store_supplies_vendor')}}
                              @endif 
                           </div>
                           <div  class="col-sm-4">
                              <b><span class="span ws_tot_store_supply ">$0.00</span></b>
                           </div>
                        </div>
                        <!-- <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Store Supplies</label>
                           <div class="col-sm-8">
                              <input type="number" step=".01" class="form-control" id="store_supplies_vendor" 
                                 name="store_supplies_vendor" value="0.00" placeholder="+ $0.00" />
                              @if($errors->has('store_supplies_vendor'))
                              {{ $errors->first('store_supplies_vendor')}}
                              @endif 
                           </div>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Net</label>
                           <div class="col-sm-4">
                              <b><span class="sum" style="padding-right: 70px">$0.00</span></b>
                           </div>
                           <div  class="col-sm-4">
                              <b><span class="sum span" id="ws_tot_store_supply">$0.00</span></b>
                           </div>
                        </div>
                        <!--  <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Net</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="net_vendor" name="net_vendor"{{ (Input::old('net_vendor'))?'
                              value="'.Input::old('net_vendor').'"':''}} placeholder="$0.00" readonly/>
                              @if($errors->has('net_vendor'))
                              {{ $errors->first('net_vendor')}}
                              @endif 
                           </div>
                           </div> -->
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">Created Date</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="created_date" name="created_date" placeholder="Created Date" 
                                 value="<?php echo date('d-m-Y G:i:s'); ?>" readonly/>
                           </div>
                           <label for="#" class="col-sm-4 control-label">Updated Date</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="updated_date" name="updated_date" placeholder="Updated Date" 
                                 value="<?php echo date('d-m-Y G:i:s'); ?>" readonly/>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label">By</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="by" name="by" value="DSD" readonly="readonly" />
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-12 topspace" align="center">
                           <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                           <input type="reset" value="Reset" class="btn"/>
                           <!--  <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/returnhome')}}'" /> -->
                           {{ Form::token()}}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>
<script type="text/javascript" src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>
<style type="text/css"></style>
<script type="text/javascript">
   $(document).ready(function(){
 var invoice_type;
 var ws_crv_switch;
       // $("#vendornumber").on('change',function(){
       //  alert(this.value);
       //  });
      $(".showtransporter").hide();
   
      $('.select').on("click",function (){
       $(this).select();
       });
   
      $(".float").blur(function(){  
       var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
   
     });
   
      $('#qty_vendor').on("blur", function() {
              
             var store = $(this).val();
             $(".store_qty").text(store);
            });
   
    $('#store_inventory_vendor,#crv').on("blur", function() {
             var crv = 0 ;
             var diff = 0;
             var store = $('#store_inventory_vendor').val();
             var crv = $('#crv').val();
             diff = store - crv;
             diff = diff.toFixed(2);
             $(".store_inventory_store").text(diff);
    });  
   
   
    $('#selling_supplies_vendor').on("blur", function() {
              
             var store = $(this).val();
             $(".ws_tot_sell_supply").text(store);
     });
   
   
   
    $('#store_supplies_vendor').on("blur", function() {
              
             var store = $(this).val();
             $(".ws_tot_store_supply").text(store);
     });
   
   
    $(".vendor").on("keydown keyup change", function() {
        calculateSum();
     });
   
    function calculateSum() {
    var sum = 0;
   
    $(".vendor").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);   
        } 
    });
   sum = sum.toFixed(2);
    $('.sum').text(sum);
    
    }
   
   // $('#function_code').on('change', function(e) {
   //       e.preventDefault();
   //       function_code = $("#function_code").val();
   //       //alert(function_code);
   //       if (function_code != '') {
   //           $.post("ws_func_desc", {
   //               function_code: function_code
   //           }, function(result) { //alert(result); return false;
   //                $("#ws_func_desc").val(result);
   //           });
   //       }
   //   });
   

   $("#dept_name_number").on('change',function (e) {
      e.preventDefault(); 
      deptNumber = $("#dept_name_number").val();
   
      if(deptNumber != ''){
        $('#vendor').val() == '';
         $.post("vendorlist", 
            {deptnumber: deptNumber},
                     function(result){ 
                   
                   var arrayvendorlist = result.split(',');
   
                 $.each(arrayvendorlist, function (index, value) {
                      var vendor = value.split('_');
                          
                      $('#vendor').append($("<option></option>")
                       .attr("value",vendor[1]+'_'+vendor[0])
                       .text(vendor[0])); 
                     
                   });
                 
                
               });
     }
   
   });
   
   $("#vendor").on('change',function(e){ 
     e.preventDefault(); 
     vendorname_number = $("#vendor").val(); 
     //alert(vendorname_number);
     var vendo0r_numname = vendorname_number.split('_');
     
      $("#vendornumber").val(vendo0r_numname[0]);
      
       vendornumber = vendo0r_numname[0];
       //alert(vendornumber);
     
    
       if(vendornumber != ' '){
        $.post("crv_switch", {
                 vendorno: vendornumber
             }, function(result) { 
              var ws_crv_switch = result;
              if(ws_crv_switch == 'Y')
              {
                $('#crv').removeAttr('readonly');
              }
              else if(ws_crv_switch == 'N')
              {
                $('#crv').attr('readonly', 'readonly');
              }
             });

      
        $.post("transporter_list", 
         {vendornumber: vendornumber}, 
                      function(result){ 
        // alert(result);
         var transporter_data = result.split('_');
         //alert(transporter_data);
               if(result == 'empty')
                   {
                     $("#transporter").hide();
                     //$("#transporter").removeAttr("name");
                      //$("img").attr("name", "sravan");
                     $(".showtransporter").show();
                     $(".showtransporter").attr("name", "transporter");
                     //$(".showtransporter").removeAttr("id");
                   }
                   else
                   {
                     var vendorname = transporter_data[0];
                  var vendornumber = transporter_data[1]; 
                  //alert(vendorname);
                  
                   var res = transporter_data[1].slice(0, 6);
                   //alert(res);return false;
                   $("#transporter").val(vendorname);
                   //$(".showtransporter").removeAttr("name");
                    
                   $("#number_2").val(res);
                   }
                  
                   
               });
   
       }
      
       
   });


   $("#invoice_type").on('change',function(e){
     e.preventDefault();  
     var invoice_type = $('#invoice_type').val();
     if(invoice_type == 'R')
     {
        $('#crv').attr('readonly', 'readonly');
     }
     /*else if (invoice_type == 'D') {

      $('#crv').removeAttr('readonly', 'readonly');
       
     }*/
      });

   
   
  /* $("#vendor").on('change',function(e){ 
     e.preventDefault(); vendorname_number = $("#vendor").val(); 
    
     var vendo0r_numname = vendorname_number.split('_');
      $("#vendornumber").val(vendo0r_numname[0]);
    
       vendornumber = vendo0r_numname[0];
   
       $.post("crv_status", {vendornumber: vendornumber}, function(result){ 
         //alert(result);
         var transporter_data = result.split('_');
         //alert(transporter_data);
               if(result == 'empty')
                   {
                     $("#transporter").hide();
                     //$("#transporter").removeAttr("name");
                      //$("img").attr("name", "sravan");
                     $(".showtransporter").show();
                     $(".showtransporter").attr("name", "transporter");
                     //$(".showtransporter").removeAttr("id");
                   }
                   else
                   {
                     var vendorname = transporter_data[0];
                  var vendornumber = transporter_data[1]; 
                  //alert(vendornumber);return false;
                   var res = transporter_data[1].slice(0, 6);
                   //alert(res);
                   $("#transporter").val(vendorname);
                   //$(".showtransporter").removeAttr("name");
                    
                   $("#number_2").val(res);
                   }
                  
                   
               });
   });*/
   
   
   $(".showtransporter").change(function (e){
   e.preventDefault(); transporter_number = $(".showtransporter").val(); //alert(deptNumber);
      if(transporter_number != '')
      {
         $("#number_2").val(transporter_number); return true;
      }
   });
   
   $("#placevendorlist").change(function(e){ 
     e.preventDefault(); vendorname_number = $("#vendorname").val(); //alert(vendorname_number);
     var vendo0r_numname = vendorname_number.split('_');
      $("#vendornumber").val(vendo0r_numname[0]);
   });
   
   
   
   /* $("#qty_vendor").blur(function(){
        qty_vendor=$("#qty_vendor").val();
        $("#qty_store").val(qty_vendor); return true;
     });
   */
     /*$("#store_inventory_vendor").blur(function(){
       
         store_inventory_vendor=parseFloat($("#store_inventory_vendor").val(),10).toFixed(2);
       $("#store_inventory_vendor").val(store_inventory_vendor);
        $("#store_inventory_store").val(store_inventory_vendor);
        calnet(); return true;
     });*/
   
    /* $("#store_inventory_vendor1").blur(function(){
       
        store_inventory_vendor1=$("#store_inventory_vendor1").val();
       $("#store_inventory_store1").val(store_inventory_vendor1);
       calnet(); return true;
     });*/
   
     /*$("#store_inventory_vendor2").blur(function(){
        store_inventory_vendor2=$("#store_inventory_vendor2").val();
        $("#store_inventory_store2").val(store_inventory_vendor2);
        calnet(); return true;
     });*/
   
   
    /* $("#selling_supplies_vendor").blur(function(){
       selling_supplies_vendor=parseFloat($("#selling_supplies_vendor").val(),10).toFixed(2);
         $("#selling_supplies_vendor").val(selling_supplies_vendor);
        $("#selling_supplies_store").val(selling_supplies_vendor);
        calnet(); return true;
     });*/
   
     /*$("#store_supplies_vendor").blur(function(){
       
     store_supplies_vendor=parseFloat($("#store_supplies_vendor").val(),10).toFixed(2);
     $("#store_supplies_vendor").val(store_supplies_vendor);
       $("#store_supplies_store").val(store_supplies_vendor);
       calnet(); return true;
     });
   */
   
   
   /* function calnet() {
           alert('hii');
        qty_vendor=$("#qty_vendor").val();
        alert(qty_vendor);
        store_inventory_vendor=$("#store_inventory_vendor").val(); 
       /* store_inventory_vendor1=$("#store_inventory_vendor1").val(); 
        store_inventory_vendor2=$("#store_inventory_vendor2").val();*/
        //selling_supplies_vendor=$("#selling_supplies_vendor").val();
        //store_supplies_vendor=$("#store_supplies_vendor").val();
   
        //if(qty_vendor == ''){qty_vendor  = 0; }
        //if(store_inventory_vendor == ''){store_inventory_vendor  = 0; }
       /* if(store_inventory_vendor1 == ''){store_inventory_vendor1  = 0; }
        if(store_inventory_vendor2 == ''){store_inventory_vendor2  = 0; }*/
        //if(selling_supplies_vendor == ''){selling_supplies_vendor  = 0; }
        //if(store_supplies_vendor == ''){store_supplies_vendor  = 0; }
        //parseFloat(store_inventory_vendor1)+parseFloat(store_inventory_vendor2)
        //tot=parseFloat(store_inventory_vendor)+parseFloat(selling_supplies_vendor)+parseFloat(store_supplies_vendor);
        //$("#net_vendor").val(tot.toFixed(2)); $("#net_store").val(tot);
       
   
   });
   
   
   
   
     $(function() {
           //Initialize Select2 Elements
           $(".select2").select2();
       });
   
</script>
@stop
