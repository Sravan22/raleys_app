@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>
<style> .form-horizontal .control-label {
   text-align: right;
   /* padding-left: 60px; */
   }
</style>
<div class="container">
   <div id="loginbox" style="margin-top:10px;" class="mainbox col-md-6 col-md-offset-3 col-sm-5 col-sm-offset-3">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Cost Discrepancy Report</div>
         </div>
         <div class="panel-body"  style="margin-top:10px;" >
            <form action="{{URL::route('mktmgr-post-report-costdiscrepancyreport')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <label for="inputPassword" class="col-sm-6 control-label">Please key date of report</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control" id="keydate" placeholder="MM-DD-YY" name="keydate" autofocus="" tabindex="2" />
                     @if($errors->has('keydate'))
                     {{ $errors->first('keydate')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 52%">
                        <input type="submit" id="submit" tabindex="3" value="Search" class="btn"/>
                        <input type="reset"  id="btn" tabindex="4" value="Cancel" class="btn"/>
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="5"></div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>

<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>


<script type="text/javascript">
   $('#cancel-btn').click(function() {
     location.reload();
    
   });
    $(function() {
   $('#focusguard-2').on('focus', function() {
   $('#keydate').focus();
   });
   
   $('#focusguard-1').on('focus', function() {
   $('#btn').focus();
   });
   
     });
</script>
@stop
