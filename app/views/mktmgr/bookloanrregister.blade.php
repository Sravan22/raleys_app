@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <div class="mainbox">
      <div class="panel panel-info">
         <div class="panel_heading">
            <div class="panel-title">LOANS TO REGISTERS</div>
         </div>
         <div class="panel_body">
            {{-- <form action="{{URL::route('mktmgr-postbookloanrregisterreg')}}" class="form-horizontal" method="post"  --}}
            <form action="{{URL::route('mktmgr-postloantoregregister')}}" class="form-horizontal" method="post" 
               role="form" style="display: block;">
               <div class="form-group">
                  <label for="reg_num" class="control-label col-sm-6">Register Number</label>
                  <div class="col-sm-6">
                     <select class="form-control select2" id="reg_num" name="reg_num">
                        <option value="">Choose Register Number</option>
                        @foreach ($registers_num as $key => $value) 
                        <option value="{{ $value->reg_num }}">{{ $value->reg_num }}</option>
                        @endforeach
                     </select>
                     @if($errors->has('reg_num'))
                     {{ $errors->first('reg_num')}}
                     @endif
                  </div>
               </div>
               <div class="form-group padding_bottom">
                  <label for="dateofinfo" class="control-label col-sm-6"> Date of the Information</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control" id="dateofinfo" placeholder="" name="dateofinfo" value="{{ $previewsdate }}">
                     @if($errors->has('dateofinfo'))
                     {{ $errors->first('dateofinfo')}}
                     @endif
                  </div>
               </div>
               {{-- <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div> --}}
               <div class="form-group padding_bottom">
                 <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                       <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                       <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                       {{ Form::token()}}
                    </div>
                 </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>
<script type="text/javascript">
   $(function(){
                   var dtToday = new Date();
                   
                   var month = dtToday.getMonth() + 1;
                   var day = dtToday.getDate();
                   var year = dtToday.getFullYear();
                   if(month < 10)
                       month = '0' + month.toString();
                   if(day < 10)
                       day = '0' + day.toString();
                   
                   var maxDate = year + '-' + month + '-' + day;
                   $('#dateofinfo').attr('max', maxDate);
               });
</script>
@stop