@extends('layout.dashboardbookkeepermarket')
@section('page_heading','Postage')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >POSTAGE STAMP INVENTORY - {{date('m/d/Y', strtotime($work_data->dateofinfo))}}</div>
         </div>
         <div style="padding-top:10px" class="panel-body" >
            <form action="{{URL::route('mktmgr-bookpostagepostdata')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <input type="hidden" name="ps_date" id="ps_date" value="{{date('Y-m-d', strtotime($work_data->dateofinfo))}}" />
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-9">Postage Stamp Invoice Number_1 Received</label>
                  <div class="col-sm-3">
                     <input type="text" tabindex="1" class="form-control sel" id="ck_no_1" placeholder="" name="ck_no_1" value="{{$work_data->ck_no_1}}" maxlength="10" style="width: 105px;"
                        <?php if($file_locked =='Y'){echo 'disabled';} ?> />
                     @if($errors->has('ck_no_1'))
                     {{ $errors->first('ck_no_1')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-9">Postage Stamp Invoice Number_2 Received</label>
                  <div class="col-sm-3">
                     <input type="text" tabindex="2" class="form-control sel" id="ck_no_2" placeholder="" name="ck_no_2" value="{{$work_data->ck_no_2}}" maxlength="10" style="width: 105px;" 
                        <?php if($file_locked =='Y'){echo 'disabled';} ?>/>
                     @if($errors->has('ck_no_2'))
                     {{ $errors->first('ck_no_2')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 10px;">
                  <label for="inputPassword" class="control-label col-sm-9">Beginning Postage Stamp Inventory</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control txt" id="begin_inv" placeholder="" name="begin_inv" value="{{$work_data->begin_inv}}" readonly="" style="width: 105px;">
                     @if($errors->has('begin_inv'))
                     {{ $errors->first('begin_inv')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-9">Deliveries</label>
                  <div class="col-sm-3">
                     <input type="text" tabindex="3" class="form-control txt" id="purchases" placeholder="" name="purchases" value="{{$work_data->purchases}}" style="width: 105px;" <?php if($file_locked =='Y'){echo 'disabled';} ?> />
                     @if($errors->has('purchases'))
                     {{ $errors->first('purchases')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-9">Ending Postage Stamp Inventory Safe</label>
                  <div class="col-sm-3">
                     <input type="text" tabindex="4" class="form-control txt" id="end_inv_safe" placeholder="" name="end_inv_safe" value="{{$work_data->end_inv_safe}}" style="width: 105px;" 
                        <?php if($file_locked =='Y'){echo 'disabled';} ?> />
                     @if($errors->has('end_inv_safe'))
                     {{ $errors->first('end_inv_safe')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-9">Ending Postage Stamp Inventory Tills</label>
                  <div class="col-sm-3">
                     <input type="text" tabindex="5" class="form-control txt" id="end_inv_tills" placeholder="" name="end_inv_tills" value="{{$work_data->end_inv_tills}}" style="width: 105px;"
                        <?php if($file_locked =='Y'){echo 'disabled';} ?>/>
                     @if($errors->has('end_inv_tills'))
                     {{ $errors->first('end_inv_tills')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-9">Ending Postage Stamp Inventory CashAdd (Belair Only)</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="end_inv_ca" placeholder="" name="end_inv_ca" value="{{$work_data->end_inv_ca}}" readonly="" style="width: 105px;">
                     @if($errors->has('end_inv_ca'))
                     {{ $errors->first('end_inv_ca')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 15px;">
                  <label for="inputPassword" class="control-label col-sm-9">Calculated Sales</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="calc_sales" placeholder="" name="calc_sales" value="{{$work_data->calc_sales}}" readonly="" style="width: 105px;">
                     @if($errors->has('calc_sales'))
                     {{ $errors->first('calc_sales')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-9">P/S Sales From Unity Report</label>
                  <div class="col-sm-3">
                     <input type="text" tabindex="6" class="form-control txt" id="ps_unity_sales" placeholder="" name="ps_unity_sales" value="{{$work_data->ps_unity_sales}}" style="width: 105px;" <?php if($file_locked =='Y'){echo 'disabled';} ?> />
                     @if($errors->has('ps_unity_sales'))
                     {{ $errors->first('ps_unity_sales')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-9">Sales Difference</label>
                  <div class="col-sm-3">
                     <input type="text" readonly="" class="form-control txt" id="sales_diff" placeholder="" name="sales_diff" value="{{$work_data->sales_diff}}" style="width: 105px;">
                     @if($errors->has('sales_diff'))
                     {{ $errors->first('sales_diff')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 20px;">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="accept-submit" id="submit" tabindex="7" value="Accept" class="btn"
                           <?php if($file_locked =='Y'){echo 'disabled';} ?> />  
                        <input type="submit" name="login-submit" id="submit" tabindex="8" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               {{-- files locked  --}}
               <br>
               @if($file_locked =="Y")
               <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @endif
            </form>
         </div>
      </div>
   </div>
</div>
</div>
@section('jssection')
@parent
<script>
   function isFloat(num){
    
   if(num.toString().indexOf(".")==-1){
       return false;
   }
   return true;
   }
      function convertnulltozero(val){
    
     if(isNaN(val)==true || val==NaN || val==null || val===undefined || val=='NaN' || typeof(val)=='undefined' || val==''){
        
           val=0;
           
       } else {
           val=val.toFixed(2);
       }
       
     return val;
   }
   jQuery(document).ready(function($) {
   $(".txt,.sel").on("click", function () {
   $(this).select();
   });
   
   /*  $(".txt").blur(function(){  
   var sum = 0; 
   selfield = this.id;
    
   selfieldval=$("#"+selfield).val(); 
   $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
   });*/
    $("#ck_no_1,#ck_no_2,#begin_inv, #purchases, #end_inv_safe, #end_inv_tills,#end_inv_ca,#ps_unity_sales").keydown(function (e) {
   // Allow: backspace, delete, tab, escape, enter 
   
   if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl/cmd+A
       (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
       (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
       (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
       (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
   }

   // Ensure that it is a number and stop the keypress
   if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
       e.preventDefault();
   }

 
   });
      $( "#begin_inv, #purchases, #end_inv_safe, #end_inv_tills,#end_inv_ca,#ps_unity_sales " ).on( "blur", function() {
        if(isFloat($(this).val())==true){
              
          } else {
           var nw=parseFloat($(this).val())/100;
   
            $(this).val(convertnulltozero(nw));
           }
         });
         
     $( "#begin_inv, #purchases, #end_inv_safe, #end_inv_tills,#end_inv_ca,#ps_unity_sales " ).on( "blur", function() {
         
       calculateSalesDiff();
     });
     
     function calculateSalesDiff(){
         
         var begin_inv=parseFloat($("#begin_inv").val());
         var purchases=parseFloat($("#purchases").val());
         var end_inv_safe=parseFloat($("#end_inv_safe").val());
         var end_inv_tills=parseFloat($("#end_inv_tills").val());
         var end_inv_ca=parseFloat($("#end_inv_ca").val());
         var ps_unity_sales=$("#ps_unity_sales").val();
         var calc_sales=0;
         var sales_diff=0;
         
          calc_sales =   (begin_inv + purchases -  end_inv_safe - end_inv_tills - end_inv_ca) ;
            //calc_sales = calc_sales / 100;
     
           
           sales_diff =   ((calc_sales) - ps_unity_sales); 
           //sales_diff = sales_diff /100;
           
           
           if(calc_sales==NaN || calc_sales==undefined){
               calc_sales=0;
           }
           if(sales_diff==NaN || sales_diff==undefined){
               sales_diff=0;
           }
           
           /*$("#calc_sales").val(calc_sales.toFixed(2));
           $("#sales_diff").val(sales_diff.toFixed(2));*/
           $("#calc_sales").val(convertnulltozero(calc_sales));
           $("#sales_diff").val(convertnulltozero(sales_diff));
           
     }
      
   });
</script>
@endsection
@stop
