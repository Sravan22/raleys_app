@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >CHG ADD</div>
         </div>
         <div  class="panel-body" >
            <form action="{{URL::route('mktmgr-post-bookkeeper-chgadd')}}" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return validateForm()">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Register Number</label>
                  <div class="col-sm-6">
                     <input type="number" class="form-control" id="dateofinfo" placeholder="" name="regnumber" min="1" max="999">
                     @if($errors->has('regnumber'))
                     {{ $errors->first('regnumber')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Registration Type</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="regtype" style="text-transform: uppercase" maxlength="1" placeholder="" name="regtype">
                     @if($errors->has('regtype'))
                     {{ $errors->first('regtype')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop
<script type="text/javascript">
   function validateForm()
   {
      var str = document.getElementById("regtype").value;
      var regtype = str.toUpperCase();
      if(!(regtype == 'G'))
      {
         alert('Registration Type must be  G');
         $('#regtype').val('');$('#regtype').focus();return false;
      }
   }
   
</script>
