<?php 
   // echo '<pre>';
   // print_r($utility_rec_array);exit;
   ?>
@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style>
   @media print {
   a[href]:after {
   content: none !important;
   }
   }
</style>
<div class="container">
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Entry Date</th>
            <th>Utility Payment Cnt</th>
         </tr>
      </thead>
      <tbody>
         @if($utility_rec_array)
         @for ($i = 0; $i< count($utility_rec_array); $i++) 
         <tr>
            <td>{{date('m/d/Y',strtotime($utility_rec_array[$i]['entry_date']))}}</td>
            <td>{{ $utility_rec_array[$i]['util_cnt'] }}</td>
         </tr>
         @endfor    
         @else   
         <tr>
         <tr>
            <td colspan="5" align="center">No records found!</td>
         </tr>
         </tr>
         @endif
      </tbody>
   </table>
   <div class="form-group">
   <div class="row">
      <div class="col-sm-12" align="center">
         <input type="button" value="Go Back" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookutilitypymntcnt')}}'" />
      </div>
   </div>
</div>
</div>

@stop