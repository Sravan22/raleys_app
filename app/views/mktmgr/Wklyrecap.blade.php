@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
</header>

<!-- last staurday date -->
<?php  $d=strtotime("last Saturday"); $lastsaturday = date("Y-m-d", $d); ?>


<div class="container">

   <!-- Start flash-message -->
   <!-- <div class="flash-message" id="recapmsg" style="margin-top: %;margin-left: 26%;width: 48%;">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      </p>
      @endif
      @endforeach
   </div>
   <div class="alert alert-warning" id="recapmsg2" style="width: 35%;margin-left: 25%;display: none;">
           If you hav already printed your recap and are 
           recalculating it, don't forget to reprint<br>
            click to <a href="#" class="alert-link" style="color:red" ;>Continue</a>.
   </div> -->

   
   <!-- end .flash-message -->

   <!-- start form  -->
   <div id="" style="margin-top:5px;" class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >RECAP REPORT</div>
         </div>
         <div class="panel-body" style="padding-top: 20px;">
            <form action="{{URL::route('mktmgr-postwklyrecap')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-7" style="text-align: right;">Please enter the Date of the Information</label>
                  <div class="col-sm-5">
                     <input type="date" class="form-control" id="dateofinfo" placeholder="" name="dateofinfo"  value ="<?php echo $lastsaturday; ?>">
                     @if($errors->has('dateofinfo'))
                     {{ $errors->first('dateofinfo')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>

</div>

<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>        
	<script type="text/javascript">
		$(function(){
		var dtToday = new Date();

		var month = dtToday.getMonth() + 1;
		var day = dtToday.getDate();
		var year = dtToday.getFullYear();
		if(month < 10)
		month = '0' + month.toString();
		if(day < 10)
		day = '0' + day.toString();

		var maxDate = year + '-' + month + '-' + day;
		$('#dateofinfo').attr('max', maxDate);
		});


	/*	jQuery(document).ready(function($) {
			$('#recapmsg2').hide();
			$('#cont').click(function(){
				$('#recapmsg').hide();
				$('#recapmsg2').show();
			});
			
		});*/
	</script>

@stop