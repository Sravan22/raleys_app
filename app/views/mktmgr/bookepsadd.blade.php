@extends('layout.dashboardbookkeepermarket')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container" style="margin-left: 128px;">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-7 col-md-offset-2 ">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >EPS ERROR FORM</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-bookepsshow')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="0"></div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-7" style="padding-left: 63px;">Please enter the Date of the Information</label>
                  <div class="col-sm-5">
                     <input type="date" class="form-control lastdate" id="date" placeholder="" name="dateofinfo" value="{{$data->dateofinfo}}" tabindex="1" autofocus="">
                     @if($errors->has('dateofinfo'))
                     {{ $errors->first('dateofinfo')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="2" value="Submit" class="btn">
                        <input type="Reset"  id="reset" tabindex="3" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
                <div class="focusguard" id="focusguard-2" tabindex="4"></div>

            </form>
         </div>
      </div>
   </div>
</div>
 
<script>
   $( function() {
        $('#focusguard-2').on('focus', function() {
        $('#date').focus();
        });

        $('#focusguard-1').on('focus', function() {
        $('#reset').focus();
        });

     
   } );
</script>
@endsection
@stop