<?php
   //echo '<pre>';print_r($resultArray);exit;
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >GRAND TOTALS &nbsp;&nbsp;&nbsp;&nbsp;   {{ date("m/d/Y", strtotime($dateofinfo)) }}</div>
         </div>
         <form action="" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;">
         <?php //$i=1; $gtot =0;
            ?>
         <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Beginning Loans.................</label>
            <div class="col-sm-6">
               <span><b>{{ number_format((float)$resultArray[1]['item_amt'], 2, '.', ''); }}</b></span>
            </div>
         </div>
         <?php 
            $bl = $resultArray[1]['item_id'];
            $lr = $resultArray[2]['item_id'];
            $exclude = array($bl,$lr);
            for($i=0;$i<count($resultArray[0]);$i++)
            {
               if (in_array($resultArray[0][$i]['item_id'], $exclude)) continue; ?>
               <div class="form-group">
                  <label for="inputEmail" class="control-label col-sm-6">(<?php echo $resultArray[0][$i]['item_id']; ?>) <?php echo $resultArray[0][$i]['item_desc']; ?></label>
                  <div class="col-sm-6">
                     <span><b><?php echo number_format((float)$resultArray[0][$i]['item_amt'], 2, '.', ''); ?></b></span>
                  </div>
               </div>
         <?php  }    ?>
         <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6"> Loans To Registers Totals............</label>
            <div class="col-sm-6">
               <span><b>{{ number_format((float)$resultArray[2]['item_amt'], 2, '.', ''); }}</b></span>
            </div>
         </div>
         <br>
      </div>
   </div>
</div>
</div>     
<div class="form-group">
   <div class="row">
      <div class="col-sm-12 topspace" align="center">
         <input type="button" value="Go Back" class="btn" id="cancel" name="cancel" onclick="goBack()">
         {{ Form::token()}}
      </div>
   </div>
</div>
<script>
   function goBack() {
       window.history.back();
   }
</script>  
@stop