<?php 
// echo $arrtot['groctot']."<br>";
// echo $arrtot['drugtot']."<br>";exit;
//echo '<pre>';print_r($arrtot);exit;
// echo '<pre>';print_r($drugtot);
//echo '<pre>';print_r($deliveriesArray);exit;
$dateofinfo = date("m/d/Y", strtotime($dateofinfo));
// echo '<pre>';print_r($coin_data)."<br>";
// echo '<pre>';print_r($currency_data)."<br>";
// echo '<pre>';print_r($other_data);exit;
//echo $dateofinfo;exit;
if($coin_data)
{
   $coin_total = $coin_data[0]->pennies_box + $coin_data[0]->pennies_roll + $coin_data[0]->nickles_box + $coin_data[0]->nickles_roll + $coin_data[0]->dimes_box  + $coin_data[0]->dimes_roll + $coin_data[0]->quarters_box + $coin_data[0]->quarters_roll + $coin_data[0]->misc_box + $coin_data[0]->misc_roll;
}
if($currency_data)
{
    $currency_total = $currency_data[0]->ones_box + $currency_data[0]->ones_roll + $currency_data[0]->ones_held + $currency_data[0]->fives_box + $currency_data[0]->fives_roll + $currency_data[0]->fives_held + $currency_data[0]->tens_box + $currency_data[0]->tens_roll + $currency_data[0]->tens_held + $currency_data[0]->twenties_box
   + $currency_data[0]->twenties_roll + $currency_data[0]->twenties_held + $currency_data[0]->misc1_box + $currency_data[0]->misc1_roll + $currency_data[0]->misc1_held + $currency_data[0]->misc2_box + $currency_data[0]->misc2_roll + $currency_data[0]->misc2_held;
}
   

  
if($other_data)
{
   //echo $currency_total;exit;
   $other_total = $other_data[0]->food_stamps + $other_data[0]->merch_fives + $other_data[0]->merch_tens + $other_data[0]->merch_twenties + $other_data[0]->merch_misc + $other_data[0]->script1 + $other_data[0]->script2 + $other_data[0]->script3 + $other_data[0]->script4 + $other_data[0]->script5 + $other_data[0]->script6 + $other_data[0]->script7 
   + $other_data[0]->script8 + $other_data[0]->script9 + $other_data[0]->script10 + $other_data[0]->begin_loans + $other_data[0]->misc1_amt + $other_data[0]->misc2_amt + $other_data[0]->instr_chrg + $other_data[0]->pay_adv + $other_data[0]->night_crew + $other_data[0]->debit + $other_data[0]->credit + $other_data[0]->ebt + $other_data[0]->giftcard + $other_data[0]->misc3_amt;
   //echo $other_total;exit;
   }
   if($coin_data && $currency_data && $other_data)
   {
      $safe_count_total = $coin_total + $currency_total + $other_total;
      //echo $safe_count_total;exit;
   }
   else
   {
      $safe_count_total = 0;
     // echo $safe_count_total;exit;
   }
   //
?>


@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div id="safecountbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >SAFE REPORT  {{ $dateofinfo }}</div>

         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-postsafereportdetail')}}" id="coin-form" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return checkamounts()">
              <input type="hidden" name="entry_date" value="{{ $dateofinfo }}">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">(BS) Beginning Safe</label>
                  <div class="col-sm-4">
                     <input  type="button"  tabindex="4"  class="btn" style="width:189px;" value="0.00" id=""  name="beginningsafe">
                     </div>
                  <!-- <div class="col-sm-3">
                     <input type="button" class="btn" id="" placeholder="" value="show detail" onclick="hide_show_toggle_safecnt('coinbox','safecountbox')">
                  </div> -->
               </div>


               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">(DT) Deliveries & Transfers</label>
                  <div class="col-sm-4">
                     {{-- <input  type="text" placeholder="0.00" readonly="readonly"  class="form-control" id="showdandt_total"  name="dandt"> --}}
                     <input type="button" onclick="hide_show_toggle_safecnt('dandt','safecountbox')" id="showdandt_total" name="dandt" tabindex="4"  class="btn" style="width:189px;" value="0.00">
                     </div>
                 {{--  <div class="col-sm-3">
                     <input type="button" class="btn" id="" placeholder="" value="show detail" onclick="hide_show_toggle_safecnt('dandt','safecountbox')">
                  </div> --}}
               </div>
                <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">(DT) Deposits</label>
                  <div class="col-sm-4">
                     {{-- <input  type="text" placeholder="0.00" readonly="readonly"  class="form-control" id="deposits_total"  name="deposits"> --}}
                     <input type="button" onclick="hide_show_toggle_safecnt('deposits','safecountbox')" id="deposits_total" name="deposits" tabindex="4"  class="btn" style="width:189px;" value="0.00">
                     </div>
                  {{-- <div class="col-sm-3">
                     <input type="button" class="btn" id="" placeholder="" value="show detail" onclick="hide_show_toggle_safecnt('deposits','safecountbox')">
                  </div> --}}
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">(ST) Store Transactions</label>
                  <div class="col-sm-4">
                    {{--  <input  type="text" placeholder="0.00" readonly="readonly"  class="form-control" id="st_total"  name="st"> --}}
                     <input type="button" onclick="hide_show_toggle_safecnt('stbox','safecountbox')" id="st_total" name="st" tabindex="4"  class="btn" style="width:189px;" value="0.00">
                     </div>
                  {{-- <div class="col-sm-3">
                     <input type="button" class="btn" id="" placeholder="" value="show detail" onclick="hide_show_toggle_safecnt('stbox','safecountbox')">
                  </div> --}}
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">(PO) Paid Out Types</label>
                  <div class="col-sm-4">
                     {{-- <input  type="text" placeholder="0.00" readonly="readonly"  class="form-control" id="pot_total"  name="pot"> --}}
                     <input type="button" onclick="hide_show_toggle_safecnt('potbox','safecountbox')" id="pot_total" name="pot" tabindex="4"  class="btn" style="width:189px;" value="0.00">
                     </div>
                 {{--  <div class="col-sm-3">
                     <input type="button" class="btn" id="" placeholder="" value="show detail" onclick="hide_show_toggle_safecnt('potbox','safecountbox')">
                  </div> --}}
               </div>
               
               <hr style="border:1px solid #000;" />
               <div class="form-group">
               <!-- <div class="col-sm-3">&nbsp;</div> -->
                  <label for="inputPassword" class="control-label col-sm-6">(TS) Total to Account for</label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control finaltotal"  readonly="readonly" placeholder="0.00" id="final_total"  name="final_total">
                     </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <hr style="border:1px solid #000;" />
               <div class="form-group">
               <!-- <div class="col-sm-3">&nbsp;</div> -->
                  <label for="inputPassword" class="control-label col-sm-6">(CT) Safe Count Total</label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control"  readonly="readonly" placeholder="0.00" id="safe_count_total"  name="safe_count_total" step="0.01" value="{{ number_format($safe_count_total, 2, '.', '');}}">
                    
                     </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-6">(SA) Safe Over <<span>Short</span>></label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control"  readonly="readonly" placeholder="0.00" id="safe_over"  name="safe_over">
                     </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-6">(GO) Grocery Over <<span>Short</span>></label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control"  readonly="readonly" placeholder="0.00" id=""  name="groctot" value="{{ number_format($arrtot['groctot'], 2, '.', '');  }}">
                     </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-6">(DO) Drug Over <<span>Short</span>></label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control"  readonly="readonly" placeholder="0.00" id=""  name="drugtot" value="{{number_format($arrtot['drugtot'], 2, '.', ''); }}">
                     </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-6">STORE TOTAL OVER <<span>Short</span>></label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control"  readonly="readonly" placeholder="0.00" id=""  name="store_total_over">
                     </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center" style="margin-left:-31px;">
                        <input type="submit" name="safe-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" name="" id="submit" tabindex="4" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/booksafereport')}}'">
                     </div>
                  </div>
               </div>
               <!-- </form> -->
         </div>
      </div>
   </div>
   <!-- Deliveries and Transfer form Starts-->
   <div id="dandt" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >DELIVERY AND TRANSFER &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $dateofinfo }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <!-- <div class="form-group">
            <div class="col-sm-12">
               <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
            </div>
            
           
         </div> -->
         <div class="form-group">
            <div class="col-sm-4">
               <label for="inputPassword" class="control-label col-sm-3">Deliveries</label>
            </div>
            <div class="col-sm-4">
               {{-- <input  type="number" placeholder="0.00" readonly=""  class="form-control" id="deliv_total"  name=""> --}}
               <input type="button" onclick="hide_show_toggle_safecnt('deliverybox','dandt')" id="deliv_total" name="" tabindex="4" class="btn" style="width:189px;" value="0.00">
            </div>
           {{--  <div class="col-sm-4">
                     <input type="button" class="btn" id="" placeholder="" value="show detail" onclick="hide_show_toggle_safecnt('deliverybox','dandt')">
                  </div> --}}
         </div>
         <br><br>
         <div class="form-group">
            <div class="col-sm-4">
               <label for="inputPassword" class="control-label col-sm-3">Transfers</label>
            </div>
            <div class="col-sm-4">
               {{-- <input  type="number" placeholder="0.00" readonly="" class="form-control" id="transfer_total"  name=""> --}}
               <input type="button" onclick="hide_show_toggle_safecnt('transfersbox','dandt')" id="transfer_total" name="" tabindex="4" class="btn" style="width:189px;" value="0.00">
            </div>
            {{-- <div class="col-sm-4">
                     <input type="button" class="btn" id="" placeholder="" value="show detail" onclick="hide_show_toggle_safecnt('transfersbox','dandt')">
                  </div> --}}
         </div>
         <br><br>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-3" style="padding-left: 40px;">Total</label>
            <div class="col-sm-7" style="padding-left: 56px;  ">
               <input  class="form-control dandttotal col-sm-12" style="width:189px;"  readonly="readonly" placeholder="0.00" id="dandt_total"  name="">
               </div>
         </div>
         <div class="form-group">
            <div class="row " style="padding-left: 30px;">
               <div class="col-sm-12" align="center">
                  <input type="button" name="" id="dandt_submit" tabindex="4" value="Submit" class="btn" onclick="cancel_click('dandt','safecountbox')">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('dandt','safecountbox')">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
         <!--   </form> -->
      </div>
   </div>
</div>
   <!-- Deliveries and Transfer form Ends -->

<div id="deliverybox"  class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >DELIVERIES  &nbsp;&nbsp;&nbsp;&nbsp; {{ $dateofinfo }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
        <!--  <div class="form-group">
           <div class="col-sm-12">
              <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
           </div>
         </div> -->
         
          <div class="form-group">
            <div class="col-sm-2">
               (&nbsp;&nbsp;D1&nbsp;&nbsp;)
               <input type="hidden" name="deliver_item_id[]" value="D1">
            </div>
            <div class="col-sm-6">
               #1 MONEY DELIVE
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control delivery dandt final" id=""  name="deliver_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-2">
               (&nbsp;&nbsp;D2&nbsp;&nbsp;)
               <input type="hidden" name="deliver_item_id[]" value="D2">
            </div>
            <div class="col-sm-6">
               #2 MONEY DELIVE
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control delivery dandt final" id=""  name="deliver_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-2">
               (&nbsp;&nbsp;D3&nbsp;&nbsp;)
               <input type="hidden" name="deliver_item_id[]" value="D3">
            </div>
            <div class="col-sm-6">
               #3 MONEY DELIVE
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control delivery dandt final" id=""  name="deliver_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-2">
               (&nbsp;&nbsp;D4&nbsp;&nbsp;)
               <input type="hidden" name="deliver_item_id[]" value="D4">
            </div>
            <div class="col-sm-6">
               #4 MONEY DELIVE
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control delivery dandt final" id=""  name="deliver_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-2">
               (&nbsp;&nbsp;D5&nbsp;&nbsp;)
               <input type="hidden" name="deliver_item_id[]" value="D5">
            </div>
            <div class="col-sm-6">
               #5 MONEY DELIVE
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control delivery dandt final" id="" name="deliver_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-2">
               (&nbsp;&nbsp;D6&nbsp;&nbsp;)
               <input type="hidden" name="deliver_item_id[]" value="D6">
            </div>
            <div class="col-sm-6">
               #6 MONEY DELIVE
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control delivery dandt final" id="" name="deliver_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-2">
               (&nbsp;&nbsp;D7&nbsp;&nbsp;)
               <input type="hidden" name="deliver_item_id[]" value="D7">
            </div>
            <div class="col-sm-6">
               #7 MONEY DELIVE
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control delivery dandt final" id="" name="deliver_item_amt[]">
            </div>
         </div>
         <hr style="border:1px solid #000;">
            <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-3" style="text-align: right;">Total</label>
            <div class="col-sm-7" style="padding-left: 37px;">
               <input class="form-control deliverytotal" readonly="readonly" placeholder="0.00" id="deliverytotal" name="">
                           </div>
         </div>


         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" name="" id="delivery_submit" tabindex="4" value="Submit" class="btn" onclick="cancel_click('deliverybox','dandt')">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('deliverybox','dandt')">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
         <!--   </form> -->
      </div>
   </div>
</div>







<div id="transfersbox" class="mainbox col-md-8 col-md-offset-2 col-sm-9 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >TRANSFERS &nbsp;&nbsp;&nbsp;&nbsp;  {{ $dateofinfo }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
       <div class="row">
         <div class="form-group">
            <div class="col-sm-2" style="padding-left: 26px;">
               <label for="inputPassword" class="control-label col-sm-12">ID</label>
            </div>
            <div class="col-sm-4" style="padding-left: 45px;">
               <label for="inputPassword" class="control-label col-sm-12">Description</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-12">Trf Amount</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-12">ID Number</label>
            </div>
         </div>
         </div>
         <div class="row">
         <div class="form-group">
            <div class="col-sm-2">
               <label for="inputPassword" class="control-label col-sm-12">(&nbsp;&nbsp;T1&nbsp;&nbsp;)</label>
               <input type="hidden" name="transfer_item_id[]" value="T1">
            </div>
            <div class="col-sm-4">
               <label for="" class="control-label col-sm-12">GIFT CARD TSFR #1</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12 transfer dandt final" id=""  name="transfer_item_amt[]">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12" id=""  name="transfer_source_id[]">
            </div>
         </div>
         </div>
         <div class="row">
         <div class="form-group">
            <div class="col-sm-2">
               <label for="inputPassword" class="control-label col-sm-12">(&nbsp;&nbsp;T2&nbsp;&nbsp;)</label>
               <input type="hidden" name="transfer_item_id[]" value="T2">
            </div>
            <div class="col-sm-4">
               <label for="" class="control-label col-sm-12">GIFT CARD TSFR #2</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12 transfer dandt final" id=""  name="transfer_item_amt[]">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12" id=""  name="transfer_source_id[]">
            </div>
         </div>
         </div>
         <div class="row">
         <div class="form-group">
            <div class="col-sm-2">
               <label for="inputPassword" class="control-label col-sm-12">(&nbsp;&nbsp;T3&nbsp;&nbsp;)</label>
               <input type="hidden" name="transfer_item_id[]" value="T3">
            </div>
            <div class="col-sm-4">
               <label for="" class="control-label col-sm-12">GIFT CARD TSFR #3</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12 transfer dandt final" id=""  name="transfer_item_amt[]">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12" id=""  name="transfer_source_id[]">
            </div>
         </div>
         </div>
         <div class="row">
         <div class="form-group">
            <div class="col-sm-2">
               <label for="inputPassword" class="control-label col-sm-12">(&nbsp;&nbsp;T4&nbsp;&nbsp;)</label>
               <input type="hidden" name="transfer_item_id[]" value="T4">
            </div>
            <div class="col-sm-4">
               <label for="" class="control-label col-sm-12">GIFT CARD TSFR #4</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12 transfer dandt final" id=""  name="transfer_item_amt[]">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12" id=""  name="transfer_source_id[]">
            </div>
         </div>
         </div>
         <div class="row">
         <div class="form-group">
            <div class="col-sm-2">
               <label for="inputPassword" class="control-label col-sm-12">(&nbsp;&nbsp;T5&nbsp;&nbsp;)</label>
               <input type="hidden" name="transfer_item_id[]" value="T5">
            </div>
            <div class="col-sm-4">
               <label for="" class="control-label col-sm-12">GIFT CARD TSFR #5</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12 transfer dandt final" id=""  name="transfer_item_amt[]">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12" id=""  name="transfer_source_id[]">
            </div>
         </div>
         </div>
         <div class="row">
         <div class="form-group">
            <div class="col-sm-2">
               <label for="inputPassword" class="control-label col-sm-12">(&nbsp;&nbsp;T6&nbsp;&nbsp;)</label>
               <input type="hidden" name="transfer_item_id[]" value="T6">
            </div>
            <div class="col-sm-4">
               <label for="" class="control-label col-sm-12">GIFT CARD TSFR #6</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12 transfer dandt final" id=""  name="transfer_item_amt[]">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12" id=""  name="transfer_source_id[]">
            </div>
         </div>
         </div>
         <div class="row">
         <div class="form-group">
            <div class="col-sm-2">
               <label for="inputPassword" class="control-label col-sm-12">(&nbsp;&nbsp;T7&nbsp;&nbsp;)</label>
               <input type="hidden" name="transfer_item_id[]" value="T7">
            </div>
            <div class="col-sm-4">
               <label for="" class="control-label col-sm-12">GIFT CARD TSFR #7</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12 transfer dandt final" id=""  name="transfer_item_amt[]">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control col-sm-12" id=""  name="transfer_source_id[]">
            </div>
         </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4" style="text-align: right;">Grand Total</label>
            <div class="col-sm-6" style="padding-left: 30px;">
               <input  class="form-control transfertotal"  readonly="readonly" placeholder="0.00" id="transfertotal"  name="">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center" style="margin-left: 25px">
                  <input type="button" name="" id="transfersbox_submit" tabindex="4" value="Submit" class="btn" onclick="cancel_click('transfersbox','dandt')">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('transfersbox','dandt')">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
         <!--   </form> -->
      </div>
   </div>
</div>





<div id="deposits" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >DEPOSITS  &nbsp;&nbsp;&nbsp;&nbsp; {{ $dateofinfo }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <!-- <div class="form-group">
            <div class="col-sm-12">
               <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
            </div>
          </div> -->
         
          <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;K1&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="K1">
            </div>
            <div class="col-sm-7">
              &nbspCHECK DEP #1
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;K2&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="K2">
            </div>
            <div class="col-sm-7">
              &nbspCHECK DEP #2
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;C1&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="C1">
            </div>
            <div class="col-sm-7">
               &nbsp#1 CURRENCY DEP
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;C2&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="C2">
            </div>
            <div class="col-sm-7">
               &nbsp#2 CURRENCY DEP
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;C3&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="C3">
            </div>
            <div class="col-sm-7">
              &nbsp#3 CURRENCY DEP
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;C4&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="C4">
            </div>
            <div class="col-sm-7">
              &nbsp#4 CURRENCY DEP
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;DD&nbsp;&nbsp;)
              <input type="hidden" name="deposits_item_id[]" value="DD"> 
            </div>
            <div class="col-sm-7">
               &nbspDEBIT EFT DEP 
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;ZD&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="ZD">
            </div>
            <div class="col-sm-7">
               &nbspDISCOVER DEP 
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>

         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;CD&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="CD">
            </div>
            <div class="col-sm-7">
                &nbspMC/VISA DEPOSIT
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;AX&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="AX">
            </div>
            <div class="col-sm-7">
               &nbspXAMER EXP DEP
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;DI&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="DI">
            </div>
            <div class="col-sm-7">
               &nbspEBT-WIC
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;ED&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="ED">
            </div>
            <div class="col-sm-7">
               &nbspEBT-CASH/FS
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;F2&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="F2">
            </div>
            <div class="col-sm-7">
               &nbspSUN TRUST MC/VI
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">
               (&nbsp;&nbsp;E9&nbsp;&nbsp;)
               <input type="hidden" name="deposits_item_id[]" value="E9">
            </div>
            <div class="col-sm-7">
               &nbspE-CHECK
            </div>
            <div class="col-sm-4">
               <input  type="number" placeholder="0.00"  class="form-control deposits final" id=""  name="deposits_item_amt[]">
            </div>
         </div>
         <hr style="border:1px solid #000;">
               
            <div class="form-group">
            <label for="inputPassword" class="control-label col-md-4">Total to account for</label>
            <div class="col-md-6">
               <input class="form-control depositstotal" readonly="readonly" placeholder="0.00" id="depositstotal" name="">
                           </div>
         </div>


         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" style="padding-left:45px;" align="center">
                  <input type="button" name="" id="deposits_submit" tabindex="4" value="Submit" class="btn" onclick="cancel_click('deposits','safecountbox')">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('deposits','safecountbox')">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
         <!--   </form> -->
      </div>
   </div>
</div>



<div id="stbox" style="margin-left: 100px;" class="mainbox miscbox col-md-10">
   <div class="panel panel-info safe_total">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >STORE TRANSACTIONS &nbsp;&nbsp;&nbsp;&nbsp;{{ $dateofinfo }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <div class="row vertical-divider" style="margin-top: 10px">
            <div class="col-xs-6" >
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;55&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="55">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">CRV PAID OUT CU</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;86&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="86">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">CHECK FREE PAY</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="text" placeholder="0.00" class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;AR&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="AR">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">AR ELECTRONIC C</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00" class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;CS&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="CS">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">COINSTAR REDEMP</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;EE&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="EE">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">EPS ERRORS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;GE&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="GE">
                  </div>
                  <div class="col-xs-6 ">
                     <label for="inputPassword" class="control-label">GIFT CARD TRANS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <br />
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;L9&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="L9">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">LOTTERY O/S</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;NV&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="NV">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">NCH E-COUPON</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OB&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OB">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">POSTAL STATION</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OD&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OD">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">BLACKHAWK GC O/</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OG&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OG">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">VISA GC O/S</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OI&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OI">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RALEYS GIFT CAR</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OK&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OK">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">POSTAGE STAMP O</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OT&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OT">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">MASTERCARD GC O</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;PG&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="PG">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">PARTNER COINSTA</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RC&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="RC">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">TENDER POINTS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;TD&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="TD">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">TOTAL DEBITS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;VR&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="VR">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">VENDOR REWARDS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;XP&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="XP">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">EPS TO OFFICE</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;56&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="56">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">CRV RCVD FR VEN</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
                <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;AN&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="AN">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">AUTHORIZE .NET P</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;BZ&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="BZ">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">PAPER AR CHARGE</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;CV&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="CV">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">COOP E-COUPON</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <br />
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;FC&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="FC">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">FS GIFT CERTS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;HD&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="HD">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">PAPER GIFT CERT</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;LR&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="LR">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">LOANS TO REGIST</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;09&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="09">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">E-CHECK O/S</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OC&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OC">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">AMEX GC O/S</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OE&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OE">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">INCOMM GC O/S</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OH&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OH">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">MONEY ORDER/GRA</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OJ&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OJ">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">COMMUTER O/S</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OO&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="OO">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">UTILITY PMT O/S</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;PC&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="PC">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">TP SALES</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;R5&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="R5">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">CRM EARNED</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RY&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="RY">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RECYCLING</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;VC&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="VC">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">VENDOR COUPONS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;WC&nbsp;&nbsp;)</label>
                     <input type="hidden" name="store_item_id[]" value="WC">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">WIC O/S</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control st final" id=""  name="store_item_amt[]">
                  </div>
               </div>
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-3"> Total to account for</label>
            <div class="col-sm-3">
               <input  class="form-control sttotal"  readonly="readonly" placeholder="0.00" id="sttotal"  name="">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" style="padding-left: 260px;">
                  <input type="button" name="" id="st_submit" tabindex="4" value="Submit" class="btn" onclick="cancel_click('stbox','safecountbox')">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('stbox','safecountbox')">
               </div>
            </div>
         </div>
         </div>
         </div>
         </div>








  <div id="potbox" style="margin-left: 100px;" class="mainbox miscbox col-md-10">
   <div class="panel panel-info safe_total">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >PAID OUT TYPES &nbsp;&nbsp;&nbsp;&nbsp;{{ $dateofinfo }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <div class="row vertical-divider" style="margin-top: 10px">
            <div class="col-xs-6" >
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;AB&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="AB">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">CUSTOMER RELATI</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;CX&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="CX">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RX GC PROMO</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00" class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;ER&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="ER">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">COLLEAGUE RELAT</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00" class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;GA&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="GA">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">GARBAGE</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;M1&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="M1">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">MONEY TRANSFERS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OM&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="OM">
                  </div>
                  <div class="col-xs-6 ">
                     <label for="inputPassword" class="control-label">INSTORE PROMO-M</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <br />
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;P1&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="P1">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">SPECIAL SERVICE</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;PZ&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="PZ">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">PROMOTION-C.R</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RB&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RB">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - BAKERY</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RE&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RE">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - WALL D</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RG&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RG">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - GROCER</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RM&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RM">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - MEAT P</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RO&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RO">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - P STAM</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RQ&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RQ">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - LIQUOR</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;SG&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="SG">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">SUSPENCE - CLEARI</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;SM&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="SM">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">OTHER SUPPLY</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;TV&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="TV">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">TRAVEL</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;WI&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="WI">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">WINE TASTING EX</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;CB&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="CB">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">COMMUNITY EVENT</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
                <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;EA&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="EA">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">CLEANING SUPPLI</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;FU&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="FU">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">FURN, FIXT, EQU</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;GH&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="GH">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">GIFT CARD DISCO</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <br />
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;MM&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="MM">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">MAINTAINCE</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;OZ&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="OZ">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">OFFICE SUPPLIES</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;PV&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="PV">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">PAYROLL PAY-OUT</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RA&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RA">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - RX P/O</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RD&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RD">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - DRUG P</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RF&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RF">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - FSD P/</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RH&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RH">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - HOT WO</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RN&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RN">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - WELLNE</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RP&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RP">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - PRODUCT</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;RS&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="RS">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">RESALE - FLORAL</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;SL&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="SL">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">SPECIAL PROMOTI</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;T9&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="T9">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">TAX & LICENCE F</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
               <div class="form-group">
               <div class="col-xs-2">
                     <label for="inputPassword" class="control-label col-sm-3">(&nbsp;&nbsp;UI&nbsp;&nbsp;)</label>
                     <input type="hidden" name="pot_item_id[]" value="UI">
                  </div>
                  <div class="col-xs-6">
                     <label for="inputPassword" class="control-label">UNIFORMS</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control pot final" id=""  name="pot_item_amt[]">
                  </div>
               </div>
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-3">Total to account for</label>
            <div class="col-sm-3">
               <input  class="form-control pottotal"  readonly="readonly" placeholder="0.00" id="pottotal"  name="">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-8" style="margin-left: 243px;">
                  <input type="button" name="" id="pot_submit" tabindex="4" value="Submit" class="btn" onclick="cancel_click('potbox','safecountbox')">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('potbox','safecountbox')">
               </div>
            </div>
         </div>
         </div>
         </div>
         </div>












         </form>
      </div>
   </div>
</div>






   

</div>
@stop
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>
<script>
   $(document).ready(function() {
      $('#dandt').hide();
      $('#deliverybox').hide();
      $('#transfersbox').hide();
      $('#deposits').hide();
      $('#coinbox').hide();
      $('#currencybox').hide();
      $('#stbox').hide();
      $('#potbox').hide();
   });
  
   function hide_show_toggle_safecnt(pop_div,main_div)
   {
    //alert('hiii');
    $('#'+pop_div).show();
    $('#'+main_div).hide();
   }
   function cancel_click(pop_div,main_div)
   {
    $('#'+pop_div).hide();
    $('#'+main_div).show();
   }
   //alert("Hiii");
</script>
<script type="text/javascript" src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>

<!-- Sravan Kumar Sriramula Code js Code -->
<script type="text/javascript">  
$(document).ready(function() {
     $("#dandt_submit").click(function (e) { 
            showdandt_total=$('#dandt_total').val();
            $('#showdandt_total').val(showdandt_total);
            return false;

      });

     $("#delivery_submit").click(function (e) { 
         deliv_total=$('#deliverytotal').val();
         $('#deliv_total').val(deliv_total);
         return false;
   });


     $("#transfersbox_submit").click(function (e) { 
         transfer_total=$('#transfertotal').val();
         $('#transfer_total').val(transfer_total);
         return false;
   });

     $("#deposits_submit").click(function (e) { 
         deposits_total=$('#depositstotal').val();
         $('#deposits_total').val(deposits_total);
         return false;
   });

     $("#st_submit").click(function (e) { 
         st_total=$('#sttotal').val();
         $('#st_total').val(st_total);
         return false;
   });
     $("#pot_submit").click(function (e) { 
         pot_total=$('#pottotal').val();
         $('#pot_total').val(pot_total);
         return false;
   });
});
</script>


<script type="text/javascript">
   /* Delivery Sum */
   $(document).ready(function() {
    $(".delivery").each(function() {
         $(this).keyup(function() {
         calculateDeliverySum();
        });
    });
});

function calculateDeliverySum() {
    var sum = 0;
    $(".delivery").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".deliverytotal").val(sum.toFixed(2));
 }


 /* Transfer Sum */
 $(document).ready(function() {
    $(".transfer").each(function() {
         $(this).keyup(function() {
         calculateTransferSum();
        });
    });
});

function calculateTransferSum() {
    var sum = 0;
    $(".transfer").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".transfertotal").val(sum.toFixed(2));
 }

 /* DandT Sum */
 $(document).ready(function() {
    $(".dandt").each(function() {
         $(this).keyup(function() {
         calculateDandTSum();
        });
    });
});

function calculateDandTSum() {
    var sum = 0;
    $(".dandt").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".dandttotal").val(sum.toFixed(2));
 }


 /* Deposits Sum */
 $(document).ready(function() {
    $(".deposits").each(function() {
         $(this).keyup(function() {
         calculateDepositsSum();
        });
    });
});

function calculateDepositsSum() {
    var sum = 0;
    $(".deposits").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".depositstotal").val(sum.toFixed(2));
 }


 /* Store Transactions Sum */
 $(document).ready(function() {
    $(".st").each(function() {
         $(this).keyup(function() {
         calculateSTSum();
        });
    });
});

function calculateSTSum() {
    var sum = 0;
    $(".st").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".sttotal").val(sum.toFixed(2));
 }


 /* Paid Out Types Sum */
 $(document).ready(function() {
    $(".pot").each(function() {
         $(this).keyup(function() {
         calculatePOTSum();
        });
    });
});

function calculatePOTSum() {
    var sum = 0;
    $(".pot").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".pottotal").val(sum.toFixed(2));
 }


 /* Final Sum */
 $(document).ready(function() {
    $(".final").each(function() {
         $(this).keyup(function() {
         calculateFinalSum();
         var safe_over = $("#safe_count_total").val() - $("#final_total").val();
         $("#safe_over").val(safe_over);
        });
    });
});

function calculateFinalSum() {
    var sum = 0;
    $(".final").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
         
   });
   //   alert($("#safe_over").val());
    //alert($("#safe_count_total").val());
   //safe_over = safe_count_total - final_total
   
    $(".finaltotal").val(sum.toFixed(2));
 }
  $(document).ready(function() {
   //alert($("#safe_over").val());
   //alert($("#final_total").val());
   //safe_over = safe_count_total - final_total
   //var safe_count_total = $("#safe_over").val() - $("#final_total").val();
   //$("#safe_over").val(safe_count_total);

});
</script>



<style type="text/css">
   .row.vertical-divider {
   overflow: hidden;
   }
   .row.vertical-divider > div[class^="col-"] {
   text-align: left !important;
   padding-bottom: 100px;
   margin-bottom: -100px;
   border-left: 1px solid #000;
   border-right: 0px solid #000;
   }
   .row.vertical-divider div[class^="col-"]:first-child {
   border-left: none;
   }
   .row.vertical-divider div[class^="col-"]:last-child {
   border-right: none;
   }
   .misc_desc{
      display: initial !important;
   } 
   .misc1_desc{
      display: initial !important;
   }
   .misc2_desc{
      display: initial !important;
   }
   .safeothermisc1{
      display: initial !important;
   }
   .safeothermisc2{
      display: initial !important;
   }
   @media (min-width: 992px){
   .miscbox.safe_total{
       margin-left: 91px !important;
   }
}
</style>