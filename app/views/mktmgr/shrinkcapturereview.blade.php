@extends('layout.dashboard')
@extends('layout.datejs')
@section('page_heading','Shrink Capture Review')
@section('content')
@section('section')

<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container" style="margin-top: 8%">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Shrink Capture Review Store {{{ Session::get('storeid') }}}</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-post-shrink-review')}}" id="booksafedate" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="row">
                  <div class="form-group">
                     <!-- <div class="col-sm-6"> -->
                     <label style="padding-left:80px;padding-top:0px;" for="inputPassword" class="control-label col-sm-5">Shrink Date</label>
                     <!-- </div> -->
                     <div class="col-sm-7">
                        <!-- <input type="date" class="form-control" id="info_date" name="info_date"{{ (Input::old('info_date'))?'
                           value="'.Input::old('info_date').'"':''}} placeholder="MM-DD-YY" /> -->
                        <input type="date" placeholder="MM-DD-YYYY" name="shrink_date" id="" autocomplete="off" isimportant="true" class="form-control lastdate" value="{{ $previewsdate }}">
                        @if($errors->has('info_date'))
                        {{ $errors->first('info_date')}}
                        @endif       
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        {{-- <input type="reset" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn"> --}}
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{ URL::route('mktmgr-returnhome')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop