<?php 
    //echo $file_locked;exit;
?>

@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style type="text/css">
   .btn-width{
   width: 120px;  
   text-align: right;
   }
   .side-space{
   margin-left: 17px;
   }
</style>
<?php $sale_date = date("m/d/Y", strtotime($sales_date));?>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <!------------------- First Sales popup---------------------------->
   <div id="depsalesbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Department Sales &emsp;{{$sale_date}}</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form  action="{{URL::route('bookspro-salesdata')}}"  id="" class="form-horizontal" method="post" role="form" style="display: block;">
               {{-- hidden input field --}}
               <input type="hidden" value ="{{$sales_date}}" name ="sales_date"/>
               <input type="hidden" value ="{{$per}}" name ="period"/>
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <div class="col-sm-1 side-space">&nbsp;</div>
                  <label  class="control-label col-sm-5">Customer Cnt</label>
                  @if($file_locked == "Y")
                  <div class="col-sm-5 text-right" style="margin-left: -94px;">
                     <strong><span>{{$mainsale_rec['custcnt']}}</span></strong>
                  </div>
                  @else
                  <div class="col-sm-5 ">
                     <input  type="text"  tabindex="2" class="form-control btn-width text-right" autofocus="" value="{{$mainsale_rec['custcnt']}}"  id="customercnt" placeholder="0"  name="customercnt" onKeyPress="return StopNonNumeric(this,event)"/>
                  </div>
                  @endif
               </div>
               <div class="form-group">
                  <div class="col-sm-1 side-space">&nbsp;</div>
                  <label  class="control-label col-sm-5">Competitor Coupon</label>
                  @if($file_locked == "Y")
                  <div class="col-sm-5 text-right" style="margin-left: -94px;">
                     <strong><span>{{ number_format((float)$mainsale_rec['comp_coupon']/100, 2, '.', ''); }}</span></strong>
                  </div>
                  @else
                  <div class="col-sm-5">
                     <input  type="text" tabindex="3" class="form-control dec btn-width text-right"  value="{{ number_format((float)$mainsale_rec['comp_coupon']/100, 2, '.', ''); }}"  id="comptitorcoupon" placeholder="0.00" name="comptitorcoupon" />
                  </div>
                  @endif
               </div>
               <div class="form-group">
                  <div class="col-sm-1 side-space">&nbsp;</div>
                  <label  class="control-label col-sm-5">Total Sales</label>
                  <div class="col-sm-5">
                     <input  type="button" tabindex="4"  value="<?php if($mainsale_rec['tot_sales']) { echo $mainsale_rec['tot_sales']/100;} else{ echo '0.00';} ?>"  class="btn btn-width text-right" id="totsale"  onclick="hide_show_toggle_safecnt('salesbox','depsalesbox')" name="totsale"/>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-1 side-space">&nbsp;</div>
                  <label  class="control-label col-sm-5">Adjustments</label>
                  <div class="col-sm-5 ">
                     <input  type="button" tabindex="5"  value="<?php if($mainsale_rec['adj']) { echo number_format((float)$mainsale_rec['adj']/100, 2, '.', '');} else{ echo '0.00';} ?>"  class="btn btn-width " id="adj_total"  name="adj_total" onclick="hide_show_toggle_safecnt('adjustmentbox','depsalesbox')"  />
                  </div>
               </div>
               <hr style="border:1px solid #000;" />
               <div class="row">
                  <div class="col-sm-4">&nbsp;</div>
                  <div class="col-sm-6">
                     <b>TOTAL ADJ SALES .. &nbsp; </b><strong><span id="grand_total"><?php if($tot_adjsales) { echo number_format((float)$tot_adjsales/100, 2, '.', ''); } 
                        else { echo '0.00'; } ?></span></strong>
                  </div>
                  <!-- <div class="col-sm-1">&nbsp;</div> -->
               </div>
               <br>
               <div class="row">
                  <div class="col-sm-6">&nbsp;</div>
                  <div class="col-sm-4">
                     <input type="submit" name="depsalessubmit" tabindex="6" id="depsalessubmit" value="Submit" class="btn">
                     <input type="button" id="cancel" value="Cancel" tabindex="7" class="btn" onClick="document.location.href='{{URL::to('mktmgr/wklysalesdate')}}'" />
                  </div>
                  <div class="col-sm-1">&nbsp;</div>
               </div>
               <br>
               <div class="focusguard" id="focusguard-2" tabindex="8"></div>
            </form>
         </div>
      </div>
   </div>
   <!------------------- Ends Sales popup---------------------------->
   <!------------------- Department Sales popup---------------------------->
   <div id="salesbox" class="mainbox col-md-12" style="display: none;overflow: hidden;">
      <form method="post" id="dept-sales-form">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Department Sales  </div>
            </div>
            <div class="row">
               <div  class="panel-body" style="margin-top: 10px;">
                  <div class="col-xs-6"  >
                     <div class="form-horizontal">
                        <div class="form-group">
                           <div class="col-xs-5">
                              <label for="inputPassword" style="margin-left: 30px;" class="control-label col-sm-12">DEPARTMENT</label>
                           </div>
                           <div class="col-sm-4">
                              <label for="inputPassword" style="text-align: right;margin-left: 10px;" class="control-label col-sm-12">SALES</label>
                           </div>
                           <div class="col-sm-3">
                              <label for="inputPassword" style="text-align: center;margin-left: -5px" class="control-label col-sm-10">CUST CT</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6"  >
                     <div class="form-horizontal">
                        <div class="form-group">
                           <div class="col-xs-5">
                              <label for="inputPassword" style="margin-left: 14px;" class="control-label col-sm-12">DEPARTMENT</label>
                           </div>
                           <div class="col-sm-4">
                              <label for="inputPassword" style="text-align: right;margin-left: 9px;" class="control-label col-sm-11">SALES</label>
                           </div>
                           <div class="col-sm-3">
                              <label for="inputPassword" style="text-align: left;margin-left: -18px" class="control-label col-sm-10">CUST CT</label>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div  class="panel-body" style="margin-top: 10px;">
               <?php for($i = 0; $i < count($sales_entry); $i++)
                  {
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-6">
                        <label for="inputPassword" class="control-label col-sm-12">
                        <?php echo $sales_entry[$i]['item_desc'];?></label>
                     </div>
                     <div class="col-sm-3 text-right">
                        <span><b><?php if($sales_entry[$i]['dept_sales']) { echo number_format( (float)$sales_entry[$i]['dept_sales'], 2, '.', ''); } else { echo '0.00'; } ?></b></span>
                     </div>
                     <div class="col-sm-2 text-right">
                        <span>
                        <b>
                        <?php if($sales_entry[$i]['cust_cnt']) { echo $sales_entry[$i]['cust_cnt']; } else { echo '0'; } ?>
                        </b>      
                        </span>
                     </div>
                  </div>
               </div>
               <?php
                  }
                  ?>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <label for="inputPassword" style="text-align: right;" class="control-label col-sm-6">Total Sales :</label>
               <div class="col-sm-3">
                  <span id="tot_sales"><b><?php if($mainsale_rec['tot_sales']){echo $mainsale_rec['tot_sales']/100;} else{ echo '0.00';}?></b></span>
                  @if($errors->has(''))
                  {{ $errors->first('')}}
                  @endif
               </div>
            </div>
            <!--  <div class="focusguard" id="focusguard-1" tabindex="1"></div> -->
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" style="margin-left: 42%">
                     <input type="button" id="deptsalessubmit" value="Submit" class="btn" onclick="cancel_click('salesbox','depsalesbox')">
                     <input type="reset" id="reset"  value="Cancel" class="btn" 
                        onclick="cancel_click('salesbox','depsalesbox')">       
                  </div>
               </div>
            </div>
            <!-- <div class="focusguard" id="focusguard-2" tabindex="4"></div> -->
         </div>
      </form>
   </div>
   <!-------------------Ends Department Sales popup---------------------------->
   <!------------------- Adjustment Sales popup---------------------------->
   <div id="adjustmentbox" class="mainbox col-md-12" style="display: none;overflow: hidden;">
      <form method="post" id="adj-sales-form">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >DEPARTMENT SALES ADJUSTMENTS &nbsp;&nbsp;<?php echo $sale_date ;?></div>
            </div>
            <div class="row">
               <div  class="panel-body" style="margin-top: 10px;">
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <div class="col-xs-4">
                              <label for="inputPassword" style="margin-left: 27px;" class="control-label col-sm-12">DEPARTMENT</label>
                           </div>
                           <div class="col-sm-4">
                              <label for="inputPassword" style="text-align: right;" class="control-label col-sm-12">SALES</label>
                           </div>
                           <div class="col-sm-4">
                              <label for="inputPassword" style="text-align: right" class="control-label col-sm-10">CUST CT</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6"  >
                     <div class="form-horizontal">
                        <div class="form-group">
                           <div class="col-xs-4">
                              <label for="inputPassword" style="margin-left: 14px;" class="control-label col-sm-12">DEPARTMENT</label>
                           </div>
                           <div class="col-sm-4">
                              <label for="inputPassword" style="text-align: right;" class="control-label col-sm-12">SALES</label>
                           </div>
                           <div class="col-sm-4">
                              <label for="inputPassword" style="text-align: right" class="control-label col-sm-10">CUST CT</label>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div  class="panel-body" style="margin-top: 10px;">
               <?php for($i = 0; $i < count($adj_entry); $i++)
                  {
                     $salesid='salesid'.$i; $custcnt = 'custcntid'.$i; $itemidcnt = 'itemidcntid'.$i;
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-6">
                        <label for="inputPassword" class="control-label col-sm-12"><?php echo $adj_entry[$i]['item_desc'];?></label>
                     </div>
                     <!-- <input type="hidden" id="{{ $itemidcnt }}" name="{{ $itemidcnt }}" value="{{$adj_entry[$i]['item_id'];}}"> -->
                     @if($file_locked == "Y")
                     <div class="col-sm-3 text-right" style="margin-left: -53px;">
                        <strong><span>{{ number_format((float)$adj_entry[$i]['dept_sales']/100, 2, '.', ''); }}</span></strong>
                     </div>
                     <div class="col-sm-3 text-right">
                        <strong><span>{{ $adj_entry[$i]['cust_cnt']  }}</span></strong>
                     </div>
                     @else
                     <div class="col-sm-3">
                        <input type="text" class="form-control caltot text-right" id="{{ $salesid }}"  name="<?php echo $adj_entry[$i]['item_id'];?>" value="<?php if($adj_entry[$i]['dept_sales']) 
                           { echo number_format( (float)$adj_entry[$i]['dept_sales']/100, 2, '.', '');} else { echo '0.00'; } ?>" style="width: 100%" onKeyPress="return StopNonNumeric(this,event)"/>
                     </div>
                     <div class="col-sm-3">
                        <input type="text" class="form-control text-right" name="<?php echo $adj_entry[$i]['item_id'];?>" value="<?php if($adj_entry[$i]['cust_cnt']) { echo $adj_entry[$i]['cust_cnt']; } else { echo '0'; } ?>" onKeyPress="return StopNonNumeric(this,event)"/>
                     </div>
                     @endif
                  </div>
               </div>
               <?php
                  }
                  ?>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <label for="inputPassword" style="text-align: right;margin-left:27px;" class="control-label col-sm-6">Total Sales</label>
               <div class="col-sm-3">
                  <b><span id="adjustments"><?php if($mainsale_rec['adj']){ echo $mainsale_rec['adj']/100;} else {'0.00';}?></span></b>
                  <!-- <input  class="form-control "  readonly="readonly" placeholder="0.00" id=""  
                     name="misc_grand_total" value=""> -->
                  @if($errors->has(''))
                  {{ $errors->first('')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" style="margin-left: 43%">
                     <input type="button" <?php if($file_locked == "Y"){ echo 'disabled'; } ?> id="adjsalessubmit" tabindex="2" value="Submit" class="btn">
                     <input type="reset" id="reset" tabindex="3" value="Cancel" class="btn" 
                        onclick="cancel_click('adjustmentbox','depsalesbox')">       
                  </div>
               </div>
            </div>
            <br>
            <div class="row">
               <br>
               <div class="col-md-1">&nbsp;</div>
               <div class="col-md-10 text-center waitmessage" style="color:red;">
               </div>
               <div class="col-md-1">&nbsp;</div>
            </div>
            @if($file_locked == "Y")
            <div class="row">
               <div class="col-md-1">&nbsp;</div>
               <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
               <div class="col-md-1">&nbsp;</div>
            </div>
            @endif
            <br>
         </div>
      </form>
   </div>
</div>
<!-------------------Ends Adjustment Sales popup---------------------------->
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>
<script type="text/javascript">
   $(document).ready(function() {
     
        $('#customercnt').select();
        /*select functionality*/
      // $(':input').focus(function(event) {
      //    $('#customercnt').select();
      // });
   
       /*$('#focusguard-2').on('focus', function() {
       $('#deptsalessubmit').focus();
       });
   
       $('#focusguard-1').on('focus', function() {
       $('#reset').focus();
       });
   */
      $("#adj_total").click(function(event) {
         $('.waitmessage').hide();
      });
      $( "#adj_total" ).click(function() {
       $( ".caltot" ).first().focus();
     });
      
      $( "#totsale" ).click(function() {
       /*$( "#deptsalessubmit" ).focus();*/
     });
         /*Jquery post  insert or update the data */
       $("#adjsalessubmit").click(function(e){
           $('.waitmessage').show();
           var datastring = $("#adj-sales-form").serializeArray();
           
           var per = '{{$per}}';
          
           var sales_date = '{{$sales_date}}';
         
           $.ajax({
                        url: "adjsalessubmit",
                         type: "POST", 
                        data: { data: datastring,period:per,wk_end_date:sales_date},
                        //dataType:'json',
                        beforeSend: function () {
                            $('#adjsalessubmit').attr('disabled', 'disabled');
                            $('.waitmessage').html("<div class='recapmessage alert alert-info' role='alert'><strong>Please wait untill recap sales is proccessed...</strong></div>");
                            //return false;
   
                        },
                        success: function(data) {
   
                            $('#adjsalessubmit').removeAttr('disabled');
                          $('.waitmessage').html("<div class='recapmessage alert alert-success' role='alert'><strong>Recap sales value is added</strong></div>");
                           setTimeout(function() {cancel_click('adjustmentbox','depsalesbox') }, 1600);
                            
                        }
   
                      });
   
   
          /* $.post("adjsalessubmit",{data:datastring,period:per,wk_end_date:sales_date} ,function(data) {
       
           cancel_click('adjustmentbox','depsalesbox');
           });*/
        //   
          
     });
   
     
     /*focus input field and multiply by 100*/    
     $(".caltot,.dec").focus(function(event) {
     var sum = 0; 
     selfield = this.id;
     selfieldval=$("#"+selfield).val(); 
     $("#"+selfield).val(Math.round(selfieldval*100));
     this.select();
     });
   
   
       /*Competitor Coupon float value */
     $('.dec').blur(function(event) {
        selfield = this.id;
        selfieldval=$("#"+selfield).val();
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
     });
   
   
       /*Adjustment float value */
       $(".caltot").blur(function(){  
      var sum = 0; 
       selfield = this.id;
       selfieldval=$("#"+selfield).val();
       $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
       $('.caltot').each(function() { 
       sum += parseFloat($(this).val()); 
       });
       $("#adjustments").text(sum.toFixed(2));
       $("#adj_total").val(sum.toFixed(2));
       finaltot();
       return true;
       
     });
   
    
   });
   
     /*Total adj sales total value (Grand total)*/
     function finaltot()
     {
     tot_sales=$("#totsale").val();
     
     adjustments=$("#adj_total").val();
     gt = (parseFloat(tot_sales)+parseFloat(adjustments)).toFixed(2);
     
     $("#grand_total").text(gt);
     }  
   
   
     /*hide and show main div and pop div */
   function hide_show_toggle_safecnt(pop_div,main_div)
     {
        $('#'+pop_div).show();
        $('#'+main_div).hide();
     }
     function cancel_click(pop_div,main_div)
     {
        $('#'+pop_div).hide();
        $('#'+main_div).show();   
     }
   
      $(".form-control").keydown(function (e) {
   // Allow: backspace, delete, tab, escape, enter 
   
   if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl/cmd+A
       (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
       (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
       (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
       (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
   }

   // Ensure that it is a number and stop the keypress
   if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
       e.preventDefault();
   }

 
   });

     /*focus and tab functionality*/
     $('#focusguard-2').on('focus', function() {
     $('#customercnt').focus();
     });
   
     $('#focusguard-1').on('focus', function() {
     $('#cancel').focus();
     });
   
</script>
@stop