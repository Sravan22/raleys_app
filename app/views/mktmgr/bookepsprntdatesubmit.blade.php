@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
   .text-center
{
    text-align: center !important;
}
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php $wk_date = date("m/d/Y", strtotime($date));?>

<!-- container -->
<div class="container">

   
         
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Raley's / Bel Air
            <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
        
         </span></div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">EPS ERROR FORM</div>
         </div>
         <div class="col-md-4"></div>
         <div class="col-md-6" style=" text-align:center; font-weight:bold;">
            <div class="panel-title" style="width: 320px;"> Store Number : <?php echo Session::get('storeid');?> &nbsp;&nbsp;&nbsp; DATE :{{$wk_date}} </div>
         </div>
         
         
      </div>
   
<br>
@if(!empty($result_set))

<div style="padding-top:30px" class="panel-body">
   <div class="table-responsive">
      <table class="table table-bordered table-colored">
         <thead>
            <tr>
               <th class="text-center">DATE/DAY</th>
               <th class="text-center">
                  PREVIOUS OFFLINE TO CARRY OVER<br>(PREV SAFE CNT)
               </th>
               <th class="text-center">NET SALES<br>(STORE TOTALS REPORT)</th>
               <th class="text-center">CURRENT OFFLINE PENDING (OFFLINE PENDING RPT)</th>
               <th class="text-center">
                  OFFLINE REJ (OFFLINE REJ REPORT)
               </th>
               <th class="text-center">
                  HOST TOTAL
               </th>
               <th class="text-center">
                  HOST TOTAL (DAILY DEP SETTLEMENT)
               </th>
               <th class="text-center">
                  CALCULATED HOST TOTAL
               </th>
               <th class="text-center">
                  DIFFERENCE
               </th>
            </tr>
         </thead>
         <tbody>
            @for($i=0;$i<count($result_set);$i++)
            <!--DEBITS  -->
         <div >
            <tr>
            

               <td><b>DEBITS</b></br>{{ date("m/d/Y-D", strtotime($result_set[$i]->eps_date));}}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->prev_pend_deb/100, 2, '.', ''); }}</td>
               
               <td class="text-center">{{ number_format((float)$result_set[$i]->net_sales_deb/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->curr_pend_deb/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->offline_rej_deb/100, 2, '.', ''); }}</td>

               
               <td class="text-center">{{ number_format((float)($result_set[$i]->prev_pend_deb + $result_set[$i]->net_sales_deb -$result_set[$i]->curr_pend_deb - $result_set[$i]->offline_rej_deb)/100, 2, '.', ''); }}</td>
               
               <td class="text-center">{{$result_set[$i]->dep_sum_deb/100}}</td>
               
               <td class="text-center">{{($result_set[$i]->prev_pend_deb + $result_set[$i]->net_sales_deb -$result_set[$i]->curr_pend_deb - $result_set[$i]->offline_rej_deb)/100}}</td>
               
               <td class="text-center">{{ number_format((float)($result_set[$i]->dep_sum_deb - ($result_set[$i]->prev_pend_deb + $result_set[$i]->net_sales_deb -$result_set[$i]->curr_pend_deb - $result_set[$i]->offline_rej_deb))/100, 2, '.', ''); }}</td>
            </tr>

               <!--EBT  -->
            <tr>
               <td><b>EBT</b><br>{{ date("m/d/Y-D", strtotime($result_set[$i]->eps_date));}}</td>

               <td class="text-center"> {{ number_format((float)$result_set[$i]->prev_pend_ebt/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->net_sales_ebt/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->curr_pend_ebt/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->offline_rej_ebt/100, 2, '.', ''); }}</td>

               <td class="text-center"> {{  number_format((float)($result_set[$i]->prev_pend_ebt + $result_set[$i]->net_sales_ebt -$result_set[$i]->curr_pend_ebt - $result_set[$i]->offline_rej_ebt)/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->dep_sum_ebt/100, 2, '.', ''); }}</td>
                  
               <td class="text-center">{{ number_format((float)($result_set[$i]->prev_pend_ebt + $result_set[$i]->net_sales_ebt -$result_set[$i]->curr_pend_ebt - $result_set[$i]->offline_rej_ebt)/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)($result_set[$i]->dep_sum_ebt - ($result_set[$i]->prev_pend_ebt + $result_set[$i]->net_sales_ebt -$result_set[$i]->curr_pend_ebt - $result_set[$i]->offline_rej_ebt))/100, 2, '.', ''); }}</td>
            </tr>
            <!-- Credits -->
            <td ><b>CREDITS</b><br>{{ date("m/d/Y-D", strtotime($result_set[$i]->eps_date));}}</td>

            <td class="text-center"> {{ number_format((float)$result_set[$i]->prev_pend_cr/100, 2, '.', ''); }}</td>

            <td class="text-center">{{ number_format((float)$result_set[$i]->net_sales_cr/100, 2, '.', ''); }}</td>

            <td class="text-center">{{ number_format((float)$result_set[$i]->curr_pend_cr/100, 2, '.', ''); }}</td>

            <td class="text-center"> {{ number_format((float)$result_set[$i]->offline_rej_cr/100, 2, '.', ''); }}</td>

            <td class="text-center"> {{number_format((float)($result_set[$i]->prev_pend_cr + $result_set[$i]->net_sales_cr -$result_set[$i]->curr_pend_cr - $result_set[$i]->offline_rej_cr)/100, 2, '.', ''); }}</td>


            <td class="text-center"> {{ number_format((float)$result_set[$i]->dep_sum_cr/100, 2, '.', ''); }}</td>
            

            <td class="text-center"> {{ number_format((float)($result_set[$i]->prev_pend_cr + $result_set[$i]->net_sales_cr -$result_set[$i]->curr_pend_cr - $result_set[$i]->offline_rej_cr)/100, 2, '.', ''); }}</td>


            <td class="text-center">{{ number_format((float)($result_set[$i]->dep_sum_cr - ($result_set[$i]->prev_pend_cr + $result_set[$i]->net_sales_cr -$result_set[$i]->curr_pend_cr - $result_set[$i]->offline_rej_cr))/100, 2, '.', ''); }}</td>
            </tr>
            <!--gift card  -->
            <tr>
               <td><b>Gift CARD</b><br>{{ date("m/d/Y-D", strtotime($result_set[$i]->eps_date));}}</td>
                
                 
                
                 
                
               <td class="text-center">{{ number_format((float)$result_set[$i]->prev_pend_gc/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->net_sales_gc/100, 2, '.', ''); }}</td>

               <td class="text-center"> {{ number_format((float)$result_set[$i]->curr_pend_gc/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->offline_rej_gc/100, 2, '.', ''); }}</td>

               <td class="text-center"> 
                 {{  number_format((float)($result_set[$i]->prev_pend_gc + $result_set[$i]->net_sales_gc -$result_set[$i]->curr_pend_gc - $result_set[$i]->offline_rej_gc)/100, 2, '.', ''); }}</td>

               <td class="text-center"> {{ number_format((float)$result_set[$i]->dep_sum_gc/100, 2, '.', ''); }}</td>


               <td class="text-center"> {{ number_format((float)($result_set[$i]->prev_pend_gc + $result_set[$i]->net_sales_gc -$result_set[$i]->curr_pend_gc - $result_set[$i]->offline_rej_gc)/100, 2, '.', ''); }}</td>
               
               <td class="text-center">{{ number_format((float)($result_set[$i]->dep_sum_gc - ($result_set[$i]->prev_pend_gc + $result_set[$i]->net_sales_gc -$result_set[$i]->curr_pend_gc - $result_set[$i]->offline_rej_gc))/100, 2, '.', ''); }}</td>
            </tr>
            <!--STORE CREDIT  -->
            <tr>
               <td><b>STORE CREDIT</b><br>{{ date("m/d/Y-D", strtotime($result_set[$i]->eps_date));}}</td>
              
               <td  class="text-center"> {{ number_format((float)$result_set[$i]->prev_pend_sc/100, 2, '.', ''); }}</td>

               <td class="text-center"> {{ number_format((float)$result_set[$i]->net_sales_sc/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->curr_pend_sc/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->offline_rej_sc/100, 2, '.', ''); }}</td>


               <td class="text-center"> {{ number_format((float)($result_set[$i]->prev_pend_sc + $result_set[$i]->net_sales_sc -$result_set[$i]->curr_pend_sc - $result_set[$i]->offline_rej_sc)/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->dep_sum_sc/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)($result_set[$i]->prev_pend_sc + $result_set[$i]->net_sales_sc - $result_set[$i]->curr_pend_sc - $result_set[$i]->offline_rej_sc)/100, 2, '.', ''); }}
               </td>
               

               <td class="text-center">{{ number_format((float)($result_set[$i]->dep_sum_sc - ($result_set[$i]->prev_pend_sc + $result_set[$i]->net_sales_sc -$result_set[$i]->curr_pend_sc - $result_set[$i]->offline_rej_sc))/100, 2, '.', ''); }}</td>
            </tr>
            <!--E-CHECK  -->
            <tr>
               <td><b>E-CHECK</b><br>{{ date("m/d/Y-D", strtotime($result_set[$i]->eps_date));}}</td>

               
              
               
               <td class="text-center">{{ number_format((float)$result_set[$i]->prev_pend_ec/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)$result_set[$i]->net_sales_ec/100, 2, '.', ''); }}</td>

               <td class="text-center"> {{ number_format((float)$result_set[$i]->curr_pend_ec/100, 2, '.', ''); }}</td>

               <td class="text-center"> {{ number_format((float)$result_set[$i]->offline_rej_ec/100, 2, '.', ''); }}</td>
             
              


               <td class="text-center">  {{ number_format((float)($result_set[$i]->prev_pend_ec + $result_set[$i]->net_sales_ec -$result_set[$i]->curr_pend_ec - $result_set[$i]->offline_rej_ec)/100, 2, '.', ''); }}</td>

               <td class="text-center"> {{ number_format((float)$result_set[$i]->dep_sum_ec/100, 2, '.', ''); }}</td>

               <td class="text-center"> {{ number_format((float)($result_set[$i]->prev_pend_ec + $result_set[$i]->net_sales_ec -$result_set[$i]->curr_pend_ec - $result_set[$i]->offline_rej_ec)/100, 2, '.', ''); }}</td>

               <td class="text-center">{{ number_format((float)($result_set[$i]->dep_sum_ec - ($result_set[$i]->prev_pend_ec + $result_set[$i]->net_sales_ec -$result_set[$i]->curr_pend_ec - $result_set[$i]->offline_rej_ec))/100, 2, '.', ''); }}</td>
                  

            </tr> 
         </div>  
                    
            @endfor
            
            <tr>
            <td><b>Total</b></td>
            <td class="text-center">{{ number_format((float)$work_data['total_prev'], 2, '.', ''); }}</td>
            <td class="text-center">{{ number_format((float)$work_data['total_net'], 2, '.', ''); }}</td>
            <td class="text-center">
               {{ number_format((float)$work_data['total_curr'], 2, '.', ''); }}
            </td>
            <td class="text-center">
               {{ number_format((float)$work_data['total_rej'], 2, '.', ''); }}
            </td>
            <td class="text-center">
               {{ number_format((float)$work_data['total_host'], 2, '.', ''); }}
            </td>
            <td class="text-center">
               {{ number_format((float)$work_data['total_dep'], 2, '.', ''); }}
            </td>
            <td class="text-center">
               {{ number_format((float)$work_data['total_cal_host'], 2, '.', ''); }}
            </td>
            <td class="text-center">
               {{ number_format((float)$work_data['total_diff'], 2, '.', ''); }}
            </td>
            </tr>
         </tbody>
      </table>
   </div>
</div>

@else
<div class="invalertdanger" role="alert" ">
  <strong>Warning! </strong>Their is No data for {{$wk_date}} date. <a href="{{ ('bookepsprint')}}" class="alert-link" style="color: blue;">Go Back </a>for submit another date.
</div>
@endif
</div>
@stop