@extends('layout.dashboardbookkeepermarket')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<!-- 
   <div class="col-md-12">
      <br>
      <div class="alert alert-success" role="alert">
         <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
         Total Safe Details added successfully!                                 
      </div>
   </div> -->
<div class="col-md-12">
    
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div class="mainbox">
      <div class="panel panel-info">
         <div class="panel_heading">
            <div class="panel-title">COMMENT LIST</div>
         </div>
         <div class="panel_body">
            <form action="{{URL::route('mktmgr-post-commentdate')}}" id="booksafedate" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="row">
                  <div class="form-group">
                     <!-- <div class="col-sm-6"> -->
                     <label for="comment_date" class="control-label col-sm-5">Please enter the Date of the Information</label>
                     <!-- </div> -->
                     <div class="col-sm-7" style="padding-top: 10px;">
                        
                        <input type="date" placeholder="MM-DD-YYYY" name="comment_date" id="" autocomplete="off" isimportant="true" class="form-control lastdate" value="{{ $previewsdate }}">
                        @if($errors->has('comment_date'))
                        {{ $errors->first('comment_date')}}
                        @endif       
                     </div>
                  </div>
               </div>
               {{-- <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div> --}}
               <div class="form-group">
                 <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4 padding_bottom">
                       <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                       <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                       {{ Form::token()}}
                    </div>
                 </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop