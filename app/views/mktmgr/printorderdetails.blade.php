<?php

  // echo '<pre>';print_r($seq_number_result_array);exit; 
?>
@extends('layout.dashboard')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<div class="container">
   <div class="row">
      <div class="col-md-6">
         <h3>Browse</h3>
      </div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
        <a href="{{ route('pdf-report-printorderdetails',['download'=>'pdf','description'=>$description,
             'order_date'=>$order_date,'status_code'=>$status_code,'seq_number'=>$seq_number])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
<div class="container">
   <div class="row">
      <div class="col-md-4"><b>Order Date</b> : {{ $order_date }}</div>
      <div class="col-md-4">&nbsp;</div>
      <div class="col-md-4">&nbsp;</div>
   </div>
   <div class="row">

      <div class="col-md-4">
      Status : 
      @if($status_code == "O")
      OPEN
      @elseif($status_code == "T")
      TRANSMITTED
      @elseif($status_code == "S")
      SENDING
      @elseif($status_code == "H")
      HOLD
      @elseif($status_code == "V")
      VOID
      @elseif($status_code == "R")
      RELEASED
      @elseif($status_code == "F")
      PLUSSED OUT
      @else
      UNKNOWN
      @endif
      </div>
      <div class="col-md-4">{{ $description }}</div>
      <div class="col-md-4">&nbsp;</div>
   </div>
   <br>
</div>
<div class="container">
   @if ($seq_number_result_array)
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>SKU Number</th>
            <th>Item Number</th>
            <th>Description</th>
            <th>Pack Size</th>
            <th>Container Size</th>
            <th>Qty</th>
         </tr>
      </thead>
      <tbody>
         @for ($i = 0; $i < count($seq_number_result_array); $i++)
         <tr>
            <td>{{ $seq_number_result_array[$i]['sku_number'] }}</td>
            <td>{{ $seq_number_result_array[$i]['item_number'] }}</td>
            <?php 
            $soitem_result = DB::select('select * from soitem WHERE sku_number  = "'.$seq_number_result_array[$i]['sku_number'].'"');
            $soitem_array = json_decode(json_encode($soitem_result), true);
            //echo '<pre>';print_r($soitem_array);exit;
            ?>
             @if(!empty($soitem_array))
              <td>{{ $soitem_array[0]['description'] }}</td>
              <td>{{ $soitem_array[0]['case_pack'] }}</td>
             <td>{{ $soitem_array[0]['cont_size'] }}</td>
              @else
              <td>ITEM DETAIL NOT FOUND</td>
              <td>0</td>
              <td>0</td>
             @endif 
            
            
            <td>{{ $seq_number_result_array[$i]['quantity'] }}</td>
         </tr>
         @endfor
      </tbody>
   </table>
   @else
   <div class="alert alert-danger">
      <strong>Alert!</strong> No Store Transfers meet Query criteria.
   </div>
   @endif

   <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
    <input type="button" class="btn" name="" onclick="history.go(-1);" value="Back">        
 
   {{ Form::token()}}
        </div>
        </div>
  </div>
</div>
@stop