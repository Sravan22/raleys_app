<?php 
   //echo '<pre>';print_r($StoreUpdateArray);exit;
?>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storetransfersmenu')
</header>

<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transfers Inquiry</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-post-store-transfer-update')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               {{-- <input type="text" name="" value="{{ $StoreUpdateArray[0]['seq_number'] }}"> --}}
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">From Store Number</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="From Store Number" readonly="" name="" value="{{ $StoreUpdateArray[0]['from_store_no'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">From Department</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="From Department" name="from_dept_no" value="{{ $StoreUpdateArray[0]['from_dept_no'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Acct to CREDIT</label>
                  </div>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="" placeholder="Acct to CREDIT" name="from_account" value="{{ $StoreUpdateArray[0]['from_account'] }}">
                  </div>
                  <div class="col-sm-4" style="margin-left:-57px;width:39%;">
                     <input type="text" class="form-control" id="" placeholder="" name="from_account_desc" value="{{ $StoreUpdateArray[0]['from_account_desc'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">To Store No</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="To Store No" name="to_store_no" value="{{ $StoreUpdateArray[0]['to_store_no'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">To Department</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="To Department" name="to_dept_no" value="{{ $StoreUpdateArray[0]['to_dept_no'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Acct to CHARGE</label>
                  </div>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="" placeholder="Acct to CHARGE" name="to_account" value="{{ $StoreUpdateArray[0]['to_account'] }}">
                  </div>
                  <div class="col-sm-4" style="margin-left:-57px;width:39%;">
                     <input type="text" class="form-control" id="" placeholder="" name="from_account" value="{{ $StoreUpdateArray[0]['to_account_desc'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Transfer Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Transfer Date" name="create_date" value="{{ date("m/d/Y", strtotime($StoreUpdateArray[0]['create_date'])) }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Transfer Number</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Transfer Number" name="" value="{{ $StoreUpdateArray[0]['transfer_number'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Transfer Type</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Transfer Type" name="transfer_type" value="{{ $StoreUpdateArray[0]['transfer_type'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Employee ID</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Employee ID" name="" value="{{ $StoreUpdateArray[0]['employee_id'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Status</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Status" name="" value="{{ $StoreUpdateArray[0]['status'] }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Create Date / Time</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Create Date / Time" readonly="" name="" value="{{ date("m/d/Y H:i:s", strtotime($StoreUpdateArray[0]['create_datetime'])) }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Last Update</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Last Update" readonly="" name="" value="{{ date("m/d/Y H:i:s", strtotime($StoreUpdateArray[0]['last_update'])) }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="store-submit" id="submit" tabindex="4" value="Update" class="btn">
                        <input type="reset" name="" id="submit" tabindex="4" value="Cancel" onclick="history.go(-1);" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop