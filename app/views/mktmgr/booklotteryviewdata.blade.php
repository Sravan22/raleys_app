<?php 
  //echo '<pre>',print_r($lot_array);exit();
?>

@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-lottery-inventory',['download'=>'pdf','dateofinfo'=> $end_date])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
<div class="container">
   <div id="loginbox" style="margin-top:50px;" class="mainbox col-sm-12">
      <div class="panel">
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title" >LOTTERY INVENTORY OVERVIEW - {{date('m/d/Y',strtotime($end_date))}}</div>
         </div>

         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-booklotteryviewdata')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="table-responsive ">
                  <table class="table table-striped">
                     <thead>
                        <tr>
                           <th>GAME</th>
                           <th>PRICE</th>
                           <th>BEGIN</th>
                           <th>DEL</th>
                           <th>RETURNS</th>
                           <th>END INV</th>
                           <th>SALES</th>
                           <th>UNITY</th>
                           <th>DIFF</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php 
                      $exclude = array('ZZZZ');
                     ?>
                      @foreach($lot_array as $key => $value)
                      <?php 
                        if (in_array($value['game_no'], $exclude)) continue;
                      ?>                                                     
                      <tr>
                          <td>{{ $value['game_no'] }}</td>
                          <td>{{ number_format((float)$value['ticket_value'], 2, '.', ''); }}</td>
                          <td>{{ number_format((float)$value['begin_inv'], 2, '.', ''); }}</td>
                          <td>{{ number_format((float)$value['deliveries'], 2, '.', ''); }}</td>
                          <td>{{ number_format((float)$value['returns'], 2, '.', ''); }}</td>
                          <td>{{ number_format((float)$value['end_inv'], 2, '.', ''); }}</td>
                          <td>{{ number_format((float)$value['calc_sales'], 2, '.', ''); }}</td>
                          <td>{{ number_format((float)$value['sales_unity'], 2, '.', ''); }}</td>
                          <td>{{ number_format((float)$value['sales_diff'], 2, '.', ''); }}</td>
                      </tr>
                      @endforeach
                       
                     </tbody>
                  </table>
                  
                  <input type="button" class="btn" name="" onclick="history.go(-1);" style="margin-left: 46%;" value="Back">  

               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop