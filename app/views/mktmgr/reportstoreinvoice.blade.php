@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>
<script>
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
            invoiceno: "required"
        },
        
        // Specify the validation error messages
        messages: {
            firstname: "Please enter your first name"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<div class="container">
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store invoice</div>
         </div>
         <div class="panel-body" style="margin-top:10px; padding-top: 20px;">
            <form action="{{URL::route('mktmgr-post-report-storeinvoice')}}" class="form-horizontal" id="register-form" method="post" role="form" style="display: block;">
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <label for="invoiceno" class="col-sm-4 control-label">Invoice Number</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control keyfirst" id="invoiceno" placeholder="Invoice Number" name="invoiceno" autofocus="" tabindex="2" />
                     @if($errors->has('invoiceno'))
                     {{ $errors->first('invoiceno')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12 topspace" style="margin-left: 45%">
                        <input type="submit"  tabindex="3" value="Search" class="btn"/>
                        <input type="reset" tabindex="4" value="Reset" class="btn keylast"/>
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="5"></div>
            </form>
         </div>
      </div>
   </div>
</div>

@stop
