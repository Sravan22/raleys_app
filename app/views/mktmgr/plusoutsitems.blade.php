<?php
   //echo '<pre>';print_r($soorditm_rec);exit; 
?>
@extends('layout.dashboard')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<div class="container">
   <div class="row">
      <div class="col-md-6">
         <h3>Browse</h3>
      </div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
        <a href="{{ route('pdf-report-plusoutitems',['download'=>'pdf','seq_number'=>$seq_number])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
<div class="container">

   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>SKU</th>
            <th>Description</th>
            <th>Case Pack</th>
            <th>Cont Size</th>
            <th>Qty</th>
         </tr>
        </thead> 

        <tbody>
        @if(count($soorditm_rec)>0)
         @for ($i = 0; $i < count($soorditm_rec); $i++)   
         <tr>
            <td>{{ $soorditm_rec[$i]->sku_number }}</td>
            <?php 
                $soorditm_rec_query =  DB::select('SELECT description, case_pack, cont_size   FROM soitem WHERE sku_number = "'.$soorditm_rec[$i]->sku_number.'" ');
           $soorditm_rec_query_array = json_decode(json_encode($soorditm_rec_query), true);
             if(!empty($soorditm_rec_query_array))
              {
                  echo '<td>'.$soorditm_rec_query_array[0]['description'].'</td>';
                  echo '<td>'.$soorditm_rec_query_array[0]['case_pack'].'</td>';
                  echo '<td>'.$soorditm_rec_query_array[0]['cont_size'].'</td>';
              }
              else
              {
                  echo '<td>UNKNOWN ITEM</td>';
                  echo '<td></td>';
                  echo '<td></td>'; 
              }
            ?>
            
            <td>{{ $soorditm_rec[$i]->quantity }}</td>
         </tr>
        
         @endfor

              @else
 <tr><td colspan="6" align="center">No records found!</td></tr> 
   @endif  
      </tbody>
   </table>
   <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
    <input type="button" class="btn" name="" onclick="history.go(-1);" value="Back">        
 
   {{ Form::token()}}
        </div>
        </div>
  </div>
</div>
@stop