@extends('layout.dashboard')
@section('page_heading','Receivings >> Printer Selection Menu')
@section('content')
@section('section')
<style>
@media print 
{
  a[href]:after { content: none !important; }
  img[src]:after { content: none !important; }
}
</style>
<header class="row">
        @include('mktmgr.recivermenu')
    </header>
<div class="container">
@if($results)
 <div class="col-md-12 text-right">
            <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-weeklyreport',['download'=>'pdf' , 'storeno'=>$storeno, 'department'=>$department, 'fromdate'=>$fromdate, 'todate'=>$todate]) }}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
        </div>
@endif        
    <table class="table table-striped">
        <thead>
          <tr>
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th>Name</th>
            <th>Type</th>
            <th>Total Cost</th>
          </tr>
        </thead>
        <tbody>
        <?php if(count($results) > 0) { ?>
        @foreach ($results as $rs)
          <tr>
            <td>{{  $rs->id }}</td>
            <td>{{ date("m/d/Y", strtotime($rs->invoice_date)) }}</td>
            {{-- <td>{{ $rs->invoice_date }}</td> --}}
            <td>{{ $rs->name }}</td>
            {{-- <td>{{ $rs->type_code }}</td> --}}
            @if($rs->type_code == 'D') 
            <td>Delivery</td>
            @elseif ($rs->type_code == 'R')
            <td>Return</td>
            @elseif ($rs->type_code == 'S')
            <td>Store Supplies</td>
            @else
            <td>Unknown</td>
            @endif
            <td>{{ $rs->tot_vend_cost }}</td>
          </tr>
        @endforeach
    <?php }
    else { ?>
      <tr><td colspan="5" align="center">No records found!</td></tr>  
    <?php } ?>
        </tbody>
    </table>          
    <?php //echo $results->links(); ?>
</div>
@stop
