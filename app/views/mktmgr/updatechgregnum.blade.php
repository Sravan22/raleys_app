@extends('layout.dashboardbookkeepermarket')
@section('page_heading','Update Chg Reg')
@section('content')
@section('section')
<header class="row">
         @include('mktmgr.bookkepermenu')
    </header>
<div class="container">
  <!-- <p id="menulist">
      <a href="{{ URL::route('mktmgr-receivings')}}" >Receivings</a> >> Store Expenses >>
      <a href="{{ URL::route('mktmgr-receivings-sequery')}}" >Query</a>
  </p> -->
          <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >cHg-regs </div>
                       </div> 

                 <div style="padding-top:5px" class="panel-body" >

            
                <form action="{{URL::route('mktmgr-post-chg-reg-update')}}" class="form-horizontal" method="post" role="form" style="display: block;">

                <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Register Number</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" readonly="" id="store" placeholder="Store" name="reg_num" value="{{ $chg_data->reg_num }}">
                 @if($errors->has('store'))
            {{ $errors->first('store')}}
            @endif
            </div>
        </div> 

         
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Registration Type</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" style="text-transform: uppercase" maxlength="1" id="status" placeholder="Status" name="reg_type" value="{{ $chg_data->reg_type }}" autofocus="">
                 @if($errors->has('status'))
            {{ $errors->first('status')}}
            @endif
            </div>
        </div>        
                  
          <div class="form-group">
            <div class="row">
               <div class="col-sm-12" style="margin-left: 44%">
    <input type="submit" name="" id="submit" tabindex="4" value="Update" class="btn">
        <input type="button" onclick="history.go(-1)" value="Cancel" class="btn">
                                        {{ Form::token()}}
              </div>

            </div>
          </div>
          
        </form>
                
              
          </div>
        </div>
      </div>
</div>
@stop
