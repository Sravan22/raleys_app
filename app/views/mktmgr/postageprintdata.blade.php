 @extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
   p.big {
    line-height: 250%;
   }
   .dottedUnderline { border-bottom: 1px dotted; }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php $wk_date = date("m/d/Y", strtotime($end_date));?>
<div class="container">
@if(!empty($work_data))
   <div id="loginbox" class="mainbox col-sm-12">
      
         <div class="" style=" text-align:center;">
         <div class=" col-md-12" >
             <b style="font-size: 22px">Raley's /  Bel Air </b>
               <span class="pull-right">
               <a href="#" onclick="printPage()" id="romovepdf"><i class="fa fa-print fa-fw iconsize "></i> </a>
               </span></div>
               <div class="panel-title" style="margin-right: 30px;font-weight:bold;">
               Postage stamp Inventory
               </div>
               <div class="col-md-1">
               &nbsp;
                  
               </div>
             <div class="col-md-5 panel-title" style="text-align: right;font-weight:bold;">
               Store Number.. <?php echo Session::get('storeid');?>
               </div>
               <div class="col-md-6 panel-title" style="text-align: left;font-weight:bold;">
               Week-Ending Date..<?php echo $wk_date;?>
             </div>
              
                
            </div>
            
           <!--  <div class="col-md-1">
               <span class="pull-right">
               <a href="#" onclick="printPage()" id="romovepdf"><i class="fa fa-print fa-fw iconsize "></i> </a>
                  {{--  <a href="#" target="_blank">
                   <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
                   </a> --}}
               </span>

            </div> -->
         </div>
         <br> <br> 
         <div>
          <table  class="table table-bordered table-colored" align="center" style="width: 50%;margin-top: : 10%;font-family:arial;">
          <tbody>
            
             <tr>
               <th scope="row">POSTAGE STEMP CHECK NUMBER RECEIVER</th>
               <td>{{$work_data['check_num'][0]['ck_no_1'];}}</td>
             </tr>
             <tr>
               <th scope="row">POSTAGE STEMP CHECK NUMBER RECEIVER</th>
               <td>{{$work_data['check_num'][0]['ck_no_2'];}}</td>
             </tr>
              <tr>
               <th scope="row">BEGINNING POSTAGE STEMP INVENTORY</th>
               <td>{{number_format((float)$work_data['all'][0]->begin_inv, 2, '.', '');}}</td>
             </tr>
             <tr>
               <th scope="row">PURCHASES OR POSTAGE STEMP CHECK</th>
               <td>{{number_format((float)$work_data['all'][0]->purchases, 2, '.', '');}}</td>
             </tr>
             <tr>
               <th scope="row">ENDING POSTAGE STEMP INVENTORY</th>
               <td>{{number_format((float)$work_data['all'][0]->end_inv_tills, 2, '.', '');}}</td>
             </tr>
             <tr>
               <th scope="row">CALCULATED SALES</th>
               <td>{{number_format((float)$work_data['all'][0]->calc_sales, 2, '.', '');}}</td>
             </tr>
             <tr>
               <th scope="row">P/S SALES FROM UNITY REPORT</th>
               <td>{{number_format((float)$work_data['all'][0]->ps_unity_sales, 2, '.', '');}}</td>
             </tr>
             <tr>
               <th scope="row">SALES DIFFERENCE</th>
               <td>{{number_format((float)$work_data['all'][0]->sales_diff, 2, '.', '');}}</td>
             </tr>
          </tbody>
               
         </table>
               
   </div>
</div>
@else
<div class="invalertdanger" role="alert" ">
  <strong>Warning! </strong>Their is No data for {{$wk_date}} date. <a href="{{ ('bookcommuterprnt')}}" class="alert-link" style="color: blue;">Go Back </a>for submit another date.
</div>
@endif
</div>
         <!-- <div style="padding-top:30px" class="panel-body">
            <div class="table-responsive">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center">PERIOD</th>
                        <th class="text-center">WEEK-ENDING DATE</th>
                        <th class="text-center">MARKET NUMBER</th>
                        <th class="text-center">DRUG NUMBER</th>
                        <th class="text-center">COMPETITOR COUPON</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td class="text-center"><b>  </b></td>
                        <td class="text-center"><b>  </b></td>
                        <td class="text-center"><b> </b></td>
                        <td class="text-center"><b>  </b></td>
                        <td class="text-center"><b>.00 </b></td>
                     </tr>
                  </tbody>
               </table>
            </div> -->
@stop
 

<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".romovepdf").show();
    $(".romovepdf").click(function() {
      $(".romovepdf").hide();
    });
  });
</script>