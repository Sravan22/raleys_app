<?php 
   //echo '<pre>',print_r($resultsArray);exit();
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store : 355{{-- {{{ Session::get('storeid') }}} --}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter Deliveries</div>

         </div>
         <div class="panel-body">
            <div class="table-responsive">
            <form action="{{ URL::route('bookspro-post-data-deliveryadd_update') }}" class="form-horizontal" method="post" role="form" style="display: block;">
               <table class="table table-striped" style="margin-bottom: 0px;">
                  <thead>
                     <tr>
                        <th class="text-center">Invoice No : {{ $invoice_no }}</th>
                        <th class="text-center">Date : {{ date("m/d/Y", strtotime($invoice_date)) }}</th>
                        <th class="text-center">Inv Count : {{ $invCount }}</th>
                     </tr>
                     <input type="hidden" name="invoice_no" value="{{ $invoice_no }}">
                     <input type="hidden" name="invoice_date" value="{{ $invoice_date }}">
                     <tr>
                        <th class="text-center">Grade</th>
                        <th class="text-center">Gallons Count</th>
                        <th class="text-center">Gallons Price</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $exclude = array('M');
                        for($i = 0;$i<count($resultsArray);$i++)
                        {
                        if(!($resultsArray[$i]['gallon_count'])=='')
                           {
                              $gallon_count = number_format((float)$resultsArray[$i]['gallon_count'], 2, '.', '');
                           }
                           else
                           {
                              $gallon_count = '';
                           }
                        if (in_array($resultsArray[$i]['grade_id'], $exclude)) continue; ?>
                     <tr>
                        <td class="text-center">{{  $resultsArray[$i]['descr']  }}</td>
                        <td class="text-center">
                        <input type="hidden" name="grade_id[]" value="{{ $resultsArray[$i]['grade_id'] }}">
                        <input type="text" class="myinputclass twodots" name="gallon_count[]" id="{{ $resultsArray[$i]['grade_id'] }}" value="{{ $gallon_count  }}" onKeyPress="return StopNonNumeric(this,event)"></td>
                        <td class="text-center"><input type="text" class="myinputclass fourdots" name="gallon_price[]" id="{{  $i  }}" value="{{  $resultsArray[$i]['gallon_price']  }}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>
                     </tr>
                     <?php
                        }
                        ?>
                        <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn"> <input type="button" class="btn" value="Cancel" name="" onClick="document.location.href='{{URL::to('mktmgr/deliveryadd_update')}}'"></td>
                        <td>&nbsp;</td>
                        </tr>
                  </tbody>
               </table>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

@stop
<style type="text/css">
   .myinputclass{
   width: 110px;
   padding: 4px 20px;
   border: 1px solid #ccc;
   border-radius: 4px;
   text-align: center;
   font-size: 15px;
   }
</style>
<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>     
<script>
$(document).ready(function() {
   $(".myinputclass").on("click", function () {
        $(this).select();
   });

   $(".twodots").on('blur',function(){  
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        //alert(selfieldval);return false;
        if(selfieldval!= '')
        {
         $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
        }
        else
        {
         $("#"+selfield).val();
        }
        
   }); 
   $(".fourdots").on('blur',function(){  
        sel_field = this.id;
        sel_fieldval=$("#"+sel_field).val(); 
        if(sel_fieldval!= '')
        {
         $("#"+sel_field).val(parseFloat(sel_fieldval).toFixed(4));
        }
        else
        {
         $("#"+sel_field).val();
        }
   }); 



// $(document).ready(function() {
   $("#submit").click(function(){
          var inputs = $(".fourdots");
            for(var i = 0; i < inputs.length; i++){
              if($(inputs[i]).val() == '')
                {
                  $(inputs[i]).focus();return false;
                }
            }
           });
      $("#submit").click(function(){
          var inputs1 = $(".twodots");
            for(var i = 0; i < inputs1.length; i++){
              if($(inputs1[i]).val() == '')
                {
                  $(inputs1[i]).focus();return false;
                }
            }
           });

   });  
    //  });
 function StopNonNumeric(el, evt)
       {
           var charCode = (evt.which) ? evt.which : event.keyCode;
           var number = el.value.split('.');
           if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
               return false;
           }
           //just one dot (thanks ddlab)
           if(number.length>1 && charCode == 46){
                return false;
           }
           //get the carat position
           var dotPos = el.value.indexOf(".");
           if( dotPos>-1 && (number[1].length > 3)){
               return false;
           }
           return true;
       }

</script>