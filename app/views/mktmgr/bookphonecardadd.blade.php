
@extends('layout.dashboardbookkeepermarket')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container" style="margin-left: 95px">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-7 col-md-offset-2 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >PHONE CARD INVENTORY</div>
         </div>
         <div style="padding-top:15px" class="panel-body" >
            <form action="{{URL::route('mktmgr-bookphonecardshow')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-7" style="padding-left: 61px;">Please enter the Date of the Information</label>
                  <div class="col-sm-5">
                     <input type="date" class="form-control lastdate" id="" placeholder="" name="dateofinfo" value="{{$data->dateofinfo}}">
                     @if($errors->has('dateofinfo'))
                     {{ $errors->first('dateofinfo')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 10px;">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
 
@stop