<?php
   //echo $sort_by_sw."<br>";
    //echo $dept_name_array[0]['name']."<br>";
    //echo '<pre>';print_r($dept_name_array)."<br>";
    //echo '<pre>';print_r($by_desc_curs_array)."<br>";

   // echo  $item_upper_part_details['transfer_number'];
   //exit;
  //exit;
   ?>
  
@extends('layout.dashboard')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<div class="container">
  
   <div class="row">
   <div class="col-md-8"><b>Key Order Quantities for...</b></div><div class="col-md-4"><b>Status: NEW</b></div>
   </div>
   <div class="row">
   <div class="col-md-8"> 12/31/1899 Weekly Ad for {{ $dept_name_array[0]['name'] }} - {{ $subtype_code_array[0]['name'] }}</div>
   </div>
   <br>
   <div class="row">
   <div class="col-md-7"> &nbsp;</div><div class="col-md-5"><b>SKU :</b></div>
   </div>
<div class="row">
  <div class="col-md-4">Sorted by : <b>@if($sort_by_sw == "T") PRODUCT TYPE  @endif</b></div>
  <div class="col-md-4 text-center"><b>--------------------------  Previous Ad  --------------------------</b></div>
</div>

<div class="row">
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Description</th>
            <th>Ad Retail</th>
            <th>Retail</th>
            <th>Sold</th>
            <th>Cases</th>
            <th>PO Qty</th>
            <th>Ord Qty</th>
         </tr>
      </thead>
      <tbody>
      @if($sort_by_sw == "T")
        @for ($i = 0; $i < count($by_desc_curs_array); $i++)
        <?php 
        //echo $by_desc_curs_array[$i]['sku_number']."<br>";
         $ad_bgn_date =  $by_desc_curs_array[$i]['ad_bgn_date'];
         //echo $ad_bgn_date."<br>";
        //$yesterday = date('Y-m-d',strtotime($date1 . "-1 days"));
        //$days_ago = date('Y-m-d', strtotime('-5 days', strtotime('2008-12-02')));
         $ad_bgn_date_minus_eight =  date('Y-m-d',strtotime($ad_bgn_date . "-8 days"));//exit;
         //echo $ad_bgn_date_minus_eight."<br>";
        $plus_out_qty_query =   DB::select('SELECT plus_out_type_code, plus_out_qty
            FROM sogoapod
           WHERE sku_number = "'.$by_desc_curs_array[$i]['sku_number'].'" 
             AND ship_date  < "'.$by_desc_curs_array[$i]['ad_bgn_date'].'"
             AND ship_date  > "'.$ad_bgn_date_minus_eight.'" ');
        $plus_out_qty_query = json_decode(json_encode($plus_out_qty_query), true); 

        //echo '<pre>';print_r($plus_out_qty_query);exit;
        ?>
          <tr>
          <td>{{ $by_desc_curs_array[$i]['description'] }}</td>
          <td>{{ $by_desc_curs_array[$i]['ad_retail'] }}</td>
          <td>{{ $by_desc_curs_array[$i]['prev_ad_retail'] }}</td>
          <td>{{ $by_desc_curs_array[$i]['prev_ad_sold'] }}</td>
          <td>{{ $by_desc_curs_array[$i]['case_pack'] }}</td>
          <td>0.00</td>
          <td>{{ $by_desc_curs_array[$i]['order_qty'] }}</td>
          </tr>
        @endfor
      @endif
         <!-- <tr>
           <td>dsadsa</td>
           <td>dsadsa</td>
           <td>dsadsa</td>
           <td>dsadsa</td>
           <td>dsadsa</td>
           <td>dsadsa</td>
           <td>dsadsa</td>
         </tr> -->
      </tbody>
   </table>
   <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
    <input type="button" class="btn" name="" onclick="window.location.href='{{URL::route('mktmgr-aditemordering')}}'" value="Back">        
 
   {{ Form::token()}}
        </div>
        </div>
  </div>
</div>
@stop




