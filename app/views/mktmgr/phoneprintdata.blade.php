@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
   p.big {
    line-height: 250%;
   }
   .dottedUnderline { border-bottom: 1px dotted; }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php $wk_date = date("m/d/Y", strtotime($end_date));?>
<div class="container">

   <div id="loginbox" class="mainbox col-sm-12">
      
         <div class="" style=" text-align:center;">
            <div class=" col-md-12" style="font-size: 18px">
               Raley's &nbsp / &nbsp Bel Air <span class="pull-right">
               <a href="#" onclick="printPage()" id="romovepdf"><i class="fa fa-print fa-fw iconsize "></i> </a>
                  
               </span><br>
                Phone Card Inventory<br>
               Store Number.. <?php echo Session::get('storeid');?><br>
               Week-Ending Date..<?php echo $wk_date;?>
                
            </div>
            
         </div>
         <br>
         <div>
            <p style=" text-align:center;font-size: 18px;" class="col-md-12 well"><b>INSTRUCTIONS</b><br>Give to the Grocery Managment on Grocery Inventory Day.</p>
            
         </div>
         <div class="table-responsive col-md-12">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center" rowspan="2" >&nbsp;</th>
                        <th class="text-center" colspan="2">FIVE DOLLAR</th>
                        <th class="text-center" colspan="2">TEN DOLLAR</th>
                        <th class="text-center" colspan="2">TWENTY DOLLAR</th>
                        
                     </tr>
                     <tr>
                        
                        <td class="text-center">Count</td>
                        <td class="text-center">$ Amount</td>
                        <td class="text-center">Count</td>
                        <td class="text-center">$ Amount</td>
                        <td class="text-center">Count</td>
                        <td class="text-center">$ Amount</td>
                     </tr>
                  </thead>
                  <tbody>
                  <tr>
                     <th class="text-center"><b>BEGINNING<br>INVENTORY</b></th>

                     <td class="text-center"><b>{{$work_data->five_begin_inv}}</b></td>
                     
                     <td class="text-center"><b>{{ number_format((float)$work_data->five_begin_inv_amt, 2, '.', ''); }}</b></td>
                     
                     <td class="text-center"><b>{{$work_data->ten_begin_inv}}</b></td>
                     
                     <td class="text-center"><b>{{ number_format((float)$work_data->ten_begin_inv_amt, 2, '.', ''); }}</b></td>
                     
                     <td class="text-center"><b>{{$work_data->twenty_begin_inv}}</b></td>
                     
                     <td class="text-center"><b>{{ number_format((float)$work_data->twenty_begin_inv_amt, 2, '.', ''); }}</b></td>

                  </tr>
                      <tr>
                        <th class="text-center"><b>DELIVERIES</b></th>

                     <td class="text-center"><b>{{$work_data->five_deliveries}}</b></td>
                     
                     <td class="text-center"><b>{{ number_format((float)$work_data->five_deliveries_amt, 2, '.', ''); }}</b></td>
                     
                     <td class="text-center"><b>{{$work_data->ten_deliveries}}  </b></td>

                     <td class="text-center"><b>{{ number_format((float)$work_data->ten_deliveries_amt, 2, '.', ''); }}</b></td>

                     <td class="text-center"><b>{{$work_data->twenty_deliveries}}  </b></td>

                     <td class="text-center"><b>{{ number_format((float)$work_data->twenty_deliveries_amt, 2, '.', ''); }}</b></td>

                     </tr>
                      <tr>
                        <th class="text-center"><b>RETURNS</b></th>
                        <td class="text-center"><b>{{$work_data->five_returns}}</b></td>
                        <td class="text-center"><b>{{ number_format((float)$work_data->five_returns_amt, 2, '.', ''); }}</b></td>
                        <td class="text-center"><b>{{$work_data->ten_returns}}</b></td>
                        <td class="text-center"><b>{{ number_format((float)$work_data->ten_returns_amt, 2, '.', ''); }}</b></td>
                        <td class="text-center"><b>{{$work_data->twenty_returns}}</b></td>
                        <td class="text-center"><b>{{ number_format((float)$work_data->twenty_returns_amt, 2, '.', ''); }}</b></td>

                     </tr>
                      <tr>
                        <th class="text-center"><b>ENDING<br>INVENTORY</b></th>

                     <td class="text-center"><b>{{$work_data->five_end_inv}}</b></td>

                     <td class="text-center"><b>{{ number_format((float)$work_data->five_end_inv_amt, 2, '.', ''); }}</b></td>

                     <td class="text-center"><b>{{$work_data->ten_end_inv}}</b></td>
                     
                     <td class="text-center"><b>{{ number_format((float)$work_data->five_returns_amt, 2, '.', ''); }}</b></td>
                     
                     <td class="text-center"><b>{{$work_data->ten_end_inv_amt}}</b></td>
                     
                     <td class="text-center"><b>{{ number_format((float)$work_data->twenty_end_inv_amt, 2, '.', ''); }}</b></td>

                     </tr>
                      <tr>
                        <th class="text-center"><b>SALES</b></th>
                        <td class="text-center"><b>  </b></td>
                        <td class="text-center"><b>{{ number_format((float)$work_data->five_sales_amt, 2, '.', ''); }}</b></td>
                        <td class="text-center"><b>  </b></td>
                        <td class="text-center"><b>{{ number_format((float)$work_data->ten_sales_amt, 2, '.', ''); }}</b></td>
                        <td class="text-center"><b>  </b></td>
                        <td class="text-center"><b>{{ number_format((float)$work_data->twenty_sales_amt, 2, '.', ''); }}</b></td>

                     </tr>
                     <tr>
                        <th class="text-center" colspan="7" rowspan="" headers="" scope=""><h4><b>TOTAL SALES: {{ number_format((float)$work_data->tot_sales, 2, '.', ''); }}</b></h4></th>

                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         </div>
         

@stop         