@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storetransfersmenu')
</header>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transfer Hardcopy</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">From Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control" id="fstnumber" placeholder="" name="fstnumber">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">To Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control" id="fstnumber" placeholder="" name="fstnumber">
                  </div>
               </div>
               <!-- <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Copies</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="fstnumber" placeholder="Number of Copies" name="fstnumber">
                  </div>
               </div> -->
               
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop