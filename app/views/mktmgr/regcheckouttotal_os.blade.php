<?php
   //echo 'Reg Check Out Totals';
// echo $groctot;
// echo $drugtot;exit;
   //echo '<pre>';print_r($resultArray);exit;
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style type="text/css">
   .makecenter
   {
   margin-left:20px;
   }
   .row.vertical-divider > div[class^="col-"]
   {
   text-align: left;
   padding: 2px;
   }
</style>
<form class="form-horizontal" role="form" method="post" action="">
   <div class="container" style="">
      <div class="flash-message">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >REGISTER CHECK OUT</div>
            </div>
            <div style="padding-top:15px" class="panel-body" >
               <div class="row horizantal-divider">
                  <div class="col-xs-6">
                     <div class="col-md-4 text-right"><b>Date :</b></div>
                     <div class="col-md-8"><b>{{ date('m/d/Y', strtotime($dateofinfo)) }}</b></div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <div class="col-xs-6">
                     <div class="col-md-4 text-right"><b>CATEGORY :</b></div>
                     <div class="col-md-8"><b>TOTAL OVER <<span>SHORT</span>>{{-- {{ $itemname }} --}}</b></div>
                  </div>
                  <div class="col-xs-12">
                     <hr>
                  </div>
                  <div class="row">
                     <div class="col-xs-12" style="margin-left: 15px;margin-top:10px;">
                        <div class="col-sm-2"><strong>Rg/Chkr</strong></div>
                        <div class="col-sm-2"><strong>Amount</strong></div>
                        <div class="col-sm-2"><strong>Rg/Chkr</strong></div>
                        <div class="col-sm-2"><strong>Amount</strong></div>
                        <div class="col-sm-2"><strong>Rg/Chkr</strong></div>
                        <div class="col-sm-2"><strong>Amount</strong></div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12" style="margin-left: 15px">
                        <div class="col-sm-2"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12" style="margin-left: 30px;">
                        <div class="row" style="margin-top:25px;">
                        <?php $total = 0; ?>
                        @if($resultArray)
                        @foreach($resultArray as $row)
                           <?php $total=$total+$row['item_amt'];  ?>
                           <div class="col-md-2 col-xs-6"><strong>{{ $row['reg_num'] }}</strong></div>
                           {{-- <div class="col-md-2 col-xs-6"><strong>@if($row['bl_total']) {{   number_format((float)$row['bl_total'], 2, '.', ''); }} @else  0.00  @endif </strong></div> --}}
                           <div class="col-md-2 col-xs-6" style="margin-right:-15px;"><strong>@if($row['item_amt']) {{ number_format((float)$row['item_amt'], 2, '.', '');   }} @else  0.00  @endif </strong></div>
                           
                           @endforeach
                           @else
                              
                                 <div class="col-md-11">
                                 <table class="table table-striped">
                                 <tbody>
                                    <tr>
                                     
                                     <td  colspan="5" style="text-align:center;"><b>No Data</b>
                                     </td>
                                   </tr>
                                   </tbody>
                                 </table>
                                </div>
                              
                           @endif
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <hr>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="col-md-6 text-right"><b>Total :</b></div>
                        {{-- <div class="col-md-6"><b>{{  number_format((float)$total, 2, '.', ''); }}</b></div> --}}
                        <div class="col-md-6"><b>{{  number_format((float)$total, 2, '.', '');  }}</b></div>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <hr>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="col-md-6 text-right"><b>GROCERY O<<span>S</span>>:</b></div>
                        {{-- <div class="col-md-6"><b>{{  number_format((float)$total, 2, '.', ''); }}</b></div> --}}
                        <div class="col-md-6"><b>{{  number_format((float)$groctot, 2, '.', '');  }}</b></div>
                     </div>
                     <div class="col-md-6">
                        <div class="col-md-6 text-right"><b>DRUG O<<span>S</span>> :</b></div>
                        {{-- <div class="col-md-6"><b>{{  number_format((float)$total, 2, '.', ''); }}</b></div> --}}
                        <div class="col-md-6"><b>{{  number_format((float)$drugtot, 2, '.', '');  }}</b></div>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <hr>
                  </div>
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        {{-- <input type="button" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn"> --}}
                        <input type="button" value="Go Back" class="btn" id="cancel" name="cancel" onclick="goBack()">
                     </div>
                  </div>
                  <br>
               </div>
            </div>
         </div>
      </div>
   </div>

</form>
@stop
<script type="text/javascript">
  function goBack() {
   window.history.back();
 }
</script>