        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >{{ $title }} - {{ date('m/d/Y', strtotime($date_stamp)) }}</div>
            </div> 
            <div style="padding-top:10px" class="panel-body" >

                <form action="{{URL::route('mktmgr-post-bookmngmntdailydetailaccept')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                    @foreach ($results as $rs)
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">{{$rs->item_desc}}</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control calgetval" id="" placeholder="" name="res[{{$rs->item_id}}]" value="<?php echo number_format($rs->item_amt/100,2,'.','') ?>">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    @endforeach

                    <div class="form-group">
                        <strong><label for="inputPassword" class="control-label col-sm-5">Total To Account For :</label>
                        <div class="col-sm-7" id="total_acc_for"><?php echo number_format($decValue/100,2,'.','') ?></div></strong>
                    </div>
                    <input type="hidden" name="safe_date" value="{{ $date_stamp }}">
                    <div class="form-group" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Accept" class="btn">
                                <input type="button" name="cancel" id="cancel" value="Cancel" class="btn" onclick="clickCancel();">
                                {{ Form::token()}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

