<?php 
  //echo $resultArray[1][4]['item_id'];exit;
  //echo '<pre>';print_r($resultArray[1]);exit;
//echo count($resultArray[1]);exit;
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>

<div class="container">
<div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

 <!------------------- Department Sales popup---------------------------->      
<div id="deptsalestot" style="margin-left: 100px;" class="mainbox miscbox col-md-10">
   <div class="panel panel-info safe_total">
  <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
      <div class="panel-title left" >Work By Register Screen</div>
         <div class="panel-title right" >Grand Total For : {{ date("m/d/Y", strtotime($dateofinfo)) }}</div>
      </div>
      <div  class="panel-body" style="margin-top: 10px;">
          <div  class="panel-body" style="margin-top: 10px;">
        
         <?php //$gtot=0; 
       for($i=0;$i<count($resultArray[1]);$i++) {?>
      <div class="col-xs-6" >
                <div class="form-group">
                  <div class="col-sm-2">
                     <label for="inputPassword">(<?php echo $resultArray[1][$i]['item_id']; ?>)</label>
                  </div>
                  <div class="col-sm-7">
                   <b><?php echo $resultArray[1][$i]['item_desc']; ?></b>
                  </div>
                  <div class="col-sm-3">
                     <span><b><?php echo number_format((float)$resultArray[1][$i]['item_amt'], 2, '.', ''); ?></span></b>
                  </div>
               </div>
        </div>
     <?php } ?>
     </div>  
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-2">Total Credits :</label>
            <div class="col-sm-2">
               <b>{{ number_format((float)$resultArray[0]['tot_cred'], 2, '.', ''); }}</b>
            </div>

            <label for="inputPassword" class="control-label col-sm-2">Total Debits :</label>
            <div class="col-sm-2">
              <b>{{ number_format((float)$resultArray[0]['tot_deb'], 2, '.', ''); }}</b>
            </div>

            <label for="inputPassword" class="control-label col-sm-2">Total O/S :</label>
            <div class="col-sm-2">
               <b>{{ number_format((float)$resultArray[0]['tot_os'], 2, '.', ''); }}</b>
            </div>
         </div>
         </form>
      </div>
   </div>
   <div class="form-group">
   <div class="row">
      <div class="col-sm-12 topspace" align="center">
         <input type="button" value="Go Back" class="btn" id="cancel" name="cancel" onclick="goBack()">
         {{ Form::token()}}
      </div>
   </div>
</div>

<!------------------ End of Department Sales popup ---------------------->
@stop
<script>
   function goBack() {
       window.history.back();
   }
</script>  