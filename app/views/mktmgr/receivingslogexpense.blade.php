@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')

<header class="row">
   @include('mktmgr.recivermenu')
</header>
<?php $todays_date = date('Y-m-d'); 
?>
<div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Log Expenses </div>
         </div>
         <div style="padding-top:10px" class="panel-body" >
            <form action="{{URL::route('mktmgr-post-receivings-receivingslogexpense')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Store</label>
                  <div class="col-sm-8">
                     <input type="text" readonly="" class="form-control" id="store" readonly="" placeholder="Store" name="store" value="{{{ Session::get('storeid') }}}">
                     @if($errors->has('store'))
                     {{ $errors->first('store')}}
                     @endif
                  </div>
               </div>
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Vendor</label>
                  <div class="col-sm-8">
                     <select id="vendor" name="vendor" class="form-control keyfirst " value="vendor" autofocus="" tabindex="2">
                        <option value="">Select</option>
                        @for ($i = 0; $i < count($vendor_details); $i++)
                        <option value="{{ $vendor_details[$i]['vendor_number'] }}">{{ $vendor_details[$i]['name'] }}</option>
                        @endfor
                     </select>
                     <span class="error">@if($errors->has('vendor'))
                     {{ $errors->first('vendor')}}
                     @endif</span>
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Vendor Number</label>
                  <div class="col-sm-8">
                     <input type="text" readonly="" class="form-control" name="vendornum" id="vendornum" placeholder="Vendor Number"  />
                     @if($errors->has('vendornum'))
                     {{ $errors->first('vendornum')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Invoice Date</label>
                  <div class="col-sm-8">
                     <input type="date" class="form-control" id="invdate" placeholder="MM-DD-YY" name="invdate" tabindex="3" value="{{$todays_date}}"  />
                     @if($errors->has('invdate'))
                     {{ $errors->first('invdate')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Invoice Number</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" name="invnum" {{ (Input::old('invnum'))?' 
                     value="'.Input::old('invnum').'"':''}} id="invnum" placeholder="Invoice Number" tabindex="4"  />
                     <span class="error">@if($errors->has('invnum'))
                     {{ $errors->first('invnum')}}
                     @endif</span>
                     <div id="msg" style="color: red;"></div>
                  </div>
                    
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Expense Type</label>
                  <div class="col-sm-8">
                     <!--<input type="text" class="form-control" id="expensetype" placeholder="Expense Type" name="expensetype">-->
                     <select class="form-control" id="expensetype" placeholder="Expense Type" name="expensetype" tabindex="5" >
                        <!-- <option value="S">S - Store Supplies</option> -->
                        {{-- <option value="">Select Expense Type</option> --}}
                         <option value="G">G - General Repair</option> 
                         <option value="S">S - Store Supply</option>
                          <option value="V">V  - Service </option>
                        
                     </select>
                     @if($errors->has('expensetype'))
                     {{ $errors->first('expensetype')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Amount</label>
                  <div class="col-sm-8">
                     <input type="number"  step="0.01" class="form-control" id="amount" placeholder="Amount" name="amount" {{ (Input::old('amount'))?' 
                     value="'.Input::old('amount').'"':''}} tabindex="6" />
                     <span class="error">@if($errors->has('amount'))
                     {{ $errors->first('amount')}}
                     @endif</span>
                  </div>
               </div>
               <!-- <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Status</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="status" placeholder="Status" name="status">
                     <select class="form-control" id="status" placeholder="Status" name="status">
                        <option value="A">A</option>
                           <option value="B">B</option>
                           <option value="I">I</option>
                           <option value="O" selected>O - Open</option>
                           <option value="P">P</option>
                           <option value="R">R</option>
                           <option value="U">U</option>
                           <option value="V">V</option>
                        <option value="A">A - Accepted</option>
                        <option value="H">H - On Hold</option>
                        <option value="I">I - Incomplete</option>
                        <option value="O" selected="">O - Open</option>
                        <option value="R">R - Reviewed</option>
                        <option value="U">U - Uploaded</option>
                        <option value="V">V – Voided</option>
                     </select>
                     </select>
                     @if($errors->has('status'))
                     {{ $errors->first('status')}}
                     @endif
                  </div>
               </div> -->
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 45%">
                        <input type="submit" name="login-submit" id="submit" tabindex="7" value="Submit" class="btn">
                        <input type="reset"  tabindex="8" id="reset" value="Reset" class="btn keylast clearerrors">
                        <!-- <input type="button" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn"  onClick="document.location.href='{{URL::to('mktmgr/returnhome')}}'">
                                                 -->                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="9"></div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>

<script>

      $('#invnum').blur(function() {
         //alert('Hiii');return false;
         //e.preventDefault();
         invnum = $("#invnum").val();
         vendornum = $("#vendornum").val();
         //alert(invnum);return false;
         if (invnum != '') {
            //alert(vendor);return false;
             $.post("check_inv_number", {
                 invnum: invnum,
                 vendornum: vendornum,
             }, function(result) { //alert(result); return false;
                  $("#msg").html(result);
             });
         }
       });

// $('#vendor').on('change', function(e) {
//          e.preventDefault();
//          vendor = $("#vendor").val();
         
//          if (vendor != '') {
//             //alert(vendor);return false;
//              $.post("get_vendor_number", {
//                  vendor: vendor
//              }, function(result) { //alert(result); return false;
//                   $("#vendornum").val(result);
//              });
//          }
//      });
$('#vendor').on('change', function(e) {
   vendor = $("#vendor").val();
   $("#vendornum").val(vendor);
});
$('.clearerrors').on('click', function(e) {
  $('.error').text('');
});

   $("#amount").blur(function(){
       
         amount=parseFloat($("#amount").val(),10).toFixed(2);
       $("#amount").val(amount);
        $("#amount").val(amount);
        calnet(); return true;
     });
   /*});*/
   
   
$(function() {
        //Initialize Select2 Elements
        $(".select2").select2();
    });

            $(function(){
                            var dtToday = new Date();
                            
                            var month = dtToday.getMonth() + 1;
                            var day = dtToday.getDate();
                            var year = dtToday.getFullYear();
                            if(month < 10)
                                month = '0' + month.toString();
                            if(day < 10)
                                day = '0' + day.toString();
                            
                            var maxDate = year + '-' + month + '-' + day;
                            $('#invdate').attr('max', maxDate);
                        });
 
   function calnet() {
        amount=$("#amount").val(); 
       
       }

       
</script>
@stop