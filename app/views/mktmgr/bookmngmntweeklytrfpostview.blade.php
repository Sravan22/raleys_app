        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >DELIVERY &amp; TRANSFER DETAIL - {{date('m/d/Y',strtotime($date_stamp))}}</div>
            </div> 

            <div style="padding-top:10px" class="panel-body">


                <form action="{{URL::route('mktmgr-post-bookmngmntweeklymgrok')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                <input type="hidden" name="recap_date" value="{{ $date_stamp }}">
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Transfers</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('TRANSFERS')" name="TRANSFERS" tabindex="4" value="<?php echo number_format($wk_maintrf_tranfs,2) ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                  <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Deliveries</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('DELIVERIES')" name="DELIVERIES" tabindex="4" value="<?php echo number_format($wk_maintrf_del,2) ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-5">Total</label>
                        <div class="col-sm-7">
                            <?php echo number_format($wk_maintrf_total,2) ?>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Accept" class="btn">
                                <input type="button" name="cancel" value="Cancel" class="btn" onclick="clickCancel()">
                                {{ Form::token()}}
                            </div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
