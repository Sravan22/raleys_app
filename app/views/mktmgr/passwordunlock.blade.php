@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php 
   $d=strtotime("last Saturday"); $lastsaturday = date("Y-m-d", $d);
   
   ?>
<div class="container">
   <div class="flash-message" id="recapmsg">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
         
      <a href="{{URL::route('mktmgr-bookwklyunlock')}}" style="color:red;">Go Back</a><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      </p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   {{-- <div id="yesnomsg">
      <p class="alert alert-info">This process will unlock the files, do you want to continue 
         <a href="#" id="yescallrecapdate" style="color:red;">Yes</a><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      </p>
   </div> --}}
    
</div>
<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>        
<script type="text/javascript">
   $(document).ready(function(){ 
       $("#datetounlock").hide(); //$("#pwdform").hide();
       $("#yescallrecapdate").click(function(){
           $("#yesnomsg").hide();
           $("#datetounlock").show();
           return false;
       });
   });
</script>
@stop