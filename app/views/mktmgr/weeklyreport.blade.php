@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>
<div class="container">
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-4 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Weekly Receiving Report</div>
         </div>
         <div class="panel-body" style="margin-top:10px" >
            <form action="{{URL::route('mktmgr-post-report-weeklyreport')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <input type="hidden" name="storeno" value="305">
               <div class="form-group">
                <div class="col-md-12 text-center">
                  <b>Please Key Date Range</b>
                </div>
            </div>
            <br>
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <label for="inputPassword" class="col-sm-4 control-label">Department</label>
                  <div class="col-sm-8">
                     <select class="form-control keyfirst" id="department" name="department" autofocus=""
                        tabindex="2">
                        <option value="">Select</option>
                        @foreach ($dept_list as $key => $value) 
                        <option value="{{ $value->retail_dept }}">{{ $value->retail_dept }} : {{ $value->name }}</option>
                        @endforeach
                     </select>
                     <span class="error">@if($errors->has('department'))
                     {{ $errors->first('department')}}
                     @endif</span>
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="col-sm-4 control-label">From Date</label>
                  <div class="col-sm-8">
                     <input type="date" class="form-control" id="fromdate" placeholder="MM-DD-YY" name="fromdate" tabindex="3" />
                     <span class="error">@if($errors->has('fromdate'))
                     {{ $errors->first('fromdate')}}
                     @endif</span>
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="col-sm-4 control-label">To Date</label>
                  <div class="col-sm-8">
                     <input type="date" class="form-control" id="todate" placeholder="MM-DD-YY" name="todate" tabindex="4" />
                     <span class="error">@if($errors->has('todate'))
                     {{ $errors->first('todate')}}
                     @endif</span>
                  </div>
               </div>
               <!--  <div class="form-group">
                  <label for="inputPassword" class="col-sm-4 control-label">Copies</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="copies" placeholder="Copies" name="copies">
                       @if($errors->has('copies'))
                  {{ $errors->first('copies')}}
                  @endif
                  </div>
                  </div> -->
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12 topspace" style="margin-left: 45%">
                        <input type="submit"  id="submit" tabindex="5" value="Search" class="btn"/>
                        <input type="reset" name="cancel-btn" tabindex="6" id="cancel-btn" value="Reset" class="btn keylast clearerrors"/>
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="7"></div>
            </form>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
$('.clearerrors').on('click', function(e) {
   //alert('Hiii');
   $('.error').text('');
});
</script>
@stop
