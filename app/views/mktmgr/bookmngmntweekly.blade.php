@extends('layout.dashboardbookkeepermarket')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php 
   $d=strtotime("last Saturday"); $lastsaturday = date("Y-m-d", $d);
   
   ?>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >RECAP REPORT</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            {{-- <form action="{{URL::route('mktmgr-post-bookmngmntweekly')}}" class="form-horizontal" method="post" role="form" style="display: block;"> --}}
            <form action="{{URL::route('bookspro-post-mgmtweekly')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-7">Please enter the date of the information</label>
                  <div class="col-sm-5">
                     <input type="date" class="form-control lastdate" placeholder="" name="recap_date" value="{{ $lastsaturday }}">
                     @if($errors->has('recap_date'))
                     {{ $errors->first('recap_date')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 10px;">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="Reset" tabindex="4" value="Reset" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop
 
 