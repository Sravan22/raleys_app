@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php 
   $d=strtotime("last Saturday"); $lastsaturday = date("Y-m-d", $d);
   //echo $lastsaturday;exit;
?>
<div class="container">
   <div id="loginbox" style="margin-top:5px;" class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-2">
      <!--   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-2">   -->                  
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Department Sales</div>
         </div>
         <div class="panel-body" style="padding-top: 20px;" >
            <form action="{{URL::route('mktmgr-postbookwklysalesview')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-7" style="text-align: right;  ">Please enter the date of the Information</label>
                  <div class="col-sm-5">
                     <input type="date" class="form-control" id="dateofinfo" placeholder="" value ="<?php echo $lastsaturday; ?>" name="dateofinfo">
                     @if($errors->has('dateofinfo'))
                     {{ $errors->first('dateofinfo')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>        
<script type="text/javascript">
   $(function(){
                   var dtToday = new Date();
                   
                   var month = dtToday.getMonth() + 1;
                   var day = dtToday.getDate();
                   var year = dtToday.getFullYear();
                   if(month < 10)
                       month = '0' + month.toString();
                   if(day < 10)
                       day = '0' + day.toString();
                   
                   var maxDate = year + '-' + month + '-' + day;
                   $('#dateofinfo').attr('max', maxDate);
               });
</script>
@stop
