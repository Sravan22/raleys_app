

@extends('layout.dashboard')

@section('page_heading','Non Perishable Inventory Prep')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">

</header>

<div class="container" style="padding-top:10px;">

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

<style>table a { text-decoration:underline; } </style>


    <div id="loginbox">


        <div class="" style=" text-align:center; font-weight:bold; margin-bottom: 30px;">
            <div class="panel-title" >Non Perishable Inv Prep {{$data->sodinvno}}: Item Scan Detail</div>
        </div> 
      
         <div class="row" style=" margin-bottom: 30px;">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <span class="pull-right">
                    <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-nonpar-scan',['download'=>'pdf','deptno'=> $data->dept_inp,'sodinvno'=>$data->sodinvno])  }}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
                </span>
            </div>
          </div>
        
        

        <form action="#" class="form-horizontal" method="post" role="form" style="display: block;">


            <table class="table ">
                <tr>
                    <td>Inventory No:</td>
                    <td>{{$data->sodinv_hdrs_rec['inventory_number']}}</td>
                    <td>Total Line Items:</td>
                    <td>{{$data->sodinv_hdrs_rec['total_scans']}}</td>
                </tr>
                <tr>
                    <td>Inventory Date:</td>
                    <td>{{date('m/d/Y',strtotime($data->sodinv_hdrs_rec['create_date']))}}</td>
                    <td>Total Cst Value:</td>
                    <td>${{$data->sodinv_hdrs_rec['total_value_cst']}}</td>
                </tr>
                <tr>
                    <td>Employee ID:</td>
                    <td>{{$data->sodinv_hdrs_rec['employee_id']}}</td>
                    <td>Total Rtl Value:</td>
                    <td>${{$data->sodinv_hdrs_rec['total_value_rtl']}}</td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td>{{$data->sodinv_hdrs_rec['hdr_status']}}</td>
                    <td>Total CRV Value:</td>
                    <td>${{$data->sodinv_hdrs_rec['total_crv_rtl']}}</td>
                </tr>
            </table>


            <table class="table table-striped">
                <thead>
                    <tr>        
                        <th>Scn Number</th>
                        <th>Description</th>
                        <th>Itm Cst</th>
                        <th>Itm Rt1</th>
                        <th>Qty</th>
                        <th>Ext Cost</th>
                        <th>Ext Rt1</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($data->sodinv_dtls_rec as $key => $value)

                    <tr>

                        <td><a href="javascript:void(0)" onclick="showdetails({{$data->sodinv_hdrs_rec['inventory_number']}},{{$value['seq_number']}})" >{{$value['seq_number']}}</a></td>
                        <td>{{$value['item_desc']}}</td>
                        <td>${{$value['upc_cost']}}</td>
                        <td>${{$value['rtl_amt']}}</td>
                        <td>{{$value['quantity']}}</td>
                        <td>${{$value['ext_value_cst']}}</td>
                        <td>${{$value['ext_value_rtl']}}</td>
                    </tr>
                    @endforeach


                </tbody>
            </table>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12" align="center">
                       
                        <input type="button" name="addinfo" id="addinfo" tabindex="4" onclick="window.location.href ='{{URL::route('mktmgr-noninvdeptinfo',$data->dept_inp)}}'" value="Back" class="btn">
                        {{ Form::token()}}
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@section('jssection')
@parent
<script>

    parentHtml = $("#loginbox").html();
function showdetails(inventory_number,seq_number) {
    $.ajax({
        url:'{{URL::route('mktmgr-noninvitemdetail')}}',
        type:'GET',
        data: "hdrseq="+inventory_number+"&seqno="+seq_number,
        success:function(data){
            $("#loginbox").html(data);
        }
    });
}

function clickCancel() {
    $("#loginbox").html(parentHtml);
    //htmlStore = parentHtml;
}
</script>
@endsection



@stop
