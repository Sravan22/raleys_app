@extends('layout.dashboardbookkeepermarket')
@section('page_heading','Receivings >> Printer Selection Menu')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<div class="container">
   @if(!empty($searchresult) )
   <!-- <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>
      Print</span></a> -->
   <div class="col-md-12 text-right">
      <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
      <a href="{{ route('pdf-htmltopdfview',['download'=>'pdf','regtype'=>$regtype,'regnumber'=>$regnumber]) }}" target="_blank">
      <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
      </a>
   </div>
   <table align="center" class="table table-striped" id="example" style="width: 60%">
      <thead>
         <tr>
            <th class="text-center">Register Number</th>
            <th class="text-center">Register Type</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($searchresult as $result)
         <tr>
            <!-- <td>{{ $result->reg_type }}</td> -->
            <!--  <td>{{ $result->reg_num }}</td> -->
            <td class="text-center"><u>{{ HTML::link(URL::route('mktmgr-view-chg-reg',['reg_num' => $result->reg_num]), $result->reg_num) }}</u></td>
            <td class="text-center">{{ $result->reg_type }}</td>
            <!-- <td><a href=""><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a> &nbsp;&nbsp;<a href=""><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td> -->
            @endforeach    
      </tbody>
   </table>
   <div class="col-sm-12" align="center">
      <input type="button" name="addinfo" id="addinfo" tabindex="4" onclick="window.location.href ='{{URL::route('mktmgr-bookchgquery')}}'" value="Back" class="btn">
      {{ Form::token()}}
   </div>
   <nav aria-label="..." >
      <ul class="pagination" style="margin-left: 227px">
         <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
         <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
      </ul>
   </nav>
   @else
   <div class="alert alert-danger" role="alert" style="margin-top: 3%">
      There is no row satisfying the conditions <a href="bookchgquery" class="alert-link"> Go Back</a>
   </div>
   @endif
</div>
@stop