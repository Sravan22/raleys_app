@extends('layout.dashboard')
@extends('layout.datejs')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>

<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel_heading">
            <div class="panel-title" >Print Order Query</div>
         </div>
         <div  class="panel_body" >
            <form action="{{URL::route('mktmgr-post-order-query')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Store Number</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" readonly="" id="storenumber" minlength="1" maxlength="3" placeholder="Store Number" name="storenumber" onKeyPress="return StopNonNumeric(this,event)" value="{{{ Session::get('storeid') }}}">
                     @if($errors->has('fromstorenumber'))
            {{ $errors->first('fromstorenumber')}}
            @endif
                  </div>
               </div>
              
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
              
              <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Department Number</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control keyfirst" id="dept_number" minlength="1" maxlength="2" placeholder="From Department" name="dept_number" onKeyPress="return StopNonNumeric(this,event)" tabindex="2" autofocus=""  />
                      @if($errors->has('dept_number'))
                       {{ $errors->first('dept_number')}}
                       @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Order Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control lastdate" id="fstnumber" placeholder="Order Date" name="order_date" tabindex="3">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Type</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Type" name="type" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
               </div>
               
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Status</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Status" name="status">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Total Lines</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Total Lines" name="total_lines">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Total Units</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" readonly id="fstnumber" placeholder="Total Units" name="total_units">
                  </div>
               </div>
               
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 43%">
                        <input type="submit" name="store_submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="reset" name="" id="submit" tabindex="5" value="Reset" class="btn">
                        <input type="reset"  value="Cancel" class="btn keylast" onclick="document.location.href='{{URL::to('/mktmgr/storeorderview')}}'" tabindex="6">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>

               <div class="focusguard" id="focusguard-2" tabindex="7"></div>

            </form>
         </div>
      </div>
   </div>
</div>
@stop
<script type="text/javascript">
   function StopNonNumeric(el, evt)
{
    //var r=e.which?e.which:event.keyCode;
    //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot (thanks ddlab)
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var dotPos = el.value.indexOf(".");
    if( dotPos>-1 && (number[1].length > 3)){
        return false;
    }
    return true;
}
</script>