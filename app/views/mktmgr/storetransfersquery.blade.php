@extends('layout.dashboardstoretransfer')
@extends('layout.datejs')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storetransfersmenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>

<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel_heading">
            <div class="panel-title" >Store Transfers Inquiry</div>
         </div>
         <div  class="panel_body" >
            <form action="{{URL::route('mktmgr-post-store-transfer')}}" class="form-horizontal" method="post" role="form" >
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="from_store_no" class="control-label col-sm-12">From Store Number</label>
                  </div>
                  <div class="col-sm-7">
                    
                     <input type="text" class="form-control keyfirst" id="from_store_no" minlength="1" maxlength="3" placeholder="From Store Number" name="from_store_no" onKeyPress="return StopNonNumeric(this,event) " autofocus="" tabindex="2">
                      
                      @if($errors->has('from_store_no'))
                      {{ $errors->first('from_store_no')}}
                      @endif

                  </div>
               
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="from_dept_no" class="control-label col-sm-12">From Department</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="from_dept_no" minlength="1" maxlength="2" placeholder="From Department" name="from_dept_no" onKeyPress="return StopNonNumeric(this,event)" tabindex="3" >
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Acct to CREDIT</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="from_account" minlength="1" maxlength="5" placeholder="Acct to CREDIT" name="from_account" onKeyPress="return StopNonNumeric(this,event)" tabindex="4">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">To Store No</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="to_store_no" minlength="1" maxlength="3" placeholder="To Store No" name="to_store_no" onKeyPress="return StopNonNumeric(this,event)" tabindex="5">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">To Department</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="to_dept_no" placeholder="To Department" name="to_dept_no" onKeyPress="return StopNonNumeric(this,event)" tabindex="6">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Acct to CHARGE</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="to_account" placeholder="Acct to CHARGE" name="to_account" onKeyPress="return StopNonNumeric(this,event)" tabindex="7">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Transfer Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control lastdate" id="create_date" placeholder="Transfer Date" name="create_date" tabindex="8">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Transfer Number</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="transfer_number" placeholder="Transfer Number" name="transfer_number" onKeyPress="return StopNonNumeric(this,event)" tabindex="9">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Transfer Type</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="transfer_type" placeholder="Transfer Type" name="transfer_type" tabindex="10">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Employee ID</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="employee_id" placeholder="Employee ID" name="employee_id" onKeyPress="return StopNonNumeric(this,event)" tabindex="11">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Status</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Status" name="status" tabindex="12">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Create Date / Time</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" readonly="" class="form-control" id="" placeholder="Create Date / Time" name="date_time">
                  </div>
               </div>
               <div class="form-group padding_bottom" >
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Last Update</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" readonly="" class="form-control" id="" placeholder="Last Update" name="last_update">
                  </div>
               </div>
               <div class="form-group padding_bottom">
                  <div class="row">
                   <div class="col-md-4"></div>
                    <div class="col-md-2"></div>
                     <div class="col-md-4">
                        <input type="submit" name="store_submit" id="submit" tabindex="13" value="Search" class="btn">
                        <input type="button" value="Cancel" class="btn keylast" onClick="document.location.href='{{ URL::route('mktmgr-returnhome')}}'" tabindex="14" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="15"></div>
            </form>
         </div>
      </div>
   </div>
</div>


<script type="text/javascript">
   function StopNonNumeric(el, evt)
{
    //var r=e.which?e.which:event.keyCode;
    //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot (thanks ddlab)
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var dotPos = el.value.indexOf(".");
    if( dotPos>-1 && (number[1].length > 3)){
        return false;
    }
    return true;
}
</script>
@stop