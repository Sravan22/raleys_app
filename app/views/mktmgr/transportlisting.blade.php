@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.recivermenu')
    </header>
 <style>
 
  @media print {
  a[href]:after {
    content: none !important;
  }
}
</style>   
  <div class="container">
    <div class="row">
    <div class="col-md-6">
    <h3>Transport Listing</h3></div>
    <div class="col-md-6">
    <span class="pull-right">
     <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
     <a href="{{ route('pdf-transporterlist',['download'=>'pdf']) }}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
      </a>
      </span>
        </div>
    <table class="table table-striped">
        <thead>
          <tr>
            <th>Transporter Number</th>
            <th>Transporter Name</th>
            <th>City</th>
            <th>State</th>
            <th>Zipcode</th>
            <th>Phone</th>
          </tr>
        </thead>
        <tbody>
        <?php if(count($transports) > 0) { ?>
        @foreach ($transports as $trns)
          <tr>
            <td>{{ $trns->trns_number }}</td>
            <td>{{ $trns->name }}</td>
            <td>{{ $trns->city }}</td>
            <td>{{ $trns->state }}</td>
            <td>{{ $trns->zip_code }}</td>
            <td>{{ $trns->phone }}</td>
          </tr>
      @endforeach
    <?php }
    else { ?>
      <tr><td colspan="6" align="center">No records found!</td></tr>  
    <?php } ?>
        </tbody>
    </table>          
    <?php echo $transports->links(); ?>
</div>
@stop
