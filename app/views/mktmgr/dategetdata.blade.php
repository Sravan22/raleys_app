@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<style type="text/css">
    .makecenter
    {
        margin-left:20px;
    }
    .row.vertical-divider > div[class^="col-"]
    {
      text-align: left;
      padding: 2px;
    }
</style>
<form class="form-horizontal" role="form" method="post" action="{{URL::route('mktmgr-updatebookregcheckoutitem')}}">
  <div class="container" style="">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
        <div id="loginbox" style="margin-top:-20px;" class="mainbox col-xs-6 col-sm-12 col-sm-offset-1">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >REGISTER CHECK OUT</div>
                       </div> 

                 <div style="padding-top:15px" class="panel-body" >
                  
<div class="row horizantal-divider">
  <div class="col-xs-6">    
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Date</label>
     <div class="col-sm-4" style="text-align: left;margin-left: -48px;">
     <input class="form-control col-sm-6" type="text" name="selecteddate" id="selecteddate" value="{{ $dateofinfo }}" 
            readonly="readonly">
    </div>
  </div>
  </div>
 
 <!-- <div class="col-lg-12"> -->
 <div class="col-xs-6">
  <div class="form-horizontal">

        <div class="form-group">
          <label for="#" class="col-sm-2 control-label">Category</label>
           <div class="col-sm-8" style="padding-left:0px;">  
           <input type="text" class="form-control col-sm-4" name="selecteditemsname" id="selecteditemsname" value="{{ $itemname }}" readonly="readonly">
          </div>
         </div>

    
  </div>
 </div>
 <div class="col-xs-12"><hr></div>
   <div class="col-sm-4">
   <div class="col-xs-6">
   <label for="ex1" class="makecenter">Rg/Chkr</label>
   </div>
   <div class="col-xs-6">
   <label for="ex1" class="makecenter">Amount</label>
   </div>
   
   @foreach($resultArray as $rowelement)
      
     <div class="col-xs-6">
       <input class="form-control" id="rg{{ $rowelement->reg_num }}" 
              name="rg{{ $rowelement->reg_num }}" type="text" readonly="readonly" value="{{ $rowelement->reg_num }}">
     </div>
     <div class="col-xs-6">
       <input class="form-control" id="amt{{ $rowelement->reg_num }}" 
              name="amt{{ $rowelement->reg_num }}" type="text" readonly="readonly" 
              value="{{ $rowelement->total }}">
     </div>
   @endforeach

   <div class="col-xs-6">
   <label for="ex1" class="makecenter">Grand Total</label>
   </div>
   <div class="col-xs-6">
   <input class="form-control" id="gtot" name="gtot" type="text" readonly="readonly" 
          value="gt">
   </div>
    </div> 
    
    </div>
    </div>
         <!--  <div class="form-group">
                    <div class="row">
                       <div class="col-sm-12 topspace" align="center">
        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
        <input type="reset" name="login-submit" id="cancel" tabindex="4" value="Cancel" class="btn">
                                                {{ Form::token()}}
                      </div>
                    </div>
                  </div> -->
</div>


 </div>
        </div>  </div>
      </div>
  </form>  
@stop
