@extends('layout.dashboardbookkeepermarket')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
 
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div class="mainbox">
      <div class="panel panel-info">
         <div class="panel_heading">
            <div class="panel-title">BEGINNING LOANS</div>
         </div>
         <div class="panel_body">
            <form action="{{URL::route('mktmgr-reg-beginloan')}}" class="form-horizontal" method="post" role="form">
               <div class="form-group">
                  <label for="reg_number" class="control-label col-sm-6">Register Number</label>
                  <div class="col-sm-6">
                     <select id="reg_number" name="reg_number" class="form-control" value="reg_number">
                        @foreach($registers_num as $row)
                        <option value="{{$row->reg_num}}">{{$row->reg_num}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="form-group padding_bottom">
                  <label for="dateofreg" class="control-label col-sm-6">Date of the Information</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control lastdate" id="dateofreg" placeholder="" name="dateofreg" value="{{ $previewsdate }}"> 
                     @if($errors->has('dateofreg'))
                     {{ $errors->first('dateofreg')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                 <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                       <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                       <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                       {{ Form::token()}}
                    </div>
                 </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
        
<script type="text/javascript">
   
</script>
@stop