@extends('layout.dashboardstoretransfer')
@extends('layout.datejs')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>

<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel_heading" >
            <div class="panel-title" >Out of Stock Items</div>
         </div>
         <div  class="panel_body" >
            <form action="{{URL::route('mktmgr-post-outofstock')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Date Scanned From Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control keyfirst lastdate" id="datascannedfromdate" minlength="1" maxlength="3" name="datascannedfromdate" tabindex="2" autofocus="">
                     <span class="error">@if($errors->has('datascannedfromdate'))
                     {{ $errors->first('datascannedfromdate')}}
                     @endif</span>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Date Scanned To Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control lastdate" id="datascannedtodate" minlength="1" maxlength="3" name="datascannedtodate" tabindex="3">
                     <span class="error">@if($errors->has('datascannedtodate'))
                     {{ $errors->first('datascannedtodate')}}
                     @endif</span>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">SKU Number</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="number" class="form-control" id="" minlength="1" maxlength="2" placeholder="SKU Number" name="skunumber" tabindex="4">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Description</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" readonly="" id=""  placeholder="Description" name="description">
                  </div>
               </div>
             
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Pack Size</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" readonly="" id="" placeholder="Pack Size" name="packsize">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">GL Department Number</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="number" class="form-control" id="" placeholder="GL Department Number" name="gl_department_number" tabindex="5">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">UPC Number</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="UPC Number" name="upc_number" tabindex="6">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Type</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" readonly="" id="" placeholder="Type" name="type">
                  </div>
               </div>
               <div class="form-group padding_bottom">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Date / Time Noted</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control" readonly="" id="" placeholder="" name="date_time_noted">
                  </div>
               </div>
               
               <div class="form-group padding_bottom">
                  <div class="row">
                  <div class="col-md-4"></div>
                    <div class="col-md-2"></div>
                     <div class="col-md-4">
                    
                        <input type="submit" name="store_submit" id="submit" tabindex="7" value="Search" class="btn">
                        <input type="reset" name="" id="submit" tabindex="8" value="Reset" class="btn keylast clearerrors">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="9"></div>
            </form>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
$('.clearerrors').on('click', function(e) {
   //alert('Hiii');
   $('.error').text('');
});
</script>
@stop
