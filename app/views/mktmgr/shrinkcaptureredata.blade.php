<?php
   // echo '<pre>';print_r($dept_no).'<br>';
    //echo '<pre>';print_r($dept_11_tot_value).'<br>';exit; 
   //// echo '<pre>';print_r($dept_03_tot_value).'<br>';
   // echo '<pre>';print_r($dept_04_tot_value).'<br>';exit;
?>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>Shrink Captures for {{ date('m/d/Y',strtotime($shrink_date))  }} </h3>
         </b>
      </div>
   </div>
</header>

<div class="container">
   <div class="row">

      <div class="col-md-6">
          <div class="menu_search_line">
      <div class="dropdown sub">
         <a  href="{{ URL::route('mktmgr-returnhome')}}" >
         Exit
         </a>
      </div>
   </div>
      </div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-shrinkcapturedata',['download'=>'pdf','shrink_date'=>$shrink_date])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>

<div class="container">
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Dept No</th>
            <th>Department Description</th>
            <th>Shrink Captured?</th>
            <th>Shrink Amount </th>
         </tr>
      </thead>
      <tbody>
      
      <?php 
              $params = array(
                        'dept_no' =>  1,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>1</td>
            @if($dept_01_tot_value[0]->dept_01_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Grocery') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_01_tot_value[0]->dept_01_tot_value; }}</td>
            @else
            <td>Grocery</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  2,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>2</td>
            @if($dept_02_tot_value[0]->dept_02_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Liquor') }}</u></td>
            <td>Y</td>
            <td>{{ $dept_02_tot_value[0]->dept_02_tot_value; }}</td>
            @else
            <td>Liquor</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
          <?php 
              $params = array(
                        'dept_no' =>  3,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>3</td>
            @if($dept_03_tot_value[0]->dept_03_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Meat') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_03_tot_value[0]->dept_03_tot_value; }}</td>
            @else
            <td>Meat</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  4,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>4</td>
            @if($dept_04_tot_value[0]->dept_04_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Deli') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_04_tot_value[0]->dept_04_tot_value; }}</td>
            @else
            <td>Deli</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  5,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>5</td>
            @if($dept_05_tot_value[0]->dept_05_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Fresh Service Deli') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_05_tot_value[0]->dept_05_tot_value; }}</td>
            @else
            <td>Fresh Service Deli</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  6,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>6</td>
            @if($dept_06_tot_value[0]->dept_06_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Bakery') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_06_tot_value[0]->dept_06_tot_value; }}</td>
            @else
            <td>Bakery</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  7,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>7</td>
            @if($dept_07_tot_value[0]->dept_07_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Produce') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_07_tot_value[0]->dept_07_tot_value; }}</td>
            @else
            <td>Produce</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  8,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>8</td>
            @if($dept_08_tot_value[0]->dept_08_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Natural Foods') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_08_tot_value[0]->dept_08_tot_value; }}</td>
            @else
            <td>Natural Foods</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  9,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>9</td>
            @if($dept_09_tot_value[0]->dept_09_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'General Merchandise') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_09_tot_value[0]->dept_09_tot_value; }}</td>
            @else
            <td>General Merchandise</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  10,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>10</td>
            @if($dept_10_tot_value[0]->dept_10_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Hot Wok') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_10_tot_value[0]->dept_10_tot_value; }}</td>
            @else
            <td>Hot Wok</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  11,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>11</td>
            @if($dept_11_tot_value[0]->dept_11_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Floral') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_11_tot_value[0]->dept_11_tot_value; }}</td>
            @else
            <td>Floral</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  12,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>12</td>
            @if($dept_12_tot_value[0]->dept_12_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Coffee Shop') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_12_tot_value[0]->dept_12_tot_value; }}</td>
            @else
            <td>Coffee Shop</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  35,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>35</td>
            @if($dept_35_tot_value[0]->dept_35_tot_value > 0)
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-department',$queryString), 'Seafood') }}</u></td>
            <td>Y</td>
            <td>${{ $dept_35_tot_value[0]->dept_35_tot_value; }}</td>
            @else
            <td>Seafood</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>


         {{--  @endforeach --}}
      </tbody>
   </table>
     <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
            
            <input type="button" name="login-submit" id="submit" tabindex="4" onclick="window.location.href='{{URL::route('mktmgr-shrinkcapturereview')}}'" value="Back" class="btn"/>
              {{ Form::token()}}
        </div>
        </div>
  </div>
   {{-- @else
   <div class="alert alert-danger">
      <strong>Alert!</strong> No Store Transfers meet Query criteria.
   </div>
   @endif --}}
</div>
@stop