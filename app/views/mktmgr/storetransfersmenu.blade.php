<div class="container">
 {{--  <div class="menu_search_line"> --}}
    <ul class="navbar dib_float_left">
    
    <li class="dropdown menu_choice">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Store Transfers<span class="caret"></span></a>
        <ul id="menu4" class="dropdown-menu">
        <li><a href="{{ URL::route('mktmgr-storetransfersquery')}}">Query (Alt+a)</a></li>
        {{-- <li><a href="{{ URL::route('mktmgr-storetransfersupdatemsg')}}">Update (Alt+b)</a></li>
        <li><a href="{{ URL::route('mktmgr-storetransfersitemsmsg')}}">Items (Alt+c)</a></li> --}}
        
        </ul>
    </li>

    <li class="dropdown menu_choice">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reports<span class="caret"></span></a>
        <ul id="menu4" class="dropdown-menu">
<li><a href="{{ URL::route('mktmgr-storetransferweeklyreport')}}">Weekly Transfers Accounting Recap Report (Alt+d)</a></li>
{{-- <li><a href="{{ URL::route('mktmgr-storetransferhardcopy')}}">Store Transfer Hardcopy (Alt+e)</a></li> --}}
<li><a href="{{ URL::route('mktmgr-storetransfersquery')}}">Store Transfer Hardcopy (Alt+e)</a></li>
<li><a href="{{ URL::route('mktmgr-storetransfersweeklystatus')}}">Weekly Transfers Status Report (Alt+f)</a></li>
        </ul>
    </li>

    {{-- <li class="menu_choice">
        <a id="seShowEnrollmentModal" href="{{ URL::route('mktmgr-storetransfersoperators')}}">Operators (Alt+g)</a>
    </li> --}}

    <li class="menu_choice">
        <a id="seShowEnrollmentModal" href="{{ URL::route('mktmgr-returnhome')}}" >Exit (Alt+z)</a>
    </li>
    </ul>
{{-- </div> --}}
</div>  