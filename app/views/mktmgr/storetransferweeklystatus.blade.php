@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storetransfersmenu')
</header>
<div class="col-md-12">
 <br>

</div>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel_heading" >
            <div class="panel-title" >Weekly Store Transfers Report</div>
         </div>
         <div  class="panel_body" >
            <form action="{{URL::route('post-weekly-trns-report')}}" class="form-horizontal" method="post" role="form">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>
            <div class="form-group">
                <div class="col-md-12 text-center">
                  <b>Please Key Date Range</b>
                </div>
            </div>
            <br>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="fromdate" class="control-label col-sm-12">From Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control keyfirst" id="fromdate" placeholder="mm/dd/yyyy" name="fromdate" autofocus="" tabindex="2">
                      @if($errors->has('fromdate'))
            {{ $errors->first('fromdate')}}
            @endif
                  </div>
               </div>
               <div class="form-group padding_bottom">
                  <div class="col-md-5">
                     <label for="todate" class="control-label col-sm-12">To Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control" id="todate" placeholder="mm/dd/yyyy" name="todate" tabindex="3">
                    @if($errors->has('todate'))
                    {{ $errors->first('todate')}}
                    @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Copies</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Number of Copies" name="copies" value="1">
                  </div>
               </div>
               
               <div class="form-group padding_bottom">
                  <div class="row">
                     <div class="col-md-4"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="reset" name="" id="submit" tabindex="5" value="Cancel" class="btn keylast">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="6"></div>
            </form>
         </div>
      </div>
   </div>
</div> 
@stop