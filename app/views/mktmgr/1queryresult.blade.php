@extends('layout.dashboard')

@section('page_heading','Receivings >> Printer Selection Menu')
@section('content')
@section('section')
<header class="row">
        @include('mktmgr.recivermenu')
    </header>
<style>
 
  @media print {
  a[href]:after {
    content: none !important;
  }
}
</style>
<div class="container">
          <div class="row">
            <div class="col-md-6"><h3>Browse</h3></div>
            <div class="col-md-6">
                <span class="pull-right">
                    <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
                    <a href="{{ route('pdf-queryresultadvance',['download'=>'pdf' , 'storeID'=>$storeID, 'deptnum'=>$deptnum, 'invoice_date'=>$invoice_date, 'invoice_end_date'=>$invoice_end_date, 'invoice_number'=>invoice_number, 'vendornumber'=>$vendornumber, 'invoice_type'=>$invoice_type, 'status'=>status, 'method_received'=>$method_received]) }}" target="_blank">
                      <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
                    </a>
                </span>
            </div>
          </div>
        </div>

<div class="container">
<!-- <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>
        Print</span></a> -->
       
    <table class="table table-striped" id="example">
        <thead>
          <tr>
            <th>Invoice Created Date/Time</th>
            <th>Invoice Number</th>
            <th>Vendor Name</th>
            <th>Invoice Type</th>
            <th>Status</th>
            <th>Method Received</th>
          </tr>
        </thead>
        <tbody>
       @if(count($receivings) > 0)
        @foreach ($receivings as $vnd)
          <tr>
            <?php $time = strtotime($vnd->create_datetime); ?>
            <td>{{ date("m/d/Y g:i A", $time) }}</td>


           {{--  <td>{{ $vnd->create_datetime }}</td> --}}
            <td><u>{{ HTML::link(URL::route('mktmgr-view-seletedrow',['seq_number' => $vnd->seq_number]), $vnd->id) }}</u></td>
            <td>{{ $vnd->vname }}</td>
           {{--  <td>{{ $vnd->type_code }}</td> --}}
            @if($vnd->type_code == 'D')
            <td>Delivery</td>
            @elseif($vnd->type_code == 'R')
            <td>Return</td>
            @else
            <td>Unknown</td>
            @endif
            {{-- <td>{{ $vnd->status_code }}</td> --}}
            @if($vnd->status_code == 'A') 
            <td>Accepted</td>
            @elseif ($vnd->status_code == 'H')
            <td>On Hold</td>
            @elseif ($vnd->status_code == 'I')
            <td>Incomplete</td>
            @elseif ($vnd->status_code == 'O')
            <td>Open</td>
            @elseif ($vnd->status_code == 'R')
            <td>Reviewed</td>
            @elseif ($vnd->status_code == 'U')
            <td>Uploaded</td>
            @else
            <td>Voided</td>
            @endif
            {{-- <td>{{ $vnd->method_rcvd }}</td> --}}
            @if($vnd->method_rcvd == 'D') 
            <td>Dex</td>
            @elseif ($vnd->method_rcvd == 'R')
            <td>Receiver Scan</td>
            @elseif ($vnd->method_rcvd == 'O')
            <td>Offline</td>
            @elseif ($vnd->method_rcvd == 'N')
            <td>NEX - EDI</td>
            @else
            <td>UNKNOWN</td>
            @endif
            </tr>
        @endforeach
        @else
          <tr><td colspan="7" align="center">No records found!</td></tr>
        @endif
        </tbody>
    </table>          
    <?php //echo $receivings->links(); ?>
</div>

@stop
