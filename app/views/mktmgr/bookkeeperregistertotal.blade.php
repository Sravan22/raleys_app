@extends('layout.dashboardbookkeepermarket')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <div class="mainbox">
      <div class="panel panel-info">
         <div class="panel_heading">
            <div class="panel-title" >BEGINNING LOANS</div>
         </div>
         <div class="panel_body">
            <form action="{{URL::route('mktmgr-regbeginloan-total')}}" class="form-horizontal" method="post" role="form" style="display: block;padding-top: 15px;">
               <div class="form-group padding_bottom">
                  <label for="dateofinfo" class="control-label col-sm-5">Date of the Information</label>
                  <div class="col-sm-7">
                     <input type="date" placeholder="MM-DD-YYYY" name="dateofinfo" id="" autocomplete="off" isimportant="true" class="form-control lastdate" value="{{ $previewsdate }}">
                     @if($errors->has('dateofinfo'))
                     {{ $errors->first('dateofinfo')}}
                     @endif
                  </div>
               </div>
                
               <div class="form-group">
                 <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4 padding_bottom">
                       <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                       <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                       {{ Form::token()}}
                    </div>
                 </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
<script type="text/javascript">
   $(function() {
   $( "#dateofinfo" ).datepicker({  maxDate: new Date() });
   });
</script>
@stop