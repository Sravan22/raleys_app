<div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >COIN {{ $safe_date }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">BOXED</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">ROLLED</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Pennies</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt" id="pennies_box"  name="pennies_box">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt" id="pennies_roll"  name="pennies_roll" >
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly=""  class="form-control " id="pennies_tot"  name="pennies_tot">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Nickles</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt1" id="nickle_box"  name="nickles_box">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt1" id="nickle_roll"  name="nickles_roll">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control " id="nickle_tot"  name="nickle_tot">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Dimes</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt2" id="dimes_box"  name="dimes_box">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt2" id="dimes_roll"  name="dimes_roll">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control" id="dimes_tot"  name="dimes_tot">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Quarters</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt3" id="quarter_box"  name="quarters_box">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt3" id="quarter_roll"  name="quarters_roll" >
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control" id="quarter_tot"  name="quarter_tot">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label">Misc</label>&nbsp;&nbsp;<input type="text" style="width:60px" class="form-control misc_desc" placeholder="0" name="misc_desc">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt4" id="misc_box"  name="misc_box" onblur="sum_of_qty_price(this.value,'misc')">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt4" id="misc_roll"  name="misc_roll" onblur="sum_of_qty_price(this.value,'misc')">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control" id="misc_tot"  name="misc_tot">
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <div class="col-sm-2">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-3" style="text-align: right;">Grand Total</label>
            <div class="col-sm-6">
               <input  class="form-control"  readonly="readonly" placeholder="0.00" id="Coin_GrandTot"  name="Coin_GrandTot">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="submit" style="margin-left: 109px;" name="" id="coin_submit" tabindex="4" id="submit" onclick="return totalsum();"  value="Submit" class="btn">
                  <input type="reset" name="" tabindex="4" value="Cancel" class="btn" onclick="clickCancel()">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
         <!--   </form> -->
      </div>
   </div>