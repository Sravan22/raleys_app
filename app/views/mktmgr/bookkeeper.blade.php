@extends('layout.dashboardbookkeepermarket')
@section('page_heading','Bookkeeper')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-2 col-md-offset-5">
         <img style="margin-left: -181px;" src="{{ asset("assets/images/BooksPro.jpg") }}">
         <div style="margin-left: -7px;margin-top:-50px;"><b>RALEYS / BEL AIR                                 
            AUTOMATED                                     
            BOOKKEEPING SYSTEM </b>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-4">
         <div style="padding: 114px"><b>**** Store#:{{{ Session::get('storeid') }}} ****</b></div>
      </div>
      <div class="col-md-4 col-md-offset-4 text-right">
         <div style="padding: 114px"><b>Date: <?php echo date('m/d/Y'); ?></b></div>
      </div>
      <div class="col-md-12" style="margin-top:-85px;">
         @if($msg)
         <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
            {{  $msg  }}  
         </div>
         @endif
      </div>
   </div>
</div>
@stop