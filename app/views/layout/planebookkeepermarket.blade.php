<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8"/>
	<title>Raley's - Bookkeeper Market</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/custom.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/ralyesmenu.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/subsubmenu.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/select2.min.css") }}" />
	<!-- datepicker -->
	<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css"> -->
</head>
<body>
	@yield('body')
<script type="text/javascript" src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>

	<script type="text/javascript" src="{{ asset("assets/scripts/generalfunctions.js") }}"></script>
	<script src="{{ asset("assets/scripts/frontend.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/jquery.hotkeys-0.7.9.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/select2.full.min.js") }}" type="text/javascript"></script>
    @yield('jssection')
    @include('layout.bkmshortcutkeys')
</body>
</html>