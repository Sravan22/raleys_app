<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8"/>
	<title>Raley's</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
    <title>Raley's - Store Transfers</title>

	<link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/custom.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/ralyesmenu.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/subsubmenu.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/select2.min.css") }}" />
  <link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
</head>
<body>
	@yield('body')
	<script type="text/javascript" src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets/scripts/generalfunctions.js") }}"></script>
	<script src="{{ asset("assets/scripts/frontend.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/jquery.hotkeys-0.7.9.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/select2.full.min.js") }}" type="text/javascript"></script>
    @yield('jssection')
 <script src="{{ asset("assets/scripts/jquery.hotkeys.js") }}" type="text/javascript"></script>
<script type="text/javascript">
           
            function domo(){
                jQuery('#platform-details').html('<code>' + navigator.userAgent + '</code>');
                
                var elements = ["Alt+a","Alt+b","Alt+c","Alt+d","Alt+e","Alt+f", "Alt+g", "Alt+z"];
                
               
                $.each(elements, function(i, e) { 
                  
                   $(document).bind('keydown', elements[i], function assets() {
                       //alert(elements[i]);
                       if(elements[i] == 'Alt+a')
                       {

                          window.location="{{ URL::route('mktmgr-storetransfersquery') }}";
                       }
                       else if(elements[i] == 'Alt+b')
                       {
                          window.location="{{ URL::route('mktmgr-storetransfersupdatemsg') }}";
                       }
                       else if(elements[i] == 'Alt+c')
                       {
                         window.location = "{{ URL::route('mktmgr-storetransfersitemsmsg')}}";
                       }
                       else if(elements[i] == 'Alt+d')
                       {
                          window.location = "{{ URL::route('mktmgr-storetransferweeklyreport')}}";
                       }
                       else if(elements[i] == 'Alt+e')
                       {
                          window.location = "{{ URL::route('mktmgr-storetransferhardcopy')}}";
                       }
                       else if(elements[i] == 'Alt+f')
                       {
                          window.location = "{{ URL::route('mktmgr-storetransfersweeklystatus')}}";
                       }
                       else if(elements[i] == 'Alt+g')
                       {
                          window.location = "{{ URL::route('mktmgr-storetransfersoperators')}}";
                       }
                       else if(elements[i] == 'Alt+z')
                       {
                           window.location = "{{ URL::route('mktmgr-returnhome')}}";
                       }
                       
                   });
                });
                
            }
            
            jQuery(document).ready(domo);
</script>
</body>
</html>