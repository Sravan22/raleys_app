<!--<script type="text/javascript" src="{{ asset("assets/jquery/1.12.4/jquery.min.js") }}"></script>-->
<!--<script src="{{ asset("assets/scripts/jquery-1.4.2.js") }}" type="text/javascript"></script>-->
<script src="{{ asset("assets/scripts/jquery.hotkeys.js") }}" type="text/javascript"></script>
<script type="text/javascript">
           
            function domo(){
                jQuery('#platform-details').html('<code>' + navigator.userAgent + '</code>');
                
                var elements = ["Alt+a","Alt+b","Alt+c","Alt+d","Alt+e","Alt+f", "Alt+g", "Alt+h", "Alt+i", "Alt+j", "Alt+k",
                                "Alt+l", "Alt+m", "Alt+z"];
                
               
                $.each(elements, function(i, e) { 
                  
                   $(document).bind('keydown', elements[i], function assets() {
                       //alert(elements[i]);
                       if(elements[i] == 'Alt+a')
                       {

                          window.location="{{ URL::route('mktmgr-receivings-receivingsofflinereceiving') }}";
                       }
                       else if(elements[i] == 'Alt+b')
                       {
                          window.location="{{ URL::route('mktmgr-receivings-query') }}";
                       }
                       else if(elements[i] == 'Alt+c')
                       {
                         window.location = "{{ URL::route('mktmgr-receivings-itemsmsg')}}";
                       }
                       else if(elements[i] == 'Alt+d')
                       {
                          window.location = "{{ URL::route('mktmgr-receivings-sequery')}}";
                       }
                       else if(elements[i] == 'Alt+e')
                       {
                          window.location = "{{ URL::route('mktmgr-receivings-receivingslogexpense')}}";
                       }
                       else if(elements[i] == 'Alt+f')
                       {
                          window.location = "{{ URL::route('mktmgr-receivings-sereports')}}";
                       }
                       else if(elements[i] == 'Alt+g')
                       {
                          window.location = "{{ URL::route('mktmgr-report-weeklyreport')}}";
                       }
                       else if(elements[i] == 'Alt+h')
                       {
                          window.location = "{{ URL::route('mktmgr-report-offlinereport')}}";
                       }
                       else if(elements[i] == 'Alt+i')
                       {
                          window.location = "{{ URL::route('mktmgr-report-storeinvoice')}}";
                       }
                       else if(elements[i] == 'Alt+j')
                       {
                          window.location = "{{ URL::route('mktmgr-report-costdiscrepancyreport')}}";
                       }
                       else if(elements[i] == 'Alt+k')
                       {
                          window.location = "{{ URL::route('mktmgr-report-transportlisting')}}";
                       }
                       else if(elements[i] == 'Alt+l')
                       {
                           window.location = "{{ URL::route('mktmgr-DEX-invoicedate')}}";
                       }
                       else if(elements[i] == 'Alt+m')
                       {
                           window.location = "{{ URL::route('mktmgr-browse-vendor')}}";
                       }
                       else if(elements[i] == 'Alt+z')
                       {
                           window.location = "{{ URL::route('mktmgr-returnhome')}}";
                       }
                       
                   });
                });
                
            }
            
            jQuery(document).ready(domo);
</script>