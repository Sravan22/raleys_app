@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
         @include('slcadm05.slcadm05menu')
    </header>
    <style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<?php 
    $params = array(
              'download' =>  'pdf',
              'userid' => $report_data['userid'],
              'update_date' => $report_data['update_date'],
              'update_time' => $report_data['update_time'],
              'action_cd' => $report_data['action_cd'],
              'maint_type_cd' => $report_data['maint_type_cd'],
              'item_code' => $report_data['item_code'],
              'mstr_fld_no' => $report_data['mstr_fld_no'],
              'orig_value' => $report_data['orig_value'],
              'new_value' => $report_data['new_value']
              ); 
              $queryString = http_build_query($params);
 ?>
<div class="container">
          <div class="row">
            <div class="col-md-6"><h3>Browse</h3></div>
            <div class="col-md-6">
                <span class="pull-right">
                    <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
                    <a href="{{ route('pdf-slcadm05-detail_report',$queryString) }}" target="_blank">
                    <i class="fa fa-file-pdf-o fa-fw iconsize"></i></a>
                </span>
            </div>
          </div>
        </div>

<div class="container">
<!-- <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>
        Print</span></a> -->
      @if($data) 
    <table class="table table-striped" id="example">
        <thead>
          <tr>
          
            {{-- <th>User Id</th> --}}
            <th> Date/Time</th>
            <th>UPC / PLU / SKU </th>
            <th> Description</th>
            <th>Action</th>
            <th>Fld No</th>
          </tr>
        </thead>

        <tbody>
        @foreach($data as $result)

        <tr>  
          <?php
          $params = array(
                        'seq_no' =>  $result->seq_no
                        ); 
              $queryString = http_build_query($params);
          ?>
            <td><u><a href="{{ URL::route('slcadm05-modi-audit-view',$queryString) }}">{{ date("d/m/Y", strtotime($result->update_date)) }}  {{ $result->update_time }}</a></u></td>
            {{-- <td>{{ date("d/m/Y", strtotime($result->update_date)) }}  {{ $result->update_time }}</td> --}}
            <td>{{ $result->item_code }}</td>
           
            <td>{{ $result->item_desc }}</td>
            @if($result->action_cd == 'L') 
            <td>SIGNIN</td>
            @elseif ($result->action_cd == 'O')
            <td>SIGNOUT</td>
            @elseif ($result->action_cd == 'C')
            <td>SCANNED</td>
            @elseif ($result->action_cd == 'S')
            <td>AUDIT SCAN</td>
            @elseif ($result->action_cd == 'A')
            <td>ADD</td>
            @elseif ($result->action_cd == 'U')
            <td>UPDATE</td>
            @elseif ($result->action_cd == 'D')
            <td>DELETE</td>
            @elseif ($result->action_cd == 'P')
            <td>PRICE CHNG</td>
            @elseif ($result->action_cd == 'F')
            <td>FM NOTE</td>
            @elseif ($result->action_cd == 'W')
            <td>AISLE CHNG</td>
            @elseif ($result->action_cd == 'V')
            <td>TAG QTY CHNG</td>
            @else
            <td>UNKNOWN</td>
            @endif
            <td>{{ $result->mstr_fld_no }}</td>
          </tr>
          

       
        @endforeach

        </tbody>

    </table>
    <input type="button" style="margin-left: 48%" class="btn" name="" onclick="history.go(-1);" value="Back">          
    @else
    <div class="alert alert-danger">
              <strong>Alert!</strong> No Result meet Query criteria.
    </div>
    @endif
   
</div>


@stop
