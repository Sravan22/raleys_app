<?php
 //echo $username;exit;
    //echo '<pre>';print_r($summary_report_query_array);exit;
 ?>
@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
         @include('slcadm05.slcadm05menu')
    </header>
@if($summary_report_query_array) 
<?php 
    $params = array(
              'download' =>  'pdf',
              'userid' => $userid,
              'username' => $username,
              'update_date' => $update_date
              ); 
              $queryString = http_build_query($params);
 ?>
<div class="container">
          <div class="row">
            <div class="col-md-6"><h3>Browse</h3></div>
            <div class="col-md-6">
                <span class="pull-right">
                    <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
                    <a href="{{ route('pdf-slcadm05-summary_report',$queryString) }}" target="_blank">
                    <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
                </span>
            </div>
          </div>
        </div>
@endif
<div class="container">
<!-- <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>
        Print</span></a> -->
      @if($summary_report_query_array) 
    <table class="table table-striped" id="example">
        <thead>
          <tr>
            <th> Userid</th>
            <th>Username</th>
            <th> Update Date and Time </th>
            <th>Seq No</th>
            <th>Main Type</th>
            <th>Action CD</th>
            <th>Item Code</th>
            <th>Item Desc</th>
            <th>Item Dept</th>
            <th>Master Field No</th>
            <th>Original Value</th>
            <th>New Value</th>
          </tr>
        </thead>
        <tbody>
        @for ($i = 0; $i < count($summary_report_query_array); $i++)
        <tr>  

            <td>{{ $summary_report_query_array[$i]['userid'] }}</td>
            <td>{{ $username }}</td>
            <td>{{ date('m/d/Y',strtotime($summary_report_query_array[$i]['update_date'])) }} {{ $summary_report_query_array[$i]['update_time'] }}</td>
            <td>{{ $summary_report_query_array[$i]['seq_no'] }}</td>
            <td>{{ $summary_report_query_array[$i]['maint_type_cd'] }}</td>
            <td>{{ $summary_report_query_array[$i]['action_cd'] }}</td>
            <td>{{ $summary_report_query_array[$i]['item_code'] }}</td>
            <td>{{ $summary_report_query_array[$i]['item_desc'] }}</td>
            <td>{{ $summary_report_query_array[$i]['item_dept'] }}</td>
            <td>{{ $summary_report_query_array[$i]['mstr_fld_no'] }}</td>
            <td>{{ $summary_report_query_array[$i]['orig_value'] }}</td>
            <td>{{ $summary_report_query_array[$i]['new_value'] }}</td>
            
          </tr>
       
        @endfor 
        </tbody>
    </table>          
    @else
    <div class="alert alert-danger">
              <strong>Alert!</strong> No Result meet Query criteria.
    </div>
    @endif
   
</div>


@stop
