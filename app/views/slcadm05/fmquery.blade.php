@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm05.slcadm05fm_menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Fm Note Details</div>
         </div>
         <div class="panel-body" >
            <form action="" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label col-sm-5">User Id</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="User Id" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control" id="" placeholder="Date" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Time</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Time" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">UPC / PLU / SKU</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="UPC / PLU / SKU" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">FM Note</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="FM Note" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop