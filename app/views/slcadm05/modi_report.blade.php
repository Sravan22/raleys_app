@extends('layout.dashboard')
@extends('layout.datejs')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('slcadm05.slcadm05menu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Modification Audit</div>
         </div>
         <div class="panel-body" style="padding-top: 20px;" >
            <form action="{{URL::route('slcadm05-post-modi-query')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>
            <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">User Id</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="userid" placeholder="User Id" name="userid" autofocus="" tabindex="2" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control lastdate" id="update_date" placeholder="Date" name="update_date" tabindex="3" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Time</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="update_time" placeholder="Time" name="update_time" tabindex="4" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Action Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="action_cd" placeholder="Action Code" name="action_cd" tabindex="5" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Maintenance Type</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="maint_type_cd" maxlength="1" placeholder="Maintenance Type" name="maint_type_cd"
                     tabindex="6" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">UPC / PLU / SKU</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="item_code" placeholder="UPC / PLU / SKU" name="item_code" tabindex="7" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Field Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="mstr_fld_no" placeholder="Field Number" name="mstr_fld_no" tabindex="8" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Original Value</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="orig_value" placeholder="Original Value" name="orig_value" tabindex="9" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5"> New Value</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="new_value" placeholder=" New Value" name="new_value" tabindex="10" />
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="login-submit" id="submit" tabindex="11" value="Search" class="btn" />
                        <input type="reset" value="Reset" class="btn" tabindex="12" />
                        <input type="reset" id="reset" value="Cancel" class="btn" onclick="document.location.href='{{URL::to('slcadm05/return-home')}}'" tabindex="13" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="14"></div>

            </form>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>


<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
<script type="text/javascript">
$('#focusguard-2').on('focus', function() {
  $('#userid').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#reset').focus();
});

</script>
@stop