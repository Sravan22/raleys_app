@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('slcadm05.slcadm05menu') --}}
</header>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">SLIC System Reports</div>
         </div>
         
         <form action="{{URL::route('slcadm05-post-slic-report')}}" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return validateForm()">
            <div style="padding-top:30px" class="panel-body" >
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Enter Selection </label>
                  </div>
                  <div class="col-sm-7">
                     <select id="slic_report" name="slic_report" class="form-control" value="" autofocus="" tabindex="2">
                        <option value="">Select</option>
                        <option value="1">Modification Audit Report</option>
                        <option value="2">FM Notes Report</option>
                        {{-- <option value="3">Batch Admin Menu</option> --}}
                     </select>
                     @if($errors->has('slic_report'))
                     {{ $errors->first('slic_report')}}
                     @endif
                     {{-- @if($message!='')
                     {{ $message }}
                     @endif --}}
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 48%">
                        <input type="submit" name="login-submit" id="submit" tabindex="3" value="Submit" class="btn">
                        <input type="reset" name="" id="reset" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="5"></div>
         </form>
         </div>
      </div>
   </div>
</div>
@stop
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right !important;
   }
</style>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>
<script type="text/javascript">
   $(function() {
      $('#focusguard-2').on('focus', function() {
  $('#slic_report').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#reset').focus();
});

   });
   function validateForm()
   {
      var slic_report = document.getElementById("slic_report").value;
      if(slic_report == '')
      {
         alert('Please Select any Report');
         $('#slic_report').focus();
         return false;
      }
   }
   
</script>
