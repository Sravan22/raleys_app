@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
        {{-- @include('slcadm05.slcadm05menu') --}}
    </header>

 
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">SLIC System Reports</div>
         </div>
         <div class="form-group" style="padding-top:20px;  " align="center">
         <table>
            <thead >
               <tr >
                  <th class="text-center">
                     Report ID
                  </th>
                  <th class="text-center">
                     Description
                  </th>
               </tr>
            </thead>
            <tbody>
            <tr>
               <td>
                  &nbsp;
               </td>
            </tr>
               <tr>
                  <td class="text-center">
                     1
                  </td>
                  
                  <td>
                     Modification Audit Report
                  </td>
               </tr>
               <tr>
                  <td class="text-center">
                     2
                  </td>
                  <td>
                     FM Notes Report
                  </td>
               </tr>
               <tr>
                  <td class="text-center">
                     3  
                  </td>
                  <td>
                     Batch Admin Menu
                  </td>
               </tr>
            </tbody>

         </table>
            
         </div>
         <form action="{{URL::route('slcadm05-post-slic-report')}}" class="form-horizontal" method="post" role="form" style="display: block;">
         <div style="padding-top:30px" class="panel-body" >
            
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Enter Slection </label>
                  </div>
                  
                  <div class="col-sm-7">
                     <input type="text" class="form-control" name="slic_report">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                      @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop
<style type="text/css">
   .form-horizontal .control-label {
      text-align: right !important;
   }
</style>