<?php
 // echo '<pre>';print_r($query_array);exit();
?>
@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('slcadm05.slcadm05menu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Modification Audit</div>
         </div>
         <div class="panel-body" style="padding-top: 20px;" >
            <form class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">User Id</label>
                  <div class="col-sm-6">
                     <input type="text" style="width:47px;" class="form-control" width="10px" value="{{ $query_array[0]['userid'] }}" readonly> <div style="margin-left: 53px;margin-top: -32px;">
                      <?php 
                        $user_name = DB::select('SELECT name  as user_name  FROM sluser
     WHERE userid = "'. $query_array[0]['userid'].'" ');
                        $user_name_array = json_decode(json_encode($user_name), true);
                      ?>
                     <input type="text" style="width:135px;" class="form-control" disabled="" name="" value="{{ $user_name_array[0]['user_name'] }}">
                     </div>
                    
                     
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Date</label>
                  <div class="col-sm-6">
                    <input type="date" class="form-control" id="update_date" placeholder="Date" value="{{ $query_array[0]['update_date'] }}"  name="update_date" disabled> 

                   
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Time</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="update_time" placeholder="Time" name="update_time" value="{{$query_array[0]['update_time']}}" disabled>
                    
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Action Code</label>
                  <div class="col-sm-6">
                     <input type="text" style="width:47px;" class="form-control" id="action_cd" placeholder="Action Code" name="action_cd" value="{{$query_array[0]['action_cd']}}" disabled="">
                  </div >
                   
                  <div style="margin-left: 302px; position: absolute;">
                    <input type="text" style="width:135px;" class="form-control" disabled="" name="" value="{{$query_array[0]['item_desc']}}">
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Maintenance Type</label>
                  <div class="col-sm-6">
                     <input type="text" style="width:47px;" class="form-control" id="maint_type_cd" maxlength="1" placeholder="Maintenance Type" name="maint_type_cd" value="{{$query_array[0]['maint_type_cd']}}" disabled="">
                    <div style="margin-left: 55px; margin-top: -32px; position: absolute;">
                    <?php
                    if($query_array[0]['maint_type_cd'] == 'I' )
                    {
                      ?>
                        
                       <input type="text" style="width:135px;" class="form-control"  value="IMMEDIATE" disabled="">
                       
                  <?php
                    }
                     
                    elseif($query_array[0]['maint_type_cd'] == 'B')
                    {
                      ?>
                         <input type="text" style="width:135px;" class="form-control"  value="BATCH" disabled="">
                  <?php  }
                    else
                    { ?>
                        <input type="text" style="width:135px;" class="form-control"  name="" value="" disabled="">
                    <?php
                    }
                    ?>
                    </div>
                 
               </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">UPC / PLU / SKU</label>
                  <div class="col-sm-6">
                     <input type="text" style="width:47px;" class="form-control" id="item_code" placeholder="UPC / PLU / SKU" name="item_code" value="{{$query_array[0]['mstr_fld_no']}}" disabled="">
                    
                  </div>
                  <div style="margin-left: 302px; position: absolute;">
                   <input type="text" style="width:135px;" class="form-control"  value="N/A" disabled=""> 
                                                    
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Field Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="mstr_fld_no" placeholder="Field Number" name="mstr_fld_no" value="{{$query_array[0]['mstr_fld_no']}}" disabled="">
                    
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Original Value</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="orig_value" placeholder="Original Value" name="orig_value" value="{{$query_array[0]['orig_value']}}" disabled="">
                   
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5"> New Value</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="new_value" placeholder=" New Value" name="new_value"  value="{{$query_array[0]['new_value']}}" disabled="">
                     
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 50%" >
                       
                        <input type="button" value="Go Back" class="btn" onclick="document.location.href='{{URL::to('slcadm05/post-modi-query')}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop