@extends('layout.dashboard')
@section('content')
@section('section')

<div class="containerhome homepageicons"> 
   <!--  <div class="row"> -->
           
           <a href="{{ URL::route('socscn01-socdeptfunctions')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Dept Functions
            </div>
            </div></a>
            

           <a href="{{ URL::route('socscn01-socreports')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
             <div class="caption">
               Reports
            </div>
            </div></a>
            

           
            <a href="{{ URL::route('socscn01-socretailpricechk')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Retail Price Chk
            </div>
            </div></a>
            
            <a href="{{ URL::route('socscn01-socdsdreceiving')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                DSD Receiving
            </div>
            </div></a>
            
            <a href="{{ URL::route('socscn01-socqueryitem')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Query Item
            </div>
            </div></a>

            <a href="{{ URL::route('socscn01-socstoretransfer')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Store Transfers
            </div>
            </div></a>

            <a href="{{ URL::route('socscn01-socpricesaleschk')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Price/Sales Chk
            </div>
            </div></a>
           
        <!-- </div> -->
</div>
@stop
