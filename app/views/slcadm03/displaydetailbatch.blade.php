<?php 
  //echo '<pre>';print_r($data->disp_type);exit;
?>

@extends('layout.dashboard')
@section('page_heading','Display Batches')
@section('content')
@section('section')
<header class="row">
   {{-- @include('slcadm03.slcadm03menu') --}}
</header>
<div class="container">
   <div class="dropdown sub">
      <a  href="{{ URL::route('slcadm03-returnhome')}}" >
      Exit
      </a>
   </div>
  
    @if(($batch_function=='D' || $batch_function=='S')  && $disp_type!='D')
   <a href="{{ URL::route('slcadm03-batch-display-detail',array('type'=>$batch_function,'batch'=>$data->batch_no,'disp_type'=>'D'))}}" class="btn btn-primary" style="float:right;">View Batch Details</a>
   @endif
   
</div>
 <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>
<div class="container" style="padding-top:10px;">
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Batch</th>
            <th>{{$data->function_desc}}  Description</th>
            <th>Batch Load</th>
            <th>Eff Date</th>
            <th>Item Count</th>
            <th>Tags Print</th>
            <th>Regst Apply</th>
            <th>Rpt Cd</th>
         </tr>
      </thead>
      <tbody>
           @foreach($data->results as $row)
      <tr>
      <td>{{$row->batch}}</td>       
        <td>{{$row->desc}}</td>
        <td>{{date('m/d',  strtotime($row->batch_loaded))}}</td>
        <td>{{date('m/d',  strtotime($row->eff_date))}}</td>
        <td>{{$row->item_count}}</td>
        @if($row->tags_printed == '1899-12-31')
        <td><b>---</b></td>
        @elseif($row->tags_printed == '1900-01-01')
        <td><b>***</b></td>
        @elseif($row->tags_printed)
        <td>{{ date('m/d', strtotime($row->tags_printed)) }}</td>
        @else
        <td></td>
        @endif
        @if($row->reg_applied)
        <td>{{date('m/d',  strtotime($row->reg_applied))}}</td>
        @else
        <td></td>
        @endif
        @if($row->report_code == 0)
        <td></td>
        @else
        <td>**</td>
        @endif

      </tr>
      
       @endforeach
    
      </tbody>
   </table>
</div>

@if($disp_type=="T")
<div class="container">
   <div class="row">
      <div class="col-md-6 verticalLine">
      <b>Sign Counts:</b>
         <table class="table table-striped">
            <thead>
               <tr>
                  <th>Type</th>
                  <th>Items</th>
                  <th>Signs</th>
               </tr>
            </thead>
            <tbody>
                @if(!empty($data->disp_tag['sign_array']))
                @foreach($data->disp_tag['sign_array'] as $sign)
                
               <tr>
                  <td> {{$sign['sign_type']}}</td>
                   <td> {{$sign['item_count']}}</td>
                  <td> {{$sign['sign_count']}}</td>
               </tr>
               @endforeach
               @endif
            </tbody>
         </table>
      </div>
      <div class="col-md-6">
      <b>Tag Counts:</b>
         <table class="table table-striped">
            <thead>
               <tr>
                  <th>Type</th>
                  <th>Items</th>
                  <th>Tags</th>
               </tr>
            </thead>
            <tbody>
                @if(!empty($data->disp_tag['tag_array']))
                @foreach($data->disp_tag['tag_array'] as $sign)
                
               <tr>
                  <td> {{$sign['tag_type']}}</td>
                   <td> {{$sign['item_count']}}</td>
                  <td> {{$sign['tag_count']}}</td>
               </tr>
               @endforeach
               @endif
             </tbody>
         </table>
      </div>
   </div>
</div>
@endif

@if($disp_type=="D")
<div class="container">
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Dept</th>
            <th>Ad On</th>
            <th>Ad Off</th>
            <th>OnTPR</th>
            <th>OffTPR</th>
            <th>Price</th>
            <th>Item</th>
            <th>Disco</th>
            <th>New</th>
            <th>Total</th>
         </tr>
      </thead>
      <tbody>
          @if(!empty($data->disp_depts))
         <?php $total=array();
          for($i=0;$i<=7;$i++) {
            $total[$i]=0;
          }          
          ?>
          
          
          @foreach($data->disp_depts as $dept=>$row)
          
        
          <?php 
          for($i=0;$i<=7;$i++) {
            $total[$i]+=$row[$i];
          } 
          ?>
          
         <tr>
            <td><u>{{ HTML::link(URL::route('slcadm03-batch-department-details',array('type'=>$batch_function,'batch'=>$data->batch_no,'dept_id'=>$dept)), $dept ) }}</u></td>
            <td>{{$row[0]}}</td>
            <td>{{$row[1]}}</td>
           <td>{{$row[2]}}</td>
           <td>{{$row[3]}}</td>
           <td>{{$row[4]}}</td>
           <td>{{$row[5]}}</td>
           <td>{{$row[6]}}</td>
           <td>{{$row[7]}}</td>
           
            <td>{{$row['total']}}</td>
         </tr>
         
         @endforeach
         
         <tr>
             <td><b>Total</b></td>
              <?php  
              $total_main=0;
          for($i=0;$i<=7;$i++) { 
              $total_main+=$total[$i];  ?>
             <td><?php  echo $total[$i]; ?></td>
         <?php }  ?>
           
            <td><?php echo $total_main; ?></td>
         </tr>
         
         @endif
        
      </tbody>
   </table>
</div>



@endif

<div class="container" style="padding-top:10px;">

   
        <div class="col-sm-12" align="center">
         
           <?php if($disp_type=="D") { ?>
            <input type="button" value="Back" class="btn"  onclick="window.location.href='{{URL::route('slcadm03-batch-display-detail',array('type'=>$batch_function,'batch'=>$data->batch_no))}}'" />
           {{-- <input type="button" value="Back" class="btn" onclick="history.go(-1);"> --}}
           <?php } else {?>
              <input type="button" value="Back" class="btn"  onclick="window.location.href='{{URL::route('slcadm03-displayallbatch',array('type'=>$batch_function,'batch'=>$data->batch_no))}}'" />
           {{--  <input type="button" value="Back" class="btn" onclick="history.go(-1);"> --}}
           <?php } ?>
        </div>
     
</div>

@stop



<style type="text/css">
  .verticalLine {
    border-right-style:  dotted;
}
</style>