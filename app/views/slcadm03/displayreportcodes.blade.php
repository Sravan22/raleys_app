<?php 
   //echo '<pre>';print_r($query); exit();
   ?>
@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
    <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>Report Codes</h3>
         </b>
      </div>
   </div>
</header>
 
<div class="container" style="padding-top:10px;">
   <form action="#" class="form-horizontal" method="post" role="form" style="display: block;">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <table class="table table-striped">
           <thead>
              <tr>
                 <th class="text-center">TYPE</th>
                 <th class="text-center">End Date</th>
                 <th class="text-center">Code</th>
              </tr>
           </thead>
           <tbody>
              <?php
                 for($i = 0 ; $i < count($slrep_cdArray); $i++)
                 
                 {
                 ?>
              <tr>
                 <td class="text-center">{{ $slrep_cdArray[$i]['type'] }}</td>
                 <td class="text-center">{{ date("m/d/Y", strtotime($slrep_cdArray[$i]['tpr_end'] )) }}</td>
                 <td class="text-center">{{ $slrep_cdArray[$i]['report_code'] }}</td>
              </tr>
              <?php
                 }
                 ?>
                 <tr>
                 <td class="text-center">&nbsp;</td>
                 <td class="text-center"><input type="button" class="btn" name="" onclick="history.go(-1);" value="Go Back"> </td>
                 <td class="text-center">&nbsp;</td>
              </tr>
           </tbody>
        </table>
      </div>
      <div class="col-md-3"></div>
   </form>
</div>
@stop