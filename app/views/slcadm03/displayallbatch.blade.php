 

@extends('layout.dashboard')
@section('page_heading','Display Batches')
@section('content')
@section('section')
<header class="row">
   {{-- @include('slcadm03.slcadm03menu') --}}
</header>
<div class="container">
   <div class="col-md-6">
     {{--  <a  class="btn" href="{{ URL::route('slcadm03-returnhome')}}" >Exit</a> --}}
      <input type="button" class="btn" name=""  onclick="window.location.href='{{ URL::route('slcadm03-returnhome')}}'" value="Exit">
   </div>
   @if($batch_function == "A" || $batch_function == "Z")
   <div class="col-md-6" style="text-align: right;"><input type="button" data-toggle="modal" data-target="#myModal"  value="Enter Batch Details" class="btn btn-primary"></div>
   @endif 
</div>
<div class="container" style="padding-top:10px;">
   <style type="text/css">
      tr a {
      text-decoration: underline;
      }
   </style>
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Batch</th>
            <th>{{$data->function_desc}}  Description</th>
            <th>Batch Load</th>
            <th>Eff Date</th>
            <th>Item Count</th>
            <th>Tags Print</th>
            <th>Regst Apply</th>
            <th>Rpt Cd</th>
         </tr>
      </thead>
      <tbody>
      @if($data->results)
         @foreach($data->results as $row)
         <tr>
            @if($batch_function == "A" || $batch_function == "Z")
            <td>{{ $row->batch }}</td>
            @elseif($batch_function == "P" || $batch_function == "R")
            <td><u>{{ HTML::link(URL::route('slcadm03-print-details',array('batch_function'=>$batch_function,'batch'=>$row->batch,'user_id'=>$user_id,'storeno'=>$storeno)), $row->batch ) }}</u></td>
            @else
            <td><u>{{ HTML::link(URL::route('slcadm03-batch-display-detail',array('type'=>$batch_function,'batch'=>$row->batch)), $row->batch ) }}</u></td>
            @endif
            <td>{{$row->desc}}</td>
            <td>{{date('m/d',  strtotime($row->batch_loaded))}}</td>
            <td>{{date('m/d',  strtotime($row->eff_date))}}</td>
            <td style="text-align: right; padding-right:70px;">{{$row->item_count}}</td>
            @if($row->tags_printed == '1899-12-31')
            <td><b>---</b></td>
            @elseif($row->tags_printed == '1900-01-01')
            <td><b>***</b></td>
            @elseif($row->tags_printed)
            <td>{{ date('m/d', strtotime($row->tags_printed)) }}</td>
            @else
            <td></td>
            @endif
            @if($row->reg_applied)
            <td>{{date('m/d',  strtotime($row->reg_applied))}}</td>
            @else
            <td></td>
            @endif
            @if($row->report_code == 0)
            <td></td>
            @else
            <td>**</td>
            @endif
         </tr>
         @endforeach
      @else
        <tr><td colspan="8" class="text-center">No Batches to Display</td></tr>
      @endif   
      </tbody>
   </table>
   <div class="row">
      <div class="col-sm-12" align="center"> {{$pagination->links()}}</div>
   </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center"><strong> Enter Beginning & Ending Batch Details </strong></h4>
         </div>
         <div class="modal-body">
         <form method="post" action="#" onsubmit="return validateForm()">
            <div class="panel-body">
               <div class="table-responsive">
                  <div class="form-group">
                     <label for="reg_num" class="control-label col-sm-6" style="    text-align: right;">Beginning Batch Details</label>
                     <div class="col-sm-6">
                        <select class="form-control" id="beg_batch_no" name="beg_batch_no" <?php if(empty($data->results)){ echo 'disabled'; } ?>>
                            @if($data->results)

                              @foreach($data->results as $row)
                                <option value="{{ $row->batch }}">{{ $row->batch }}</option>
                              @endforeach
                            @else
                            <option value="">No Batch to Restore</option>
                            @endif  
                        </select>
                     </div>
                  </div>
                  @if($batch_function == "A")
                    <div class="form-group">
                       <label for="reg_num" class="control-label col-sm-6" style="    text-align: right;">Ending Batch Details</label>
                       <div class="col-sm-6">
                          <select class="form-control" id="end_batch_no" name="end_batch_no">
                              <option value="">Select Ending Batch Details</option>
                              @foreach($data->results as $row)
                                <option value="{{ $row->batch }}">{{ $row->batch }}</option>
                              @endforeach
                          </select>
                       </div>
                    </div>
                    @endif
               </div>
               <hr>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="button" <?php if(empty($data->results)){ echo 'disabled'; } ?> name="login-submit" id="batch-submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" data-dismiss="modal" class="btn" />
                        {{ Form::token()}}
                     </div>
                  </div>
                  <div class="row batchsuccessmsg"></div>
               </div>
            </div>
          </form>
         </div>
        {{--  <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div> --}}
      </div>
   </div>
</div>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>  
<script src="{{ asset("assets/jquery/bootstrap/3.3.7/bootstrap.min.js") }}"></script>
<script type="text/javascript">


      $(document).ready(function(){
          
         $('#batch-submit').on('click', function(e) {
           
            var storeno = '{{ $storeno }}';
            var batch_function = '{{ $batch_function }}';
            var beg_batch_no = $('#beg_batch_no').val();
            var end_batch_no = $('#end_batch_no').val();
            //alert(batch_function);return false;
            //alert(end_batch_no);
            if(batch_function == "A")
            {
              if(end_batch_no != '')
              {
                if(end_batch_no<beg_batch_no)
                {
                  alert('Ending Batch Should be greater than Beginning Batch');return false;
                }
              }
            }
            

            
            //alert(beg_batch_no);alert(end_batch_no);alert(batch_function);return false;
            //alert($(this).parent().find(".invoice_date").text());return false;
             $.ajax({
                         url: "updatebatchdetails",
                          type: "POST", 
                         data: { beg_batch_no: beg_batch_no, end_batch_no:end_batch_no,storeno:storeno,batch_function:batch_function},
                         //dataType:'json',
                         // beforeSend: function () {
                         //     $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please wait till the Balance is transfered...</div>");
                         // },
                         success: function(data) {
                          //alert(data);return false;
                           //$('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data+"</div>");
                           $('.batchsuccessmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data+"</div>");
                           //$('#myModal').modal('hide');
                           setTimeout(function() { $('#myModal').modal('hide');location.reload(); }, 1800);
                            
                         }
                       });

            
         });
      });
</script>
@stop