<?php 
   //echo '<pre>';print_r($sign_cur_Array);exit;
   //echo $storeno;exit;
   ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="col-md-6 text-center" style="margin-left:290px;">
      <b>
         <h4>TAGS & SIGNS QUEUED @if($batch_function == "R") TO REPRINT @else FOR PRINT @endif<br><br>
            Batch: {{ $batch_no }}&nbsp;&nbsp; &nbsp; &nbsp;Date:{{date("m-d-Y");}}
         </h4>
      </b>
   </div>
</header>
<br>
<div class="container">
   <div class="row">
      <div class="panel-body col-md-6 col-md-offset-3 text-center" style="padding-top:30px" >
         <div class="table-responsive">
            @if(!empty($sign_cur_Array))
            <table class="table table-bordered table-colored">
               <thead>
                  <tr>
                     <th class="text-center">Stock</th>
                     <th class="text-center">
                        Sign Type
                     </th>
                     <th class="text-center">Copies Requested</th>
                     <th class="text-center">Sheet Count</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($sign_cur_Array as $row)
                  <tr>
                     <?php 
                        $ws_sign_count_query =  DB::select('SELECT sign_count
                        as ws_sign_count
                        FROM ssstock
                        WHERE stock_code = "'.$row->stock_code.'"');
                        $ws_sign_count = $ws_sign_count_query[0]->ws_sign_count;
                        
                        ?>
                     <td><u><a href="#" id="{{ $user_id;}}^{{ $password;}}^{{ $batch_no;}}^{{ $row->sign_id;}}^{{$storeno}}" onclick="print_submit(this.id)">{{ $row->stock_code }}</a></u></td>
                     <td>{{ $row->sign_name }}</td>
                     <td>{{ $row->copies }}</td>
                     <?php 
                        $ws_remainder = round($row->copies/$ws_sign_count);
                        if($ws_remainder == 0)
                        {
                          $ws_remainder = 1;
                        }
                        ?>
                     <td>{{ $ws_remainder }}</td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
            @else
            <div class="col-md-12">
               <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert"></button>
                  No Tags/Signs To Print.  
               </div>
            </div>
            @endif
            <div class="col-sm-12" align="center">
               <input type="button" name="login-submit" id="submit" tabindex="4" onclick="history.go(-1);" value="Cancel" class="btn">
            </div>
            <br><br>
            <div class="col-sm-12 showmsg" align="center">
                
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   function print_submit(id){
    //alert(id);return false;
    var batch_function = '{{  $batch_function }}'; 
    //alert(batch_function);return false;
    if(confirm("Insert Stock\n FACE DOWN;TOP SIDE TO FRONT OF PRINTER \nwhen ready, click Ok print")){
       //alert(id);
       var myVal = id;
    var myVal = myVal.split('^');
    //alert(myVal);
    var user_id = myVal[0];
    var password = myVal[1];
   
    var batch_no = myVal[2];
    var sign_id = myVal[3];
    var storeno = myVal[4];
    //console.log(id);return false;
    //alert("values "+user_id);
   $.ajax({
   
         url: "slcadm03_prnt_sh_file",
          type: "GET", 
         data: { user_id: user_id,password:password, batch_no:batch_no,sign_id:sign_id,batch_function:batch_function},
          beforeSend: function () {   
                      $('.showmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please Wait until the print is done....</div>");
                     },
         success: function(data) {
             //alert(data);return false;
            if(data == 'printed'){
              //alert('Print Done');
                $.ajax({
                  url: "slcadm03_update_tag_print",
                  type: "GET",
                  data: { user_id: user_id,batch_no:batch_no,storeno:storeno},
                  success: function(data) {
                      //alert(data);return false;
                        if(data == 'updated'){
                        $('.showmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Printing Done</div>");
                        setTimeout(function() {
                         $(".showmsg").hide();location.reload(); }, 2500);
                      }
                    }
                });
   
            }    
            else
            {
              alert('Printing Aborted');
            } 
         }
       });
    }
    else{
   
    }
   }
   
</script>
@endsection
@stop