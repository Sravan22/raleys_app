<?php 
   //echo '<pre>';print_r($stock_array);exit;
 ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3 style="margin-left:-74px;">Store {{ $storeno }} Batch Maintenance Menu</h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div class="row">
      <div class="col-md-5" style="margin-left:27%">
         <table class="table table-striped" id="example">
            <thead>
               {{-- <tr>
                  <th class="text-center">Stock Code</th>
                  <th class="text-center">Stock Description</th>
               </tr> --}}
            </thead>
            <tbody>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center"> 1 </td>
                  <td class="text-left" style="padding-left:150px;"> <a href="{{ URL::route('slcadm03-displayallbatch',array('type'=>'d','storeno'=>$storeno))}}"><u>Display Batches</u> </a></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center"> 2 </td>
                  <td class="text-left" style="padding-left:150px;"><a href="{{ URL::route('slcadm03-displayallbatch',array('type'=>'p','storeno'=>$storeno,'user_id'=>$user_id))}}"><u> Print Batch Tags/Signs</u></a></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center"> 3 </td>
                  <td class="text-left" style="padding-left:150px;"><a href="{{ URL::route('slcadm03-displayallbatch',array('storeno'=>$storeno,'tbatch'=>0,'type'=>'A'))}}"><u>Apply Batch to POS File</u></a></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center"> 4 </td>
                  <td class="text-left" style="padding-left:150px;"><a href="#"><u class="disable">Delete Batch</u></a></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center"> 5 </td>
                  <td class="text-left" style="padding-left:150px;"><a href="{{ URL::route('slcadm03-displayallbatch',array('type'=>'r','storeno'=>$storeno,'user_id'=>$user_id))}}"><u>Reprint Tags/Signs</u></a></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center"> 6 </td>
                  <td class="text-left" style="padding-left:150px;"><a href="{{ URL::route('slcadm03-displayallbatch',array('type'=>'s'))}}"><u>Summary Pages</u></a></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center"> 7 </td>
                  <td class="text-left" style="padding-left:150px;"><a href="{{ URL::route('slcadm03-displayallbatch',array('storeno'=>$storeno,'tbatch'=>0,'type'=>'Z'))}}"><u>Restore Batch</u></a></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center"> 8 </td>
                  <td class="text-left" style="padding-left:150px;"><a href="{{ URL::route('slcadm03-displayreportcode',array('storeno'=>$storeno))}}"><u>Display Report Codes</u></a></td>
               </tr>
               
            </tbody>
         </table>
      </div>

   </div>
  
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
   ::-webkit-input-placeholder {
   text-transform: initial;
}

:-moz-placeholder { 
   text-transform: initial;
}

::-moz-placeholder {  
   text-transform: initial;
}

:-ms-input-placeholder { 
   text-transform: initial;
}
</style>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>
<script>
$(document).ready(function(){
    $(".disable").click(function(){
        alert("Function Disabled for Delete Batch.");
    });
});
</script>
@stop