<?php 
   //echo '<pre>';print_r($sign_cur_Array);exit;
   //echo $storeno;exit;
   ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="col-md-6 text-center" style="margin-left:290px;">
      <b>
         <h4>TAGS & SIGNS QUEUED @if($batch_function == "R") TO REPRINT @else FOR PRINT @endif<br><br>
            Batch: {{ $batch_no }}&nbsp;&nbsp; &nbsp; &nbsp;Date:{{date("m-d-Y");}}
         </h4>
      </b>
   </div>
</header>
<br>
<div class="container">
   <div class="row">
      <div class="panel-body col-md-6 col-md-offset-3 text-center" style="padding-top:30px" >
         <div class="table-responsive">
            @if(!empty($sign_cur_Array))
            <table class="table table-bordered table-colored">
               <thead>
                  <tr>
                     <th class="text-center">Stock</th>
                     <th class="text-center">
                        Sign Type
                     </th>
                     <th class="text-center">Copies Requested</th>
                     <th class="text-center">Sheet Count</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($sign_cur_Array as $row)
                  <tr>
                     <?php 
                        $ws_sign_count_query =  DB::select('SELECT sign_count
                        as ws_sign_count
                        FROM ssstock
                        WHERE stock_code = "'.$row->stock_code.'"');
                        $ws_sign_count = $ws_sign_count_query[0]->ws_sign_count;
                        
                        ?>
                     <td><u><a href="#" id="{{ $user_id }}^{{ $password }}^{{ $batch_no }}^{{ $row->sign_id;}}^{{$storeno}}"  data-toggle="modal" data-target="#myModal{{ $row->sign_id }}" data-id="{{ $user_id;}}^{{ $password;}}^{{ $batch_no;}}^{{ $row->sign_id;}}^{{$storeno}}" class="stock_code">{{ $row->stock_code }}</a></u></td>
                     <td>{{ $row->sign_name }}</td>
                     <td>{{ $row->copies }}</td>
                     <?php 
                        $ws_remainder = round($row->copies/$ws_sign_count);
                        if($ws_remainder == 0)
                        {
                          $ws_remainder = 1;
                        }
                        ?>
                     <td>{{ $ws_remainder }}</td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
            @else
            <div class="col-md-12">
               <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert"></button>
                  No Tags/Signs To Print.  
               </div>
            </div>
            @endif
            <div class="col-sm-12" align="center">
               <input type="button" name="login-submit" id="submit" tabindex="4" onclick="history.go(-1);" value="Cancel" class="btn">
            </div>
            <br><br>
            <div class="col-sm-12 showmsg" align="center">
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center"><strong> Enter First SKU To Print </strong></h4>
         </div>
         <div class="modal-body">
            {{--  
            <p>Some text in the modal.</p>
            --}}
            <div class="panel-body">
               <div class="table-responsive">
                  <div class="form-group">
                     <label for="skuno"  class="control-label col-sm-2" style="padding-top: 5px;">SKU</label>
                     <div class="col-sm-10">
                        <input type="text" style="width: 100%;" class="form-control skuno" name="skuno" placeholder="Numeric or Blank">
                     </div>
                     <span class="upbss" style="display: none;"></span>
                     <div class="col-sm-12 showskumsg" align="center">
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <input type="button" class="btn btn-info" id="sku-submit" name="" value="Submit">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>  
<script src="{{ asset("assets/jquery/bootstrap/3.3.7/bootstrap.min.js") }}"></script>
<script type="text/javascript">
   $(document).ready(function() {
    $(".stock_code").click(function() {
        $('.skuno').val('');
        var id = $(this).attr('id');
        $(".upbss").text(id);
        $(this).attr("data-target", "#myModal")
    });

    $("#sku-submit").click(function() {
        var skuno = $('.skuno').val();
        var myVal = $(".upbss").text();
        var myVal = myVal.split('^');
        //alert(myVal);
        var batch_no = myVal[2];
        var sign_id = myVal[3]; 
        //alert(skuno);
        $.ajax({

            url: "check_sku_no",
            type: "GET",
            data: {
                skuno: skuno,
                batch_no: batch_no,
                sign_id: sign_id
            },
            success: function(data) {
                //alert(data);return false;
                if (data == 'notfound') {
                    
                    $('.showskumsg').html("Could Not Find SKU");
                    return false;
                }
                else {

                   
                   if (confirm("Insert Stock\n FACE DOWN;TOP SIDE TO FRONT OF PRINTER \nwhen ready, click Ok print")) {
                    var skuno = $('.skuno').val();
                     //alert(data);return false;
                    //alert(skuno);
                    var myVal = $(".upbss").text();
                    var myVal = myVal.split('^');
                    //alert(myVal);
                    var user_id = myVal[0];
                    var password = myVal[1];
                    var batch_no = myVal[2];
                    var sign_id = myVal[3];
                    var storeno = myVal[4];
                    var skuno = data;
                    //alert(skuno);return false;
                    $("#myModal").hide();

                    $.ajax({

                        url: "slcadm03_prnt_sh_file",
                        type: "GET",
                        data: {
                            user_id: user_id,
                            password: password,
                            batch_no: batch_no,
                            sign_id: sign_id,
                            batch_function: batch_function,
                            sku_number: skuno
                        },
                        beforeSend: function() {
                            $('.showmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please Wait until the print is done....</div>");
                        },
                        success: function(data) {
                            //alert(data);
                            //return false;
                            if (data == 'printed') {
                                //alert('Print Done');
                                $.ajax({
                                    url: "slcadm03_update_tag_print",
                                    type: "GET",
                                    data: {
                                        user_id: user_id,
                                        batch_no: batch_no,
                                        storeno: storeno
                                    },
                                    success: function(data) {
                                        //alert(data);return false;
                                        if (data == 'updated') {
                                            $('.showmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Printing Done</div>");
                                            setTimeout(function() {
                                                $(".showmsg").hide();
                                                location.reload();
                                            }, 2500);
                                        }
                                    }
                                });

                            } else {
                                alert('Printing Aborted');
                            }
                        }
                    });
                } 
                }
                

            }
        });
        var batch_function = '{{  $batch_function }}';

    })

});
</script>
@endsection
@stop