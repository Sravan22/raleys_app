@extends('layout.dashboard')
@section('page_heading','Display Batches')
@section('content')
@section('section')
<header class="row">
   @include('slcadm03.slcadm03menu')
</header>
<div class="container">
   <div class="dropdown sub">
      <a  href="{{ URL::route('slcadm03-returnhome')}}" >
      Exit
      </a>
   </div>
   <a href="{{ URL::route('slcadm03-view-batch-details')}}" class="btn btn-primary" style="float:right;">View Batch Details</a>
</div>
<div class="container" style="padding-top:10px;">
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Batch</th>
            <th>Display Batches  Description</th>
            <th>Batch Load</th>
            <th>Eff Date</th>
            <th>Item Count</th>
            <th>Tags Print</th>
            <th>Regst Apply</th>
            <th>Rpt Cd</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td> 6279</td>
            <td>Floral Price Changes</td>
            <td>12/15</td>
            <td>12/21</td>
            <td>7</td>
            <td>---</td>
            <td></td>
            <td></td>
         </tr>
      </tbody>
   </table>
</div>
<div class="container">
   <div class="row">
      <div class="col-md-6 verticalLine">
      <b>Sign Counts:</b>
         <table class="table table-striped">
            <thead>
               <tr>
                  <th>Type</th>
                  <th>Items</th>
                  <th>Signs</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td> G CLEARANCE</td>
                  <td>15</td>
                  <td>S</td>
               </tr>
            </tbody>
         </table>
      </div>
      <div class="col-md-6">
      <b>Tag Counts:</b>
         <table class="table table-striped">
            <thead>
               <tr>
                  <th>Type</th>
                  <th>Items</th>
                  <th>Signs</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td> T WHITE TAG </td>
                  <td>46</td>
                  <td>49</td>
               </tr>
               <tr>
                  <td> Y RALEY D'S NO SAVE </td>
                  <td>1</td>
                  <td>1</td>
               </tr>
             </tbody>
         </table>
      </div>
   </div>
</div>
@stop

<style type="text/css">
  .verticalLine {
    border-right-style:  dotted;
}
</style>