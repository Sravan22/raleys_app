@extends('layout.planebookkeepermarket')
@section('body')

<div class="container-fluid">
  <div class="row">
        <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ URL::route('user-home') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
        <div class="col-md-6 text-right">
           @if(Auth::check())
           <div class="dropdown sub" style="border-color:#fff;">
           {{ Auth::user()->username }}
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
               <span class="glyphicon glyphicon-cog"></span> 
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                 <li><a href="#">Profile</a></li>
                 <li><a href="#">Change Password</a></li>
                 <li><a href="{{ URL::route('account-sign-out')}}">Sign Out</a></li>
            </ul>
        </div>
  
    @endif
           
        </div>
  </div>
</div>
      <div class="row">  
           
        @yield('section')
      
        
            </div>
        
    
@stop