@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('sign1.menu')
</header>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">AD Signs</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('post-new-signs')}}" class="form-horizontal" method="post" role="form" style="display: block;">
         <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Select AD Begin Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control" id="begin_date" placeholder="Select AD Begin Date" name="begin_date" >
                  </div> 
               </div>
             <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Department</label>
                  </div>
                 
                  <div class="col-sm-7">
                     <select id="dept_name_number" name="dept_name_number" class="form-control" value="dept_name_number">
                        <option value="">Select</option>
                        @for ($i = 0; $i < count($departments); $i++)
                        <option value="{{ $departments[$i]['type_code'] }}">{{ $departments[$i]['name'] }}</option>
                        @endfor
                     </select>
                     @if($errors->has('dept_name_number'))
            {{ $errors->first('dept_name_number')}}
            @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop
