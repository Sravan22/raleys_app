@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
   @include('sign1.menu')
</header>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">Sign Stock :A --Daily  Department Hour</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Open Daily From</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Open Daily From" name="" >
                  </div> 
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Until</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="Until" class="form-control" id="" placeholder="Until" name="" >
                  </div> 
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop
