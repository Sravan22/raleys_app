
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>{{-- Select Ad Begin Date --}} </h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="row">
         <div class="col-md-8" style="margin-left:182px;">
            <h4> STORE SIGN STOCKS AND NAMES
            </h4>
            <br>  
         </div>
      </div>
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >{{-- SAFE COUNT --}}</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{ URL::route('store-sign-stocks-names') }}" data-href="{{URL::route('sign1-requestdisplay')}}"  id="myform" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return validateForm()">
               <div class="row">
               <input type="hidden" name="ad_begin_date" value="{{ $ad_begin_date }}">
               <input type="hidden" name="gl_dept_no" value="{{ $gl_dept_no }}">
                  <div class="form-group">
                     <label style="padding-left:80px;padding-top:3px;" for="inputPassword" class="control-label col-sm-5">Store Sign Stocks</label>
                     <div class="col-sm-7">
                        <select name="stock_option" id="stock_option" class="form-control">
                           <option value="">Select Store Sign Stocks</option>
                           @for($i=0;$i<count($stock_option_array);$i++)
                           <option value='{{ $stock_option_array[$i]['stock_code'] }}^{{ $stock_option_array[$i]['stock_desc'] }}'>{{ $stock_option_array[$i]['stock_code'] }} : {{ $stock_option_array[$i]['stock_desc'] }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label style="padding-left:80px;padding-top:3px;" for="inputPassword" class="control-label col-sm-5">Store Sign Names</label>
                     <div class="col-sm-7">
                        <select name="sign_option" id="sign_option" class="form-control">
                           <option value="">Select Store Sign Names</option>
                           {{-- @for($i=0;$i<count($gl_dept_array);$i++)
                           <option value="{{ $gl_dept_array[$i]['gl_dept_no'] }}^{{ $gl_dept_array[$i]['description'] }}">{{ $gl_dept_array[$i]['description'] }}</option>
                           @endfor --}}
                        </select>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        @if($stock_or_upc == "Upc")
                        <input type="button" name="login-submit" id="stock_sign_submit" tabindex="4" value="Submit" class="btn">
                        @else
                          <input type="submit" name="login-submit" id="stock_sign_submit" tabindex="4" value="Submit" class="btn">
                        @endif

                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('/home')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               </br>
            </form>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>  
<script src="{{ asset("assets/jquery/bootstrap/3.3.7/bootstrap.min.js") }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    //location.reload();

   $('#stock_sign_submit').click(function(){

    var stock_option = document.getElementById("stock_option").value;
    if (stock_option == "") {
        alert("Store Sign Stocks must be required");
        document.getElementById("stock_option").focus();
        return false;
    }
    var sign_option = document.getElementById("sign_option").value;
    if (sign_option == "") {
        alert("Store Sign Names must be required");
        document.getElementById("sign_option").focus();
        return false;
    }
    var stock_or_upc = '{{ $stock_or_upc }}';
    var seq_no = '{{ $seq_no }}';
    var url = 'sign1-requestdisplay';
    // alert(stock_option);
    // alert(sign_option);
    //  alert(stock_or_upc);
    // alert(seq_no);return false;
    var stock_option = stock_option.split('^');
    var stock_code = stock_option[0];
    var sign_option = sign_option.split('^');
    var sign_id = sign_option[0];
    if(stock_or_upc == 'Upc')
    {
       //alert('hii');
        //document.getElementById("#myform").removeAttribute("action"); 
       // window.location.href=url+'?seq_no='seq_no+'&sign_id='+sign_id+'&stock_code='+stock_code;
       //alert($('#myform').attr('data-href')+'?seq_no='+seq_no+'&sign_id='+sign_id+'&stock_code='+stock_code);return false;
       window.location.href=$('#myform').attr('data-href')+'?seq_no='+seq_no+'&sign_id='+sign_id+'&stock_code='+stock_code;
    }
    // else
    // {
    //     window.location.href=$('#myform').attr('action');
    // }

    });

 });
// function validateForm() {
    
   
//     // alert(stock_code);
//     // alert(sign_option);return false; 
// }
   $(document).ready(function() {
     $("#stock_option").on('change',function(){
          
            $('#sign_option').find('option').remove().end().append('<option value="">Select</option>');
           var aurl = 'sign_option';
            //alert(aurl);
            $.ajax({
             url: "sign_option",
              type: "POST", 
             data: { stock_option: $(this).val()},
             dataType:'json',
             // beforeSend: function () {
             //    // $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please wait till the Balance is transfered...</div>");
             // },
             success: function(data) {
               //alert(data);
               $.each(data, function(i, data) {
               $('#sign_option').append("<option value='" + data.sign_id +"^"+ data.sign_name + "'>" + data.sign_name + "</option>");
             });
             }
           });
   });
           
      });

   $(document).one('ready',function(){
   if( window.localStorage )
  {
    if( !localStorage.getItem( 'firstLoad' ) )
    {
      localStorage[ 'firstLoad' ] = true;
      location.reload(true);
    }  
    else
      localStorage.removeItem( 'firstLoad' );
  }
   
});
</script>
@stop