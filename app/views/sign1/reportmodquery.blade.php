@extends('layout.dashboard')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('sign1.sign1reportmodmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Modification Audit<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('sign1-report-mod-browse')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">User ID</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="userid" placeholder="User ID" name="userid">
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                  </div>
               </div>

   
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control lastdate" id="update_date" placeholder="Date" name="update_date">
                     @if($errors->has('update_date'))
                     {{ $errors->first('update_date')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Time</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="update_time" placeholder="Time" name="update_time">
                     @if($errors->has('update_time'))
                     {{ $errors->first('update_time')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Action Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="action_cd" placeholder="Action Code" name="action_cd">
                     @if($errors->has('action_cd'))
                     {{ $errors->first('action_cd')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Maintenance Type</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="maint_type_cd" placeholder="Maintenance Type" name="maint_type_cd">
                     @if($errors->has('maint_type_cd'))
                     {{ $errors->first('maint_type_cd')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5"> UPC / PLU / SKU</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="item_code" placeholder=" UPC / PLU / SKU" name="item_code">
                     @if($errors->has('item_code'))
                     {{ $errors->first('item_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Field Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="mstr_fld_no" placeholder="Field Number" name="mstr_fld_no">
                     @if($errors->has('mstr_fld_no'))
                     {{ $errors->first('mstr_fld_no')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Original Value</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="orig_value" placeholder="Original Value" name="orig_value">
                     @if($errors->has('orig_value'))
                     {{ $errors->first('orig_value')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">New Value</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="new_value" placeholder="New Value" name="new_value">
                     @if($errors->has('new_value'))
                     {{ $errors->first('new_value')}}
                     @endif
                  </div>
               </div>
               

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="reset" name="accept-submit" id="submit" tabindex="4" value="Reset" class="btn">
                          <input type="reset" id="" tabindex="4" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::route('sign_option_for_stock_code',array('stock_code'=>'Z'))}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@section('jssection')
 @parent
  
  
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
 <script src="{{ URL::asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script>
  $( function() {
    $( "#update_time" ).datetimepicker({
        viewSelect: 'hour',
        startView:"hour",
        autoclose:true,
        format:"hh:ii:ss"
    });
  } );
  </script>
   
      @endsection
@stop