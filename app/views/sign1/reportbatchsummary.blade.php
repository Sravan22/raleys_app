@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('sign1.sign1reportbatchmenu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    
</div>
   
     
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
        <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>


<div class="col-md-12 text-right">
          <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
          
        </div>

<h3 style="text-align:center;">Batch Summary Screen</h3>



<table class="table table-striped">
   
    <tr>
        
        <td>Batch Id:{{$data->batch_id}}</td>
        <td>User Id:{{$data->userid}}</td>
    </tr>
    <tr>
       
        <td> Batch Type:{{$data->batch_type}} </td>
         <td>Status:{{$data->stat_desc}}</td>
    </tr>
    <tr>
        <td> Created:{{date('m/d/Y',strtotime($data->created_date))}}</td>
        <td>&nbsp;</td>
       
    </tr>
</table>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Item</th>
        <th>Fld</th>
        <th>Chg Cd</th>
        <th> New Value </th>
        <th> Active</th>
       
    </tr>
    </thead>
    <tbody>
        
        @foreach($data->results as $row)
      <tr>
          <td><a href="{{URL::route('sign1-report-batch-summary-view',array('userid'=>$data->usrid,'hdr_seq_no'=>$data->sqno,'seq_no'=>$data->sqno))}}"> {{$row->item_cd}}</a></td>        
        
        <td>{{$row->field_no}}</td>
        <td>{{$row->change_cd}}</td>
        <td>{{$row->new_value}}</td>        
        <td>{{$row->org_value}}</td>
        <td>{{$row->active_sw}}</td>
      </tr>
      
      @endforeach
      
    </tbody>
  </table>


 <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        
                       
                         <input type="button" onclick="window.location.href='{{URL::route('sign1-report-batch-view',array('userid'=>$data->usrid,'seq_no'=>$data->sqno))}}'" name="login-submit" id="submit" tabindex="4" value="Back" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>

 </form>
</div>
@stop
