<?php 
   //echo '<pre>';print_r($results);exit;
   ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
<style type="text/css">
  .style{
    float:right;margin-right:-120px;
  }
</style>
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>AD SIGNS for {{ $department_name }}</h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div class="row">
      <div class="col-md-8" style="margin-left:182px;">
         <strong>Sign Stock:<span class="stock_name"></span></strong> <br>
         <strong>Name:<span class="sign_name"></span></strong><br>
        {{--  <strong>ID:<span class="sign_id"></span></strong> --}}
         <span class="sign_id hidden" data-param2=""></span>
         {{-- <input type="button" data-toggle="modal" data-target="#myModal" style="float:right;margin-right:-119px;" value="Stock" class="btn btn-primary"> --}}
         <a href="{{ route('signs_for_stockcode_N')}}" style="float:right;margin-left:-127px;" class="btn btn-info">Signs Queued(Ctrl + c)</a>
         <?php 
                      $params = array(
                                'ad_begin_date' =>  $ad_begin_date,
                                'gl_dept_no' =>  $gl_dept_no,
                                'stock_or_upc' => 'Stock'
                                ); 
                      $queryString = http_build_query($params);
                    ?>
         <u>{{ HTML::link(URL::route('sstypsgn-dynamic-from',$queryString), 'Stock',array('class' => 'btn btn-primary style')) }}</u>
         {{-- <a href="#" class="btn btn-primary" >Stock</a> --}}
         <a href="#" class="btn btn-primary" style="float:right;margin-right:-190px;">Print</a>
         
      </div>
      <div id="loginbox" class="mainbox col-sm-12">
         <div style="padding-top:30px" class="panel-body" >
            <div class="table-responsive ">
               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th class="text-center">UPC</th>
                        <th class="text-center">Item Description</th>
                        <th class="text-center">Size</th>
                        <th class="text-center">Sale Price</th>
                        <th class="text-center">Regular Price</th>
                        <th class="text-center">Prt</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if($results)
                     @foreach($results as $row)
                     <tr>
                       <?php 
                      $params = array(
                                'seq_no' =>  $row->seq_no,
                                // 'sign_id' =>  $sign_id,
                                // 'stock_code' =>  $stock_code,
                                'ad_begin_date' =>  $ad_begin_date,
                                'gl_dept_no' =>  $gl_dept_no,
                                'stock_or_upc' => 'Upc'
                                ); 
                      $queryString = http_build_query($params);
                    ?>
                     <td class="text-center"><u>{{ HTML::link(URL::route('sstypsgn-dynamic-from',$queryString), $row->upc_number) }}</u></td>
                       {{-- <td class="text-center"> <a href="javascript:void(0)" data-href="{{URL::route('sstypsgn-dynamic-from')}}" class="ancUrl" data-param1="{{ $row->seq_no }}">{{ $row->upc_number }}</a></td> --}}
                        {{-- <td class="text-center">{{ $row->upc_number }}</td> --}}
                        <td class="text-left" style="padding-left:100px;">{{ $row->description }}</td>
                        <td class="text-center">{{ $row->size }}</td>
                        <td class="text-center">{{ $row->sale_price }}</td>
                        <td class="text-center">{{ $row->regular_price }}</td>
                        <td class="text-center">{{ $row->printed_sw }}</td>
                     </tr>
                     @endforeach
                     @else
                     <tr>
                        <td colspan="6">
                           <div class="alert alert-danger" role="alert">
                              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
                              THERE ARE NO AD ITEMS TO DISPLAY FOR THIS DEPARTMENT 
                           </div>
                        </td>
                     </tr>
                     @endif   
                  </tbody>
               </table>
               {{-- {{ $pagination->links() }} --}}
               <input type="button" style="margin-left:45%;" onclick="history.go(-1);" name="cancel-submit" id="submit" tabindex="" value="Cancel" class="btn">
            </div>
         </div>
         <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title text-center"><strong> S T O R E  &nbsp;&nbsp; S I G N S </strong></h4>
                  </div>
                  <div class="modal-body">
                     <div class="panel-body">
                        <div class="form-group">
                           <label style="padding-left:80px;padding-top:3px;" for="inputPassword" class="control-label col-sm-5">Store Sign Stocks</label>
                           <div class="col-sm-7">
                              <select name="stock_option" id="stock_option" class="form-control">
                                 <option value="">Store Sign Stocks</option>
                                 @for($i=0;$i<count($stock_option_array);$i++)
                                 <option value='{{ $stock_option_array[$i]['stock_code'] }}^{{ $stock_option_array[$i]['stock_desc'] }}'>{{ $stock_option_array[$i]['stock_code'] }} : {{ $stock_option_array[$i]['stock_desc'] }}</option>
                                 @endfor
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label style="padding-left:80px;padding-top:3px;" for="inputPassword" class="control-label col-sm-5">Store Sign Names</label>
                           <div class="col-sm-7">
                              <select name="sign_option" id="sign_option" class="form-control">
                                 <option>Select</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                              <div class="col-sm-12" align="center">
                                 <input type="submit" name="login-submit" id="storesignsubmit" tabindex="4" value="Submit" class="btn">
                                 <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('/home')}}'" />
                                  
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>  
<script src="{{ asset("assets/jquery/bootstrap/3.3.7/bootstrap.min.js") }}"></script>
<script type="text/javascript">
$(document).ready(function() {
   jQuery(document).bind('keydown', 'Ctrl+c', function(e) {  
  // right arrow key pressed
    window.location="{{ URL::route('signs_for_stockcode_N') }}";return false;
});
   $("#stock_option").on('change',function(){
       
         $('#sign_option').find('option').remove().end().append('<option value="">Select</option>');
        //var aurl = 'sign_option';
         //alert(url);
        $.ajax({
          url: "sign_option",
           type: "POST", 
          data: { stock_option: $(this).val()},
          dataType:'json',
          // beforeSend: function () {
          //    // $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please wait till the Balance is transfered...</div>");
          // },
          success: function(data) {
            //alert(data);
            $.each(data, function(i, data) {
            $('#sign_option').append("<option value='" + data.sign_id +"^"+ data.sign_name + "'>" + data.sign_name + "</option>");
          });
          }
        });

        
   });

   $('#storesignsubmit').on('click', function(e) {
            //e.preventDefault();

             var stock_option = $('#stock_option').val();
             var stock_options = stock_option.split('^');
             var stock_name = stock_options[1];
             var sign_data = $('#sign_option').val();
              var sign_data = sign_data.split('^');
              var sign_name = sign_data[1];
              var sign_id = sign_data[0];
              $(".sign_id").attr("data-param2", sign_id);
              
              //alert(sign_id);exit;
        //       $.ajax({
        //   url: "sstypsgn-dynamic-from",
        //    type: "GET", 
        //   data: { sign_id: sign_id},
        //   //dataType:'json',
        //   // beforeSend: function () {
        //   //    // $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please wait till the Balance is transfered...</div>");
        //   // },
        //   success: function(data) {
        //     alert(data);  
        //     //return true;          
        //   }
        // });
             $('.stock_name').text(stock_name);
             $('.sign_name').text(sign_name);
             $('.sign_id').text(sign_id);
             $('#stock_option').val('');  
             $('#sign_option').val(''); 
            // $('.target_invoice_date').val($(this).parent().find(".invoice_date").text());
            // //alert($(this).parent().find(".invoice_date").text());
             $('#myModal').modal('hide');
         });

   $('.ancUrl').click(function(){
    window.location.href=$(this).attr('data-href')+'?param1='+$(this).attr('data-param1')+'&param2='+$('.sign_id').attr('data-param2');
})

});
 
// $(document).ready(function() {
//    // var username = ($(".sign_id").val());
//     var sign_id =  $('.sign_id').attr("data-param2");
//     $.cookie('sign_id', sign_id ); 
//     alert($.cookie('sign_id'));  
// });   
</script>


@stop