@extends('layout.dashboard')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('sign1.sign1reportfmmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Fm Notes Detail<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('sign1-report-fm-report-browse')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">User ID</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="userid" placeholder="User ID" name="userid">
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                  </div>
               </div>

   
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">From Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control lastdate" id="from_date" placeholder="Date" name="from_date">
                     @if($errors->has('from_date'))
                     {{ $errors->first('from_date')}}
                     @endif
                  </div>
               </div>
                
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">To Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control lastdate" id="to_date" placeholder="Date" name="to_date">
                     @if($errors->has('to_date'))
                     {{ $errors->first('to_date')}}
                     @endif
                  </div>
               </div>
               
              
              
               
              
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Search" class="btn">
                       <input type="reset" name="accept-submit" id="submit" tabindex="4" value="Reset" class="btn">
                         <input type="reset" id="" tabindex="4" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::route('sign1-report-fm-query')}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>

 
@stop