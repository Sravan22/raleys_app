<?php 
   //echo '<pre>';print_r($sssign_array);exit;
   ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>S T O R E &nbsp;&nbsp;&nbsp;&nbsp; S I G N  &nbsp;&nbsp;&nbsp;&nbsp; N A M E S </h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
<div class="row">
   <div class="col-md-5" style="margin-left: 30%;">
      <table class="table table-striped" id="example">
         <thead>
            {{-- 
            <tr>
               <th class="text-center">S. No</th>
               <th class="text-center">Seq No</th>
               <th class="text-center">Stock Description</th>
            </tr>
            --}}
         </thead>
         <tbody>
            <?php $i = 0; ?>
            @foreach($sssign_array as $row)
            <?php
               //  $params = array(
               //           'seq_no' =>  $row['seq_no'],
               //      'sign_id' =>  $row['sign_id'],
               //      'stock_code' => $row['stock_code']
               //           ); 
               // $queryString = http_build_query($params);
               
               
                $params = array( 
                    'sign_id' =>  $row['sign_id'],
                    'stock_code' => $row['stock_code']
                         ); 
               $queryString = http_build_query($params);
                ?>
            <tr>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               {{-- <td class="text-center"><u>{{ HTML::link(URL::route('sign1-requestdisplay',$queryString), $i+1 ) }}</u></td> --}}
               <td class="text-center"> {{ $i+1 }}.</td>
               <td class="text-left sign_name{{$i+1}}" style="padding-left:125px;">{{ $row['sign_name'] }}</td>
               <td class="text-left signid{{$i+1}} hidden" style="padding-left:125px;">{{ $row['sign_id'] }}</td>
               <td class="text-left stock_code{{$i+1}} hidden" style="padding-left:125px;">{{ $row['stock_code'] }}</td>
            </tr>
            <?php $i++; ?>
            @endforeach
         </tbody>
      </table>
   </div>
</div>

  <div class="row">
     <div class="form-group" style="margin-left: 59px;">
        <div class="col-md-5" style="padding-right:0px;">
           <span class="pull-right"><label for="inputPassword" class="control-label col-sm-12">Select Sign :</label></span>
        </div>
        <div class="col-sm-7" style="padding-left:0px;">
           <input type="text" class="form-control" id="selectedsignno" name="selectedsignno" maxlength="2" minlength="1" autocomplete="off" onKeyPress="return StopNonNumeric(this,event)" style="width:106px;" placeholder="">
           <span class="errormsg"></span>
           {{-- @if($errors->has('stock_code'))
           {{ $errors->first('stock_code') }}
           @endif  --}}
        </div>
     </div>
  </div>
  <div class="row">
     <div class="form-group" style="margin-left: 84px;">
        <div class="row">
           <div class="col-md-8"  style="margin-left:81px;" align="center">
              <input type="button" name="submit" id="submit" tabindex="4" value="Submit" class="btn">
              <input type="reset" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('home')}}'"/>
            </div>
        </div>
     </div>
  </div>

<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
   ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   text-transform: initial;
   }
</style>
<script type="text/javascript">
   function goBack() {
       window.history.back();
   }
   function StopNonNumeric(el, evt)
       {
           var charCode = (evt.which) ? evt.which : event.keyCode;
           var number = el.value.split('.');
           if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
               return false;
           }
           //just one dot (thanks ddlab)
           if(number.length>1 && charCode == 46){
                return false;
           }
           //get the carat position
           var dotPos = el.value.indexOf(".");
           if( dotPos>-1 && (number[1].length > 3)){
               return false;
           }
           return true;
       }

   
    $(document).ready(function() {
      $(document).bind('keypress', function(e) {
            if(e.keyCode==13){
                 $('#submit').trigger('click');
             }
        });
      $('#selectedsignno').focus();
        $("#submit").click(function(){
         
          var dollarivalue = {{ $i }};
          var selectedsignno = $("#selectedsignno").val();
          if(selectedsignno <= 0 || selectedsignno > dollarivalue)
          {
            //alert('Invalid Sign Selection');return false;
            $('.errormsg').text('Invalid Sign Selection');
            $('#selectedsignno').val('');
            $('#selectedsignno').focus();
            return false;
          }
          //var sign_name = $(".sign_name" + selectedsignno).text();
          var sign_id = $(".signid" + selectedsignno).text();
          var stock_code = $(".stock_code" + selectedsignno).text();stock_code
          window.location.href='remotlyrequestdisplay'+'?sign_id='+sign_id+'&stock_code='+stock_code;
      });
        
        
   
    });
   
   
   
</script>
@stop