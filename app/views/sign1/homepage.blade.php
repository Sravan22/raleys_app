<?php 
   //echo '<pre>';print_r($stock_array);exit;
 ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3 style="margin-left:-74px;">S T O R E &nbsp;&nbsp;&nbsp;&nbsp; S I G N &nbsp;&nbsp;&nbsp;&nbsp; S T O C K S </h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div class="row">
      <div class="col-md-5" style="margin-left:27%">
         <table class="table table-striped" id="example">
            <thead>
               {{-- <tr>
                  <th class="text-center">Stock Code</th>
                  <th class="text-center">Stock Description</th>
               </tr> --}}
            </thead>
            <tbody>
               @foreach($stock_array as $row)
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="text-center">{{ $row['stock_code'] }}</td>
                  <td class="text-left" style="padding-left:150px;">{{ $row['stock_desc'] }}</td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
   <div class="row" style="margin-left: 37px">
      <form action="{{URL::route('sign_option_for_stock_code')}}" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return validateForm()">
         <div class="form-group">
            <div class="col-md-5" style="padding-right:0px;">
               <span class="pull-right"><label for="inputPassword" class="control-label col-sm-12">Key Stock Code :</label></span>
            </div>
            <div class="col-sm-7" style="padding-left:0px;">
               <input type="text" class="form-control" id="stock_code" name="stock_code" maxlength="1" minlength="1" autocomplete="off" style="width:106px;text-transform: uppercase;" placeholder="Stock Code">
               @if($errors->has('stock_code'))
               {{ $errors->first('stock_code') }}
               @endif 
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-md-8"  style="margin-left:81px;" align="center">
                  <input type="submit" name="submit" id="submit" tabindex="4" value="Submit" class="btn">
                  <input type="reset" value="Cancel" class="btn"/>
                  {{ Form::token()}}
               </div>
            </div>
         </div>
      </form>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
   ::-webkit-input-placeholder {
   text-transform: initial;
}

:-moz-placeholder { 
   text-transform: initial;
}

::-moz-placeholder {  
   text-transform: initial;
}

:-ms-input-placeholder { 
   text-transform: initial;
}
</style>
 <script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>

<script type="text/javascript">
	$('#stock_code').focus();
	function validateForm() {

    var stock_code = document.getElementById("stock_code").value.length;
    if (stock_code == 0) {
        alert('Stock Code is Required');
        $('#stock_code').focus();
        return false;
    }
    // $(document).ready(function() {

    //     var stock_code = document.getElementById("stock_code").value.length;
    //     //alert(stock_code);
    //     if (stock_code != 0) {
    //         	 stock_code = $("#stock_code").val();
    //         	 alert(stock_code);return false;
    //     }return false;
    // });
}



</script>
@stop