<?php
//echo $num_args;exit;
   //echo '<pre>';print_r($sign_line_array);exit;
?>
@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row"></header>
<div class="col-md-12">
   <br>
 {{--   @if(Session::has('message'))
      {{ Session::get('message'); }} 
   @endif --}}
    
   @if(Session::has('message'))
   <div class="alert alert-success successmsg" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('message'); }}                                
   </div>
   @endif
   <?php 
    if(Session::has('print_data')){
      //echo '<pre>';print_r(Session::get('print_data'));
      $print_data = array();
      $print_data = Session::get('print_data');
      //echo '<pre>';print_r($print_data);
      //echo $print_data[0];
    }

   ?>
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-7 col-md-offset-2 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" > Sign Stock: {{ $stock_code }} -- {{ $sign_name }}</div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('dynamic-form-post')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               @if(!empty($sign_line_array))
               @for($i=0;$i<count($sign_line_array);$i++)
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">{{trim($sign_line_array[$i]['element_prompt'] ) }}</label>
                  <div class="col-sm-6">
                     <input type="hidden" name="stock_code" value="{{ $stock_code }}">
                     <input type="hidden" name="sign_id" value="{{ $sign_id }}">
                     <input type="hidden" name="num_args" value="{{ $num_args }}">
                     <input type="hidden" name="special_proc_cd[]" value="{{ $sign_line_array[$i]['special_proc_cd'] }}">
                     <input type="text" class="form-control class_field{{ $i }} field_name_{{ $sign_line_array[$i]['special_proc_cd'] }}"   name="sign_line[]" <?php if($sign_line_array[$i]['element_type_cd'] == "S"){ echo 'style="width: 98%;text-transform: uppercase"'; } ?>      style="width: 98%"  onblur="myFunction(this.id,this.value);" id="{{ $sign_line_array[$i]['required_sw'] }}^{{ $sign_line_array[$i]['element_type_cd'] }}^{{ $sign_line_array[$i]['special_proc_cd'] }}^{{ $sign_line_array[$i]['max_length'] }}^{{ $i }}" value="<?php if(!empty($print_data)){ echo $print_data[$i]; }  ?>">
                     <div class="error_msg_empty error_msg<?php echo $i; ?>"></div>
                  </div>
               </div>
               @endfor
               @endif
               <div class="form-group" style="padding-top: 20px;">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                     <input type="button" name="reset-submit" id="submit" tabindex="<?php   ?>" value="Signs Queued(Ctrl + c)" class="btn btn-info" onClick="document.location.href='{{URL::to('/sign1/queued_signs')}}'"> 

                        <input type="submit" name="login-submit" id="print-submit" tabindex="<?php   ?>" value="Print" class="btn">
                        <input type="button" name="reset-submit" id="submit" tabindex="<?php   ?>" value="Reset" class="btn reset">
                        {{-- <input type="button" onclick="history.go(-1);" name="cancel-submit" id="submit" tabindex="" value="Cancel" class="btn"> --}}
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>  
<script src="{{ asset("assets/jquery/bootstrap/3.3.7/bootstrap.min.js") }}"></script>
 
<script>
   $(document).ready(function() {
    //   $("body").on("contextmenu",function(e){
    //     return false;
    // });
     jQuery(document).bind('keydown', 'Ctrl+c', function(e) {  
  // right arrow key pressed
    window.location="{{ URL::route('queued_signs') }}";return false;
});
     $("input:text:visible:first").focus();
     $('.class_field' + 0).select();
     //if (special_proc_cd == "LB") {
      //alert(special_proc_cd);
      $('.field_name_LB').val('N');

    //}
     // var class_field0 = $('.class_field' + 0).val();
     // alert(class_field0);
     // if(empty(class_field0))
     // {
     //    $('.error_msg' + 0).text("");
     // }
     $(".reset").on('click',function(){
         //alert('hiiii');
        $(this).closest('form').find("input[type=text], textarea").val("");
         //$('.focus_out').focusout()
        $('.error_msg_empty').hide();
        $('.successmsg').hide();
         //location.reload();
     });
     //$('.error_msg_empty').show();
   });
   function myFunction(id, val) {

    $('.error_msg_empty').show();
    // console.log($('.error_msg_empty').text().trim().length);
    if($('.error_msg_empty').text().trim().length > 0){
      $("#print-submit").prop("disabled", true);
      //$("#print-submit").removeAttr('disabled'); 
    }
    else{
        $("#print-submit").removeAttr('disabled'); 
        //$("#print-submit").prop("disabled", true);
    }
    
    // console.log("val is"+id);
    var myVal = id;
    var myVal = myVal.split('^');
    //alert(myVal);
    var required_sw = myVal[0];
    var element_type_cd = myVal[1];

    var special_proc_cd = myVal[2];
    //alert(special_proc_cd);
    var max_length = myVal[3];
    var sign_data = val;

    var data_len = sign_data.length;

    var i = myVal[4];
    //$( "#foodstamps" ).select();
    //$("#print-submit").prop("disabled", true);
    // console.log($('.error_msg' + i).text().trim().length);
    // if($('.error_msg' + i).text().trim().length > 0){
    //   $("#print-submit").prop("disabled", true);
    //   //$("#print-submit").removeAttr('disabled'); 
    // }
    // else{
    //     $("#print-submit").removeAttr('disabled'); 
    //     //$("#print-submit").prop("disabled", true);
    // }
    //var save_sale_price=0;
    $('.error_msg' + i).text("");
    if (element_type_cd == 'G') {

      } else {
        if (data_len == 0) {
            if (required_sw == "Y") {
                //console.log(required_sw);return false;
                //console.log(required_sw);
                //console.log("inside required field");
                //$("#print-submit").prop("disabled", true);
                $('.error_msg' + i).text("This information is required...");
                $('.class_field' + i).val('');
                $('.class_field' + i).focus();

                return false;
            } 
            // else {
            //   console.log("inside required field");
            //   $("#print-submit").removeAttr('disabled'); 
            // } 
        } else {
            var max_len = max_length;
            if (data_len > max_len) {
                $('.error_msg' + i).text("Maximum Length for this is " + max_len);
                $('.class_field' + i).val('');
                $('.class_field' + i).focus();
                return false;
                //$(this).find("span").text();
            }

        }
    }


    if (special_proc_cd == "PQ") {

    }
    if (special_proc_cd == "QF") {

    }
    if (special_proc_cd == "BQ") {
      if(sign_data.length>0){
        var save_buy_qty = sign_data;
      }else{
        var save_buy_qty = 0;
      }
      if(save_buy_qty < 2) {
        $('.error_msg' + i).text("Buy More Save Qyt must be greater than 1");
        $('.class_field' + i).val('');
        $('.class_field' + i).focus();
        return false;
      }
    }
    if (special_proc_cd == "BP") {

    }
    if (special_proc_cd == "FQ") {
      if(sign_data.length>0){
        var save_for_qty = sign_data;
      }else{
        var save_for_qty = 0;
      }
      if(save_for_qty < 2){
        $('.class_field' + i).val('');
      }
    }
    if (special_proc_cd == "SP") {
        
    }
    if (special_proc_cd == "EP") {
      
    }
    if (special_proc_cd == "RP") {
        var save_reg_price = 0;
        var save_for_qty = $(".field_name_FQ").val();
        var save_reg_price = $(".field_name_RP").val();
        
        
        if(isNaN(sign_data)){
          $('.class_field' + i).val('0.00');
        }
        else
        {
          $('.class_field' + i).val(parseFloat(sign_data).toFixed(2));
        }
        if (sign_data > 0) {
          save_reg_price = sign_data;

          if ($(".field_name_SP")[0]){
          var save_sale_price = $(".field_name_SP").val();
          console.log("Element exists field_name_SP");
           if (save_sale_price > 0) {
            } else {

                if (save_reg_price > 0) {
                    $('.error_msg' + i).text("Sale Price required prior to Regular Price's entry");
                    $('.class_field' + i).val('');
                    $('.field_name_SP').focus();
                    return false;
                }
            }
          } else if($(".field_name_S1")[0]){
            var save_sale_price = $(".field_name_S1").val();
          console.log("Element does not exist field_name_S1");
          if (save_sale_price > 0) {
            } else {

                if (save_reg_price > 0) {
                    $('.error_msg' + i).text("Sale Price required prior to Regular Price's entry");
                    $('.class_field' + i).val('');
                    $('.field_name_S1').focus();
                    return false;
                }
            }
          }else{

          }
            
           
        }
    }
    if (special_proc_cd == "S1") {
     
     //save_sale_price = $(".field_name_S1").val();
     //console.log("inside s1"+save_sale_price);
      
    }
    if (special_proc_cd == "GP") {
      
    }
    if (special_proc_cd == "GS") {
      
    }
     if (special_proc_cd == "WP") {
        var save_was_price = 0;
            //console.log("inside wp");
            var save_for_qty = $(".field_name_FQ").val();
            var save_sale_price = $(".field_name_SP").val();
            save_was_price = $(".field_name_WP").val();
            //console.log("save_was_price"+save_was_price);
            if (save_sale_price > 0) {
                if (save_for_qty > 1) {

                } else {
                    save_for_qty = 1;
                }
                var ws_unit_price = save_sale_price / save_for_qty;
                //console.log(ws_unit_price);
                if (save_was_price > ws_unit_price) {

                } else {
                    $('.error_msg'+i).text("WAS Price must be larger than Sale Price");
                    $('.class_field'+i).val('');
                    $('.class_field'+i).focus();return false;
                }
            } else {
                if (save_was_price > 0) {
                    $('.error_msg' + i).text("Sale Price required prior to WAS Price's entry");
                    $('.class_field' + i).val('');
                    $('.class_field' + i).focus();
                    return false;
                }
            }
      }


    if (special_proc_cd == "SA") {
        // Special processing for Save Amount
        var save_reg_price = $(".field_name_RP").val();
        //alert("outside"+save_reg_price);
        if (save_reg_price === undefined) {
            var save_was_price = 0;
            console.log("inside wp");
            var save_for_qty = $(".field_name_FQ").val();
            var save_sale_price = $(".field_name_SP").val();
            save_was_price = $(".field_name_WP").val();
            if (save_for_qty > 1) {

            } else {
                save_for_qty = 1;
            }


            var save_amount = (save_was_price * save_for_qty) - save_sale_price;
            //alert("save amount"+save_amount);
            $('.field_name_SA').val(save_amount.toFixed(2));



        } else {
            var save_for_qty = $(".field_name_FQ").val();
            var save_sale_price = $(".field_name_SP").val();
            var save_reg_price = $(".field_name_RP").val();
            //alert("inside regular price");
            if (save_for_qty > 0) {

            } else {
                save_for_qty = 1;
            }
            var ws_save_amt = (save_for_qty * save_reg_price) - save_sale_price;
            console.log(ws_save_amt);
            if (ws_save_amt < 0) {
                var error_value = "Error! Invalid Everyday Price save_reg_price and Sale Price save_sale_price - creates a negative Save Amount";
                $('.error_msg' + i).text("Invalid Everyday Price,and Sale Price- creates a negative Save Amount");
                $('.class_field' + i).val('');
                $('.class_field' + i).focus();
                return false;
            } else {

                $('.field_name_SA').val(ws_save_amt.toFixed(2));
            }
        }
    }

    if (special_proc_cd == "RP") {
        //alert("1 st one");
        var save_reg_price = 0;
        var save_for_qty = $(".field_name_S1").val();

        if ($('.field_name_RP')[0].val()) {
            save_reg_price = $(".field_name_RP").val();

            if (save_for_qty > 0) {

            } else {
                if (save_reg_price > 0) {
                    //alert("inside rp err msg");
                    $('.error_msg' + i).text("Sale Price required prior to Regular Price's entry");
                    return false;

                }
            }

        }
    }

    if (special_proc_cd == "30") {
        var one_qty_price = $(".field_name_S1").val();
        var save_reg_price = $(".field_name_RP").val();
        var six_qty_price = (one_qty_price * 70) / 100;
        var save_amt = ((one_qty_price - six_qty_price) * 6) + ((save_reg_price - one_qty_price) * 6);
        if (save_amt > 0) {
            $('.field_name_30').val(six_qty_price.toFixed(2));
            $('.field_name_S3').val(save_amt.toFixed(2));
        } else {
            $('.error_msg' + i).text("Invalid Everyday Price(" + save_reg_price + ") and Sale Price(" + one_qty_price + ") - creates a negative Save Amount");
            return false;
        }
    }
    if (special_proc_cd == "10") {
        var one_qty_price = $(".field_name_S1").val();
        var save_reg_price = $(".field_name_RP").val();
        var six_qty_price = (one_qty_price * 90) / 100;
        var save_amt = ((one_qty_price - six_qty_price) * 6) + ((save_reg_price - one_qty_price) * 6);
        $('.field_name_10').val(six_qty_price.toFixed(2));
        $('.field_name_S3').val(save_amt.toFixed(2));
    }
    if (special_proc_cd == "25") {
        var save_reg_price = $(".field_name_SP").val();
        var clearence_price = (save_reg_price * 75) / 100;
        var save_amt = save_reg_price - clearence_price;
        $('.field_name_25').val(clearence_price.toFixed(2));
        $('.field_name_S3').val(save_amt.toFixed(2));
    }
    if (special_proc_cd == "50") {
        var save_reg_price = $(".field_name_SP").val();
        var clearence_price = (save_reg_price * 50) / 100;
        var save_amt = save_reg_price - clearence_price;
        $('.field_name_50').val(clearence_price.toFixed(2));
        $('.field_name_S3').val(save_amt.toFixed(2));
    }
    if (special_proc_cd == "75") {
        var save_reg_price = $(".field_name_SP").val();
        var clearence_price = (save_reg_price * 25) / 100;
        var save_amt = save_reg_price - clearence_price;
        $('.field_name_75').val(clearence_price.toFixed(2));
        $('.field_name_S3').val(save_amt.toFixed(2));


        
    }
       switch (element_type_cd) {
        case "I":
            if (sign_data == '') {
                return "";
            }
            if (isNaN(sign_data) || sign_data < 1) {
                $('.error_msg' + i).text("Must be numeric and greater than zero");
                $('.class_field' + i).val('');
                $('.class_field' + i).focus();
                return false;
            }
            break;
        case "M":
            if (sign_data == '') {
                return "";
            }
            if(isNaN(sign_data)){
              $('.class_field' + i).val('0'); return false;
            }
            if (isNaN(sign_data) || sign_data < 0) {
                $('.error_msg' + i).text("Must be numeric, greater than zero, and decimal is required");
                $('.class_field' + i).val('');
                $('.class_field' + i).focus();
                return false;
            } else {
                if (sign_data > 999.99) {
                    $('.error_msg' + i).text("Value must be less than $1000.00");
                    $('.class_field' + i).val('');
                    $('.class_field' + i).focus();
                    return false;
                }
            }
            break;
        case "S":
            var res = sign_data.toUpperCase();
            if (res == '') {
                return "";
            }
            if (!(res == "Y" || res == "N")) {
                $('.error_msg' + i).text("Must be  Y  or  N");
                $('.class_field' + i).val('');
                $('.class_field' + i).focus();
                return false;
            } else {
                return "";
            }
            break;
    }


}
     
</script>
@stop