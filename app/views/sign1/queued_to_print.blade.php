<?php 
   //echo '<pre>';print_r($stock_array);exit;
 ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <!-- <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>Print Queued Tags & Signs</h3>
         </b>
      </div>
   </div> -->
</header><br>
<div class="container">
   <div class="row">
   <div class="col-md-6 text-center" style="margin-left:290px;">
         <b><h4>Date Requested : 01/09/2017<br><br>
         Process Signs Requested by...</h4></b>
      </div><br>
      <div class="flash-message col-md-6 col-md-offset-3 text-center"">
      @if(Session::has('not_print'))
      <p class="alert alert-warning">{{ Session::get('not_print') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
   </div>  
      
      <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 
      <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                       <div class="panel-title" >
         Print Queued Tags & Signs</div>
                       </div> 
                       

                 <div style="padding-top:30px" class="panel-body" >
                

                  
                        <form action="{{URL::route('sign1-prnt_tags_signs')}}" class="form-horizontal" method="POST" role="form" style="display: block;">
                        <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-4 col-sm-offset-2">User ID</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" autocomplete="off" isimportant="true" name="user_id"{{ (Input::old('user_id'))?' value="'.Input::old('user_id').'"':''}} placeholder="" keyIndex="1" required>
                 @if($errors->has('user_id'))
            {{ $errors->first('user_id')}}
            @endif
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4 col-sm-offset-2">Password</label>
            <div class="col-sm-6">
                <input type="Password" class="form-control" autocomplete="off" isimportant="true" id="password" placeholder="" name="password" keyIndex="2" required>
                 @if($errors->has('password'))
            {{ $errors->first('password')}}
            @endif
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4 col-sm-offset-2">Printed Yet</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="prnt_yet" autocomplete="off" isimportant="true" value="N" placeholder="" name="prnt_yet" keyIndex="3" style="text-transform:uppercase;" required>
                 @if($errors->has('prnt_yet'))
            {{ $errors->first('prnt_yet')}}
            @endif
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4 col-sm-offset-2">Yesterday/Today/Both</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="dayinfo" autocomplete="off" isimportant="true" value="T" placeholder="" name="dayinfo" keyIndex="4" style="text-transform:uppercase;"  required>
                 @if($errors->has('dayinfo'))
            {{ $errors->first('dayinfo')}}
            @endif
            </div>
        </div>
                           
                           <div class="form-group">
                              <div class="row">
                                  <div class="col-sm-12" align="center">
            <input type="submit" name="login-submit" id="submit" onClick="check()"; tabindex="4" value="Sign In" class="btn">
                <input type="button" name="login-submit" id="submit" tabindex="4" value="Cancel" onClick="document.location.href='{{URL::to('/home')}}'" class="btn">
                                                {{ Form::token()}}
                                 </div>
                              </div>
                           </div>
                           
                        </form>
                        
                     
               </div>
            </div>
            </div>
   </div>
 

</div>
@section('jssection')


<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>
<script>
// function check(){
//    //alert("hai");
//    var print_yet_value=document.getElementById('prnt_yet').value;
//    var dayinfo_value=document.getElementById('dayinfo').value;
//      // alert("value"+value);
//       if (print_yet_value == "N" || print_yet_value == "n" || print_yet_value == "Y" || print_yet_value == "y") {
//     }  else {
//         alert("Please Enter Either N/Y");
//         return false;
//     }
     
//      if (dayinfo_value == "T" || dayinfo_value == "t" || dayinfo_value == "Y" || dayinfo_value == "y" || dayinfo_value == "B" || dayinfo_value == "b") {
//     } else {
//         alert("Please Enter Either T/Y/B");
//         return false;
//     }
// }
 jQuery(document).ready(function($) {
 $('#prnt_yet').keyup(function() {
      var value1 = this.value.toUpperCase();

      if(value1 != "Y" && value1 != "N") {
      this.value = '';
      alert( "'Y' or 'N' is required. \n Please try again.");
      }
});
 $('#dayinfo').keyup(function() {
      var value1 = this.value.toUpperCase();

      if(value1 != "T" && value1 != "Y" && value1 != "B") {
      this.value = '';
      alert( "'T' or 'Y' or 'B' is required. \n Please try again.");
      }
});
 });

</script>
@endsection
@stop