@extends('layout.dashboard')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('sign1.sign1reportbatchmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Batch Maintenance<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('sign1-report-batch-browse')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                
                <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">Store No</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="store_no" placeholder="Store No" name="store_no">
                     @if($errors->has('store_no'))
                     {{ $errors->first('store_no')}}
                     @endif
                  </div>
               </div>
                
            <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">User ID</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="userid" placeholder="User ID" name="userid">
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                  </div>
               </div>
                
                
                <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Batch ID</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="batch_id" placeholder="Batch ID" name="batch_id">
                     @if($errors->has('batch_id'))
                     {{ $errors->first('batch_id')}}
                     @endif
                  </div>
               </div>

<div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Status</label>
                  <div class="col-sm-6">
                    <select class="form-control" id="status_cd" name="status_cd">
                        <option value="">Select Status</option>
                        <option value="P">PENDING</option>
                        <option value="A">APPLY</option>
                        <option value="D">DELETE</option>
                        <option value="H">HOLD</option>
                        <option value="R">REVERSED</option>
                        <option value="B">BACKOUT</option>
                        <option value="C">COMPLETE</option>
                        <option value="E">ERROR</option>
                    </select>
                     @if($errors->has('status_cd'))
                     {{ $errors->first('status_cd')}}
                     @endif
                  </div>
               </div>
                
                 <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Batch Type</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="batch_type" placeholder="Batch Type" name="batch_type">
                     @if($errors->has('batch_type'))
                     {{ $errors->first('batch_type')}}
                     @endif
                  </div>
               </div>
   
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Create Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control lastdate" id="created_date" placeholder="Date" name="created_date">
                     @if($errors->has('created_date'))
                     {{ $errors->first('created_date')}}
                     @endif
                  </div>
               </div>
                
                
                  <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="last_update_by" placeholder="Last Updated By" name="last_update_by" value="">
                     @if($errors->has('last_update_by'))
                     {{ $errors->first('last_update_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update">
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>
                
             
               
              
              
               
              
               <div class="form-group" style="padding-top: 20px;">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Search" class="btn">
                       
                          <input type="button" id="" tabindex="4" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::route('sign_option_for_stock_code',array('stock_code'=>'Z'))}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>

 
@stop