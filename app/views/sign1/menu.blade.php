<div class="container">
  <div class="menu_search_line">
    <ul class="navbar dib_float_left">
    <!-- <li id="main-home-link" class="menu_choice home_menu_choice active">
        Bookkeeper
    </li> -->
    <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">8.5" X 11"<span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             
        <li><a href="{{ URL::route('sign1-everyday-value')}}">EVERYDAY VALUE</a></li>
        <li><a href="{{ URL::route('sign1-daily-department-hour')}}">DAILY DEPARTMENT HOURS</a></li>
        <li><a href="{{URl::route('sign1-weekday')}}">WEEKDAY, SAT & SUN HOURS</a></li>
        <li><a href="{{URl::route('sign1-weekday-hour')}}">WEEKDAY, SAT, SUN HOURS</a></li>
        <li><a href="{{URl::route('sign1-weekday-sat-hour')}}">WEEKDAY, SATURDAY HOURS</a></li>
          
            </ul>
        </div>
        
        <div class="dropdown sub">
            <a id="dLabel" href="#" role="button" 
               class="menu_choice" data-target="#">
                11" X 8.5"
            </a>
        </div>
         
        
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                FLORAL <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             <li><a href="{{URl::route('floral-sale')}}">SALE (AD)</a></li>
            <li><a href="{{URl::route('floral-price-drop')}}">PRICE DROP</a></li>
            <li><a href="{{URl::route('floral-blank')}}">BLANK (NO SAVINGS)</a></li>
            <li><a href="{{URl::route('floral-blank-brand')}}">BLANK (OUR BRANDS)</a></li>
            <li><a href="{{URl::route('floral-bogo')}}">BOGO</a></li>
        </ul>
        </div>
     <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                3.5" X 11" <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
          <li><a href="{{URl::route('floral-sale(AD)')}}">SALE (AD) </a></li>
          <li><a href="{{URl::route('floral-D-price-drop')}}">PRICE DROP </a></li>
          <li><a href="{{URl::route('floral-D-blank')}}">BLANK (NO SAVINGS)</a></li>
          <li><a href="{{URl::route('floral-seasonal')}}">SEASONAL PRICE DROP </a></li>
          <li><a href="{{URl::route('floral-raleys-dailies')}}">RALEYS DAILIES </a></li>
          <li><a href="{{URl::route('floral-pct-off')}}">30 PCT OFF WINE/SPIRITS </a></li>
          <li><a href="{{URl::route('floral-local')}}">LOCAL </a></li>
          <li><a href="{{URl::route('floral-buy/save')}}">BUY/SAVE MORE </a></li>
          <li><a href="{{URl::route('floral-bogo2')}}">BOGO </a></li>
          <li><a href="{{URl::route('floral-b2go')}}">B2GO </a></li>
          <li><a href="{{URl::route('floral-buy-get')}}">BUY - GET </a></li>
          <li><a href="{{URl::route('floral-buy-get-free')}}">BUY - GET FREE </a></li>
          <li><a href="{{URl::route('floral-special-promo')}}">SPECIAL PROMO </a></li>
          <li><a href="{{URl::route('floral-percent-off')}}">PERCENT OFF </a></li>
          <li><a href="{{URl::route('floral-special-%-off')}}">SPECIAL % OFF </a></li> 
          <li><a href="{{URl::route('floral-text-only')}}">TEXT ONLY </a></li>    
        </ul>
        </div>
    

    
       <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                3.5" X 5.5" <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             <li><a href="{{URl::route('fifth-sale')}}">SALE (AD)</a></li>
            <li><a href="{{URl::route('fifth-price-drop')}}">PRICE DROP</a></li>
            <li><a href="{{URl::route('fifth-blank')}}">BLANK (NO SAVINGS)</a></li>
            <li><a href="{{URl::route('fifth-seasonal-price-drop')}}">SEASONAL PRICE DROP</a></li>
            <li><a href="{{URl::route('fifth-raleys-dailies')}}">RALEYS DAILIES</a></li>
            <li><a href="{{URl::route('fifth-pct-off')}}">30 PCT OFF WINE/SPIRITS</a></li>
            <li><a href="{{URl::route('fifth-buy-more-save')}}">BUY MORE SAVE MORE</a></li>
            <li><a href="{{URl::route('fifth-percent-off')}}">PERCENT OFF</a></li>
            <li><a href="{{URl::route('fifth-text-only')}}">TEXT ONLY</a></li>
            <li><a href="{{URl::route('fifth-bogo')}}">BOGO</a></li>
            <li><a href="{{URl::route('fifth-buy-get')}}">BUY - GET</a></li>
            <li><a href="{{URl::route('fifth-buy-get-free')}}">BUY - GET FREE</a></li>
        </ul>
        </div>
        
     <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                7"   X 11" <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
            <li><a href="{{URl::route('six-sale')}}">SALE (AD) </a></li>
            <li><a href="{{URl::route('six-price-drop')}}">PRICE DROP </a></li>
            <li><a href="{{URl::route('six-blank')}}">BLANK (NO SAVINGS) </a></li>
            <li><a href="{{URl::route('six-local')}}">LOCAL </a></li>
            <li><a href="{{URl::route('six-raleys-dailies')}}">RALEYS DAILIES </a></li>
            <li><a href="{{URl::route('six-pct-off')}}">30 PCT OFF WINE/SPIRITS </a></li>
            <li><a href="{{URl::route('six-seasonal-price-drop')}}">SEASONAL PRICE DROP </a></li>
            <li><a href="{{URl::route('six-buy-save-more')}}">BUY/SAVE MORE </a></li>
            <li><a href="{{URl::route('six-bogo')}}">BOGO </a></li>
            <li><a href="{{URl::route('six-b2go')}}">B2GO </a></li>
            <li><a href="{{URl::route('six-buy-get')}}">BUY - GET </a></li>
            <li><a href="{{URl::route('six-buy-get-free')}}">BUY - GET FREE </a></li>
            <li><a href="{{URl::route('six-special-promo')}}">SPECIAL PROMO </a></li>
            <li><a href="{{URl::route('six-percent-off')}}">PERCENT OFF </a></li>
            <li><a href="{{URl::route('six-text-only')}}">TEXT ONLY</a></li>
        </ul>
        </div> 
    
    <div class="dropdown sub">
   <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
   2.5" X 4" <span class="caret"></span>
   </a>
   <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
      <li><a href="{{URl::route('seven-sale')}}">SALE (AD)</a></li>
      <li><a href="{{URl::route('seven-price-drop')}}">PRICE DROP</a></li>
      <li><a href="{{URl::route('seven-blank')}}">BLANK (NO SAVINGS)</a></li>
      <li><a href="{{URl::route('seven-raleys-dailies')}}">RALEYS DAILIES</a></li>
      <li><a href="{{URl::route('seven-raleys')}}">RALEY D'S NO SAVE</a></li>
      <li><a href="{{URl::route('seven-pct-off')}}">30 PCT OFF WINE/SPIRITS</a></li>
      <li><a href="{{URl::route('seven-clearance')}}">CLEARANCE</a></li>
      <li><a href="{{URl::route('seven-pct-off-beer')}}">10 PCT OFF BEER</a></li>
      <li><a href="{{URl::route('seven-bogo')}}">BOGO</a></li>
      <li><a href="{{URl::route('seven-b2go')}}">B2GO</a></li>
      <li><a href="{{URl::route('seven-buy-save')}}">BUY/SAVE MORE</a></li>
      <li><a href="{{URl::route('seven-25-pct-off')}}">25 PCT OFF CLEARANCE</a></li>
      <li><a href="{{URl::route('seven-50-pct-off')}}">50 PCT OFF CLEARANCE</a></li>
      <li><a href="{{URl::route('seven-75-pct-off')}}">75 PCT OFF CLEARANCE</a></li>
      <li><a href="{{URl::route('seven-blank-brands')}}">BLANK (OUR BRANDS)</a></li>
      <li><a href="{{URl::route('seven-local')}}">LOCAL</a></li>
      <li><a href="{{URl::route('seven-seasonal-price-drop')}}">SEASONAL PRICE DROP</a></li>
   </ul>
</div>

<div class="dropdown sub">
  <a id="dLabel" href="{{ URL::route('sign1-new-signs')}}" role="button" 
               class="menu_choice" data-target="#">
   NEW AD SIGNS
  </a>
</div>



        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                FRESH SERVICE CASE SIGNS <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{URl::route('meat')}}">MEAT</a></li>
              <li><a href="{{URl::route('seafood')}}">SEAFOOD</a></li>
        </ul>
        </div>
        
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                2" X 2" (20 UP) <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{URl::route('Y-sale')}}">SALE (AD)</a></li>
              <li><a href="{{URl::route('Y-price-drop')}}">PRICE DROP</a></li>
              <li><a href="{{URl::route('Y-blank-brands')}}">BLANK (OUR BRANDS)</a></li>
              <li><a href="{{URl::route('Y-raleys-dailys')}}">RALEYS DAILYS</a></li>
              <li><a href="{{URl::route('Y-raley')}}">RALEY D'S NO SAVE</a></li>
              <li><a href="{{URl::route('Y-clearance')}}">CLEARANCE</a></li>
              <li><a href="{{URl::route('Y-new-item')}}">NEW ITEM</a></li>
              <li><a href="{{URl::route('Y-blank')}}">BLANK (NO SAVINGS)</a></li>
              <li><a href="{{URl::route('Y-bogo')}}">BOGO</a></li>
              <li><a href="{{URl::route('Y-was-now')}}">WAS/NOW</a></li>
            </ul>
        </div>

<div class="dropdown sub">
            <a id="dLabel" href="{{URl::route('Remotely-signs')}}" role="button" 
               class="menu_choice" data-target="#">
               Remotely Requested Signs
            </a>
</div>

<div class="dropdown sub">
            <a id="dLabel" href="{{URl::route('print-tags')}}" role="button" 
               class="menu_choice" data-target="#">
              Print Queued Tags & Signs
            </a>
</div>
    

 <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Audit/FM Notes <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="#">Modification Audit Report</a></li> 
               <li><a href="#">FM Notes Report</a></li>
               <li><a href="#">Batch Admin Menu</a></li>
            </ul>
        </div>
 </ul>
</div>
</div>  
