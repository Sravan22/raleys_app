@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('sign1.menu')
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-7 col-md-offset-2 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Sign Stock :D -- PRICE DROP</div>
         </div>
         <div class="panel-body" >
            <form action="" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label col-sm-5">Manufacturer</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="Manufacturer" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Product Type / Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="Product Type / Description" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
              
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Additional Product Detail</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Additional Product Detail" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Additional Product Detail</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Additional Product Detail" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Size</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Size" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">For Qty</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="For Qty" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               
               
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Sale Price</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Sale Price" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Price Per</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Price Per" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Regular Price </label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Regular Price " name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
                 <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Save Amount</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Save Amount " name="">
                     @if($errors->has(''))
                        {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               
              
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">CRV</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="CRV" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Print "lbs" next to For Qty</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Print "lbs" next to For Qty" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Department/Date</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder=" Department/Date" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               
               
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop