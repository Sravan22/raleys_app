@extends('layout.dashboard')
@section('page_heading','Shrink Capture Review')
@section('content')
@section('section')
<div class="container">
<form action="{{URL::route('account-shrink-capture-review-date')}}" method="post" class="form-horizontal" role="form"> 
<!--  <div class="col-sm-6"> -->
<div class="row">
    <div class="col-lg-4">
       
<div class="form-group">
  <label for="inputEmail3" class="col-sm-4 control-label"> Selected Shrink Date</label>
  <div class="col-sm-6">
    <input type="date" min="2016-01-01" max="2016-12-01" class="form-control" id="shrinkdate" placeholder="Shrink Date" />
  </div>
</div>

</div>
</div>
<div class="row">
    <div class="col-md-4 text-center">
 <div class="form-group">
     <input type="submit" value="Submit"  class="btn btn-default">
        {{ Form::token()}}
</div> 
</div>
</div>

</form>
 
</div>
@stop
