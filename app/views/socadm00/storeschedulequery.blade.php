@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
<form action="{{URL::route('socadm00-post-schedule-query')}}" class="form-horizontal" role="form" method="post" action="">
   <div class="container" style="">
      <div class="flash-message">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Store Order Schedule Configuration Manager</div>
            </div>
            <div style="padding-top:35px" class="panel-body" >
               <div class="row vertical-divider">
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">GL Dept</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="gl_dept_number" maxlength="2" name="gl_dept_number" placeholder="GL Dept" {{ (Input::old('gl_dept_number'))?' 
                              value="'.Input::old('gl_dept_number').'"':''}}  value=""  onKeyPress="return StopNonNumeric(this,event)"/>
                              @if($errors->has('gl_dept_number'))
                              {{ $errors->first('gl_dept_number')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Type Code
                           </label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" maxlength="2"  id="type_code" style="text-transform: uppercase" placeholder="Type Code" name="type_code" {{ (Input::old('type_code'))?' 
                              value="'.Input::old('type_code').'"':''}}>
                              @if($errors->has('type_code'))
                              {{ $errors->first('type_code')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Sub-Type Code</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" maxlength="2" id="subtype_code" style="text-transform: uppercase" placeholder="Sub-Type Code" name="subtype_code" {{ (Input::old('subtype_code'))?' 
                              value="'.Input::old('subtype_code').'"':''}}>
                              @if($errors->has('subtype_code'))
                              {{ $errors->first('subtype_code')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Vendor Number</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="vendor_number" maxlength="8" placeholder="Vendor Number" name="vendor_number" {{ (Input::old('vendor_number'))?' 
                              value="'.Input::old('vendor_number').'"':''}}>
                              @if($errors->has('vendor_number'))
                              {{ $errors->first('vendor_number')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Partition Code</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="partition_code" maxlength="2" style="text-transform: uppercase" placeholder="Partition Code" name="partition_code" {{ (Input::old('partition_code'))?' 
                              value="'.Input::old('partition_code').'"':''}}>
                              @if($errors->has('partition_code'))
                              {{ $errors->first('partition_code')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Order Deadline</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" maxlength="4" id="deadline" placeholder="Order Deadline" name="deadline" onKeyPress="return StopNonNumeric(this,event)" {{ (Input::old('deadline'))?' 
                              value="'.Input::old('deadline').'"':''}}>
                              @if($errors->has('deadline'))
                              {{ $errors->first('deadline')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Order Resume Time</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" maxlength="4"  id="resume_time" placeholder="Order Resume Time" name="resume_time" onKeyPress="return StopNonNumeric(this,event)" {{ (Input::old('resume_time'))?' 
                              value="'.Input::old('resume_time').'"':''}}>
                              @if($errors->has('resume_time'))
                              {{ $errors->first('resume_time')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Vendor Description</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="description" placeholder="Vendor Description" name="description" {{ (Input::old('description'))?' 
                              value="'.Input::old('description').'"':''}}>
                              @if($errors->has('description'))
                              {{ $errors->first('description')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="#" class="col-sm-4 control-label"></label>
                           <label for="#" class="col-sm-6 control-label" style="text-align: center;">Order Days</label>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Sun</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" maxlength="1" id="sun_sw" style="text-transform: uppercase" placeholder="Sun" name="sun_sw">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Mon</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="mon_sw" style="text-transform: uppercase" placeholder="Mon" name="mon_sw">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Tue </label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="tue_sw" style="text-transform: uppercase" placeholder="Tue" name="tue_sw">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Wed </label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="wed_sw" style="text-transform: uppercase" placeholder="Wed" name="wed_sw">
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Thu</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="thr_sw"  placeholder="Thu" name="thr_sw" style="text-transform: uppercase">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Fri</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="fri_sw"  placeholder="Fri" name="fri_sw" style="text-transform: uppercase">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Sat</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="sat_sw"  placeholder="Sat" name="sat_sw" style="text-transform: uppercase">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Hard Deadline Switch</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="hard_deadline_sw" placeholder="Hard Deadline Switch" style="text-transform: uppercase" name="hard_deadline_sw">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Order Reminder Switch</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="reminder_sw" placeholder="Order Reminder Switch" style="text-transform: uppercase" name="reminder_sw">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Days to go back for history</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" maxlength="1" id="days_history" placeholder="Please Enter 0 to 7" name="days_history" {{ (Input::old('days_history'))?' 
                              value="'.Input::old('days_history').'"':''}} onKeyPress="return StopNonNumeric(this,event)">
                              @if($errors->has('days_history'))
                              {{ $errors->first('days_history')}}
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Active Switch</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="active_sw" style="text-transform: uppercase" placeholder="Active Switch" name="active_sw">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Last Updated By</label>
                           <div class="col-sm-8">
                              <input type="date" class="form-control" readonly="" id="" placeholder="Last Updated By" name="">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-12 topspace" align="center">
                           <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                           <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('socadm00/storetransmission')}}'" />
                           {{ Form::token()}}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>
@stop

<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
   ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   text-transform: initial;
   }
</style>
<script type="text/javascript">
   function StopNonNumeric(el, evt)
   {
       //var r=e.which?e.which:event.keyCode;
       //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
       var charCode = (evt.which) ? evt.which : event.keyCode;
       var number = el.value.split('.');
       if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
           return false;
       }
       //just one dot (thanks ddlab)
       if(number.length>1 && charCode == 46){
            return false;
       }
       //get the carat position
       var dotPos = el.value.indexOf(".");
       if( dotPos>-1 && (number[1].length > 3)){
           return false;
       }
       return true;
   }
   function validateForm()
   {     
   var days_history = document.getElementById("days_history").value;
      if(days_history < 0)
      {
         alert('Days to go back for History must be 0 to 7');
         $('#days_history').val('');$('#days_history').focus();return false;
      }
      if(days_history > 7)
      {
         alert('Days to go back for History must be 0 to 7');
         $('#days_history').val('');$('#days_history').focus();return false;
      }
   var str = document.getElementById("active_sw").value;
   var active_sw = str.toUpperCase();
   if(!(active_sw == 'Y' || active_sw == 'N'))
   {
      alert('Active Switch must be  Y  or  N');
      $('#active_sw').val('');$('#active_sw').focus();return false;
   }
   }
   
</script>