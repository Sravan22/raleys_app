<?php 
   // echo '<pre>';print_r($data);
   // echo '<br>';exit;
   //echo '<pre>';print_r($pagination);exit;
   ?>
@extends('layout.dashboard')
@section('page_heading',"Store Transmission Menu Manager")
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
 <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">


<div class="container" style="padding-top:10px;">
   @if ($data)
   <table class="table table-striped">
      <thead>
         <tr>
            <th>GL Dept</th>
            <th>Menu Level</th>
            <th>Function Code</th>
            <th>Type Code</th>
            <th>SubType Code</th>
            <th>Description</th>
            <th>Menu Pos</th>
            <th>Goto Pgm ID</th>
            <th>Active Switch</th>
         </tr>
      </thead>
      <tbody>
         @foreach($data as $row)
         <tr>
          <?php 
              $params = array(
                        'entry_number' =>  $row->entry_number,
                        'function_code' =>  $row->function_code
                        ); 
              $queryString = http_build_query($params);
            ?>
             <td><u>{{ HTML::link(URL::route('socadm00-menuquery-update',$queryString), $row->gl_dept_number) }}</u></td>
            {{-- <td>{{ $row->gl_dept_number }}</u></td> --}}
            <td>{{ $row->menu_level }}</td>
            <td>{{ $row->function_code }}</td>
            <td>{{ $row->type_code }}</td>
            <td>{{ $row->subtype_code }}</td>
            <td>{{ $row->description }}</td>
            <td>{{ $row->menu_position }}</td>
            <td>{{ $row->goto_pgm_id }}</td>
            <td>{{ $row->active_switch }}</td>
         </tr>
         @endforeach
      </tbody>
   </table>
   {{$pagination->links()}}
   @else
   <div class="alert alert-danger">
      <strong>Alert!</strong> No Store Transfers meet Query criteria.
   </div>
   @endif
   <!--  <div class="form-group">
      <div class="row">
      <div class="col-sm-12" align="center">
          <input type="submit" name="addinfo" id="addinfo" tabindex="4" value="Query" class="btn">
            {{ Form::token()}}
      </div>
      </div>
      </div> -->
</div>
@stop