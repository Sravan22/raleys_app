@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transfer Configuration Manager</div>
         </div>
         <div class="panel-body" >
            <form action="" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Type Code</label>
                  <div class="col-sm-6">
                     <input type="number" class="form-control" id="" placeholder="" name="" min="1" max="999">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Sub-Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Banner Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="" name="regtype">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Partition Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="regtype" placeholder="" name="regtype">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Department Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="regtype" placeholder="" name="regtype">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Maximum Item Length</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="regtype" placeholder="" name="regtype">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Maximum Quantity</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="regtype" placeholder="" name="regtype">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Special Process Flag</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="regtype" placeholder="" name="regtype">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Zero Qty Flag</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="regtype" placeholder="" name="regtype">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Return Flag</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="regtype" placeholder="" name="regtype">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">History FLag</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Vendor Account Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Active Switch</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="Date" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated:</label>
                  <div class="col-sm-6">
                     <input type="Date" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop