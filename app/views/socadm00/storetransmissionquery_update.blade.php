<?php
   //echo '<pre>';print_r($sotrncfg_data_array);exit;
?>
@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transmissions Menu Manager</div>
         </div>
         <br>
         <div class="panel-body" >
            <form action="{{URL::route('socadm00-storetransmission-update')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">GL Dept Number</label>
                  <div class="col-sm-6">
                  <input type="hidden" name="entry_number" value="{{ $somencfg_data_array[0]['entry_number'] }}">
                     <input type="number" class="form-control" id="gl_dept_number" placeholder="GL Dept Number" name="gl_dept_number" value="{{ $somencfg_data_array[0]['gl_dept_number'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Menu Level</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="menu_level" placeholder="Menu Level" name="menu_level" value="{{ $somencfg_data_array[0]['menu_level'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Function Code</label>
                  <div class="col-sm-6">
                     <select id="function_code" name="function_code" class="form-control" value="">
                        <option value="">Select Function Code</option>
                        @for ($i = 0; $i < count($function_code); $i++) 
                        <option <?php if($function_code[$i]->function_code == $somencfg_data_array[0]['function_code'] ){ echo 'selected'; } ?> value="{{ $function_code[$i]->function_code }}">{{ $function_code[$i]->function_code }}</option>
                        @endfor 
                     </select>
                     @if($errors->has('function_code'))
                     {{ $errors->first('function_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Function Code Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" readonly="" id="ws_func_desc" placeholder="Function Code Description" name="ws_func_desc" value="{{ $function_code_description[0]->description }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="type_code" style="text-transform: uppercase" placeholder="Type Code" name="type_code" value="{{ $somencfg_data_array[0]['type_code'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Sub-Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="subtype_code" style="text-transform: uppercase" placeholder="Sub-Type Code" name="subtype_code" value="{{ $somencfg_data_array[0]['subtype_code'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Menu Item Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="description" placeholder="Menu Item Description" name="description" value="{{ $somencfg_data_array[0]['description'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Position Number on Menu</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="menu_position" placeholder="Position Number on Menu" name="menu_position" value="{{ $somencfg_data_array[0]['menu_position'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Goes to Program</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="goto_pgm_id" placeholder="Goes to Program" name="goto_pgm_id" value="{{ $somencfg_data_array[0]['goto_pgm_id'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Acitve Switch</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="active_switch" placeholder="Acitve Switch" name="active_switch" value="{{ $somencfg_data_array[0]['active_switch'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="last_updated_by" placeholder="" name="last_updated_by" value="{{ $somencfg_data_array[0]['last_updated_by'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Last Update</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" readonly="" id="last_updated" placeholder="" name="last_updated" value="{{ $somencfg_data_array[0]['last_update'] }}">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Update" class="btn">
                        <input type="reset" name="login-submit" id="submit" tabindex="4" value="Cancel" onclick="history.go(-1);" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop

<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>
<script type="text/javascript">
   $(document).ready(function() {
     $('#function_code').on('change', function(e) {
         e.preventDefault();
         function_code = $("#function_code").val();
         //alert(function_code);
         if (function_code != '') {
             $.post("ws_func_desc", {
                 function_code: function_code
             }, function(result) { //alert(result); return false;
                  $("#ws_func_desc").val(result);
             });
         }
     });
 });
 </script>

