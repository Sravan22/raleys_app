<?php
   //echo '<pre>';print_r($sodsdvnd_array);exit;
?>
@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
<div class="container">
<div class="menu_search_line">    </div>
<div class="container" style="padding-top:10px;">
    @if ($data)
      <table class="table table-striped">
         <thead>
            <tr>
               <th>Vender Number</th>
               <th>Name</th>
               <th>Commid</th>
               <th>Active Switch</th>
            </tr>
         </thead>
         <tbody>
         @foreach ($data as $sodsdvnd_array) 
            <tr>
            <?php 
              $params = array(
                        'vendor_number' =>  $sodsdvnd_array->vendor_number
                        ); 
              $queryString = http_build_query($params);
            ?>
            <td><u>{{ HTML::link(URL::route('socadm00-storedexcommids-update',$queryString), $sodsdvnd_array->vendor_number) }}</u></td>
               {{-- <td>{{ $sodsdvnd_array[$i]['vendor_number'] }}</td>--}}
               <td>{{ $sodsdvnd_array->name }}</td>
               <td>{{ $sodsdvnd_array->commid }}</td>
               <td>{{ $sodsdvnd_array->active_switch }}</td>
            </tr>
            @endforeach
         </tbody>
      </table>
      {{$pagination->links()}}
      @else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Store Transfers meet Query criteria.
            </div>
        @endif
   </form>
</div>
@stop