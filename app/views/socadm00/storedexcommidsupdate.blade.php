<?php
   //echo '<pre>';print_r($sodsdvnd_query_array);exit;
?>
@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
<form action="{{URL::route('socadm00-post-storedexcommidsupdate')}}" class="form-horizontal" role="form" method="post" onsubmit="return validateForm()">
   <div class="container" style="">
      <div class="flash-message">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >DEX Commid and Dun Assignment</div>
            </div>
            <div style="padding-top:35px" class="panel-body" >
               <div class="row vertical-divider">
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Vender Number</label>
                           <div class="col-sm-8">
                              <input type="text" readonly="" class="form-control" id="vendor_number" name="vendor_number" placeholder="Vender Number" value="{{ $sodsdvnd_query_array[0]['vendor_number'] }}"/>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Name</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Name" name="" value="{{ $sodsdvnd_query_array[0]['name'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Address</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Address" name="" value="{{ $sodsdvnd_query_array[0]['address'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">City</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="City" name="" value="{{ $sodsdvnd_query_array[0]['city'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">State</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="State" name="" value="{{ $sodsdvnd_query_array[0]['state'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Zip Code</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Zip Code" name="" value="{{ $sodsdvnd_query_array[0]['zip_code'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Phone</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Phone" name="" value="{{ $sodsdvnd_query_array[0]['phone'] }}">
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Rep</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="REP" name="" value="{{ $sodsdvnd_query_array[0]['rep'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Delivery Unit</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Delivery Unit" name="" value="{{ $sodsdvnd_query_array[0]['delivery_unit'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Venodor DUN</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="duns_number" placeholder="Venodor DUN" name="duns_number" value="{{ $sodsdvnd_query_array[0]['duns_number'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">UCC Comm ID</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="commid" placeholder="UCC Comm ID" name="commid" value="{{ $sodsdvnd_query_array[0]['commid'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Type Code</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="type_code" placeholder="Type Code" name="type_code" value="{{ $sodsdvnd_query_array[0]['type_code'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Active Switch</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" style="text-transform: uppercase"  id="active_switch" placeholder="Active Switch" name="active_switch" value="{{ $sodsdvnd_query_array[0]['active_switch'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Last Updated By</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Last Updated By" name="" value="{{ $sodsdvnd_query_array[0]['last_updated_by'] }}">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Last Update</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Last Update" name="" value="{{ $sodsdvnd_query_array[0]['last_update'] }}">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-12 topspace" align="center">
                           <input type="submit" name="login-submit" id="submit" tabindex="4" value="Update" class="btn">
                            <input type="reset" name="" onclick="history.go(-1);" id="submit" tabindex="4" value="Cancel" class="btn">
                           {{ Form::token()}}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>
@stop 

<style type="text/css">
    ::-webkit-input-placeholder {
   text-transform: initial;
}

:-moz-placeholder { 
   text-transform: initial;
}

::-moz-placeholder {  
   text-transform: initial;
}

:-ms-input-placeholder { 
   text-transform: initial;
}
</style>



<script type="text/javascript">
   function validateForm()
   {
      var duns_number = document.getElementById("duns_number").value;
      //alert(duns_number.length);return false;
      if(duns_number.length < 9 )
      {
         alert('Duns Number must be 9 characters in length');
         //$('#duns_number').val('');
         $('#duns_number').focus();return false;
      }

      var commid = document.getElementById("commid").value;
      if(commid.length < 10 )
      {
         alert('Comm ID must be 10 characters in length');
         $('#commid').focus();return false;
      }

      var str = document.getElementById("active_switch").value;

      var active_switch = str.toUpperCase();
      if(!(active_switch == 'Y' || active_switch == 'N'))
      {
         alert('Active Switch must be  Y  or  N');
         $('#active_switch').val('');$('#active_switch').focus();return false;
      }

   }
   
</script>