@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transmissions Configuration Manager</div>
         </div>
         <br>
         <div class="panel-body" >
            <form action="{{URL::route('socadm00-post-storetransconf-add')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="type_code" placeholder="Type Code" name="type_code">
                     @if($errors->has('type_code'))
                     {{ $errors->first('type_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Sub-Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="subtype_code" placeholder="Sub-Type Code" name="subtype_code">
                     @if($errors->has('subtype_code'))
                     {{ $errors->first('subtype_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Banner Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="description" placeholder="Banner Description" name="description">
                     @if($errors->has('description'))
                     {{ $errors->first('description')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Partition Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="partition_code" placeholder="Partition Code" name="partition_code">
                     @if($errors->has('partition_code'))
                     {{ $errors->first('partition_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Department Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="dept_number" placeholder="Department Number" name="dept_number">
                     @if($errors->has('dept_number'))
                     {{ $errors->first('dept_number')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Maximum Item Length</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="max_item_length" placeholder="Maximum Item Length" name="max_item_length">
                     @if($errors->has('max_item_length'))
                     {{ $errors->first('max_item_length')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Maximum Quantity</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="max_qty" placeholder="Maximum Quantity" name="max_qty">
                     @if($errors->has('max_qty'))
                     {{ $errors->first('max_qty')}}
                     @endif
                  </div>
               </div>
               {{-- <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Special Process Flag</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="neg_qty_flag" placeholder="" name="neg_qty_flag" value="N">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div> --}}
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Special Process Flag</label>
                  <div class="col-sm-6">
                      <select class="form-control" name="neg_qty_flag">
                         <option value="N">N - Nothing Special</option>
                         <option value="S">S - SKU required</option>
                      </select>
                  </div>
               </div>
               {{-- <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Zero Qty Flag</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="zero_qty_flag" name="zero_qty_flag"  value="N" >
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div> --}}
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Zero Qty Flag</label>
                  <div class="col-sm-6">
                      <select class="form-control" name="zero_qty_flag">
                         <option value="N">N - Zero Not Allowed</option>
                         <option value="Y">Y - Allow Zero</option>
                      </select>
                  </div>
               </div>
               {{-- <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Return Flag</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="return_flag" name="return_flag" value="N">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div> --}}
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Return Flag</label>
                  <div class="col-sm-6">
                      <select class="form-control" name="return_flag">
                         <option value="N">N - No Return Flag</option>
                         <option value="C">C - Show Cost</option>
                         <option value="E">E - Early Order</option>
                         <option value="P">P - Plusout</option>
                         <option value="R">R - Qty Rounding</option>
                         <option value="D">D - No Disco's</option>
                      </select>
                  </div>
               </div>
               {{-- <div class="form-group">
                  <label for="" class="control-label col-sm-6">History FLag</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="history_flag" name="history_flag" value="N">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div> --}}
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">History FLag</label>
                  <div class="col-sm-6">
                      <select class="form-control" name="history_flag">
                         <option value="N">N - No History</option>
                         <option value="Y">Y - Yes Show History</option>
                         <option value="S">S - Specific Order</option>
                      </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Vendor Account Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="vendor_acct_no" placeholder="Vendor Account Number" name="vendor_acct_no">
                     @if($errors->has('vendor_acct_no'))
                     {{ $errors->first('vendor_acct_no')}}
                     @endif
                  </div>
               </div>
               {{-- <div class="form-group">
                  <label for="" class="control-label col-sm-6">Active Switch</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="active_switch" placeholder="" name="active_switch" value="Y">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div> --}}
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Active Switch</label>
                  <div class="col-sm-6">
                      <select class="form-control" name="active_switch">
                         <option value="N">N</option>
                         <option value="Y" selected="">Y</option>
                      </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="" readonly="" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated</label>
                  <div class="col-sm-6">
                     <input type="date" readonly="" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="padding-left: 260px;">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Add" class="btn">
                        <input type="reset" name="login-submit" id="submit" tabindex="4" value="Reset" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('socadm00/storetransmission')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop