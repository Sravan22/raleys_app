@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
<form class="form-horizontal" role="form" method="post" action="">
   <div class="container" style="">
      <div class="flash-message">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >DEX Commid and Dun Assignment</div>
            </div>
            <div style="padding-top:35px" class="panel-body" >
               <div class="row vertical-divider">
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Vender Number</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="storeid" name="storeid" placeholder="Vender Number" value=""  />
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Name</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Name" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Address</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Address" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">City</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="City" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">State</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="State" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Zip Code</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Zip Code" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Phone</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Phone" name="">
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Rep</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="REP" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Delivery Unit</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Delivery Unit" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Venodor DUN</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="fstnumber" placeholder="Venodor DUN" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">UCC Comm ID</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="fstnumber" placeholder="UCC Comm ID" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Type Code</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Type Code" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Active Switch</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="fstnumber" placeholder="Active Switch" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Last Updated By</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="fstnumber" placeholder="Last Updated By" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Last Update</label>
                           <div class="col-sm-8">
                              <input type="Date" class="form-control" readonly="" id="fstnumber" placeholder="Last Update" name="">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-12 topspace" align="center">
                           <input type="submit" name="login-submit" id="submit" tabindex="4" value="Update" class="btn">
                            <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn">
                           {{ Form::token()}}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>
@stop 