@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transmissions Menu Manager</div>
         </div>
         <br>
         <div class="panel-body" >
            <form action="{{URL::route('socadm00-post-menuquery')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">GL Dept Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="gl_dept_number" placeholder="GL Dept Number" name="gl_dept_number" minlength="1" maxlength="2" onKeyPress="return StopNonNumeric(this,event)">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Menu Level</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="menu_level" placeholder="Menu Level" name="menu_level" minlength="1" maxlength="2" onKeyPress="return StopNonNumeric(this,event)">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Function Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="function_code" placeholder="Function Code" style="text-transform: uppercase" maxlength="2" name="function_code">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="type_code" maxlength="2" style="text-transform: uppercase" placeholder="Type Code" name="type_code">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Sub-Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="subtype_code" maxlength="2" style="text-transform: uppercase" placeholder="Sub-Type Code" name="subtype_code">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Menu Item Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="description" placeholder="Menu Item Description" maxlength="15" name="description">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Position Number on Menu</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="menu_position" placeholder="Position Number on Menu" name="menu_position" minlength="1" maxlength="2" onKeyPress="return StopNonNumeric(this,event)">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Goes to Program</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="goto_pgm_id" placeholder="Goes to Program" name="goto_pgm_id" maxlength="8">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Acitve Switch</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="active_switch" style="text-transform: uppercase" maxlength="1" placeholder="Acitve Switch" name="active_switch">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="last_updated_by" placeholder="" name="last_updated_by">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Last Update</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control" readonly="" id="last_updated" placeholder="" name="last_updated">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="padding-left: 250px;">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="reset" value="Reset" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('socadm00/storetransmission')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
    ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   text-transform: initial;
   }

</style>
@stop

<script type="text/javascript">
   function StopNonNumeric(el, evt)
   {
       //var r=e.which?e.which:event.keyCode;
       //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
       var charCode = (evt.which) ? evt.which : event.keyCode;
       var number = el.value.split('.');
       if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
           return false;
       }
       //just one dot (thanks ddlab)
       if(number.length>1 && charCode == 46){
            return false;
       }
       //get the carat position
       var dotPos = el.value.indexOf(".");
       if( dotPos>-1 && (number[1].length > 3)){
           return false;
       }
       return true;
   }


   </script>