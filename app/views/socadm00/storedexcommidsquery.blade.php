@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>

<div class="col-md-12">
   

   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach

</div>
<div class="container" style="">
   <form action="{{URL::route('socadm00-post-storedexcommidsquery')}}" class="form-horizontal" role="form" method="post" onsubmit="return validateForm()">
      <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >DEX Commid and Dun Assignment</div>
            </div>
            <div style="padding-top:35px" class="panel-body" >
               <div class="row vertical-divider">
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">Vender Number</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" id="vendor_number" name="vendor_number" maxlength="10" placeholder="Vender Number" value=""  />
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Name</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Name" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Address</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Address" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">City</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="City" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">State</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="State" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Zip Code</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Zip Code" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Phone</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Phone" name="">
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Rep</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="REP" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Delivery Unit</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Delivery Unit" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Vendor DUN</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control"  id="duns_number" placeholder="Vendor DUN" maxlength="9" name="duns_number">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">UCC Comm ID</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" maxlength="10" id="commid" placeholder="UCC Comm ID" name="commid">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Type Code</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" style="text-transform: uppercase" minlength="1" maxlength="3" id="type_code" placeholder="Type Code" name="type_code">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Active Switch</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" style="text-transform: uppercase" maxlength="1" id="active_switch" placeholder="Active Switch" name="active_switch">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Last Updated By</label>
                           <div class="col-sm-8">
                              <input type="text" class="form-control" readonly="" id="" placeholder="Last Updated By" name="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputPassword" class="control-label col-sm-4">Last Update</label>
                           <div class="col-sm-8">
                              <input type="Date" class="form-control" readonly="" id="" placeholder="Last Update" name="">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-sm-12 topspace" align="center">
                           <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                           <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('socadm00/storetransmission')}}'" />
                           {{ Form::token()}}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>   
</div>

@stop 
<style type="text/css">
   ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   text-transform: initial;
   }
</style>
{{-- <script type="text/javascript">
   function validateForm()
   {
      var str = document.getElementById("active_switch").value;
      var active_switch = str.toUpperCase();
      if(!(active_switch == 'Y' || active_switch == 'N'))
      {
         alert('Active Switch must be  Y  or  N');
         $('#active_switch').val('');$('#active_switch').focus();return false;
      }
   }
   
</script> --}}