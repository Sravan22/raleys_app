@extends('layout.admindashboard')


@section('content')
@section('section')

  <div class="container">
     @foreach ($userinfo as $usr)	 

					<div class="containerhome homepageicons"> 
   <!--  <div class="row"> -->
   @if($usr->admin  === 1)
            <a href="{{ URL::route('admin-viewusers')}}" accesskey="1">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               Admin
            </div>
            </div>
            </a>
 @endif 
  @if($usr->superadmin  === 1)
            <a href="{{ URL::route('admin-create')}}" accesskey="2">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               Superadmin
            </div>
            </div>
            </a>
@endif
 @if($usr->subadmin  === 1)
            <a href="{{ URL::route('admin-viewusers')}}" accesskey="3">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Subadmin
            </div>
            </div>
            </a>

   @endif        
      <!--   </div> -->
</div>	
		 @endforeach						
					
		</div>
@stop
