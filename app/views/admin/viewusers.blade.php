@extends('layout.admindashboard')


@section('content')
@section('section')

 
      <div class="container">
      <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
  <div class="panel-body">
        <table class="table table-bordered">
      <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
             @if(Auth::user()->username == 'admin') <th>Action</th> @else  @endif
        </tr>
      </thead>
      <tbody id="userlistall">
       @foreach ($superadminlist as $sir)
        <tr class="" id="{{ $sir->id }}">
            <td>{{ $sir->firstname}}</td>
            <td>{{ $sir->email }}</td> 
            @if(Auth::user()->username == 'admin') 
           
            	<!-- <button type="button" class="btn btn-default btn-sm" > -->
                              <td>
                              <a href="{{ route('admin-edituser',['id'=>$sir->id]) }}" class="glyphicon glyphicon-edit">Update</a>
                              
                              

			   <!--  </button> -->
             &nbsp;&nbsp; 
            <!-- <button type="button" class="btn btn-default btn-sm"> -->
            <a href="{{ route('admin-deleteuser',['id'=>$sir->id]) }}" class="glyphicon glyphicon-trash">Delete</a>
             
            <!-- </button> -->
            </td>  @else @endif
        </tr>
        @endforeach
      </tbody>
    </table>  
      </div>
    <?php //echo $superadminlist->links(); ?>
</div>	
<style type="text/css">
   .table-bordered > thead > tr > th
  {
    background-color: #000;
    text-align: center;
    color: #ffffff;
  }
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">  
$(document).ready(function(){
$(".glyphicon-trash").click(function (e) { 
	e.preventDefault();
	var value = $(".glyphicon-trash").attr("href");
	
	var userid = value.substr(value.indexOf("=") + 1);
	
   if(userid != '')
   {
   	 $.post("deleteuser",{userid: userid}, function(result){
	 	//alert(result);
   	 	 
     	//userlistall
     	//$("#userlistall").html(result);
		jQuery('#'+userid).remove();
       });
   }
});
});
</script>
@stop
