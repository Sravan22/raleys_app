@extends('layout.admindashboard')


@section('content')
@section('section')

  <div class="container">
  
    	  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-9 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#000; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" style="color:#fff;" >User Register Account</div>
                       </div> 

                 <div style="padding-top:30px" class="panel-body" >

						    @foreach ($superadminlist_user as $usr)
                            
								<form action="{{URL::route('admin-update-register')}}" class="form-horizontal" method="post" role="form" 
                                        style="display: block;">
                                <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> 
                         <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Store Number</label>
			            <div class="col-sm-9">
                        <input type="hidden" class="form-control" id="userid" name="userid" value="{{  $usr->id }}" readonly />
			                <input type="text" class="form-control" name="storenumber" value="{{ $usr->store_number }}" placeholder="storenumber" >
			                     
			            </div>
			        </div>				

					<div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">First Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="firstname" value="{{ $usr->firstname }}" placeholder="First Name">
			                      
			            </div>
			        </div>				

					<div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Last Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="lastname" value="{{ $usr->lastname }}" placeholder="Last Name">
			                 
			            </div>
			        </div>	

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">User Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="username" value="{{ $usr->username }}" placeholder="User Name">
			                
			            
			            </div>
			        </div>

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">User Email</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="useremail" value="{{ $usr->email }}" placeholder="User Email">
			                
			            </div>
			        </div>

			       <!-- <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Password</label>
			            <div class="col-sm-9">
			                <input type="password" class="form-control" name="passwrod"{{ (Input::old('passwrod'))?' 
			                       value="'.Input::old('passwrod').'"':''}} placeholder="Passwrod">
			                
			            </div>
			        </div>

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Retype-Password</label>
			            <div class="col-sm-9">
			                <input type="password" class="form-control" name="retypepassword"{{ (Input::old('retypepassword'))?' 
			                       value="'.Input::old('retypepassword').'"':''}} placeholder="Retype Passwrod">
			                 
			            </div>
			        </div>  -->
                    
                    
                     <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">User Type</label>
			            <div class="col-sm-9">
                        @if($usr->superadmin  === 1)
			                <input type="checkbox"  name="Superadmin"  value="1"  checked> Superadmin
                           @else 
                             <input type="checkbox"  name="Superadmin"  value="1"> Superadmin
                        @endif 
                         @if($usr->admin  === 1)
			                <input type="checkbox"  name="admin"  value="1"  checked> Admin	
                           @else 
                             <input type="checkbox"  name="admin"  value="1"> Admin
                        @endif 
                         @if($usr->subadmin  === 1)
			                <input type="checkbox"  name="subadmin"  value="1"  checked> Subadmin
                           @else 
                             <input type="checkbox"  name="subadmin"  value="1"> Subadmin
                        @endif    
                        
                          
                            
                            
			                
			            </div>
			        </div>

				<div class="form-group">
					<div class="row">
					<div class="col-sm-12" align="center">
                  
					<input type="submit" name="login-submit" id="login-submit" tabindex="4" value="Update" class="btn">
                         {{ Form::token()}}
					</div>
					</div>
				</div>

					<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
	
	
											
												
						<!-- <a href="{{ URL::route('admin')}}" tabindex="5" class="forgot-password">Sign In</a>							
                          &nbsp; &nbsp;
                         <a href="{{ URL::route('admin-forgotpassword')}}" tabindex="5" class="forgot-password">Forgot Password?</a> -->
                         
												</div>
											</div>
										</div>
									</div>

			</form>
             @endforeach
					</div>
				</div>
			</div>
		</div>

		<div class="container">
  <div class="panel-body">
          
      </div>
    <?php //echo $superadminlist->links(); ?>
</div>	
<style type="text/css">
   .table-bordered > thead > tr > th
  {
    background-color: #000;
    text-align: center;
    color: #ffffff;
  }
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">  
$(document).ready(function(){
$(".glyphicon-trash").click(function (e) { 
	userid=this.id;
   if(userid != '')
   {
   	 $.post("deleteuser", {userid: userid}, function(result){

   	 	alert(result);
     	//userlistall
     	$("#userlistall").html(result);
       });
   }
});
});
</script>
@stop
