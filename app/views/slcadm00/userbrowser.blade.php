@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
    @include('slcadm00.slcadm00menu')
</header>
<div class="container">
    <div class="menu_search_line">    
    </div>
    <div class="container" style="padding-top:10px;">
        <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
            <style type="text/css">
                tr a {
                    text-decoration: underline;
                }
            </style>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th rowspan="2">User Id </th>
                        <th rowspan="2">User Name</th>
                        <th colspan="4" style="text-align: center">Allow</th>
                    </tr>
                    <tr>
                        <th>Add</th>
                        <th>Update</th>
                        <th>Delete</th>
                        <th>Batch</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $row)
                    <tr>
                        <td><a href="{{URL::route('slcadm00-user-update',array('userid'=>$row->userid,'seq_no'=>$row->seq_no))}}">{{$row->userid}}</a></td>
                        <td>{{$row->name}}</td>

                        <td>{{$row->add_cd}}</td>
                        <td>{{$row->update_cd}}</td>
                        <td>{{$row->delete_cd}}</td>
                        <td>{{$row->batch_cd}}</td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
          <div class="row">
    <div class="col-sm-6" align="left"> {{$pagination->links()}}</div>
        <div class="col-sm-6" align="left">
           
            <input type="button" value="Back" class="btn" style=" margin: 20px 0;" onclick="window.location.href='{{URL::route('slcadm00-user-query')}}'" />
        </div>
        </div>


        </form>
    </div>
    @stop
