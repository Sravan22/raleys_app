@extends('layout.dashboard')
@section('page_heading','Pos Field')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00menu')
    
    </header>
    <style type="text/css" media="screen">
    .text-align{
      text-align: center;
      }  
    </style>
    
 <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
       <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>

<table class="table table-striped">
    <thead>
    <tr>
        <th class="text-center" rowspan="2">Pos Type</th>
        <th class="text-center" rowspan="2">Master Field Number</th>
        <th class="text-center" rowspan="2">Pos Field Number</th>
        <th class="text-center"  rowspan="2">Description Displayed</th>
        <th  colspan="2" style="text-align:center">Field</th>
                </tr>
        <tr> 
    <th class="text-center">Type</th>
    <th class="text-center">Length</th> 
    </tr>        
    </thead>
    <tbody>
    
        
        @foreach($data as $row)
      <tr>
        <td class="text-center">{{$row->pos_type}}</td>
        <td class="text-center"><a href="{{URL::route('slcadm00-pos-update',array('mstr'=>$row->mstr_field_no))}}">{{$row->mstr_field_no}}</a></td>
        <td class="text-center">{{$row->pos_field_no1}}</td>
        <td class="text-center">{{$row->desc_displayed}}</td>
        <td class="text-center">{{$row->field_type}}</td>
        <td class="text-center">{{$row->field_length}}</td> 
      </tr>
      @endforeach
    </tbody>
  </table>
<div class="row">
    <div class="col-sm-6" align="left"> {{$pagination->links()}}</div>
        <div class="col-sm-6" align="left">
           
            <input type="button" value="Back" class="btn" style=" margin: 20px 0;" onclick="window.location.href='{{URL::route('slcadm00-pos-query')}}'" />
        </div>
        </div>
 </form>
</div>
@stop
