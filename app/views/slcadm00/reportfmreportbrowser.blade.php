@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00reportfmmenu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    
</div>
   
     
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
        <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>
<table class="table table-striped">
    <thead>
    <tr>
        <th>User</th>
        <th>Date / Time</th>
        <th> UPC / PLU / SKU</th>
        <th> Description</th>
     
        <th> FM Note</th>
    </tr>
    </thead>
    <tbody>
        
        @foreach($data->results as $row)
      <tr>
          <td>{{$row->userid}} {{$row->user_name}}</td>
          <td>{{date('m/d/Y',strtotime($row->update_date))}} {{$row->update_time}}</td>        
        
        <td>{{$row->item_code}}</td>
        <td>{{$row->item_desc}}</td>
       
        <td>{{$row->new_value}}</td>
      
      </tr>
      
      @endforeach
      
    </tbody>
  </table>

 
            <h3 class="text-center">User Summary</h3>
            <table class="table table-striped">

                <tbody>
                    
                 
                   
                    <tr>
                        <td>  Total FM Notes for User: </td>
                        <td> {{$data->tot_fm_notes}}</td>

                    </tr>
                   

                </tbody>
            </table>

 </form>
</div>
@stop
