@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00menu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >POS Type</div>
         </div>
         
         <div class="panel-body">
            <form action="{{URL::route('slcadm00-types-add')}}" class="form-horizontal" method="post" role="form" id="typesadd" style="display: block;">
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label col-sm-5">Pos Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control keyfirst UpperChar" id="id" placeholder="Pos Type Code" name="id" value="{{$data->id}}" autofocus="" tabindex="2" maxlength="1" onkeydown="upperCaseF(this)" />
                     @if($errors->has('id'))
                     {{ $errors->first('id')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="description" placeholder="Description" name="description" value="{{$data->description}}" tabindex="3"  onkeydown="upperCaseF(this)" maxlength="10"/>
                     @if($errors->has('description'))
                     {{ $errors->first('description')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Active</label>
                  <div class="col-sm-6">
                     <select class="form-control" id="active_switch" placeholder="Active" name="active_switch" tabindex="4">
                     <option value="Y" @if($data->active_switch=='Y')  {{'selected'}} @endif>Y</option>
                     <option value="N" @if($data->active_switch=='N' || $data->active_switch=='')  {{'selected'}} @endif>N</option >
                     </select>
                     @if($errors->has('active_switch'))
                     {{ $errors->first('active_switch')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="last_updated_by" placeholder="Last Updated By" name="last_updated_by" value="{{$data->last_updated_by}}" readonly="" />
                     @if($errors->has('last_updated_by'))
                     {{ $errors->first('last_updated_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="last_update" placeholder="mm/dd/yyyy" name="last_update" value="{{date('m/d/Y H:i',strtotime($data->last_update))}}" readonly =""/>
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 47%">
                        <input type="submit" id="submit"  name="accept-submit" tabindex="5" value="Add" class="btn"/>
                        <input type="reset"  id="reset" tabindex="6" value="Reset" class="btn keylast"/>
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="7"></div>
            </form>
         </div>
         
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
      $("#typesadd").validate({
      rules:{
         id:{
            required:true
         },
         description:{
            required:true
         }

      },
      messages:{
         id: "Pos Type Code is required",
         description:"Description is required"
      }
   });
   });
</script>
@stop