@extends('layout.dashboard')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00reportmodmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Modification Audit<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('slcadm00-report-mod-browse')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">User ID</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="userid" placeholder="User ID" name="userid" tabindex="2" autofocus="" />
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                  </div>
               </div>

   
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control lastdate" id="update_date" placeholder="Date" name="update_date" tabindex="3" />
                     @if($errors->has('update_date'))
                     {{ $errors->first('update_date')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Time</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="update_time" placeholder="Time" name="update_time" tabindex="4" />
                     @if($errors->has('update_time'))
                     {{ $errors->first('update_time')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Action Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="action_cd" placeholder="Action Code" name="action_cd" tabindex="5"/>
                     @if($errors->has('action_cd'))
                     {{ $errors->first('action_cd')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Maintenance Type</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="maint_type_cd" placeholder="Maintenance Type" name="maint_type_cd" tabindex="6" />
                     @if($errors->has('maint_type_cd'))
                     {{ $errors->first('maint_type_cd')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5"> UPC / PLU / SKU</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="item_code" placeholder=" UPC / PLU / SKU" name="item_code" tabindex="7" />
                     @if($errors->has('item_code'))
                     {{ $errors->first('item_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Field Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="mstr_fld_no" placeholder="Field Number" name="mstr_fld_no" tabindex="8" />
                     @if($errors->has('mstr_fld_no'))
                     {{ $errors->first('mstr_fld_no')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Original Value</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="orig_value" placeholder="Original Value" name="orig_value" tabindex="9" />
                     @if($errors->has('orig_value'))
                     {{ $errors->first('orig_value')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">New Value</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="new_value" placeholder="New Value" name="new_value" tabindex="10" />
                     @if($errors->has('new_value'))
                     {{ $errors->first('new_value')}}
                     @endif
                  </div>
               </div>
               

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="11" value="Search" class="btn">
                        <input type="reset" name="accept-submit" id="submit" tabindex="12" value="Reset" class="btn">
                          <input type="reset" id="cancel" tabindex="13" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('/slcadm00/report')}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="14"></div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@section('jssection')
 @parent
 <script>
  
  </script>
  
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
 <script src="{{ URL::asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
  <script>
  $( function() {
    $( "#update_time" ).datetimepicker({
        viewSelect: 'hour',
        startView:"hour",
        autoclose:true,
        format:"hh:ii:ss"
    });
     $('#focusguard-2').on('focus', function() {
  $('#userid').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#cancel').focus();
});

  } );
  </script>
   
      @endsection
@stop