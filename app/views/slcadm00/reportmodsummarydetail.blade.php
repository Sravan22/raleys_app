@extends('layout.dashboard')
@extends('layout.datejs')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00reportmodmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Modification Audit<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('slcadm00-report-mod-summary-browse')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group" style="padding-top: 20px;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

                  <label for="inputPassword" class="control-label  col-sm-5">User ID</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="userid" placeholder="User ID" name="userid" tabindex="2" autofocus="" />
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                  </div>
               </div>

   
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control lastdate" id="update_date" placeholder="Date" name="update_date" tabindex="3" />
                     @if($errors->has('update_date'))
                     {{ $errors->first('update_date')}}
                     @endif
                  </div>
               </div>
              

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="reset" name="accept-submit" id="submit" tabindex="5" value="Reset" class="btn">
                          <input type="reset" id="cancel" tabindex="6" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('/slcadm00/report')}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="7"></div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@section('jssection')
 @parent
  
  <script>
  $( function() {
    
  $('#focusguard-2').on('focus', function() {
  $('#userid').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#cancel').focus();
});

  } );
  </script>
     
      @endsection
@stop