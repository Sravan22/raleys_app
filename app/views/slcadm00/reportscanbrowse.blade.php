@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00reportscanmenu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
       <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>
<table class="table table-striped">
    <thead>
    <tr>
        <th>User ID</th>
        <th>Date / Time</th>
        <th> UPC / PLU / SKU</th>
        <th> Item Description</th>
        <th> Item Department </th>
      
    </tr>
    </thead>
    <tbody>
       @foreach($data as $row)
      <tr>
           <td>{{$row->userid}}</td>
       
          <td>{{date('m/d/Y',strtotime($row->update_date))}} {{$row->update_time}}</td>        
        
        <td>{{$row->item_code}}</td>
        <td>{{$row->item_desc}}</td>
        <td>{{$row->item_dept}}</td>
       
      
      </tr>
      
      @endforeach
      
    </tbody>
  </table>

 </form>
</div>
@stop
