@extends('layout.dashboard')
@section('page_heading',$data->page_title)
@section('content')
@section('section')
<header class="row">
    @include('slcadm00.slcadm00menu')

</header>
<div class="col-md-12">
    <br>
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <div class="alert alert-{{ $msg }}" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button>
        {{ Session::get('alert-' . $msg) }}                               
    </div>
    @endif
    @endforeach
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <b> {{$data->page_title}} </b>
        </div>
        <div class="col-sm-6"> 
            <b>  USER ID:</b> {{$data->usrid}}  {{$data->name}}
        </div>
        <div class="col-sm-2"> 

            <input type="button" onclick='window.location.href ="{{URl::route('slcadm00-display-via-fm-add',array('userid' => $data->usrid, 'name' => $data->name, 'pos_type' => $data->pos_type, 'fldtype' => $data->fldtype,'seq_no'=>$data->seq_no))}}"' value="Add New" class="btn">
        </div>
    </div>
</div>
<div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
    <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
        <style type="text/css">
            tr a {
                text-decoration: underline;
            }
        </style>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Seq No.</th>
                    <th>----------Fields------------</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($data->results))
                @foreach($data->results as $row)
                <tr>
                    <td> <a href="{{URl::route('slcadm00-display-via-fm-update',array('userid' => $data->usrid, 'name' => $data->name, 'pos_type' => $data->pos_type, 'fldtype' => $data->fldtype,'seq_no'=>$data->seq_no,'rseq_no'=>$row->seq_no))}}">{{$row->seq_no}}</a> </td>
                    <td>{{$row->mstr_field_no}}   &nbsp;&nbsp;&nbsp;&nbsp;     {{$row->description}}</td>
                </tr>

                @endforeach
                @endif

            </tbody>
        </table>
             <div class="row">
    <div class="col-sm-6" align="left"> </div>
        <div class="col-sm-6" align="left">
           
            <input type="button" value="Back" class="btn" style=" margin: 20px 0;" onclick="history.go(-1);" />
        </div>
        </div>

        {{$pagination->links()}}
    </form>
</div>
@stop
