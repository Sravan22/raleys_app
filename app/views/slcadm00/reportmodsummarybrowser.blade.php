@extends('layout.dashboard')
@section('page_heading','Maintenance Summary')
@section('content')
@section('section')

<header class="row">
    @include('slcadm00.slcadm00reportmodmenu')

</header>
<div class="container">
    <div class="menu_search_line">    
    </div>
    <div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
    <div class="container" style="padding-top:10px;">
        <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
            <style type="text/css">
                tr a {
                    text-decoration: underline;
                }
            </style>
            <table class="table table-striped">

                <tbody>
                    <tr>
                        <td><b>User:</b> {{$data->userid}} {{$data->user_name}}</td> 
                        <td><b>Scan Date:</b> {{$data->update_date}}</td> 
                    </tr>
                    <tr><td></td><td></td></tr>
                    <tr>
                        <td>Total Items Scanned: </td>
                        <td> {{$data->tot_scanned}}</td>
                    </tr>
                    <tr>
                        <td>Total Aisle Scanned:</td>
                        <td> {{$data->tot_aisle_scanned}}</td>
                    </tr>
                    <tr>
                        <td>Items Not Found: </td>
                        <td> {{$data->tot_notfound}} </td>

                    </tr>
                    <tr>
                        <td> Items Added:</td>
                        <td> {{$data->tot_add}}</td>

                    </tr>
                    <tr>
                        <td>  Items Deleted:</td>
                        <td> {{$data->tot_del}}</td>

                    </tr>
                    <tr>
                        <td>  Price Changes:</td>
                        <td>{{$data->tot_updprice}}</td>



                    </tr>
                    <tr>
                        <td>     Updated Items: </td>
                        <td>{{$data->tot_upd}}</td>

                    </tr>
                    <tr>
                        <td>   Total FM Notes: </td>
                        <td> {{$data->tot_fm_notes}}</td>

                    </tr>
                    <tr>
                        <td>  Total Signin's:</td>
                        <td> {{$data->tot_signin}}</td>

                    </tr>
                    <tr>
                        <td>  Total Signout's: </td>
                        <td> {{$data->tot_signout}}</td>

                    </tr>
                    <tr>
                        <td>  Total Unknown:</td>
                        <td> {{$data->tot_unknown}}</td>
                    </tr>

                </tbody>
            </table>

        </form>
    </div>
    @stop
