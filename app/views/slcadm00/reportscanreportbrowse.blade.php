@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00reportscanmenu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
       <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>
<table class="table table-striped">
    <thead>
    <tr>
        <th>User ID</th>
        <th>Date / Time</th>
        <th> UPC / PLU / SKU</th>
        <th> Item Description</th>
        <th>Field</th>
        <th> Item Department </th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
       @foreach($data->results as $row)
      <tr>
           <td>{{$row->userid}} {{$row->user_name}}</td>
       
          <td>{{date('m/d/Y',strtotime($row->update_date))}} {{$row->update_time}}</td>        
        
        <td>{{$row->item_code}}</td>
       
        <td>{{$row->item_desc}}</td>
         <td>{{$row->mstr_fld_no}} {{$row->mstr_fld_desc}}</td>
        <td>{{$row->item_dept}}</td>
       <td>{{$row->action_desc}}</td>
      
      </tr>
      
      @endforeach
      
    </tbody>
  </table>


<h3 class="text-center">User Summary</h3>
    <table class="table table-striped">

                <tbody>
                   
                    <tr>
                        <td>Total Items Scanned</td>
                          <td>Total Aisle Scanned</td>
                           <td>Items Not Found </td>
                             <td> Items Added</td>
                               <td>  Items Deleted</td>
                                 <td>  Price Changes</td>
                                   <td>     Updated Items </td>
                                   <td>   Total FM Notes: </td>
                                     <td>  Total Signin's</td>
                                      <td>  Total Signout's </td>
                                      <td>  Total Unknown</td>
                      
                    </tr>
                     <tr><td colspan="11"></td></tr>
                    @foreach($data->total_sum as $uid => $data)
                    
                   
                    <tr>
                        <td colspan="11"><b>For User Id: <?php echo $uid; ?></b></td> 
                    </tr>
                   
                    <tr>
                        <td> {{$data['tot_scanned']}}</td>
                        <td> {{$data['tot_aisle_scanned']}}</td>
                          <td> {{$data['tot_notfound']}} </td>
                           <td> {{$data['tot_add']}}</td>
                            <td> {{$data['tot_del']}}</td>
                             <td>{{$data['tot_updprice']}}</td>
                             <td>{{$data['tot_upd']}}</td>
                             <td> {{$data['tot_fm_notes']}}</td>
                                <td> {{$data['tot_signin']}}</td>
                                <td> {{$data['tot_signout']}}</td>
                                  <td> {{$data['tot_unknown']}}</td>

                    </tr>
                    
                    @endforeach
                  

                </tbody>
            </table>


 </form>
</div>
@stop
