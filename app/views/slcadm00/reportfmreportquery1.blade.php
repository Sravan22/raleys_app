
@extends('layout.dashboard')
@section('content')
@section('section')
<style>
.table th,td {
   text-align: center;   
}
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('slcadm00.slcadm00reportfmmenu')
</header>
<div class="container">
   <div class="row">
      
      <div class="col-md-12 text-right">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
         {{--  <a href="" target="_blank">
         <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
         </a> --}}
         </span>
      </div>
   </div>
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="row">
         <div class="col-md-2">
            <div class="panel-title">SLFMNDTL</div>
         </div>
         <div class="col-md-8">
            <div class="panel-title text-center">
               <b>
                  <h4>S L I C &nbsp;&nbsp; S Y S T E M</h4>
               </b>
            </div>
         </div>
         <div class="col-md-2 text-left">
            <div class="panel-title">Time</div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12 text-center" style="font-family: Georgia, serif">
            <div class="panel-title">
               <p>SELF &nbsp; LABEL &nbsp; INFO  &nbsp;CONFIRMATION</p>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12 text-center">
            <div class="panel-title">
               <b>
                  <h4>F M &nbsp;&nbsp; N o t e s </h4>
               </b>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12 text-center">
            <div class="panel-title"> Store : </div>
         </div>
      </div>
       <div class="row">
         <div class="col-md-12 text-left">
            <div class="panel-title">
              User: {{$results[0]['userid']}}
            </div>
         </div>
      </div>
      <div style="padding-top:30px" class="panel-body">
         <table class="table table table-striped table-responsive">
            <thead>
               <tr>
                  <th>Date</th>
                  <th>Time</th>
                  <th>UPC / PLU / SKU</th>
                  <th>Description</th>
                  <th>FM Note</th>
               </tr>
            </thead>
            <tbody>
            @foreach($results as $row)
               <tr>
               
                  <td>{{$row['update_date']}}</td>
                  <td>{{$row['update_time']}}</td>
                  <td>{{$row['item_code']}}</td>
                  <td>{{$row['item_desc']}}</td>
                  <td>{{$row['new_value']}}</td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
</div>
@stop