<div class="container" >
    <div class="menu_search_line" >
        <ul class="navbar dib_float_left" >
            <div class="dropdown sub">
                <a id="dLabel" role="button" class="menu_choice" data-target="#" href="{{ URL::route('slcadm00-report-mod-query')}}">
                    Search
                </a>
                
            </div>  
           
            <div class="dropdown sub">
                <a id="dLabel" role="button" class="menu_choice" data-target="#" href="{{ URL::route('slcadm00-report-mod-summary-query')}}">
                    Summary Report
                </a>
               
            </div>  
           
			 <div class="dropdown sub">
                <a id="dLabel" href="{{ URL::route('slcadm00-report')}}" role="button" 
                   class="menu_choice" data-target="#">
                                     Exit            
                </a>
            </div>
