@extends('layout.dashboard')
@section('page_heading','Update Field')
@section('content')
@section('section')
<header class="row">
    @include('slcadm00.slcadm00menu')
</header>
<div class="col-md-12">
    <br>
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <div class="alert alert-{{ $msg }}" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button>
        {{ Session::get('alert-' . $msg) }}                               
    </div>
    @endif
    @endforeach
</div>
<div class="container">
    <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title">Update Field</div>
            </div>
            <div style="padding-top:30px;" class="panel-body" >
                <form action="{{URl::route('slcadm00-display-via-fm-update',array('userid' => $data->usrid, 'name' => $data->name, 'pos_type' => $data->postyp, 'fldtype' => $data->fldtype,'seq_no'=>$data->sqno,'rseq_no'=>$data->rsqno))}}" class="form-horizontal" method="post" role="form" style="display: block;"  id="frmfld" name="frmfld">
                    <div class="form-group ">
                        <div class="col-md-offset-1 col-md-4">
                            <label for="inputPassword" class="control-label col-sm-12">User ID</label>
                        </div>

                        <div class="col-sm-7">
                            {{$data->userid}}   {{$data->name}}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-4">
                            <label for="inputPassword" class="control-label col-sm-12">Sequence No.</label>
                        </div>
                        <div class="col-sm-7">
                            {{$data->rsqno}}

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-4">
                            <label for="inputPassword" class="control-label col-sm-12">Field No.</label>
                        </div>
                        <div class="col-sm-7">

                            <select name="mstr_field_no" id="mstr_field_no" class="form-control">                          
                                @foreach($data->mstfld_cur as $mstrow)
                                <option value="{{$mstrow->field_no}}" @if($data->mstr_field_no==$mstrow->field_no) {{'selected'}} @endif >{{$mstrow->description}}</option>
                                @endforeach
                            </select>



                            @if($errors->has('mstr_field_no'))
                            {{ $errors->first('mstr_field_no')}}
                            @endif


                        </div>
                    </div>

                    <input type="hidden" name="usrid" value="{{$data->usrid}}" />
                    <div class="form-group" style="padding-top: 10px">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <input type="submit" name="update-submit" id="submit" tabindex="4" value="Update" class="btn">
                                <input type="button" name="delete-submit" onclick="confirmdelete()" id="submit" tabindex="4" value="Delete" class="btn">
                                <input type="button" id="submit" tabindex="4" onclick='window.location.href ="{{URl::route('slcadm00-user-fm-display',array('userid' => $data->usrid, 'name' => $data->name, 'pos_type' => $data->postyp, 'fldtype' => $data->fldtype,'seq_no'=>$data->sqno,'rseq_no'=>$data->rsqno))}}"' value="Cancel" class="btn">
                                {{ Form::token()}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@section('jssection')
@parent

<script>
    function confirmdelete() {


        var x = confirm("Remove this field from list?");
        if (x) {

            window.location.href = "{{URl::route('slcadm00-display-via-fm-update',array('userid' => $data->usrid, 'name' => $data->name, 'pos_type' => $data->postyp, 'fldtype' => $data->fldtype,'seq_no'=>$data->sqno,'rseq_no'=>$data->rsqno,'action'=>'delete'))}}";
            return true;
        }

        return false;
    }
</script>
@endsection
@stop
