@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >POS Field <FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('slcadm00-pos-add')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label col-sm-5">Pos Type</label>
                  <div class="col-sm-6">
                    
                      
                      <select name="pos_type" id="pos_type" class="form-control" tabindex="2" autofocus="" >
                          @foreach($data->post_types as $posrow)
                          <option value="{{$posrow->id}}" @if($data->pos_type==$posrow->id) {{'selected'}} @endif>{{$posrow->description}}</option>
                          @endforeach
                      </select>
                      
                     @if($errors->has('pos_type'))
                     {{ $errors->first('pos_type')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Master Field Number</label>
                  <div class="col-sm-6">
                      
                       <select name="mstr_field_no" id="mstr_field_no" class="form-control" tabindex="3">
                          @foreach($data->mstfld_cur as $mstrow)
                          <option value="{{$mstrow->field_no}}" @if($data->mstr_field_no==$mstrow->field_no) {{'selected'}} @endif>{{$mstrow->description}}</option>
                          @endforeach
                      </select>
                      
                      
                     @if($errors->has('mstr_field_no'))
                     {{ $errors->first('mstr_field_no')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Pos Field Number</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="pos_field_no1" placeholder="Pos Field Number" name="pos_field_no1" value="{{$data->pos_field_no1}}" tabindex="4" />
                     @if($errors->has('pos_field_no1'))
                     {{ $errors->first('pos_field_no1')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Descriptor Displayed</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="desc_displayed" placeholder="Descriptor Displayed" name="desc_displayed" value="{{$data->desc_displayed}}" tabindex="5" />
                     @if($errors->has('desc_displayed'))
                     {{ $errors->first('desc_displayed')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Display Descriptor</label>
                  <div class="col-sm-6">
                      
                         <select class="form-control" id="disp_desc_sw" placeholder="" name="disp_desc_sw" tabindex="6">
                          <option value="Y" @if($data->disp_desc_sw=='Y') {{'selected'}} @endif>Y</option>
                          <option value="N" @if($data->disp_desc_sw=='N' || $data->disp_desc_sw=='') {{'selected'}} @endif>N</option>
                      </select>
                      
                      
                     @if($errors->has('disp_desc_sw'))
                     {{ $errors->first('disp_desc_sw')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Field Type</label>
                  <div class="col-sm-6">
                      
                      
                       <select class="form-control" id="field_type" placeholder="" name="field_type" tabindex="7">
                           <option value="">Select</option>
                          <option value="C" @if($data->field_type=='C') {{'selected'}} @endif>C = Char</option>
                          <option value="D" @if($data->field_type=='D') {{'selected'}} @endif>D = Date</option>
                          <option value="I" @if($data->field_type=='I') {{'selected'}} @endif>I = Integer</option>
                          <option value="M" @if($data->field_type=='M') {{'selected'}} @endif>M = Money</option>
                          <option value="S" @if($data->field_type=='S') {{'selected'}} @endif>S = Y/N Switch</option>
                      </select>
                      
                   
                     @if($errors->has('field_type'))
                     {{ $errors->first('field_type')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Field Length</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="field_length" placeholder="Field Length" name="field_length" value="{{$data->field_length}}" tabindex="8" />
                     @if($errors->has('field_length'))
                     {{ $errors->first('field_length')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Update Via FM</label>
                  <div class="col-sm-6">
                      
                        <select class="form-control" id="upd_via_fm_sw" placeholder="" name="upd_via_fm_sw" tabindex="9">
                         <option value="Y" @if($data->upd_via_fm_sw=='Y') {{'selected'}} @endif>Y</option>
                          <option value="N" @if($data->upd_via_fm_sw=='N' || $data->upd_via_fm_sw=='') {{'selected'}} @endif>N</option>
                      </select>
                      
                     
                     @if($errors->has('upd_via_fm_sw'))
                     {{ $errors->first('upd_via_fm_sw')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Valid Minimum Value</label>
                  <div class="col-sm-6">
             <input type="text" class="form-control" id="min_value" placeholder="Valid Minimum Value" name="min_value" value="{{(int) $data->min_value}}" tabindex="10" />
                     @if($errors->has('min_value'))
                     {{ $errors->first('min_value')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Valid Maximum Value</label>
                  <div class="col-sm-6">
                       <input type="text" class="form-control" id="max_value" placeholder="Valid Maximum Value" name="max_value" value="{{(int) $data->max_value}}" tabindex="11" />
                     @if($errors->has('max_value'))
                     {{ $errors->first('max_value')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Default Value</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="default_value" placeholder=" Default Value" name="default_value" value="{{$data->default_value}}" tabindex="12" />
                     @if($errors->has('default_value'))
                     {{ $errors->first('default_value')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="last_updated_by" placeholder="Last Updated By" name="last_updated_by" value="{{$data->last_updated_by}}" readonly>
                     @if($errors->has('last_updated_by'))
                     {{ $errors->first('last_updated_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update" value="{{date('m/d/Y H:i',strtotime($data->last_update))}}" readonly>
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 48%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="13" value="Add" class="btn">
                        <input type="submit" name="login-submit" id="cancel" tabindex="14" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="15"></div>

            </form>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset("assets/jquery/1.7.0/jquery.min.js") }}"></script>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
<script type="text/javascript">

  $(document).ready(function(){

  $('#field_type').on('change', function(e) {
   
    var field_type = $("#field_type").val();
    //alert(field_type);
    if(field_type == "S")
    {
      $('#field_length').val("1");
      $("#field_length").attr("readonly","readonly");
    }
   else
   {
      $('#field_length').val(" ");
      $("#field_length"). removeAttr("readonly");
   }
   });
 }); 
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#focusguard-2').on('focus', function() {
    $('#pos_type').focus();
  });

  $('#focusguard-1').on('focus', function() {
    $('#cancel').focus();
  });
});
</script>
@stop