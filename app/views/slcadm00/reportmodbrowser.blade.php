@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00reportmodmenu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    
</div>
     <div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
     <input type="button" onclick="window.location.href='{{ URL::route('slcadm00-report-mod-detail-browse',$paginate_array)}}'" name="login-submit" id="submit" tabindex="4" value="Detail Report" class="btn">
      </span>
      </div>
   </div>
</div>
     
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
        <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Date / Time</th>
        <th> UPC / PLU / SKU</th>
        <th> Description</th>
        <th> Action </th>
        <th> Fld No</th>
    </tr>
    </thead>
    <tbody>
        
        @foreach($data as $row)
      <tr>
          <td><a href="{{URL::route('slcadm00-report-mod-view',array('userid'=>$row->userid,'seq_no'=>$row->seq_no))}}">{{date('m/d/Y',strtotime($row->update_date))}} {{$row->update_time}}</a></td>        
        
        <td>{{$row->item_code}}</td>
        <td>{{$row->item_desc}}</td>
        <td>{{$row->action_desc}}</td>
        <td>{{$row->mstr_fld_no}}</td>
      
      </tr>
      
      @endforeach
      
    </tbody>
  </table>
{{$pagination->links()}}
 </form>
</div>
@stop
