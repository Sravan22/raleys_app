@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Master Field <FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('slcadm00-master-update')}}" class="form-horizontal" method="post" role="form" style="display: block;" id="masterupdate">
            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label col-sm-5">Field Number</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="field_no" placeholder="Field Number" name="field_no" value="{{$data->field_no}}" readonly >
                     @if($errors->has('field_no'))
                     {{ $errors->first('field_no')}}
                     @endif
                  </div>
               </div>
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Description</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control keyfirst" id="description" placeholder="Description" name="description" value="{{$data->description}}" autofocus="" tabindex="2" onkeydown="upperCaseF(this)"/>
                     @if($errors->has('description'))
                     {{ $errors->first('description')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Field Type Code</label>
                  <div class="col-sm-6">
                      <select class="form-control" id="type_cd" placeholder="" name="type_cd" tabindex="3">
                         
                          <option value="N" @if($data->type_cd=='N' || $data->type_cd=='')  {{'selected'}} @endif>N = Normal</option>
                           <option value="C" @if($data->type_cd=='C')  {{'selected'}} @endif>C = Combo Field</option>
                           <option value="F" @if($data->type_cd=='F')  {{'selected'}} @endif>F = FM Note</option>
                      </select>
                     @if($errors->has('type_cd'))
                     {{ $errors->first('type_cd')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="last_updated_by" placeholder="Last Updated By" name="last_updated_by" value="{{$data->last_updated_by}}" readonly>
                     @if($errors->has('last_updated_by'))
                     {{ $errors->first('last_updated_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update" value="{{date('m/d/Y H:i',strtotime($data->last_update))}}" readonly>
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Update" class="btn">
                          <input type="button" onclick="window.location.href='{{URL::route('slcadm00-master-query')}}'" name="login-submit" id="submit" tabindex="5" value="Cancel" class="btn keylast">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="6"></div>
            </form>
         </div>
      </div>
   </div>
</div>
<script  type="text/javascript" >
   $(document).ready(function() {
      $("#masterupdate").validate({
      rules:{
         
         description:{
            required:true
         }
      },

      messages:{
         description:"Description is required"
      }
   });

});
</script>

@stop