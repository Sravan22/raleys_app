<div class="container">
  <div class="menu_search_line">
    <ul class="navbar dib_float_left">
    <!-- <li id="main-home-link" class="menu_choice home_menu_choice active">
        Bookkeeper
    </li> -->
    <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">Reg<span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Begin-Loans</a>
                <ul class="dropdown-menu">
                   <li><a href="{{URL::route('menu-bookkeeperregister')}}">Register</a></li>
                   <li><a href="{{URL::route('menu-bookkeeperregistertotal')}}">Totals</a></li>
                   <li><a href="{{URL::route('menu-bookkeeperregistercmt')}}">Comments</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Loans-to-Reg</a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::route('menu-bookloanrregister')}}">Register</a></li>
                    <li><a href="{{URL::route('menu-bookloanrregtotal')}}">Totals</a></li>
                    <li><a href="{{URL::route('menu-bookloanrregcmt')}}">Comments</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Reg-Check-Out</a>
                <ul class="dropdown-menu">
                  <li><a href="{{URL::route('menu-bookregcheckoutitem')}}">Item</a></li>
                  <li><a href="{{URL::route('menu-bookregcheckoutreg')}}">Register</a></li>
                  <li><a href="{{URL::route('menu-bookregcheckouttotl')}}">Total</a></li>
                  <li><a href="{{URL::route('menu-bookregcheckoutprnt')}}">Print</a></li>
                  <li><a href="{{URL::route('menu-bookregcheckoutcmt')}}">Comment</a></li>
                </ul>
              </li>
                <li><a href="{{URL::route('menu-bookregcmt')}}">Comment</a></li>
            </ul>
        </div>
        
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Safe <span class="caret mainheadlink"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Total-Safe-Count</a>
                <ul class="dropdown-menu">
                   <li><a href="{{URL::route('menu-booksafecnt')}}">Detail</a></li>
                   <li><a href="#">Print</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Safe-Report</a>
                <ul class="dropdown-menu">
                 <li><a href="{{URL::route('menu-booksafereport')}}">Safe-Report</a></li>
                 <li><a href="{{URL::route('menu-booksafereportprnt')}}">Print</a></li>
                 <li><a href="{{URL::route('menu-booksafereportcmt')}}">Comments</a></li>
                </ul>
               </li>
               
                 <li><a href="{{URL::route('menu-booksafecmt')}}">Comment</a></li>
            </ul>
        </div>
         
        
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Wkly-recap <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{URL::route('menu-bookwklysales')}}">Sales</a></li>
              <li><a href="{{URL::route('menu-bookwklysales')}}">Recap</a></li>
              <li><a href="{{URL::route('menu-bookwklysales')}}">View</a></li>
              <li><a href="{{URL::route('menu-bookwklysales')}}">Print</a></li>
              <li><a href="{{URL::route('menu-bookwklyunlock')}}">Unlock</a></li>
              <li><a href="{{URL::route('menu-bookwklysales')}}">Comments</a></li>
        </ul>
        </div>
     <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Inv <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Postage</a>
                <ul class="dropdown-menu">
                  <li><a href="{{URL::route('menu-bookpostageadd')}}">Add</a></li>
                  <li><a href="{{URL::route('menu-bookpostageprnt')}}">Print</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Lottery</a>
                <ul class="dropdown-menu">
                 <li><a href="{{URL::route('menu-booklotteryadd')}}">Add</a></li>
                 <li><a href="{{URL::route('menu-booklotterytotl')}}">Totals</a></li>
                 <li><a href="{{URL::route('menu-booklotteryprnt')}}">Print</a></li>
                 <li><a href="{{URL::route('menu-booklotteryview')}}">View</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Commuter</a>
              <ul class="dropdown-menu">
                <li><a href="{{URL::route('menu-bookcommuteradd')}}">Add</a></li>
                <li><a href="{{URL::route('menu-bookcommuterprnt')}}">Print</a></li>
              </ul>
              </li>
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Mthly-Commuter</a>
                <ul class="dropdown-menu">
                 <li><a href="{{URL::route('menu-bookcommutermonthlyadd')}}">Add</a></li>
                 <li><a href="{{URL::route('menu-bookcommutermonthlyprnt')}}">Print</a></li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">pHone</a>
                <ul class="dropdown-menu">
                  <li><a href="{{URL::route('menu-bookphonecardadd')}}">Add</a></li>
                  <li><a href="{{URL::route('menu-bookphonecardprnt')}}">Print</a></li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">epS</a>
                <ul class="dropdown-menu">
                   <li><a href="{{URL::route('menu-bookepsadd')}}">Add</a></li>
                   <li><a href="{{URL::route('menu-bookepsprnt')}}">Print</a></li>
                </ul>
              </li>
                <li><a href="#">Comment</a></li>
            </ul>
        </div>
    

    
       <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Fuel <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             <li><a href="#">N/A for store 305</a></li>
            </ul>
        </div>
        
     <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Mgmt <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             <li><a href="{{URL::route('menu-bookmngmntdaily')}}">Daily</a></li>
          <li><a href="{{URL::route('menu-bookmngmntweekly')}}">Weekly</a></li>
          <li><a href="{{URL::route('menu-bookmngmntcmt')}}">Comments</a></li>
            </ul>
        </div>
    
    <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                cHg-regs <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="#">Query</a></li>
          <li><a href="#">Browse</a></li>
          <li><a href="#">Next</a></li>
          <li><a href="#">Previous</a></li>
          <li><a href="#">Add</a></li>
          <li><a href="#">Update</a></li>
          <li><a href="#">Delete</a></li>
            </ul>
        </div>
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Utility <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{URL::route('menu-bookutilitypymntcnt')}}">Utility Payment Count</a></li>
            </ul>
        </div>
        
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Comments <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="{{URL::route('menu-bookcommentlist')}}">Comment List</a></li>
            </ul>
        </div>

    

 <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Others <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="#">N/A Supervisor only</a></li>
            </ul>
        </div>

    </ul>
</div>
</div>  
