<div class="container">



	<div class="container-fluid">
		
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		    <span class="sr-only">Toggle navigation</span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		  </button>
		  
		</div>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
      <!-- .dropdown -->  
		    <li class="dropdown">
			   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Grocery <span class="caret"></span></a>
			  <ul class="dropdown-menu" role="menu">				          
			  
			    <li class="dropdown-submenu">
			        <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
			        <ul class="dropdown-menu">
			        <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistertotal')}}">SSI Dry Grocery</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">SSI Frz Grocery</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">SSI Misc</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">MidValley Dairy </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">NuCal Egg</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Market Centre</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Core-Mark</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">More options</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Blue Dog</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Market Centre</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Core-Mark</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">More Options</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Blue Dog</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular</a></li>
			        </ul>
			    </li>			            
			    <li><a href="#">Shrink Capture</a></li>
			    <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Non Perish Inv <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Employee one</a></li>
                  <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Employee Two</a></li>
                  <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Employee Three</a></li>
                  <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Employee Four</a></li>
                  <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">Employee Five</a></li>
             
              </ul>
          </li> 
          <li><a href="#">Shrink RIP Team</a></li>
			  </ul>
			</li> <!-- .dropdown -->						

       <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Liquor <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistertotal')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Advance</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Ad Item Ordering</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Plus Outs</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmitd Orders</a></li>
              
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Non Perish Inv <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Employee one</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Employee Two</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Employee Three</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Employee Four</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">Employee Five</a></li>
             
              </ul>
          </li> 
          
        </ul>
      </li> <!-- .dropdown -->
      
      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Meat <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistertotal')}}">SSI Frz Meat</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Tony's</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Ad Item Ordering </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Tony's</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmitd Orders</a></li>
              
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Retail</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Cost</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Sunday Sales</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Transmit Inventory</a></li>
              
             
              </ul>
          </li> 
          
        </ul>
      </li> <!-- .dropdown -->
     
      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Deli <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Tony's</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Ad Item Ordering </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmitd Orders</a></li>
              
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Non Perish Inv <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Employee one</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Employee Two</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Employee Three</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Employee Four</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">Employee Five</a></li>
              
             
              </ul>
          </li> 
          <li><a href="#">Shrink RIP Team</a></li>
          
        </ul>
      </li> <!-- .dropdown -->
     
     <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">FSD <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Reg DC/ Tote</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Tony's</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Ad Item Ordering </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Reg DC /Tote</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Tony's</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Freezer</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Freezer</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmited Orders</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">More Options</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order Schd</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Previous Options</a></li>
              
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Retail</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Cost</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Sunday Sales</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Transmit Inventory</a></li>
              
             
              </ul>
          </li> 
          
          
        </ul>
      </li> <!-- .dropdown -->
       <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Bakery <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Tony's</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">DC Totes</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Tony's</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early DC Totes</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Freezer</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Freezer</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">More Options</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order Schd</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmited Orders</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Previous Options</a></li>
              
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Retail</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Cost</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Sunday Sales</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Transmit Inventory</a></li>
              
             
              </ul>
          </li> 
          
          
        </ul>
      </li> <!-- .dropdown -->
<!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Produce <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Advance </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Ad Item Ordering</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmited Orders</a></li>
             
              
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Cost</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Sunday Sales</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Transmit Inventory</a></li>
              
             
              </ul>
          </li> 
          
          
        </ul>
      </li> <!-- .dropdown -->
      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Nat Fds <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">SSI Dry Nat Fds </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">NF Distributor </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">National Vitamin</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Ad Item Ordering</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmited Orders</a></li>
                <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">More Options</a></li>
                  <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Plus Outs</a></li>
                  <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Previous Options</a></li>
             
              
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Non Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Employee one</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Employee Two</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Employee Three</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Employee Four</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">Employee Five</a></li>
              
             
              </ul>
          </li> 
           <li><a href="#">Shrink RIP Team</a></li>
          
        </ul>
      </li> <!-- .dropdown -->
     
<!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">GM <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Raleys Natomas A</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Raleys Natomas B </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Unified </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Central Pet</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Unified</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">UNFI GM</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Advance</a></li>
              
                <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">More Options</a></li>
                <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
                  <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmitd orders</a></li>
                  <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Previous Options</a></li>
             
              
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Non Perish Inv <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Employee one</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Employee Two</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Employee Three</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutprnt')}}">Employee Four</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutcmt')}}">Employee Five</a></li>
              
             
              </ul>
          </li> 
           <li><a href="#">Shrink RIP Team</a></li>
          
        </ul>
      </li> <!-- .dropdown -->

      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hot Work <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
                         
         
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Cost</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Sunday Sales</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Transmit Inventory</a></li>
          
              
             
              </ul>
          </li> 
          
        </ul>
      </li> <!-- .dropdown -->
      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Floral <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Short / Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Advance</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmited Orders</a></li>
      
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Retail</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Cost</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Sunday Sales</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Transmit Inventory</a></li>
              
             
              </ul>
          </li> 
           <li><a href="#">Shrink RIP Team</a></li>
          
        </ul>
      </li> <!-- .dropdown -->
     
     <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">CoffShp <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Retail</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Cost</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Sunday Sales </a></li>
              
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmit Inventory</a></li>
      
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li><a href="#">Tony's</a></li>
          <li><a href="#">Regular Raley DC</a></li>
          <li><a href="#">Early Regular</a></li> 
           <li><a href="#">Early Tonys</a></li>
          
        </ul>
      </li> <!-- .dropdown -->

      <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Seafood <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Regular Raley DC</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">SSI Frz Seafood</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Tony's </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Pacific Fresh</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Regular</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Early Pacific</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmitd Orders</a></li>
              </ul>
          </li>                 
          <li><a href="#">Shrink Capture</a></li>
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Perish Inventory <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Retail</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Cost</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Sunday Sales</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckouttotl')}}">Transmit Inventory</a></li>
              
             
              </ul>
          </li> 
           <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Signs <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Advertised</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutitem')}}">Regular</a></li>
              <li><a href="{{URL::route('mktmgr-bookregcheckoutreg')}}">Reprint Last</a></li>
              
              
             
              </ul>
          </li> 
           <li><a href="#">Shrink RIP Team</a></li>
          
        </ul>
      </li> <!-- .dropdown -->
       <!-- .dropdown -->  
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">RX <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">                  
        
          <li class="dropdown-submenu">
              <a tabindex="-1" href="#">Orders <i class="fa fa-chevron-right"></i></a>
              <ul class="dropdown-menu">
              <li><a href="{{URL::route('mktmgr-bookkeeperregister')}}">Rx Bergen</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Reg Misc/Supply</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Rx Generic Whse </a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Print Order</a></li>
              <li><a href="{{URL::route('mktmgr-bookkeeperregistercmt')}}">Transmitd Orders</a></li>
              </ul>
          </li>                 
          
        
           
          
        </ul>
      </li> <!-- .dropdown -->
          <li><a href="{{URL::route('socscn01-returnhome')}}">Exit</a></li>
       </ul> <!-- .nav .navbar-nav -->		 
		</div><!-- /.navbar-collapse -->
		
	</div><!-- /.container-fluid -->

 
</div>  

<style>
.dropdown-submenu {
  position: relative;
}
.dropdown-submenu > .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
}
.dropdown-submenu:hover > .dropdown-menu {
  display: block;
}
.dropdown-submenu:hover > a:after {
  border-left-color: #fff;
}
.dropdown-submenu.pull-left {
  float: none;
}
.dropdown-submenu.pull-left > .dropdown-menu {
  left: -100%;
  margin-left: 10px;
}

</style>