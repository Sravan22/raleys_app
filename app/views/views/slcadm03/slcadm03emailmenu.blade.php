<div class="container">



	<div class="container-fluid">
		
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		    <span class="sr-only">Toggle navigation</span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		  </button>
		  
		</div>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
       <li><a href="#">New Mail - Summary</a></li>
        <li><a href="#">Display New Mail</a></li>
        <li><a href="#">Display All Mail</a></li>
        <li><a href="#">View & Print Single letter</a></li>
        <li><a href="#">Delete old news letters</a></li>
        <li><a href="{{ URL::route('slcadm03-returnhome')}}">Exit</a></li>
      </ul> <!-- .nav .navbar-nav -->		 
		</div><!-- /.navbar-collapse -->
		
	</div><!-- /.container-fluid -->

 
</div>  

<style>
.dropdown-submenu {
  position: relative;
}
.dropdown-submenu > .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
}
.dropdown-submenu:hover > .dropdown-menu {
  display: block;
}
.dropdown-submenu:hover > a:after {
  border-left-color: #fff;
}
.dropdown-submenu.pull-left {
  float: none;
}
.dropdown-submenu.pull-left > .dropdown-menu {
  left: -100%;
  margin-left: 10px;
}

</style>