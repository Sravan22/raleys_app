<?php 
   //echo '<pre>';print_r($msg);exit;
 ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>Unlock TABLE for Daily Recap </h3>
         </b>
      </div>
   </div>
</header>
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<div class="col-md-12">

</div>
<div class="container">
<div class="flash-message">
  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Unlock TABLE</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{ URL::route('actsup-unlocktable') }}" id="booksafedate" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="row">
                  <div class="form-group">
                     <!-- <div class="col-sm-6"> -->
                     <label style="padding-right:2px;" for="inputPassword" class="control-label col-sm-5">Enter DATE to be UNLOCKED</label>
                     <!-- </div> -->
                     <div class="col-sm-7">
                       <input type="date" placeholder="MM-DD-YYYY" name="unlockdate" id="" autocomplete="off" isimportant="true" class="form-control" value="{{ $previewsdate }}">
                        @if($errors->has('unlockdate'))
                        {{ $errors->first('unlockdate')}}
                        @endif       
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
   ::-webkit-input-placeholder {
   text-transform: initial;
}

:-moz-placeholder { 
   text-transform: initial;
}

::-moz-placeholder {  
   text-transform: initial;
}

:-ms-input-placeholder { 
   text-transform: initial;
}
</style>
 
@stop