@extends('layout.dashboard')


@section('content')
@section('section')


    <div class="container" style="text-align:center;">    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#ce0002; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >LOGIN</div>
                       </div> 

                 <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
        <form action="{{URL::route('account-sign-post')}}" method="post"> 
        <div class="input-group col-sm-7" class="field" style="margin-left:70px;">
        <div style="margin-top:10px;" class="form-group">
         <label class="control-label col-sm-4" for="jbe">  Email </label>
         <input type="text" name="email"{{ (Input::old('email'))?' value="'.Input::old('email').'"':''}}>
          @if($errors->has('email'))
            {{ $errors->first('email')}}
            @endif
        </div> 
      
        <div style="margin-top:10px" class="form-group">
          <label class="control-label col-sm-4" for="jbe">  Password </label>               
          <input type="password" name="userpassword">
          @if($errors->has('password'))
            {{ $errors->first('password')}}
            @endif
        </div>
                   
        
        <input type="submit" value="Sign In"  class="btn btn-default" style="margin-left:85px;">
        {{ Form::token()}}

         <div style="margin-top:10px" class="form-group">
          <a href="{{ URL::route('account-forgotpassword')}}">Forgot Password</a>
                                    <!-- Button -->
 <label class="control-label col-sm-4" for="jbe">  </label>
                                    <div class="col-sm-7 controls">
                                     
                                     

                                    </div>
                                </div>
                                </form>     
                    </div>                     
                    </div>  
        </div>
         
    </div>
   <!--  <form  action="{{URL::route('account-sign-post')}}" method="post">
        <div class="field">
          Email : <input type="text" name="email"{{ (Input::old('email'))?' value="'.Input::old('email').'"':''}}>
          @if($errors->has('email'))
            {{ $errors->first('email')}}
            @endif
        </div>
        <div class="field">
          Password : <input type="password" name="userpassword">
          @if($errors->has('password'))
            {{ $errors->first('password')}}
            @endif
        </div>
        <a href="{{ URL::route('account-forgotpassword')}}">Forgot Password</a>
        <input type="submit" value="Sign In">
        {{ Form::token()}}
    </form> -->


        
@stop   