<?php

class Somenu extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'somenu';
	protected $SafeCoin_cols = array('entry_number', 'type_code', 'subtype_code', 'menu_position',  'description', 
											  'goto_pgm_id', 'active_switch','last_updated_by','last_update');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
   

}
