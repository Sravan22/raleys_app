<?php

class SafeOther extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'safe_other';
	protected $SafeOther_cols = array('food_stamps', 'merch_fives', 'merch_tens', 'merch_twenties', 
											  'merch_misc', 'script1','script2', 'script3', 
											  'script4', 'script5', 'script6', 'script7', 'script8', 'script9', 'script10', 'begin_loans', 'misc1_amt', 'misc1_desc', 'misc2_amt', 'misc2_desc', 'instr_chrg', 'pay_adv', 'night_crew', 'debit', 'credit', 'ebt', 'giftcard', 'misc3_amt', 'misc3_desc', 'safe_date');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
   

}
