<?php

class ChgReg extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'registers';
	protected $ChgReg_cols = array('reg_num','reg_type');
	/**re
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     public $timestamps = false;

}
