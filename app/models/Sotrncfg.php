<?php

class Sotrncfg extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sotrncfg';
	protected $SafeCoin_cols = array('entry_number', 'type_code', 'subtype_code', 'description', 
											  'partition_code', 'dept_number','vendor_acct_no', 'max_item_length','max_qty', 'neg_qty_flag', 'zero_qty_flag', 'return_flag','history_flag','active_switch','last_updated_by','last_update');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
   

}
