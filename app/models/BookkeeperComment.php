<?php

class BookkeeperComment extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'comment';
	protected $SafeCoin_cols = array('date_stamp', 'comment_line1', 'comment_line2', 'comment_line3', 'comment_line4', 'comment_line5','comment_line6', 'comment_line7','comment_line8', 'comment_line9');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
}
