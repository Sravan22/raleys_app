<?php

class SafeCurrent extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'safe_curr';
	protected $SafeCurrent_cols = array('ones_box', 'ones_roll', 'ones_held', 'fives_box', 
											  'fives_roll', 'fives_held','tens_box', 'tens_roll', 
											  'tens_held', 'twenties_box', 'twenties_roll', 'twenties_held', 'misc1_box', 'misc1_roll', 'misc1_held', 'misc1_desc', 'misc2_box', 'misc2_roll', 'misc2_held', 'misc2_desc', 'safe_date');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
   

}
