<?php

class Socadm00Sodsdhdr extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sodsdhdr';
	protected $socadm00_sodsdhdr_cols = array('vendor_number', 'cost_discrep_sw', 'status_code', 'tot_vend_qty', 
	'tot_vend_cost', 'tot_vend_allow','tot_vend_crv', 'invoice_date','credit_memo_id', 'create_datetime', 'trns_number', 'tot_sell_supply',	'tot_store_supply');
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
     const CREATED_AT = 'create_datetime';
     const UPDATED_AT = 'last_update';

}
