<?php

class Sovndschd extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sovndschd';
	protected $SafeCoin_cols = array('gl_dept_number', 'type_code', 'subtype_code', 'vendor_number', 'partition_code', 'deadline','resume_time','description','sun_sw','mon_sw','tue_sw','wed_sw','thr_sw','fri_sw','sat_sw','active_sw','last_updated_by','last_update','hard_deadline_sw','reminder_sw','days_history');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
   

}
