<?php

class UserSlcadm00Controller extends BaseController {

    public function getReturnHome() {
        return View::make('slcadm00.home');
    }

    Public function getTypesQuery() {
        $work_data = new StdClass();

        return View::make('slcadm00.typesquery')->with('data', $work_data);
    }

    public function getTypesbrowser() {

        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {


        $int_flag = FALSE;

        $this->id = trim(Input::get('id'));
        $this->description = trim(Input::get('description'));
        $this->last_updated_by = trim(Input::get('last_updated_by'));
        $this->last_update = trim(Input::get('last_update'));
        //$this->active_switch = trim(Input::get('active_switch'));

        $paginate_array = array();

        $sql = "SELECT * FROM slpostyp WHERE 1=1 ";

        if ($this->id != '') {
            $sql.=" AND id='" . strtoupper($this->id) . "' ";
            $paginate_array['id'] = strtoupper($this->id);
        }

        if ($this->description != '') {
            $sql.=" AND description='" . $this->description . "' ";
            $paginate_array['description'] = $this->description;
        }
        
        /*if ($this->active_switch != '') {
            $sql.=" AND active_switch='" . strtoupper($this->active_switch) . "' ";
            $paginate_array['active_switch'] = strtoupper($this->active_switch);
        }*/
        
        if ($this->last_updated_by != '') {
            $sql.=" AND last_updated_by='" . strtoupper($this->last_updated_by) . "' ";
            $paginate_array['last_updated_by'] = $this->last_updated_by;
        }

        if ($this->last_update != '') {
            $sql.=" AND DATE(last_update)='" . date('Y-m-d', strtotime($this->last_update)) . "' ";
            $paginate_array['last_update'] = $this->last_update;
        }


        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            
        } else {

            Session::flash('alert-danger', " No POS Types found that match Query value(s)  ");
            return Redirect::route('slcadm00-types-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;


//$pagination = Paginator::make($results, count($results), 1);
//echo $pagination->appends($paginate_array)->links(); die;

        return View::make('slcadm00.typebrowser')->with('data', $work_data);
    }

    Public function getTypesAdd() { 

        //print_r(Input::all());exit;

        $work_data = new StdClass();

        $id = $description = $active_switch = $last_updated_by = '';
        $last_update = date('Y-m-d H:i:s');
        $last_updated_by = 'SLCADM00';

        if (Input::has('accept-submit')) {

            $validator = Validator::make(Input::all(), array(
                        'id' => 'required|unique:slpostyp,id',
            ));




            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-types-add')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                $id = strtoupper(Input::get('id'));
                $description = Input::get('description');
                $active_switch = Input::get('active_switch');
                $last_updated_by = Input::get('last_updated_by');
                $last_update = Input::get('last_update');


                DB::table('slpostyp')
                        ->insert(
                                ['last_updated_by' => 'SLCADM00', 'id' => $id, 'description' => $description, 'active_switch' => $active_switch, 'last_update' => date('Y-m-d H:i:s')]
                );




                Session::flash('alert-success', 'POS Type added');
                return Redirect::route('slcadm00-types-query');
            }
        }


        $work_data->id = $id;
        $work_data->description = $description;
        $work_data->active_switch = $active_switch;
        $work_data->last_update = $last_update;
        $work_data->last_updated_by = $last_updated_by;

        return View::make('slcadm00.typesadd')->with('data', $work_data);
    }

    Public function getTypesUpdate() {

            //print_r(Input::all());
        $row_id = Input::get('id');

        if ($row_id == '') {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-types-query');
        }

        $work_data = new StdClass();

        $std_curs = DB::select(DB::raw("SELECT * FROM slpostyp WHERE id = '" . strtoupper($row_id) . "' FOR UPDATE"));

        if (empty($std_curs)) {
            Session::flash('alert-danger', " No POS Types found that match Query value(s)  ");
            return Redirect::route('slcadm00-types-query')->withInput();
        } else {
            $row_data = $std_curs[0];
            $work_data->id = $row_data->id;
            $work_data->description = $row_data->description;
            $work_data->active_switch = $row_data->active_switch;
            $work_data->last_updated_by = $row_data->last_updated_by;
            $work_data->last_update = $row_data->last_update;
        }


        if (Input::has('accept-submit')) {

            $id = Input::get('id');
            $description = Input::get('description');
            $active_switch = Input::get('active_switch');
            $last_updated_by = Input::get('last_updated_by');
            $last_update = Input::get('last_update');




            DB::table('slpostyp')
                    ->where('id', $id)
                    ->update(
                            ['description' => $description, 'active_switch' => $active_switch, 'last_update' => date('Y-m-d H:i:s')]
            );




            Session::flash('alert-success', 'This POS Type has been changed');
            return Redirect::route('slcadm00-types-query');
        }


        return View::make('slcadm00.typesupdate')->with('data', $work_data);
    }

    Public function getMasterQuery() {

        $work_data = new StdClass();

        return View::make('slcadm00.masterquery')->with('data', $work_data);
    }

    public function getMasterBrowser() {


        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {




        $this->field_no = trim(Input::get('field_no'));
        $this->description = trim(Input::get('description'));
        $this->type_cd = trim(Input::get('type_cd'));
        $this->last_updated_by = trim(Input::get('last_updated_by'));
        $this->last_update = trim(Input::get('last_update'));

        $paginate_array = array();

        $sql = "SELECT * FROM slmstfld WHERE 1=1 ";

        if ($this->field_no != '') {
            $sql.=" AND field_no='" . $this->field_no . "' ";
            $paginate_array['field_no'] = $this->field_no;
        }

        if ($this->description != '') {
            $sql.=" AND description='" . $this->description . "' ";
            $paginate_array['description'] = $this->description;
        }

        if ($this->type_cd != '') {
            $sql.=" AND type_cd='" . strtoupper($this->type_cd) . "' ";
            $paginate_array['type_cd'] = $this->type_cd;
        }

        if ($this->last_updated_by != '') {
            $sql.=" AND last_updated_by='" . strtoupper($this->last_updated_by) . "' ";
            $paginate_array['last_updated_by'] = $this->last_updated_by;
        }

        if ($this->last_update != '') {
            $sql.=" AND DATE(last_update)='" . date('Y-m-d', strtotime($this->last_update)) . "' ";
            $paginate_array['last_update'] = $this->last_update;
        }



        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Master Fields found that match Query value(s)  ");
            return Redirect::route('slcadm00-master-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);
        return View::make('slcadm00.masterbrowser')->with(array('data' => $work_data, 'pagination' => $pagination));
    }

    Public function getMasterAdd() {


        $work_data = new StdClass();

        $field_no = $description = $type_cd = $last_updated_by = '';
        $last_update = date('Y-m-d H:i:s');
        $last_updated_by = 'SLCADM00';

        if (Input::has('accept-submit')) {

            $validator = Validator::make(Input::all(), array(
                        'field_no' => 'required|unique:slmstfld,field_no',
                        'type_cd' => 'required'
            ),array('type_cd.required' => 'Field type code is required'));




            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-master-add')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                $field_no = strtoupper(Input::get('field_no'));
                $description = Input::get('description');
                $type_cd = strtoupper(Input::get('type_cd'));
                $last_updated_by = Input::get('last_updated_by');
                $last_update = Input::get('last_update');


                DB::table('slmstfld')
                        ->insert(
                                ['last_updated_by' => 'SLCADM00', 'field_no' => $field_no, 'description' => $description, 'type_cd' => $type_cd, 'last_update' => date('Y-m-d H:i:s')]
                );




                Session::flash('alert-success', 'Master Field added');
                return Redirect::route('slcadm00-master-query');
            }
        }


        $work_data->field_no = $field_no;
        $work_data->description = $description;
        $work_data->type_cd = $type_cd;
        $work_data->last_update = $last_update;
        $work_data->last_updated_by = $last_updated_by;

        return View::make('slcadm00.masteradd')->with('data', $work_data);
    }

    Public function getMasterUpdate() {


        $field_no = Input::get('field_no');

        if ($field_no == '') {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-master-query');
        }

        $work_data = new StdClass();

        $std_curs = DB::select(DB::raw("SELECT * FROM slmstfld WHERE field_no = '" . $field_no . "' FOR UPDATE"));

        if (empty($std_curs)) {
            Session::flash('alert-danger', " No Master Fields found that match Query value(s)  ");
            return Redirect::route('slcadm00-master-query')->withInput();
        } else {
            $row_data = $std_curs[0];
            $work_data->field_no = $row_data->field_no;
            $work_data->description = $row_data->description;
            $work_data->type_cd = $row_data->type_cd;
            $work_data->last_updated_by = $row_data->last_updated_by;
            $work_data->last_update = $row_data->last_update;
        }


        if (Input::has('accept-submit')) {

            $field_no = Input::get('field_no');
            $description = Input::get('description');
            $type_cd = Input::get('type_cd');
            $last_updated_by = Input::get('last_updated_by');
            $last_update = Input::get('last_update');




            DB::table('slmstfld')
                    ->where('field_no', $field_no)
                    ->update(
                            ['description' => $description, 'type_cd' => $type_cd, 'last_update' => date('Y-m-d H:i:s')]
            );




            Session::flash('alert-success', 'This Master Field has been changed');
            return Redirect::route('slcadm00-master-query');
        }

        return View::make('slcadm00.masterupdate')->with('data', $work_data);
    }

    Public function getPosQuery() {
        $work_data = new StdClass();

        $post_types = DB::select(DB::raw("SELECT * FROM slpostyp WHERE slpostyp.active_switch = 'Y'"));

        $mstfld_cur = DB::select(DB::raw("SELECT * FROM slmstfld"));



        $work_data->post_types = $post_types;
        $work_data->mstfld_cur = $mstfld_cur;


        return View::make('slcadm00.posquery')->with('data', $work_data);
    }

    public function getPosBrowser() {

        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {




        $this->pos_type = strtoupper(trim(Input::get('pos_type')));
        $this->mstr_field_no = trim(Input::get('mstr_field_no'));
        $this->pos_field_no1 = trim(Input::get('pos_field_no1'));
        $this->desc_displayed = trim(Input::get('desc_displayed'));
        $this->disp_desc_sw = strtoupper(trim(Input::get('disp_desc_sw')));
        $this->field_type = strtoupper(trim(Input::get('field_type')));
        $this->field_length = trim(Input::get('field_length'));
        $this->upd_via_fm_sw = strtoupper(trim(Input::get('upd_via_fm_sw')));
        $this->min_value = trim(Input::get('min_value'));
        $this->max_value = trim(Input::get('max_value'));
        $this->default_value = trim(Input::get('default_value'));
        $this->last_updated_by = trim(Input::get('last_updated_by'));
        $this->last_update = trim(Input::get('last_update'));

        $paginate_array = array();

        $sql = "SELECT * FROM slposfld WHERE 1=1 ";

        if ($this->pos_type != '') {
            $sql.=" AND pos_type='" . $this->pos_type . "' ";
            $paginate_array['pos_type'] = $this->pos_type;
        }

        if ($this->mstr_field_no != '') {
            $sql.=" AND mstr_field_no='" . $this->mstr_field_no . "' ";
            $paginate_array['mstr_field_no'] = $this->mstr_field_no;
        }

        if ($this->pos_field_no1 != '') {
            $sql.=" AND pos_field_no1='" . $this->pos_field_no1 . "' ";
            $paginate_array['pos_field_no1'] = $this->pos_field_no1;
        }


        if ($this->desc_displayed != '') {
            $sql.=" AND desc_displayed='" . $this->desc_displayed . "' ";
            $paginate_array['desc_displayed'] = $this->desc_displayed;
        }


        if ($this->disp_desc_sw != '') {
            $sql.=" AND disp_desc_sw='" . $this->disp_desc_sw . "' ";
            $paginate_array['disp_desc_sw'] = $this->disp_desc_sw;
        }

        if ($this->field_type != '') {
            $sql.=" AND field_type='" . $this->field_type . "' ";
            $paginate_array['field_type'] = $this->field_type;
        }
        if ($this->field_length != '') {
            $sql.=" AND field_length='" . $this->field_length . "' ";
            $paginate_array['field_length'] = $this->field_length;
        }
        if ($this->upd_via_fm_sw != '') {
            $sql.=" AND upd_via_fm_sw='" . $this->upd_via_fm_sw . "' ";
            $paginate_array['upd_via_fm_sw'] = $this->upd_via_fm_sw;
        }
        if ($this->min_value != '') {
            $sql.=" AND min_value='" . $this->min_value . "' ";
            $paginate_array['min_value'] = $this->min_value;
        }

        if ($this->max_value != '') {
            $sql.=" AND max_value='" . $this->max_value . "' ";
            $paginate_array['max_value'] = $this->max_value;
        }
        if ($this->default_value != '') {
            $sql.=" AND default_value='" . $this->default_value . "' ";
            $paginate_array['default_value'] = $this->default_value;
        }

        if ($this->last_updated_by != '') {
            $sql.=" AND last_updated_by='" . strtoupper($this->last_updated_by) . "' ";
            $paginate_array['last_updated_by'] = $this->last_updated_by;
        }

        if ($this->last_update != '') {
            $sql.=" AND DATE(last_update)='" . date('Y-m-d', strtotime($this->last_update)) . "' ";
            $paginate_array['last_update'] = $this->last_update;
        }



        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No POS Fields meet Query criteria  ");
            return Redirect::route('slcadm00-pos-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('slcadm00.posbrowser')->with(array('data' => $work_data, 'pagination' => $pagination));
    }

    Public function getPosAdd() {

        $work_data = new StdClass();
        $mstr_field_no=Input::get('mstr_field_no');

        $pos_type = $mstr_field_no = $pos_field_no1 = $pos_field_no2 = $desc_displayed = $disp_desc_sw = $field_type = $field_length = $upd_via_fm_sw = $min_value = $max_value = $default_value = '';
        $last_update = date('Y-m-d H:i:s');
        $last_updated_by = 'SLCADM00';

        if (Input::has('accept-submit')) {


             

            Validator::extend('checkposexists', function($attribute, $mstr_field_no, $parameters) {


                
                $pos_type = Input::get('pos_type');
                // echo "SELECT * FROM slposfld WHERE mstr_field_no = '" . $value . "' and pos_type='" . $pos_type . "'";exit;
                $std_curs = DB::select(DB::raw("SELECT * FROM slposfld WHERE mstr_field_no = '" .$mstr_field_no. "' and pos_type='" . $pos_type . "'"));
               // echo $value;exit;


                if (!empty($std_curs)) {
                    return false;
                }

                else{
                return true;
            }
            }, 'Already exist with same pos type.');


            Validator::extend('checkmingreater', function($attribute, $mstr_field_no, $parameters) {


                $min_value=Input::get('min_value');
                $field_length = Input::get('field_length');

                if (strlen($min_value) > intval($field_length)) {
                    return false;
                   // echo $min_value;exit;
                } 
                else{
                    return true;
                }
                

                //return false;
            }, 'Length of Value for Minimum Value out of range');

            Validator::extend('checkmaxgreater', function($attribute, $mstr_field_no, $parameters) {

                $field_length = Input::get('field_length');
                $max_value=Input::get('max_value');
                //echo 'max_value '.$max_value.' '.$field_length;exit;       

                if (strlen($max_value) > intval($field_length)) {
                    return false;
                }
                else{
                return true;
            }
            }, 'Length of Value for Maximum Value out of range');

            Validator::extend('checkmaxgreater2', function($attribute, $mstr_field_no, $parameters) {


                $min_value = Input::get('min_value');



                if (intval($mstr_field_no) == intval($min_value) || intval($mstr_field_no) > intval($min_value)) {
                    return true;
                } else {
                    return false;
                }
            }, 'Max Value must be Greater or Equal to Min Value');

            $validator = Validator::make(Input::all(), array(
                        'pos_type' => 'required',
                        'mstr_field_no' => 'required|checkposexists',
                        'pos_field_no1' => 'required|integer|min:1',
                        'desc_displayed' => 'required',
                        'disp_desc_sw' => 'required',
                        'field_type' => 'required',
                        'field_length' => 'required|integer|min:1',
                        'upd_via_fm_sw' => 'required',
                        'min_value' => 'required|checkmingreater',
                        'max_value' => 'required|checkmaxgreater|checkmaxgreater2'
            ));



            if ($validator->fails()) {

                return Redirect::route('slcadm00-pos-add')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                $pos_type = Input::get('pos_type');
                $mstr_field_no = Input::get('mstr_field_no');
                $pos_field_no1 = Input::get('pos_field_no1');
                $pos_field_no2 = Input::get('pos_field_no2');
                $desc_displayed = Input::get('desc_displayed');
                $disp_desc_sw = Input::get('disp_desc_sw');
                $field_type = Input::get('field_type');
                $field_length = Input::get('field_length');
                $upd_via_fm_sw = Input::get('upd_via_fm_sw');
                $min_value = Input::get('min_value');
                $max_value = Input::get('max_value');
                $default_value = Input::get('default_value');





                DB::table('slposfld')
                        ->insert(
                                ['last_updated_by' => 'SLCADM00', 'pos_type' => $pos_type, 'mstr_field_no' => $mstr_field_no, 'pos_field_no1' => $pos_field_no1, 'desc_displayed' => $desc_displayed, 'disp_desc_sw' => $disp_desc_sw, 'field_type' => $field_type, 'field_length' => $field_length, 'upd_via_fm_sw' => $upd_via_fm_sw, 'min_value' => $min_value, 'max_value' => $max_value, 'default_value' => $default_value, 'last_update' => date('Y-m-d H:i:s')]
                );




                Session::flash('alert-success', 'POS Field added');
                return Redirect::route('slcadm00-pos-query');
            }
        }


        $work_data->pos_type = $pos_type;
        $work_data->mstr_field_no = $mstr_field_no;
        $work_data->pos_field_no1 = $pos_field_no1;
        $work_data->pos_field_no2 = $pos_field_no2;
        $work_data->desc_displayed = $desc_displayed;
        $work_data->disp_desc_sw = $disp_desc_sw;
        $work_data->field_type = $field_type;
        $work_data->field_length = $field_length;
        $work_data->upd_via_fm_sw = $upd_via_fm_sw;
        $work_data->min_value = $min_value;
        $work_data->max_value = $max_value;
        $work_data->default_value = $default_value;


        $work_data->last_updated_by = $last_updated_by;
        $work_data->last_update = $last_update;

        $post_types = DB::select(DB::raw("SELECT * FROM slpostyp WHERE slpostyp.active_switch = 'Y'"));

        $mstfld_cur = DB::select(DB::raw("SELECT * FROM slmstfld"));



        $work_data->post_types = $post_types;
        $work_data->mstfld_cur = $mstfld_cur;

        return View::make('slcadm00.posadd')->with('data', $work_data);
    }

    Public function getPosUpdate() {

        $work_data = new StdClass();
        $mstr = Input::get('mstr');

        if ($mstr == '') {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-pos-query');
        }

        $work_data->mstr = $mstr;

        $std_curs = DB::select(DB::raw("SELECT * FROM slposfld WHERE mstr_field_no = '" . $mstr . "' FOR UPDATE"));



        if (empty($std_curs)) {
            Session::flash('alert-danger', " No POS Fields meet Query criteria  ");
            return Redirect::route('slcadm00-pos-query')->withInput();
        } else {
            $row_data = $std_curs[0];

            $work_data->pos_type = $row_data->pos_type;
            $work_data->mstr_field_no = $row_data->mstr_field_no;
            $work_data->pos_field_no1 = $row_data->pos_field_no1;
            $work_data->pos_field_no2 = $row_data->pos_field_no2;
            $work_data->desc_displayed = $row_data->desc_displayed;
            $work_data->disp_desc_sw = $row_data->disp_desc_sw;
            $work_data->field_type = $row_data->field_type;
            $work_data->field_length = $row_data->field_length;
            $work_data->upd_via_fm_sw = $row_data->upd_via_fm_sw;
            $work_data->min_value = $row_data->min_value;
            $work_data->max_value = $row_data->max_value;
            $work_data->default_value = $row_data->default_value;


            $work_data->last_updated_by = $row_data->last_updated_by;
            $work_data->last_update = $row_data->last_update;
        }


        if (Input::has('accept-submit')) {

            


            Validator::extend('checkmingreater', function($attribute, $mstr_field_no, $parameters) {


                $min_value=Input::get('min_value');
                $field_length = Input::get('field_length');

                if (strlen($min_value) > intval($field_length)) {
                    return false;
                   // echo $min_value;exit;
                } 
                else{
                    return true;
                }
                

                //return false;
            }, 'Length of Value for Minimum Value out of range');

            Validator::extend('checkmaxgreater', function($attribute, $mstr_field_no, $parameters) {

                $field_length = Input::get('field_length');
                $max_value=Input::get('max_value');
                //echo 'max_value '.$max_value.' '.$field_length;exit;       

                if (strlen($max_value) > intval($field_length)) {
                    return false;
                }
                else{
                return true;
            }
            }, 'Length of Value for Maximum Value out of range');

            Validator::extend('checkmaxgreater2', function($attribute, $mstr_field_no, $parameters) {


                $min_value = Input::get('min_value');



                if (intval($mstr_field_no) == intval($min_value) || intval($mstr_field_no) > intval($min_value)) {
                    return true;
                } else {
                    return false;
                }
            }, 'Max Value must be Greater or Equal to Min Value');

            $validator = Validator::make(Input::all(), array(
                        'mstr_field_no' => 'required',
                        'pos_field_no1' => 'required|integer|min:1',
                        'desc_displayed' => 'required',
                        'disp_desc_sw' => 'required',
                        'field_type' => 'required',
                        'field_length' => 'required|integer|min:1',
                        'upd_via_fm_sw' => 'required',
                        'min_value' => 'required|checkmingreater',
                        'max_value' => 'required|checkmaxgreater|checkmaxgreater2'
            ));










            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-pos-update', array('mstr' => $mstr))
                                ->withErrors($validator)
                                ->withInput();
            } else {

                $pos_type = Input::get('pos_type');
                $mstr_field_no = Input::get('mstr_field_no');
                $pos_field_no1 = Input::get('pos_field_no1');
                //$pos_field_no2 = Input::get('pos_field_no2');
                $desc_displayed = Input::get('desc_displayed');
                $disp_desc_sw = Input::get('disp_desc_sw');
                $field_type = Input::get('field_type');
                $field_length = Input::get('field_length');
                $upd_via_fm_sw = Input::get('upd_via_fm_sw');
                $min_value = Input::get('min_value');
                $max_value = Input::get('max_value');
                $default_value = Input::get('default_value');

                $last_updated_by = Input::get('last_updated_by');
                $last_update = Input::get('last_update');




                DB::table('slposfld')
                        ->where('mstr_field_no', $mstr)
                        ->update(
                                ['pos_field_no1' => $pos_field_no1, 'desc_displayed' => $desc_displayed, 'disp_desc_sw' => $disp_desc_sw, 'field_type' => $field_type, 'field_length' => $field_length, 'upd_via_fm_sw' => $upd_via_fm_sw, 'min_value' => $min_value, 'max_value' => $max_value, 'default_value' => $default_value, 'last_update' => date('Y-m-d H:i:s')]
                );






                Session::flash('alert-success', 'POS Field characteristics have been changed');
                return Redirect::route('slcadm00-pos-query');
            }
        }




        $post_types = DB::select(DB::raw("SELECT * FROM slpostyp WHERE slpostyp.active_switch = 'Y'"));

        $mstfld_cur = DB::select(DB::raw("SELECT * FROM slmstfld"));



        $work_data->post_types = $post_types;
        $work_data->mstfld_cur = $mstfld_cur;


        return View::make('slcadm00.posupdate')->with('data', $work_data);
    }

    Public function getComboQuery() {

        $work_data = new StdClass();



        $mstfld_cur = DB::select(DB::raw("SELECT * FROM slmstfld"));


        $work_data->mstfld_cur = $mstfld_cur;

        return View::make('slcadm00.comboquery')->with('data', $work_data);
    }

    public function getComboBrowser() {

        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {





        $this->mstr_field_no = trim(Input::get('mstr_field_no'));
        $this->seq_no = trim(Input::get('seq_no'));
        $this->when_value = trim(Input::get('when_value'));
        $this->other_fld_1 = strtoupper(trim(Input::get('other_fld_1')));
        $this->other_fld_2 = strtoupper(trim(Input::get('other_fld_2')));

        $this->last_updated_by = trim(Input::get('last_updated_by'));
        $this->last_update = trim(Input::get('last_update'));

        $paginate_array = array();

        $sql = "SELECT * FROM slcombos WHERE 1=1 ";

        if ($this->mstr_field_no != '') {
            $sql.=" AND mstr_field_no='" . $this->mstr_field_no . "' ";
            $paginate_array['mstr_field_no'] = $this->mstr_field_no;
        }

        if ($this->seq_no != '') {
            $sql.=" AND seq_no='" . $this->seq_no . "' ";
            $paginate_array['seq_no'] = $this->seq_no;
        }


        if ($this->when_value != '') {
            $sql.=" AND when_value='" . $this->when_value . "' ";
            $paginate_array['when_value'] = $this->when_value;
        }


        if ($this->other_fld_1 != '') {
            $sql.=" AND other_fld_1='" . $this->other_fld_1 . "' ";
            $paginate_array['other_fld_1'] = $this->other_fld_1;
        }

        if ($this->other_fld_2 != '') {
            $sql.=" AND other_fld_2='" . $this->other_fld_2 . "' ";
            $paginate_array['other_fld_2'] = $this->other_fld_2;
        }


        if ($this->last_updated_by != '') {
            $sql.=" AND last_updated_by='" . strtoupper($this->last_updated_by) . "' ";
            $paginate_array['last_updated_by'] = $this->last_updated_by;
        }

        if ($this->last_update != '') {
            $sql.=" AND DATE(last_update)='" . date('Y-m-d', strtotime($this->last_update)) . "' ";
            $paginate_array['last_update'] = $this->last_update;
        }



        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Combo Fields meet Query criteria ");
            return Redirect::route('slcadm00-combo-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('slcadm00.combobrowser')->with(array('data' => $work_data, 'pagination' => $pagination));
    }

    Public function getComboAdd() {

        $work_data = new StdClass();


        $mstr_field_no = $seq_no = $when_value = $other_fld_1 = $other_fld_2 = '';
        $last_update = date('Y-m-d H:i:s');
        $last_updated_by = 'SLCADM00';

        if (Input::has('accept-submit')) {


            Validator::extend('checkcomboexists', function($attribute, $value, $parameters) {



                $when_value = Input::get('when_value');
                $other_fld_1 = Input::get('other_fld_1');
                $std_curs = DB::select(DB::raw("SELECT * FROM slcombos WHERE mstr_field_no = '" . $value . "' and when_value='" . $when_value . "' and other_fld_1='" . $other_fld_1 . "'"));


                if (!empty($std_curs)) {
                    return false;
                }


                return true;
            }, 'Already exist with same combo.');


            $validator = Validator::make(Input::all(), array(
                        'mstr_field_no' => 'required|checkcomboexists',
                        'seq_no' => 'integer|max:20'
            ));

            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-combo-add')
                                ->withErrors($validator)
                                ->withInput();
            } else {

                $mstr_field_no = Input::get('mstr_field_no');
                $seq_no = Input::get('seq_no');
                $when_value = Input::get('when_value');
                $other_fld_1 = Input::get('other_fld_1');
                $other_fld_2 = Input::get('other_fld_2');





                DB::table('slcombos')
                        ->insert(
                                ['last_updated_by' => 'SLCADM00', 'mstr_field_no' => $mstr_field_no, 'seq_no' => $seq_no, 'when_value' => $when_value, 'other_fld_1' => $other_fld_1, 'other_fld_2' => $other_fld_2, 'last_update' => date('Y-m-d H:i:s')]
                );




                Session::flash('alert-success', 'Combo Field added');
                return Redirect::route('slcadm00-combo-query');
            }
        }


        $work_data->mstr_field_no = $mstr_field_no;
        $work_data->seq_no = $seq_no;
        $work_data->when_value = $when_value;
        $work_data->other_fld_1 = $other_fld_1;
        $work_data->other_fld_2 = $other_fld_2;



        $work_data->last_updated_by = $last_updated_by;
        $work_data->last_update = $last_update;

        $mstfld_cur = DB::select(DB::raw("SELECT * FROM slmstfld"));
        $work_data->mstfld_cur = $mstfld_cur;

        return View::make('slcadm00.comboadd')->with('data', $work_data);
    }

    Public function getComboUpdate() {
        $work_data = new StdClass();


        $mstr = Input::get('mstr');
        $whv = Input::get('whv');
        $otherfld1 = Input::get('otherfld1');



        if ($mstr == '' || $whv == '' || $otherfld1 == '') {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-combo-query');
        }

        $work_data->mstr = $mstr;
        $work_data->whv = $whv;
        $work_data->otherfld1 = $otherfld1;

        $std_curs = DB::select(DB::raw("SELECT * FROM slcombos WHERE mstr_field_no = '" . $mstr . "' and when_value='" . $whv . "' and other_fld_1='" . $otherfld1 . "' FOR UPDATE"));



        if (empty($std_curs)) {
            Session::flash('alert-danger', " No Combo Fields meet Query criteria  ");
            return Redirect::route('slcadm00-combo-query')->withInput();
        } else {
            $row_data = $std_curs[0];


            $work_data->mstr_field_no = $row_data->mstr_field_no;
            $work_data->seq_no = $row_data->seq_no;
            $work_data->when_value = $row_data->when_value;
            $work_data->other_fld_1 = $row_data->other_fld_1;
            $work_data->other_fld_2 = $row_data->other_fld_2;



            $work_data->last_updated_by = $row_data->last_updated_by;
            $work_data->last_update = $row_data->last_update;
        }


        if (Input::has('accept-submit')) {



            $validator = Validator::make(Input::all(), array(
                        'mstr_field_no' => 'required',
                        'seq_no' => 'integer|max:20'
            ));




            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-combo-update', array('mstr' => $mstr, 'whv' => $whv, 'otherfld1' => $otherfld1))
                                ->withErrors($validator)
                                ->withInput();
            } else {


                $mstr_field_no = Input::get('mstr_field_no');

                $seq_no = Input::get('seq_no');
                $when_value = Input::get('when_value');
                $other_fld_1 = Input::get('other_fld_1');
                $other_fld_2 = Input::get('other_fld_2');

                $last_updated_by = Input::get('last_updated_by');
                $last_update = Input::get('last_update');




                DB::table('slcombos')
                        ->where('mstr_field_no', $mstr)
                        ->where('when_value', $whv)
                        ->where('other_fld_1', $otherfld1)
                        ->update(
                                ['seq_no' => $seq_no, 'when_value' => $when_value, 'other_fld_1' => $other_fld_1, 'other_fld_2' => $other_fld_2, 'last_update' => date('Y-m-d H:i:s')]
                );






                Session::flash('alert-success', 'Combo Field characteristics have been changed');
                return Redirect::route('slcadm00-combo-query');
            }
        }




        $mstfld_cur = DB::select(DB::raw("SELECT * FROM slmstfld"));
        $work_data->mstfld_cur = $mstfld_cur;

        return View::make('slcadm00.comboupdate')->with('data', $work_data);
    }

    Public function getUserQuery() {

        $work_data = new StdClass();


        $post_types = DB::select(DB::raw("SELECT * FROM slpostyp WHERE slpostyp.active_switch = 'Y'"));

        $work_data->post_types = $post_types;
        return View::make('slcadm00.userquery')->with('data', $work_data);
    }

    public function getUserBrowser() {

        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {

        $this->seq_no = '';

        $this->userid = trim(Input::get('userid'));
        $this->passwd = trim(Input::get('passwd'));
        $this->name = trim(Input::get('name'));
        $this->pos_type = trim(Input::get('pos_type'));

        $this->batch_cd = trim(Input::get('batch_cd'));
        $this->add_cd = trim(Input::get('add_cd'));
        $this->delete_cd = trim(Input::get('delete_cd'));
        $this->update_cd = trim(Input::get('update_cd'));


        $this->last_updated_by = trim(Input::get('last_updated_by'));
        $this->last_update = trim(Input::get('last_update'));

        $paginate_array = array();

        $sql = "SELECT * FROM sluser WHERE 1=1 ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->seq_no != '') {
            $sql.=" AND seq_no='" . $this->seq_no . "' ";
            $paginate_array['seq_no'] = $this->seq_no;
        }


        if ($this->name != '') {
            $sql.=" AND name='" . $this->name . "' ";
            $paginate_array['name'] = $this->name;
        }


        if ($this->pos_type != '') {
            $sql.=" AND pos_type='" . $this->pos_type . "' ";
            $paginate_array['pos_type'] = $this->pos_type;
        }

        if ($this->batch_cd != '') {
            $sql.=" AND batch_cd='" . $this->batch_cd . "' ";
            $paginate_array['batch_cd'] = $this->batch_cd;
        }

        if ($this->add_cd != '') {
            $sql.=" AND add_cd='" . $this->add_cd . "' ";
            $paginate_array['add_cd'] = $this->add_cd;
        }
        if ($this->update_cd != '') {
            $sql.=" AND update_cd='" . $this->update_cd . "' ";
            $paginate_array['update_cd'] = $this->update_cd;
        }
        if ($this->delete_cd != '') {
            $sql.=" AND delete_cd='" . $this->delete_cd . "' ";
            $paginate_array['delete_cd'] = $this->delete_cd;
        }


        if ($this->last_updated_by != '') {
            $sql.=" AND last_updated_by='" . strtoupper($this->last_updated_by) . "' ";
            $paginate_array['last_updated_by'] = $this->last_updated_by;
        }

        if ($this->last_update != '') {
            $sql.=" AND DATE(last_update)='" . date('Y-m-d', strtotime($this->last_update)) . "' ";
            $paginate_array['last_update'] = $this->last_update;
        }



        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Users meet Query criteria ");
            return Redirect::route('slcadm00-user-query')->withInput();
        }





        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);




        return View::make('slcadm00.userbrowser')->with(array('data' => $work_data, 'pagination' => $pagination));
    }

    Public function getUserAdd() {


        $work_data = new StdClass();


        $seq_no = $userid = $passwd = $name = $pos_type = $batch_cd = $add_cd = $delete_cd = $update_cd = '';
        $last_update = date('Y-m-d H:i:s');
        $last_updated_by = 'SLCADM00';

        if (Input::has('accept-submit')) {


            Validator::extend('checkuserexists', function($attribute, $value, $parameters) {




                $std_curs = DB::select(DB::raw("SELECT * FROM sluser WHERE userid = '" . $value . "'"));


                if (!empty($std_curs)) {
                    return false;
                }


                return true;
            }, 'User Id already exists');


            $validator = Validator::make(Input::all(), array(
                        'userid' => 'required|integer|checkuserexists',
                        'passwd' => 'required',
                        'name' => 'required',
                        'pos_type' => 'required'
            ));

            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-user-add')
                                ->withErrors($validator)
                                ->withInput();
            } else {



                $userid = Input::get('userid');
                $passwd = Input::get('passwd');
                $name = Input::get('name');
                $pos_type = Input::get('pos_type');
                $batch_cd = Input::get('batch_cd');
                $add_cd = Input::get('add_cd');
                $delete_cd = Input::get('delete_cd');
                $update_cd = Input::get('update_cd');




                DB::table('sluser')
                        ->insert(
                                ['last_updated_by' => 'SLCADM00', 'userid' => $userid, 'passwd' => $passwd, 'name' => $name, 'pos_type' => $pos_type, 'batch_cd' => $batch_cd, 'add_cd' => $add_cd, 'delete_cd' => $delete_cd, 'update_cd' => $update_cd, 'last_update' => date('Y-m-d H:i:s')]
                );




                Session::flash('alert-success', 'User added');
                return Redirect::route('slcadm00-user-query');
            }
        }


        $work_data->seq_no = $seq_no;

        $work_data->userid = $userid;

        $work_data->passwd = $passwd;
        $work_data->name = $name;
        $work_data->pos_type = $pos_type;

        $work_data->batch_cd = $batch_cd;
        $work_data->add_cd = $add_cd;
        $work_data->delete_cd = $delete_cd;
        $work_data->update_cd = $update_cd;



        $work_data->last_updated_by = $last_updated_by;
        $work_data->last_update = $last_update;



        $post_types = DB::select(DB::raw("SELECT * FROM slpostyp WHERE slpostyp.active_switch = 'Y'"));

        $work_data->post_types = $post_types;

        $top_dtl = "";
        $add_dtl = "";
        $upd_dtl = "";
        $disp_dtl = "";


        $work_data->top_dtl = $top_dtl;
        $work_data->add_dtl = $add_dtl;
        $work_data->upd_dtl = $upd_dtl;
        $work_data->disp_dtl = $disp_dtl;

        return View::make('slcadm00.useradd')->with('data', $work_data);
    }

    Public function getUserUpdate() {

        $work_data = new StdClass();


        $usrid = Input::get('userid');
        $seqn = Input::get('seq_no');


        if ($usrid == '' || $usrid <= 0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-user-query');
        }



        $work_data->usrid = $usrid;
        $work_data->seqn = $seqn;


        $std_curs = DB::select(DB::raw("SELECT * FROM sluser WHERE  seq_no='" . $seqn . "' FOR UPDATE"));



        if (empty($std_curs)) {
            Session::flash('alert-danger', " No Users meet Query criteria ");
            return Redirect::route('slcadm00-user-query')->withInput();
        } else {
            $row_data = $std_curs[0];


            $work_data->seq_no = $row_data->seq_no;

            $work_data->userid = $row_data->userid;

            $work_data->passwd = $row_data->passwd;
            $work_data->name = $row_data->name;
            $work_data->pos_type = $row_data->pos_type;

            $work_data->batch_cd = $row_data->batch_cd;
            $work_data->add_cd = $row_data->add_cd;
            $work_data->delete_cd = $row_data->delete_cd;
            $work_data->update_cd = $row_data->update_cd;



            $work_data->last_updated_by = $row_data->last_updated_by;
            $work_data->last_update = $row_data->last_update;




            $top_dtl = "";
            $add_dtl = "";
            $upd_dtl = "";
            $disp_dtl = "";





            $ws_fldtype = "D";

            $sql = "SELECT mstr_field_no FROM slusrfld WHERE userid = '" . $work_data->userid . "' AND type = '" . $ws_fldtype . "'";
            $fields_curs = DB::select(DB::raw($sql));

            if (!empty($fields_curs)) {
                foreach ($fields_curs as $frow) {
                    $disp_dtl .= $frow->mstr_field_no . ' ';
                }
            }



            $ws_fldtype = "T";
            $sql = "SELECT mstr_field_no FROM slusrfld WHERE userid = '" . $work_data->userid . "' AND type = '" . $ws_fldtype . "'";
            $fields_curs = DB::select(DB::raw($sql));

            if (!empty($fields_curs)) {
                foreach ($fields_curs as $frow) {
                    $top_dtl .= $frow->mstr_field_no . ' ';
                }
            }


            if ($work_data->add_cd == "N") {
                $add_dtl = "Add Prohibited";
            } else {
                $ws_fldtype = "A";
                $sql = "SELECT mstr_field_no FROM slusrfld WHERE userid = '" . $work_data->userid . "' AND type = '" . $ws_fldtype . "'";
                $fields_curs = DB::select(DB::raw($sql));

                if (!empty($fields_curs)) {
                    foreach ($fields_curs as $frow) {
                        $add_dtl .= $frow->mstr_field_no . ' ';
                    }
                }
            }


            if ($work_data->update_cd == "A") {
                $upd_dtl = "Update All Fields";
            } else {
                if ($work_data->update_cd == "C") {
                    $upd_dtl = "Update Price on Clearance Items only";
                } else {
                    if ($work_data->update_cd == "N") {
                        $upd_dtl = "Update Prohibited";
                    } else {
                        if ($work_data->update_cd == "R") {
                            $upd_dtl = "Retail Inspection Only";
                        } else {
                            $ws_fldtype = "U";

                            $sql = "SELECT mstr_field_no FROM slusrfld WHERE userid = '" . $work_data->userid . "' AND type = '" . $ws_fldtype . "'";
                            $fields_curs = DB::select(DB::raw($sql));

                            if (!empty($fields_curs)) {
                                foreach ($fields_curs as $frow) {
                                    $upd_dtl .= $frow->mstr_field_no . ' ';
                                }
                            }
                        }
                    }
                }
            }



            $work_data->top_dtl = $top_dtl;
            $work_data->add_dtl = $add_dtl;
            $work_data->upd_dtl = $upd_dtl;
            $work_data->disp_dtl = $disp_dtl;


            //===

            $ws_fldtype = "D";
            $fm_Display_link = URL::route('slcadm00-user-fm-display', array('userid' => $work_data->userid, 'name' => $work_data->name, 'pos_type' => $work_data->pos_type, 'fldtype' => $ws_fldtype, 'seq_no' => $work_data->seq_no));

            $ws_fldtype = "A";
            $fm_add_link = URL::route('slcadm00-user-fm-display', array('userid' => $work_data->userid, 'name' => $work_data->name, 'pos_type' => $work_data->pos_type, 'fldtype' => $ws_fldtype, 'seq_no' => $work_data->seq_no));

            $ws_fldtype = "U";
            $fm_modify_link = URL::route('slcadm00-user-fm-display', array('userid' => $work_data->userid, 'name' => $work_data->name, 'pos_type' => $work_data->pos_type, 'fldtype' => $ws_fldtype, 'seq_no' => $work_data->seq_no));


            $ws_fldtype = "T";
            $fm_top_link = URL::route('slcadm00-user-fm-display', array('userid' => $work_data->userid, 'name' => $work_data->name, 'pos_type' => $work_data->pos_type, 'fldtype' => $ws_fldtype, 'seq_no' => $work_data->seq_no));





            $work_data->fm_Display_link = $fm_Display_link;
            $work_data->fm_add_link = $fm_add_link;
            $work_data->fm_modify_link = $fm_modify_link;
            $work_data->fm_top_link = $fm_top_link;


            //====
        }


        if (Input::has('accept-submit')) {



            $validator = Validator::make(Input::all(), array(
                        'userid' => 'required|integer',
                        'passwd' => 'required',
                        'name' => 'required',
                        'pos_type' => 'required'
            ));




            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-user-update', array('userid' => $usrid, 'seq_no' => $seqn))
                                ->withErrors($validator)
                                ->withInput();
            } else {

                $seqno  = Input::get('seq_no');
                $userid = Input::get('userid');
                $passwd = Input::get('passwd');
                $name = Input::get('name');
                $pos_type = Input::get('pos_type');
                $batch_cd = Input::get('batch_cd');
                $add_cd = Input::get('add_cd');
                $delete_cd = Input::get('delete_cd');
                $update_cd = Input::get('update_cd');

                $last_updated_by = Input::get('last_updated_by');
                $last_update = Input::get('last_update');




                DB::table('sluser')
                        ->where('seq_no', $seqno)
                        ->update(
                                ['userid' => $userid, 'passwd' => $passwd, 'name' => $name, 'pos_type' => $pos_type, 'batch_cd' => $batch_cd, 'add_cd' => $add_cd, 'delete_cd' => $delete_cd, 'update_cd' => $update_cd, 'last_update' => date('Y-m-d H:i:s')]
                );






                Session::flash('alert-success', 'User detail has been updated');
                return Redirect::route('slcadm00-user-query');
            }
        }


        $post_types = DB::select(DB::raw("SELECT * FROM slpostyp WHERE slpostyp.active_switch = 'Y'"));

        $work_data->post_types = $post_types;



        return View::make('slcadm00.userupdate')->with('data', $work_data);
    }

    Public function getUserFmDisplay() {

        $work_data = new StdClass();


        $usrid = Input::get('userid');
        $user_name = Input::get('name');
        $pos_type = Input::get('pos_type');
        $fldtype = Input::get('fldtype');
        $seqn = Input::get('seq_no');



        if ($usrid == '' || $usrid <= 0 || $pos_type == '' || $fldtype == "") {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-user-query');
        }

        $paginate_array = array();


        $sql = "SELECT slusrfld.seq_no,slusrfld.mstr_field_no,slmstfld.description FROM slusrfld left join slmstfld on slmstfld.field_no=slusrfld.mstr_field_no  WHERE slusrfld.userid = '" . $usrid . "' AND slusrfld.type = '" . $fldtype . "'";


        $paginate_array['userid'] = $usrid;
        $paginate_array['seq_no'] = $seqn;
        $paginate_array['name'] = $user_name;
        $paginate_array['fldtype'] = $fldtype;
        $paginate_array['pos_type'] = $pos_type;




        $count_results = DB::select(DB::raw($sql));



        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));




        $work_data->results = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        $work_data->usrid = $usrid;
        $work_data->name = $user_name;
        $work_data->seq_no = $seqn;
        $work_data->fldtype = $fldtype;
        $work_data->pos_type = $pos_type;

        if ($fldtype == "A") {
            $page_title = "Required for Add via FM";
        } else {
            if ($fldtype == "U") {
                $page_title = "Allowed for Update via FM";
            } else {
                if ($fldtype == "T") {
                    $page_title = "Displayed at Top of Screen";
                } else {
                    $page_title = "Displayed via FM";
                }
            }
        }

        $work_data->page_title = $page_title;




        return View::make('slcadm00.userfmdisplay')->with(array('data' => $work_data, 'pagination' => $pagination));
    }

    public function getDisplayViaFm() {


        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $user_name = Input::get('name');
        $postyp = Input::get('pos_type');
        $fldtype = Input::get('fldtype');
        $seqn = Input::get('seq_no');




        if ($usrid == '' || $usrid <= 0 || $postyp == '' || $fldtype == "") {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-user-query');
        }

        $std_curs = DB::select(DB::raw("SELECT count(*) as total FROM slusrfld WHERE slusrfld.userid = '" . $usrid . "' AND slusrfld.type = '" . $fldtype . "' "));

        $rseqn = 0;
        if (!empty($std_curs)) {
            $row = $std_curs[0];
            $rseqn = $row->total;
        }
        $rseqn = $rseqn + 1;

        $work_data->usrid = $usrid;
        $work_data->name = $user_name;
        $work_data->sqno = $seqn;
        $work_data->rsqno = $rseqn;
        $work_data->fldtype = $fldtype;
        $work_data->postyp = $postyp;


        if (Input::has('accept-submit')) {



            $validator = Validator::make(Input::all(), array(
                        'usrid' => 'required|integer'
            ));




            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-display-via-fm-add', array('userid' => $usrid, 'name' => $user_name, 'pos_type' => $postyp, 'fldtype' => $fldtype, 'seq_no' => $seqn))
                                ->withErrors($validator)
                                ->withInput();
            } else {


                $mstr_field_no = Input::get('mstr_field_no');



                DB::table('slusrfld')
                        ->insert(
                                ['seq_no' => $rseqn, 'type' => $fldtype, 'userid' => $usrid, 'last_updated_by' => 'SLCADM00', 'mstr_field_no' => $mstr_field_no, 'last_update' => date('Y-m-d H:i:s')]
                );








                Session::flash('alert-success', 'Field Added');
                return Redirect::route('slcadm00-user-fm-display', array('userid' => $usrid, 'name' => $user_name, 'pos_type' => $postyp, 'fldtype' => $fldtype, 'seq_no' => $seqn, 'rseq_no' => $rseqn));
            }
        }






        $mstfld_cur = DB::select(DB::raw("SELECT field_no,description FROM slmstfld "));
        $work_data->mstfld_cur = $mstfld_cur;



        return View::make('slcadm00.displayviafm')->with('data', $work_data);
    }

    public function getDisplayViaFmUpdate() {


        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $user_name = Input::get('name');
        $postyp = Input::get('pos_type');
        $fldtype = Input::get('fldtype');
        $seqn = Input::get('seq_no');
        $rseqn = Input::get('rseq_no');

        if ($usrid == '' || $usrid <= 0 || $postyp == '' || $fldtype == "" || $rseqn == '' || $rseqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-user-query');
        }


        $std_curs = DB::select(DB::raw("SELECT * FROM slusrfld WHERE slusrfld.userid = '" . $usrid . "' AND slusrfld.type = '" . $fldtype . "' AND slusrfld.seq_no = '" . $rseqn . "' FOR UPDATE"));




        if (empty($std_curs)) {
            Session::flash('alert-danger', " No Users meet Query criteria ");
            return Redirect::route('slcadm00-user-query');
        } else {
            $row_data = $std_curs[0];




            $work_data->userid = $row_data->userid;
            $work_data->type = $row_data->type;
            $work_data->seq_no = $row_data->seq_no;
            $work_data->mstr_field_no = $row_data->mstr_field_no;



            $work_data->usrid = $usrid;
            $work_data->name = $user_name;
            $work_data->sqno = $seqn;
            $work_data->rsqno = $rseqn;
            $work_data->fldtype = $fldtype;
            $work_data->postyp = $postyp;

            //====
        }

        if (Input::get('action') == 'delete') {

            $del_curs = DB::select(DB::raw("SELECT * FROM slusrfld WHERE slusrfld.userid = '" . $usrid . "' AND slusrfld.type = '" . $fldtype . "' AND slusrfld.seq_no = '" . $rseqn . "' FOR UPDATE"));


            $current_seqno = $rseqn;

            $next_seqno = $current_seqno;
            $done_sw = "N";

            WHILE ($done_sw == "N") {
                $next_seqno = $next_seqno + 1;

                //echo $next_seqno; echo "<br/>";

                $h_record = DB::select(DB::raw("SELECT * FROM slusrfld WHERE slusrfld.userid = '" . $usrid . "' AND slusrfld.type = '" . $fldtype . "' AND slusrfld.seq_no = '" . $next_seqno . "' FOR UPDATE"));


                if (!empty($h_record)) {


                    $h_record_row = $h_record[0];

                    // echo "====".$h_record_row->mstr_field_no; echo "<br/>";
                    // echo "========".$current_seqno; echo "<br/>";

                    DB::update("UPDATE slusrfld SET mstr_field_no ='" . $h_record_row->mstr_field_no . "' WHERE slusrfld.userid = '" . $usrid . "' AND slusrfld.type = '" . $fldtype . "' AND slusrfld.seq_no = '" . $current_seqno . "' ");


                    $current_seqno = $next_seqno;
                    //  echo "============".$current_seqno; echo "<br/>";
                } else {
                    $done_sw = "Y";
                }
            }

            //echo "==================".$current_seqno; echo "<br/>"; 
            // die;
            DB::delete(DB::raw("DELETE FROM slusrfld WHERE slusrfld.userid = '" . $usrid . "' AND slusrfld.type = '" . $fldtype . "' AND slusrfld.seq_no = '" . $current_seqno . "' "));

            Session::flash('alert-success', 'Field Deleted');
            return Redirect::route('slcadm00-user-fm-display', array('userid' => $usrid, 'name' => $user_name, 'pos_type' => $postyp, 'fldtype' => $fldtype, 'seq_no' => $seqn, 'rseq_no' => $rseqn));
        }

        if (Input::has('update-submit')) {



            $validator = Validator::make(Input::all(), array(
                        'usrid' => 'required|integer'
            ));




            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('slcadm00-display-via-fm-update', array('userid' => $usrid, 'name' => $user_name, 'pos_type' => $postyp, 'fldtype' => $fldtype, 'seq_no' => $seqn, 'rseq_no' => $rseqn))
                                ->withErrors($validator)
                                ->withInput();
            } else {

            $date=date('Y-m-d H:i:s');
                $mstr_field_no = Input::get('mstr_field_no');
                
                //echo $mstr_field_no;exit;
                DB::update(DB::raw("UPDATE slusrfld SET last_updated_by = 'SLCADM00',mstr_field_no = 
".$mstr_field_no.",last_update = date('Y-m-d H:i:s') WHERE userid=".$usrid." AND type='".$fldtype."' AND
 seq_no =".$rseqn.""));

                // DB::table('slusrfld')
                //         ->where('userid', $usrid)
                //         ->where('type', $fldtype)
                //         ->where('seq_no', $seqn)
                //         ->update(
                //                 ['last_updated_by' => 'SLCADM00', 'mstr_field_no' => $mstr_field_no, 'last_update' => date('Y-m-d H:i:s')]
                // );








                Session::flash('alert-success', 'Field Updated');
                return Redirect::route('slcadm00-user-fm-display', array('userid' => $usrid, 'name' => $user_name, 'pos_type' => $postyp, 'fldtype' => $fldtype, 'seq_no' => $seqn, 'rseq_no' => $rseqn));
            }
        }






        $mstfld_cur = DB::select(DB::raw("SELECT field_no,description FROM slmstfld "));
        $work_data->mstfld_cur = $mstfld_cur;



        return View::make('slcadm00.displayviafmupdate')->with('data', $work_data);
    }

    public function getReport() {
        return View::make('slcadm00.report');
    }

    public function getReportModQuery() {


        return View::make('slcadm00.reportmodquery');
    }

    public function getRepModBrowser() {




        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->update_date = trim(Input::get('update_date'));
        $this->update_time = trim(Input::get('update_time'));
        $this->action_cd = trim(Input::get('action_cd'));
        $this->maint_type_cd = trim(Input::get('maint_type_cd'));

        $this->item_code = trim(Input::get('item_code'));
        $this->mstr_fld_no = trim(Input::get('mstr_fld_no'));
        $this->orig_value = trim(Input::get('orig_value'));
        $this->new_value = trim(Input::get('new_value'));

        $paginate_array = array();




        $sql = "SELECT maint_type_cd,slhistry.userid,slhistry.update_date, slhistry.update_time, slhistry.item_code, slhistry.item_desc, slhistry.action_cd, slhistry.mstr_fld_no,seq_no  FROM slhistry WHERE 1=1 ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->update_date != '') {
            $sql.=" AND DATE(update_date)='" . date('Y-m-d', strtotime($this->update_date)) . "' ";
            $paginate_array['update_date'] = $this->update_date;
        }


        if ($this->update_time != '') {
            $sql.=" AND update_time='" . $this->update_time . "' ";
            $paginate_array['update_time'] = $this->update_time;
        }

        if ($this->action_cd != '') {
            $sql.=" AND action_cd='" . strtoupper($this->action_cd) . "' ";
            $paginate_array['action_cd'] = $this->action_cd;
        }

        if ($this->maint_type_cd != '') {
            $sql.=" AND maint_type_cd='" . strtoupper($this->maint_type_cd) . "' ";
            $paginate_array['maint_type_cd'] = $this->maint_type_cd;
        }

        if ($this->item_code != '') {
            $sql.=" AND item_code='" . $this->item_code . "' ";
            $paginate_array['item_code'] = $this->item_code;
        }
        if ($this->orig_value != '') {
            $sql.=" AND orig_value='" . $this->orig_value . "' ";
            $paginate_array['orig_value'] = $this->orig_value;
        }
        if ($this->new_value != '') {
            $sql.=" AND new_value='" . $this->new_value . "' ";
            $paginate_array['new_value'] = $this->new_value;
        }




        $sql.=" ORDER BY userid, update_date, update_time";



        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));



        if (!empty($count_results)) {




            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;

                switch ($action_cd) {
                    case "L":
                        $action_desc = "SIGNIN";
                        break;
                    case "O":
                        $action_desc = "SIGNOUT";
                        break;
                    case "C":
                        $action_desc = "SCANNED";
                        break;
                    case "S":
                        $action_desc = "AUDIT SCAN";
                        break;
                    case "N":
                        $action_desc = "NOT FOUND";
                        break;
                    case "A":
                        $action_desc = "ADD";
                        break;
                    case "U":
                        $action_desc = "UPDATE";
                        break;
                    case "D":
                        $action_desc = "DELETE";
                        break;
                    case "P":
                        $action_desc = "PRICE CHNG";
                        break;
                    case "F":
                        $action_desc = "FM NOTE";
                        break;
                    case "W":
                        $action_desc = "AISLE CHNG";
                        break;
                    case "V":
                        $action_desc = "TAG QTY CHNG";
                        break;
                    default:
                        $action_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->action_desc = $action_desc;

                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;



                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;
            }

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There is no Scan History satisfying the query conditions  ");
            return Redirect::route('slcadm00-report-mod-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;



        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);


        return View::make('slcadm00.reportmodbrowser')->with(array('data' => $work_data, 'pagination' => $pagination, 'paginate_array' => $paginate_array));
    }

    public function getRepModView() {
        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $seqn = Input::get('seq_no');

        if ($usrid == '' || $usrid <= 0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-report-mod-query');
        }


        $std_curs = DB::select(DB::raw("SELECT * FROM slhistry WHERE slhistry.userid = '" . $usrid . "' AND slhistry.seq_no = '" . $seqn . "' "));




        if (empty($std_curs)) {
            Session::flash('alert-danger', "There is no Scan History satisfying the query conditions");
            return Redirect::route('slcadm00-report-mod-query');
        } else {
            $row_data = $std_curs[0];




            $work_data->userid = $row_data->userid;
            $work_data->update_date = $row_data->update_date;
            $work_data->update_time = $row_data->update_time;
            $work_data->action_cd = $row_data->action_cd;
            $work_data->maint_type_cd = $row_data->maint_type_cd;
            $work_data->item_code = $row_data->item_code;
            $work_data->orig_value = $row_data->orig_value;
            $work_data->new_value = $row_data->new_value;
            $work_data->mstr_fld_no = $row_data->mstr_fld_no;



            $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

            $sql = "SELECT name FROM sluser WHERE userid = '" . $row_data->userid . "'";

            $userdata = DB::select(DB::raw($sql));

            if (!empty($userdata)) {
                $rowdata = $userdata[0];

                $user_name = $rowdata->name;
            }

            $work_data->user_name = $user_name;


            $action_cd = $row_data->action_cd;

            switch ($action_cd) {
                case "L":
                    $action_desc = "SIGNIN";
                    break;
                case "O":
                    $action_desc = "SIGNOUT";
                    break;
                case "C":
                    $action_desc = "SCANNED";
                    break;
                case "S":
                    $action_desc = "AUDIT SCAN";
                    break;
                case "N":
                    $action_desc = "NOT FOUND";
                    break;
                case "A":
                    $action_desc = "ADD";
                    break;
                case "U":
                    $action_desc = "UPDATE";
                    break;
                case "D":
                    $action_desc = "DELETE";
                    break;
                case "P":
                    $action_desc = "PRICE CHNG";
                    break;
                case "F":
                    $action_desc = "FM NOTE";
                    break;
                case "W":
                    $action_desc = "AISLE CHNG";
                    break;
                case "V":
                    $action_desc = "TAG QTY CHNG";
                    break;
                default:
                    $action_desc = "UNKNOWN";
                    break;
            }

            $work_data->action_desc = $action_desc;

            if ($row_data->maint_type_cd == "I") {
                $maint_desc = "IMMEDIATE";
            } else {
                if ($row_data->maint_type_cd == "B") {
                    $maint_desc = "BATCH";
                } else {
                    $maint_desc = " ";
                }
            }

            $work_data->maint_desc = $maint_desc;



            $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row_data->mstr_fld_no . "'";

            $mstflddata = DB::select(DB::raw($sql));

            if (!empty($mstflddata)) {
                $rowdata = $mstflddata[0];

                $mstr_fld_desc = $rowdata->description;

                if ($mstr_fld_desc == '') {
                    $mstr_fld_desc = "N/A";
                }
            }

            $work_data->mstr_fld_desc = $mstr_fld_desc;


            $work_data->usrid = $usrid;
            $work_data->sqno = $seqn;


            //====
        }




        return View::make('slcadm00.reportmodview')->with('data', $work_data);
    }

    public function getReportModSummaryQuery() {

        $work_data = new StdClass();

        return View::make('slcadm00.reportmodsummarydetail')->with('data', $work_data);
    }

    public function getReportModSummarybrowse() {


        $work_data = new StdClass();

        $this->userid = trim(Input::get('userid'));
        $this->update_date = trim(Input::get('update_date'));


        Validator::extend('checkuserexists', function($attribute, $value, $parameters) {




            $std_curs = DB::select(DB::raw("SELECT * FROM sluser WHERE userid = '" . $value . "'"));


            if (!empty($std_curs)) {
                return true;
            }


            return false;
        }, 'User Id is not exists');


        $validator = Validator::make(Input::all(), array(
                    'userid' => 'required|integer|checkuserexists',
                    'update_date' => 'required',
        ),array('userid.required'=>'User id is required',
                'update_date.required'=>'Update date is required'));




        //print_r($validator);

        if ($validator->fails()) {

            return Redirect::route('slcadm00-report-mod-summary-query')
                            ->withErrors($validator)
                            ->withInput();
        } else {

            $paginate_array = array();




            $sql = "SELECT * FROM slhistry WHERE 1=1 ";

            if ($this->userid != '') {
                $sql.=" AND userid='" . $this->userid . "' ";
                $paginate_array['userid'] = $this->userid;
            }

            if ($this->update_date != '') {
                $sql.=" AND DATE(update_date)='" . date('Y-m-d', strtotime($this->update_date)) . "' ";
                $paginate_array['update_date'] = $this->update_date;
            }


            $sql.=" ORDER BY seq_no asc";



            $results = DB::select(DB::raw($sql));

            if (!empty($results)) {

                $previous_item = 0;
                $previous_act = "";


                $tot_scanned = 0;
                $tot_aisle_scanned = 0;
                $tot_add = 0;
                $tot_upd = 0;
                $tot_del = 0;
                $tot_updprice = 0;
                $tot_updsprice = 0;
                $tot_fm_notes = 0;
                $tot_notfound = 0;
                $tot_signin = 0;
                $tot_signout = 0;
                $tot_unknown = 0;



                $get_user = DB::select(DB::raw("SELECT * FROM sluser WHERE userid = '" . $this->userid . "'"));


                $user_name = '';
                if (isset($get_user[0])) {
                    if (!empty($get_user[0])) {
                        $rowuser = $get_user[0];
                        $user_name = $rowuser->name;
                    }
                }



                foreach ($results as $row) {

                    $action_cd = $row->action_cd;
                    $item_code = $row->item_code;

                    if ($action_cd == "C" OR $action_cd == "N") {
                        
                    } else {
                        if ($previous_item == $item_code) {
                            
                        } else {
                            $previous_act = "";
                        }
                    }


                    switch ($action_cd) {
                        CASE "L":
                            $tot_signin = $tot_signin + 1;
                        CASE "O":
                            $tot_signout = $tot_signout + 1;
                        CASE "C":
                            $tot_scanned = $tot_scanned + 1;
                        CASE "S":
                            $tot_scanned = $tot_scanned + 1;
                        CASE "W":
                            $tot_aisle_scanned = $tot_aisle_scanned + 1;
                        CASE "N":
                            $tot_scanned = $tot_scanned + 1;
                            $tot_notfound = $tot_notfound + 1;
                        default:
                            if ($action_cd == $previous_act) {
                                
                            } else {
                                switch ($action_cd) {
                                    CASE "A":
                                        $tot_add = $tot_add + 1;
                                    CASE "U":
                                        $tot_upd = $tot_upd + 1;
                                    CASE "D":
                                        $tot_del = $tot_del + 1;
                                    CASE "P":
                                        $tot_updprice = $tot_updprice + 1;
                                    # CASE "S":
                                    #    $tot_updsprice = $tot_updsprice + 1;
                                    CASE "F":
                                        $tot_fm_notes = $tot_fm_notes + 1;
                                    default:
                                        $tot_unknown = $tot_unknown + 1;
                                }
                            }
                    }


                    # s"C"anned and "N"otFound audit records do not affect
                    # previous item logic
                    if ($action_cd == "C" OR $action_cd == "N") {
                        
                    } else {
                        $previous_item = $item_code;
                        $previous_act = $action_cd;
                    }
                }


                $work_data->userid = $this->userid;
                $work_data->update_date = $this->update_date;
                $work_data->user_name = $user_name;
                $work_data->tot_scanned = $tot_scanned;
                $work_data->tot_aisle_scanned = $tot_aisle_scanned;
                $work_data->tot_add = $tot_add;
                $work_data->tot_upd = $tot_upd;
                $work_data->tot_del = $tot_del;
                $work_data->tot_updprice = $tot_updprice;
                $work_data->tot_updsprice = $tot_updsprice;
                $work_data->tot_fm_notes = $tot_fm_notes;
                $work_data->tot_notfound = $tot_notfound;
                $work_data->tot_signin = $tot_signin;
                $work_data->tot_signout = $tot_signout;
                $work_data->tot_unknown = $tot_unknown;
            } else {

                Session::flash('alert-danger', " There is NO Scan History for this User and Date to be printed.");
                return Redirect::route('slcadm00-report-mod-summary-query')->withInput();
            }



            //}
            //echo "<pre>"; print_r($results); die;
        }


        return View::make('slcadm00.reportmodsummarybrowser')->with(array('data' => $work_data));
    }

    public function getReportModDetailbrowse() {


        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->update_date = trim(Input::get('update_date'));
        $this->update_time = trim(Input::get('update_time'));
        $this->action_cd = trim(Input::get('action_cd'));
        $this->maint_type_cd = trim(Input::get('maint_type_cd'));

        $this->item_code = trim(Input::get('item_code'));
        $this->mstr_fld_no = trim(Input::get('mstr_fld_no'));
        $this->orig_value = trim(Input::get('orig_value'));
        $this->new_value = trim(Input::get('new_value'));




        $sql = "SELECT maint_type_cd,slhistry.userid,slhistry.update_date, slhistry.update_time, slhistry.item_code, slhistry.item_desc, slhistry.action_cd, slhistry.mstr_fld_no,seq_no,new_value,orig_value  FROM slhistry WHERE 1=1 ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->update_date != '') {
            $sql.=" AND DATE(update_date)='" . date('Y-m-d', strtotime($this->update_date)) . "' ";
            $paginate_array['update_date'] = $this->update_date;
        }


        if ($this->update_time != '') {
            $sql.=" AND update_time='" . $this->update_time . "' ";
            $paginate_array['update_time'] = $this->update_time;
        }

        if ($this->action_cd != '') {
            $sql.=" AND action_cd='" . strtoupper($this->action_cd) . "' ";
            $paginate_array['action_cd'] = $this->action_cd;
        }

        if ($this->maint_type_cd != '') {
            $sql.=" AND maint_type_cd='" . strtoupper($this->maint_type_cd) . "' ";
            $paginate_array['maint_type_cd'] = $this->maint_type_cd;
        }

        if ($this->item_code != '') {
            $sql.=" AND item_code='" . $this->item_code . "' ";
            $paginate_array['item_code'] = $this->item_code;
        }
        if ($this->orig_value != '') {
            $sql.=" AND orig_value='" . $this->orig_value . "' ";
            $paginate_array['orig_value'] = $this->orig_value;
        }
        if ($this->new_value != '') {
            $sql.=" AND new_value='" . $this->new_value . "' ";
            $paginate_array['new_value'] = $this->new_value;
        }




        $sql.=" ORDER BY userid, update_date, update_time";





        $results = DB::select(DB::raw($sql));



        if (!empty($results)) {


            $previous_item = 0;
            $previous_act = "";
            $previous_time = 0;

            $tot_scanned = 0;
            $tot_aisle_scanned = 0;
            $tot_add = 0;
            $tot_upd = 0;
            $tot_del = 0;
            $tot_updprice = 0;
            $tot_updsprice = 0;
            $tot_fm_notes = 0;
            $tot_notfound = 0;
            $tot_signin = 0;
            $tot_signout = 0;
            $tot_unknown = 0;


            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;





                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;



                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;


                $item_code = $row->item_code;

                if ($action_cd == "C" OR $action_cd == "N") {
                    
                } else {
                    if ($previous_item == $item_code) {
                        
                    } else {
                        $previous_act = "";
                        $previous_time = 0;
                    }
                }

                $action_desc = '';

                switch ($action_cd) {
                    CASE "L":
                        $tot_signin = $tot_signin + 1;
                    CASE "O":
                        $tot_signout = $tot_signout + 1;
                    CASE "C":
                        $tot_scanned = $tot_scanned + 1;
                    CASE "S":
                        $tot_scanned = $tot_scanned + 1;
                    CASE "W":
                        $tot_aisle_scanned = $tot_aisle_scanned + 1;
                    CASE "N":
                        $tot_scanned = $tot_scanned + 1;
                        $tot_notfound = $tot_notfound + 1;
                    default:
                        if ($action_cd == $previous_act) {
                            
                        } else {
                            switch ($action_cd) {
                                CASE "A":
                                    $tot_add = $tot_add + 1;
                                    $action_desc = "ADD";
                                CASE "U":
                                    $tot_upd = $tot_upd + 1;
                                    $action_desc = "UPDATE";
                                CASE "D":
                                    $tot_del = $tot_del + 1;
                                    $action_desc = "DELETE";
                                CASE "P":
                                    $tot_updprice = $tot_updprice + 1;
                                    $action_desc = "PRICE CHANGE";
                                # CASE "S":
                                #    $tot_updsprice = $tot_updsprice + 1;
                                CASE "F":
                                    $tot_fm_notes = $tot_fm_notes + 1;
                                    $action_desc = "FM NOTE";
                                default:
                                    $tot_unknown = $tot_unknown + 1;
                                    $action_desc = "UNKNOWN";
                            }
                        }
                }


                if ($item_code == $previous_time AND $action_cd == $previous_act AND $row->update_time == $previous_time) {
                    $action_desc = "";
                }

                $results[$key]->action_desc = $action_desc;

                # Because s"C"anned and "N"otFound audit records are only tallied 
                # and not detailed on the report, do not note them as a previous
                # item
                if ($action_cd == "C" OR $action_cd == "N") {
                    
                } else {
                    $previous_item = $item_code;
                    $previous_act = $action_cd;
                    $previous_time = $row->update_time;
                }
            }

            $work_data->tot_scanned = $tot_scanned;
            $work_data->tot_aisle_scanned = $tot_aisle_scanned;
            $work_data->tot_add = $tot_add;
            $work_data->tot_upd = $tot_upd;
            $work_data->tot_del = $tot_del;
            $work_data->tot_updprice = $tot_updprice;
            $work_data->tot_updsprice = $tot_updsprice;
            $work_data->tot_fm_notes = $tot_fm_notes;
            $work_data->tot_notfound = $tot_notfound;
            $work_data->tot_signin = $tot_signin;
            $work_data->tot_signout = $tot_signout;
            $work_data->tot_unknown = $tot_unknown;
            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There is no Scan History satisfying the query conditions  ");
            return Redirect::route('slcadm00-report-mod-query')->withInput();
        }





        $work_data->results = $results;

        //echo "<pre>"; print_r($work_data); die;

        return View::make('slcadm00.reportmoddetailbrowser')->with(array('data' => $work_data));
    }

    public function getReportFmQuery() {
        return View::make('slcadm00.reportfmquery');
    }

    public function getReportFmBrowser() {


        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->from_date = trim(Input::get('from_date'));
        $this->to_date = trim(Input::get('to_date'));


        $paginate_array = array();




        $sql = "SELECT * FROM slhistry WHERE slhistry.action_cd = 'F'  ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->from_date != '' && $this->to_date != '') {
            $sql.=" AND ( DATE(update_date)  BETWEEN '" . date('Y-m-d', strtotime($this->from_date)) . "' AND '" . date('Y-m-d', strtotime($this->to_date)) . "' ) ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = $this->to_date;
        } elseif ($this->from_date != '') {
            $sql.=" AND DATE(update_date)>='" . date('Y-m-d', strtotime($this->from_date)) . "' ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = '';
        } elseif ($this->to_date != '') {
            $sql.=" AND DATE(update_date)<='" . date('Y-m-d', strtotime($this->to_date)) . "' ";
            $paginate_array['to_date'] = $this->to_date;
            $paginate_array['from_date'] = '';
        }




        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));



        if (!empty($count_results)) {




            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;

                switch ($action_cd) {
                    case "L":
                        $action_desc = "SIGNIN";
                        break;
                    case "O":
                        $action_desc = "SIGNOUT";
                        break;
                    case "C":
                        $action_desc = "SCANNED";
                        break;
                    case "S":
                        $action_desc = "AUDIT SCAN";
                        break;
                    case "N":
                        $action_desc = "NOT FOUND";
                        break;
                    case "A":
                        $action_desc = "ADD";
                        break;
                    case "U":
                        $action_desc = "UPDATE";
                        break;
                    case "D":
                        $action_desc = "DELETE";
                        break;
                    case "P":
                        $action_desc = "PRICE CHNG";
                        break;
                    case "F":
                        $action_desc = "FM NOTE";
                        break;
                    case "W":
                        $action_desc = "AISLE CHNG";
                        break;
                    case "V":
                        $action_desc = "TAG QTY CHNG";
                        break;
                    default:
                        $action_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->action_desc = $action_desc;

                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;



                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;
            }

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There are no FM Notes satisfying the query conditions    ");
            return Redirect::route('slcadm00-report-fm-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;



        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);


        return View::make('slcadm00.reportfmbrowser')->with(array('data' => $work_data, 'pagination' => $pagination, 'paginate_array' => $paginate_array));
    }

    public function getReportFmView() {

        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $seqn = Input::get('seq_no');

        if ($usrid == '' || $usrid <= 0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('slcadm00-report-fm-query');
        }


        $std_curs = DB::select(DB::raw("SELECT * FROM slhistry WHERE slhistry.action_cd = 'F' and slhistry.userid = '" . $usrid . "' AND slhistry.seq_no = '" . $seqn . "' "));




        if (empty($std_curs)) {
            Session::flash('alert-danger', "There are no FM Notes satisfying the query conditions");
            return Redirect::route('slcadm00-report-fm-query');
        } else {
            $row_data = $std_curs[0];




            $work_data->userid = $row_data->userid;
            $work_data->update_date = $row_data->update_date;
            $work_data->update_time = $row_data->update_time;
            $work_data->action_cd = $row_data->action_cd;
            $work_data->maint_type_cd = $row_data->maint_type_cd;
            $work_data->item_code = $row_data->item_code;
            $work_data->orig_value = $row_data->orig_value;
            $work_data->new_value = $row_data->new_value;
            $work_data->mstr_fld_no = $row_data->mstr_fld_no;



            $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

            $sql = "SELECT name FROM sluser WHERE userid = '" . $row_data->userid . "'";

            $userdata = DB::select(DB::raw($sql));

            if (!empty($userdata)) {
                $rowdata = $userdata[0];

                $user_name = $rowdata->name;
            }

            $work_data->user_name = $user_name;


            $action_cd = $row_data->action_cd;

            switch ($action_cd) {
                case "L":
                    $action_desc = "SIGNIN";
                    break;
                case "O":
                    $action_desc = "SIGNOUT";
                    break;
                case "C":
                    $action_desc = "SCANNED";
                    break;
                case "S":
                    $action_desc = "AUDIT SCAN";
                    break;
                case "N":
                    $action_desc = "NOT FOUND";
                    break;
                case "A":
                    $action_desc = "ADD";
                    break;
                case "U":
                    $action_desc = "UPDATE";
                    break;
                case "D":
                    $action_desc = "DELETE";
                    break;
                case "P":
                    $action_desc = "PRICE CHNG";
                    break;
                case "F":
                    $action_desc = "FM NOTE";
                    break;
                case "W":
                    $action_desc = "AISLE CHNG";
                    break;
                case "V":
                    $action_desc = "TAG QTY CHNG";
                    break;
                default:
                    $action_desc = "UNKNOWN";
                    break;
            }

            $work_data->action_desc = $action_desc;

            if ($row_data->maint_type_cd == "I") {
                $maint_desc = "IMMEDIATE";
            } else {
                if ($row_data->maint_type_cd == "B") {
                    $maint_desc = "BATCH";
                } else {
                    $maint_desc = " ";
                }
            }

            $work_data->maint_desc = $maint_desc;



            $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row_data->mstr_fld_no . "'";

            $mstflddata = DB::select(DB::raw($sql));

            if (!empty($mstflddata)) {
                $rowdata = $mstflddata[0];

                $mstr_fld_desc = $rowdata->description;

                if ($mstr_fld_desc == '') {
                    $mstr_fld_desc = "N/A";
                }
            }

            $work_data->mstr_fld_desc = $mstr_fld_desc;


            $work_data->usrid = $usrid;
            $work_data->sqno = $seqn;


            //====
        }
        return View::make('slcadm00.reportfmview')->with('data', $work_data);
    }

    public function getReportFmReport() {
           $sql = "SELECT * FROM slhistry WHERE slhistry.action_cd = 'F'  ";
     $result = DB::select(DB::raw($sql));
      $results= json_decode(json_encode($result), true);
     
        return View::make('slcadm00.reportfmreportquery1',compact('results'));
    }

    public function getReportFmReportBrowser() {


        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->from_date = trim(Input::get('from_date'));
        $this->to_date = trim(Input::get('to_date'));


        $paginate_array = array();




        $sql = "SELECT * FROM slhistry WHERE slhistry.action_cd = 'F'  ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->from_date != '' && $this->to_date != '') {
            $sql.=" AND ( DATE(update_date)  BETWEEN '" . date('Y-m-d', strtotime($this->from_date)) . "' AND '" . date('Y-m-d', strtotime($this->to_date)) . "' ) ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = $this->to_date;
        } elseif ($this->from_date != '') {
            $sql.=" AND DATE(update_date)>='" . date('Y-m-d', strtotime($this->from_date)) . "' ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = '';
        } elseif ($this->to_date != '') {
            $sql.=" AND DATE(update_date)<='" . date('Y-m-d', strtotime($this->to_date)) . "' ";
            $paginate_array['to_date'] = $this->to_date;
            $paginate_array['from_date'] = '';
        }






        $results = DB::select(DB::raw($sql));



        $tot_fm_notes = 0;


        if (!empty($results)) {




            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;

                switch ($action_cd) {
                    case "L":
                        $action_desc = "SIGNIN";
                        break;
                    case "O":
                        $action_desc = "SIGNOUT";
                        break;
                    case "C":
                        $action_desc = "SCANNED";
                        break;
                    case "S":
                        $action_desc = "AUDIT SCAN";
                        break;
                    case "N":
                        $action_desc = "NOT FOUND";
                        break;
                    case "A":
                        $action_desc = "ADD";
                        break;
                    case "U":
                        $action_desc = "UPDATE";
                        break;
                    case "D":
                        $action_desc = "DELETE";
                        break;
                    case "P":
                        $action_desc = "PRICE CHNG";
                        break;
                    case "F":
                        $action_desc = "FM NOTE";
                        break;
                    case "W":
                        $action_desc = "AISLE CHNG";
                        break;
                    case "V":
                        $action_desc = "TAG QTY CHNG";
                        break;
                    default:
                        $action_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->action_desc = $action_desc;

                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;

                $tot_fm_notes = $tot_fm_notes + 1;

                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;
            }

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There are no FM Notes satisfying the query conditions    ");
            return Redirect::route('slcadm00-report-fm-report-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data->results = $results;

        $work_data->tot_fm_notes = $tot_fm_notes;





        return View::make('slcadm00.reportfmreportbrowser')->with(array('data' => $work_data, 'paginate_array' => $paginate_array));
    }

    public function getReportScanQuery() {
        return View::make('slcadm00.reportscanquery');
    }

    public function getReportScanBrowser() {



        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->from_date = trim(Input::get('from_date'));
        $this->to_date = trim(Input::get('to_date'));


        $paginate_array = array();




        $sql = "SELECT * FROM slhistry WHERE 1=1  ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->from_date != '' && $this->to_date != '') {
            $sql.=" AND ( DATE(update_date)  BETWEEN '" . date('Y-m-d', strtotime($this->from_date)) . "' AND '" . date('Y-m-d', strtotime($this->to_date)) . "' ) ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = $this->to_date;
        } elseif ($this->from_date != '') {
            $sql.=" AND DATE(update_date)>='" . date('Y-m-d', strtotime($this->from_date)) . "' ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = '';
        } elseif ($this->to_date != '') {
            $sql.=" AND DATE(update_date)<='" . date('Y-m-d', strtotime($this->to_date)) . "' ";
            $paginate_array['to_date'] = $this->to_date;
            $paginate_array['from_date'] = '';
        }




        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));



        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " There are no Scanning Details satisfying the query conditions ");
            return Redirect::route('slcadm00-report-scan-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;



        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);


        return View::make('slcadm00.reportscanbrowse')->with(array('data' => $work_data, 'pagination' => $pagination, 'paginate_array' => $paginate_array));
    }

    public function getReportScanReport() {
        return View::make('slcadm00.reportscanreportquery');
    }

    public function getReportScanReportBrowser() {



        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->from_date = trim(Input::get('from_date'));
        $this->to_date = trim(Input::get('to_date'));


        $paginate_array = array();




        $sql = "SELECT * FROM slhistry WHERE 1=1  ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->from_date != '' && $this->to_date != '') {
            $sql.=" AND ( DATE(update_date)  BETWEEN '" . date('Y-m-d', strtotime($this->from_date)) . "' AND '" . date('Y-m-d', strtotime($this->to_date)) . "' ) ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = $this->to_date;
        } elseif ($this->from_date != '') {
            $sql.=" AND DATE(update_date)>='" . date('Y-m-d', strtotime($this->from_date)) . "' ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = '';
        } elseif ($this->to_date != '') {
            $sql.=" AND DATE(update_date)<='" . date('Y-m-d', strtotime($this->to_date)) . "' ";
            $paginate_array['to_date'] = $this->to_date;
            $paginate_array['from_date'] = '';
        }




        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;


        $sql.=" ORDER BY userid, seq_no ";

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));



        if (!empty($count_results)) {

            $previous_item = 0;
            $previous_act = "";
            $previous_time = 0;

            $tot_scanned = 0;
            $tot_aisle_scanned = 0;
            $tot_add = 0;
            $tot_upd = 0;
            $tot_del = 0;
            $tot_updprice = 0;
            $tot_updsprice = 0;
            $tot_fm_notes = 0;
            $tot_notfound = 0;
            $tot_signin = 0;
            $tot_signout = 0;
            $tot_unknown = 0;

            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;

                switch ($action_cd) {
                    case "L":
                        $action_desc = "SIGNIN";
                        break;
                    case "O":
                        $action_desc = "SIGNOUT";
                        break;
                    case "C":
                        $action_desc = "SCANNED";
                        break;
                    case "S":
                        $action_desc = "AUDIT SCAN";
                        break;
                    case "N":
                        $action_desc = "NOT FOUND";
                        break;
                    case "A":
                        $action_desc = "ADD";
                        break;
                    case "U":
                        $action_desc = "UPDATE";
                        break;
                    case "D":
                        $action_desc = "DELETE";
                        break;
                    case "P":
                        $action_desc = "PRICE CHNG";
                        break;
                    case "F":
                        $action_desc = "FM NOTE";
                        break;
                    case "W":
                        $action_desc = "AISLE CHNG";
                        break;
                    case "V":
                        $action_desc = "TAG QTY CHNG";
                        break;
                    default:
                        $action_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->action_desc = $action_desc;


                $item_code = $row->item_code;
                
                if ($action_cd == "C" OR $action_cd == "N") {
                    
                } else {
                    if ($previous_item == $item_code) {
                        
                    } else {
                        $previous_act = "";
                        $previous_time = 0;
                    }
                }
                $total_sum[$row->userid]['tot_signin']=0;
                $total_sum[$row->userid]['tot_signout']=0;
                $total_sum[$row->userid]['tot_scanned']=0;
                $total_sum[$row->userid]['tot_aisle_scanned']=0;
                $total_sum[$row->userid]['tot_del']=0;
                $total_sum[$row->userid]['tot_fm_notes']=0;
                $total_sum[$row->userid]['tot_notfound']=0;
                $total_sum[$row->userid]['tot_add']=0;
                $total_sum[$row->userid]['tot_upd']=0;
                $total_sum[$row->userid]['tot_updprice']=0;
                $total_sum[$row->userid]['tot_unknown']=0;
                
                
                switch ($action_cd) {
                    CASE "L":
                        $total_sum[$row->userid]['tot_signin'] = $total_sum[$row->userid]['tot_signin'] + 1;
                    CASE "O":
                        $total_sum[$row->userid]['tot_signout'] = $total_sum[$row->userid]['tot_signout'] + 1;
                    CASE "C":
                        $total_sum[$row->userid]['tot_scanned'] = $total_sum[$row->userid]['tot_scanned'] + 1;
                    CASE "S":
                        $total_sum[$row->userid]['tot_scanned'] = $total_sum[$row->userid]['tot_scanned'] + 1;
                    CASE "W":
                        $total_sum[$row->userid]['tot_aisle_scanned'] = $total_sum[$row->userid]['tot_aisle_scanned'] + 1;
                    CASE "D":
                        $total_sum[$row->userid]['tot_del'] = $total_sum[$row->userid]['tot_del'] + 1;
                    CASE "F":
                        $total_sum[$row->userid]['tot_fm_notes'] = $total_sum[$row->userid]['tot_fm_notes'] + 1;
                    CASE "N":
                        $total_sum[$row->userid]['tot_notfound'] = $total_sum[$row->userid]['tot_notfound'] + 1;
                    CASE "A":
                        if ($action_cd == $previous_act && $item_code == $previous_item) {
                           
                        } else {
                            $total_sum[$row->userid]['tot_add'] = $total_sum[$row->userid]['tot_add'] + 1;
                        }
                    CASE "U":
                        if ($action_cd == $previous_act && $item_code == $previous_item) {
                            
                        } else {
                            $total_sum[$row->userid]['tot_upd'] = $total_sum[$row->userid]['tot_upd'] + 1;
                        }
                    CASE "P":
                        if ($action_cd == $previous_act && $item_code == $previous_item) {
                            
                        } else {
                            $total_sum[$row->userid]['tot_updprice'] = $total_sum[$row->userid]['tot_updprice'] + 1;
                        }
                    default:

                        $total_sum[$row->userid]['tot_unknown'] = $total_sum[$row->userid]['tot_unknown'] + 1;
                }

                
                if ($action_cd == "C" OR $action_cd == "N") {
                    
                } else {
                    $previous_item = $item_code;
                    $previous_act = $action_cd;
                    $previous_time = $row->update_time;
                }
                
                
                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;



                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;
            }
            
             $work_data->total_sum = $total_sum;
            
        } else {

            Session::flash('alert-danger', " There are no Scanning Details satisfying the query conditions ");
            return Redirect::route('slcadm00-report-scan-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data->results = $results;



        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);


        return View::make('slcadm00.reportscanreportbrowse')->with(array('data' => $work_data, 'pagination' => $pagination, 'paginate_array' => $paginate_array));
    }

}

?>
