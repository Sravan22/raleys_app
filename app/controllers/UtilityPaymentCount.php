<?php
class UtilityPaymentCount extends BaseController
{
	public function getbookutilitypymntCnt()
	{
		return View::make('mktmgr.bookutilitypymntcnt'); 
	}
	public function postbookutilitypymntCnt()
	{
		date_default_timezone_set('America/Los_Angeles');
		 Validator::extend('prevdate', function($attribute, $value, $parameters) {
		 	$previewsdate =  date('Y-m-d', strtotime('-1 day')); 
		 	$entry_date = Input::get('entry_date');          
            if($previewsdate==$entry_date){
                return true;
            }            
            return false;
        },'Date must be previous day');
        
        $validator = Validator::make(Input::all(),
        array(
           'entry_date'=>'required|date|prevdate',
        ),
        array(
        	'entry_date.required' =>'Utility Payment Date is Required'
        	));
        
        if($validator->fails())
		{
			return Redirect::route('mktmgr-bookutilitypymntcnt')
					->withErrors($validator)
					->withInput();
		}
		else
		{
			$entry_date = Input::get('entry_date');
			$utility_rec = DB::select('SELECT * FROM utility WHERE utility.entry_date = "'.$entry_date.'"');
			if(empty($utility_rec))
			{	
				return View::make('mktmgr.postutilitypymtcnt',compact('entry_date'));
			}
			else
			{
				$entry_date = $utility_rec[0]->entry_date;
				$util_cnt = $utility_rec[0]->util_cnt;
				$change_date = $utility_rec[0]->change_date;
				return View::make('mktmgr.postutilitypymtcnt_update',compact('entry_date','util_cnt','change_date'));
			}
		}
		
	}
	public function postbookutilitypymntCntData()
	{
		$util_cnt = Input::get('util_cnt');
		$entry_date = Input::get('entry_date');
		$change_date = date('Y-m-d');
		$utility_rec = DB::select('SELECT * FROM utility WHERE utility.entry_date = "'.$entry_date.'"');
		if(empty($utility_rec))
		{
			$insertdata = DB::table('utility')->insert(
					array(
						'entry_date' => $entry_date,
						'util_cnt' => $util_cnt,
						'change_date' => $change_date
					)
				);
			if($insertdata)
			{
				Session::flash('alert-success', 'Utility Payment Count Added Successfully!');
                return Redirect::route('mktmgr-bookutilitypymntcnt');
			}
		}
	}
	public function postbookutilitypymntCntDataUpdate()
	{
		$util_cnt = Input::get('util_cnt');
		$entry_date = Input::get('entry_date');
		$change_date = date('Y-m-d');
		$update_utility = DB::table('utility')
		            ->where('entry_date', $entry_date)
		            ->update(array(
		            	'entry_date' => $entry_date,
		            	'util_cnt' => $util_cnt,
		            	'change_date' => $change_date
		            	));
	        if($update_utility)
	        {
	        	Session::flash('alert-success', 'Utility Payment Count has been Updated!'); 
	          	return Redirect::route('mktmgr-bookutilitypymntcnt');
	        }
	        else
	        {
	        	Session::flash('alert-danger', 'Utility Payment Count has not been Updated.!'); 
	          	return Redirect::route('mktmgr-bookutilitypymntcnt');
	        }
	}
	public function viewCounts()
	{
		$entry_date =  date('Y-m-d', strtotime('-14 days'));
		$utility_rec = DB::select('SELECT * FROM utility WHERE utility.entry_date >= "'.$entry_date.'"');
		$utility_rec_array = json_decode(json_encode($utility_rec), true);
		return View::make('mktmgr.viewcounts',compact('utility_rec_array'));
	}
}