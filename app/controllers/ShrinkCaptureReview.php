<?php

class  ShrinkCaptureReview extends  BaseController
{
	public function shrinkCaptureReview()
	{
		return View::make('mktmgr.shrinkcapturereview');
	}	
	public function postShrinkCaptureReview()
	{
		$shrink_date = Input::get('shrink_date');
		$results = DB::select('select * from shrnkhdr where shrink_date = "'.$shrink_date.'" and dept_no in (1,2,3,4,5,6,7,8,9,10,11,12,35) order by dept_no');
		$shrinkdata = json_decode(json_encode($results), true); 
		
		$dept_01_tot_value = DB::select('SELECT sum(tot_value) as dept_01_tot_value  FROM shrnkhdr WHERE dept_no = 01 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_02_tot_value = DB::select('SELECT sum(tot_value) as dept_02_tot_value  FROM shrnkhdr WHERE dept_no = 02 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_03_tot_value = DB::select('SELECT sum(tot_value) as dept_03_tot_value  FROM shrnkhdr WHERE dept_no = 03 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_04_tot_value = DB::select('SELECT sum(tot_value) as dept_04_tot_value  FROM shrnkhdr WHERE dept_no = 04 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_05_tot_value = DB::select('SELECT sum(tot_value) as dept_05_tot_value  FROM shrnkhdr WHERE dept_no = 05 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_06_tot_value = DB::select('SELECT sum(tot_value) as dept_06_tot_value  FROM shrnkhdr WHERE dept_no = 06 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_07_tot_value = DB::select('SELECT sum(tot_value) as dept_07_tot_value  FROM shrnkhdr WHERE dept_no = 07 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_08_tot_value = DB::select('SELECT sum(tot_value) as dept_08_tot_value  FROM shrnkhdr WHERE dept_no = 08 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_09_tot_value = DB::select('SELECT sum(tot_value) as dept_09_tot_value  FROM shrnkhdr WHERE dept_no = 09 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_10_tot_value = DB::select('SELECT sum(tot_value) as dept_10_tot_value  FROM shrnkhdr WHERE dept_no = 10 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_11_tot_value = DB::select('SELECT sum(tot_value) as dept_11_tot_value  FROM shrnkhdr WHERE dept_no = 11 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_12_tot_value = DB::select('SELECT sum(tot_value) as dept_12_tot_value  FROM shrnkhdr WHERE dept_no = 12 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
		$dept_35_tot_value = DB::select('SELECT sum(tot_value) as dept_35_tot_value  FROM shrnkhdr WHERE dept_no = 35 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');

		return View::make('mktmgr.shrinkcaptureredata',compact('shrinkdata','shrink_date','dept_01_tot_value','dept_02_tot_value','dept_03_tot_value','dept_04_tot_value','dept_05_tot_value','dept_06_tot_value','dept_07_tot_value','dept_08_tot_value','dept_09_tot_value','dept_10_tot_value','dept_11_tot_value','dept_12_tot_value','dept_35_tot_value'));
	}
	public function ShrinkCaptureDepartment()
	{
		$dept_no = Input::get('dept_no');
		$shrink_date = Input::get('shrink_date');
		// echo $dept_no.'<br>';
		// echo $shrink_date;exit;
		$shrinkcapturedepart = DB::select('SELECT seq_number, shrink_date, shrink_number, employee_id, tot_line_items, tot_value, shrnkhdr.status  FROM shrnkhdr  WHERE dept_no = "'.$dept_no.'"
         AND shrink_date = "'.$shrink_date.'" ');
		return View::make('mktmgr.shrinkcapturefordepartment',compact('shrinkcapturedepart','dept_no','shrink_date'));
	}
	public function ShrinkCaptureItemScan()
	{	
		$shrink_date = Input::get('shrink_date');
		$shrink_number = Input::get('shrink_number');
		$employee_id = Input::get('employee_id');
		$tot_line_items = Input::get('tot_line_items');
		$tot_value = Input::get('tot_value');
		$seq_number = Input::get('seq_number');
		$status = Input::get('status');
		$shrinkcaptureitemscan = DB::select('SELECT seq_number, item_desc, upc_cost, quantity,rtl_amt, rw_rtl_amt                     
          FROM shrnkdtl
         WHERE hdr_seq_number = "'.$seq_number.'"
           AND shrnkdtl.status = "I"');
		return View::make('mktmgr.shrinkcapturereitemscan',compact('shrinkcaptureitemscan','shrink_date','shrink_number','employee_id','tot_line_items','tot_value','seq_number','status'));
	}
	public function ShrinkCaptureItemScanUpdate()
	{
		$shrink_number = Input::get('shrink_number');
		$seq_number = Input::get('seq_number');
		$iteminnerdetails = DB::select('SELECT * FROM shrnkdtl WHERE hdr_seq_number = "'.$shrink_number.'" AND seq_number = "'.$seq_number.'" ');
		 return View::make('mktmgr.shrinkcaptureiteminnerdetails',compact('iteminnerdetails'));
	}
	public function UpdateShrinkItemSan()
	{
		$hdr_seq_number = Input::get('hdr_seq_number');
		$seq_number = Input::get('seq_number');
		$quantity = Input::get('quantity');
		$iteminnerdetails = DB::select('SELECT * FROM shrnkdtl WHERE hdr_seq_number = "'.$hdr_seq_number.'" AND seq_number = "'.$seq_number.'" ');
		
		return View::make('mktmgr.shrinkcaptureitemupdate',compact('iteminnerdetails'));
	}
	public function PostUpdateShrinkItemSan()
	{
		//echo 'Hiii';exit;
		$hdr_seq_number = Input::get('hdr_seq_number');
		$seq_number = Input::get('seq_number');
		$quantity = Input::get('quantity');


		$update_limit = DB::table('shrnkdtl')
		            ->where('seq_number', $seq_number)
		            ->where('hdr_seq_number', $hdr_seq_number)
		            ->update(array('quantity' => $quantity));
        if($update_limit)
        {
        	Session::flash('alert-success', 'Shrink Quantiy Updated!'); 
          	return Redirect::route('mktmgr-shrinkcapturereview');
        	//echo 'success';
        }
        else
        {
        	Session::flash('alert-danger', 'Shrink Quantiy Not Updated!'); 
          	return Redirect::route('mktmgr-shrinkcapturereview');
        }
	}
}	