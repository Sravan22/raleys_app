<?php

class UserSocadm00Controller extends BaseController
{
	
	public function getReturnhome()
	{
		return View::make('socadm00.homepage');
	}
	public function storetransmission()
	{
		return View::make('socadm00.storetransmission');

	}
	public function getStoreTransmissionQuery()
	{
		return View::make('socadm00.storetransmissionquery');
	}
	/*public function postmenuquery()
	{
		$inputdata = Input::all();
		$gl_dept_number = Input::get('gl_dept_number');
		$menu_level = Input::get('menu_level');
		$function_code = Input::get('function_code');
		$type_code = Input::get('type_code');
		$subtype_code = Input::get('subtype_code');
		$description = Input::get('description');
		$menu_position = Input::get('menu_position');
		$goto_pgm_id = Input::get('goto_pgm_id');
		$active_switch = Input::get('active_switch');

		$select = "select * from somencfg ";
        $where = 'where 1 = 1 ';
	            	if(Input::has('gl_dept_number'))
	            	{
	            		$where.="and  gl_dept_number = '$gl_dept_number' ";
                    }
                	if(Input::has('menu_level'))
            		{
            			$where.=" and menu_level = '$menu_level' ";
                	}
                	if(Input::has('function_code'))
            		{
            			$where.=" and function_code = '$function_code' ";
                	}
                	if(Input::has('type_code'))
            		{
            			$where.=" and type_code = '$type_code' ";
                	}
                	if(Input::has('subtype_code'))
            		{
            			$where.=" and subtype_code = '$subtype_code' ";
                	}
                	if(Input::has('description'))
            		{
            			$where.=" and description = '$description' ";
                	}
                	if(Input::has('menu_position'))
            		{
            			$where.=" and menu_position = '$menu_position' ";
                	}
                	if(Input::has('goto_pgm_id'))
            		{
            			$where.=" and goto_pgm_id = '$goto_pgm_id' ";
                	}
                	if(Input::has('active_switch'))
            		{
            			$where.=" and active_switch = '$active_switch' ";
                	}

                $order="ORDER BY  gl_dept_number,  menu_level,  function_code, menu_position";	
              	$query=$select.$where.$order;
              	//echo $query;exit;
            	$menu_query_result = DB::select($query);
            	$menu_query_result_array = json_decode(json_encode($menu_query_result), true);
            	return View::make('socadm00.storetransmissionbroswer',compact('menu_query_result_array', 'dept_number'));
	}*/
  public function postmenuquery()
  {
      $work_data = $results = new StdClass();
      $this->gl_dept_number = Input::get('gl_dept_number');
      $this->menu_level = Input::get('menu_level');
      $this->function_code = strtoupper(Input::get('function_code'));
      $this->type_code = strtoupper(Input::get('type_code'));
      $this->subtype_code = strtoupper(Input::get('subtype_code'));
      $this->description = Input::get('description');
      $this->menu_position = Input::get('menu_position');
      $this->goto_pgm_id = Input::get('goto_pgm_id');
      $this->active_switch = strtoupper(Input::get('active_switch'));

      $paginate_array = array();
      $sql = "SELECT * FROM somencfg WHERE 1=1 ";

      if ($this->gl_dept_number != '') {
            $sql.=" AND gl_dept_number='" . $this->gl_dept_number . "' ";
            $paginate_array['gl_dept_number'] = $this->gl_dept_number;
        }
        if ($this->menu_level != '') {
            $sql.=" AND menu_level='" . $this->menu_level . "' ";
            $paginate_array['menu_level'] = $this->menu_level;
        }
        if ($this->function_code != '') {
            $sql.=" AND function_code='" . $this->function_code . "' ";
            $paginate_array['function_code'] = $this->function_code;
        }
        if ($this->type_code != '') {
            $sql.=" AND type_code='" . $this->type_code . "' ";
            $paginate_array['type_code'] = $this->type_code;
        }
        if ($this->subtype_code != '') {
            $sql.=" AND subtype_code='" . $this->subtype_code . "' ";
            $paginate_array['subtype_code'] = $this->subtype_code;
        }
        if ($this->description != '') {
            $sql.=" AND description='" . $this->description . "' ";
            $paginate_array['description'] = $this->description;
        }
        if ($this->menu_position != '') {
            $sql.=" AND menu_position='" . $this->menu_position . "' ";
            $paginate_array['menu_position'] = $this->menu_position;
        }
        if ($this->goto_pgm_id != '') {
            $sql.=" AND goto_pgm_id='" . $this->goto_pgm_id . "' ";
            $paginate_array['goto_pgm_id'] = $this->goto_pgm_id;
        }
        if ($this->active_switch != '') {
            $sql.=" AND active_switch='" . $this->active_switch . "' ";
            $paginate_array['active_switch'] = $this->active_switch;
        }
        $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Store Transfers meet Query criteria.  ");
            return Redirect::route('socadm00-storetransmission-query')->withInput();
        }

        $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('socadm00.storetransmissionbroswer')->with(array('data' => $work_data, 'pagination' => $pagination));
  }
	public function getStoreTransmissionBrowser()
	{
      return View::make('socadm00.menuquerymsg');
	}
	public function getStoreTransmissionAdd()
	{
		$function_code = DB::select('SELECT sofnctns.function_code, sofnctns.description FROM sofnctns WHERE sofnctns.active_switch = "Y" ORDER BY sofnctns.function_code');
		return View::make('socadm00.storetransmissionadd',compact('function_code'));	
	}
	public function getFunctionDesc()
	{
		$function_code = Input::get('function_code');
		$function_code_desc = DB::select('SELECT  sofnctns.description FROM sofnctns WHERE sofnctns.active_switch = "Y"  and sofnctns.function_code = "'.$function_code.'" ');
		$function_code_desc_array = json_decode(json_encode($function_code_desc), true);
		return $function_code_desc_array[0]['description'];
	}
	public function postStoreTransmissionAdd()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'gl_dept_number' =>'required',
                   'menu_level' =>'required',
                   'function_code' =>'required',
                   'type_code' =>'required',
                   'subtype_code' =>'required',
                   'description' =>'required',
                   'menu_position' =>'required',
                   'goto_pgm_id' =>'required',
                   'active_switch' =>'required'
                ),array(
                   'gl_dept_number.required' =>'GL Dept Number is Required',
                   'menu_level.required' =>'Menu Level is Required',
                   'function_code.required' =>'Function Code is Required',
                   'type_code.required' =>'Type Code is Required',
                   'subtype_code.required' =>'Sub-Type Code is Required',
                   'description.required' =>'Description is Required',
                   'menu_position.required' =>'Menu Position is Required',
                   'goto_pgm_id.required' =>'Goes to Program is Required',
                   'active_switch.required' =>'Acitve Switch is Required',
                ));
		if($validator->fails())
        {          
		    return Redirect::route('socadm00-storetransmission-add')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$max_entry_number = DB::select('SELECT entry_number FROM somencfg ORDER BY entry_number Desc limit 1');
		$max_entry_number_array = json_decode(json_encode($max_entry_number), true);
		$new_entry_number = $max_entry_number_array[0]['entry_number'] + 1;
		$menuAddSomencfg = new Somencfg;
        	$menuAddSomencfg->entry_number = $new_entry_number;
        	if(Input::has('gl_dept_number')) 
        	{
           		 $menuAddSomencfg->gl_dept_number = Input::get('gl_dept_number');
       		}
       		if(Input::has('menu_level')) 
        	{
           		 $menuAddSomencfg->menu_level = Input::get('menu_level');
       		}
       		if(Input::has('function_code')) 
        	{
           		 $menuAddSomencfg->function_code = Input::get('function_code');
       		}
       		if(Input::has('type_code')) 
        	{
           		 $menuAddSomencfg->type_code = strtoupper(Input::get('type_code'));
       		}
       		if(Input::has('subtype_code')) 
        	{
           		 $menuAddSomencfg->subtype_code = strtoupper(Input::get('subtype_code'));
       		}
       		if(Input::has('description')) 
        	{
           		 $menuAddSomencfg->description = Input::get('description');
       		}
       		if(Input::has('menu_position')) 
        	{
           		 $menuAddSomencfg->menu_position = Input::get('menu_position');
       		}
       		if(Input::has('goto_pgm_id')) 
        	{
           		 $menuAddSomencfg->goto_pgm_id = Input::get('goto_pgm_id');
       		}
       		if(Input::has('active_switch')) 
        	{
           		 $menuAddSomencfg->active_switch = strtoupper(Input::get('active_switch'));
       		}
       		$menuAddSomencfg->last_updated_by = 'SOCADM00';
       		$menuAddSomencfg->last_update = date("Y-m-d H:i:s");
       		
       		if($menuAddSomencfg->save())
	        {  
	          Session::flash('alert-success', 'Menu Option added Successfully!'); 
	          return Redirect::route('socadm00-storetransmission-add');
	        }
        }
	}
	public function menuQueryUpdate()
	{	
		//echo Input::get('function_code');exit;
		$function_code = DB::select('SELECT sofnctns.function_code, sofnctns.description FROM sofnctns WHERE sofnctns.active_switch = "Y" ORDER BY sofnctns.function_code');
		$entry_number = Input::get('entry_number');
		$fc = Input::get('function_code');//exit;
		$function_code_description = DB::select('SELECT sofnctns.function_code, sofnctns.description FROM sofnctns WHERE sofnctns.active_switch = "Y" and sofnctns.function_code = "'.$fc.'" ');
		$somencfg_data = DB::select('SELECT * from somencfg where entry_number = "'.$entry_number.'" ');
		$somencfg_data_array = json_decode(json_encode($somencfg_data), true);
		return View::make('socadm00.storetransmissionquery_update',compact('somencfg_data_array','function_code','function_code_description'));
	}
	public function postStoreTransConfAdd()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'type_code' =>'required',
                   'subtype_code' =>'required',
                   'description' =>'required',
                   'partition_code' =>'required',
                   'dept_number' =>'required',
                   'max_item_length' =>'required',
                   'max_qty' =>'required',
                   'vendor_acct_no' =>'required'
                ),array(
                   'type_code.required' =>'Type Code is Required',
                   'subtype_code.required' =>'Sub-Type Code is Required',
                   'description.required' =>'Banner Description is Required',
                   'partition_code.required' =>'Partition Code is Required',
                   'dept_number.required' =>'Department Number is Required',
                   'max_item_length.required' =>'Maximum Item Length is Required',
                   'max_qty.required' =>'Maximum Quantity is Required',
                   'vendor_acct_no.required' =>'Vendor Account Number is Required'
                ));
		if($validator->fails())
        {          
		    return Redirect::route('socadm00-storetransconf-add')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$max_entry_number = DB::select('SELECT entry_number FROM sotrncfg ORDER BY entry_number Desc limit 1');
		$max_entry_number_array = json_decode(json_encode($max_entry_number), true);
		$new_entry_number = $max_entry_number_array[0]['entry_number'] + 1;
        	$confAddSotrncfg = new Sotrncfg;
        	$confAddSotrncfg->entry_number = $new_entry_number;
        	if(Input::has('type_code')) 
        	{
           		 $confAddSotrncfg->type_code = Input::get('type_code');
       		}
       		if(Input::has('subtype_code')) 
        	{
           		 $confAddSotrncfg->subtype_code = strtoupper(Input::get('subtype_code'));
       		}
       		if(Input::has('description')) 
        	{
           		 $confAddSotrncfg->description = Input::get('description');
       		}
       		if(Input::has('partition_code')) 
        	{
           		 $confAddSotrncfg->partition_code = Input::get('partition_code');
       		}
       		if(Input::has('dept_number')) 
        	{
           		 $confAddSotrncfg->dept_number = Input::get('dept_number');
       		}
       		if(Input::has('max_item_length')) 
        	{
           		 $confAddSotrncfg->max_item_length = Input::get('max_item_length');
       		}
       		if(Input::has('max_qty')) 
        	{
           		 $confAddSotrncfg->max_qty = Input::get('max_qty');
       		}
       		if(Input::has('neg_qty_flag')) 
        	{
           		 $confAddSotrncfg->neg_qty_flag = Input::get('neg_qty_flag');
       		}
       		if(Input::has('zero_qty_flag')) 
        	{
           		 $confAddSotrncfg->zero_qty_flag = Input::get('zero_qty_flag');
       		}
       		if(Input::has('return_flag')) 
        	{
           		 $confAddSotrncfg->return_flag = Input::get('return_flag');
       		}
       		if(Input::has('history_flag')) 
        	{
           		 $confAddSotrncfg->history_flag = Input::get('history_flag');
       		}
       		if(Input::has('vendor_acct_no')) 
        	{
           		 $confAddSotrncfg->vendor_acct_no = Input::get('vendor_acct_no');
       		}
       		if(Input::has('active_switch')) 
        	{
           		 $confAddSotrncfg->active_switch = Input::get('active_switch');
       		}
       		$confAddSotrncfg->last_updated_by = 'SOCADM00';
       		$confAddSotrncfg->last_update = date("Y-m-d H:i:s");
       		
       		if($confAddSotrncfg->save())
	        {  
	          Session::flash('alert-success', 'Transmission Configuration added!'); 
	          return Redirect::route('socadm00-storetransconf-add');
	        }
        }
	}
	public function getStoreTransmissionUpdate()
	{
		$entry_number = Input::get('entry_number');
		$update_limit = DB::table('somencfg')
		            ->where('entry_number', $entry_number)
		            ->update(array(
		            	'gl_dept_number' => Input::get('gl_dept_number'),
		            	'menu_level' => Input::get('menu_level'),
		            	'function_code' => Input::get('function_code'),
		            	'type_code' => strtoupper(Input::get('type_code')),
		            	'subtype_code' => strtoupper(Input::get('subtype_code')),
		            	'description' => Input::get('description'),
		            	'menu_position' => Input::get('menu_position'),
		            	'goto_pgm_id' => Input::get('goto_pgm_id'),
		            	'active_switch' => strtoupper(Input::get('active_switch')),
		            	'last_updated_by' => 'SOCADM00',
		            	'last_update' => date("Y-m-d H:i:s")
		            	));
        if($update_limit)
        {
        	Session::flash('alert-success', 'This Menu Option has been Updated!'); 
          	return Redirect::route('socadm00-storetransmission-query');
        }
        else
        {
        	Session::flash('alert-danger', 'This Menu Option has not been Updated!'); 
          	return Redirect::route('socadm00-storetransmission-query');
        }
	}
  public function getStoreTransmissionUpdatemsg()
  {
    return View::make('socadm00.menuquerymsg');
  }
	public function getStoreTransConfQuery()
	{
		return View::make('socadm00.storetransconfquery');
	}
	/*public function postStoreTransConfQuery()
	{
		$type_code = strtoupper(Input::get('type_code'));
		$subtype_code = strtoupper(Input::get('subtype_code'));
		$description = Input::get('description');
		$partition_code = Input::get('partition_code');
		$dept_number = Input::get('dept_number');
		$max_item_length = Input::get('max_item_length');
		$max_qty = Input::get('max_qty');
		$neg_qty_flag = strtoupper(Input::get('neg_qty_flag'));
		$zero_qty_flag = strtoupper(Input::get('zero_qty_flag'));
		$return_flag = strtoupper(Input::get('return_flag'));
		$history_flag = strtoupper(Input::get('history_flag'));
		$vendor_acct_no = Input::get('vendor_acct_no');
		$active_switch = strtoupper(Input::get('active_switch'));


		$select = "select * from sotrncfg ";
        $where = 'where 1 = 1 ';
	            	if (Input::has('type_code'))
                  {
                      $where.= " and type_code = '$type_code' ";
                  }

                if (Input::has('subtype_code'))
                  {
                      $where.= " and subtype_code = '$subtype_code' ";
                  }

                if (Input::has('description'))
                  {
                      $where.= " and description = '$description' ";
                  }

                if (Input::has('partition_code'))
                  {
                      $where.= " and partition_code = '$partition_code' ";
                  }

                if (Input::has('dept_number'))
                  {
                      $where.= " and dept_number = '$dept_number' ";
                  }

                if (Input::has('max_item_length'))
                  {
                      $where.= " and max_item_length = '$max_item_length' ";
                  }

                if (Input::has('max_qty'))
                  {
                      $where.= " and max_qty = '$max_qty' ";
                  }

                if (Input::has('neg_qty_flag'))
                  {
                      $where.= " and neg_qty_flag = '$neg_qty_flag' ";
                  }

                if (Input::has('zero_qty_flag'))
                  {
                      $where.= " and zero_qty_flag = '$zero_qty_flag' ";
                  }

                if (Input::has('return_flag'))
                  {
                      $where.= " and return_flag = '$return_flag' ";
                  }

                if (Input::has('history_flag'))
                  {
                      $where.= " and history_flag = '$history_flag' ";
                  }

                if (Input::has('vendor_acct_no'))
                  {
                      $where.= " and vendor_acct_no = '$vendor_acct_no' ";
                  }

                if (Input::has('active_switch'))
                  {
                      $where.= " and active_switch = '$active_switch' ";
                  }

                  $data=array(
                          'type_code' => $type_code,
                          'subtype_code' => $subtype_code,
                          'description' => $description,
                          'partition_code' => $partition_code,
                          'dept_number' => $dept_number,
                          'max_item_length' => $max_item_length,
                          'max_qty' => $max_qty,
                          'neg_qty_flag' => $neg_qty_flag,
                          'zero_qty_flag' => $zero_qty_flag,
                          'return_flag' => $return_flag,
                          'history_flag' => $history_flag,
                          'vendor_acct_no' => $vendor_acct_no,
                          'active_switch'  => $active_switch
                          );

                //$order="ORDER BY  gl_dept_number,  menu_level,  function_code, menu_position";	
              	$query=$select.$where;
              		//echo $query;exit;
            	$confbrowser = DB::select($query);
            	$confbrowser_array = json_decode(json_encode($confbrowser), true);
            	return View::make('socadm00.storetransconfbrowser',compact('confbrowser_array','data'));
	}*/
  public function postStoreTransConfQuery()
  {
    $work_data = $results = new StdClass();
    $this->type_code = strtoupper(Input::get('type_code'));
    $this->subtype_code = strtoupper(Input::get('subtype_code'));
    $this->description = Input::get('description');
    $this->partition_code = Input::get('partition_code');
    $this->dept_number = Input::get('dept_number');
    $this->max_item_length = Input::get('max_item_length');
    $this->max_qty = Input::get('max_qty');
    $this->neg_qty_flag = strtoupper(Input::get('neg_qty_flag'));
    $this->zero_qty_flag = strtoupper(Input::get('zero_qty_flag'));
    $this->return_flag = strtoupper(Input::get('return_flag'));
    $this->history_flag = strtoupper(Input::get('history_flag'));
    $this->vendor_acct_no = Input::get('vendor_acct_no');
    $this->active_switch = strtoupper(Input::get('active_switch'));

    


    $paginate_array = array();
      $sql = "SELECT * FROM sotrncfg WHERE 1=1 ";

      if ($this->type_code != '') {
            $sql.=" AND type_code='" . $this->type_code . "' ";
            $paginate_array['type_code'] = $this->type_code;
        }
         if ($this->subtype_code != '') {
            $sql.=" AND subtype_code='" . $this->subtype_code . "' ";
            $paginate_array['subtype_code'] = $this->subtype_code;
        }
         if ($this->description != '') {
            $sql.=" AND description='" . $this->description . "' ";
            $paginate_array['description'] = $this->description;
        }
        if ($this->partition_code != '') {
            $sql.=" AND partition_code='" . $this->partition_code . "' ";
            $paginate_array['partition_code'] = $this->partition_code;
        }
        if ($this->dept_number != '') {
            $sql.=" AND dept_number='" . $this->dept_number . "' ";
            $paginate_array['dept_number'] = $this->dept_number;
        }
        if ($this->max_item_length != '') {
            $sql.=" AND max_item_length='" . $this->max_item_length . "' ";
            $paginate_array['max_item_length'] = $this->max_item_length;
        }
        if ($this->max_qty != '') {
            $sql.=" AND max_qty='" . $this->max_qty . "' ";
            $paginate_array['max_qty'] = $this->max_qty;
        }
        if ($this->neg_qty_flag != '') {
            $sql.=" AND neg_qty_flag='" . $this->neg_qty_flag . "' ";
            $paginate_array['neg_qty_flag'] = $this->neg_qty_flag;
        }
        if ($this->zero_qty_flag != '') {
            $sql.=" AND zero_qty_flag='" . $this->zero_qty_flag . "' ";
            $paginate_array['zero_qty_flag'] = $this->zero_qty_flag;
        }
        if ($this->return_flag != '') {
            $sql.=" AND return_flag='" . $this->return_flag . "' ";
            $paginate_array['return_flag'] = $this->return_flag;
        }
         if ($this->history_flag != '') {
            $sql.=" AND history_flag='" . $this->history_flag . "' ";
            $paginate_array['history_flag'] = $this->history_flag;
        }
        if ($this->vendor_acct_no != '') {
            $sql.=" AND vendor_acct_no='" . $this->vendor_acct_no . "' ";
            $paginate_array['vendor_acct_no'] = $this->vendor_acct_no;
        }
        if ($this->active_switch != '') {
            $sql.=" AND active_switch='" . $this->active_switch . "' ";
            $paginate_array['active_switch'] = $this->active_switch;
        }

        $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Store Transfers meet Query criteria.  ");
            return Redirect::route('socadm00-storetransconf-query')->withInput();
        }
        $work_data = $results;
        $pagination = Paginator::make($results, count($count_results), $perPage);
        $pagination->appends($paginate_array);
        $report_data=array(
                          'type_code' => $this->type_code,
                          'subtype_code' => $this->subtype_code,
                          'description' => $this->description,
                          'partition_code' => $this->partition_code,
                          'dept_number' => $this->dept_number,
                          'max_item_length' => $this->max_item_length,
                          'max_qty' => $this->max_qty,
                          'neg_qty_flag' =>$this->neg_qty_flag,
                          'zero_qty_flag' => $this->zero_qty_flag,
                          'return_flag' => $this->return_flag,
                          'history_flag' => $this->history_flag,
                          'vendor_acct_no' => $this->vendor_acct_no,
                          'active_switch'  => $this->active_switch
                          );
        return View::make('socadm00.storetransconfbrowser')->with(array('data' => $work_data, 'pagination' => $pagination,'report_data' => $report_data));
  }
	public function StoreTransConfUpdate()
	{
		$entry_number = Input::get('entry_number');
		$sotrncfg_data = DB::select('SELECT * from sotrncfg where entry_number = "'.$entry_number.'" ');
		$sotrncfg_data_array = json_decode(json_encode($sotrncfg_data), true);
		return View::make('socadm00.storetransconf_update',compact('sotrncfg_data_array'));
	}
	public function postStoreTransConfUpdate()
	{
		$entry_number = Input::get('entry_number');
		$update_limit = DB::table('sotrncfg')
		            ->where('entry_number', $entry_number)
		            ->update(array(
		            	'type_code' => strtoupper(Input::get('type_code')),
		            	'subtype_code' => strtoupper(Input::get('subtype_code')),
		            	'description' => Input::get('description'),
		            	'partition_code' => Input::get('partition_code'),
		            	'dept_number' => Input::get('dept_number'),
		            	'max_item_length' => Input::get('max_item_length'),
		            	'max_qty' => Input::get('max_qty'),
		            	'neg_qty_flag' => strtoupper(Input::get('neg_qty_flag')),
		            	'zero_qty_flag' => strtoupper(Input::get('zero_qty_flag')),
		            	'return_flag' => strtoupper(Input::get('return_flag')),
		            	'history_flag' => strtoupper(Input::get('history_flag')),
		            	'vendor_acct_no' => Input::get('vendor_acct_no'),
		            	'active_switch' => strtoupper(Input::get('active_switch')),
		            	'last_updated_by' => 'SOCADM00',
		            	'last_update' => date("Y-m-d H:i:s")
		            	));
        if($update_limit)
        {
        	Session::flash('alert-success', 'This Transmission Configuration has been Updated!'); 
          	return Redirect::route('socadm00-storetransconf-query');
        }
        else
        {
        	Session::flash('alert-danger', 'This Transmission Configuration has not been Updated!'); 
          	return Redirect::route('socadm00-storetransconf-query');
        }
	}
	public function getStoreTransConfBrowser()
	{
		$data=array('tabcol1' => 'Active Switch',
						'tabcol2' => 'Type Code',
						'tabcol3' => 'Sub-Type Code',
						'tabcol4' => 'Description',
						'tabcol5' => 'Partition Code',
						'tabcol6' => 'Dep Number',
						'page_heading'  => 'Store Transmission Configuration Manager');
		return View::make('socadm00.storetransconfbrowser')->with('data',$data);
	}
  public function getStoreTransConfBrowsermsg()
  {
    return View::make('socadm00.configurationsquerymsg');
  }
	Public function getStoreTransConfAdd()
	{
	return View::make('socadm00.storetransconfadd');	
	}
	Public function getStoreTransConfUpdate()
	{
	   return View::make('socadm00.configurationsquerymsg');
	}
  public function getStoreTransConfQueryMsg()
  {
    return View::make('socadm00.functionsquerymsg');
  }
	public function getStoreTransFunQuery()
	{
		return View::make('socadm00.storetransfunquery');
	}
  public function getStoreDexCommidsmsg()
  {
      return View::make('socadm00.dex_commidsquerymsg');
  }
  public function StoreScheduleQueryMsg()
  {
      return View::make('socadm00.schedulesquerymsg');
  }
  public function getStoreOldMenuQueryMsg()
  {
      return View::make('socadm00.old_menus_querymsg');
  }
  public function postStoreTransFunQuery()
  {
    $work_data = $results = new StdClass();

    $this->function_code = Input::get('function_code');
    $this->description = Input::get('description');
    $this->active_switch = Input::get('active_switch');

    $paginate_array = array();
      $sql = "SELECT * FROM sofnctns WHERE 1=1 ";

      if ($this->function_code != '') {
            $sql.=" AND function_code='" . $this->function_code . "' ";
            $paginate_array['function_code'] = $this->function_code;
        }
        if ($this->description != '') {
            $sql.=" AND description='" . $this->description . "' ";
            $paginate_array['description'] = $this->description;
        }
        if ($this->active_switch != '') {
            $sql.=" AND active_switch='" . $this->active_switch . "' ";
            $paginate_array['active_switch'] = $this->active_switch;
        }
       
        $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Menu Options meet Query criteria.  ");
            return Redirect::route('socadm00-storetransfun-query')->withInput();
        }
        $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('socadm00.storetransfunbrowser')->with(array('data' => $work_data, 'pagination' => $pagination));          


  }
	public function getStoreTransFunBrowser()
	{
		$data=array('tabcol1' => 'Function Code',
						'tabcol2' => 'Description',
						'tabcol3' => 'Active Switch',
						
						'page_heading'  => 'Store Transmission Configuration Manager');
		return View::make('socadm00.storetransfunbrowser')->with('data',$data);
	}
	Public function getStoreTransFunAdd()
	{
	return View::make('socadm00.storetransfunadd');	
	}
  public function postStoreTransFunAdd()
  {
    $validator = Validator::make(Input::all(),
            array(
                   'function_code' =>'required',
                   'description' =>'required',
                   'active_switch' =>'required'
                ),array(
                   'function_code.required' =>'Function Code is Required',
                   'description.required' =>'Description is Required',
                   'active_switch.required' =>'Active Switch is Required'
                ));
    if($validator->fails())
        {          
        return Redirect::route('socadm00-storetransfun-add')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
          $max_entry_number = DB::select('SELECT entry_number FROM sofnctns ORDER BY entry_number Desc limit 1');
            $max_entry_number_array = json_decode(json_encode($max_entry_number), true);
            $new_entry_number = $max_entry_number_array[0]['entry_number'] + 1;
            $funcAddSofnctns = new Sofnctns;
            $funcAddSofnctns->entry_number = $new_entry_number;
            if(Input::has('function_code')) 
            {
                 $funcAddSofnctns->function_code = strtoupper(Input::get('function_code'));
            }
            if(Input::has('description')) 
            {
                 $funcAddSofnctns->description = Input::get('description');
            }
            if(Input::has('active_switch')) 
            {
                 $funcAddSofnctns->active_switch = strtoupper(Input::get('active_switch'));
            }
            $funcAddSofnctns->last_updated_by = 'SOCADM00';
            $funcAddSofnctns->last_update = date("Y-m-d H:i:s");
            if($funcAddSofnctns->save())
            {  
              Session::flash('alert-success', 'Menu Option added Successfully!'); 
              return Redirect::route('socadm00-storetransfun-add');
            }
        }  
  }
  public function StoreTransFunUpdate()
  {
      $entry_number = Input::get('entry_number');
      $sofnctns_query = DB::select('SELECT * FROM sofnctns where entry_number = "'.$entry_number.'" ');
            $sofnctns_query_array = json_decode(json_encode($sofnctns_query), true);
      return View::make('socadm00.storetransfunupdate',compact('sofnctns_query_array'));  
  }
  public function postStoreTransFunUpdate()
  {
    $entry_number = Input::get('entry_number');
      $update_limit = DB::table('sofnctns')
                ->where('entry_number', $entry_number)
                ->update(array(
                  'function_code' =>strtoupper(Input::get('function_code')),
                  'description' => Input::get('description'),
                  'active_switch' => strtoupper(Input::get('active_switch')),
                  'last_updated_by' => 'SOCADM00',
                  'last_update' => date("Y-m-d H:i:s")
                  ));
        if($update_limit)
        {
          Session::flash('alert-success', 'This Menu Option has been Updated!'); 
            return Redirect::route('socadm00-storetransfun-query');
        }
        else
        {
          Session::flash('alert-danger', 'This Menu Option has not been Updated!'); 
            return Redirect::route('socadm00-storetransfun-query');
        }
  }
	Public function getStoreTransFunUpdate()
	{
	return View::make('socadm00.storetransfunupdate');	
	}
	public function getStoreOldMenuQuery()
	{
		return View::make('socadm00.storeoldmenuquery');
	}
  public function postStoreOldMenuQuery()
  {

    $work_data = $results = new StdClass();

    $this->type_code = Input::get('type_code');
    $this->subtype_code = Input::get('subtype_code');
    $this->description = Input::get('description');
    $this->menu_position = Input::get('menu_position');
    $this->goto_pgm_id = Input::get('goto_pgm_id');
    $this->active_switch = Input::get('active_switch');

    $paginate_array = array();
      $sql = "SELECT * FROM somenu WHERE 1=1 ";

      if ($this->type_code != '') {
          $sql.=" AND type_code='" . $this->type_code . "' ";
          $paginate_array['type_code'] = $this->type_code;
      }
      if ($this->subtype_code != '') {
          $sql.=" AND subtype_code='" . $this->subtype_code . "' ";
          $paginate_array['subtype_code'] = $this->subtype_code;
      }
      if ($this->description != '') {
          $sql.=" AND description='" . $this->description . "' ";
          $paginate_array['description'] = $this->description;
      }
      if ($this->menu_position != '') {
          $sql.=" AND menu_position='" . $this->menu_position . "' ";
          $paginate_array['menu_position'] = $this->menu_position;
      }
      if ($this->goto_pgm_id != '') {
          $sql.=" AND goto_pgm_id='" . $this->goto_pgm_id . "' ";
          $paginate_array['goto_pgm_id'] = $this->goto_pgm_id;
      }
      if ($this->active_switch != '') {
          $sql.=" AND active_switch='" . $this->active_switch . "' ";
          $paginate_array['active_switch'] = $this->active_switch;
      }
      $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Store Transfers meet Query criteria.  ");
            return Redirect::route('socadm00-storeold-menu-query')->withInput();
        }

      $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('socadm00.storeoldmenubrowser')->with(array('data' => $work_data, 'pagination' => $pagination));
      }


	public function getStoreOldMenuBrowser()
	{
		// $data=array('tabcol1' => 'Active Switch',
		// 				'tabcol2' => 'Type Code',
		// 				'tabcol3' => 'Sub-Type Code',
		// 				'tabcol4' => 'Menu Item Description',
		// 				'tabcol5' => 'Goto Pgm ID',	
		// 				'page_heading'  => 'Store Transmission Configuration Manager');
		// return View::make('socadm00.storeoldmenubrowser')->with('data',$data);
	}

	public function getStoreOldMenuAdd()
	{
		return View::make('socadm00.storeoldmenuadd');
	}
  public function postStoreOldMenuAdd()
  {
    $validator = Validator::make(Input::all(),
            array(
                   'type_code' =>'required',
                   'subtype_code' =>'required',
                   'description' =>'required',
                   'menu_position' =>'required',
                   'goto_pgm_id' =>'required',
                   'active_switch' =>'required'
                ),array(
                   'type_code.required' =>'Type Code is Required',
                   'subtype_code.required' =>'Sub-Type Code is Required',
                   'description.required' =>'Menu Item Description is Required',
                   'menu_position.required' =>'Menu Position is Required',
                   'goto_pgm_id.required' =>'Goes to Program is Required',
                   'active_switch.required' =>'Active Switch is Required'
                ));
    if($validator->fails())
        {          
        return Redirect::route('socadm00-storeold-menu-add')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
            $max_entry_number = DB::select('SELECT entry_number FROM somenu ORDER BY entry_number Desc limit 1');
          $max_entry_number_array = json_decode(json_encode($max_entry_number), true);
          $new_entry_number = $max_entry_number_array[0]['entry_number'] + 1;
          $oldMenuAddSomenu = new Somenu;
          $oldMenuAddSomenu->entry_number = $new_entry_number;
          if(Input::has('type_code')) 
          {
               $oldMenuAddSomenu->type_code = strtoupper(Input::get('type_code'));
          }
          if(Input::has('subtype_code')) 
          {
               $oldMenuAddSomenu->subtype_code = strtoupper(Input::get('subtype_code'));
          }
          if(Input::has('description')) 
          {
               $oldMenuAddSomenu->description = Input::get('description');
          }
          if(Input::has('menu_position')) 
          {
               $oldMenuAddSomenu->menu_position = Input::get('menu_position');
          }
          if(Input::has('goto_pgm_id')) 
          {
               $oldMenuAddSomenu->goto_pgm_id = Input::get('goto_pgm_id');
          }
          if(Input::has('active_switch')) 
          {
               $oldMenuAddSomenu->active_switch = strtoupper(Input::get('active_switch'));
          }
          $oldMenuAddSomenu->last_updated_by = 'SOCADM00';
          $oldMenuAddSomenu->last_update = date("Y-m-d H:i:s");
          
          if($oldMenuAddSomenu->save())
          {  
            Session::flash('alert-success', 'Menu Option added Successfully!'); 
            return Redirect::route('socadm00-storeold-menu-add');
          }
        }
  }
	public function getStoreOldMenuUpdate()
	{
    $entry_number = Input::get('entry_number');
    //echo $entry_number;exit;
    $somenu_query = DB::select('SELECT * FROM somenu where entry_number = "'.$entry_number.'" ');
            $somenu_query_array = json_decode(json_encode($somenu_query), true);
		return View::make('socadm00.storeoldmenuupdate',compact('somenu_query_array'));
	}
  public function postStoreOldMenuUpdate()
  {
    $entry_number = Input::get('entry_number');
    $update_limit = DB::table('somenu')
                ->where('entry_number', $entry_number)
                ->update(array(
                  'type_code' => strtoupper(Input::get('type_code')),
                  'subtype_code' => strtoupper(Input::get('subtype_code')),
                  'description' => Input::get('description'),
                  'menu_position' => Input::get('menu_position'),
                  'goto_pgm_id' => Input::get('goto_pgm_id'),
                  'active_switch' => strtoupper(Input::get('active_switch')),
                  'last_updated_by' => 'SOCADM00',
                  'last_update' => date("Y-m-d H:i:s")
                  ));
        if($update_limit)
        {
          Session::flash('alert-success', 'This Menu Option has been Updated!'); 
            return Redirect::route('socadm00-storetransmission-query');
        }
        else
        {
          Session::flash('alert-danger', 'This Menu Option has not been Updated!'); 
            return Redirect::route('socadm00-storetransmission-query');
        }
  }
	public function getQtyLimits()
	{
		
			$results = DB::select('SELECT type_code,name,gl_dept_number FROM sodept WHERE active_switch = "Y" ORDER BY gl_dept_number');
			$departments = json_decode(json_encode($results), true); 
			return View::make('socadm00.qtylimits',compact('departments'));
		
	}
	public function postQtyLimits()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'dept_name_number' =>'required'
                 ),array(
                   'dept_name_number.required' =>'Department is Required'
                  ));
		if($validator->fails())
        {          
		    return Redirect::route('socadm00-Qtylimits')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {	
        	$department = Input::get('dept_name_number');
        	
        	$department_name = DB::select('SELECT type_code,name,gl_dept_number FROM sodept WHERE type_code = "'.$department.'" and active_switch = "Y" ORDER BY gl_dept_number' );
			$department_name_array = json_decode(json_encode($department_name), true); 
        	$current_limit = DB::select('select entry_number, description, max_qty from sotrncfg where type_code = "'.$department.'" and subtype_code = "00" ');
        	$current_limit_array = json_decode(json_encode($current_limit), true);
			return View::make('socadm00.epostQtyLimits',compact('department_name_array','current_limit_array'));
		}
	}
	public function postQtyLimitsUpdate()
	{
		$inputdata = Input::all();
		$new_limit = Input::get('new_limit');
		$entry_number = Input::get('entry_number');
		$update_limit = DB::table('sotrncfg')
		            ->where('entry_number', $entry_number)
		            ->update(array('max_qty' => $new_limit));
        if($update_limit)
        {
        	Session::flash('alert-success', 'Limit Updated!'); 
          	return Redirect::route('socadm00-Qtylimits');
        }
        else
        {
        	Session::flash('alert-danger', 'Limit Not Updated!'); 
          	return Redirect::route('socadm00-Qtylimits');
        }
       
	}
	public function getStoreDexCommidsQuery()
	{
		return View::make('socadm00.storedexcommidsquery');
	}
  public function postStoreDexCommidsQuery()
  {
    $work_data = $results = new StdClass();

    $this->vendor_number = Input::get('vendor_number');
    $this->duns_number = Input::get('duns_number');
    $this->commid = Input::get('commid');
    $this->type_code = Input::get('type_code');
    $this->active_switch = strtoupper(Input::get('active_switch'));
    
    $paginate_array = array();
      $sql = "SELECT * FROM sodsdvnd WHERE 1=1 ";
    

                if ($this->vendor_number != '') {
                    $sql.=" AND vendor_number='" . $this->vendor_number . "' ";
                    $paginate_array['vendor_number'] = $this->vendor_number;
                }
                if ($this->duns_number != '') {
                    $sql.=" AND duns_number='" . $this->duns_number . "' ";
                    $paginate_array['duns_number'] = $this->duns_number;
                }
                if ($this->commid != '') {
                    $sql.=" AND commid='" . $this->commid . "' ";
                    $paginate_array['commid'] = $this->commid;
                }
                if ($this->type_code != '') {
                    $sql.=" AND type_code='" . $this->type_code . "' ";
                    $paginate_array['type_code'] = $this->type_code;
                }
                if ($this->active_switch != '') {
                    $sql.=" AND active_switch='" . $this->active_switch . "' ";
                    $paginate_array['active_switch'] = $this->active_switch;
                }

                $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Store Transfers meet Query criteria.  ");
            return Redirect::route('socadm00-storeDex-commids-query')->withInput();
        }

    $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('socadm00.storedexcommidsbrowser')->with(array('data' => $work_data, 'pagination' => $pagination));

  }
  public function StoreDexCommidsUpdate()
  {
    $vendor_number = Input::get('vendor_number');
    $sodsdvnd_query = DB::select('select * from sodsdvnd where vendor_number = "'.$vendor_number.'" ');
    $sodsdvnd_query_array = json_decode(json_encode($sodsdvnd_query), true);
    return View::make('socadm00.storedexcommidsupdate',compact('sodsdvnd_query_array'));
  }
  public function postStoreDexCommidsUpdate()
  {
    $vendor_number = Input::get('vendor_number');
      $update_limit = DB::table('sodsdvnd')
                ->where('vendor_number', $vendor_number)
                ->update(array(
                  'duns_number' => Input::get('duns_number'),
                  'commid' => Input::get('commid'),
                  'type_code' => strtoupper(Input::get('type_code')),
                  'active_switch' => strtoupper(Input::get('active_switch')),
                  'last_updated_by' => 'SODSDVIU',
                  'last_update' => date("Y-m-d H:i:s")
                  ));
        if($update_limit)
        {
          Session::flash('alert-success', 'This Vendor\'s DUN has been Updated!'); 
            return Redirect::route('socadm00-storeDex-commids-query');
        }
        else
        {
          Session::flash('alert-danger', 'This Vendor\'s DUN has been Updated!'); 
            return Redirect::route('socadm00-storeDex-commids-query');
        }
  }
	public function getStoreDexCommidsBrowser()
	{
		$data=array('tabcol1' => 'Vender Number',
						'tabcol2' => 'Name',
						'tabcol3' => 'Commid',
						'tabcol4' => 'Active Switch',
							
						'page_heading'  => 'DEX Commid and DUN Assignment');
		return View::make('socadm00.storedexcommidsbrowser')->with('data',$data);
	}

	
	public function getStoreDexCommidsUpdate()
	{
		return View::make('socadm00.storedexcommidsupadte');
	}

	public function getStoreScheduleQuery()
	{
		return View::make('socadm00.storeschedulequery');
	}
  public function postStoreScheduleQuery()
  {

    $work_data = $results = new StdClass();

    $this->gl_dept_number = Input::get('gl_dept_number');
    $this->type_code = Input::get('type_code');
    $this->subtype_code = Input::get('subtype_code');
    $this->vendor_number = Input::get('vendor_number');
    $this->partition_code = Input::get('partition_code');
    $this->deadline = Input::get('deadline');
    $this->resume_time = Input::get('resume_time');
    $this->description = Input::get('description');
    $this->sun_sw = Input::get('sun_sw');
    $this->mon_sw = Input::get('mon_sw');
    $this->tue_sw = Input::get('tue_sw');
    $this->wed_sw = Input::get('wed_sw');
    $this->thr_sw = Input::get('thr_sw');
    $this->fri_sw = Input::get('fri_sw');
    $this->sat_sw = Input::get('sat_sw');
    $this->hard_deadline_sw = Input::get('hard_deadline_sw');
    $this->reminder_sw = Input::get('reminder_sw');
    $this->days_history = Input::get('days_history');
    $this->active_sw = Input::get('active_sw');
    $this->last_updated_by = Input::get('last_updated_by');
    $this->last_update = Input::get('last_update');

    $paginate_array = array();
      $sql = "SELECT * FROM sovndschd WHERE 1=1 ";

    if ($this->gl_dept_number != '') {
          $sql.=" AND gl_dept_number='" . $this->gl_dept_number . "' ";
          $paginate_array['gl_dept_number'] = $this->gl_dept_number;
      }
    if ($this->type_code != '') {
        $sql.=" AND type_code='" . $this->type_code . "' ";
        $paginate_array['type_code'] = $this->type_code;
    }
    if ($this->subtype_code != '') {
        $sql.=" AND subtype_code='" . $this->subtype_code . "' ";
        $paginate_array['subtype_code'] = $this->subtype_code;
    }
    if ($this->vendor_number != '') {
        $sql.=" AND vendor_number='" . $this->vendor_number . "' ";
        $paginate_array['vendor_number'] = $this->vendor_number;
    }
    if ($this->partition_code != '') {
        $sql.=" AND partition_code='" . $this->partition_code . "' ";
        $paginate_array['partition_code'] = $this->partition_code;
    }
    if ($this->deadline != '') {
        $sql.=" AND deadline='" . $this->deadline . "' ";
        $paginate_array['deadline'] = $this->deadline;
    }
    if ($this->resume_time != '') {
        $sql.=" AND resume_time='" . $this->resume_time . "' ";
        $paginate_array['resume_time'] = $this->resume_time;
    }
    if ($this->description != '') {
        $sql.=" AND description='" . $this->description . "' ";
        $paginate_array['description'] = $this->description;
    }
    if ($this->sun_sw != '') {
        $sql.=" AND sun_sw='" . $this->sun_sw . "' ";
        $paginate_array['sun_sw'] = $this->sun_sw;
    }
    if ($this->mon_sw != '') {
        $sql.=" AND mon_sw='" . $this->mon_sw . "' ";
        $paginate_array['mon_sw'] = $this->mon_sw;
    }
    if ($this->tue_sw != '') {
        $sql.=" AND tue_sw='" . $this->tue_sw . "' ";
        $paginate_array['tue_sw'] = $this->tue_sw;
    }
    if ($this->wed_sw != '') {
        $sql.=" AND wed_sw='" . $this->wed_sw . "' ";
        $paginate_array['wed_sw'] = $this->wed_sw;
    }
    if ($this->thr_sw != '') {
        $sql.=" AND thr_sw='" . $this->thr_sw . "' ";
        $paginate_array['thr_sw'] = $this->thr_sw;
    }
    if ($this->fri_sw != '') {
        $sql.=" AND fri_sw='" . $this->fri_sw . "' ";
        $paginate_array['fri_sw'] = $this->fri_sw;
    }
    if ($this->sat_sw != '') {
        $sql.=" AND sat_sw='" . $this->sat_sw . "' ";
        $paginate_array['sat_sw'] = $this->sat_sw;
    }
    if ($this->hard_deadline_sw != '') {
        $sql.=" AND hard_deadline_sw='" . $this->hard_deadline_sw . "' ";
        $paginate_array['hard_deadline_sw'] = $this->hard_deadline_sw;
    }
    if ($this->reminder_sw != '') {
        $sql.=" AND reminder_sw='" . $this->reminder_sw . "' ";
        $paginate_array['reminder_sw'] = $this->reminder_sw;
    }
    if ($this->days_history != '') {
        $sql.=" AND days_history='" . $this->days_history . "' ";
        $paginate_array['days_history'] = $this->days_history;
    }
    if ($this->active_sw != '') {
        $sql.=" AND active_sw='" . $this->active_sw . "' ";
        $paginate_array['active_sw'] = $this->active_sw;
    }
    if ($this->last_updated_by != '') {
        $sql.=" AND last_updated_by='" . $this->last_updated_by . "' ";
        $paginate_array['last_updated_by'] = $this->last_updated_by;
    }

   $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));


        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Store Schedules meet Query criteria. ");
            return Redirect::route('socadm00-schedule-query')->withInput();
        }

    $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('socadm00.storeschedulebrowser')->with(array('data' => $work_data, 'pagination' => $pagination));
            
  }
	public function getStoreScheduleBrowser()
	{
		$data=array('tabcol1' => 'GL',
						'tabcol2' => 'Order',
						'tabcol3' => 'Vender Number',
						'tabcol4' => 'Description',
						'tabcol5' => 'Sun',
						'tabcol6' => 'Mon',
						'tabcol7' => 'Tue',
						'tabcol8' => 'Wed',
						'tabcol9' => 'Thr',
						'tabcol0' => 'Fri',
						'tabcol11' => 'Sat',
						'page_heading'  => 'Store Order Schedule Configuration Manager');
		return View::make('socadm00.storeschedulebrowser')->with('data',$data);
	}

	public function getStoreScheduleAdd()
	{
		return View::make('socadm00.storescheduleadd');
	}
  public function postStoreScheduleAdd()
  {
    $validator = Validator::make(Input::all(),
            array(
                   'gl_dept_number' =>'required',
                   'type_code' =>'required',
                   'subtype_code' =>'required',
                   'vendor_number' =>'required',
                   'partition_code' =>'required',
                   'deadline' =>'required',
                   'resume_time' =>'required',
                   'description' =>'required',
                   'days_history' =>'required'
                ),array(
                   'gl_dept_number.required' =>'GL Dept Number is Required',
                   'type_code.required' =>'Menu Level is Required',
                   'subtype_code.required' =>'Function Code is Required',
                   'vendor_number.required' =>'Type Code is Required',
                   'partition_code.required' =>'Sub-Type Code is Required',
                   'deadline.required' =>'Order Deadline is Required',
                   'resume_time.required' =>'Resume Time is Required',
                   'description.required' =>'Vendor Description is Required',
                   'days_history.required' =>'Day History is Required',
                ));
    if($validator->fails())
        {          
        return Redirect::route('socadm00-schedule-add')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
          $scheduleAddSovndschd = new Sovndschd;
          if(Input::has('gl_dept_number')) 
          {
               $scheduleAddSovndschd->gl_dept_number = Input::get('gl_dept_number');
          }
          if(Input::has('type_code')) 
          {
               $scheduleAddSovndschd->type_code = strtoupper(Input::get('type_code'));
          }
          if(Input::has('subtype_code')) 
          {
               $scheduleAddSovndschd->subtype_code = strtoupper(Input::get('subtype_code'));
          }
          if(Input::has('vendor_number')) 
          {
               $scheduleAddSovndschd->vendor_number = Input::get('vendor_number');
          }
          if(Input::has('partition_code')) 
          {
               $scheduleAddSovndschd->partition_code = strtoupper(Input::get('partition_code'));
          }
          if(Input::has('deadline')) 
          {
               $scheduleAddSovndschd->deadline = Input::get('deadline');
          }
          if(Input::has('resume_time')) 
          {
               $scheduleAddSovndschd->resume_time = Input::get('resume_time');
          }
          if(Input::has('description')) 
          {
               $scheduleAddSovndschd->description = Input::get('description');
          }
          if(Input::has('sun_sw')) 
          {
               $scheduleAddSovndschd->sun_sw = strtoupper(Input::get('sun_sw'));
          }
          if(Input::has('mon_sw')) 
          {
               $scheduleAddSovndschd->mon_sw = strtoupper(Input::get('mon_sw'));
          }
          if(Input::has('tue_sw')) 
          {
               $scheduleAddSovndschd->tue_sw = strtoupper(Input::get('tue_sw'));
          }
          if(Input::has('wed_sw')) 
          {
               $scheduleAddSovndschd->wed_sw = strtoupper(Input::get('wed_sw'));
          }
          if(Input::has('thr_sw')) 
          {
               $scheduleAddSovndschd->thr_sw = strtoupper(Input::get('thr_sw'));
          }
          if(Input::has('fri_sw')) 
          {
               $scheduleAddSovndschd->fri_sw = strtoupper(Input::get('fri_sw'));
          }
          if(Input::has('sat_sw')) 
          {
               $scheduleAddSovndschd->sat_sw = strtoupper(Input::get('sat_sw'));
          }
          if(Input::has('hard_deadline_sw')) 
          {
               $scheduleAddSovndschd->hard_deadline_sw = strtoupper(Input::get('hard_deadline_sw'));
          }
          if(Input::has('reminder_sw')) 
          {
               $scheduleAddSovndschd->reminder_sw = strtoupper(Input::get('reminder_sw'));
          }
          if(Input::has('days_history')) 
          {
               $scheduleAddSovndschd->days_history = Input::get('days_history');
          }
          if(Input::has('active_sw')) 
          {
               $scheduleAddSovndschd->active_sw = strtoupper(Input::get('active_sw'));
          }
          $scheduleAddSovndschd->last_updated_by = 'SOCADM00';
          $scheduleAddSovndschd->last_update = date("Y-m-d H:i:s");
          
          if($scheduleAddSovndschd->save())
          {  
            Session::flash('alert-success', 'Store Schedule added!'); 
            return Redirect::route('socadm00-schedule-add');
          }
        }
  }	
	public function getStoreScheduleUpdate()
	{
		return View::make('socadm00.storescheduleupadte');
	}
  public function StoreScheduleUpdateForm()
  {
    $id = Input::get('id');
    //echo 'select * from sovndschd where id = "'.$id.'" ';exit;
    $sovndschd_query = DB::select('select * from sovndschd where id = "'.$id.'" ');
    $sovndschd_query_array = json_decode(json_encode($sovndschd_query), true);
    return View::make('socadm00.storescheduleupdate',compact('sovndschd_query_array'));
  }
  public function postStoreScheduleUpdateForm()
  {
    // $inputdata = Input::all();
    // echo '<pre>';print_r($inputdata);exit;
    $id = Input::get('id');
    $update_limit = DB::table('sovndschd')
                ->where('id', $id)
                ->update(array(
                  'gl_dept_number' => Input::get('gl_dept_number'),
                  'type_code' => strtoupper(Input::get('type_code')),
                  'subtype_code' => strtoupper(Input::get('subtype_code')),
                  'vendor_number' => Input::get('vendor_number'),
                  'partition_code' => Input::get('partition_code'),
                  'deadline' => Input::get('deadline'),
                  'resume_time' => Input::get('resume_time'),
                  'description' => Input::get('description'),
                  'sun_sw' => strtoupper(Input::get('sun_sw')),
                  'mon_sw' => strtoupper(Input::get('mon_sw')),
                  'tue_sw' => strtoupper(Input::get('tue_sw')),
                  'wed_sw' => strtoupper(Input::get('wed_sw')),
                  'thr_sw' => strtoupper(Input::get('thr_sw')),
                  'fri_sw' => strtoupper(Input::get('fri_sw')),
                  'sat_sw' => strtoupper(Input::get('sat_sw')),
                  'hard_deadline_sw' => strtoupper(Input::get('hard_deadline_sw')),
                  'reminder_sw' => strtoupper(Input::get('reminder_sw')),
                  'days_history' => Input::get('days_history'),
                  'active_sw' => strtoupper(Input::get('active_sw')),
                  'last_updated_by' => 'SOCADM00',
                  'last_update' => date("Y-m-d H:i:s")
                  ));
        if($update_limit)
        {
          Session::flash('alert-success', 'This Store Order Schedule has been Updated!'); 
            return Redirect::route('socadm00-schedule-query');
        }
        else
        {
          Session::flash('alert-danger', 'This Store Order Schedule has not been Updated!'); 
            return Redirect::route('socadm00-schedule-query');
        }
  }
}
