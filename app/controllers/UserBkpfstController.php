<?php 

class UserBkpfstController extends BaseController
{

	public function getReturnHome()
	{
		return View::make('bkpfst.homepage');
	}
	
	public function getBookkeeper1st()
	{
		return View::make('bkpfst.bookkeeper1st');
	}
	public function getbook1chgquery()
    {
    	return View::make('bkpfst.book1chgquery');
    }
     public function postbook1chglist()
    {
       
        $regtype = Input::get('regtype');
        $regnumber = Input::get('regnumber');
        //echo 'select * from registers where reg_num = "'.$regnumber.'" and reg_type = "'.$regtype.'" ';exit;
        $searchresult = DB::select('select * from registers order by reg_num'); 
        //$searchresult = DB::select('select * from registers where reg_num = '.$regnumber.' and reg_type = "'.$regtype.'" ');
        if(Input::has('regtype')) 
        {    
            $searchresult = DB::select('select * from registers where reg_type = "'.$regtype.'" ');
        }
        if(Input::has('regnumber')) 
        {    
            $searchresult = DB::select('select * from registers where reg_num = "'.$regnumber.'" ');
        }
        if(Input::has('regnumber') && Input::has('regtype')) 
        {    
            $searchresult = DB::select('select * from registers where reg_num = "'.$regnumber.'" and reg_type = "'.$regtype.'"');
        }
        return View::make('bkpfst.bookchglist',compact('searchresult','regnumber','regtype'));
    }   
}