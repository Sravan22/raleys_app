<?php

/**
 * 
 */
class BeginloanController extends BaseController {

public function beginLoanRegister()
{
	
	$inserteddate = Input::get('dateofreg'); $reg_num=Input::get('reg_number');
	$seldate=$this->convertDateToMDY(Input::get('dateofreg'));
	$data = array('reg_number' => $reg_num, 'reg_date' => $seldate);
	$approvalsOk = ''; $querymake='update';

	$approvalsOk=$this->approvalsOk2($inserteddate, 'bookkeeperregister'); //frist check date is approved or not
	/*if(!$approvalsOk)
	{
		$registers_num=DB::select("select * from registers order by 1");	
        return View::make('mktmgr.bookkeeperregister', compact('registers_num'));
	} */

    $beginloadtabdata = array();
	

      $beginloandata = DB::select('select reg_num, food_stamps, ones/100 as ones, fives/100 as fives, tens/100 as tens, 
								   twenties/100 as twenties, rolled_coins/100 as rolled_coins, quarters/100 as quarters,
								   nickles/100 as nickles, dimes/100 as dimes, pennies/100 as pennies, misc/100 as misc, 
								   bl_total/100 as bl_total, entry_date from begin_loan where 
								   begin_loan.reg_num = "'.$reg_num.'" and begin_loan.entry_date = "'.$inserteddate.'"');

      if(count($beginloandata) <= 0)
      {
      	 $chklock=$this->file_lock($inserteddate);
      	 if($chklock == false) // given date is not locked then insert sample data
      	 { $querymake='insert';
      	 	/*$sql=DB::select('INSERT INTO begin_loan(reg_num, food_stamps, ones, fives, tens, twenties, rolled_coins, quarters,nickles, dimes, pennies, misc, bl_total, entry_date)VALUES("'.$reg_num.'",0,0,0,0,0,0,0,0,0,0,0,0,"'.$inserteddate.'")');*/
      	 	$beginloadtabdata['food_stamps']='0'; $beginloadtabdata['ones']='0.00'; $beginloadtabdata['fives']='0.00'; 
      	 	$beginloadtabdata['tens']='0.00'; 
      	 	$beginloadtabdata['twenties']='0.00'; $beginloadtabdata['rolled_coins']='0.00'; $beginloadtabdata['quarters']='0.00'; 
      	 	$beginloadtabdata['nickles']='0.00'; $beginloadtabdata['dimes']='0.00'; $beginloadtabdata['pennies']='0.00'; 
      	 	$beginloadtabdata['misc']='0.00'; $beginloadtabdata['bl_total']='0.00'; $beginloadtabdata['rolled_coins']='0.00';
      	 	return View::make('mktmgr.savebeginloanregister',compact('data', 'beginloadtabdata', 'approvalsOk', 'querymake'));//->with('data', $data);
      	 }
      	 else
      	 {
      	 	Session::flash('alert-danger','FILES ARE LOCKED');
        	$registers_num=DB::select("select * from registers order by 1");	
        	return View::make('mktmgr.bookkeeperregister', compact('registers_num'));
      	 }
      }

    $int_flag = 'FALSE';
	$resultArray = json_decode(json_encode($beginloandata), true); 	      
    $bl_total = $this->sum_bl_record($resultArray);
    $beginloadtabdata['food_stamps']=$resultArray[0]['food_stamps']; 
    $beginloadtabdata['ones']=number_format($resultArray[0]['ones'], 2, '.', ' '); 
	$beginloadtabdata['fives']=number_format($resultArray[0]['fives'], 2, '.', ' '); 
	$beginloadtabdata['tens']=number_format($resultArray[0]['tens'], 2, '.', ' '); 
	$beginloadtabdata['twenties']=number_format($resultArray[0]['twenties'], 2, '.', ' '); 
	$beginloadtabdata['rolled_coins']=number_format($resultArray[0]['rolled_coins'], 2, '.', ' '); 
	$beginloadtabdata['quarters']=number_format($resultArray[0]['quarters'], 2, '.', ' ');  
	$beginloadtabdata['nickles']=number_format($resultArray[0]['nickles'], 2, '.', ' '); 
	$beginloadtabdata['dimes']=number_format($resultArray[0]['dimes'], 2, '.', ' ');  
	$beginloadtabdata['pennies']=number_format($resultArray[0]['pennies'], 2, '.', ' '); 
	$beginloadtabdata['misc']=number_format($resultArray[0]['misc'], 2, '.', ' '); 
	$beginloadtabdata['bl_total']=number_format($resultArray[0]['bl_total'], 2, '.', ' ');
      return View::make('mktmgr.savebeginloanregister',compact('data', 'beginloadtabdata', 'approvalsOk', 'querymake'));//->with('data', $data);
}

 public function convertDateToMDY($date) {
    	return date("m/d/Y", strtotime($date));
    }
 public function reconvertDateToMDY($date) {
    	return date("Y-m-d", strtotime($date));
    }   

 public function file_lock($date)
 {
 	$sql=DB::select('select * from locks where date_stamp="'.$date.'"');
 	if(count($sql) <= 0)
 	{
 		return false;
 	}
 	else
 	{
 		return true;
 	}
 }

 public function sum_bl_record($beginloadtabdata)
 {
 	return $tot = $beginloadtabdata[0]['ones']+$beginloadtabdata[0]['fives']+$beginloadtabdata[0]['tens']+$beginloadtabdata[0]['twenties']+$beginloadtabdata[0]['rolled_coins']+$beginloadtabdata[0]['quarters']+$beginloadtabdata[0]['nickles']+$beginloadtabdata[0]['dimes']+$beginloadtabdata[0]['pennies']+$beginloadtabdata[0]['misc'];
 }

 public function approvalsOk($date)
 {
 	$retVal=true; $msg='';
 	$prev_date = date('Y-m-d', strtotime($date .' -1 day')); $dayIsLocked = "N";
 	$chklastdate=DB::select('select max(date_stamp) as lastAprvlDate from mgmt where date_stamp <"'.$date.'"');
 	$chklastdate = json_decode(json_encode($chklastdate), true); 	 
 	$lastAprvlDate=$chklastdate[0]['lastAprvlDate'];
 	if($prev_date <> $lastAprvlDate)
 	{
 		$lastAprvlDate=$this->convertDateToMDY($lastAprvlDate);	
 		$msg='Last approved date was '.$lastAprvlDate.'. A manager needs to approve daily work .';
 		$retVal=false;
 	}
 	
 	$now = time(); // or your date as well
	$datediff = $now - strtotime($date);
	$numdays=floor($datediff / (60 * 60 * 24));
	if($numdays > 1)
		{
			$sql=DB::select('select * from mgmt where  mgmt.date_stamp ="'.$date.'"');
			if(count($sql) == 0)
			{
				$dayIsLocked = "Y"; $retVal = true;
				$seldate=$this->convertDateToMDY($date);
				$msg='Data is locked for , '.$seldate.'.Any changes won\'t be saved.';
			}
		}
		if($msg != ''){ Session::flash('alert-danger',$msg); }
	return $retVal;
 }

 public function approvalsOk2($date, $page)
 {
 	$retVal=true; $msg='';
 	$prev_date = date('Y-m-d', strtotime($date .' -1 day')); $dayIsLocked = "N";
 	$chklastdate=DB::select('select max(date_stamp) as lastAprvlDate from mgmt');
 	$chklastdate = json_decode(json_encode($chklastdate), true); 	 
 	$lastAprvlDate=$chklastdate[0]['lastAprvlDate'];
	$approvedateplusone = date('Y-m-d', strtotime($lastAprvlDate .' +1 day')); 
	//echo $approvedateplusone .'=='. $date; exit;
	if($approvedateplusone == $date)
	{
		return $dayIsLocked;
	}
	else if($approvedateplusone > $date)
	{ 
		$dayIsLocked = "Y"; $retVal = true;
		$seldate=$this->convertDateToMDY($date);
		$msg='Data is locked for '.$seldate.'.Any changes won\'t be saved.';
	}
	else if($approvedateplusone < $date)
	{
		$dayIsLocked = "N"; $retVal = true;
		$seldate=$this->convertDateToMDY($lastAprvlDate);
		$msg='Last approved date was '.$seldate.'. A manager needs to approve daily work .'; 
		Session::flash('alert-danger',$msg);
		$registers_num=DB::select("select * from registers order by 1");	
        return View::make('mktmgr.'.$page, compact('registers_num'));
	}
	Session::flash('alert-info',$msg);
	return $dayIsLocked;
 }

  public function approvalsOk3($date, $page)
 {
 	$retVal=true; $msg='';
 	$prev_date = date('Y-m-d', strtotime($date .' -1 day')); $dayIsLocked = "Y";
 
 	$chklastdate=DB::select('select * from mgmt where date_stamp="'.$prev_date.'"');
 	if(count($chklastdate) == 0)
 	{
 		$msg = "A MANAGER HAS NOT APPROVED PREVIOUS DAYS WORK.";
 		Session::flash('alert-danger',$msg);
 		$dayIsLocked = "Y";
 	}
	return $dayIsLocked;
 }

 public function postBeginLoanRegister()
 {
 	//querymake
 	$msg = '';
 	$action=Input::get('querymake'); 
 	$reg_num=Input::get('reg_number');
	$food_stamps=Input::get('food_stamps');
	$ones=Input::get('ones'); $ones=str_replace(".","",$ones);
	$fives=Input::get('fives'); $fives=str_replace(".","",$fives);
	$tens=Input::get('tens'); $tens=str_replace(".","",$tens);
	$twenties=Input::get('twenties'); $twenties=str_replace(".","",$twenties);
	$rolled_coins=Input::get('rolled_coins');  $rolled_coins=str_replace(".","",$rolled_coins);
	$quarters=Input::get('quarters'); $quarters=str_replace(".","",$quarters);
	$nickles=Input::get('nickles'); $nickles=str_replace(".","",$nickles);
	$dimes=Input::get('dimes'); $dimes=str_replace(".","",$dimes);
	$pennies=Input::get('pennies'); $pennies=str_replace(".","",$pennies);
	$misc=Input::get('misc'); $misc=str_replace(".","",$misc);
	$bl_total=Input::get('bl_total');  $bl_total=str_replace(".","",$bl_total);
	$entry_date=$this->reconvertDateToMDY(Input::get('reg_date'));
 	if($action == 'insert')
 	{ 
 		$sql=DB::insert('insert into begin_loan(reg_num, food_stamps, ones, fives, tens, twenties, rolled_coins, quarters,nickles, dimes, pennies, misc, bl_total, entry_date)values("'.$reg_num.'","'.$food_stamps.'","'.$ones.'","'.$fives.'","'.$tens.'","'.$twenties.'","'.$rolled_coins.'","'.$quarters.'","'.$nickles.'","'.$dimes.'",
 			       "'.$pennies.'","'.$misc.'","'.$bl_total.'","'.$entry_date.'")');	
 		 $msg = 'Record has been added!';
 	}
 	else if($action == 'update')
 	{
 		/*echo 'update begin_loan set food_stamps="'.$food_stamps.'", ones="'.$ones.'", fives="'.$fives.'", tens="'.$tens.'", 
 						twenties="'.$twenties.'", rolled_coins="'.$rolled_coins.'", quarters="'.$quarters.'",nickles="'.$nickles.'", 
 						dimes="'.$dimes.'", pennies="'.$pennies.'", misc="'.$misc.'", bl_total="'.$bl_total.'"
 						where reg_num="'.$reg_num.'" and entry_date="'.$entry_date.'"';
 						exit;*/
 		$sql=DB::update('update begin_loan set food_stamps="'.$food_stamps.'", ones="'.$ones.'", fives="'.$fives.'", tens="'.$tens.'", 
 						twenties="'.$twenties.'", rolled_coins="'.$rolled_coins.'", quarters="'.$quarters.'",nickles="'.$nickles.'", 
 						dimes="'.$dimes.'", pennies="'.$pennies.'", misc="'.$misc.'", bl_total="'.$bl_total.'"
 						where reg_num="'.$reg_num.'" and entry_date="'.$entry_date.'"');
 		 $msg = 'Record has been updated!'; 
 	}
 	if($msg != ''){ Session::flash('alert-danger',$msg); }
 	$registers_num=DB::select("select * from registers order by 1");	
    return View::make('mktmgr.bookkeeperregister', compact('registers_num'));
 }
 
public function approvalsOk4($dateofinfo)
{
	$retVal = true;
	$prev_date = date('Y-m-d', strtotime($dateofinfo .' -1 day')); $dayIsLocked = "N";
 	$chklastdate=DB::select('select max(date_stamp) as lastAprvlDate from mgmt where  mgmt.date_stamp < "'.$dateofinfo.'"');
 	$chklastdate = json_decode(json_encode($chklastdate), true);
 	$lastAprvlDate= $chklastdate[0]['lastAprvlDate'];

 	if ($prev_date <> $lastAprvlDate)  
 	{    
 		$msg="Last approved date was " . date("m-d-Y", strtotime($lastAprvlDate)) .". A manager needs to approve daily work .";
 		$retVal = false;
 		Session::flash('alert-danger',$msg);
 		return View::make('mktmgr.bookkeeperregistertotal');
 	}

 	$dayIsLocked = 'N';
 	$now = time();
	$your_date = strtotime($dateofinfo);
	$datediff = $now - $your_date;
	$numdays=floor($datediff / (60 * 60 * 24));

	if($numdays > 1)
	{
	  $chktodatedate=DB::select('select * from mgmt where  mgmt.date_stamp = "'.$dateofinfo.'"');	
	  $chktodatedate = json_decode(json_encode($chktodatedate), true);
		if(count($chktodatedate) > 0)
		{
			$dayIsLocked = "Y";
			$msg="Data is locked for ,". $dateofinfo .". Any changes won't be saved."; $retVal = true;
			Session::flash('alert-danger',$msg);
 		    return View::make('mktmgr.bookkeeperregistertotal');
		}
	}

	return $retVal;
}

 public function postDateGetData()
 {	
 	    $validator = Validator::make(Input::all(),
            array(
                   'dateofinfo'=>'required',
                ),array('dateofinfo.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-bookkeeperregistertotal')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
	       /* $addloanbegin = new User;
	        $addloanbegin = Auth::user();*/
	        $dateofinfo =Input::get('dateofinfo'); $reg_num = ''; $itemid= ''; $itemname = ''; $dayIsLocked = 'N';
	        
	       // $dayIsLocked=$this->approvalsOk2($dateofinfo, 'bookkeeperregistertotal'); 
	        $dayIsLocked=$this->approvalsOk4($dateofinfo); 

	        /*echo 'select r.reg_num, b.bl_total as item_amt from registers r, begin_loan b 
	          					   where b.reg_num = r.reg_num and b.entry_date = "' . Input::get('dateofinfo') . '"
	  							   order by r.reg_num';
	  							   exit;*/
	        $results = DB::select('select r.reg_num, b.bl_total as item_amt from registers r, begin_loan b 
	          					   where b.reg_num = r.reg_num and b.entry_date = "' . Input::get('dateofinfo') . '"
	  							   order by r.reg_num');
	        $resultArray = json_decode(json_encode($results), true);

	      
	      	$dayIsLocked= 'Y'; $itemname='Begin Loans'; $itemid='BL';
	       return View::make('mktmgr.bookloanrregtotlist',compact('resultArray', 'dateofinfo', 'itemid', 'itemname','dayIsLocked'));
	     
        }
 }

 public function postBookLoanRRegister()
 {
 		$validator = Validator::make(Input::all(),
            array(
                   'reg_num' =>'required',
                   'dateofinfo'=>'required',
                ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-bookloanrregistervalidation')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
          $dateofinfo = Input::get('dateofinfo');
          $reg_num = Input::get('reg_num');
          $approvalsOk=$this->approvalsOk3($dateofinfo, 'bookloanrregister'); 

          if($approvalsOk == 'N')
          {
          	$validator = Validator::make(Input::all(),
            array(
                   'reg_num' =>'required',
                   'dateofinfo'=>'required',
                ));
			if($validator->fails())
	        {          
			    return Redirect::route('mktmgr-bookloanrregistervalidation')
	                    ->withErrors($validator)
	                    ->withInput();
	        }
          }

          $item_id = DB::select('select l.*, itid.item_desc from loans l, loanitems itid 
            	                   where l.item_id = itid.item_id and l.entry_Date="'.$dateofinfo.'"
            	                   and l.reg_num ="'.$reg_num.'"');
		  if(count($item_id) <= 0)
		  	{ 
		  	  $dayIsLocked = "Y";$item_id = DB::select("select * from loanitems order by 1"); 
		      return View::make('mktmgr.postbookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked'));
			}
		 else{
		 	  $dayIsLocked = "N";
		      return View::make('mktmgr.updatebookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked')); 
			 }
		}	 
  }

  public function getBookLoanRegisterReg()
  {
  	$registers_num=DB::select("select * from registers order by 1");
    return View::make('mktmgr.bookloanrregister', compact('registers_num'));
  }

  public function postBookLoanrRegisterReg()
  {
  	$validator = Validator::make(Input::all(),
            array(
                   'reg_num' =>'required',
                   'dateofinfo'=>'required',
                ),array('reg_num.required'=>'Register Number is required','dateofinfo.required'=>'required'));
	if($validator->fails())
    {          
	  return Redirect::route('mktmgr-bookloanrregisterreg')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {

        	$date = Input::get('dateofinfo'); $reg_num=Input::get('reg_num'); $action = 'update';
        	$approvalsOk=$this->approvalsOk2($date, 'bookloanrregister');
            $dateofinfo=$this->convertDateToMDY($date);

		 	$alldata = DB::select("select loans.*, loanitems.item_desc, loanitems.item_id as allitems from loanitems left join 
		 		            	   loans on loanitems.item_id = loans.item_id
		 		            	   and loans.entry_date = '".$date."' and loans.reg_num='".$reg_num."'");
		 		$alldata = json_decode(json_encode($alldata), true); 
		 	return View::make('mktmgr.bookloanrregisterinsertupdate', compact('alldata', 'action', 'approvalsOk','reg_num', 'dateofinfo'));
        }	
  }

  public function saveBookLoanReg()
  {
  		$date = Input::get('dateofinfo'); 
  		$regnum=Input::get('regnum');
  		$totfields = Input::get('totfields');
  		//itemdesc
  		//accesspoint =
  		$entry_date=$this->reconvertDateToMDY(Input::get('dateofinfo'));


  		for($i=0; $i<= $totfields; $i++)
  		{
  			$amt = Input::get('amt'.$i); $itemid = Input::get('itemdesc'.$i); 
  			$sql=DB::select("select * from loans where entry_date = '".$entry_date."' and reg_num='".$regnum."' and item_id='".$itemid."'");
  			$amt=str_replace(".","",$amt);
  			$alldata = json_decode(json_encode($sql), true);
  			if(count($alldata) == 0)
  			{

  				$sql=DB::insert("insert into loans(item_id, item_amt, reg_num, source_id, entry_date)
	  							values('".$itemid."','".$amt."','".$regnum."','book1','".$entry_date."')");
	  			 $msg = 'Record has been added!';
  			}	
  			else
  			{
  				 $sql=DB::update('update loans set item_amt="'.$amt.'", source_id="book1"
	  			  	                 where reg_num="'.$regnum.'" and entry_date="'.$entry_date.'"
	  			  	                 and item_id="'.$itemid.'"');
	  					$msg = 'Record has been updated!'; 	
  			}

  		}

  		/*$sql=DB::select("select * from loans where entry_date = '".$entry_date."' and reg_num='".$regnum."'");
  		$alldata = json_decode(json_encode($sql), true); 
  		if(count($alldata) == 0)
  		{
  			for($i=0; $i<= $totfields; $i++)
	  		{
	  			$amt = Input::get('amt'.$i); $itemid = Input::get('itemdesc'.$i); // echo "<br />";
	  			if($amt != 0)
	  			{	//$amt=str_replace(".","",$amt);
	  			 $sql=DB::insert("insert into loans(item_id, item_amt, reg_num, source_id, entry_date)
	  							values('".$itemid."','".$amt."','".$regnum."','book1','".$entry_date."')");
	  			 $msg = 'Record has been added!';
	  			}
	  		}
  		}
  		else
  		{
  			for($i=0; $i<= $totfields; $i++)
	  		{
	  		    $amt = Input::get('amt'.$i); $itemid = Input::get('itemdesc'.$i);
	  			if($amt != 0)
	  			{	//$amt=str_replace(".","",$amt);
	  			  $sql=DB::update('update loans set item_amt="'.$amt.'", source_id="book1"
	  			  	                 where reg_num="'.$regnum.'" and entry_date="'.$entry_date.'"
	  			  	                 and item_id="'.$itemid.'"');
	  					$msg = 'Record has been updated!'; 
	  			}		
	  		}
  		}*/
  		
  		Session::flash('alert-success',$msg);
  		$registers_num=DB::select("select * from registers order by 1");
    	return View::make('mktmgr.bookloanrregister', compact('registers_num'));
  }

  public function getbookloanregTotal(){
		   return View::make('mktmgr.bookloanrregtotal'); 
	 }


  public function postBookLoanRRegTotal()
  {
  		$validator = Validator::make(Input::all(),
            array(
                   'totcode' =>'required',
                   'dateofinfo'=>'required',
                ),array('dateofinfo.required'=>'Date is required'));
	if($validator->fails())
    {          
	  return Redirect::route('mktmgr-bookloanrregtotal')
                    ->withErrors($validator)
                    ->withInput();
    }
    else
    {
    	$dateofinfo = Input::get('dateofinfo'); 
  		$itemid=Input::get('totcode');
  		if($itemid == 'GT')
  		{
  			$itemname = 'Grand Total';
  			$sql = DB::select('select loans.*, loanitems.item_desc, loanitems.item_id as allitems 
  								from loanitems left join loans on loanitems.item_id = loans.item_id
  								and loans.entry_date = "'.$dateofinfo.'" order by 1 desc;');
  			$alldata = json_decode(json_encode($sql), true); 
  			$dateofinfo=$this->convertDateToMDY($dateofinfo);
  			return View::make('mktmgr.loantoregtotalgt', compact('dateofinfo','alldata'));
  		}
  		else if($itemid == 'LR')
  		{
  			$itemname = 'LOANS TO REG TOTAL'; 
  			$getdata = DB::select("select * from loans where entry_date = '".$dateofinfo."' and item_id='".$itemid."'");
  			$resultArray = json_decode(json_encode($getdata), true); 
  			$dateofinfo=$this->convertDateToMDY($dateofinfo);
  			return View::make('mktmgr.loantoregtotallr', compact('dateofinfo', 'resultArray', 'itemname', 'itemid'));
  		}
    }	
  }	 

  public function postBookRegCheckoutRefSave()
	 { 
	 	$validator = Validator::make(Input::all(),
            array(
                   'reg_num' =>'required',
                   'dateofinfo'=>'required',
                ),array('dateofinfo.required'=>'Date is required','reg_num.required'=>'Register number is required'));
	 	$reg_num = Input::get('reg_num');
	 	$dateofinfo = Input::get('dateofinfo');
		
		if($validator->fails() || $reg_num == '')
        {
            return Redirect::route('mktmgr-bookregcheckoutreg')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$dayIsLocked = "N";
        	$reg_num = Input::get('reg_num'); 
	 		$dateofinfo = Input::get('dateofinfo');
        	$dayIsLocked=$this->approvalsOk2($dateofinfo, 'bookregcheckoutreg'); 
        	$sql = DB::select("select e.item_id, e.item_amt/100 as item_amt, e.reg_num, e.source_id, e.entry_date, i.item_id as itemid, 
        					   i.item_desc as itemdesc 
							   from entry e right join items i on e.item_id = i.item_id
							   and i.item_type not in('T','X') and i.disable = 'N' 
							   and e.reg_num = '".$reg_num."' and e.entry_date = '".$dateofinfo."'
							   order by i.item_type,i.item_desc");
        	$resultArray = json_decode(json_encode($sql), true); 
        	$convertedate=$this->convertDateToMDY($dateofinfo);
        return View::make('mktmgr.bookregcheckoutsaveupdate', compact('convertedate', 'resultArray', 'dayIsLocked', 'reg_num', 'dateofinfo'));
        }
	 }

	 public function postIURegCheckOutReg()
	 {
	 	$reg_num = Input::get('selregnum'); 
	 	$dateofinfo = Input::get('seldate'); 
	 	$numrows = Input::get('numrows');
	 	for($i=1; $i<= $numrows; $i++) 
	 	{
	 		$itemamt='itemamt'.$i; $itemcode='itemcode'.$i;
	 		$itemcode = Input::get($itemcode); $itemamt = Input::get($itemamt); 
	 		$checkinsert = DB::select("select * from entry where reg_num = '".$reg_num."' and entry_date = '".$dateofinfo."'
	 			                       and item_id = '".$itemcode."'");
	 		$checkinsert = json_decode(json_encode($checkinsert), true); 
	 		$itemamt=str_replace(".","",$itemamt);
	 		if($itemamt != 0)
	 		{
	 			if(count($checkinsert) <= 0)
	 			{
	 				/*echo "insert into entry(item_id, item_amt, reg_num, source_id, entry_date)
	 									   values('".$itemcode."','".$itemamt."','".$reg_num."','book1','".$dateofinfo."'')";  echo "<br />";*/
	 			  $checkinsert = DB::insert("insert into entry(item_id, item_amt, reg_num, source_id, entry_date)
	 									   values('".$itemcode."','".$itemamt."','".$reg_num."','book1','".$dateofinfo."')");
	 		   }
		 		else
		 		{
		 			/*echo "update : ". "update entry set item_amt ='".$itemamt."' where reg_num = '".$reg_num."' and entry_date = '".$dateofinfo."'
		 			                       and item_id = '".$itemcode."'"; echo "<br />";*/
		 			$checkinsert = DB::update("update entry set item_amt ='".$itemamt."' where reg_num = '".$reg_num."' and entry_date = '".$dateofinfo."'
		 			                       and item_id = '".$itemcode."' and reg_num = '".$reg_num."'");
		 		}	
	 		}
	 		
	 	}
	 	Session::flash('alert-info','Record updated');
	 	$registers_num=DB::select("select * from registers order by 1");
		return View::make('mktmgr.bookregcheckoutreg', compact('registers_num'));
	 }

}
