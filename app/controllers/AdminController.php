<?php

class AdminController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	/*public function showWelcome()
	{
		return View::make('hello');
	}*/

	public function admin()
	{
		//echo $user = User::find(1)->username;
		//echo '<pre>',print_r($user),'</pre>';
		
		/*Mail::send('emails.auth.testemail', array('name' => 'khizar'), function($message)
		{
		    $message->to('khussain7@live.com', 'Khizar')->subject('Welcome!');
		});*/
		return View::make('admin.signin');
		/*return View::make("home");*/
  }
  public function createuser()
  {
  	echo 'createuser<br>';
  	echo Hash::make('bkpthd');
  }
    public function getCreateadmin()
	{

		//usercreated_by_admin
		$superadminlist = new AdminUser;
	    $superadminlist = DB::select('SELECT * FROM users WHERE usercreated_by_admin =1 ORDER BY firstname');
		return View::make("admin.create", compact('superadminlist'));
		 $username = Auth::user()->username; //exit;
	  
	   		 return View::make('admin.create');
	   
	}
	public function getviewusers()
	{
		$superadminlist = new AdminUser;
	    $superadminlist = DB::select('SELECT * FROM users WHERE usercreated_by_admin =1 ORDER BY firstname');
		return View::make('admin.viewusers', compact('superadminlist'));
	}
	public function getForgotpassword()
	{
		return View::make("admin.forgotpassword");
	}
	public function AdminpostSignIn()
	{
		$validator = Validator::make(Input::all(),
			array(
				   'username' =>'required|max:30',
				   'userpassword'=>'required|min:6'
				));

		//print_r($validator);

		if($validator->fails())
		{
			return Redirect::route('admin')
					->withErrors($validator)
					->withInput();
		}
		else
		{
			//echo 'email-'.Input::get('email'); echo "<br />";
			//echo 'password-'.Input::get('userpassword'); echo "<br />";
			//echo 'password-'.$password=Hash::make(Input::get('userpassword')); exit;
			$auth = Auth::attempt(array(
					'username'=>Input::get('username'),
					'password'=>Input::get('userpassword'),
					'active'=>1
			));	

			if($auth)
			{
				// redirect to user home page
				//print_r($auth); exit;
				//Session::put('storeid', '305');
				$user=Input::get('username');
				if($user == 'superadmin'){return Redirect::route('admin-create');}
				if($user == 'admin'){return Redirect::route('admin-viewusers');}
		        if($user == 'subadmin'){return Redirect::route('admin-viewusers');}
				if($user){ 
				$userinfo = new AdminUser;
				 $userinfo = DB::select("SELECT * FROM users WHERE username ='".$user."' ORDER BY firstname");
				 foreach ($userinfo  as $key) {
					 if($key->superadmin==1 || $key->admin==1 || $key->subadmin==1  )
					 {
						 return Redirect::route('admin-home',array('id' => $key->id));
					 }
				 }
		         
			
				 }
	   
			}
			else
			{
				return Redirect::route('admin')
						->with('global', 'Email/Password is worng or accout not activated');			

			}	
		}
		return Redirect::route('admin')
						->with('global', 'Problem in signing in');	

	}
	public function UserCreate()
	{
		$validator = Validator::make(Input::all(),
			array(
				    'firstname' =>'required|max:30',
				    'lastname' =>'required|max:30',
				    'username' =>'required|max:30|username|unique:users',
				    'email' =>'required|max:50|email|unique:users',
				   	'username'=>'required|max:20|min:3|unique:users',
				   	'passwrod'=>'required|min:6',
				   	'retypepassword'=>'required|same:passwrod',
				   	'storenumber'=>'required|min:3',
				   	
				)
		);

		//print_r($validator);

		if($validator->fails())
		{ 
			/* $failedRules = $validator->failed(); 
			  print_r($failedRules);
exit;*/
			return Redirect::route('admin-create')
					->withErrors($validator)
					->withInput();
		}
		else
		{
			 $email = Input::get('email'); 
			 $username = Input::get('username');
			 $firstname = Input::get('firstname');
			 $lastname = Input::get('lastname');
			 $password = Input::get('passwrod');
			 $storenumber = Input::get('storenumber');
			 
			
			 if(Input::get('mktmgr') == '1' ){ $mktmgr=1; }else {$mktmgr=0;}
			if(Input::get('socscn01') == '1' ){ $socscn01=1; }else {$socscn01=0;}
			if(Input::get('slcscn01') == '1' ){ $slcscn01 =1; }else {$slcscn01 =0;}
			if(Input::get('slcadm03') == '1' ){ $slcadm03=1; }else {$slcadm03=0;}
			if(Input::get('sign1') == '1' ){ $sign1=1; }else {$sign1=0;}
			if(Input::get('bkpfst') == '1' ){ $bkpfst=1; }else {$bkpfst=0;}
			if(Input::get('slcadm00') == '1' ){ $slcadm00=1; }else {$slcadm00=0;}
			if(Input::get('bkpfst') == '1' ){ $bkpfst=1; }else {$bkpfst=0;}
			if(Input::get('socadm00') == '1' ){ $socadm00=1; }else {$socadm00=0;}
			
			if(Input::get('superadmin') == '1' ){ $superadmin=1; }else {$superadmin=0;}
			if(Input::get('admin') == '1' ){ $admin=1; }else {$admin=0;}
			if(Input::get('subadmin') == '1' ){ $subadmin=1; }else {$subadmin=0;}
			// Activaion code

			$code = str_random(60);
			$user = AdminUser::create(array(
				    'firstname'  => $firstname,
				    'lastname'   => $lastname,
				    'username'   => $username,
					'email'      => $email,
					'password'   => Hash::make($password),
					'code'		 => $code,
					'mktmgr'	 => $mktmgr,
					'socscn01'	 => $socscn01,
					'slcscn01'	 => $slcscn01,	
					'slcadm03'   => $slcadm03, 
					'sign1'      => $sign1,
					'bkpfst'     => $bkpfst,
					'slcadm00'   => $slcadm00,
					'socadm00'   => $socadm00,
					'store_number'=> $storenumber,
					'superadmin'=> $superadmin,
					'admin'=> $admin,
					'subadmin'=> $subadmin,
					'active'=>1,
					'usercreated_by_admin' =>1
				));

			if($user)
			{
				 // sent email funcationality

				/*Mail::send('emails.auth.activate', array('link' =>URL::route('/account-activate/', $code), 'username' => $username), function($message) use ($user){
						$message->to($user->email, $user->username)->subject('Activate your account');
				});	*/

				return Redirect::route('admin-create')
						->with('success', 'Yout account has been created! We have sent you an email');	
			}
		}
	}
public function Admincreate()
  {
	  return View::make('admin.admregistration');
  }
  
   public function Userscreate()
  {
	  return View::make('admin.userregistration');
  }
  public function Edituser()
  {
	  $id=Input::get('id');
	 
	  	$superadminlist_user = new AdminUser;
	    $superadminlist_user = DB::select('SELECT * FROM users WHERE id ='.$id.' ORDER BY firstname');
		return View::make("admin.edituser",compact('superadminlist_user'));
  }
	public function postDeleteUser()
	{   
		$deletwithid=Input::get('userid');
		//$superadminlist = new AdminUser;
		//$updateuser = DB::table('users')->where('id',$deletwithid)->update(['usercreated_by_admin' => 0]);
		$updateuser=DB::update('UPDATE users SET 
                        usercreated_by_admin="0"
                        WHERE id ='.Input::get('userid'));
		if($updateuser == 1)
		{
			 return ['success' => 'Record deleted successfully'];
			//return ("Record deleted successfully");
		}
	    /* $superadminlist = DB::select('SELECT * FROM users WHERE usercreated_by_admin =1 ORDER BY firstname');
	   $data = '';
	    foreach ($superadminlist as $key) {
	    	$data .='<tr><td>'.$key['firstname'].'</td>
            		<td>'.$key['email'].'</td>
            		<td><span class="glyphicon glyphicon-edit"></span> Edit &nbsp;&nbsp; 
						<span class="glyphicon glyphicon-trash" id="'.$key['id'].'"></span> Delete
		            </td></tr>';
	    } */

	   // return $data;

		//return View::make("admin.create", compact('superadminlist'));
	}
	public function getSignOut() {
			/*	echo "hit"; exit;*/
		    Auth::logout(); Session::flush();
		    return Redirect::route('admin');

	}
	
	public function Updateuser()
	{
		$userid=Input::get('userid');
		
		//$superadminlist = new AdminUser; 
			 $username = Input::get('username');
			 $firstname = Input::get('firstname');
			 $lastname = Input::get('lastname');
			 $password = Input::get('passwrod');
			 $email = Input::get('useremail');
			 
			 $storenumber = Input::get('storenumber');
			if(Input::get('superadmin') == '1' ){ $superadmin=1; }else {$superadmin=0;}
			if(Input::get('admin') == '1' ){ $admin=1; }else {$admin=0;}
			if(Input::get('subadmin') == '1' ){ $subadmin=1; }else {$subadmin=0;}
			
			$queryupdate=DB::update('UPDATE users SET 
                        firstname="'.$firstname.'",
                        lastname="'.Input::get('lastname').'",
                        username="'.Input::get('username').'",
                        email="'.Input::get('useremail').'",
                        superadmin="'.Input::get('superadmin').'",
                        store_number="'.Input::get('storenumber').'"
                        WHERE id ='.Input::get('userid'));
			
		
				$superadminlist_user = new AdminUser;
	    $superadminlist_user = DB::select('SELECT * FROM users WHERE id ='.$userid.' ORDER BY firstname');
				return View::make("admin.edituser",compact('superadminlist_user'));
	    //$superadminlist = DB::select('SELECT * FROM users WHERE usercreated_by_admin =1 ORDER BY firstname');
	   

		//return View::make("admin.create", compact('superadminlist'));
	}
	public function Adminhome()
	{
		
	  $id=Input::get('id');
	 
	  	$userinfo = new AdminUser;
	    $userinfo = DB::select('SELECT * FROM users WHERE id ='.$id.' ORDER BY firstname');
		return View::make("admin.home",compact('userinfo'))->with('message', 'State saved correctly!!!');
		
		/*return View::make("home");*/
  }
}
