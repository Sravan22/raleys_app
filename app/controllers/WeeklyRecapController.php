<?php

/**
 * 
 */
class WeeklyRecapController extends BaseController 
{





	public function getwklySalesDate()
	{
		return View::make('mktmgr.wklysalesdate'); 
	}
	public function wklySalesView()
	{
		Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
      
        
        $validator = Validator::make(Input::all(),
            array(                   
                   'sales_date'=>'required|date|weekenddatecheck',
                ),array(
                	'sales_date.required' => 'This date is required'
                	));
         //echo '<pre>',print_r($validator);exit;
	    if($validator->fails())
	    {
	        return Redirect::route('bookspro_weeklysalesdate')
	                    ->withErrors($validator)
	                    ->withInput();
	    }
	    else
	    {
		    $sales_date = Input::get('sales_date');
		   
		   $per = $this->get_per($sales_date);
		    

		    $rec = $this->get_ccpc($sales_date);
		   
		     $mainsale_rec['custcnt']  = $rec[0];
		     $mainsale_rec['comp_coupon'] = $rec[1];
		     
		     $rec1 = $this->totsales_entry($sales_date);
		       
		     $mainsale_rec['tot_sales']= $rec1[0];
		     $mainsale_rec['adj']=$rec1[1]; 
		     
		  
		      $sumtot =  $mainsale_rec['tot_sales'] + $mainsale_rec['adj'];
		      $tot_adjsales = $sumtot;
		     
		       
		      $sales_entry = $this->sales_entry($sales_date);
		      $adj_entry = $this->adj_entry($sales_date); 
		      $file_locked = $this->file_locked($sales_date);
	        	//echo $file_locked;exit;
        	if($file_locked)
        	{
        		$file_locked = "Y";
        	}
        	else
        	{
        		$file_locked = "N";
        	}

			return View::make('mktmgr.wklysalesdetails',compact('sales_date','per','mainsale_rec','tot_adjsales','sales_entry','adj_entry','file_locked'));     
		     
		}
    }
    public function file_locked($stamp_date)
	{
		$file_locked=DB::select('SELECT * from locks  WHERE locks.date_stamp = "'.$stamp_date.'" ');
		if($file_locked)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	/*gets the current period*/
	public function get_per($sales_date)
	{
			//echo $sales_date;exit;
			$per = 0;
			$sql= DB::Select('SELECT misc.cur_fiscal_per as per FROM misc
			WHERE  misc.cur_per_start_date <= "'.$sales_date.'"
			AND misc.cur_per_end_date >= "'.$sales_date.'"');
		 	//echo '<pre>',print_r($sql);exit;
			if(count($sql) !=0){
			$resultArray = json_decode(json_encode($sql), true);
			//echo $resultArray[0]['custcnt'];exit; 
			$per =  $resultArray[0]['per'];
			}


		 if(empty($sql))
		 {			
		 	//	echo 'hello ';exit;
				$sql1 = DB::Select('SELECT fiscdates.fiscal_period as per FROM fiscdates
				WHERE  fiscdates.fiscal_per_sdate <= "'.$sales_date.'"
				AND fiscdates.fiscal_per_edate >= "'.$sales_date.'"');
            	//echo '<pre>',print_r($sql1);exit;
				if(count($sql1) !=0){
				$resultArray = json_decode(json_encode($sql1), true);
					//echo $resultArray[0]['custcnt'];exit; 
				$per = $resultArray[0]['per'];
				}
 		  }
 		  return $per;
	}


	/*Gets the customer count and competitor coupon amts*/
	public function get_ccpc($sales_date)
	{
		$custcnt = 0;
		$compcoupon = 0.00;	
		$custcnt_query = DB::Select('SELECT recapmain.cust_cnt as custcnt FROM recapmain
		WHERE recapmain.dept_id = "CC"
		AND recapmain.wk_end_date = "'.$sales_date.'"');
       if($custcnt_query)
       {
       		$custcnt = $custcnt_query[0]->custcnt;
       }

      

		$compcoupon_query= DB::Select('SELECT recapmain.cust_cnt as compcoupon FROM recapmain
		WHERE recapmain.dept_id = "PC"
		AND recapmain.wk_end_date = "'.$sales_date.'"');
		if($compcoupon_query)
		{
			$compcoupon = $compcoupon_query[0]->compcoupon;
		}

        return array($custcnt,$compcoupon);
	}

	/* Sums the total sales and total adjustments for the sales*/
	public function totsales_entry($sales_date)
	{
		$saletot = $adjtot = 0.00 ; 

		
		$status =	DB::SELECT('SELECT SUM(recapmain.dept_sales) as saletot FROM recapmain,recapitems
		WHERE recapitems.item_type IN ("M","J")
		AND recapmain.dept_id NOT IN ("CC")
		AND recapitems.item_id = recapmain.dept_id
		AND recapmain.wk_end_date = "'.$sales_date.'"');
		$status = json_decode(json_encode($status), true);
		$saletot = $status[0]['saletot'];

	

		$status1 = DB::Select('SELECT SUM(recapmain.dept_sales) as adjtot
		FROM recapmain,recapitems
		WHERE recapitems.item_type = "A"
		AND recapitems.item_id = recapmain.dept_id
		AND recapmain.wk_end_date = "'.$sales_date.'"');
		$status1 = json_decode(json_encode($status1), true);
		$adjtot = $status1[0]['adjtot'];

	
		$saletot = $saletot;
		$adjtot  = $adjtot;

		return array($saletot,$adjtot);


	}

	public function sales_entry($sales_date)
	{
		$sales_entry = array();
		$sql1 = DB::Select('SELECT i.item_id,i.acct_num, i.item_desc, 
			           m.dept_sales/100 as dept_sales, m.cust_cnt
			     FROM recapitems i left outer join recapmain m
			     on m.dept_id = i.item_id
			     and m.wk_end_date = "'.$sales_date.'"
			     WHERE i.item_type = "M"
			       AND i.item_id NOT IN ("CC")   
			       and i.disable = "N"
			       order by i.acct_num asc');
		$sql1_array = json_decode(json_encode($sql1), true);
		//echo '<pre>', print_r($entry); exit;
			$sql2 = DB::Select('SELECT i.item_id,i.acct_num, i.item_desc,
			      m.dept_sales/100 as dept_sales, m.cust_cnt
			    FROM recapitems i LEFT outer join recapmain m
			    on m.dept_id = i.item_id 
			    and m.wk_end_date = "'.$sales_date.'"
			     WHERE i.item_type = "J" 
			         and i.disable = "N" 
			       ORDER BY i.acct_num asc');
		$sql2_array = json_decode(json_encode($sql2), true);
		$sales_entry = array_merge($sql1_array,$sql2_array);
		//echo '<pre>', print_r($final_array); exit;
		return $sales_entry;
	}

	public function adj_entry($sales_date)
	{

			$adj_entry = array();
             $sql1 = DB::Select('SELECT item_id,acct_num, item_desc,m.dept_sales, m.cust_cnt  FROM  recapitems LEFT OUTER JOIN recapmain m on m.dept_id = item_id and m.wk_end_date ="'.$sales_date.'" 
                 WHERE item_type ="A" 
                 AND item_id NOT IN ("3A","TX","IM","XM","QM","KM","EM","TM") 
                 and disable = "N" 
                 ORDER BY acct_num');
             $sql1_array = json_decode(json_encode($sql1), true);
             //echo count($sql1_array).'<br>';
             //$final_array[] = $sql1_array;
             $sql2 = DB::Select('SELECT i.item_id,i.acct_num, i.item_desc,
                  m.dept_sales, m.cust_cnt FROM recapitems i left outer join 
                  recapmain m on  m.dept_id = i.item_id 
                  and m.wk_end_date = "'.$sales_date.'"
	              WHERE i.item_type = "A" 
                  AND i.item_id IN ("IM","XM","QM","KM","TM","EM")
		          and i.disable = "N" 
                  ORDER BY i.acct_num ');
             $sql2_array = json_decode(json_encode($sql2), true);
             //echo count($sql2_array).'<br>';
             //$final_array[] = $sql2_array;
             $sql3 = DB::Select('SELECT i.item_id,i.acct_num, i.item_desc,
                  m.dept_sales , m.cust_cnt FROM recapitems i left outer join 
                  recapmain m on m.dept_id = i.item_id 
                  and m.wk_end_date = "'.$sales_date.'"
                  WHERE i.item_type = "A" 
                  AND i.item_id IN ("TX") 
                  and i.disable = "N"
                  ORDER BY i.acct_num');
             $sql3_array = json_decode(json_encode($sql3), true);
             //echo count($sql3_array).'<br>';exit;
             //$final_array[] = $sql3_array;
             $adj_entry = array_merge($sql1_array,$sql2_array,$sql3_array);
             return $adj_entry;
	}


	public function AdjSalesSubmit()
	{

		$data =  Input::get('data');
		$wk_end_date =  Input::get('wk_end_date');
        $period = Input::get('period');
		/*$evenarray = array();
		$oddarray = array();
		foreach($data as $key=>$values)
        {
        	 if($key%2==0)
                {
                	if(is_array($values))
		            {
		                $evenarray[] = $values;
		            }
                }
                else
                {
                	if(is_array($values))
		            {
		                $oddarray[] = $values;
		            }
                }
      	}*/
      	
        
		for($i=0;$i<count($data);$i+=2)
		{
			
			$subtot1_query = DB::select('select * from recapmain where recapmain.dept_id =  "'.$data[$i]['name'].'"  AND recapmain.wk_end_date = "'.$wk_end_date.'" ');
			$dept_sales = $data[$i]['value']*100;
			
			if($subtot1_query)
			{	
				DB::update('update recapmain SET  recapmain.dept_sales = "'.$dept_sales.'" ,recapmain.period = "'.$period.'" WHERE recapmain.dept_id = "'.$data[$i]['name'].'" AND recapmain.wk_end_date = "'.$wk_end_date.'"');
			}
			else
			{
				DB::insert('INSERT INTO recapmain VALUES ("'.$data[$i]['name'].'",0,0, "'.$wk_end_date.'","'.$period.'")');
			}


			/*update table as per 4Gl row no 530*/
			
			if($data[$i]['name'] ==  "DA")
			{
				$dept_sales = $dept_sales * (-1);

				$status= DB::select(" SELECT * FROM recap
                             WHERE recap.item_id = 'FG'
                             AND recap.wk_end_date = '{$wk_end_date}'");
				if($status)
				{
					DB::update(" UPDATE recap
                           SET recap.item_amt = '{$dept_sales}'
                           WHERE recap.item_id = 'FG'
                           AND recap.wk_end_date = '{$wk_end_date}'");
                 
			    }
			    else
			    {
			    	DB::insert("INSERT INTO recap
                     VALUES ('FG','{$dept_sales}','{$wk_end_date}')");
			    }
			}
		}

		/*update the cust_cnt tables*/
		for($i=1;$i<count($data);$i+=2)
		{
			
			$recapmain_query = DB::select('select * from recapmain where recapmain.dept_id =  "'.$data[$i]['name'].'"  AND recapmain.wk_end_date = "'.$wk_end_date.'" ');
			    $cust_cnt = $data[$i]['value'];
			if($recapmain_query)
			{

				DB::update('update recapmain SET  recapmain.cust_cnt = "'.$data[$i]['value'].'" WHERE recapmain.dept_id = "'.$data[$i]['name'].'" AND recapmain.wk_end_date = "'.$wk_end_date.'"');
			}
			 
		}
	}

	public function PostSalesData()
	{
		
		//echo '<pre>';print_r(Input::all());
		$customercnt = Input::get('customercnt');
		$comptitorcoupon = Input::get('comptitorcoupon');
		$comptitorcoupon = $comptitorcoupon*100;
		$wk_end_date = Input::get('sales_date');
		$period = Input::get('period');


	    /*Checking the value available or not */


		$custcnt_query = DB::Select('SELECT recapmain.cust_cnt as custcnt FROM recapmain
		WHERE recapmain.dept_id = "CC"
		AND recapmain.wk_end_date = "'.$wk_end_date.'"');
       if($custcnt_query)
       {		
       			/*updating the values */
       		DB::update('update recapmain SET  recapmain.cust_cnt = "'.$customercnt.'" WHERE recapmain.dept_id = "CC" AND recapmain.wk_end_date = "'.$wk_end_date.'"');
       }
       else
       {
       	 /*Insert  the values */
       	DB::insert('Insert into recapmain  VALUES ( "CC",0,"'.$customercnt.'" ,"'.$wk_end_date.'","'.$period.'")');
       }

      

		$compcoupon_query= DB::Select('SELECT recapmain.cust_cnt as compcoupon FROM recapmain WHERE recapmain.dept_id = "PC"
		AND recapmain.wk_end_date = "'.$wk_end_date.'"');
		
		if($compcoupon_query)
		{
			DB::Update('update recapmain SET recapmain.cust_cnt ="'.$comptitorcoupon.'" WHERE recapmain.dept_id ="PC" AND recapmain.wK_end_date ="'.$wk_end_date.'"');
		}
		else
		{
		 DB::insert('Insert into recapmain  VALUES ("PC",0,"'.$comptitorcoupon.'" ,
		 	"'.$wk_end_date.'","'.$period.'")');
		}
		
		return Redirect::route('bookspro_weeklysalesdate');
	}


	public function getWklyRecap()
	 {
		return View::make('mktmgr.Wklyrecap'); 
	 }
	
      public function PostWklyRecap()
    {
    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {     
    	
    	$weekday = date('N', strtotime($value)); 
                  
			if($weekday == 6){
			$GLOBALS['val'] = $value;
			return true;
			}            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
    	

    	$validator = Validator::make(Input::all(),
        array('dateofinfo'=>'required|date|weekenddatecheck'),array(
                	'dateofinfo.required' => 'Date is required'
                ));


        if($validator->fails())
        {
             
            return Redirect::route('mktmgr-wklyrecap')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {   
        	// echo '<pre>';print_r(Input::all());exit;
        	   
          $calcdate= Input::get('dateofinfo');
        
        	
          $sql=DB::select("SELECT COUNT(*) AS lck from locks 
          	WHERE locks.date_stamp ='{$calcdate}' ");
         	//echo $sql  ->num_rows();exit;
          $lck = $sql[0]->lck;
         	
          if($lck != 2)
         	{
         		$msg=$this->checkPostageInv($calcdate);
         		//echo $msg ;exit;
         		 # alert the user if postage ending baalance is 0	
         		
         		if($msg !='')
         		{
         			
         			Session::flash('alert-warning',$msg);
         			return View::make('mktmgr.wklycheckpostageinv',compact('calcdate'));
         			
         		}
         		else
         		{
         			
         			
         			$openInvCurs = $this->checkLotteryInv($calcdate);
         			//echo '<pre>',print_r($openInvCurs);exit;
         			if(count($openInvCurs) > 0)
					{
			         		
			       return View::make('mktmgr.bookwklyrecaplotterydate',compact('openInvCurs','calcdate')); 	
			         }
			         else
			         {
			         	
			         	$msg = $this->message($calcdate);
						Session::flash('alert-warning',$msg);
						return View::make('mktmgr.wklycheckpostageinv',compact('calcdate'));
			         }
		     	}
         	}	
         		
         		//$lotterydata=$this->checkLotteryInv($calcdate);
         		//echo '<pre>',print_r($lotterydata);exit;
         		/*if(count($lotterydata) > 0)
				{
         			
                return View::make('mktmgr.bookwklyrecaplotterydate',compact('lotterydata'));   }*/
        }
         	/*else
         	{
         	  Session::flash('alert-info','FILES HAVE BEEN LOCKED, CAN NOT PROCESS THIS RECAP REQUEST'); 
         	  return View::make('mktmgr.bookwklyrecap'); 
         	}*/
    }
    
    public function postSaveWklyRecap()
    {	
    	//$postal = Input::all();
    	//echo '<pre>',print_r($postal,true);exit;
    	$recapDate =Input::get('recapDate');
    	$count = Input::get('count');

    	for($i=0;$i<=$count;$i++)
		
		{
			$gameno_id="gameno".$i;
			$gameno = $_POST[$gameno_id];
			
			$endinv_id = "endinv".$i;
			$endinv = $_POST[$endinv_id];
			
			$ticketvalue_id ="ticketvalue".$i;
			$ticketvalue = $_POST[$ticketvalue_id];
			
			$inhand_id='inhand'.$i;
			$inhand=$_POST[$inhand_id];
			
			$ltryCnt= DB::select('select count("game_no") as ltryCnt from lottery
             where lottery_date = "'.$recapDate.'" and
               game_no = '.$gameno.'');
			//echo $ltryCnt[0]->ltryCnt ;exit;
			
			if ($ltryCnt[0]->ltryCnt == 0) {
			DB::insert('insert into lottery (lottery_date, game_no, ticket_value, 
                       begin_inv, end_inv, deliveries, returns,sales_unity) values
                       ( "'.$recapDate.'",'.$gameno.', '.$ticketvalue.','.$endinv.','.$inhand.',0,0,0)');
			}
			else
			{
			$calcdate = $recapDate;
			$msg = $this->message($recapDate);		
			Session::flash('alert-warning',$msg);
         	return View::make('mktmgr.wklycheckpostageinv',compact('calcdate'));
			}	

		}
		return View::make('mktmgr.Wklyrecap'); 	
    }


    public function checkPostageInv($calcdate)
    {		
    	 
    	
    	$sql=DB::select("SELECT end_inv_safe+end_inv_tills+end_inv_ca as endingInv from postage WHERE ps_date='{$calcdate}'");
    		if($sql)
    		{
    			$endingInv = $sql[0]->endingInv;	
    		}
    		else
    		{
    			$endingInv = 0;
    		}
    			//echo $endingInv;exit;
			if ($endingInv == 0 ) {

			$convert_date = date("m/d/Y", strtotime($calcdate));


			$msg="The POSTAGE ENDING INVENTORY for <span id='calcdate' style='color:red;'>".$convert_date."</span> is zero .<br>
			Please continue if you know this is correct, otherwise,<br> ENTER THE CORRECT ENDING BALANCE AND RECALCULATE THE RECAP <br />Click to <a href='checklotteryinv?date=$calcdate' id='' style='color:red;'>Continue</a>&nbsp;&nbsp <a href='#' id='cancelrecap' style='color:red;'>Cancel</a>";

			//return $msg;


			}
			else{
				$msg = '';
			}
    	
    	return $msg;


    }
    public function checklottery()
    {
    	$calcdate = Input::get('date');
    	//echo $calcdate;
    	$openInvCurs = array();

    	 DB::insert('insert into tmpGameDate (select game_no, max(lottery_date) as maxLottDate from lottery group by game_no)');

    	 	$max = DB::Select('select  max(maxLottDate) as max from tmpGameDate');
    	 	//echo '<pre>',print_r($max);exit;
    		/*echo $max[0]->max;exit;
    		if($calcdate == $max[0]->max)
    		{

    		}  
           */
		$lotterydata= DB::select('select l.game_no  as gameno, l.lottery_date as lotterydate, l.end_inv as endinv , l.ticket_value as ticketvalue ,0.00 as inhand
		from lottery l INNER JOIN tmpGameDate t on  l.game_no = t.game_no
		where l.end_inv> 0 
		and  l.lottery_date != "'.$calcdate.'"  
		and  l.lottery_date = (select  max(maxLottDate) from tmpGameDate)
		order by gameno');

		$openInvCurs = json_decode(json_encode($lotterydata), true);
    	//echo '<pre>',print_r($openInvCurs);exit;
    	//Delete temp table
    	DB::delete('delete from tmpGameDate');
		
		//check value is available or not 
		if(count($openInvCurs) > 0)
		{
		 			
		return View::make('mktmgr.bookwklyrecaplotterydate',compact('openInvCurs','calcdate')); 	
		 }
    	 
		
		else
		{
			
			$msg = $this->message($calcdate);
			//echo '<pre>',print_r($msg);exit;
			//$message = $msg['msg'];
			Session::flash('alert-warning',$msg);
         	return View::make('mktmgr.wklycheckpostageinv',compact('calcdate'));
			
		}         

    }

    public function message($calcdate)
    {
    	//$date_msg_array = array();
   		$msg = "If you have already printed your recap and are
          recalculating it,<br> don't forget to reprint<br />Click to <a href='#' id='continue' style='color:red;'>Continue</a>&nbsp;&nbsp <a href='wklyrecap' id='cancelrecap' style='color:red;'>Cancel</a>";
          //$date_msg_array['msg'] = $msg;
          //$date_msg_array['calcdate'] = $calcdate;
      	//echo '<pre>',print_r($date_msg_array);exit;
         //return $date_msg_array;
          return $msg;
    }

    public function checkLotteryInv($calcdate)
    {
         
    	
    	//echo $calcdate;
    	$openInvCurs = array();

    	 DB::insert('insert into tmpGameDate (select game_no, max(lottery_date) as maxLottDate from lottery group by game_no)');
    	 

		$lotterydata= DB::select('select l.game_no  as gameno, l.lottery_date as lotterydate, l.end_inv as endinv , l.ticket_value as ticketvalue ,0.00 as inhand
		from lottery l INNER JOIN tmpGameDate t on  l.game_no = t.game_no
		where l.end_inv> 0 
		and  l.lottery_date != "'.$calcdate.'"  
		and  l.lottery_date = (select  max(maxLottDate) from tmpGameDate  ORDER by maxLottDate DESC)
		order by gameno');

		$openInvCurs = json_decode(json_encode($lotterydata), true);
    	//echo '<pre>',print_r($openInvCurs);exit;
    	DB::delete('delete from tmpGameDate');
    	

		return $openInvCurs;         

    }


    public function runProcessWeekly()
    {
    	$date = 	date("m/d/Y", strtotime(Input::get('calcdate')));
    	$calcdate = date("Y-m-d", strtotime(Input::get('calcdate')));
    	//echo $calcdate;exit;
    	$data=$this->start_calcrecap($calcdate);
		$this->calc_deposits($calcdate);
		$this->calc_transf($calcdate);
		$this->calc_storetx($calcdate);
		$this->calc_potypes($calcdate);

		
		$msg='<p class="alert alert-success">FINISHED PROCESSING WEEKLY RECAP  For. '.$date.'<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></p>';
		return $msg;
		exit;
    }



    public function start_calcrecap($calcdate)
    {
   	 	//echo $calcdate;exit;
    	$groc_os = $drug_os = $safe_os = 0;
    	$bs = $es = 0;
    	$per = 0;
    	
    	$end_date = $calcdate;
    
    	$begin_date = date('Y-m-d',strtotime($calcdate . "-6 days"));
    	

    	
    	
    	$sql=DB::select("SELECT IFNULL(SUM(groc_os),0) as groc_os, IFNULL(SUM(drug_os),0) as drug_os, IFNULL(SUM(safe_os),0) as safe_os FROM mainsafe 
					     WHERE safe_date  >= '{$begin_date}' 
					     AND safe_date  <= '{$end_date}'");
			 
		
		if($sql)
		{
			
		$resultArray = json_decode(json_encode($sql), true); 
		
		$groc_os=$resultArray[0]['groc_os'];
		$safe_os=$resultArray[0]['safe_os'];
	    $groc_os=($groc_os + $safe_os) * (-1);
		$drug_os=$resultArray[0]['drug_os']* (-1);
		}

		
		$sql1=DB::select("SELECT count(*) as count from recapmain WHERE dept_id = 'GO' 
		 	              AND wk_end_date ='{$end_date}'");

		//echo $sql1[0]->count;exit;
		if($sql1[0]->count == 0)
		{
			

			$per=$this->get_per($end_date);
			
      	    DB::Insert('INSERT INTO recapmain(`dept_id`, `dept_sales`, `cust_cnt`, `wk_end_date`, `period`)
			      VALUES("GO","'.$groc_os.'","0","'.$end_date.'", "'.$per.'")');
      	} 
		else
		{	
			DB::update('UPDATE recapmain SET dept_sales="'.$groc_os.'"
			                WHERE dept_id="GO" AND wk_end_date="'.$end_date.'"');
		}
		
		
		$sql1=DB::select('SELECT (begin_safe * -1) as bs from mainsafe WHERE safe_date ="'.$begin_date.'"');
	  	
	  	//echo '<pre>',print_r($sql1);exit;
		if(count($sql1) == 0)
		{	
			
	       $bs=0;
		}	
		else
		{	
			
	 	   $resultArray = json_decode(json_encode($sql1), true); 
		   $bs=$resultArray[0]['bs'];
		}   
   	
   	   $sql2=DB::select('SELECT safe_cnt  as es from mainsafe WHERE safe_date ="'.$end_date.'"');
		
		if(count($sql2) == 0)
		{
		     $es=0;
		}	
		else{
		    $resultArray = json_decode(json_encode($sql2), true); 
		     $es=$resultArray[0]['es'];
		} 

		$status = DB::select(" SELECT count(*) as count  
			              FROM recapmain WHERE dept_id = 'BS'
                          AND wk_end_date = '".$end_date."'"); 
           
		if($status[0]->count === 0)
		{	
				
			    DB::Insert('INSERT INTO recapmain
			    (`dept_id`, `dept_sales`, `cust_cnt`, `wk_end_date`, `period`) 
                VALUES ("BS","'.$bs.'","0"," '.$end_date.'","'.$per.'")');
		}
		else
		{
			
			DB::UPDATE('UPDATE recapmain
                SET dept_sales =  "'.$bs.'"
                 WHERE dept_id = "BS"
                AND wk_end_date = "'.$end_date.'"');
		}
	   	
	   $status1 = DB::select('SELECT  count(*) as count from recapmain 
	   	       WHERE dept_id = "ES" AND wk_end_date = "'.$end_date.'"');
	   	  
	   	  if($status1[0]->count === 0)
	   	  {
	   	  	
	   	  	  DB::Insert('INSERT INTO recapmain
	   	  	  	 (`dept_id`, `dept_sales`, `cust_cnt`, `wk_end_date`, `period`)
				   VALUES("ES","'.$es.'","0","'.$end_date.'", "'.$per.'")');
	   	  }
	   	  
	   	  else
	   	  {
	   	  	
	   	  	$sql = DB::UPDATE('UPDATE recapmain 
		   	  		SET dept_sales="'.$es.'" 
		   	  		WHERE dept_id="ES" 
		   	  		AND wk_end_date="'.$end_date.'"');
	   	  }
	   	 // echo 'all inserted';exit;
	   	return $tot = $drug_os + $groc_os + $es + $bs;
    }

    public function calc_deposits($calcdate)
    {
    	$tot = 0;
    	$end_date = $calcdate;
    		
    	$begin_date =  date('Y-m-d',strtotime($end_date . "-6 days"));

    	
    	$sql=DB::select('SELECT * from safeentry,safeitems
    	                 WHERE safeentry.entry_date >= "'.$begin_date.'"
    		             AND safeentry.entry_date <= "'.$end_date.'"
					     AND safeitems.item_id <> "#C"
					     AND safeitems.item_id = safeentry.item_id
					     AND safeitems.item_type = "P"
					     ORDER BY safeentry.entry_date');
    	$arrayResult = json_decode(json_encode($sql), true);


    	
    	foreach ($arrayResult as $key)
    	{
    		   
    		  $item_id = $key['item_id'];

	 		  $item_amt=$key['item_amt']* (-1);
	 		  
	 		 
	 		  $sql1=DB::select('SELECT count(*)as count from recap WHERE item_id = "'.$item_id.'" AND wk_end_date ="'.$end_date.'"');
	 		 	 		 	
	 		  if($sql1[0]->count == 0)
	 		  {	
	 		  	
	 		  	  DB::insert('INSERT INTO recap (`item_id`, `item_amt`, `wk_end_date`)
			      VALUES("'.$item_id.'","'.$item_amt.'", "'.$end_date.'")');
		   	  	
		   	  }
		   	  else
		   	  {
		   	      DB::update('UPDATE recap SET item_amt="'.$item_amt.'" WHERE item_id="'.$item_id.'" AND wk_end_date="'.$end_date.'"');
		   	  }
		   	 
		}


    }


     public function calc_transf($calcdate)
    {
     
   
      $begin_date = date('Y-m-d',strtotime($calcdate . "-6 days"));
      $end_date   = $calcdate;
      $tot = 0;
      	
    	$sql=DB::select('SELECT * from safeentry,safeitems 
    		            WHERE safeentry.entry_date >= "'.$begin_date.'"
    		             AND safeentry.entry_date  <= "'.$end_date.'"
					     AND safeitems.item_id = "T"
					     AND safeitems.item_id = safeentry.item_id
					     ORDER BY safeentry.entry_date');

    	$arrayResult = json_decode(json_encode($sql), true);
    	
    	foreach ($arrayResult as $key) 
    	{
    		  
    		  $item_id = $key['item_id'];

	 		  $item_amt=$key['item_amt']* (-1);
	 	
	 		  $status = DB::select('SELECT * from recap 
	 		  	            WHERE item_id = "'.$item_id.'" 
	 		  	            AND wk_end_date ="'.$end_date.'"');
	 		  
	 		  if(empty($status) === true)
		 		  {
		 		  DB::insert('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
				      VALUES("'.$item_id.'","'.$item_amt.'", "'.$end_date.'")');
			   	  }
		   	  else
			   	  {
			   	   DB::update('UPDATE recap SET item_amt="'.$item_amt.'" WHERE item_id="'.$item_id.'" AND wk_end_date="'.$end_date.'"');
			   	  }
    	}

    	return 1;
    }


    public function calc_storetx($calcdate)
    {
    	
    	
    	$begin_date = date('Y-m-d',strtotime($calcdate . "-6 days"));
    	
    	$end_date = $calcdate;

    	$sql=DB::select('SELECT IFNULL(SUM(safeentry.item_amt),0) as item_amt, safeentry.item_id   FROM safeentry, safeitems
					     WHERE safeentry.entry_date >= "'.$begin_date.'"
					     AND safeentry.entry_date   <= "'.$end_date.'"
					     AND safeitems.item_type = "S"
					     AND safeitems.item_id NOT IN ("PS","RR","LR","TD")
					     AND safeitems.item_id = safeentry.item_id
					     GROUP BY safeentry.item_id');

    	$arrayResult = json_decode(json_encode($sql), true);
    	//echo '<pre>',print_r($arrayResult);exit;
    	foreach ($arrayResult as $key)
    	{
    		
    		$item_id = $key['item_id'];

	 		$item_amt=$key['item_amt']* (-1);
	 		
	 		$status=DB::select('SELECT * from recap WHERE 
	 			item_id = "'.$item_id.'" AND  wk_end_date ="'.$end_date.'"');
	 		 
	 		 $arrayResult = json_decode(json_encode($status), true);
	 		 
	 		  //echo '<pre>',print_r($arrayResult);exit;
			  if(empty($status) == true)
			  {	
			  		
			  	  DB::insert('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
			  VALUES("'.$item_id.'","'.$item_amt.'", "'.$end_date.'")');
			  }
			  else
			  {
			  		
			    DB::update('UPDATE recap SET item_amt="'.$item_amt.'" WHERE 
			    	item_id="'.$item_id.'" AND wk_end_date="'.$end_date.'"');
			  }

		}
		
		
		$item_id = "L1";

		$sql=DB::select('SELECT SUM(lottery.deliveries) as item_amt 
			             FROM lottery
				    	 WHERE lottery_date <= "'.$begin_date.'"
				         AND lottery_date >= "'.$end_date.'"');

		$arrayResult = json_decode(json_encode($sql), true);
    	//echo $arrayResult[0]['item_amt'];exit;
   	if($arrayResult[0]['item_amt'])
   	{	
    	foreach ($arrayResult as $key) 
    	{	
    		
	 		$item_amt=$key['item_amt']* (-1);
          // echo $item_amt;exit;

	 		$sql=DB::select('SELECT * from recap WHERE item_id = "'.$item_id.'" AND  wk_end_date ="'.$end_date.'"');
	 		  
	 		  if($sql)
	 		  {
	 		  	
	 		  	$sql = DB::update('UPDATE recap SET item_amt="'.$item_amt.'" WHERE item_id="'.$item_id.'" AND wk_end_date="'.$end_date.'"');
		   	  }
	 		  	
		   	  else
		   	  {
		   			
		   	  	 $sql = DB::insert('INSERT INTO recap(`item_id`, `item_amt`,
		   	  	                 `wk_end_date`)
			      VALUES("'.$item_id.'","'.$item_amt.'", "'.$end_date.'")');
		   	  }
		   	 
		}
	}	
		
		$item_id = "L2";

		$sql=DB::select('SELECT SUM(lottery.returns) as item_amt 
			             from lottery
				    	 WHERE lottery_date <= "'.$begin_date.'"
				          AND  lottery_date >= "'.$end_date.'"');

		$arrayResult = json_decode(json_encode($sql), true);
    
		if($arrayResult[0]['item_amt'])
		{	
			foreach ($arrayResult as $key) 
			{
		 		$item_amt=$key['item_amt']* (-1);
		 		
		 		$sql=DB::select('SELECT * from recap WHERE item_id = "'.$item_id.'" AND wk_end_date ="'.$end_date.'"');
		 		  
			 		  if($sql)
			 		  {	
			 		  		
			 		  	  DB::update('UPDATE recap SET item_amt="'.$item_amt.'" WHERE item_id="'.$item_id.'" AND wk_end_date="'.$end_date.'"');
			 		  }
				   	  else
				   	  {
				   	  	
				   		 DB::insert('INSERT INTO recap
				   		 	        (`item_id`, `item_amt`, `wk_end_date`)
					      VALUES("'.$item_id.'","'.$item_amt.'", "'.$end_date.'")');
				   	
				   	  }
			
			}
		}

		return true;
    }


    public function calc_potypes($calcdate)
    {

    	$begin_date = date('Y-m-d',strtotime($calcdate . "-6 days"));
    	
    	$end_date = $calcdate;
    	
    	$sql=DB::select('SELECT IFNULL(SUM(safeentry.item_amt),0) as item_amt,
    	              safeentry.item_id from safeentry, safeitems
    			   WHERE safeentry.entry_date >= "'.$begin_date.'"
			       AND safeentry.entry_date <= "'.$end_date.'"
			       AND safeitems.item_type = "O"
			       AND safeitems.item_id = safeentry.item_id
			       GROUP BY safeentry.item_id');
    	
    	$arrayResult = json_decode(json_encode($sql), true);
    	
    	if($sql) 
    	{	
    		
    		foreach ($arrayResult as $key) {
    			
    			$itemamt=$key['item_amt']* (-1);
    			
    			 $itemid=$key['item_id'];
    			
    			$this->datapush($itemamt, $end_date, $itemid);
    		}
    	}
    	
    	$itemid = "LO";
    		
    	$sql=DB::select('SELECT SUM(deliveries) as item_amt 
    		             from lottery
    					 WHERE lottery_date <= "'.$begin_date.'"
					     AND lottery_date >= "'.$end_date.'"');
    	
    	$arrayResult = json_decode(json_encode($sql), true);
    	//echo '<pre>',print_r($arrayResult);exit;
    	if($sql) 
    	{
    		
    		foreach ($arrayResult as $key) {
    			$itemamt=$key['item_amt']* (-1); //$itemid=$key['item_id'];
    			$this->datapush($itemamt, $end_date, $itemid);
    		}
    	}
    	
    	$itemid = "EG";
    	$sql=DB::select('SELECT SUM(returns) as item_amt 
    		             from lottery
    					 WHERE lottery_date <= "'.$begin_date.'"
					     AND lottery_date >= "'.$end_date.'"');
    	

    	$arrayResult = json_decode(json_encode($sql), true);
    	if($sql)
    	{
    		foreach ($arrayResult as $key) {
    			$itemamt=$key['item_amt']* (-1); //$itemid=$key['item_id'];
    			$this->datapush($itemamt, $calcdate, $itemid);
    		}
    	}

    	

    	return true;
    }

    public function datapush($itemamt, $calcdate, $itemid)

    {
    	
    	$sql=DB::select('SELECT * from recap WHERE item_id = "'.$itemid.'" AND wk_end_date = "'.$calcdate.'"');
    			if($sql)
    			{
    				
    		       DB::update('UPDATE recap SET item_amt="'.$itemamt.'" WHERE item_id="'.$itemid.'" AND wk_end_date="'.$calcdate.'"');
		   	  	}
		   	  else
		   	  {
		   		
		   	  	 DB::insert('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
			      VALUES("'.$itemid.'","'.$itemamt.'", "'.$calcdate.'")');
		   	  }
		 return 1;  	  
    }


    	
    public function getwklySalesPrint()
    {
    	return View::make('mktmgr.wklysalesprint'); 
    }
    public function postwklySalesPrint()
    {
    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
      
        
        $validator = Validator::make(Input::all(),
            array(                   
                   'end_date'=>'required|date|weekenddatecheck',
                ),array(
                	'end_date.required' => 'This date is required'
                	));
         //echo '<pre>',print_r($validator);exit;
	    if($validator->fails())
	    {
	        return Redirect::route('bookspro-weeklysalesprint')
	                    ->withErrors($validator)
	                    ->withInput();
	    }
	    else
	    {
	    	//echo '<pre>',print_r(Input::all());exit;
	    	$end_date = Input::get('end_date');
	    	$upd_bkpsig = $this->upd_bkpsig($end_date);
	    	$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
	    	$pwmdc = array();
	    	$misc_tabl_data = DB::select('SELECT misc.groc_num as str_num,misc.drug_num as drug_num FROM misc');
	    	$pwmdc['per'] = $this->get_per($end_date);
	    	$pwmdc['str_num'] = $misc_tabl_data[0]->str_num;
	    	$pwmdc['drug_num'] = $misc_tabl_data[0]->drug_num;
	    	$pwmdc['end_date'] = date('m/d/Y',strtotime($end_date));
	    	//$get_custcnt = $this->get_custcnt($end_date);
	    	$out_mainrpt1 = $this->out_mainrpt1($end_date,$begin_date);
	    	//$out_mainrpt2 = $this->out_mainrpt2($end_date,$begin_date);
	    	$out_netsaferpt = $this->out_netsaferpt($end_date,$begin_date);
	    	$out_deprpt = $this->out_deprpt($end_date,$begin_date);
	    	$out_strpt = $this->out_strpt($end_date,$begin_date);
	    	$out_porpt = $this->out_porpt($end_date,$begin_date);
	    	$get_netstore = $this->get_netstore($end_date);
	    	if(!($get_netstore))
        	{
        		$get_netstore = 0;;
        	}
        	$out_summaryrpt = $this->out_summaryrpt($end_date,$begin_date);
        	$out_comments = $this->out_comments($end_date,$begin_date);
	    	//echo '<pre>',print_r($out_mainrpt);exit;
	    	return View::make('mktmgr.wklysalesprintdata',compact('pwmdc','out_mainrpt1','out_netsaferpt','out_deprpt','out_strpt','out_porpt','get_netstore','out_summaryrpt','out_comments')); 
	    }
    }
    // public function get_custcnt($end_date)
    // {
    // 	DB::delete('delete from recapmain where recapmain.dept_id = "CC" and recapmain.wk_end_date = "'.$end_date.'"
		  //   and dept_sales = 0 
		  //   and cust_cnt = 0');
    // }
    public function upd_bkpsig($end_date)
    {
    	//echo 'SELECT bkp_sig FROM bkpsig WHERE recap_date = "'.$end_date.'" ';exit;
    	$bkp_sig_query = DB::select('SELECT bkp_sig FROM bkpsig WHERE recap_date = "'.$end_date.'" ');
    	if(!$bkp_sig_query)
    	{	
    		$today_date = date('Y-m-d H:i:s');//exit;
    		//echo 'Empty';exit;
    		DB::insert('INSERT INTO bkpsig VALUES ("book1","'.$end_date.'","'.$today_date.'")');
    	}
    }
    public function out_mainrpt1($end_date,$begin_date)
    {
    	$main_rec_arr = array();
    	$main_rec_array = array();
    	$fishgame = array();
    	$recapitems_query = DB::select('SELECT recapitems.item_id FROM recapitems
        WHERE recapitems.item_type =  "M"
          AND recapitems.item_id NOT IN ("CC","PC") order by acct_num asc');
    	$recapitems_array = json_decode(json_encode($recapitems_query), true);
    	for($i=0;$i<count($recapitems_array);$i++)
    	{
    		$item_id = $recapitems_array[$i]['item_id'];
    		$main_rec = DB::select('SELECT * FROM recapmain
	      	WHERE recapmain.wk_end_date = "'.$end_date.'"
	         AND  recapmain.dept_id = "'.$item_id.'"');
    		if(!($main_rec))
    		{
    			$get_acctdesc = $this->get_acctdesc($item_id);
    			$main_rec_arr['item_desc'] = $get_acctdesc[0]['item_desc'];
    			$main_rec_arr['dept_id'] = $item_id;
    			$main_rec_arr['dept_sales'] = 0;
    			$main_rec_arr['cust_cnt'] = 0;

    			$get_acctnum = $this->get_acctnum($item_id);
	    		$acctnum = $get_acctdesc[0]['acctnum'];
	        	$first = substr($acctnum, 0, 5);
	        	$second = substr($acctnum, 5, 2);
	        	$third = substr($acctnum,  -4, -1);
	        	if($third = "SSS")
	        	{
	        		$groc_num = DB::select('SELECT groc_num as third FROM misc');
	        		$third = $groc_num[0]->third;
	        	}
	        	$main_rec_arr['acctnum'] =  $first.' '.$second.' '.$third.' '.'0';

	        	$get_adj = $this->get_adj($item_id,$end_date);
	        	$main_rec_arr['fishgame'] = $get_adj[0];
	        	$main_rec_arr['aradj'] = $get_adj[1];
	        	$main_rec_arr['misc'] = $get_adj[2];
	        	$main_rec_arr['rowTotal'] = 0 + $get_adj[0] + $get_adj[1] + $get_adj[2];
	        	//echo '<pre>',print_r($get_adj);exit;//exit; 

	        	$main_rec_array[] = $main_rec_arr;
    		}
    		else
    		{
    			$get_acctdesc = $this->get_acctdesc($main_rec[0]->dept_id);
    			$main_rec_arr['item_desc'] = $get_acctdesc[0]['item_desc'];
    			$main_rec_arr['dept_id'] = $main_rec[0]->dept_id;
    			$main_rec_arr['dept_sales'] = $main_rec[0]->dept_sales/100;
    			$main_rec_arr['cust_cnt'] = $main_rec[0]->cust_cnt;

    			$get_acctnum = $this->get_acctnum($main_rec[0]->dept_id);
	    		$acctnum = $get_acctdesc[0]['acctnum'];
	        	$first = substr($acctnum, 0, 5);
	        	$second = substr($acctnum, 5, 2);
	        	$third = substr($acctnum,  -4, -1);
	        	if($third = "SSS")
	        	{
	        		$groc_num = DB::select('SELECT groc_num as third FROM misc');
	        		$third = $groc_num[0]->third;
	        	}
	        	$main_rec_arr['acctnum'] =  $first.' '.$second.' '.$third.' '.'0';

	        	$get_adj = $this->get_adj($main_rec[0]->dept_id,$end_date);
	        	$main_rec_arr['fishgame'] = $get_adj[0];
	        	$main_rec_arr['aradj'] = $get_adj[1];
	        	$main_rec_arr['misc'] = $get_adj[2];	
	        	//echo '<pre>',print_r($get_adj);exit;
	        	$main_rec_arr['rowTotal'] = ($main_rec[0]->dept_sales/100) + $get_adj[0] + $get_adj[1] + $get_adj[2];
				$main_rec_array[] = $main_rec_arr;
    		}
    	}//exit;
    	return $main_rec_array;
    	//echo '<pre>',print_r($main_rec_array);exit;//exit;
    }
    public function out_mainrpt2($end_date,$begin_date)
    {
    	$rpt_xmaincurs_query = DB::select('SELECT recapitems.item_id FROM recapitems
        WHERE recapitems.item_type =  "J"');
    	$rpt_xmaincurs_array = json_decode(json_encode($rpt_xmaincurs_query), true);
    	//echo '<pre>',print_r($rpt_xmaincurs_array);exit;
    	for($i=0;$i<count($rpt_xmaincurs_array);$i++)
    	{
    		$item_id = $rpt_xmaincurs_array[$i]['item_id'];
    		//echo $
    	}
    }
    public function out_netsaferpt($end_date,$begin_date)
    {
    	$net_safe_array = array();
    	$net_safe_array_final = array();
    	$out_netsaferpt = DB::select('SELECT recapmain.dept_id,recapmain.dept_sales/100 as dept_sales
        FROM recapmain WHERE recapmain.wk_end_date = "'.$end_date.'" AND recapmain.dept_id IN ("BS","ES","GO") order by dept_id desc');
        $out_netsaferpt_array = json_decode(json_encode($out_netsaferpt), true);
        for($i = 0;$i<count($out_netsaferpt_array);$i++)
        {	
        	$dept_id = $out_netsaferpt_array[$i]['dept_id'];
        	$get_acctdesc = $this->get_acctdesc($dept_id);
        	$net_safe_array['item_desc'] = $get_acctdesc[0]['item_desc'];
        	$net_safe_array['dept_sales'] = $out_netsaferpt_array[$i]['dept_sales'];
        	$acctnum = $get_acctdesc[0]['acctnum'];
        	$first = substr($acctnum, 0, 5);
        	$second = substr($acctnum, 5, 2);
        	$third = substr($acctnum,  -4, -1);
        	if($third = "SSS")
        	{
        		$groc_num = DB::select('SELECT groc_num as third FROM misc');
        		$third = $groc_num[0]->third;
        	}
        	$net_safe_array['acctnum'] =  $first.' '.$second.' '.$third.' '.'0';//exit;
        	$net_safe_array_final[] = $net_safe_array;
        }
        //echo '<pre>',print_r($net_safe_array_final);
        //exit;
		return $net_safe_array_final;
    }
    public function out_deprpt($end_date,$begin_date)
    {
    	$final_deposits_array = array();
    	$out_deprpt_unique = DB::select('SELECT recap.item_id  FROM recap,safeitems WHERE recap.wk_end_date >= "'.$begin_date.'" AND recap.wk_end_date <= "'.$end_date.'" AND safeitems.item_type = "P" AND safeitems.item_id = recap.item_id AND recap.item_amt <> 0 AND recap.item_id <> "#C" GROUP BY recap.item_id ORDER BY recap.item_id desc,recap.wk_end_date asc');
    	$out_deprpt_unique_array = json_decode(json_encode($out_deprpt_unique), true);
    	for($i = 0;$i<count($out_deprpt_unique_array);$i++)
    	{
    		$item_id = $out_deprpt_unique_array[$i]['item_id'];
    		$out_deprpt = DB::select('SELECT recap.item_id,recap.item_amt/100 as item_amt, recap.wk_end_date,safeitems.item_desc  FROM recap,safeitems WHERE recap.wk_end_date >= "'.$begin_date.'" AND recap.wk_end_date <= "'.$end_date.'" AND safeitems.item_type = "P" AND safeitems.item_id = recap.item_id AND recap.item_amt <> 0 AND recap.item_id <> "#C" AND recap.item_id = "'.$item_id.'"   ORDER BY recap.item_id ,recap.wk_end_date asc');
    		$final_deposits_array[] = json_decode(json_encode($out_deprpt), true);
    		//echo '<pre>',print_r($out_deprpt_array);exit;
	    	// for($i = 0;$i<count($out_deprpt_array);$i++)
	    	// {

	    	// }	
    	}//exit;
    	return $final_deposits_array;
    	//echo '<pre>',print_r($final_deposits_array);exit;
    }
    public function out_strpt($end_date,$begin_date)
    {
    	$safe_transc_array = array();
    	$safe_transc_array_final = array();
    	$out_strpt = DB::select('SELECT recap.item_id,recap.item_amt/100 as item_amt,recapitems.item_desc
        FROM recap,recapitems
          WHERE recap.wk_end_date >= "'.$begin_date.'"
            AND recap.wk_end_date <= "'.$end_date.'"
            AND recap.item_amt <> 0
            AND recapitems.item_type = "S"
            AND recapitems.item_id = recap.item_id
            AND recap.item_id <> "RR"
            AND recap.item_id <> "LR"
            AND recap.item_id <> "TD"
           ORDER BY  recapitems.item_desc');
    	$out_strpt_array = json_decode(json_encode($out_strpt), true);
    	for($i = 0;$i<count($out_strpt_array);$i++)
    	{
    		$item_id = $out_strpt_array[$i]['item_id'];
        	$get_acctdesc = $this->get_acctdesc($item_id);
        	$safe_transc_array['item_desc'] = $out_strpt_array[$i]['item_desc'];
        	$safe_transc_array['item_amt'] = $out_strpt_array[$i]['item_amt'];
        	$acctnum = $get_acctdesc[0]['acctnum'];
        	$first = substr($acctnum, 0, 5);
        	$second = substr($acctnum, 5, 2);
        	$third = substr($acctnum,  -4, -1);
        	//echo $third.'<br>';//exit;
        	if($third == "SSS")
        	{
        		$groc_num = DB::select('SELECT groc_num as third FROM misc');
        		$third = $groc_num[0]->third;
        	}
        	$safe_transc_array['acctnum'] =  $first.' '.$second.' '.$third.' '.'0';//exit;
        	$safe_transc_array_final[] = $safe_transc_array;
    	}
    	return $safe_transc_array_final;
    }
    public function out_porpt($end_date,$begin_date)
    {
    	$paid_out_types_array = array();
    	$paid_out_types_array_final = array();
    	$out_porpt = DB::select('SELECT recap.item_id,recap.item_amt/100 as item_amt,recapitems.item_desc
        FROM recap,recapitems
          WHERE recap.wk_end_date >= "'.$begin_date.'"
            AND recap.wk_end_date <= "'.$end_date.'"
            AND recap.item_amt <> 0
            AND recapitems.item_type = "O"
            AND recapitems.item_id = recap.item_id
           ORDER BY  recapitems.item_desc');
    	$out_porpt_array = json_decode(json_encode($out_porpt), true);
    	for($i = 0;$i<count($out_porpt_array);$i++)
    	{
    		$item_id = $out_porpt_array[$i]['item_id'];
        	$get_acctdesc = $this->get_acctdesc($item_id);
        	$paid_out_types_array['item_desc'] = $out_porpt_array[$i]['item_desc'];
        	$paid_out_types_array['item_amt'] = $out_porpt_array[$i]['item_amt'];
        	$acctnum = $get_acctdesc[0]['acctnum'];
        	$first = substr($acctnum, 0, 5);
        	$second = substr($acctnum, 5, 2);
        	$third = substr($acctnum,  -4, -1);
        	//echo $third.'<br>';//exit;
        	if($third == "SSS")
        	{
        		$groc_num = DB::select('SELECT groc_num as third FROM misc');
        		$third = $groc_num[0]->third;
        	}
        	$paid_out_types_array['acctnum'] =  $first.' '.$second.' '.$third.' '.'0';//exit;
        	$paid_out_types_array_final[] = $paid_out_types_array;
    	}
    	return $paid_out_types_array_final;
    }
    public function get_netstore($end_date)
    {
    	$get_netstore = DB::select('SELECT sum(recap.item_amt)/100 
        as netstore
        FROM recap,recapitems
          WHERE recapitems.item_id = recap.item_id 
            AND recapitems.item_type IN ("O","S")
            AND recap.wk_end_date = "'.$end_date.'"
            AND recap.item_id  NOT IN ("RR","PS","NG")');
    	//echo '<pre>',print_r($get_netstore);exit;
    	//$get_netstore_array = json_decode(json_encode($get_netstore), true);
    	if($get_netstore)
    	{
    		$netstore = $get_netstore[0]->netstore;
    	}
    	else
    	{
    		$netstore = 0;
    	}
    	return $netstore;//exit;
    }
    public function out_summaryrpt($end_date,$begin_date)
    {
    	$summary_report = array();
    	//$summary_report_final = array();
    	$totalsales = DB::select('SELECT SUM(recapmain.dept_sales)/100  as totalsales
        FROM recapmain,recapitems
          WHERE recapmain.wk_end_date = "'.$end_date.'"
            AND recapitems.item_type IN ("M","J","A")
            AND recapmain.dept_id = recapitems.item_id
            AND recapmain.dept_id NOT IN ("CC")');
    	$summary_report['totalsales'] = $totalsales[0]->totalsales;

    	$netsafe = DB::select('SELECT SUM(recapmain.dept_sales)/100  as netsafe FROM recapmain,recapitems
          WHERE recapmain.wk_end_date = "'.$end_date.'"
            AND recapitems.item_type = "I"
            AND recapmain.dept_id  = recapitems.item_id ');
		$summary_report['netsafe'] = $netsafe[0]->netsafe;

        $deposits = DB::select('SELECT SUM(recap.item_amt)/100  as deposits
        FROM recap,recapitems
          WHERE recap.wk_end_date <= "'.$end_date.'"
            AND recap.wk_end_date >= "'.$begin_date.'"
            AND recapitems.item_type  = "P"
            AND recap.item_id  = recapitems.item_id');
        $summary_report['deposits'] = $deposits[0]->deposits;

        $del_tranfs = DB::select('SELECT SUM(recap.item_amt)/100 as del_tranfs FROM recap,recapitems
          WHERE recap.wk_end_date <= "'.$end_date.'"
            AND recap.wk_end_date >= "'.$begin_date.'"
            AND recapitems.item_type  = "T"
            AND recap.item_id  = recapitems.item_id');
        $summary_report['del_tranfs'] = $del_tranfs[0]->del_tranfs;

        $po_types = DB::select('SELECT SUM(recap.item_amt)/100 as po_types  FROM recap,recapitems
          WHERE recap.wk_end_date = "'.$end_date.'"
            AND recapitems.item_type  IN ("S","O")
            AND recap.item_id  = recapitems.item_id
            AND recap.item_id  NOT IN ("RR","PS","NG")');
        $summary_report['po_types'] = $po_types[0]->po_types;
        $summary_report['salessum'] =$totalsales[0]->totalsales + $netsafe[0]->netsafe + $deposits[0]->deposits + $del_tranfs[0]->del_tranfs + $po_types[0]->po_types;
    	return $summary_report;
    }
    public function out_comments($end_date,$begin_date)
    {
    	$comment_array = array();
    	$comment_array_final = array();
    	$out_comments = DB::select('SELECT * FROM comment
            WHERE comment.date_stamp >= "'.$begin_date.'"
            AND comment.date_stamp <= "'.$end_date.'"
           ORDER BY  comment.date_stamp');
    	$out_comments_array = json_decode(json_encode($out_comments), true);
    	for($i=0;$i<count($out_comments_array);$i++)
    	{
    		$comment_array['date_stamp'] = $out_comments_array[$i]['date_stamp'];
    		$comment_array['comment_line'] = $out_comments_array[$i]['comment_line1'].' '.$out_comments_array[$i]['comment_line2'].' '.$out_comments_array[$i]['comment_line3'].' '.$out_comments_array[$i]['comment_line4'].' '.$out_comments_array[$i]['comment_line5'].' '.$out_comments_array[$i]['comment_line6'].' '.$out_comments_array[$i]['comment_line7'].' '.$out_comments_array[$i]['comment_line8'].' '.$out_comments_array[$i]['comment_line9'];
    		$comment_array_final[] = $comment_array;
    	}
		return $comment_array_final;
    }
    public function get_acctnum($id)
    {
    	$acctnum = 31500000000;
    	$out_netsaferpt = DB::select('SELECT recapitems.acct_num as acctnum FROM recapitems
       WHERE recapitems.item_id = "'.$id.'" ');
    	if($out_netsaferpt)
    	{
    		$acctnum = $out_netsaferpt[0]->acctnum;
    	}
    	return $acctnum;
    }
    public function get_acctdesc($id)
    {
    	$get_acctdesc = DB::select('SELECT recapitems.item_desc as item_desc, recapitems.acct_num as acctnum
         FROM recapitems
         WHERE recapitems.item_id = "'.$id.'" ');
    	$get_acctdesc_array = json_decode(json_encode($get_acctdesc), true);
		return $get_acctdesc_array;
    }
    public function get_adj($id,$end_date)
    {
    	$get_adj_array = array();
    	$get_adj_array_final = array();
    	$get_adj = DB::select('SELECT fgid,arid,miscid FROM recapitems  WHERE recapitems.item_id = "'.$id.'" ');
    	//echo 'fgid '.$get_adj[0]->fgid.'<br>';
    	$id1 = $get_adj[0]->fgid;
    	$id2 = $get_adj[0]->arid;
    	$id3 = $get_adj[0]->miscid;
    	if($id1)
    	{
    	 	$fishgame_query = DB::select('SELECT recapmain.dept_sales as fishgame FROM recapmain WHERE  recapmain.dept_id = "'.$id1.'"
         	AND  recapmain.wk_end_date = "'.$end_date.'" ');
         	if($fishgame_query)
         	{
         		$get_adj_array['fishgame'] = $fishgame_query[0]->fishgame;
         	}
         	else
         	{
         		$get_adj_array['fishgame'] = 0;
         	}
    		
    	}
    	else
     	{
     		$get_adj_array['fishgame'] = 0;
     	}
     	$get_adj_array_final[] = $get_adj_array['fishgame'];
    	//echo 'fishgame '.$fishgame.'<br>';

    	if($id2)
    	{
    	 	$aradj_query = DB::select('SELECT recapmain.dept_sales as aradj FROM recapmain WHERE  recapmain.dept_id = "'.$id2.'"
         	AND  recapmain.wk_end_date = "'.$end_date.'" ');
         	if($aradj_query)
         	{
         		$get_adj_array['aradj'] = $aradj_query[0]->aradj;
         	}
         	else
         	{
         		$get_adj_array['aradj'] = 0;
         	}
    		
    	}
    	else
     	{
     		$get_adj_array['aradj'] = 0;
     	}
     	$get_adj_array_final[] = $get_adj_array['aradj'];
     	//echo 'aradj '.$aradj.'<br>';

     	if($id3)
    	{
    	 	$misc_query = DB::select('SELECT recapmain.dept_sales as misc FROM recapmain WHERE  recapmain.dept_id = "'.$id3.'"
         	AND  recapmain.wk_end_date = "'.$end_date.'" ');
         	if($misc_query)
         	{
         		$get_adj_array['misc'] = $misc_query[0]->misc;
         	}
         	else
         	{
         		$get_adj_array['misc'] = 0;
         	}
    		
    	}
    	else
     	{
     		$get_adj_array['misc'] = 0;
     	}
     	$get_adj_array_final[] = $get_adj_array['fishgame'];
     	//echo 'misc '.$misc.'<br>';
     	//echo '<pre>';print_r($get_adj_array);
     	return $get_adj_array_final;
    }
}
?>