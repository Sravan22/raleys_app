<?php 

//namespace App\Http\Controllers;
/*use App\Http\Requests;
use Illuminate\Http\Request;
use App\Product as Product;*/
class PDFGenerator extends Controller
{
    public function htmltopdfview()
    {
         $regtype = Input::get('regtype');
        $regnumber = Input::get('regnumber');
        
        $searchresult = DB::select('select * from registers order by reg_num'); 
        
        if(Input::has('regtype')) 
        {    
            $searchresult = DB::select('select * from registers where reg_type = "'.$regtype.'" ');
        }
        if(Input::has('regnumber')) 
        {    
            $searchresult = DB::select('select * from registers where reg_num = "'.$regnumber.'" ');
        }
        if(Input::has('regnumber') && Input::has('regtype')) 
        {    
            $searchresult = DB::select('select * from registers where reg_num = "'.$regnumber.'" and reg_type = "'.$regtype.'"');
        }
    $pdf = PDF::loadView('pdf.htmltopdfview', compact('searchresult'));
         return $pdf->stream();

    }

    public function getTransporterList()
    {
        $transports = new Socadm00Sodsdtrn;
        $transports = $transports->select('trns_number' , 'name' , 'address' , 'city' , 'state' , 'zip_code' , 'phone')->orderBy('name', 'asc')->paginate(100);
        $pdf = PDF::loadView('pdf.transportlistingpdf', compact('transports'));
        return $pdf->stream();
    }

    public function getPdfExpensesReport()
    {
         $receivings = Socadm00Sodsdhdr::query();
         if(Input::get('frmdt'))
         {
            $receivings->whereBetween('invoice_date', array(Input::get('frmdt'), Input::get('todt')));   
         }
         $receivings->leftJoin('sodsdvnd', 'sodsdhdr.vendor_number', '=', 'sodsdvnd.vendor_number');
         $receivings = $receivings->paginate(100);
         $pdf = PDF::loadView('pdf.expensesreport', compact('receivings'));
         return $pdf->stream();
    }
    
    public function getPdfReportNonParScan()
    {   


        $work_data = new StdClass();
        
        $dept_inp = Input::get('deptno');

        
        if($dept_inp==0 || $dept_inp==''){        
            Session::flash('alert-danger', 'Requested dept. cannot found.'); 
            return Redirect::route('mktmgr-nonperishableinvprepview');
        }
        
        
        $sodinvno = Input::get('sodinvno');
          
        if($sodinvno==0 || $sodinvno==''){        
            Session::flash('alert-danger', 'Requested inventory number cannot found.'); 
            return Redirect::route('mktmgr-noninvdeptinfo',$dept_inp);
        }
        
        
        $sql='SELECT seq_number, item_desc, upc_cost, rtl_amt, quantity                         
        FROM sodinvdtl WHERE hdr_seq_number = '.$sodinvno.' AND sodinvdtl.status = "I"';
        
           $results = DB::select(DB::raw($sql));
//echo "<pre>";print_r($results); die;
           
           $sodinv_dtls_rec=array();
        if (!empty($results)) {

            $cnt=1;
            foreach($results as  $row ){
                $sodinv_dtls_rec[$cnt]['seq_number'] = $row->seq_number;
                $sodinv_dtls_rec[$cnt]['item_desc'] = $row->item_desc;
                $sodinv_dtls_rec[$cnt]['upc_cost'] = $row->upc_cost;
                $sodinv_dtls_rec[$cnt]['rtl_amt'] = $row->rtl_amt;
                $sodinv_dtls_rec[$cnt]['quantity'] = $row->quantity;

                    $sodinv_dtls_rec[$cnt]['ext_value_cst'] = ( $sodinv_dtls_rec[$cnt]['upc_cost'] * $sodinv_dtls_rec[$cnt]['quantity']); 
                    $sodinv_dtls_rec[$cnt]['ext_value_rtl'] = ( $sodinv_dtls_rec[$cnt]['rtl_amt'] * $sodinv_dtls_rec[$cnt]['quantity']);
                        
                    $cnt++;
            }
        }
        
        
        $sql='SELECT create_date, inventory_number, employee_id, total_scans, total_value_cst, total_value_rtl, total_crv_rtl, sodinvhdr.status as invhdr_status, inventory_type, inventory_area, inventory_section
        FROM sodinvhdr
       WHERE dept_number = '.$dept_inp.' AND inventory_number = '.$sodinvno.' AND sodinvhdr.status != "D"
         AND sodinvhdr.status != "T"
    ORDER BY status, inventory_area, inventory_section, inventory_number';

         
         $results = DB::select(DB::raw($sql));
//echo "<pre>";print_r($results); die;
        $sodinv_hdrs_rec=array();

        if (!empty($results)) {

            $cnt=1;
            foreach($results as  $row ) {
                
                
                            $sodinv_hdrs_rec[$cnt]['create_date']=$row->create_date;
                            $sodinv_hdrs_rec[$cnt]['inventory_number']=$row->inventory_number;
                            $sodinv_hdrs_rec[$cnt]['employee_id']=$row->employee_id;
                            $sodinv_hdrs_rec[$cnt]['total_scans']=$row->total_scans;
                            $sodinv_hdrs_rec[$cnt]['total_value_cst']=$row->total_value_cst;
                            $sodinv_hdrs_rec[$cnt]['total_value_rtl']=$row->total_value_rtl;
                            $sodinv_hdrs_rec[$cnt]['total_crv_rtl']=$row->total_crv_rtl;
                            $sodinv_hdrs_rec[$cnt]['invhdr_status']=$row->invhdr_status;
                            $sodinv_hdrs_rec[$cnt]['inventory_type']=$row->inventory_type;
                            $sodinv_hdrs_rec[$cnt]['inventory_area']=$row->inventory_area; 
                            $sodinv_hdrs_rec[$cnt]['inventory_section']=$row->inventory_section; 

                    $ws_status = $sodinv_hdrs_rec[$cnt]['invhdr_status'];

                    $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Unknown";

                    if( $ws_status == "O"){
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Open";
                    }

                    if($ws_status == "V"){
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Void";
                    }

                    if( $ws_status == "D") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Deleted";
                    }

                    if( $ws_status == "A") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Approved";
                    }

                    if( $ws_status == "C") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Closed";
                    }

                    if( $ws_status == "S") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Sent";
                    }

                    if( $ws_status == "R") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Reapproved";
                    }

                    if( $ws_status == "T") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Transmitted";
                    }

                    

                    

            }
        }
        
        
        
        $work_data->sodinv_dtls_rec=$sodinv_dtls_rec;
        $work_data->sodinv_hdrs_rec=$sodinv_hdrs_rec[1];
        $work_data->sodinvno=$sodinvno;
        $work_data->dept_inp=$dept_inp;
        $pdf = PDF::loadView('pdf.reportnonparscan', compact('work_data'));
        return $pdf->stream();
    }

    public function getPdfReportWeekly()
    {
        $storeno = Input::get('storeno'); $department = Input::get('department');  $fromdate = Input::get('fromdate');  
        $todate = Input::get('todate'); 

        $results = DB::select('SELECT sodsdhdr . seq_number , sodsdhdr . method_rcvd , sodsdhdr . type_code , sodsdhdr . dept_number , sodsdhdr . delivery_unit , sodsdhdr . id , sodsdhdr . vendor_number , sodsdhdr . tot_vend_cost , sodsdhdr . tot_vend_allow , sodsdhdr . tot_vend_crv , sodsdhdr . invoice_date , sodsdhdr . tot_sell_supply , sodsdhdr . tot_store_supply , sodsdvnd . name FROM sodsdhdr , sodsdvnd WHERE sodsdhdr . store_number = ? AND sodsdhdr . dept_number = ? AND ( sodsdhdr . status_code = "O" OR sodsdhdr . status_code = "A" ) AND sodsdhdr . create_datetime >= ? AND sodsdhdr . create_datetime <= ? AND sodsdvnd . vendor_number = sodsdhdr . vendor_number ORDER BY sodsdvnd . name , sodsdhdr . invoice_date , sodsdhdr . id', array($storeno, $department, $fromdate, $todate));

        $pdf = PDF::loadView('pdf.reportweekly', compact('results'));
        return $pdf->stream();
    }
    public function getPdfReportNonParDept()
    {
        //echo 'Hiiii';exit;
           $work_data = new StdClass();
        
        $dept_inp = Input::get('deptno');
       
        
        if($dept_inp==0 || $dept_inp==''){        
            Session::flash('alert-danger', 'Requested dept. cannot found.'); 
            return Redirect::route('mktmgr-nonperishableinvprepview');
        }
        
        
         //DECLARE sodinv_hdrs CURSOR FOR
      $sql='SELECT create_date, inventory_number, employee_id, total_scans, total_value_cst, total_value_rtl, total_crv_rtl, sodinvhdr.status as invhdr_status, inventory_type, inventory_area, inventory_section
        FROM sodinvhdr
       WHERE dept_number = '.$dept_inp.' AND sodinvhdr.status != "D"
         AND sodinvhdr.status != "T"
    ORDER BY status, inventory_area, inventory_section, inventory_number';

         
         $results = DB::select(DB::raw($sql));
//echo "<pre>";print_r($results); die;
        $sodinv_hdrs_rec=array();
        $sodinv_hdrs_non_disp_rec=array();
        if (!empty($results)) {

            $cnt=1;
            foreach($results as  $row ) {
                
                
                            $sodinv_hdrs_rec[$cnt]['create_date']=$row->create_date;
                            $sodinv_hdrs_rec[$cnt]['inventory_number']=$row->inventory_number;
                            $sodinv_hdrs_rec[$cnt]['employee_id']=$row->employee_id;
                            $sodinv_hdrs_rec[$cnt]['total_scans']=$row->total_scans;
                            $sodinv_hdrs_rec[$cnt]['total_value_cst']=$row->total_value_cst;
                            $sodinv_hdrs_rec[$cnt]['total_value_rtl']=$row->total_value_rtl;
                            $sodinv_hdrs_rec[$cnt]['total_crv_rtl']=$row->total_crv_rtl;
                            $sodinv_hdrs_rec[$cnt]['invhdr_status']=$row->invhdr_status;
                            $sodinv_hdrs_rec[$cnt]['inventory_type']=$row->inventory_type;
                            $sodinv_hdrs_rec[$cnt]['inventory_area']=$row->inventory_area; 
                            $sodinv_hdrs_rec[$cnt]['inventory_section']=$row->inventory_section; 

                    $ws_status = $sodinv_hdrs_rec[$cnt]['invhdr_status'];

                    $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Unknown";

                    if( $ws_status == "O"){
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Open";
                    }

                    if($ws_status == "V"){
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Void";
                    }

                    if( $ws_status == "D") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Deleted";
                    }

                    if( $ws_status == "A") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Approved";
                    }

                    if( $ws_status == "C") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Closed";
                    }

                    if( $ws_status == "S") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Sent";
                    }

                    if( $ws_status == "R") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Reapproved";
                    }

                    if( $ws_status == "T") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Transmitted";
                    }

                    $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Unknown";

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "ED") { 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Everyday Stock"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "VL") { 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Valentines Day"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "ES"){ 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Easter"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "SM"){ 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Spring/Summer"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "HR"){
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Harvest"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "HL"){ 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Halloween"; 
                    }

                    if($sodinv_hdrs_rec[$cnt]['inventory_type'] == "CH"){
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Christmas"; 
                    }

                    $cnt++;

            }
        }
        
        
        $work_data->sodinv_hdrs_rec=$sodinv_hdrs_rec;
        $work_data->sodinv_hdrs_non_disp_rec=$sodinv_hdrs_non_disp_rec;
        $work_data->dept_inp=$dept_inp;        


        $pdf = PDF::loadView('pdf.reportnonpardept', compact('work_data'));
        return $pdf->stream();
    }

      public function getPdfReportNonPar()
    {
        $work_data = new StdClass();

        $dept_sodinv_rec[1]['dept_no'] = 1;
        $dept_sodinv_rec[1]['desc'] = "Grocery";
        $dept_sodinv_rec[1]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[1]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[1]['tot_crv_rtl'] = 0.00;

        $dept_sodinv_rec[2]['dept_no'] = 2;
        $dept_sodinv_rec[2]['desc'] = "Liquor";
        $dept_sodinv_rec[2]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[2]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[2]['tot_crv_rtl'] = 0.00;

        $dept_sodinv_rec[3]['dept_no'] = 4;
        $dept_sodinv_rec[3]['desc'] = "Deli";
        $dept_sodinv_rec[3]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[3]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[3]['tot_crv_rtl'] = 0.00;

        $dept_sodinv_rec[4]['dept_no'] = 8;
        $dept_sodinv_rec[4]['desc'] = "Nat Fds";
        $dept_sodinv_rec[4]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[4]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[4]['tot_crv_rtl'] = 0.00;

        $dept_sodinv_rec[5]['dept_no'] = 9;
        $dept_sodinv_rec[5]['desc'] = "GM";
        $dept_sodinv_rec[5]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[5]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[5]['tot_crv_rtl'] = 0.00;

        $dept_01_rec_cnt = 0;
        $dept_02_rec_cnt = 0;
        $dept_04_rec_cnt = 0;
        $dept_08_rec_cnt = 0;
        $dept_09_rec_cnt = 0;

        

        $sql = 'SELECT count(*) as total FROM sodinvhdr
    WHERE dept_number = 1
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S") ';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[1]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[1]['num_of_invs'] = $row_data->total;
            }
        }


        $sql = 'SELECT count(*) as total   
     FROM sodinvhdr
    WHERE dept_number = 2
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S")';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[2]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[2]['num_of_invs'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S")';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[3]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[3]['num_of_invs'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S")';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[4]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[4]['num_of_invs'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S")';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[5]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[5]['num_of_invs'] = $row_data->total;
            }
        }
        
        
        

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as  total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_S'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_S'] = $row_data->total;
            }
        }


        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_S'] = $row_data->total;
            }
        }


        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_S'] = $row_data->total;
            }
        }


        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_S'] = $row_data->total;
            }
        }
        
        
       
    

        $dept_01_tot_value_cst = $dept_01_tot_value_rtl = $dept_01_tot_crv_rtl = 0;

        $sql = 'SELECT sum(total_value_cst) as dept_01_tot_value_cst, sum(total_value_rtl) as dept_01_tot_value_rtl, sum(total_crv_rtl) as dept_01_tot_crv_rtl FROM sodinvhdr
    WHERE dept_number = 01
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_01_tot_value_cst = $row_data->dept_01_tot_value_cst;
                $dept_01_tot_value_rtl = $row_data->dept_01_tot_value_rtl;
                $dept_01_tot_crv_rtl = $row_data->dept_01_tot_crv_rtl;
            }
        }

        $dept_02_tot_value_cst = $dept_02_tot_value_rtl = $dept_02_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_02_tot_value_cst, sum(total_value_rtl) as dept_02_tot_value_rtl, sum(total_crv_rtl) as dept_02_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 02
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_02_tot_value_cst = $row_data->dept_02_tot_value_cst;
                $dept_02_tot_value_rtl = $row_data->dept_02_tot_value_rtl;
                $dept_02_tot_crv_rtl = $row_data->dept_02_tot_crv_rtl;
            }
        }



        $dept_03_tot_value_cst = $dept_03_tot_value_rtl = $dept_03_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_03_tot_value_cst, sum(total_value_rtl) as dept_03_tot_value_rtl, sum(total_crv_rtl) as dept_03_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 03
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_03_tot_value_cst = $row_data->dept_03_tot_value_cst;
                $dept_03_tot_value_rtl = $row_data->dept_03_tot_value_rtl;
                $dept_03_tot_crv_rtl = $row_data->dept_03_tot_crv_rtl;
            }
        }


        $dept_04_tot_value_cst = $dept_04_tot_value_rtl = $dept_04_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_04_tot_value_cst, sum(total_value_rtl) as dept_04_tot_value_rtl, sum(total_crv_rtl) as dept_04_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 04
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_04_tot_value_cst = $row_data->dept_04_tot_value_cst;
                $dept_04_tot_value_rtl = $row_data->dept_04_tot_value_rtl;
                $dept_04_tot_crv_rtl = $row_data->dept_04_tot_crv_rtl;
            }
        }



        $dept_05_tot_value_cst = $dept_05_tot_value_rtl = $dept_05_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_05_tot_value_cst, sum(total_value_rtl) as dept_05_tot_value_rtl, sum(total_crv_rtl) as dept_05_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 05
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_05_tot_value_cst = $row_data->dept_05_tot_value_cst;
                $dept_05_tot_value_rtl = $row_data->dept_05_tot_value_rtl;
                $dept_05_tot_crv_rtl = $row_data->dept_05_tot_crv_rtl;
            }
        }


        $dept_06_tot_value_cst = $dept_06_tot_value_rtl = $dept_06_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_06_tot_value_cst, sum(total_value_rtl) as dept_06_tot_value_rtl, sum(total_crv_rtl) as dept_06_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 06
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_06_tot_value_cst = $row_data->dept_06_tot_value_cst;
                $dept_06_tot_value_rtl = $row_data->dept_06_tot_value_rtl;
                $dept_06_tot_crv_rtl = $row_data->dept_06_tot_crv_rtl;
            }
        }


        $dept_07_tot_value_cst = $dept_07_tot_value_rtl = $dept_07_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_07_tot_value_cst, sum(total_value_rtl) as dept_07_tot_value_rtl, sum(total_crv_rtl) as dept_07_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 07
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_07_tot_value_cst = $row_data->dept_07_tot_value_cst;
                $dept_07_tot_value_rtl = $row_data->dept_07_tot_value_rtl;
                $dept_07_tot_crv_rtl = $row_data->dept_07_tot_crv_rtl;
            }
        }


        $dept_08_tot_value_cst = $dept_08_tot_value_rtl = $dept_08_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_08_tot_value_cst, sum(total_value_rtl) as dept_08_tot_value_rtl, sum(total_crv_rtl) as dept_08_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 08
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_08_tot_value_cst = $row_data->dept_08_tot_value_cst;
                $dept_08_tot_value_rtl = $row_data->dept_08_tot_value_rtl;
                $dept_08_tot_crv_rtl = $row_data->dept_08_tot_crv_rtl;
            }
        }


        $dept_09_tot_value_cst = $dept_09_tot_value_rtl = $dept_09_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_09_tot_value_cst, sum(total_value_rtl) as dept_09_tot_value_rtl, sum(total_crv_rtl) as dept_09_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 09
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_09_tot_value_cst = $row_data->dept_09_tot_value_cst;
                $dept_09_tot_value_rtl = $row_data->dept_09_tot_value_rtl;
                $dept_09_tot_crv_rtl = $row_data->dept_09_tot_crv_rtl;
            }
        }


        $dept_10_tot_value_cst = $dept_10_tot_value_rtl = $dept_10_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_10_tot_value_cst, sum(total_value_rtl) as dept_10_tot_value_rtl, sum(total_crv_rtl) as dept_10_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 10
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_10_tot_value_cst = $row_data->dept_10_tot_value_cst;
                $dept_10_tot_value_rtl = $row_data->dept_10_tot_value_rtl;
                $dept_10_tot_crv_rtl = $row_data->dept_10_tot_crv_rtl;
            }
        }

        $dept_11_tot_value_cst = $dept_11_tot_value_rtl = $dept_11_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_11_tot_value_cst, sum(total_value_rtl) as dept_11_tot_value_rtl, sum(total_crv_rtl) as dept_11_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 11
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_11_tot_value_cst = $row_data->dept_11_tot_value_cst;
                $dept_11_tot_value_rtl = $row_data->dept_11_tot_value_rtl;
                $dept_11_tot_crv_rtl = $row_data->dept_11_tot_crv_rtl;
            }
        }



        $dept_35_tot_value_cst = $dept_35_tot_value_rtl = $dept_35_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_35_tot_value_cst, sum(total_value_rtl) as dept_35_tot_value_rtl, sum(total_crv_rtl) as dept_35_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 35
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_35_tot_value_cst = $row_data->dept_35_tot_value_cst;
                $dept_35_tot_value_rtl = $row_data->dept_35_tot_value_rtl;
                $dept_35_tot_crv_rtl = $row_data->dept_35_tot_crv_rtl;
            }
        }

        if ($dept_01_tot_value_cst > 0) {
            $dept_sodinv_rec[1]['tot_value_cst'] = $dept_01_tot_value_cst;
        }

        if ($dept_02_tot_value_cst > 0) {
            $dept_sodinv_rec[2]['tot_value_cst'] = $dept_02_tot_value_cst;
        }

        if ($dept_04_tot_value_cst > 0) {
            $dept_sodinv_rec[3]['tot_value_cst'] = $dept_04_tot_value_cst;
        }

        if ($dept_08_tot_value_cst > 0) {
            $dept_sodinv_rec[4]['tot_value_cst'] = $dept_08_tot_value_cst;
        }

        if ($dept_09_tot_value_cst > 0) {
            $dept_sodinv_rec[5]['tot_value_cst'] = $dept_09_tot_value_cst;
        }



        if ($dept_01_tot_value_rtl > 0) {
            $dept_sodinv_rec[1]['tot_value_rtl'] = $dept_01_tot_value_rtl;
        }

        if ($dept_02_tot_value_rtl > 0) {
            $dept_sodinv_rec[2]['tot_value_rtl'] = $dept_02_tot_value_rtl;
        }

        if ($dept_04_tot_value_rtl > 0) {
            $dept_sodinv_rec[3]['tot_value_rtl'] = $dept_04_tot_value_rtl;
        }

        if ($dept_08_tot_value_rtl > 0) {
            $dept_sodinv_rec[4]['tot_value_rtl'] = $dept_08_tot_value_rtl;
        }

        if ($dept_09_tot_value_rtl > 0) {
            $dept_sodinv_rec[5]['tot_value_rtl'] = $dept_09_tot_value_rtl;
        }



        if ($dept_01_tot_crv_rtl > 0) {
            $dept_sodinv_rec[1]['tot_crv_rtl'] = $dept_01_tot_crv_rtl;
        }

        if ($dept_02_tot_crv_rtl > 0) {
            $dept_sodinv_rec[2]['tot_crv_rtl'] = $dept_02_tot_crv_rtl;
        }

        if ($dept_04_tot_crv_rtl > 0) {
            $dept_sodinv_rec[3]['tot_crv_rtl'] = $dept_04_tot_crv_rtl;
        }

        if ($dept_08_tot_crv_rtl > 0) {
            $dept_sodinv_rec[4]['tot_crv_rtl'] = $dept_08_tot_crv_rtl;
        }

        if ($dept_09_tot_crv_rtl > 0) {
            $dept_sodinv_rec[5]['tot_crv_rtl'] = $dept_09_tot_crv_rtl;
        }
        
        
        
         $work_data->dept_sodinv_rec=$dept_sodinv_rec;
        $work_data->dept_sodinv_non_disp_rec=$dept_sodinv_non_disp_rec;


        $pdf = PDF::loadView('pdf.reportnonpar',compact('work_data'));
        return $pdf->stream();
    }
    public function getPdfstoretransconf()
    {
        $type_code = strtoupper(Input::get('type_code'));
        $subtype_code = strtoupper(Input::get('subtype_code'));
        $description = Input::get('description');
        $partition_code = strtoupper(Input::get('partition_code'));
        $dept_number = Input::get('dept_number');
        $max_item_length = Input::get('max_item_length');
        $max_qty = Input::get('max_qty');
        $neg_qty_flag = strtoupper(Input::get('neg_qty_flag'));
        $zero_qty_flag = strtoupper(Input::get('zero_qty_flag'));
        $return_flag = strtoupper(Input::get('return_flag'));
        $history_flag = strtoupper(Input::get('history_flag'));
        $vendor_acct_no = Input::get('vendor_acct_no');
        $active_switch = strtoupper(Input::get('active_switch'));
        $select = "select * from sotrncfg ";
        $where = 'where 1 = 1 ';
                    if(Input::has('type_code'))
                    {
                        $where.=" and type_code = '$type_code' ";
                    }
                    if(Input::has('subtype_code'))
                    {
                        $where.=" and subtype_code = '$subtype_code' ";
                    }
                    if(Input::has('description'))
                    {
                        $where.=" and description = '$description' ";
                    }
                    if(Input::has('partition_code'))
                    {
                        $where.=" and partition_code = '$partition_code' ";
                    }
                    if(Input::has('dept_number'))
                    {
                        $where.=" and dept_number = '$dept_number' ";
                    }
                    if(Input::has('max_item_length'))
                    {
                        $where.=" and max_item_length = '$max_item_length' ";
                    }
                    if(Input::has('max_qty'))
                    {
                        $where.=" and max_qty = '$max_qty' ";
                    }
                    if(Input::has('neg_qty_flag'))
                    {
                        $where.=" and neg_qty_flag = '$neg_qty_flag' ";
                    }
                    if(Input::has('zero_qty_flag'))
                    {
                        $where.=" and zero_qty_flag = '$zero_qty_flag' ";
                    }
                    if(Input::has('return_flag'))
                    {
                        $where.=" and return_flag = '$return_flag' ";
                    }
                    if(Input::has('history_flag'))
                    {
                        $where.=" and history_flag = '$history_flag' ";
                    }
                    if(Input::has('vendor_acct_no'))
                    {
                        $where.=" and vendor_acct_no = '$vendor_acct_no' ";
                    }
                    if(Input::has('active_switch'))
                      {
                        $where.=" and active_switch = '$active_switch' ";
                    }
                    $query=$select.$where;
                    //echo $query;exit;
                    $confbrowser = DB::select($query);
                    $pdf = PDF::loadView('pdf.reportstoretransconf', compact('confbrowser'));
                    return $pdf->stream();
    }


    public function getPdfStoreWeeklyRepot()
    {
        
        $start_date = Input::get('start_datec');
        $start_datec =date("m/d/Y",strtotime(trim(Input::get('start_datec'))));
        $end_date = Input::get('end_datec');
        $end_datec =date("m/d/Y",strtotime(trim(Input::get('end_datec'))));

        $out_dept_charges =DB::select('select * from soxfrhdr where soxfrhdr.create_datetime >= "'.$start_date.'" 
                        and soxfrhdr.create_datetime <= "'.$end_date.'"
                        and soxfrhdr.from_store_no = "305"
                        and soxfrhdr.from_dept_no in (select gl_dept from soxfrdpt
                        where soxfrdpt.gl_dept >= "0"
                        and soxfrdpt.gl_dept <= "100")
                        and soxfrhdr.from_account in (17100, 65100, 66000, 65000)
                        and soxfrhdr.status not in ("D", "V")
                        order by soxfrhdr.transfer_type, soxfrhdr.create_date, 
                        soxfrhdr.transfer_number, soxfrhdr.from_dept_no');

        $out_dept_credits =DB::select('select * from soxfrhdr where soxfrhdr.create_datetime >= "'.$start_date.'" 
                        and soxfrhdr.create_datetime <= "'.$end_date.'"
                        and soxfrhdr.from_store_no = "305"
                        and soxfrhdr.from_dept_no in (select gl_dept from soxfrdpt
                        where soxfrdpt.gl_dept >= "0"
                        and soxfrdpt.gl_dept <= "100")
                        and soxfrhdr.from_account in (17100, 65100, 66000, 65000)
                        and soxfrhdr.status not in ("D", "V")
                        order by soxfrhdr.transfer_type, soxfrhdr.create_date, 
                        soxfrhdr.transfer_number, soxfrhdr.from_dept_no');
            $reportquery_array = array_merge($out_dept_charges, $out_dept_credits);
            $reportquery_array = json_decode(json_encode($reportquery_array), true);

            $pdf = PDF::loadView('pdf.storetransferweeklyrecapreport',
                                 compact('reportquery_array', 'start_datec', 'end_datec'));
                
            $pdf->save('generatedreports/storetransferweeklyrecapreport.pdf');
            $title =  'Store Transfers Accounting Recap Report';
            $content ='Find the attachment as store transfer accounting recap report';
            $attach ='generatedreports/storetransferweeklyrecapreport.pdf';

        Mail::send('pdf.report', 
            ['title' => $title, 'content' => $content], function ($message) use ($attach)
        {

            $message->from('khizer@famsoft.com', 'Khizer Hussain');

            $message->to('khizer@famsoft.com');

            //Attach file
            $message->attach($attach);

            //Add a subject
            $message->subject("Store Transfers Accounting Recap Report");

        });

         return $pdf->stream();
    }

    public function getPdfStoreWeeklyStatusRepot()
    {
        $fromdate = Input::get('start_datec');
        $todate = Input::get('end_datec');
        
        $reportquery =DB::select('select * from soxfrhdr where soxfrhdr.create_datetime >= "'.$fromdate.'" 
                        and soxfrhdr.create_datetime <= "'.$todate.'"
                        and soxfrhdr.to_store_no = "305"
                        and soxfrhdr.to_dept_no in (select gl_dept from soxfrdpt
                        where soxfrdpt.gl_dept >= "0"
                        and soxfrdpt.gl_dept <= "100")
                        and soxfrhdr.from_account in (17100, 65100, 66000, 65000)
                        and soxfrhdr.status not in ("D", "V")
                        order by soxfrhdr.transfer_type, soxfrhdr.create_date, 
                        soxfrhdr.transfer_number, soxfrhdr.from_dept_no');
            $reportquery_array = json_decode(json_encode($reportquery), true);

            $fromdate =date("m/d/Y",strtotime(trim(Input::get('start_datec'))));
            $todate =date("m/d/Y",strtotime(trim(Input::get('end_datec'))));
            
            $pdf = PDF::loadView('pdf.storetransferweeklystatusreport',
                                 compact('reportquery_array', 'fromdate', 'todate'));
                
            $pdf->save('generatedreports/storetransferweeklystatusreport.pdf');
            $title =  'Store Transfers Weekly Status Report';
            $content ='Find the attachment as store transfer weekly status report';
            $attach ='generatedreports/storetransferweeklystatusreport.pdf';

        Mail::send('pdf.report', 
            ['title' => $title, 'content' => $content], function ($message) use ($attach)
        {

            $message->from('khizer@famsoft.com', 'Khizer Hussain');

            $message->to('khizer@famsoft.com');

            //Attach file
            $message->attach($attach);

            //Add a subject
            $message->subject("Store Transfers Weekly Status Report");

        });

         return $pdf->stream();
    }
    public function getSummaryReport()
    {
         $userid = strtoupper(Input::get('userid'));
        $update_date = strtoupper(Input::get('update_date'));
        $username = Input::get('username');
        $summary_report_query = DB::select('SELECT * FROM slhistry WHERE slhistry.userid = "'.$userid.'" AND slhistry.update_date = "'.$update_date.'" ORDER BY slhistry.seq_no');
        $summary_report_query_array = json_decode(json_encode($summary_report_query), true);
                    //echo $query;exit;
                    //$confbrowser = DB::select($query);
                    $pdf = PDF::loadView('pdf.summaryreport', compact('summary_report_query_array','username'));
                    return $pdf->stream();
    }
    public function getDetailReport()
    {
        $userid = Input::get('userid');
        $update_date = Input::get('update_date');
        $update_time = Input::get('update_time');
        $action_cd = Input::get('action_cd');
        $maint_type_cd = Input::get('maint_type_cd');
        $item_code = Input::get('item_code');
        $mstr_fld_no = Input::get('mstr_fld_no');
        $orig_value = Input::get('orig_value');
        $new_value = Input::get('new_value');
          $select = "select * from slhistry ";
            $where = 'where 1 = 1 ';
                    if(Input::has('userid'))
                    {
                        $where.=" and userid = '$userid' ";
                    }
                    if(Input::has('update_date'))
                    {
                        $where.=" and update_date = '$update_date' ";
                    }
                    if(Input::has('update_time'))
                    {
                        $where.=" and update_time = '$update_time' ";
                    }
                    if(Input::has('action_cd'))
                    {
                        $where.=" and action_cd = '$action_cd' ";
                    }
                    if(Input::has('maint_type_cd'))
                    {
                        $where.=" and maint_type_cd = '$maint_type_cd' ";
                    }
                    if(Input::has('item_code'))
                    {
                        $where.=" and item_code = '$item_code' ";
                    }
                    if(Input::has('mstr_fld_no'))
                    {
                        $where.=" and mstr_fld_no = '$mstr_fld_no' ";
                    }
                    if(Input::has('orig_value'))
                    {
                        $where.=" and orig_value = '$orig_value' ";
                    }
                    if(Input::has('new_value'))
                    {
                        $where.=" and new_value = '$new_value' ";
                    }
                    $query=$select.$where;
                    //echo $query;exit;
                    $detailreport_query = DB::select($query);
                    $detailreport_query_array = json_decode(json_encode($detailreport_query), true);
                    $pdf = PDF::loadView('pdf.detail_report', compact('detailreport_query_array'));
                    return $pdf->stream();
    }
    public function getFMReport()
    {
        $userid = Input::get('userid');
        $update_date = Input::get('update_date');
        $update_time = Input::get('update_time');
        $item_code = Input::get('item_code');
        $new_value = Input::get('new_value');
          $select = "select * from slhistry ";
            $where = 'where 1 = 1  and action_cd = "F" ';
                    if(Input::has('userid'))
                    {
                        $where.=" and userid = '$userid' ";
                    }
                    if(Input::has('update_date'))
                    {
                        $where.=" and update_date = '$update_date' ";
                    }
                    if(Input::has('update_time'))
                    {
                        $where.=" and update_time = '$update_time' ";
                    }
                    if(Input::has('item_code'))
                    {
                        $where.=" and item_code = '$item_code' ";
                    }
                    if(Input::has('new_value'))
                    {
                        $where.=" and new_value = '$new_value' ";
                    }
                    $query=$select.$where;
                    //echo $query;exit;
                    $fmreport_query = DB::select($query);
                    $fmreport_query_array = json_decode(json_encode($fmreport_query), true);
                    $pdf = PDF::loadView('pdf.fm_pdf_report', compact('fmreport_query_array'));
                    return $pdf->stream();
    }
    public function StoreTransferHardCopyReport()
    {
        $from_store_no = Input::get('from_store_no');
        $from_dept_no = Input::get('from_dept_no');
        $from_account = Input::get('from_account');
        $to_store_no = Input::get('to_store_no');
        $to_dept_no = Input::get('to_dept_no');
        $to_account = Input::get('to_account');
        $create_date = Input::get('create_date');
        $transfer_number = Input::get('transfer_number');
        $transfer_type = Input::get('transfer_type');
        $employee_id = Input::get('employee_id');
        $status = Input::get('status');



        $select = "select * from soxfrhdr";
        $where = ' where 1 = 1 ';
                    if (Input::has('from_store_no'))
                  {
                      $where.= " and from_store_no = '$from_store_no' ";
                  }

                if (Input::has('from_dept_no'))
                  {
                      $where.= " and from_dept_no = '$from_dept_no' ";
                  }

                if (Input::has('from_account'))
                  {
                      $where.= " and from_account = '$from_account' ";
                  }

                if (Input::has('to_store_no'))
                  {
                      $where.= " and to_store_no = '$to_store_no' ";
                  }

                if (Input::has('to_dept_no'))
                  {
                      $where.= " and to_dept_no = '$to_dept_no' ";
                  }

                if (Input::has('to_account'))
                  {
                      $where.= " and to_account = '$to_account' ";
                  }

                if (Input::has('create_date'))
                  {
                      $where.= " and create_date = '$create_date' ";
                  }

                  if (Input::has('transfer_number'))
                  {
                      $where.= " and transfer_number = '$transfer_number' ";
                  }

                  if (Input::has('transfer_type'))
                  {
                      $where.= " and transfer_type = '$transfer_type' ";
                  }
                
                if (Input::has('employee_id'))
                  {
                      $where.= " and employee_id = '$employee_id' ";
                  }
                  if (Input::has('status'))
                  {
                      $where.= " and status = '$status' ";
                  }
                  
                  $query=$select.$where;
                  $storehc_report = DB::select($query);
                    $storehc_report_array = json_decode(json_encode($storehc_report), true);
                    $pdf = PDF::loadView('pdf.storetransferhardcopy_report', compact('storehc_report_array'));
                    return $pdf->stream();
    }

    public function queryresultadvance()
    {
        $allpostval=Input::all();

            $receivings = Socadm00Sodsdhdr::query(); 

                $storeID=Input::get('storeID');
               
                $query  = 'SELECT t1.*, t2. retail_dept, t2. name,  t1. trns_number, t3. name, t3. name as vname2 , t4. name as vname FROM 
                           sodsdhdr t1, sodsddpt t2, sodsdtrn t3, sodsdvnd t4 ';
                $where = ' where t1.trns_number = t3.trns_number AND t1.dept_number=t2.retail_dept AND t1.vendor_number = t4.vendor_number AND
                           t1. store_number ="'.$storeID.'" ';
            //}
            if(Input::has('dept_name_number')) {
                $deptnum = Input::get('dept_name_number');
                $where.= ' AND t1. dept_number = "'. $deptnum .'"';
            }
             if(Input::has('invoice_date')) {
                $invoicedate = date("Y-m-d", strtotime(Input::get('invoice_date')));
                $invoice_end_date = date("Y-m-d", strtotime(Input::get('invoice_end_date')));
                if(Input::has('invoice_end_date'))
                { 
                    $convert_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
                    $convert_date_end = date("Y-m-d", strtotime(Input::get('invoice_end_date')));
                    $where.=' AND t1.invoice_date BETWEEN "'.$convert_date .'" AND "'.$convert_date_end.'"';
                }
                else
                {
                    $convert_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
                    $where.=' AND t1.invoice_date = "'.$convert_date .'"';
                }
            }
            if(Input::has('invoice_number')) {
                    $where.= ' AND t1. id = "'.Input::get('invoice_number').'"';
            }

            if(Input::has('vendornumber')) {
                    $where.= ' AND t1. vendor_number = "'.Input::get('vendornumber').'"';
            }
           
            if(Input::has('invoice_type')) {
                    $where.= ' AND t1. type_code = "'.Input::get('invoice_type').'"';
            }
             if(Input::has('status')) {
                    $where.= ' AND t1. status_code = "'.Input::get('status').'"';
            }

            if(Input::has('method_received')) {
                    $where.= ' AND t1. method_rcvd = "'.Input::get('method_received').'"';
            }

            

            if(Input::has('number_2')) {
               
                 $receivings = DB::select('SELECT t1.* , t2. retail_dept, t2. name,  t1. trns_number, t3. trns_number, 
                                           t3. name as vname FROM sodsdhdr t1, sodsddpt t2, sodsdtrn t3 
                                           WHERE t1. trns_number = t3. trns_number AND t1. dept_number=t2. retail_dept AND
                                                 t1. store_number ='.$storeID .' AND 
                                                 t1. trns_number='.Input::get('number_2'));

            }
            $order= 'ORDER BY vname asc';
            //echo $query.$where.$order; exit;
            $query_result = $query.$where.$order;
            $receivings = DB::select($query_result);
            $receivings = json_decode(json_encode($receivings), true);
            $pdf = PDF::loadView('pdf.receivings', compact('receivings'));
            return $pdf->stream();
    }

 public function offlinereport()
 {

    /* $query  = 'SELECT sodsdhdr . seq_number , sodsdhdr . method_rcvd , sodsdhdr . type_code , sodsdhdr . dept_number , sodsdhdr . id , sodsdhdr . vendor_number , sodsdhdr . tot_vend_cost , sodsdhdr . invoice_date , sodsdhdr . tot_sell_supply , sodsdhdr . tot_store_supply , sodsdvnd . name FROM sodsdhdr , sodsdvnd';
     $where = 'sodsdhdr . method_rcvd = "O" AND sodsdhdr . status_code = "O" AND';*/
    
      $storeno = Input::get('storeno');  $dept = Input::get('dept'); $fromdate = Input::get('fromdate'); $todate = Input::get('todate');
      $query_result = DB::select('SELECT sodsdhdr . seq_number , sodsdhdr . method_rcvd , sodsdhdr . type_code , sodsdhdr . dept_number , sodsdhdr . id , sodsdhdr . vendor_number , sodsdhdr . tot_vend_cost , sodsdhdr . invoice_date , sodsdhdr . tot_sell_supply , sodsdhdr . tot_store_supply , sodsdvnd . name FROM sodsdhdr , sodsdvnd WHERE sodsdhdr . store_number = "'.$storeno.'" AND sodsdhdr . dept_number = "'.$dept.'" AND sodsdhdr . method_rcvd = "O" AND sodsdhdr . status_code = "O" AND sodsdhdr . create_datetime >= "'.$fromdate.'" AND sodsdhdr . create_datetime <= "'.$todate.'" AND sodsdvnd . vendor_number = sodsdhdr . vendor_number ORDER BY sodsdhdr . dept_number , sodsdvnd . name , sodsdhdr . invoice_date , sodsdhdr . id');
           // $receivings = DB::select($query_result);
            $results = json_decode(json_encode($query_result), true);
            $pdf = PDF::loadView('pdf.browsepor', compact('results'));
            return $pdf->stream();
            //return View::make('pdf.browsepor', compact('results', 'storeno', 'dept', 'fromdate', 'todate'));
 }   

 public function getStoreInvoice()
 {
   $invno = Input::get('invno');
   $storeinv = DB::select('select * from sodsddtl WHERE hdr_seq_number = "'.$invno.'"');
   $results = json_decode(json_encode($storeinv), true);
   $pdf = PDF::loadView('pdf.browsesir', compact('results'));
   return $pdf->stream();
 }

 public function getCostDiscrepancyReport()
 {
   $keydate = Input::get('keydate'); $strno = Input::get('strno');
   $storeinv = DB::select('select seq_number , store_number , method_rcvd , type_code , dept_number , delivery_unit , id , vendor_number , 
                            cost_discrep_sw , status_code , invoice_date , create_datetime from sodsdhdr where 
                            store_number = "'.$strno.'" AND invoice_date = "'.$keydate.'" AND 
                            type_code = "D" AND status_code in ("O","A")');
   $results = json_decode(json_encode($storeinv), true);
   $pdf = PDF::loadView('pdf.browsecdr', compact('results'));
   return $pdf->stream();
 }

 public function getDexLogInvoiceReport()
 {
    $keydate = Input::get('keydate');
    $enddate = date('Y-m-d', strtotime("+1 day",strtotime($keydate)));

    $dexact = DB::select('select * from  sodexlog where 
                            create_datetime >= "'.$keydate.'" AND create_datetime <= "'.$enddate.'"
                            order by create_datetime desc');

   /* $results = Socadm00Sodexlog::query();
    $results->whereBetween('create_datetime', array($keydate, $enddate));
    $results->orderBy('create_datetime', 'desc');
*/
    $results = json_decode(json_encode($dexact), true);
    $pdf = PDF::loadView('pdf.dexlisting', compact('results'));
    return $pdf->stream();

 }
     public function getPdfShrinkCaptureData()
    {

        $shrink_date = Input::get('shrink_date');
        $results = DB::select('select * from shrnkhdr where shrink_date = "'.$shrink_date.'" and dept_no in (1,2,3,4,5,6,7,8,9,10,11,12,35) order by dept_no');
        $shrinkdata = json_decode(json_encode($results), true); 
        
        $dept_01_tot_value = DB::select('SELECT sum(tot_value) as dept_01_tot_value  FROM shrnkhdr WHERE dept_no = 01 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_02_tot_value = DB::select('SELECT sum(tot_value) as dept_02_tot_value  FROM shrnkhdr WHERE dept_no = 02 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_03_tot_value = DB::select('SELECT sum(tot_value) as dept_03_tot_value  FROM shrnkhdr WHERE dept_no = 03 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_04_tot_value = DB::select('SELECT sum(tot_value) as dept_04_tot_value  FROM shrnkhdr WHERE dept_no = 04 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_05_tot_value = DB::select('SELECT sum(tot_value) as dept_05_tot_value  FROM shrnkhdr WHERE dept_no = 05 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_06_tot_value = DB::select('SELECT sum(tot_value) as dept_06_tot_value  FROM shrnkhdr WHERE dept_no = 06 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_07_tot_value = DB::select('SELECT sum(tot_value) as dept_07_tot_value  FROM shrnkhdr WHERE dept_no = 07 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_08_tot_value = DB::select('SELECT sum(tot_value) as dept_08_tot_value  FROM shrnkhdr WHERE dept_no = 08 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_09_tot_value = DB::select('SELECT sum(tot_value) as dept_09_tot_value  FROM shrnkhdr WHERE dept_no = 09 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_10_tot_value = DB::select('SELECT sum(tot_value) as dept_10_tot_value  FROM shrnkhdr WHERE dept_no = 10 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_11_tot_value = DB::select('SELECT sum(tot_value) as dept_11_tot_value  FROM shrnkhdr WHERE dept_no = 11 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_12_tot_value = DB::select('SELECT sum(tot_value) as dept_12_tot_value  FROM shrnkhdr WHERE dept_no = 12 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');
        $dept_35_tot_value = DB::select('SELECT sum(tot_value) as dept_35_tot_value  FROM shrnkhdr WHERE dept_no = 35 AND shrink_date = "'.$shrink_date.'" AND (shrnkhdr.status = "C"  OR shrnkhdr.status = "S")');

        $pdf = PDF::loadView('pdf.shrinkcaptureredata', compact('shrinkdata','shrink_date','dept_01_tot_value','dept_02_tot_value','dept_03_tot_value','dept_04_tot_value','dept_05_tot_value','dept_06_tot_value','dept_07_tot_value','dept_08_tot_value','dept_09_tot_value','dept_10_tot_value','dept_11_tot_value','dept_12_tot_value','dept_35_tot_value'));
      
        return $pdf->stream();
    }


         public function getPdfShrinkCaptureDept()
    {

        $dept_no = Input::get('dept_no');
        $shrink_date = Input::get('shrink_date');
       
        $shrinkcapturedepart = DB::select('SELECT seq_number, shrink_date, shrink_number, employee_id, tot_line_items, tot_value, shrnkhdr.status  FROM shrnkhdr  WHERE dept_no = "'.$dept_no.'"
         AND shrink_date = "'.$shrink_date.'" ');
        
        $pdf = PDF::loadView('pdf.shrinkcaptureredept', compact('shrinkcapturedepart','dept_no'));
      
        return $pdf->stream();
    }

       public function getPdfShrinkCaptureItemScan()
    {
            
        $shrink_date = Input::get('shrink_date');
        $shrink_number = Input::get('shrink_number');
        $employee_id = Input::get('employee_id');
        $tot_line_items = Input::get('tot_line_items');
        $tot_value = Input::get('tot_value');
        $seq_number = Input::get('seq_number');
        $status = Input::get('status');
        $shrinkcaptureitemscan = DB::select('SELECT seq_number, item_desc, upc_cost, quantity,rtl_amt, rw_rtl_amt                     
          FROM shrnkdtl
         WHERE hdr_seq_number = "'.$seq_number.'"
           AND shrnkdtl.status = "I"');
        
        $pdf = PDF::loadView('pdf.shrinkcaptureitemscan', compact('shrinkcaptureitemscan','shrink_date','shrink_number','employee_id','tot_line_items','tot_value','seq_number','status'));
      
        return $pdf->stream();
    }

       public function getPdfStoreOrderQuery()
    {

        $inputdata = Input::all();
        $dept_number = Input::get('dept_number');
        $order_date = Input::get('order_date');
        //echo $dept_number;exit;
        $select = "select * from soordhdr ";
        $where = 'where 1 = 1 ';
                    if(Input::has('dept_number'))
                    {
                        $where.="and  dept_number = '$dept_number' ";
                    }
                    if(Input::has('order_date'))
                    {
                        $where.=" and order_date = '$order_date' ";
                    }

                $order="ORDER BY order_date asc";   
                $query=$select.$where.$order;
                $print_order_result = DB::select($query);
                $print_order_result_array = json_decode(json_encode($print_order_result), true);
            
                    /*echo '<pre>';print_r($print_order_result_array);exit;*/
        
        $pdf = PDF::loadView('pdf.storeorderquery', compact('print_order_result_array', 'dept_number','order_date'));
      
        return $pdf->stream();
    }
    public function getPdfPrintOrderDetails()
    {
        $seq_number = Input::get('seq_number');
        
        $order_date = Input::get('order_date');
        $description = Input::get('description');
        $status_code = Input::get('status_code');
        
        
        $seq_number_result = DB::select('SELECT * FROM soorditm  WHERE seq_number = "'.$seq_number.'"');
        $seq_number_result_array = json_decode(json_encode($seq_number_result), true);
        
           
        
        $pdf = PDF::loadView('pdf.printorderdetails', compact('seq_number_result_array','order_date','description','status_code'));
      
        return $pdf->stream();
    }

    public function getPdfPlusOut()
    {
        $sotrnhdr_rec = DB::select('select sotrnhdr.seq_number,soordhdr.order_date,sotrncfg.type_code,sotrnhdr.type_code,sotrncfg.subtype_code,sotrnhdr.subtype_code from sotrnhdr,soordhdr,sotrncfg where sotrnhdr.seq_number = soordhdr.seq_number and sotrnhdr.type_code = sotrncfg.type_code and sotrnhdr.subtype_code = sotrncfg.subtype_code order by order_date desc');
        
           //echo '<pre>'; print_r($sotrnhdr_rec);exit();
        
        $pdf = PDF::loadView('pdf.plusout',compact('sotrnhdr_rec'));
      
        return $pdf->stream();
    }

     public function getPdfPlusOutItems()
    {
          $seq_number = Input::get('seq_number');
        
        $soorditm_rec = DB::select('SELECT soorditm.sku_number, soorditm.quantity FROM soorditm WHERE soorditm.seq_number = "'.$seq_number.'" ');
        
        $pdf = PDF::loadView('pdf.plusoutitems',compact('soorditm_rec'));
      
        return $pdf->stream();
    }




     public function getLotteryInventory()
    {
          $work_data = new StdClass();
        
        if (Input::has('dateofinfo')) {
                $date_stamp = Input::get('dateofinfo');
                //$date_stamp = '1989-11-16';
                $dt = new DateTime($date_stamp);
                $datecheck = new DateTime('1990-01-01');
                if ($dt < $datecheck) {
                    $dt = new DateTime("yesterday");
                    $date_stamp = $dt->format('Y-m-d');
                }
                $this->date_stamp = date('Y-m-d',strtotime($date_stamp)); 

                $work_data->lottery_date = date('Y-m-d',strtotime($date_stamp)); 
                    
                    $tot_begin_inv = 0;
                    $tot_end_inv = 0;
                    $tot_deliveries = 0;
                    $tot_returns = 0;
                    $tot_calc_sales = 0;
                    $unity = 0;
                    $cnt = 1;
                            
                    $lot_array=array();
                           
                                    
                $results = DB::select(DB::raw("SELECT lottery.game_no as game_no,lottery.ticket_value as ticket_value,lottery.begin_inv as begin_inv,lottery.deliveries as deliveries,lottery.returns as returns,lottery.end_inv as end_inv, lottery.sales_unity as sales_unity FROM  lottery WHERE  lottery.lottery_date = ? ORDER BY lottery.game_no"), array($this->date_stamp));
                
                     $query="SELECT lottery.game_no as game_no,lottery.ticket_value/100 as ticket_value,lottery.begin_inv/100 as begin_inv,lottery.deliveries/100 as deliveries,lottery.returns/100 as returns,lottery.end_inv/100 as end_inv, lottery.sales_unity/100 as sales_unity FROM  lottery WHERE  lottery.lottery_date = '".$this->date_stamp."' ORDER BY lottery.game_no";
                
              
        
                
                
            if (!empty($results)) {
                
            

                      foreach($results as $row){
                          
                          
                          $lot_array[$cnt]['game_no']=$row->game_no;
                          $lot_array[$cnt]['ticket_value']=number_format($row->ticket_value/100,2);
                          $lot_array[$cnt]['begin_inv']=number_format($row->begin_inv/100,2);
                          $lot_array[$cnt]['deliveries']=number_format($row->deliveries/100,2);
                          $lot_array[$cnt]['returns']=number_format($row->returns/100,2);
                          $lot_array[$cnt]['end_inv']=number_format($row->end_inv/100,2);
                          
                          

                         $lot_array[$cnt]['calc_sales'] = ($lot_array[$cnt]['begin_inv'] + $lot_array[$cnt]['deliveries'] - $lot_array[$cnt]['returns'] - $lot_array[$cnt]['end_inv']);
                         
                        
                          $lot_array[$cnt]['sales_unity']=number_format($row->sales_unity/100,2);
                          if($lot_array[$cnt]['game_no'] == "ZZZZ"){
                             $unity = $lot_array[$cnt]['sales_unity'];                             
                          }
                           $lot_array[$cnt]['sales_diff'] ='';
                         $tot_begin_inv = $tot_begin_inv + $lot_array[$cnt]['begin_inv'];
                         $tot_end_inv = $tot_end_inv + $lot_array[$cnt]['end_inv'];
                         $tot_deliveries = $tot_deliveries + $lot_array[$cnt]['deliveries'];
                         $tot_returns = $tot_returns + $lot_array[$cnt]['returns'];
                         $tot_calc_sales = $tot_calc_sales + $lot_array[$cnt]['calc_sales'];

                            $cnt = $cnt + 1;
                        }
                       }   
                       
                       
                       


                      $lot_array[$cnt]['game_no'] = "TOTAL";
                      $lot_array[$cnt]['ticket_value']=0.00;
                      $lot_array[$cnt]['begin_inv'] = number_format($tot_begin_inv,2);
                      
                      $lot_array[$cnt]['deliveries'] = number_format($tot_deliveries,2);
                      $lot_array[$cnt]['returns'] = number_format($tot_returns,2);
                      $lot_array[$cnt]['end_inv'] = number_format($tot_end_inv,2);
                      $lot_array[$cnt]['calc_sales'] = number_format($tot_calc_sales,2);
                      $lot_array[$cnt]['sales_unity'] = $unity;
                      $lot_array[$cnt]['sales_diff'] = number_format(($tot_calc_sales - $unity),2);
                      
                      
                    


                    // echo "<pre>"; print_r($lot_array); die;
                      $work_data  =  $lot_array;
                   // echo "<pre>"; print_r($work_data); die; 

                
                }
        
        
        $pdf = PDF::loadView('pdf.lotteryinverntory',compact('work_data'));
        return $pdf->stream();
    }


     public function getDetailsReports()
    {
          $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->update_date = trim(Input::get('update_date'));
        $this->update_time = trim(Input::get('update_time'));
        $this->action_cd = trim(Input::get('action_cd'));
        $this->maint_type_cd = trim(Input::get('maint_type_cd'));

        $this->item_code = trim(Input::get('item_code'));
        $this->mstr_fld_no = trim(Input::get('mstr_fld_no'));
        $this->orig_value = trim(Input::get('orig_value'));
        $this->new_value = trim(Input::get('new_value'));




        $sql = "SELECT maint_type_cd,slhistry.userid,slhistry.update_date, slhistry.update_time, slhistry.item_code, slhistry.item_desc, slhistry.action_cd, slhistry.mstr_fld_no,seq_no,new_value,orig_value  FROM slhistry WHERE 1=1 ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->update_date != '') {
            $sql.=" AND DATE(update_date)='" . date('Y-m-d', strtotime($this->update_date)) . "' ";
            $paginate_array['update_date'] = $this->update_date;
        }


        if ($this->update_time != '') {
            $sql.=" AND update_time='" . $this->update_time . "' ";
            $paginate_array['update_time'] = $this->update_time;
        }

        if ($this->action_cd != '') {
            $sql.=" AND action_cd='" . strtoupper($this->action_cd) . "' ";
            $paginate_array['action_cd'] = $this->action_cd;
        }

        if ($this->maint_type_cd != '') {
            $sql.=" AND maint_type_cd='" . strtoupper($this->maint_type_cd) . "' ";
            $paginate_array['maint_type_cd'] = $this->maint_type_cd;
        }

        if ($this->item_code != '') {
            $sql.=" AND item_code='" . $this->item_code . "' ";
            $paginate_array['item_code'] = $this->item_code;
        }
        if ($this->orig_value != '') {
            $sql.=" AND orig_value='" . $this->orig_value . "' ";
            $paginate_array['orig_value'] = $this->orig_value;
        }
        if ($this->new_value != '') {
            $sql.=" AND new_value='" . $this->new_value . "' ";
            $paginate_array['new_value'] = $this->new_value;
        }




        $sql.=" ORDER BY userid, update_date, update_time";





        $results = DB::select(DB::raw($sql));



        if (!empty($results)) {


            $previous_item = 0;
            $previous_act = "";
            $previous_time = 0;

            $tot_scanned = 0;
            $tot_aisle_scanned = 0;
            $tot_add = 0;
            $tot_upd = 0;
            $tot_del = 0;
            $tot_updprice = 0;
            $tot_updsprice = 0;
            $tot_fm_notes = 0;
            $tot_notfound = 0;
            $tot_signin = 0;
            $tot_signout = 0;
            $tot_unknown = 0;


            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;





                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;



                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;


                $item_code = $row->item_code;

                if ($action_cd == "C" OR $action_cd == "N") {
                    
                } else {
                    if ($previous_item == $item_code) {
                        
                    } else {
                        $previous_act = "";
                        $previous_time = 0;
                    }
                }

                $action_desc = '';

                switch ($action_cd) {
                    CASE "L":
                        $tot_signin = $tot_signin + 1;
                    CASE "O":
                        $tot_signout = $tot_signout + 1;
                    CASE "C":
                        $tot_scanned = $tot_scanned + 1;
                    CASE "S":
                        $tot_scanned = $tot_scanned + 1;
                    CASE "W":
                        $tot_aisle_scanned = $tot_aisle_scanned + 1;
                    CASE "N":
                        $tot_scanned = $tot_scanned + 1;
                        $tot_notfound = $tot_notfound + 1;
                    default:
                        if ($action_cd == $previous_act) {
                            
                        } else {
                            switch ($action_cd) {
                                CASE "A":
                                    $tot_add = $tot_add + 1;
                                    $action_desc = "ADD";
                                CASE "U":
                                    $tot_upd = $tot_upd + 1;
                                    $action_desc = "UPDATE";
                                CASE "D":
                                    $tot_del = $tot_del + 1;
                                    $action_desc = "DELETE";
                                CASE "P":
                                    $tot_updprice = $tot_updprice + 1;
                                    $action_desc = "PRICE CHANGE";
                                # CASE "S":
                                #    $tot_updsprice = $tot_updsprice + 1;
                                CASE "F":
                                    $tot_fm_notes = $tot_fm_notes + 1;
                                    $action_desc = "FM NOTE";
                                default:
                                    $tot_unknown = $tot_unknown + 1;
                                    $action_desc = "UNKNOWN";
                            }
                        }
                }


                if ($item_code == $previous_time AND $action_cd == $previous_act AND $row->update_time == $previous_time) {
                    $action_desc = "";
                }

                $results[$key]->action_desc = $action_desc;

                # Because s"C"anned and "N"otFound audit records are only tallied 
                # and not detailed on the report, do not note them as a previous
                # item
                if ($action_cd == "C" OR $action_cd == "N") {
                    
                } else {
                    $previous_item = $item_code;
                    $previous_act = $action_cd;
                    $previous_time = $row->update_time;
                }
            }

            $work_data->tot_scanned = $tot_scanned;
            $work_data->tot_aisle_scanned = $tot_aisle_scanned;
            $work_data->tot_add = $tot_add;
            $work_data->tot_upd = $tot_upd;
            $work_data->tot_del = $tot_del;
            $work_data->tot_updprice = $tot_updprice;
            $work_data->tot_updsprice = $tot_updsprice;
            $work_data->tot_fm_notes = $tot_fm_notes;
            $work_data->tot_notfound = $tot_notfound;
            $work_data->tot_signin = $tot_signin;
            $work_data->tot_signout = $tot_signout;
            $work_data->tot_unknown = $tot_unknown;
            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There is no Scan History satisfying the query conditions  ");
            return Redirect::route('slcadm00-report-mod-query')->withInput();
        }





        $work_data->results = $results;


        $pdf = PDF::loadView('pdf.detailsreports',compact('work_data'));
      
        return $pdf->stream();
    }



}