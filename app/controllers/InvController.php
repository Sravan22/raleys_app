<?php

class InvController extends BaseController
{
	public function getbookpostagePrint()
	{
		return View::make('mktmgr.bookpostageprint'); 
	}
	public function postPostagePrint()
    {	
    	//echo 'hi';exit;
    	$work_data = array();
    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
      
        
        $validator = Validator::make(Input::all(),
            array(                   
                   'end_date'=>'required|date|weekenddatecheck',
                ),array(
                	'end_date.required' => 'This date is required'
                	));
        // echo '<pre>',print_r($validator);exit;
	    if($validator->fails())
	    {
	        return Redirect::route('mktmgr-bookpostageprnt')
	                    ->withErrors($validator)
	                    ->withInput();
	    }
	    else
	    {	
	    	 	
	    	$end_date = Input::get('end_date');
		    $begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
		      
			
	        $all= DB::select("SELECT ifNull(SUM(postage.begin_inv)/100,0) as begin_inv,
	    	 	  	ifNull(SUM(postage.purchases)/100,0) as purchases, 
	    	 	  	ifNull(SUM(postage.end_inv_safe),0) as end_inv_safe, 

	    	 	  	ifNull(SUM(postage.end_inv_tills)/100,0) as end_inv_tills, 
	    	 	  	ifNull((begin_inv + purchases - end_inv_tills)/100,0) as calc_sales,
	    	 	  	ifNull(SUM(postage.end_inv_ca),0) as end_inv_ca ,
	    	 	  	ifNull(SUM(postage.ps_unity_sales)/100,0) as ps_unity_sales,
					ifNull(((begin_inv + purchases - end_inv_tills) - ps_unity_sales)/100,0) as sales_diff
                    FROM  postage
                    WHERE  postage.ps_date <= '{$end_date}'
                    AND  postage.ps_date >= '{$begin_date}'");
	      /* if(empty($all))

           {
           	$all['begin_inv'] = 0.00;
           	$all['purchases'] = 0.00;
           	$all['end_inv_safe'] = 0.00;
           	$all['end_inv_tills'] = 0.00;
           	$all['calc_sales'] = 0.00;
           	$all['end_inv_ca'] =0.00;
           	$all['ps_unity_sales'] = 0.00;
           	$all['sales_diff'] = 0.00;
            } */
              
	        $check_num =DB::select("SELECT ck_no_1,ck_no_2
                  FROM  postage
                  WHERE  postage.ps_date = '{$end_date}'");
	        $check_num = json_decode(json_encode($check_num), true);
	        if($check_num)
	        {
	        	$work_data['check_num'] = $check_num;
	        }
	        else
	        {
	        	$check_num_array = array(array());
	        	$check_num_array[0]['ck_no_1'] = 0;
	        	$check_num_array[0]['ck_no_2'] = 0;
	        	//echo '<pre>';print_r($check_num_array);exit;
	        	$work_data['check_num'] = $check_num_array;
	        }
	        $work_data['all'] = $all;
	        
	    	
	    	return View::make('mktmgr.postageprintdata',compact('end_date','work_data'));
	    	
	    }
	} 
	public function getbookphonecardPrnt() 
	{
        return View::make('mktmgr.bookphonecardprnt');
    }

    public function postphoneprint()
    {	
    	
    	$work_data = new StdClass();
		
		$validator = Validator::make(Input::all(),
		array(
		'end_date'=>'required|date',
		),array(
		'end_date.required' => 'This date is required'));

        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-bookphonecardadd')
                    ->withErrors($validator)
                    ->withInput();
        }
	    else
	    {	
	    	$end_date = Input::get('end_date');
		    $begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
	    	
	    	$work_data->last_inventory_date=$end_date;
			$work_data->five_begin_inv = 0;
			$work_data->ten_begin_inv = 0; 
			$work_data->twenty_begin_inv = 0;

				
			$last_inventory_row = DB::select("SELECT * FROM phone_card WHERE phone_card.phone_date < '{$end_date}' ORDER BY phone_card.phone_date DESC ");
                

            if (!empty($last_inventory_row))
            {
                if (isset($last_inventory_row[0])) 
                {
                    $last_row_data = $last_inventory_row[0];
                    $work_data->last_inventory_date=$last_row_data->phone_date; 
                    $work_data->five_begin_inv = $last_row_data->five_end_inv; 
                    $work_data->ten_begin_inv = $last_row_data->ten_end_inv; 
                    $work_data->twenty_begin_inv = $last_row_data->twenty_end_inv; 
                }
            } 


		    $work_data->five_deliveries = 0; 
	        $work_data->ten_deliveries = 0; 
	        $work_data->twenty_deliveries = 0; 
	        $work_data->five_returns = 0; 
	        $work_data->ten_returns = 0; 
	        $work_data->twenty_returns = 0; 
	        $work_data->five_end_inv = 0; 
	        $work_data->ten_end_inv = 0; 
	        $work_data->twenty_end_inv = 0;

	       
	        $results = DB::select("SELECT * FROM phone_card WHERE phone_card.phone_date =  '{$end_date}'"); 

	        if (!empty($results)) 
	        {
                        
               	if (isset($results[0])) 
               	{
                    $row_data = $results[0];
                    
                    
                    $work_data->five_deliveries=$row_data->five_deliveries;
                    $work_data->ten_deliveries=$row_data->ten_deliveries;
                    $work_data->twenty_deliveries=$row_data->twenty_deliveries;
                    
                    $work_data->five_returns=$row_data->five_returns;
                    $work_data->ten_returns=$row_data->ten_returns;
                    $work_data->twenty_returns=$row_data->twenty_returns;
                    
                    $work_data->five_end_inv=$row_data->five_end_inv;
                    $work_data->ten_end_inv=$row_data->ten_end_inv;
                    $work_data->twenty_end_inv=$row_data->twenty_end_inv;
                }
            }

    		$work_data->five_begin_inv_amt = ($work_data->five_begin_inv*5.00);
            $work_data->ten_begin_inv_amt= ($work_data->ten_begin_inv * 10.00);
            $work_data->twenty_begin_inv_amt = ($work_data->twenty_begin_inv *20.00);
                    
            $work_data->five_deliveries_amt = ($work_data->five_deliveries *5.00);
            $work_data->ten_deliveries_amt = ($work_data->ten_deliveries*10.00);
            $work_data->twenty_deliveries_amt = ($work_data->twenty_deliveries*20.00);

            $work_data->five_returns_amt = ($work_data->five_returns*5.00);
            $work_data->ten_returns_amt = ($work_data->ten_returns *10.00);
            $work_data->twenty_returns_amt = ($work_data->twenty_returns *20.00);

            $work_data->five_end_inv_amt = ($work_data->five_end_inv *5.00);
            $work_data->ten_end_inv_amt= ($work_data->ten_end_inv *10.00);
            $work_data->twenty_end_inv_amt = ($work_data->twenty_end_inv*20.00);



			$work_data->five_sales_amt = ($work_data->five_begin_inv_amt + $work_data->five_deliveries_amt - $work_data->five_returns_amt - $work_data->five_end_inv_amt);

			$work_data->ten_sales_amt = ($work_data->ten_begin_inv_amt + $work_data->ten_deliveries_amt - $work_data->ten_returns_amt - $work_data->ten_end_inv_amt);

            $work_data->twenty_sales_amt = ($work_data->twenty_begin_inv_amt + $work_data->twenty_deliveries_amt - $work_data->twenty_returns_amt - $work_data->twenty_end_inv_amt);

            $work_data->tot_sales = ($work_data->five_sales_amt + $work_data->ten_sales_amt + $work_data->twenty_sales_amt);
		
	    	return View::make('mktmgr.phoneprintdata',compact('end_date','work_data'));
	    	
	    }
    
	}


	public function getbookcommuterPrnt()
	{
        return View::make('mktmgr.bookcommuterprnt');
    }


    public function postcommuterprint()
    {	

    	$work_data = new stdClass;
    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
      
        
        $validator = Validator::make(Input::all(),
            array(                   
                   'end_date'=>'required|date|weekenddatecheck',
                ),array(
                	'end_date.required' => 'This date is required'
                	));
        
	    if($validator->fails())
	    {
	        return Redirect::route('mktmgr-bookpostageprnt')
	                    ->withErrors($validator)
	                    ->withInput();
	    }
	    else
	    {	
	    	 	
	    	 $end_date = Input::get('end_date');
		 $begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
		   		
		  
			$results = DB::select("SELECT   (SELECT item_desc  FROM recapitems  WHERE recapitems.item_id = `inv_type`) as  inv_type,
                        (SELECT item_desc  FROM recapitems  WHERE recapitems.item_id = `desc`) as `desc`,trf_no_1,price/100 as price,SUM((begin_inv*price))/100 as begin_inv ,SUM((deliveries*price))/100 as deliveries,SUM((returns*price))/100 AS returns, SUM((end_inv*price))/100 AS end_inv,SUM((begin_inv * price) + (deliveries *price) - (returns * price) -(end_inv * price))/100 as sales FROM commuter WHERE comm_date ='{$end_date}' GROUP by inv_type ASC  ,price ASC WITH ROLLUP");
	       
				//echo '<pre>',print_r($results);exit;
	    	return View::make('mktmgr.commuterprintdata',compact('end_date','results'));
	    	
	    }
	}

	function get_recapitems_desc($id)
	{

     $results = DB::Select("SELECT item_desc FROM recapitems WHERE item_id = '{$id}' ");
       
        if (!empty($results)) 
        {
 		    if (isset($results[0])) 
 		    {
                $row=$results[0];
                return $row->item_desc;
            }
        }
        return '';
    } 
    public function getbookcommutermonthlyPrnt() {
        return View::make('mktmgr.bookcommutermonthlyprnt');
    }

     public function postmthlyprint()
    {	

    	$work_data = new stdClass;
    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
      
        
        $validator = Validator::make(Input::all(),
            array(                   
                   'end_date'=>'required|date|weekenddatecheck',
                ),array(
                	'end_date.required' => 'This date is required'
                	));
        
	    if($validator->fails())
	    {
	        return Redirect::route('mktmgr-bookpostageprnt')
	                    ->withErrors($validator)
	                    ->withInput();
	    }
	    else
	    {	
	    	 	
	    	 $end_date = Input::get('end_date');
		 $begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
		   		
		  
			$results = DB::select("SELECT   (SELECT item_desc  FROM recapitems  WHERE recapitems.item_id = `inv_type`) as  inv_type,
                        (SELECT item_desc  FROM recapitems  WHERE recapitems.item_id = `desc`) as `desc`,CONCAT(trf_no_1,'/' ,trf_no_2) as trf_no_1,price/100 as price,SUM((begin_inv*price))/100 as begin_inv ,SUM((deliveries*price))/100 as deliveries,SUM((returns*price))/100 AS returns, SUM((end_inv*price))/100 AS end_inv,SUM((begin_inv * price) + (deliveries *price) - (returns * price) -(end_inv * price))/100 as sales FROM monthcom WHERE comm_date ='{$end_date}' GROUP by inv_type ASC  ,price ASC WITH ROLLUP");
	       
				//echo '<pre>',print_r($results);exit;
	    	return View::make('mktmgr.mthlyprintdata',compact('end_date','results'));
	    	
	    }
	}




















}
?>