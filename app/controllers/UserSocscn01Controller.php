<?php
class UserSocscn01Controller extends BaseController
{
	public function getReturnhome()
	{
		return View::make('socscn01.homepage');
	}

	public function getsocDeptfunctions()
	{
		return View::make('socscn01.socdeptfunctions');
	}
	public function getsocReports()
	{
		return View::make('socscn01.socreports');
	}
    
    public function getsocdsdReceiving()
	{
		return View::make('socscn01.socdsdreceiving');
	}

	public function getsocRetailpricechk()
	{

		return View::make('socscn01.socretailpricechk');
	}
	public function getsocQueryitem()
	{

		return View::make('socscn01.socqueryitem');
	}
	public function getsocStoretransfer()
	{
		return View::make('socscn01.socstoretransfer');
	}
	public function getsocPricesaleschk()
	{

		return View::make('socscn01.socpricesaleschk');
	}
}