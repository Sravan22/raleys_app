<?php

class NonPerishableMenuController extends BaseController {

    public function nonPerishableInvPrepview() {


        $work_data = new StdClass();

        $dept_sodinv_rec[1]['dept_no'] = 1;
        $dept_sodinv_rec[1]['desc'] = "Grocery";
        $dept_sodinv_rec[1]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[1]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[1]['tot_crv_rtl'] = 0.00;

        $dept_sodinv_rec[2]['dept_no'] = 2;
        $dept_sodinv_rec[2]['desc'] = "Liquor";
        $dept_sodinv_rec[2]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[2]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[2]['tot_crv_rtl'] = 0.00;

        $dept_sodinv_rec[3]['dept_no'] = 4;
        $dept_sodinv_rec[3]['desc'] = "Deli";
        $dept_sodinv_rec[3]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[3]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[3]['tot_crv_rtl'] = 0.00;

        $dept_sodinv_rec[4]['dept_no'] = 8;
        $dept_sodinv_rec[4]['desc'] = "Nat Fds";
        $dept_sodinv_rec[4]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[4]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[4]['tot_crv_rtl'] = 0.00;

        $dept_sodinv_rec[5]['dept_no'] = 9;
        $dept_sodinv_rec[5]['desc'] = "GM";
        $dept_sodinv_rec[5]['tot_value_cst'] = 0.00;
        $dept_sodinv_rec[5]['tot_value_rtl'] = 0.00;
        $dept_sodinv_rec[5]['tot_crv_rtl'] = 0.00;

        $dept_01_rec_cnt = 0;
        $dept_02_rec_cnt = 0;
        $dept_04_rec_cnt = 0;
        $dept_08_rec_cnt = 0;
        $dept_09_rec_cnt = 0;

        

        $sql = 'SELECT count(*) as total FROM sodinvhdr
    WHERE dept_number = 1
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S") ';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[1]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[1]['num_of_invs'] = $row_data->total;
            }
        }


        $sql = 'SELECT count(*) as total   
     FROM sodinvhdr
    WHERE dept_number = 2
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S")';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[2]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[2]['num_of_invs'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S")';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[3]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[3]['num_of_invs'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S")';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[4]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[4]['num_of_invs'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND (sodinvhdr.status = "C"
       OR  sodinvhdr.status = "A"
       OR  sodinvhdr.status = "O"
       OR  sodinvhdr.status = "V"
       OR  sodinvhdr.status = "S")';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_non_disp_rec[5]['num_of_invs'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_non_disp_rec[5]['num_of_invs'] = $row_data->total;
            }
        }
        
        
        

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as  total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 1
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[1]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[1]['num_of_invs_S'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 2
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[2]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[2]['num_of_invs_S'] = $row_data->total;
            }
        }


        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 4
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[3]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[3]['num_of_invs_S'] = $row_data->total;
            }
        }


        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 8
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[4]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[4]['num_of_invs_S'] = $row_data->total;
            }
        }


        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "O"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_O'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_O'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "C"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_C'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_C'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "A"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_A'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_A'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "V"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_V'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_V'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "R"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_R'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_R'] = $row_data->total;
            }
        }

        $sql = 'SELECT count(*) as total
     FROM sodinvhdr
    WHERE dept_number = 9
      AND sodinvhdr.status = "S"';

        $results = DB::select(DB::raw($sql));

        $dept_sodinv_rec[5]['num_of_invs_S'] = 0;
        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_sodinv_rec[5]['num_of_invs_S'] = $row_data->total;
            }
        }
        
        
       
    

        $dept_01_tot_value_cst = $dept_01_tot_value_rtl = $dept_01_tot_crv_rtl = 0;

        $sql = 'SELECT sum(total_value_cst) as dept_01_tot_value_cst, sum(total_value_rtl) as dept_01_tot_value_rtl, sum(total_crv_rtl) as dept_01_tot_crv_rtl FROM sodinvhdr
    WHERE dept_number = 01
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_01_tot_value_cst = $row_data->dept_01_tot_value_cst;
                $dept_01_tot_value_rtl = $row_data->dept_01_tot_value_rtl;
                $dept_01_tot_crv_rtl = $row_data->dept_01_tot_crv_rtl;
            }
        }

        $dept_02_tot_value_cst = $dept_02_tot_value_rtl = $dept_02_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_02_tot_value_cst, sum(total_value_rtl) as dept_02_tot_value_rtl, sum(total_crv_rtl) as dept_02_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 02
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_02_tot_value_cst = $row_data->dept_02_tot_value_cst;
                $dept_02_tot_value_rtl = $row_data->dept_02_tot_value_rtl;
                $dept_02_tot_crv_rtl = $row_data->dept_02_tot_crv_rtl;
            }
        }



        $dept_03_tot_value_cst = $dept_03_tot_value_rtl = $dept_03_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_03_tot_value_cst, sum(total_value_rtl) as dept_03_tot_value_rtl, sum(total_crv_rtl) as dept_03_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 03
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_03_tot_value_cst = $row_data->dept_03_tot_value_cst;
                $dept_03_tot_value_rtl = $row_data->dept_03_tot_value_rtl;
                $dept_03_tot_crv_rtl = $row_data->dept_03_tot_crv_rtl;
            }
        }


        $dept_04_tot_value_cst = $dept_04_tot_value_rtl = $dept_04_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_04_tot_value_cst, sum(total_value_rtl) as dept_04_tot_value_rtl, sum(total_crv_rtl) as dept_04_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 04
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_04_tot_value_cst = $row_data->dept_04_tot_value_cst;
                $dept_04_tot_value_rtl = $row_data->dept_04_tot_value_rtl;
                $dept_04_tot_crv_rtl = $row_data->dept_04_tot_crv_rtl;
            }
        }



        $dept_05_tot_value_cst = $dept_05_tot_value_rtl = $dept_05_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_05_tot_value_cst, sum(total_value_rtl) as dept_05_tot_value_rtl, sum(total_crv_rtl) as dept_05_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 05
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_05_tot_value_cst = $row_data->dept_05_tot_value_cst;
                $dept_05_tot_value_rtl = $row_data->dept_05_tot_value_rtl;
                $dept_05_tot_crv_rtl = $row_data->dept_05_tot_crv_rtl;
            }
        }


        $dept_06_tot_value_cst = $dept_06_tot_value_rtl = $dept_06_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_06_tot_value_cst, sum(total_value_rtl) as dept_06_tot_value_rtl, sum(total_crv_rtl) as dept_06_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 06
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_06_tot_value_cst = $row_data->dept_06_tot_value_cst;
                $dept_06_tot_value_rtl = $row_data->dept_06_tot_value_rtl;
                $dept_06_tot_crv_rtl = $row_data->dept_06_tot_crv_rtl;
            }
        }


        $dept_07_tot_value_cst = $dept_07_tot_value_rtl = $dept_07_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_07_tot_value_cst, sum(total_value_rtl) as dept_07_tot_value_rtl, sum(total_crv_rtl) as dept_07_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 07
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_07_tot_value_cst = $row_data->dept_07_tot_value_cst;
                $dept_07_tot_value_rtl = $row_data->dept_07_tot_value_rtl;
                $dept_07_tot_crv_rtl = $row_data->dept_07_tot_crv_rtl;
            }
        }


        $dept_08_tot_value_cst = $dept_08_tot_value_rtl = $dept_08_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_08_tot_value_cst, sum(total_value_rtl) as dept_08_tot_value_rtl, sum(total_crv_rtl) as dept_08_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 08
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_08_tot_value_cst = $row_data->dept_08_tot_value_cst;
                $dept_08_tot_value_rtl = $row_data->dept_08_tot_value_rtl;
                $dept_08_tot_crv_rtl = $row_data->dept_08_tot_crv_rtl;
            }
        }


        $dept_09_tot_value_cst = $dept_09_tot_value_rtl = $dept_09_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_09_tot_value_cst, sum(total_value_rtl) as dept_09_tot_value_rtl, sum(total_crv_rtl) as dept_09_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 09
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_09_tot_value_cst = $row_data->dept_09_tot_value_cst;
                $dept_09_tot_value_rtl = $row_data->dept_09_tot_value_rtl;
                $dept_09_tot_crv_rtl = $row_data->dept_09_tot_crv_rtl;
            }
        }


        $dept_10_tot_value_cst = $dept_10_tot_value_rtl = $dept_10_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_10_tot_value_cst, sum(total_value_rtl) as dept_10_tot_value_rtl, sum(total_crv_rtl) as dept_10_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 10
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_10_tot_value_cst = $row_data->dept_10_tot_value_cst;
                $dept_10_tot_value_rtl = $row_data->dept_10_tot_value_rtl;
                $dept_10_tot_crv_rtl = $row_data->dept_10_tot_crv_rtl;
            }
        }

        $dept_11_tot_value_cst = $dept_11_tot_value_rtl = $dept_11_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_11_tot_value_cst, sum(total_value_rtl) as dept_11_tot_value_rtl, sum(total_crv_rtl) as dept_11_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 11
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_11_tot_value_cst = $row_data->dept_11_tot_value_cst;
                $dept_11_tot_value_rtl = $row_data->dept_11_tot_value_rtl;
                $dept_11_tot_crv_rtl = $row_data->dept_11_tot_crv_rtl;
            }
        }



        $dept_35_tot_value_cst = $dept_35_tot_value_rtl = $dept_35_tot_crv_rtl = 0;
        $sql = 'SELECT sum(total_value_cst) as dept_35_tot_value_cst, sum(total_value_rtl) as dept_35_tot_value_rtl, sum(total_crv_rtl) as dept_35_tot_crv_rtl
    
     FROM sodinvhdr
    WHERE dept_number = 35
      AND (sodinvhdr.status = "A" 
       OR  sodinvhdr.status = "R")';

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {
                $row_data = $results[0];
                $dept_35_tot_value_cst = $row_data->dept_35_tot_value_cst;
                $dept_35_tot_value_rtl = $row_data->dept_35_tot_value_rtl;
                $dept_35_tot_crv_rtl = $row_data->dept_35_tot_crv_rtl;
            }
        }

        if ($dept_01_tot_value_cst > 0) {
            $dept_sodinv_rec[1]['tot_value_cst'] = $dept_01_tot_value_cst;
        }

        if ($dept_02_tot_value_cst > 0) {
            $dept_sodinv_rec[2]['tot_value_cst'] = $dept_02_tot_value_cst;
        }

        if ($dept_04_tot_value_cst > 0) {
            $dept_sodinv_rec[3]['tot_value_cst'] = $dept_04_tot_value_cst;
        }

        if ($dept_08_tot_value_cst > 0) {
            $dept_sodinv_rec[4]['tot_value_cst'] = $dept_08_tot_value_cst;
        }

        if ($dept_09_tot_value_cst > 0) {
            $dept_sodinv_rec[5]['tot_value_cst'] = $dept_09_tot_value_cst;
        }



        if ($dept_01_tot_value_rtl > 0) {
            $dept_sodinv_rec[1]['tot_value_rtl'] = $dept_01_tot_value_rtl;
        }

        if ($dept_02_tot_value_rtl > 0) {
            $dept_sodinv_rec[2]['tot_value_rtl'] = $dept_02_tot_value_rtl;
        }

        if ($dept_04_tot_value_rtl > 0) {
            $dept_sodinv_rec[3]['tot_value_rtl'] = $dept_04_tot_value_rtl;
        }

        if ($dept_08_tot_value_rtl > 0) {
            $dept_sodinv_rec[4]['tot_value_rtl'] = $dept_08_tot_value_rtl;
        }

        if ($dept_09_tot_value_rtl > 0) {
            $dept_sodinv_rec[5]['tot_value_rtl'] = $dept_09_tot_value_rtl;
        }



        if ($dept_01_tot_crv_rtl > 0) {
            $dept_sodinv_rec[1]['tot_crv_rtl'] = $dept_01_tot_crv_rtl;
        }

        if ($dept_02_tot_crv_rtl > 0) {
            $dept_sodinv_rec[2]['tot_crv_rtl'] = $dept_02_tot_crv_rtl;
        }

        if ($dept_04_tot_crv_rtl > 0) {
            $dept_sodinv_rec[3]['tot_crv_rtl'] = $dept_04_tot_crv_rtl;
        }

        if ($dept_08_tot_crv_rtl > 0) {
            $dept_sodinv_rec[4]['tot_crv_rtl'] = $dept_08_tot_crv_rtl;
        }

        if ($dept_09_tot_crv_rtl > 0) {
            $dept_sodinv_rec[5]['tot_crv_rtl'] = $dept_09_tot_crv_rtl;
        }
        
        
        
         $work_data->dept_sodinv_rec=$dept_sodinv_rec;
        $work_data->dept_sodinv_non_disp_rec=$dept_sodinv_non_disp_rec;


    return View::make('mktmgr.nonperishableinvprepview')->with('data', $work_data);
    }

    public function getnonPerishableInvDeptInfo() {
        
        $work_data = new StdClass();
        
        $dept_inp = Route::input('deptno');
        
        if($dept_inp==0 || $dept_inp==''){        
            Session::flash('alert-danger', 'Requested dept. cannot found.'); 
            return Redirect::route('mktmgr-nonperishableinvprepview');
        }
        
        
         //DECLARE sodinv_hdrs CURSOR FOR
      $sql='SELECT create_date, inventory_number, employee_id, total_scans, total_value_cst, total_value_rtl, total_crv_rtl, sodinvhdr.status as invhdr_status, inventory_type, inventory_area, inventory_section
        FROM sodinvhdr
       WHERE dept_number = '.$dept_inp.' AND sodinvhdr.status != "D"
         AND sodinvhdr.status != "T"
    ORDER BY status, inventory_area, inventory_section, inventory_number';

         
         $results = DB::select(DB::raw($sql));
//echo "<pre>";print_r($results); die;
        $sodinv_hdrs_rec=array();
        $sodinv_hdrs_non_disp_rec=array();
        if (!empty($results)) {

            $cnt=1;
            foreach($results as  $row ) {
                
                
                            $sodinv_hdrs_rec[$cnt]['create_date']=$row->create_date;
                            $sodinv_hdrs_rec[$cnt]['inventory_number']=$row->inventory_number;
                            $sodinv_hdrs_rec[$cnt]['employee_id']=$row->employee_id;
                            $sodinv_hdrs_rec[$cnt]['total_scans']=$row->total_scans;
                            $sodinv_hdrs_rec[$cnt]['total_value_cst']=$row->total_value_cst;
                            $sodinv_hdrs_rec[$cnt]['total_value_rtl']=$row->total_value_rtl;
                            $sodinv_hdrs_rec[$cnt]['total_crv_rtl']=$row->total_crv_rtl;
                            $sodinv_hdrs_rec[$cnt]['invhdr_status']=$row->invhdr_status;
                            $sodinv_hdrs_rec[$cnt]['inventory_type']=$row->inventory_type;
                            $sodinv_hdrs_rec[$cnt]['inventory_area']=$row->inventory_area; 
                            $sodinv_hdrs_rec[$cnt]['inventory_section']=$row->inventory_section; 

                    $ws_status = $sodinv_hdrs_rec[$cnt]['invhdr_status'];

                    $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Unknown";

                    if( $ws_status == "O"){
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Open";
                    }

                    if($ws_status == "V"){
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Void";
                    }

                    if( $ws_status == "D") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Deleted";
                    }

                    if( $ws_status == "A") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Approved";
                    }

                    if( $ws_status == "C") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Closed";
                    }

                    if( $ws_status == "S") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Sent";
                    }

                    if( $ws_status == "R") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Reapproved";
                    }

                    if( $ws_status == "T") {
                        $sodinv_hdrs_non_disp_rec[$cnt]['hdr_status'] = "Transmitted";
                    }

                    $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Unknown";

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "ED") { 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Everyday Stock"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "VL") { 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Valentines Day"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "ES"){ 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Easter"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "SM"){ 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Spring/Summer"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "HR"){
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Harvest"; 
                    }

                    if( $sodinv_hdrs_rec[$cnt]['inventory_type'] == "HL"){ 
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Halloween"; 
                    }

                    if($sodinv_hdrs_rec[$cnt]['inventory_type'] == "CH"){
                        $sodinv_hdrs_non_disp_rec[$cnt]['inv_type_desc'] = "Christmas"; 
                    }

                    $cnt++;

            }
        }
        
        //echo "<pre>"; print_r($sodinv_hdrs_rec); die;
        
        $work_data->sodinv_hdrs_rec=$sodinv_hdrs_rec;
        $work_data->sodinv_hdrs_non_disp_rec=$sodinv_hdrs_non_disp_rec;
        $work_data->dept_inp=$dept_inp;
        return View::make('mktmgr.noninvdeptinfo')->with('data', $work_data);
    }

    public function getNonInvShowDetail(){
        
         $work_data = new StdClass();
        
        
       
                
                
        $sodinv_dtl['create_date'] = date('m/d/Y',strtotime(Input::get('create_date')));
        $sodinv_dtl['inventory_number'] = Input::get('inventory_number');
        $sodinv_dtl['inventory_type'] = Input::get('inventory_type').' - '.Input::get('inv_type_desc');
        $sodinv_dtl['inventory_area'] = Input::get('inventory_area');
        $sodinv_dtl['inventory_section'] = Input::get('inventory_section');
        $sodinv_dtl['employee_id'] = Input::get('employee_id');
        $sodinv_dtl['total_scans'] = Input::get('total_scans');
        $sodinv_dtl['total_value_cst'] = Input::get('total_value_cst');
        $sodinv_dtl['total_value_rtl'] = Input::get('total_value_rtl');
        $sodinv_dtl['total_crv_rtl'] = Input::get('total_crv_rtl');
        $sodinv_dtl['hdr_status'] = Input::get('hdr_status');
        $sodinv_dtl['invhdr_status'] = Input::get('invhdr_status');
              
                
                
        
        $work_data->sodinv_dtl=$sodinv_dtl;
        return View::make('mktmgr.nonperisinvshowdetail')->with('data', $work_data)->render();
        
    }
    
    
    public function getNonApproveInv(){
        
        $response=array();
        
        $response['status']=false;
        
        $inventory_number=Input::get('inventory_number');
        $invhdr_status=Input::get('invhdr_status');
        $hdr_status=Input::get('hdr_status');
        
        $new_invhdr_status=$invhdr_status;
        $new_hdr_status=$hdr_status;
        
        if ($invhdr_status == "S") {
            $results = DB::update('UPDATE sodinvhdr SET sodinvhdr.status = "S" WHERE sodinvhdr.seq_number =' . $inventory_number);
        } else {
            if ($invhdr_status == "R") {
                $results = DB::update('UPDATE sodinvhdr SET sodinvhdr.status = "R" WHERE sodinvhdr.seq_number =' . $inventory_number);
            } else {
                if ($invhdr_status == "T") {
                    $results = DB::update('UPDATE sodinvhdr SET sodinvhdr.status = "T" WHERE sodinvhdr.seq_number =' . $inventory_number);
                } else {
                    $results = DB::update('UPDATE sodinvhdr SET sodinvhdr.status = "A" WHERE sodinvhdr.seq_number =' . $inventory_number);
                }
            }
        }

        if (!$results) {
            $response['msg'] = "Problem Updating Inventory";
        } else {
            $response['status'] = true;
            if ($invhdr_status == "S") {
                $response['msg'] = "Inventory cannot be changed in this screen";
                $new_invhdr_status = "S";
                $new_hdr_status = "Sent";
            } else {
                if ($invhdr_status == "R") {
                    $response['msg'] = "Inventory is already Approved or Reapproved";
                    $new_invhdr_status = "R";
                    $new_hdr_status = "Reapproved";
                } else {
                    if ($invhdr_status == "T") {
                        $response['msg'] = "Inventory cannot be changed in this screen";
                        $new_invhdr_status = "T";
                        $new_hdr_status = "Transmit";
                    } else {
                        $response['msg'] = "Inventory has been updated";
                        $new_invhdr_status = "A";
                        $new_hdr_status = "Approved";
                    }
                }
            }
        }
        
        $response['hdr_status']=$new_hdr_status;
        $response['invhdr_status']=$new_invhdr_status;
        echo json_encode($response); die;
    }
    
    
    
    public function getNonVoidInv(){
        
        $response=array();
        
        $response['status']=false;
        
        $inventory_number=Input::get('inventory_number');
        $invhdr_status=Input::get('invhdr_status');
        $hdr_status=Input::get('hdr_status');
        
        
        $new_invhdr_status=$invhdr_status;
        $new_hdr_status=$hdr_status;
        
        if ($invhdr_status == "R") {
            $results = DB::update('UPDATE sodinvhdr SET sodinvhdr.status = "T" WHERE sodinvhdr.seq_number =' . $inventory_number);
        } else {
            if ($invhdr_status == "T") {
                $results = DB::update('UPDATE sodinvhdr SET sodinvhdr.status = "T" WHERE sodinvhdr.seq_number =' . $inventory_number);
            } else {
                if ($invhdr_status == "S") {
                    $results = DB::update('UPDATE sodinvhdr SET sodinvhdr.status = "S" WHERE sodinvhdr.seq_number =' . $inventory_number);
                } else {
                    $results = DB::update('UPDATE sodinvhdr SET sodinvhdr.status = "V" WHERE sodinvhdr.seq_number =' . $inventory_number);
                }
            }
        }

        if (!$results) {
            $response['msg'] = "Problem Updating Inventory";
        } else {
            $response['status'] = true;
            if ($invhdr_status == "R") {
                $response['msg'] = "Inventory has been updated";
                $new_invhdr_status = "T";
                $new_hdr_status = "Transmit";
            } else {
                if ($invhdr_status == "T") {
                    $response['msg'] = "Cannot VOID a previously SENT inventory";
                    $new_invhdr_status = "T";
                    $new_hdr_status = "Transmit";
                } else {
                    if ($invhdr_status == "S") {
                        $response['msg'] = "Cannot VOID an inventory that was SENT";
                        $new_invhdr_status = "S";
                        $new_hdr_status = "Sent";
                    } else {
                        $response['msg'] = "Inventory has been updated";
                        $new_invhdr_status = "V";
                        $new_hdr_status = "Void";
                    }
                }
            }
        }
        $response['hdr_status']=$new_hdr_status;
        $response['invhdr_status']=$new_invhdr_status;
        echo json_encode($response); die;
    }
    
    public function getNonMoreInfoSelDate() {
        
        
        $work_data = new StdClass();
        
        $dept_inp = Input::get('deptno');
        
        if($dept_inp==0 || $dept_inp==''){        
            Session::flash('alert-danger', 'Requested dept. cannot found.'); 
            return Redirect::route('mktmgr-nonperishableinvprepview');
        }
        
        
        $sodinvno = Input::get('sodinvno');
        
        if($sodinvno==0 || $sodinvno==''){        
            Session::flash('alert-danger', 'Requested inventory number cannot found.'); 
            return Redirect::route('mktmgr-noninvdeptinfo',$dept_inp);
        }
        
        
        $sql='SELECT seq_number, item_desc, upc_cost, rtl_amt, quantity                         
        FROM sodinvdtl WHERE hdr_seq_number = '.$sodinvno.' AND sodinvdtl.status = "I"';
        
           $results = DB::select(DB::raw($sql));
//echo "<pre>";print_r($results); die;
           
           $sodinv_dtls_rec=array();
        if (!empty($results)) {

            $cnt=1;
            foreach($results as  $row ){
                $sodinv_dtls_rec[$cnt]['seq_number'] = $row->seq_number;
                $sodinv_dtls_rec[$cnt]['item_desc'] = $row->item_desc;
                $sodinv_dtls_rec[$cnt]['upc_cost'] = $row->upc_cost;
                $sodinv_dtls_rec[$cnt]['rtl_amt'] = $row->rtl_amt;
                $sodinv_dtls_rec[$cnt]['quantity'] = $row->quantity;

                    $sodinv_dtls_rec[$cnt]['ext_value_cst'] = ( $sodinv_dtls_rec[$cnt]['upc_cost'] * $sodinv_dtls_rec[$cnt]['quantity']); 
                    $sodinv_dtls_rec[$cnt]['ext_value_rtl'] = ( $sodinv_dtls_rec[$cnt]['rtl_amt'] * $sodinv_dtls_rec[$cnt]['quantity']);
                        
                    $cnt++;
            }
        }
        
        
        $sql='SELECT create_date, inventory_number, employee_id, total_scans, total_value_cst, total_value_rtl, total_crv_rtl, sodinvhdr.status as invhdr_status, inventory_type, inventory_area, inventory_section
        FROM sodinvhdr
       WHERE dept_number = '.$dept_inp.' AND inventory_number = '.$sodinvno.' AND sodinvhdr.status != "D"
         AND sodinvhdr.status != "T"
    ORDER BY status, inventory_area, inventory_section, inventory_number';

         
         $results = DB::select(DB::raw($sql));
//echo "<pre>";print_r($results); die;
        $sodinv_hdrs_rec=array();

        if (!empty($results)) {

            $cnt=1;
            foreach($results as  $row ) {
                
                
                            $sodinv_hdrs_rec[$cnt]['create_date']=$row->create_date;
                            $sodinv_hdrs_rec[$cnt]['inventory_number']=$row->inventory_number;
                            $sodinv_hdrs_rec[$cnt]['employee_id']=$row->employee_id;
                            $sodinv_hdrs_rec[$cnt]['total_scans']=$row->total_scans;
                            $sodinv_hdrs_rec[$cnt]['total_value_cst']=$row->total_value_cst;
                            $sodinv_hdrs_rec[$cnt]['total_value_rtl']=$row->total_value_rtl;
                            $sodinv_hdrs_rec[$cnt]['total_crv_rtl']=$row->total_crv_rtl;
                            $sodinv_hdrs_rec[$cnt]['invhdr_status']=$row->invhdr_status;
                            $sodinv_hdrs_rec[$cnt]['inventory_type']=$row->inventory_type;
                            $sodinv_hdrs_rec[$cnt]['inventory_area']=$row->inventory_area; 
                            $sodinv_hdrs_rec[$cnt]['inventory_section']=$row->inventory_section; 

                    $ws_status = $sodinv_hdrs_rec[$cnt]['invhdr_status'];

                    $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Unknown";

                    if( $ws_status == "O"){
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Open";
                    }

                    if($ws_status == "V"){
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Void";
                    }

                    if( $ws_status == "D") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Deleted";
                    }

                    if( $ws_status == "A") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Approved";
                    }

                    if( $ws_status == "C") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Closed";
                    }

                    if( $ws_status == "S") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Sent";
                    }

                    if( $ws_status == "R") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Reapproved";
                    }

                    if( $ws_status == "T") {
                        $sodinv_hdrs_rec[$cnt]['hdr_status'] = "Transmitted";
                    }

                    

                    

            }
        }
        
        
        
        $work_data->sodinv_dtls_rec=$sodinv_dtls_rec;
        $work_data->sodinv_hdrs_rec=$sodinv_hdrs_rec[1];
        $work_data->sodinvno=$sodinvno;
        $work_data->dept_inp=$dept_inp;
        return View::make('mktmgr.nonmoreinfoseldate')->with('data', $work_data);
    }

    
    public function getNonInvItemDetail(){
        $work_data = new StdClass();
        
        $hdrseq = Input::get('hdrseq');
        $seqno = Input::get('seqno');
        
       
        
        $sql='SELECT * FROM sodinvdtl WHERE hdr_seq_number = '.$hdrseq.' AND seq_number ='.$seqno;
        
           $results = DB::select(DB::raw($sql));
//echo "<pre>";print_r($results); die;
           
           $sodinv_dtl=array();
           
        if (isset($results[0]) & !empty($results[0])) {
                $row=$results[0];
                
                $sodinv_dtl['hdr_seq_number'] = $row->hdr_seq_number;
                $sodinv_dtl['seq_number'] = $row->seq_number;
                $sodinv_dtl['upc_number'] = $row->upc_number;
                $sodinv_dtl['item_number'] = $row->item_number;
                $sodinv_dtl['gl_dept'] = $row->gl_dept;
                $sodinv_dtl['upc_cost'] = $row->upc_cost;
                $sodinv_dtl['rtl_amt'] = $row->rtl_amt;
                $sodinv_dtl['scale_id'] = $row->scale_id;
                $sodinv_dtl['case_pack'] = $row->case_pack;
                $sodinv_dtl['cont_size'] = $row->cont_size;
                $sodinv_dtl['item_desc'] = $row->item_desc;
                $sodinv_dtl['quantity'] = $row->quantity;
                $sodinv_dtl['carryover_sw']= $row->carryover_sw;
                
                
                if($sodinv_dtl['carryover_sw'] == "Y" ) { $itm_status = "ACTIVE"; } else { $itm_status = "INACTIVE"; }
                    
                    $sodinv_dtl['itm_status'] = $itm_status;
                    
                    
        }
        
        //echo "<pre>"; print_r($sodinv_dtl); die;
        
       
        $work_data->sodinv_dtl=$sodinv_dtl;
        return View::make('mktmgr.nonperisinvitemscandetail')->with('data', $work_data)->render();
        
    }
}
