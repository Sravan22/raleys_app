<?php

class UserSlcadm03Controller extends BaseController {

    public function getReturnHome() {
        //return View::make('slcadm03.homepage');
        //return View::make('slcadm03.landingpage');
        return View::make('slcadm03.storepage');
    }
    public function getStoreNo()
    {
        $storeno = Input::get('store_no');
        $misc_table = DB::select('select * from misc');
        $store_config = $misc_table[0]->groc_num;
        if($store_config == 305)
        {
            $type_store = "G";
            $user_id = 1000;
        }
        if($store_config == 355)
        {
            $type_store = "F";
            $user_id = 3000;
        }
        //echo $user_id.' '.$type_store;exit;
        return View::make('slcadm03.landingpage',compact('storeno','user_id','type_store'));
    }
    
    public function getDisplayAllBatch() {

        $work_data = new StdClass();

        $batch_function = strtoupper(trim(Input::get('type')));
        $storeno = Input::get('storeno');
        $user_id = Input::get('user_id');
        //echo $storeno;exit;
        $function_desc = '';

        switch ($batch_function) {
            CASE "A":
                $function_desc = "APPLY BATCHES       ";
                break;
            CASE "D":
                $function_desc = "DISPLAY BATCHES     ";
                break;
            CASE "P":
                $function_desc = "PRINT TAGS/SIGNS    ";
                break;
            CASE "R":
                $function_desc = "RE-PRINT TAGS/SIGNS ";
                break;
            CASE "S":
                $function_desc = "PRINT SUMMARY PAGES ";
                break;
            CASE "X":
                $function_desc = "DELETE BATCHES      ";
                break;
            CASE "Z":
                $function_desc = "RESTORE BATCHES     ";
                break;
            default:
                $function_desc = "Unknown Function";
                break;
                exit;
        }

        $select_stmt = "SELECT slposhdr.batch,slposhdr.desc,slposhdr.host_batch,slposhdr.batch_loaded,slposhdr.tags_printed,slposhdr.reg_applied,slposhdr.eff_date,slposhdr.report_code";

        $from_stmt = " FROM slposhdr ";
        $order_stmt = " ORDER BY eff_date,batch ";

        $tstore = 305;
        $tbatch = 0;
        $tgtin = '';

        switch ($batch_function) {
            CASE "A":
                $where_stmt = " WHERE store_number=" . $tstore . " AND reg_applied is NULL ";
                break;
            CASE "P":
                $where_stmt = " WHERE store_number=" . $tstore . " AND (tags_printed = '1900-01-01' OR tags_printed is NULL)";
                break;
            CASE "R":
                $where_stmt = " WHERE store_number=" . $tstore . "  AND tags_printed > '1899-12-31' AND tags_printed is NOT NULL";
                break;
            CASE "Z":
                $where_stmt = " WHERE store_number=" . $tstore . "  AND reg_applied is NOT NULL ";
                break;
            default:
                $where_stmt = " WHERE store_number=" . $tstore;
                break;
        }
        $sql_stmt = $select_stmt . $from_stmt . $where_stmt . $order_stmt;
        //echo $sql_stmt;exit;

        $paginate_array['type'] = $batch_function;
        $paginate_array['storeno'] = $storeno;
        $paginate_array['user_id'] = $user_id;

        $count_results = DB::select(DB::raw($sql_stmt));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql_stmt.=" limit " . $perPage . " offset " . $offset;

        $header_curs = $results = DB::select(DB::raw($sql_stmt));


        if (!empty($count_results)) {

            foreach ($results as $key => $row) {
                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT COUNT(*) as item_count FROM slposdtl WHERE store_number=" . $tstore . " AND batch='" . $row->batch . "'";

                $itemdata = DB::select(DB::raw($sql));

                $item_count = 0;
                if (!empty($itemdata)) {
                    $rowdata = $itemdata[0];

                    $item_count = $rowdata->item_count;
                }

                $results[$key]->item_count = $item_count;



                $print_date = '';



                switch ($row->tags_printed) {
                    case "1899-12-31":
                        $print_date = " --- ";
                    case "1900-01-01":
                        $print_date = " *** ";
                    default:
                        $print_date = date('m/d', strtotime($row->tags_printed));
                }

                $results[$key]->print_date = $print_date;
            }
        }


        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        $work_data->results = $results;
        $work_data->function_desc = $function_desc;

        /*




          $getBatchCount=DB::select(DB::raw("SELECT COUNT(*) as batch_count FROM slposhdr"));

          $batch_count=0;
          if(!empty($getBatchCount)){
          $row_data = $getBatchCount[0];
          $batch_count=$row_data->batch_count;
          }
         */

        return View::make('slcadm03.displayallbatch')->with(array('data' => $work_data, 'pagination' => $pagination, 'batch_function' => $batch_function,'storeno' => $storeno,'user_id'=>$user_id));
    }

    public function getBatchDisplayDetail() {


        $work_data = new StdClass();

        $batch_function = strtoupper(trim(Input::get('type')));
        $batch_no = (int) Input::get('batch');
        $disp_type_get = strtoupper(trim(Input::get('disp_type')));
        

        if ($batch_function == '' || $batch_no == '' || $batch_no == 0) {
            Session::flash('alert-danger', " Batch Not Found ...  ");
            return Redirect::route('slcadm03-returnhome')->withInput();
        }

        $function_desc = '';

        switch ($batch_function) {
            CASE "A":
                $function_desc = "APPLY BATCHES       ";
                break;
            CASE "D":
                $function_desc = "DISPLAY BATCHES     ";
                break;
            CASE "P":
                $function_desc = "PRINT TAGS/SIGNS    ";
                break;
            CASE "R":
                $function_desc = "RE-PRINT TAGS/SIGNS ";
                break;
            CASE "S":
                $function_desc = "PRINT SUMMARY PAGES ";
                break;
            CASE "X":
                $function_desc = "DELETE BATCHES      ";
                break;
            CASE "Z":
                $function_desc = "RESTORE BATCHES     ";
                break;
            default:
                $function_desc = "Unknown Function";
                break;
                exit;
        }

        $select_stmt = "SELECT slposhdr.batch,slposhdr.desc,slposhdr.host_batch,slposhdr.batch_loaded,slposhdr.tags_printed,slposhdr.reg_applied,slposhdr.eff_date,slposhdr.report_code";

        $from_stmt = " FROM slposhdr ";
        $order_stmt = " ORDER BY eff_date,batch ";

        $tstore = 305;
        $tbatch = $batch_no;
        $tgtin = '';

        switch ($batch_function) {
            CASE "A":
                $where_stmt = " WHERE store_number=" . $tstore . " AND reg_applied IS NULL";
                break;
            CASE "P":
                $where_stmt = " WHERE store_number=" . $tstore . " AND (tags_printed = '1900-01-01' OR tags_printed is NULL)";
                break;
            CASE "R":
                $where_stmt = " WHERE store_number=" . $tstore . "  AND tags_printed > '1899-12-31' AND tags_printed is NOT NULL";
                break;
            CASE "Z":
                $where_stmt = " WHERE store_number=" . $tstore . "  AND reg_applied is NOT NULL ";
                break;
            default:
                $where_stmt = " WHERE store_number=" . $tstore;
                break;
        }

        $where_stmt.=" AND batch='" . $batch_no . "' ";

        $sql_stmt = $select_stmt . $from_stmt . $where_stmt . $order_stmt;


        $paginate_array['type'] = $batch_function;




        $header_curs = $results = DB::select(DB::raw($sql_stmt));


        $eff_date = '';

        if (!empty($results)) {

            foreach ($results as $key => $row) {
                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT COUNT(*) as item_count FROM slposdtl WHERE store_number=" . $tstore . " AND batch='" . $row->batch . "'";

                $itemdata = DB::select(DB::raw($sql));

                $item_count = 0;
                if (!empty($itemdata)) {
                    $rowdata = $itemdata[0];

                    $item_count = $rowdata->item_count;
                }

                $results[$key]->item_count = $item_count;



                $print_date = '';



                switch ($row->tags_printed) {
                    case "1899-12-31":
                        $print_date = " --- ";
                    case "1900-01-01":
                        $print_date = " *** ";
                    default:
                        $print_date = date('m/d', strtotime($row->tags_printed));
                }

                $results[$key]->print_date = $print_date;

                $eff_date = $row->reg_applied;
            }
        }



        $work_data->results = $results;
        $work_data->function_desc = $function_desc;

        if ($disp_type_get == '') {
            $disp_type = 'T';
            if ($batch_function == "X" || $batch_function == "A") {
                $disp_type = 'H';
            }
        } else {
            $disp_type = $disp_type_get;
        }


        


        $disp_tag['sign_array'] = array();
        $disp_tag['tag_array'] = array();

        $disp_depts = array();
        if ($disp_type == "T") {
            $disp_tag = $this->disp_tags($batch_no);
        } elseif ($disp_type == "D") {
            $disp_depts = $this->disp_depts($batch_function, $batch_no);
        } elseif ($disp_type == "H") {
            //do nothing
        } else {
            //do nothing
        }
        
        
        
        $work_data->disp_tag = $disp_tag;
        $work_data->disp_depts = $disp_depts;
        $work_data->batch_no = $batch_no;


        return View::make('slcadm03.displaydetailbatch')->with(array('data' => $work_data, 'batch_function' => $batch_function, 'disp_type' => $disp_type));
    }

    function disp_tags($batch_no) {
        $tbatch = $batch_no;
        $tstore = 305;



        # Sign Counts #
        $sign_array = array();



        $sign_items_curs = DB::select(DB::raw("SELECT sign_flag,COUNT(*) as tcount FROM slposdtl WHERE store_number='" . $tstore . "' and batch='" . $tbatch . "' and sign_flag !='' GROUP BY sign_flag ORDER BY sign_flag"));


        $tag_count = 1;
        if (!empty($sign_items_curs)) {

            foreach ($sign_items_curs as $sign) {

                $tsign_id = $sign->sign_flag;
                $tcount = $sign->tcount;

                $tsign_name = '';
                $get_sign_name = DB::select(DB::raw("SELECT * FROM sssign  WHERE sign_id='" . $tsign_id . "'"));

                if (!empty($get_sign_name)) {
                    $row_sign = $get_sign_name[0];
                    $tsign_name = $row_sign->sign_name;
                    $stock_code = $row_sign->stock_code;
                }


                $sign_array[$tag_count]['sign_type'] = $stock_code . " " . $tsign_name;
                $sign_array[$tag_count]['item_count'] = $tcount;
                $sign_array[$tag_count]['sign_count'] = 0;


                $get_sign_qty = DB::select(DB::raw("SELECT SUM(sign_qty) as sign_count FROM slitem WHERE store_number='" . $tstore . "' AND gtin IN (SELECT gtin FROM slposdtl WHERE store_number='" . $tstore . "' AND sign_flag='" . $tsign_id . "' AND batch='" . $tbatch . "')"));


                if (!empty($get_sign_qty)) {
                    $row_sign_qty = $get_sign_qty[0];
                    $sign_array[$tag_count]['sign_count'] = $row_sign_qty->sign_count;
                }


                $tag_count++;
            }
        }



        # Tag  Counts #
        $tag_array = array();

        $tag_items_curs = DB::select(DB::raw("SELECT tag_flag,COUNT(*) as tcount FROM slposdtl WHERE store_number='" . $tstore . "' and batch='" . $tbatch . "' and tag_flag !='' GROUP BY tag_flag ORDER BY tag_flag"));

        $tag_count = 1;
        if (!empty($tag_items_curs)) {
            foreach ($tag_items_curs as $tag) {

                $tsign_id = $tag->tag_flag;
                $tcount = $tag->tcount;

                $tsign_name = '';
                $get_sign_name = DB::select(DB::raw("SELECT * FROM sssign  WHERE sign_id='" . $tsign_id . "'"));

                if (!empty($get_sign_name)) {
                    $row_sign = $get_sign_name[0];
                    $tsign_name = $row_sign->sign_name;
                    $stock_code = $row_sign->stock_code;
                }

                $tag_array[$tag_count]['tag_type'] = $stock_code . " " . $tsign_name;
                $tag_array[$tag_count]['item_count'] = $tcount;
                $tag_array[$tag_count]['tag_count'] = 0;

                $get_tag_qty = DB::select(DB::raw("SELECT SUM(tag_qty) as tag_qty FROM slitem WHERE store_number='" . $tstore . "' AND gtin IN (SELECT gtin FROM slposdtl WHERE store_number='" . $tstore . "' AND tag_flag='" . $tsign_id . "' AND batch='" . $tbatch . "')"));

                if (!empty($get_tag_qty)) {
                    $row_tag_qty = $get_tag_qty[0];
                    $tag_array[$tag_count]['tag_count'] = $row_tag_qty->tag_qty;
                }

                $tag_count++;
            }
        }

        $response['sign_array'] = $sign_array;
        $response['tag_array'] = $tag_array;

        return $response;
    }

    function disp_depts($batch_function, $batch_no) {
        $tbatch = $batch_no;
        $tstore = 305;

        $dept_array = array();
        $total_array = array();
        $dept_count = 0;
        $tdept = 0;
        $tsum_code = 0;
        $tcount = 0;
        $dept_prompt = '';
        $cont_flag = '';




        $sum_curs = DB::select(DB::raw("SELECT dept,sum_code,COUNT(*) as tcount FROM slpossum WHERE store_number='" . $tstore . "' AND batch='" . $tbatch . "' GROUP BY dept,sum_code ORDER BY dept,sum_code"));

        //echo "<pre>"; print_r($sum_curs); //die;
        //$cur_dept = 0;
        $counter = 1;
        $cont_flag = "Y";
        if (!empty($sum_curs)) {
            foreach ($sum_curs as $curs) {

                $tdept = $curs->dept;
                $tcount = $curs->tcount;
                $tsum_code = $curs->sum_code;
                
                

                //==create blank array of deptwise sumcode wise with 0 value  then loop value assign it sum it make total horizontal here and vertical in view
                //==if detail page then and then call below function for get dept_detail so make 2 different function
                
                if(!isset($dept_array[$tdept])){
                    
                    for($i=0;$i<=7;$i++){
                        $dept_array[$tdept][$i]=0;
                    }
                    $dept_array[$tdept]['total']=0;
                    
                } 
                
                $dept_array[$tdept][$tsum_code]+=$tcount;
                $dept_array[$tdept]['total']+=$tcount;
                
                
               
                
                
                /* $dept_detail = $this->dept_detail($batch_no, $eff_date, $tdept, $dept_prompt);
                
                
                $department_list_array[] = $dept_detail;if ($tdept != $cur_dept) {
                  if ($cur_dept != 0) {

                  $counter++;

                  $dept_prompt = "P";
                  while ($dept_prompt == "P") {
                  if ($batch_function == "S") {
                  // PROMPT "Press Enter for more depts., Dept. #, A for ALL, or Q to Quit: " FOR dept_prompt
                  } else {
                  //PROMPT "Press Enter for more depts.,Dept. # for UPC, & Details or Q to Quit: " FOR dept_prompt
                  }

                  if ($dept_prompt == "p" || $dept_prompt == "P") {
                  $dept_prompt = "P";
                  //CALL fgl_prtscr()
                  } else {

                  if ($dept_prompt != "q" && $dept_prompt != "Q" && $dept_prompt != '') {
                  if ($batch_function == "D") {
                  $department_list_array[]=$this->dept_detail($batch_no,$eff_date,$cur_dept,$dept_prompt);
                  } else {
                  /* OPEN WINDOW print_window AT 2,2
                  WITH 23 ROWS, 78 COLUMNS
                  ATTRIBUTE (MESSAGE LINE LAST, PROMPT LINE LAST)
                  LET run_cmd = "newfglgo slsum_page ",
                  tstore USING "<<<"," ",
                  batch_no USING "<<<<<<"," ",dept_prompt
                  RUN run_cmd
                  CLOSE WINDOW print_window *
                  }
                  $dept_prompt = "P";
                  }
                  }
                  #herex
                  }
                  //CALL dept_header(1)

                  }
                  $cur_dept = $tdept;
                  }
                  $new_tsum = $tsum_code + 1;
                  $dept_array[$new_tsum] = $tcount;
                  if (isset($total_array[$new_tsum])) {
                  $total_array[$new_tsum] = $total_array[$new_tsum] + $tcount;
                  } else {
                  $total_array[$new_tsum] = $tcount;
                  } */

                
            }
        }
        //echo "<pre>"; print_r($dept_array); die;
        return $dept_array;
    }
    
    public function batchDepartmentDetails() {
        
        $work_data = new StdClass();

        $batch_function = strtoupper(trim(Input::get('type')));
        $batch_no = (int) Input::get('batch');
        $disp_type_get = strtoupper(trim(Input::get('disp_type')));
        $cur_dept = (int) Input::get('dept_id');

        if ($batch_function == '' || $batch_no == '' || $batch_no == 0 || $cur_dept==0 || $cur_dept=='') {
            Session::flash('alert-danger', " Batch Not Found ...  ");
            return Redirect::route('slcadm03-returnhome')->withInput();
        }
        
        $tstore = 305;
        $tbatch = $batch_no;
        
       
           
               $dept_detail_list = $this->dept_detail($batch_no,$cur_dept);
               
              // echo "<pre>"; print_r($dept_detail_list); die;
             $work_data->dept_detail_list = $dept_detail_list;
       $work_data->batch_no = $batch_no;
       


        return View::make('slcadm03.batchdepartmentdetails')->with(array('data' => $work_data, 'batch_function' => $batch_function, 'disp_type' => $disp_type_get, 'cur_dept' => $cur_dept));
        
        
       
    }
    
   
    function dept_detail($batch_no, $in_dept) {

        $tbatch = $batch_no;
        $tstore = 305;
        $csum_code = 0;

        $cur_dept = $in_dept;

        $teff_date = $eff_date = '';
        $get_teff_date = DB::select(DB::raw("SELECT eff_date FROM slposhdr WHERE store_number='" . $tstore . "' AND batch='" . $tbatch . "'"));

        if (!empty($get_teff_date)) {
            $row_teff_date = $get_teff_date[0];
            $teff_date = $eff_date = $row_teff_date->eff_date;
        }
        $tgtin = '';

        $tsku_number = 0;

        $dept_curs = DB::select(DB::raw("SELECT slpossum.dept, slpossum.sum_code, slpossum.gtin,slitem.sku_number, slitem.desc_18,slitem.price,slitem.nfor_price, slitem.nfor_unt, slitem.ad_price,slitem.ad_nfor_price,slitem.ad_nfor_unt,slitem.ad_start,slitem.ad_end, slitem.bb_price, slitem.bb_nfor_price, slitem.bb_nfor_unt, slitem.bb_start,slitem.bb_end, slitem.sign_qty,slitem.tag_qty,slitem.case_pack,slitem.item_pack,slitem.item_size,slitem.item_meas,slitem.dept_override,slitem.tax_flag,slitem.fs_flag,slitem.age_flag, slitem.scale_flag,slitem.wic_flag,slitem.scrip_flag,slitem.emp_discount,slitem.liq_discount,slitem.ctrl_sub,slitem.tag_type,slitem.eff_date FROM slpossum,slitem WHERE slpossum.store_number='" . $tstore . "' and slitem.store_number='" . $tstore . "' AND slpossum.batch='" . $tbatch . "' AND slpossum.dept='" . $cur_dept . "' AND slitem.gtin=slpossum.gtin ORDER BY slpossum.sum_code,slitem.sku_number,slitem.tag_type DESC,slpossum.gtin"));

        //echo "<pre>"; print_r($dept_curs); die;

        $item_rec_full = array();

        if(!empty($dept_curs)){

        foreach ($dept_curs as $rowdept) {
            $item_rec = array();
            $save_rec = array();

            $item_rec['dept'] = $rowdept->dept;
            $item_rec['sum_code'] = $rowdept->sum_code;
            $item_rec['gtin'] = $rowdept->gtin;
            $item_rec['sku_number'] = $rowdept->sku_number;
            $item_rec['desc_18'] = $rowdept->desc_18;
            $item_rec['price'] = $rowdept->price;
            $item_rec['nfor_price'] = $rowdept->nfor_price;
            $item_rec['nfor_unt'] = $rowdept->nfor_unt;
            $item_rec['ad_price'] = $rowdept->ad_price;
            $item_rec['ad_nfor_price'] = $rowdept->ad_nfor_price;
            $item_rec['ad_nfor_unt'] = $rowdept->ad_nfor_unt;
            $item_rec['ad_start'] = $rowdept->ad_start;
            $item_rec['ad_end'] = $rowdept->ad_end;
            $item_rec['bb_price'] = $rowdept->bb_price;
            $item_rec['bb_nfor_price'] = $rowdept->bb_nfor_price;
            $item_rec['bb_nfor_unt'] = $rowdept->bb_nfor_unt;
            $item_rec['bb_start'] = $rowdept->bb_start;
            $item_rec['bb_end'] = $rowdept->bb_end;
            $item_rec['sign_qty'] = $rowdept->sign_qty;
            $item_rec['tag_qty'] = $rowdept->tag_qty;
            $item_rec['case_pack'] = $rowdept->case_pack;
            $item_rec['item_pack'] = $rowdept->item_pack;
            $item_rec['item_size'] = $rowdept->item_size;
            $item_rec['item_meas'] = $rowdept->item_meas;
            $item_rec['dept_override'] = $rowdept->dept_override;
            $item_rec['tax_flag'] = $rowdept->tax_flag;
            $item_rec['fs_flag'] = $rowdept->fs_flag;
            $item_rec['age_flag'] = $rowdept->age_flag;
            $item_rec['scale_flag'] = $rowdept->scale_flag;
            $item_rec['wic_flag'] = $rowdept->wic_flag;
            $item_rec['scrip_flag'] = $rowdept->scrip_flag;
            $item_rec['emp_discount'] = $rowdept->emp_discount;
            $item_rec['liq_discount'] = $rowdept->liq_discount;
            $item_rec['ctrl_sub'] = $rowdept->ctrl_sub;
            $item_rec['tag_type'] = $rowdept->tag_type;
            $item_rec['eff_date'] = $rowdept->eff_date;


            if ($item_rec['sku_number'] == $tsku_number) {
                $second_upc = "Y";
            } else {
                $second_upc = "N";
                $tsku_number = $item_rec['sku_number'];
            }

            if (strtotime($item_rec['eff_date']) > strtotime($eff_date)) {
                # Look for this item in slitemsv, where the effective date <= Today #
                $tgtin = $item_rec['gtin'];

                $save_curs = DB::select(DB::raw("SELECT price, nfor_price, nfor_unt,ad_price, ad_nfor_price, ad_nfor_unt, ad_start, ad_end,bb_price, bb_nfor_price, bb_nfor_unt, bb_start, bb_end,eff_date FROM slitemsv WHERE store_number='" . $tstore . "' AND gtin='" . $tgtin . "' ORDER BY eff_date DESC"));




                if (!empty($save_curs)) {

                    foreach ($save_curs as $rowsave) {
                        $save_rec['price'] = $rowsave->price;
                        $save_rec['nfor_price'] = $rowsave->nfor_price;
                        $save_rec['nfor_unt'] = $rowsave->nfor_unt;
                        $save_rec['ad_price'] = $rowsave->ad_price;
                        $save_rec['ad_nfor_price'] = $rowsave->ad_nfor_price;
                        $save_rec['ad_nfor_unt'] = $rowsave->ad_nfor_unt;
                        $save_rec['ad_start'] = $rowsave->ad_start;
                        $save_rec['ad_end'] = $rowsave->ad_end;
                        $save_rec['bb_price'] = $rowsave->bb_price;
                        $save_rec['bb_nfor_price'] = $rowsave->bb_nfor_price;
                        $save_rec['bb_nfor_unt'] = $rowsave->bb_nfor_unt;
                        $save_rec['bb_start'] = $rowsave->bb_start;
                        $save_rec['bb_end'] = $rowsave->bb_end;
                        $save_rec['eff_date'] = $rowsave->eff_date;
                        if (strtotime($save_rec['eff_date']) <= strtotime($eff_date)) {
                            # Found the record we're looking for ... bail from FOREACH
                            $item_rec['price'] = $save_rec['price'];
                            $item_rec['nfor_price'] = $save_rec['nfor_price'];
                            $item_rec['nfor_unt'] = $save_rec['nfor_unt'];
                            $item_rec['ad_price'] = $save_rec['ad_price'];
                            $item_rec['ad_nfor_price'] = $save_rec['ad_nfor_price'];
                            $item_rec['ad_nfor_unt'] = $save_rec['ad_nfor_unt'];
                            $item_rec['ad_start'] = $save_rec['ad_start'];
                            $item_rec['ad_end'] = $save_rec['ad_end'];
                            $item_rec['bb_price'] = $save_rec['bb_price'];
                            $item_rec['bb_nfor_price'] = $save_rec['bb_nfor_price'];
                            $item_rec['bb_nfor_unt'] = $save_rec['bb_nfor_unt'];
                            $item_rec['bb_start'] = $save_rec['bb_start'];
                            $item_rec['bb_end'] = $save_rec['bb_end'];
                            break;
                        }
                    }
                }
            }


            $get_detail_rec = DB::select(DB::raw("SELECT * FROM slposdtl WHERE store_number='" . $tstore . "' AND gtin='" . $item_rec['gtin'] . "' and batch='" . $batch_no . "' "));



            $tsign_count = 0;
            $ttag_count = 0;

            if (!empty($get_detail_rec)) {
                $detail_rec = $get_detail_rec[0];
                if ($detail_rec->sign_flag != '') {
                    $tsign_count = $item_rec['sign_qty'];
                } else {
                    $tsign_count = 0;
                }
                if ($detail_rec->tag_flag != '') {
                    $ttag_count = $item_rec['tag_qty'];
                } else {
                    $ttag_count = 0;
                }
            }


            $item_rec['tsign_count'] = $tsign_count;
            $item_rec['ttag_count'] = $ttag_count;

            $tchng_desc = '';
            if ($item_rec['sum_code'] != $csum_code) {
                $csum_code = $item_rec['sum_code'];
                switch ($csum_code) {
                    case 0:
                        $tchng_desc = "*** ITEMS ON AD ***  ";
                        break;
                    case 1:
                        $tchng_desc = "*** OFF AD ***       ";
                        break;
                    case 2:
                        $tchng_desc = "*** ON TPR ***       ";
                        break;
                    case 3:
                        $tchng_desc = "*** OFF TPR ***      ";
                        break;
                    case 4:
                        $tchng_desc = "*** PRICE CHANGES ***";
                        break;
                    case 5:
                        $tchng_desc = "*** ITEM CHANGES *** ";
                        break;
                    case 6:
                        $tchng_desc = "*** DISCONTINUED *** ";
                        break;
                    case 7:
                        $tchng_desc = "*** NEW ITEMS ***    ";
                        break;
                    default:
                        $tchng_desc = "*** CODE " . $item_rec['sum_code'] . " *** ";
                        break;
                }
            }

            $item_rec['tchng_desc'] = $tchng_desc;

            ##################
            # Regular Retail #
            ##################
            $reg_retail = $item_rec['price'];

            if ($item_rec['nfor_price'] != '' AND $item_rec['nfor_unt'] > 1) {
                $reg_retail = $item_rec['nfor_unt'] . "/" . $item_rec['nfor_price'];
            } elseif ($item_rec['nfor_price'] != '' AND $item_rec['nfor_unt'] == 1) {
                $reg_retail = $item_rec['nfor_unt'];
            }

            $item_rec['reg_retail'] = $reg_retail;

            ##################
            # Promo   Retail #
            ##################
            $promo_retail = '';
            switch ($item_rec['sum_code']) {
                case 0:
                    if ($item_rec['ad_nfor_price'] != '' && $item_rec['ad_nfor_unt'] > 1) {
                        
                    } else {
                        if ($item_rec['ad_nfor_price'] != '' && $item_rec['ad_nfor_unt'] == 1) {
                            $promo_retail = $item_rec['ad_nfor_price'];
                        } else {
                            $promo_retail = $item_rec['ad_price'];
                        }
                    }
                    break;
                case 2:          # On TPR #
                    if ($item_rec['bb_nfor_price'] != '' && $item_rec['bb_nfor_unt'] > 1) {
                        $promo_retail = $item_rec['bb_nfor_unt'] . "/" . $item_rec['bb_nfor_price'];
                    } else {
                        if ($item_rec['bb_nfor_price'] != '' && $item_rec['bb_nfor_unt'] == 1) {
                            $promo_retail = $item_rec['bb_nfor_price'];
                        } else {
                            $promo_retail = $item_rec['bb_price'];
                        }
                    }

                    break;
                default:
                    $promo_retail = "";
                    if ($item_rec['ad_start'] != '' && $item_rec['ad_end'] != '' && strtotime($item_rec['ad_start']) <= strtotime($teff_date) && strtotime($item_rec['ad_end']) >= strtotime($teff_date)) {
                        if ($item_rec['ad_nfor_price'] != '' && $item_rec['ad_nfor_unt'] > 1) {
                            $promo_retail = $item_rec['ad_nfor_unt'] . "/" . $item_rec['ad_nfor_price'];
                        } else {
                            if ($item_rec['ad_nfor_price'] != '' && $item_rec['ad_nfor_unt'] == 1) {
                                $promo_retail = $item_rec['ad_nfor_price'];
                            } else {
                                $promo_retail = $item_rec['ad_price'];
                            }
                        }
                    } else {
                        if ($item_rec['bb_start'] != '' && $item_rec['bb_end'] != '' && strtotime($item_rec['bb_start']) <= strtotime($teff_date) && strtotime($item_rec['bb_end']) >= strtotime($teff_date)) {

                            if ($item_rec['bb_nfor_price'] != '' && $item_rec['bb_nfor_unt'] > 1) {
                                $promo_retail = $item_rec['bb_nfor_unt'] . "/" . $item_rec['bb_nfor_price'];
                            } else {
                                if ($item_rec['bb_nfor_price'] != '' && $item_rec['bb_nfor_unt'] == 1) {
                                    $promo_retail = $item_rec['bb_nfor_price'];
                                } else {
                                    $promo_retail = $item_rec['bb_price'];
                                }
                            }
                        }
                    }
                    break;
            }

            $item_rec['promo_retail'] = $promo_retail;

            ######################
            # Display the record #
            ######################
            if ($item_rec['case_pack'] == '' || $item_rec['case_pack'] == 0) {
                $item_rec['case_pack'] = 1;
            }


            $titem_size = $item_rec['item_size'];
            /* IF titem_size[4,6]=".00" THEN
              LET titem_size=item_rec.item_size USING "<<#"
              ELSE
              IF titem_size[3,5]=".00" THEN
              LET titem_size=item_rec.item_size USING "<<#"
              ELSE
              IF titem_size[2,4]=".00" THEN
              LET titem_size=item_rec.item_size USING "<<#"
              END IF
              END IF
              END IF */

            

            if ($item_rec['item_pack'] != '' && $item_rec['item_pack'] > 1) {
                $pack_size = sprintf("%02d", $item_rec['case_pack']) . "-" . $item_rec['item_pack'] . "-" . round($titem_size,2) . " " . $item_rec['item_meas'];
            }
            else
            {
                $pack_size = sprintf("%02d", $item_rec['case_pack']) . "-" . round($titem_size) . " " . $item_rec['item_meas'];
            }

            $item_rec['pack_size'] = $pack_size;

            if ($item_rec['dept_override'] != "Y") {
                $item_rec['dept_override'] = " ";
            }

            if ($item_rec['tax_flag'] != "Y") {
                $item_rec['tax_flag'] = " ";
            }

            if ($item_rec['fs_flag'] != "Y") {
                $item_rec['fs_flag'] = " ";
            }

            if ($item_rec['scale_flag'] != "Y") {
                $item_rec['scale_flag'] = " ";
            }

            if ($item_rec['wic_flag'] != "Y") {
                $item_rec['wic_flag'] = " ";
            }



            /* IF second_upc = "N" THEN
              DISPLAY item_rec.gtin," ",
              pack_size," ",
              item_rec.desc_18," ",
              reg_retail," ",
              promo_retail," ",
              ttag_count  USING "##"," ",
              tsign_count USING "##"," ",
              item_rec.dept_override," ",
              item_rec.tax_flag,
              item_rec.fs_flag,
              item_rec.scale_flag,
              item_rec.wic_flag,
              #item_rec.scrip_flag
              item_rec.age_flag USING "##"
              AT counter,1
              ELSE
              DISPLAY item_rec.gtin,
              "                                                 ",
              ttag_count  USING "##"," ",
              tsign_count USING "##"
              AT counter,1
              END IF */

            $item_rec_full[] = $item_rec;
        }

        }
        //echo '<pre>';print_r($item_rec_full);exit;
        return $item_rec_full;
    }
    public function printBatchDetail()
    {   
        $storeno = Input::get('storeno');
        $batch_no = Input::get('batch');
        $user_id = Input::get('user_id');
        $batch_function = Input::get('batch_function');
        //echo $batch_function;exit;
        if($batch_function == "R")
        {
            $printed_sw = "Y";
        }
        else
        {
            $printed_sw = "N";
        }
        $ws_start_date =  date('Y-m-d', strtotime('-7 day'));
        $ws_end_date =  date('Y-m-d');
        $sign_cur_Array = DB::select("SELECT ssignhdr.userid,ssstock.sign_count,ssignhdr.stock_code, ssignhdr.sign_id, ssignhdr.batch_no,sssign.sign_name,SUM(copies) as copies FROM ssignhdr as ssignhdr left join sssign as sssign on sssign.sign_id=ssignhdr.sign_id left join ssstock as ssstock on ssstock.stock_code=ssignhdr.stock_code WHERE ssignhdr.userid = '".$user_id."' AND ssignhdr.passwd = '".$user_id."' AND ssignhdr.date_requested >= '".$ws_start_date."' AND ssignhdr.date_requested <= '".$ws_end_date."' AND ssignhdr.printed_sw = '".$printed_sw."' AND ssignhdr.batch_no = '".$batch_no."' GROUP BY ssignhdr.userid, ssignhdr.stock_code,ssignhdr.sign_id ORDER BY ssignhdr.sign_id");
        // echo "SELECT ssignhdr.userid,ssstock.sign_count,ssignhdr.stock_code, ssignhdr.sign_id, ssignhdr.batch_no,sssign.sign_name,SUM(copies) as copies FROM ssignhdr as ssignhdr left join sssign as sssign on sssign.sign_id=ssignhdr.sign_id left join ssstock as ssstock on ssstock.stock_code=ssignhdr.stock_code WHERE ssignhdr.userid = '".$user_id."' AND ssignhdr.passwd = '".$user_id."' AND ssignhdr.date_requested >= '".$ws_start_date."' AND ssignhdr.date_requested <= '".$ws_end_date."' AND ssignhdr.printed_sw = 'N' AND ssignhdr.batch_no = '".$batch_no."' GROUP BY ssignhdr.userid, ssignhdr.stock_code,ssignhdr.sign_id ORDER BY ssignhdr.sign_id";exit;
        // $sign_cur_Array = json_decode(json_encode($sign_cur_Array), true);
        // echo '<pre>';print_r($sign_cur_Array);exit;
        if($batch_function == "P")
        {
            return View::make('slcadm03.print_batches',compact('storeno','sign_cur_Array'),array('user_id'=> $user_id,'password'=> $user_id,'batch_no'=> $batch_no,'ws_start_date'=> $ws_start_date,'ws_end_date'=> $ws_end_date,'batch_function'=>$batch_function));
        }
        else
        {
            return View::make('slcadm03.reprint_batches',compact('storeno','sign_cur_Array'),array('user_id'=> $user_id,'password'=> $user_id,'batch_no'=> $batch_no,'ws_start_date'=> $ws_start_date,'ws_end_date'=> $ws_end_date,'batch_function'=>$batch_function));
        }
        
    }
     public function updateBatchDetails()
    {
        $batch_function = Input::get('batch_function');
        if($batch_function == "A")
        {
            $beg_batch_no = Input::get('beg_batch_no');
            $end_batch_no = Input::get('end_batch_no');
            $storeno = Input::get('storeno');
            //echo $storeno.' '.$batch_function.' '.$beg_batch_no.' '.$end_batch_no; exit;
            if($end_batch_no !="")
            {
                $slposhdr_query = DB::select('SELECT batch FROM slposhdr WHERE store_number="'.$storeno.'" AND batch>="'.$beg_batch_no.'" AND batch<="'.$end_batch_no.'" AND reg_applied is NULL ORDER BY batch');
            }
            else
            {
                $slposhdr_query = DB::select('SELECT batch FROM slposhdr WHERE store_number="'.$storeno.'" AND batch="'.$beg_batch_no.'" AND reg_applied is NULL ORDER BY batch');
            }
            $slposhdrArray = json_decode(json_encode($slposhdr_query), true);
            //echo '<pre>';print_r($slposhdrArray);exit;
            //echo $storeno;
            $today =  date('Y-m-d');
            for($i=0;$i<count($slposhdrArray);$i++)
            {
                //need to call slbatapply.php;
                $storeno = 305;
                $batch_no = $slposhdrArray[$i]['batch'];
                $slbatchapply = $this->slbatchapply($storeno,$batch_no);
                
                DB::update('UPDATE slposhdr SET reg_applied="'.$today.'" where store_number="'.$storeno.'" AND batch="'.$slposhdrArray[$i]['batch'].'"');
            }
            //$data="regbatch.".$storeno.".".$batch_no."";
            //$dir = $_SERVER['DOCUMENT_ROOT'].'/filestoupload/'.$data;
            //unlink($dir);
            echo 'Batch Applied Successfully';  
        }
        else
        {
            $beg_batch_no = Input::get('beg_batch_no'); 
            $storeno = Input::get('storeno');
            $storeno = 305;
            $slbatchapply = $this->slbatchapply_restore($storeno,$beg_batch_no);//exit;
            DB::update('UPDATE slposhdr SET reg_applied=NULL where store_number="'.$storeno.'" AND batch="'.$beg_batch_no.'"');

            echo 'Batch Restored Successfully';  
        }
        
    }
    public function slbatchapply($storeno,$batch_no)
    {
        //echo $batch_no;exit;
        //echo 'slbatchapply123';exit;
        $myfile = fopen("filestoupload/regbatch.".$storeno.".".$batch_no."", "w") or die("Unable to open file!");
        $eff_date_query = DB::select('SELECT eff_date as teffdate FROM slposhdr 
          WHERE store_number="'.$storeno.'" AND batch="'.$batch_no.'"');
        $teffdate = $eff_date_query[0]->teffdate;
        //echo $teffdate;exit;
        $regbatch_query =  DB::select('
            SELECT
            slposdtl.type_change,
            slposdtl.gtin,
            slitem.pos_dept,
            slitem.price, 
            slitem.nfor_unt, 
            slitem.ad_nfor_unt, 
            slitem.nfor_price, 
            slitem.ad_start,
            slitem.ad_end,
            slitem.ad_price, 
            slitem.ad_nfor_price, 
            slitem.ad_limit,
            slitem.bb_start,
            slitem.bb_end,
            slitem.bb_price, 
            slitem.bb_nfor_price,
            slitem.bb_nfor_unt,
            slitem.bb_limit,
            slitem.limit, 
             
            slitem.mix_match, 
            "0",    
            slitem.desc_18,
            slitem.link_code,
            slitem.eff_date,
            slitem.coupon, 
            "1", 
            slitem.dept_override,
            slitem.tax_flag,
            slitem.fs_flag,
            slitem.age_flag,
            "0",
            slitem.scale_flag,
            slitem.wic_flag,
            slitem.scrip_flag,
            slitem.qty_req,
            slitem.prhbt_qty,
            slitem.price_req,
            slitem.tare_code,
            slitem.emp_discount,
            slitem.liq_discount,
            slitem.ctrl_sub,
            slitem.recall_item,
            slitem.fsa_item,
            slitem.wic_produce,
            slitem.gift_card,
            slitem.tender_pts
            FROM slposdtl left outer join slitem  on slitem.gtin=slposdtl.gtin
            WHERE slposdtl.store_number= 305 
            AND slposdtl.batch="'.$batch_no.'" AND slitem.store_number=305
            AND slposdtl.pos_flag="Y"

            ');   
            $regbatchArray = json_decode(json_encode($regbatch_query), true);
            //return $regbatchArray;
            //echo '<pre>';print_r($regbatchArray);exit;
            
            for($i=0;$i<count($regbatchArray);$i++)
            {
                $type_change = $regbatchArray[$i]['type_change'];
                $gtin = $regbatchArray[$i]['gtin'];
                $pos_dept = $regbatchArray[$i]['pos_dept'];
                $eff_date = $regbatchArray[$i]['eff_date'];
                $save_curs_query = DB::select('SELECT slitemsv.price, slitemsv.nfor_unt, slitemsv.nfor_price,
                    ad_price, ad_nfor_unt,
                    ad_nfor_price, ad_limit, ad_start, ad_end, bb_price, 
                    bb_nfor_unt,`limit`, bb_nfor_price, bb_limit, bb_start, bb_end,   
                    mix_match, eff_date
               FROM slitemsv 
                WHERE store_number="'.$storeno.'" AND gtin="'.$gtin.'"');
                $save_cursArray = json_decode(json_encode($save_curs_query), true);
                if($eff_date > $teffdate)
                {
                    if(!empty($save_cursArray))
                    {
                        if($save_cursArray[0]['eff_date']<= $teffdate)
                        {
                            $mix_match = $save_cursArray[0]['mix_match'];
                            if(($save_cursArray[0]['ad_start'] != "") && ($save_cursArray[0]['ad_end'] != "") && ($save_cursArray[0]['ad_start']<=$teffdate && $save_cursArray[0]['ad_end']>=$teffdate) && (($save_cursArray[0]['ad_price'] !="") || ($save_cursArray[0]['ad_nfor_price'] !="")))
                            {
                                //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                                $price = $save_cursArray[$i]['ad_price']*100;
                                $nfor_unt = $save_cursArray[0]['ad_nfor_unt'];
                                $nfor_price = $save_cursArray[0]['ad_nfor_price']*100;
                                $limit = $save_cursArray[0]['ad_limit'];
                                if($save_cursArray[0]['price'] == "")
                                {
                                    $reg_price=($save_cursArray[0]['nfor_price']/$save_cursArray[0]['nfor_unt'])*100;
                                }
                                else
                                {
                                    $reg_price=$save_cursArray[0]['price']*100;
                                } 
                            }


                            if(($save_cursArray[0]['bb_start'] != "") && ($save_cursArray[0]['bb_end'] != "") && ($save_cursArray[0]['bb_start']<=$teffdate && $save_cursArray[0]['bb_end']>=$teffdate) && (($save_cursArray[0]['bb_price'] !="") || ($save_cursArray[0]['bb_nfor_price'] !="")))
                            {
                                //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                                $price = $save_cursArray[0]['bb_price']*100;
                                $nfor_unt = $save_cursArray[0]['bb_nfor_unt'];
                                $nfor_price = $save_cursArray[0]['bb_nfor_price']*100;
                                $limit = $save_cursArray[0]['bb_limit'];
                                if($save_cursArray[0]['price'] == "")
                                {
                                    $reg_price=($save_cursArray[0]['nfor_price']/$save_cursArray[0]['nfor_unt'])*100;
                                }
                                else
                                {
                                    $reg_price=$save_cursArray[0]['price']*100;
                                } 
                            }
                            else
                            {
                                $price = $save_cursArray[0]['price']*100;
                                $nfor_unt = $save_cursArray[0]['nfor_unt'];
                                $nfor_price = $save_cursArray[0]['nfor_price']*100;
                                $limit = $save_cursArray[0]['limit'];
                                $reg_price=0;
                            }
                        }
                    }
                    else
                    {
                        $mix_match = $regbatchArray[$i]['mix_match'];
                        if(($regbatchArray[$i]['ad_start'] != "") && ($regbatchArray[$i]['ad_end'] != "") && ($regbatchArray[$i]['ad_start']<=$teffdate && $regbatchArray[$i]['ad_end']>=$teffdate) && (($regbatchArray[$i]['ad_price'] !="") || ($regbatchArray[$i]['ad_nfor_price'] !="")))
                        {
                            //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                            $price = $regbatchArray[$i]['ad_price']*100;
                            $nfor_unt = $regbatchArray[$i]['ad_nfor_unt'];
                            $nfor_price = $regbatchArray[$i]['ad_nfor_price']*100;
                            $limit = $regbatchArray[$i]['ad_limit'];
                            if($regbatchArray[$i]['price'] == "")
                            {
                                $reg_price=($regbatchArray[$i]['nfor_price']/$regbatchArray[$i]['nfor_unt'])*100;
                            }
                            else
                            {
                                $reg_price=$regbatchArray[$i]['price']*100;
                            } 
                        }


                        if(($regbatchArray[$i]['bb_start'] != "") && ($regbatchArray[$i]['bb_end'] != "") && ($regbatchArray[$i]['bb_start']<=$teffdate && $regbatchArray[$i]['bb_end']>=$teffdate) && (($regbatchArray[$i]['bb_price'] !="") || ($regbatchArray[$i]['bb_nfor_price'] !="")))
                        {
                            //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                            $price = $regbatchArray[$i]['bb_price']*100;
                            $nfor_unt = $regbatchArray[$i]['bb_nfor_unt'];
                            $nfor_price = $regbatchArray[$i]['bb_nfor_price']*100;
                            $limit = $regbatchArray[$i]['bb_limit'];
                            if($regbatchArray[$i]['price'] == "")
                            {
                                $reg_price=($regbatchArray[$i]['nfor_price']/$regbatchArray[$i]['nfor_unt'])*100;
                            }
                            else
                            {
                                $reg_price=$regbatchArray[$i]['price']*100;
                            } 
                        }
                        else
                        {
                            $price = $regbatchArray[$i]['price']*100;
                            $nfor_unt = $regbatchArray[$i]['nfor_unt'];
                            $nfor_price = $regbatchArray[$i]['nfor_price']*100;
                            $limit = $regbatchArray[$i]['limit'];
                            $reg_price=0;
                        }
                    }
                    
                }
                else
                {
                    $mix_match = $regbatchArray[$i]['mix_match'];
                    if(($regbatchArray[$i]['ad_start'] != "") && ($regbatchArray[$i]['ad_end'] != "") && ($regbatchArray[$i]['ad_start']<=$teffdate && $regbatchArray[$i]['ad_end']>=$teffdate) && (($regbatchArray[$i]['ad_price'] !="") || ($regbatchArray[$i]['ad_nfor_price'] !="")))
                    {
                        //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                        $price = $regbatchArray[$i]['ad_price']*100;
                        $nfor_unt = $regbatchArray[$i]['ad_nfor_unt'];
                        $nfor_price = $regbatchArray[$i]['ad_nfor_price']*100;
                        $limit = $regbatchArray[$i]['ad_limit'];
                        if($regbatchArray[$i]['price'] == "")
                        {
                            $reg_price=($regbatchArray[$i]['nfor_price']/$regbatchArray[$i]['nfor_unt'])*100;
                        }
                        else
                        {
                            $reg_price=$regbatchArray[$i]['price']*100;
                        } 
                    }


                    if(($regbatchArray[$i]['bb_start'] != "") && ($regbatchArray[$i]['bb_end'] != "") && ($regbatchArray[$i]['bb_start']<=$teffdate && $regbatchArray[$i]['bb_end']>=$teffdate) && (($regbatchArray[$i]['bb_price'] !="") || ($regbatchArray[$i]['bb_nfor_price'] !="")))
                    {
                        //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                        $price = $regbatchArray[$i]['bb_price']*100;
                        $nfor_unt = $regbatchArray[$i]['bb_nfor_unt'];
                        $nfor_price = $regbatchArray[$i]['bb_nfor_price']*100;
                        $limit = $regbatchArray[$i]['bb_limit'];
                        if($regbatchArray[$i]['price'] == "")
                        {
                            $reg_price=($regbatchArray[$i]['nfor_price']/$regbatchArray[$i]['nfor_unt'])*100;
                        }
                        else
                        {
                            $reg_price=$regbatchArray[$i]['price']*100;
                        } 
                    }
                    else
                    {
                        $price = $regbatchArray[$i]['price']*100;
                        $nfor_unt = $regbatchArray[$i]['nfor_unt'];
                        $nfor_price = $regbatchArray[$i]['nfor_price']*100;
                        $limit = $regbatchArray[$i]['limit'];
                        $reg_price=0;
                    }
                }


                
                //$mix_match = $regbatchArray[$i]['mix_match'];
                $desc_18 = $regbatchArray[$i]['desc_18'];
                $link_code = $regbatchArray[$i]['link_code'];
                $link_code = substr($link_code, -4);
                $coupon = $regbatchArray[$i]['coupon'];
                if($coupon == "Y")
                {
                    $pricetype = 7;
                }
                else
                {
                    $pricetype = 0;
                }
                $pricemethod = 1;
                
                
                $dept_override = $regbatchArray[$i]['dept_override'];
                $tax_flag = $regbatchArray[$i]['tax_flag'];
                $fs_flag = $regbatchArray[$i]['fs_flag'];
                $age_flag = $regbatchArray[$i]['age_flag'];

                $scale_flag = $regbatchArray[$i]['scale_flag'];
                $wic_flag = $regbatchArray[$i]['wic_flag'];
                $scrip_flag = $regbatchArray[$i]['scrip_flag'];
                $qty_req = $regbatchArray[$i]['qty_req'];
                $prhbt_qty = $regbatchArray[$i]['prhbt_qty'];
                $price_req = $regbatchArray[$i]['price_req'];
                $tare_code = $regbatchArray[$i]['tare_code'];
                $emp_discount = $regbatchArray[$i]['emp_discount'];
                $liq_discount = $regbatchArray[$i]['liq_discount'];
                $ctrl_sub = $regbatchArray[$i]['ctrl_sub'];
                $recalled_item = $regbatchArray[$i]['recall_item'];
                if($recalled_item == "Y")
                {
                    $recalled_item = 1;
                }
                else
                {
                    $recalled_item = 0;
                }
                $fsa_flag = $regbatchArray[$i]['fsa_item'];
                $wicp_flag = $regbatchArray[$i]['wic_produce'];
                $gift_flag = $regbatchArray[$i]['gift_card'];
                $tender_pts = $regbatchArray[$i]['tender_pts'];
                if(empty($limit))
                {
                    $limit = 0;
                }
                $txt = $type_change.str_pad($gtin,14," ").str_pad($pos_dept,3," ",STR_PAD_LEFT).str_pad($price,5," ",STR_PAD_LEFT).str_pad($nfor_unt,2," ",STR_PAD_LEFT).str_pad($nfor_price,5," ",STR_PAD_LEFT).str_pad($limit,2," ",STR_PAD_LEFT).str_pad($mix_match,3," ",STR_PAD_LEFT).str_pad("0",2," ",STR_PAD_LEFT).str_pad($desc_18,18," ",STR_PAD_RIGHT).str_pad($link_code,4," ",STR_PAD_LEFT).str_pad($pricetype,1," ",STR_PAD_LEFT).str_pad($pricemethod,1," ",STR_PAD_LEFT).str_pad($reg_price,5," ",STR_PAD_LEFT).str_pad($dept_override,1," ",STR_PAD_LEFT).str_pad($tax_flag,1," ",STR_PAD_LEFT).str_pad($fs_flag,1," ",STR_PAD_LEFT).str_pad($age_flag,2," ",STR_PAD_LEFT).str_pad("0",1," ",STR_PAD_LEFT).str_pad($scale_flag,1," ",STR_PAD_LEFT).str_pad($wic_flag,1," ",STR_PAD_LEFT).str_pad($scrip_flag,1," ",STR_PAD_LEFT).str_pad($qty_req,1," ",STR_PAD_LEFT).str_pad($prhbt_qty,1," ",STR_PAD_LEFT).str_pad($price_req,1," ",STR_PAD_LEFT).str_pad($tare_code,2," ",STR_PAD_LEFT).str_pad($emp_discount,1," ",STR_PAD_LEFT).str_pad($liq_discount,1," ",STR_PAD_LEFT).str_pad($ctrl_sub,2," ",STR_PAD_LEFT).str_pad($recalled_item,1," ",STR_PAD_LEFT).str_pad($fsa_flag,1," ",STR_PAD_LEFT).str_pad($wicp_flag,1," ",STR_PAD_LEFT).str_pad($gift_flag,1," ",STR_PAD_LEFT).str_pad($tender_pts,1," ",STR_PAD_LEFT).
                "\r\n";
                //echo $txt;exit;
                fwrite($myfile, $txt);
            }
            fclose($myfile);
        return $myfile;
    }
    public function slbatchapply_restore($storeno,$batch_no)
    {
        $myfile = fopen("filestoupload/regbatch.".$storeno.".".$batch_no."", "w") or die("Unable to open file!");
        $eff_date_query = DB::select('SELECT eff_date as teffdate FROM slposhdr 
          WHERE store_number="'.$storeno.'" AND batch="'.$batch_no.'" AND
                                reg_applied is NOT NULL ');
        //$teffdate = $eff_date_query[0]->teffdate;
        $teffdate = date('Y-m-d');
        $regbatch_query = DB::select('
            SELECT
            slposdtl.type_change,
            slposdtl.gtin,
            slitemsv.pos_dept,
            slitemsv.price, 
            slitemsv.nfor_unt, 
            slitemsv.ad_nfor_unt, 
            slitemsv.nfor_price, 
            slitemsv.ad_start,
            slitemsv.ad_end,
            slitemsv.ad_price, 
            slitemsv.ad_nfor_price, 
            slitemsv.ad_limit,
            slitemsv.bb_start,
            slitemsv.bb_end,
            slitemsv.bb_price, 
            slitemsv.bb_nfor_price,
            slitemsv.bb_nfor_unt,
            slitemsv.bb_limit,
            slitemsv.limit, 
             
            slitemsv.mix_match, 
            "0",    
            slitemsv.desc_18,
            slitemsv.link_code,
            slitemsv.eff_date,
            slitemsv.coupon, 
            "1", 
            slitemsv.dept_override,
            slitemsv.tax_flag,
            slitemsv.fs_flag,
            slitemsv.age_flag,
            "0",
            slitemsv.scale_flag,
            slitemsv.wic_flag,
            slitemsv.scrip_flag,
            slitemsv.qty_req,
            slitemsv.prhbt_qty,
            slitemsv.price_req,
            slitemsv.tare_code,
            slitemsv.emp_discount,
            slitemsv.liq_discount,
            slitemsv.ctrl_sub,
            slitemsv.recall_item,
            slitemsv.fsa_item,
            slitemsv.wic_produce,
            slitemsv.gift_card,
            slitemsv.tender_pts
            FROM slposdtl left  join slitemsv  on slitemsv.gtin=slposdtl.gtin
            WHERE slposdtl.store_number= 305 
            AND slposdtl.batch="'.$batch_no.'" AND slitemsv.store_number=305
            AND slposdtl.pos_flag="Y"

            ');   
            $regbatchArray = json_decode(json_encode($regbatch_query), true);
            //return $regbatchArray;
            //echo '<pre>';print_r($regbatchArray);exit;
            $slitem_query =  DB::select('
            SELECT
            slposdtl.type_change,
            slposdtl.gtin,
            slitem.pos_dept,
            slitem.price, 
            slitem.nfor_unt, 
            slitem.ad_nfor_unt, 
            slitem.nfor_price, 
            slitem.ad_start,
            slitem.ad_end,
            slitem.ad_price, 
            slitem.ad_nfor_price, 
            slitem.ad_limit,
            slitem.bb_start,
            slitem.bb_end,
            slitem.bb_price, 
            slitem.bb_nfor_price,
            slitem.bb_nfor_unt,
            slitem.bb_limit,
            slitem.limit, 
             
            slitem.mix_match, 
            "0",    
            slitem.desc_18,
            slitem.link_code,
            slitem.eff_date,
            slitem.coupon, 
            "1", 
            slitem.dept_override,
            slitem.tax_flag,
            slitem.fs_flag,
            slitem.age_flag,
            "0",
            slitem.scale_flag,
            slitem.wic_flag,
            slitem.scrip_flag,
            slitem.qty_req,
            slitem.prhbt_qty,
            slitem.price_req,
            slitem.tare_code,
            slitem.emp_discount,
            slitem.liq_discount,
            slitem.ctrl_sub,
            slitem.recall_item,
            slitem.fsa_item,
            slitem.wic_produce,
            slitem.gift_card,
            slitem.tender_pts
            FROM slposdtl left outer join slitem  on slitem.gtin=slposdtl.gtin
            WHERE slposdtl.store_number= 305 
            AND slposdtl.batch="'.$batch_no.'" AND slitem.store_number=305
            AND slposdtl.pos_flag="Y"

            ');   
            $slitemArray = json_decode(json_encode($slitem_query), true);


            for($i=0;$i<count($regbatchArray);$i++)
            {
                $type_change = $regbatchArray[$i]['type_change'];
                $gtin = $regbatchArray[$i]['gtin'];
                $pos_dept = $regbatchArray[$i]['pos_dept'];
                $eff_date = $regbatchArray[$i]['eff_date'];
                //$slitemsv_eff_date = $slitemArray[$i]['eff_date'];
                $save_curs_query = DB::select('SELECT slitemsv.price, slitemsv.nfor_unt, slitemsv.nfor_price,
                    ad_price, ad_nfor_unt,
                    ad_nfor_price, ad_limit, ad_start, ad_end, bb_price, 
                    bb_nfor_unt,`limit`, bb_nfor_price, bb_limit, bb_start, bb_end,   
                    mix_match, eff_date
               FROM slitemsv 
                WHERE store_number="'.$storeno.'" AND gtin="'.$gtin.'"');
                $save_cursArray = json_decode(json_encode($save_curs_query), true);
                //echo '<pre>';print_r($save_cursArray);exit;
                if($eff_date > $teffdate)
                {
                    if($save_cursArray[0]['eff_date']<= $teffdate)
                    {
                        $mix_match = $save_cursArray[0]['mix_match'];
                        if(($save_cursArray[0]['ad_start'] != "") && ($save_cursArray[0]['ad_end'] != "") && ($save_cursArray[0]['ad_start']<=$teffdate && $save_cursArray[0]['ad_end']>=$teffdate) && (($save_cursArray[0]['ad_price'] !="") || ($save_cursArray[0]['ad_nfor_price'] !="")))
                        {
                            //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                            $price = $save_cursArray[$i]['ad_price']*100;
                            $nfor_unt = $save_cursArray[0]['ad_nfor_unt'];
                            $nfor_price = $save_cursArray[0]['ad_nfor_price']*100;
                            $limit = $save_cursArray[0]['ad_limit'];
                            if($save_cursArray[0]['price'] == "")
                            {
                                $reg_price=($save_cursArray[0]['nfor_price']/$save_cursArray[0]['nfor_unt'])*100;
                            }
                            else
                            {
                                $reg_price=$save_cursArray[0]['price']*100;
                            } 
                        }


                        if(($save_cursArray[0]['bb_start'] != "") && ($save_cursArray[0]['bb_end'] != "") && ($save_cursArray[0]['bb_start']<=$teffdate && $save_cursArray[0]['bb_end']>=$teffdate) && (($save_cursArray[0]['bb_price'] !="") || ($save_cursArray[0]['bb_nfor_price'] !="")))
                        {
                            //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                            $price = $save_cursArray[0]['bb_price']*100;
                            $nfor_unt = $save_cursArray[0]['bb_nfor_unt'];
                            $nfor_price = $save_cursArray[0]['bb_nfor_price']*100;
                            $limit = $save_cursArray[0]['bb_limit'];
                            if($save_cursArray[0]['price'] == "")
                            {
                                $reg_price=($save_cursArray[0]['nfor_price']/$save_cursArray[0]['nfor_unt'])*100;
                            }
                            else
                            {
                                $reg_price=$save_cursArray[0]['price']*100;
                            } 
                        }
                        else
                        {
                            $price = $save_cursArray[0]['price']*100;
                            $nfor_unt = $save_cursArray[0]['nfor_unt'];
                            $nfor_price = $save_cursArray[0]['nfor_price']*100;
                            $limit = $save_cursArray[0]['limit'];
                            $reg_price=0;
                        }
                    }
                }
                
                else
                {

                    $mix_match = $regbatchArray[$i]['mix_match'];
                    if(($regbatchArray[$i]['ad_start'] != "") && ($regbatchArray[$i]['ad_end'] != "") && ($regbatchArray[$i]['ad_start']<=$teffdate && $regbatchArray[$i]['ad_end']>=$teffdate) && (($regbatchArray[$i]['ad_price'] !="") || ($regbatchArray[$i]['ad_nfor_price'] !="")))
                    {
                        //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                        $price = $regbatchArray[$i]['ad_price']*100;
                        $nfor_unt = $regbatchArray[$i]['ad_nfor_unt'];
                        $nfor_price = $regbatchArray[$i]['ad_nfor_price']*100;
                        $limit = $regbatchArray[$i]['ad_limit'];
                        if($regbatchArray[$i]['price'] == "")
                        {
                            $reg_price=($regbatchArray[$i]['nfor_price']/$regbatchArray[$i]['nfor_unt'])*100;
                        }
                        else
                        {
                            $reg_price=$regbatchArray[$i]['price']*100;
                        } 
                    }


                    if(($regbatchArray[$i]['bb_start'] != "") && ($regbatchArray[$i]['bb_end'] != "") && ($regbatchArray[$i]['bb_start']<=$teffdate && $regbatchArray[$i]['bb_end']>=$teffdate) && (($regbatchArray[$i]['bb_price'] !="") || ($regbatchArray[$i]['bb_nfor_price'] !="")))
                    {
                        //echo 'bb_price '.$regbatchArray[$i]['bb_price']*100;
                        $price = $regbatchArray[$i]['bb_price']*100;
                        $nfor_unt = $regbatchArray[$i]['bb_nfor_unt'];
                        $nfor_price = $regbatchArray[$i]['bb_nfor_price']*100;
                        $limit = $regbatchArray[$i]['bb_limit'];
                        if($regbatchArray[$i]['price'] == "")
                        {
                            $reg_price=($regbatchArray[$i]['nfor_price']/$regbatchArray[$i]['nfor_unt'])*100;
                        }
                        else
                        {
                            $reg_price=$regbatchArray[$i]['price']*100;
                        } 
                    }
                    else
                    {
                        $price = $regbatchArray[$i]['price']*100;
                        $nfor_unt = $regbatchArray[$i]['nfor_unt'];
                        $nfor_price = $regbatchArray[$i]['nfor_price']*100;
                        $limit = $regbatchArray[$i]['limit'];
                        $reg_price=0;
                    }
                }


                
                //$mix_match = $regbatchArray[$i]['mix_match'];
                $desc_18 = $regbatchArray[$i]['desc_18'];
                $link_code = $regbatchArray[$i]['link_code'];
                $link_code = substr($link_code, -4);
                $coupon = $regbatchArray[$i]['coupon'];
                if($coupon == "Y")
                {
                    $pricetype = 7;
                }
                else
                {
                    $pricetype = 0;
                }
                $pricemethod = 1;
                
                
                $dept_override = $regbatchArray[$i]['dept_override'];
                $tax_flag = $regbatchArray[$i]['tax_flag'];
                $fs_flag = $regbatchArray[$i]['fs_flag'];
                $age_flag = $regbatchArray[$i]['age_flag'];

                $scale_flag = $regbatchArray[$i]['scale_flag'];
                $wic_flag = $regbatchArray[$i]['wic_flag'];
                $scrip_flag = $regbatchArray[$i]['scrip_flag'];
                $qty_req = $regbatchArray[$i]['qty_req'];
                $prhbt_qty = $regbatchArray[$i]['prhbt_qty'];
                $price_req = $regbatchArray[$i]['price_req'];
                $tare_code = $regbatchArray[$i]['tare_code'];
                $emp_discount = $regbatchArray[$i]['emp_discount'];
                $liq_discount = $regbatchArray[$i]['liq_discount'];
                $ctrl_sub = $regbatchArray[$i]['ctrl_sub'];
                $recalled_item = $regbatchArray[$i]['recall_item'];
                if($recalled_item == "Y")
                {
                    $recalled_item = 1;
                }
                else
                {
                    $recalled_item = 0;
                }
                $fsa_flag = $regbatchArray[$i]['fsa_item'];
                $wicp_flag = $regbatchArray[$i]['wic_produce'];
                $gift_flag = $regbatchArray[$i]['gift_card'];
                $tender_pts = $regbatchArray[$i]['tender_pts'];
                if(empty($limit))
                {
                    $limit = 0;
                }
                $txt = $type_change.str_pad($gtin,14," ").str_pad($pos_dept,3," ",STR_PAD_LEFT).str_pad($price,5," ",STR_PAD_LEFT).str_pad($nfor_unt,2," ",STR_PAD_LEFT).str_pad($nfor_price,5," ",STR_PAD_LEFT).str_pad($limit,2," ",STR_PAD_LEFT).str_pad($mix_match,3," ",STR_PAD_LEFT).str_pad("0",2," ",STR_PAD_LEFT).str_pad($desc_18,18," ",STR_PAD_RIGHT).str_pad($link_code,4," ",STR_PAD_LEFT).str_pad($pricetype,1," ",STR_PAD_LEFT).str_pad($pricemethod,1," ",STR_PAD_LEFT).str_pad($reg_price,5," ",STR_PAD_LEFT).str_pad($dept_override,1," ",STR_PAD_LEFT).str_pad($tax_flag,1," ",STR_PAD_LEFT).str_pad($fs_flag,1," ",STR_PAD_LEFT).str_pad($age_flag,2," ",STR_PAD_LEFT).str_pad("0",1," ",STR_PAD_LEFT).str_pad($scale_flag,1," ",STR_PAD_LEFT).str_pad($wic_flag,1," ",STR_PAD_LEFT).str_pad($scrip_flag,1," ",STR_PAD_LEFT).str_pad($qty_req,1," ",STR_PAD_LEFT).str_pad($prhbt_qty,1," ",STR_PAD_LEFT).str_pad($price_req,1," ",STR_PAD_LEFT).str_pad($tare_code,2," ",STR_PAD_LEFT).str_pad($emp_discount,1," ",STR_PAD_LEFT).str_pad($liq_discount,1," ",STR_PAD_LEFT).str_pad($ctrl_sub,2," ",STR_PAD_LEFT).str_pad($recalled_item,1," ",STR_PAD_LEFT).str_pad($fsa_flag,1," ",STR_PAD_LEFT).str_pad($wicp_flag,1," ",STR_PAD_LEFT).str_pad($gift_flag,1," ",STR_PAD_LEFT).str_pad($tender_pts,1," ",STR_PAD_LEFT).
                "\r\n";
                //echo $txt;exit;
                fwrite($myfile, $txt);
            }
            fclose($myfile);

    }
    public function generateFile()
    {
        return View::make('slcadm03.emailhelpdesk');
    }
    public function getEmailhelpdesk() {
        return View::make('slcadm03.emailhelpdesk');
    }

    public function getDisplayBatch() {
        $data = array('tabcol1' => 'Batch',
            'tabcol2' => 'DISPLAY BATCHES Batch Description',
            'tabcol3' => 'Batch Load',
            'tabcol4' => 'Eff Date',
            'tabcol5' => 'Item Count',
            'tabcol6' => 'Tags Print',
            'tabcol7' => 'Regst Apply',
            'tabcol8' => 'Rpt Cd',
            'page_heading' => 'Display Batches');
        return View::make('slcadm03.displaybatch')->with('data', $data);
    }

    public function getBatchTags() {
        $data = array('tabcol1' => 'Batch',
            'tabcol2' => 'PRINT TAGS/SIGNS Description',
            'tabcol3' => 'Batch Load',
            'tabcol4' => 'Eff Date',
            'tabcol5' => 'Item Count',
            'tabcol6' => 'Tags Print',
            'tabcol7' => 'Regst Apply',
            'tabcol8' => 'Rpt Cd',
            'page_heading' => 'Print Bathc Tags/Signs');
        return View::make('slcadm03.printbatchtags')->with('data', $data);
    }

    public function getBatchPos() {
        $data = array('tabcol1' => 'Batch',
            'tabcol2' => 'APPLY BATCHES Description',
            'tabcol3' => 'Batch Load',
            'tabcol4' => 'Eff Date',
            'tabcol5' => 'Item Count',
            'tabcol6' => 'Tags Print',
            'tabcol7' => 'Regst Apply',
            'tabcol8' => 'Rpt Cd',
            'page_heading' => 'Apply Batch to POS File');
        return View::make('slcadm03.applybatchpos')->with('data', $data);
    }

    public function getReprintTags() {
        $data = array('tabcol1' => 'Batch',
            'tabcol2' => 'RE-PRINT TAGS/SIGNS Description',
            'tabcol3' => 'Batch Load',
            'tabcol4' => 'Eff Date',
            'tabcol5' => 'Item Count',
            'tabcol6' => 'Tags Print',
            'tabcol7' => 'Regst Apply',
            'tabcol8' => 'Rpt Cd',
            'page_heading' => 'Reprint Tags/Signs');
        return View::make('slcadm03.reprinttags')->with('data', $data);
    }

    public function getSummaryPage() {
        $data = array('tabcol1' => 'Batch',
            'tabcol2' => 'PRINT SUMMARY PAGES Description',
            'tabcol3' => 'Batch Load',
            'tabcol4' => 'Eff Date',
            'tabcol5' => 'Item Count',
            'tabcol6' => 'Tags Print',
            'tabcol7' => 'Regst Apply',
            'tabcol8' => 'Rpt Cd',
            'page_heading' => 'Summary Pages');
        return View::make('slcadm03.summarypages')->with('data', $data);
    }

    public function getRestorePage() {
        $data = array('tabcol1' => 'Batch',
            'tabcol2' => 'RESTORE BATCHES Description',
            'tabcol3' => 'Batch Load',
            'tabcol4' => 'Eff Date',
            'tabcol5' => 'Item Count',
            'tabcol6' => 'Tags Print',
            'tabcol7' => 'Regst Apply',
            'tabcol8' => 'Rpt Cd',
            'page_heading' => 'Report Batch');
        return View::make('slcadm03.restorebatches')->with('data', $data);
    }

    /*public function getDisplayReportCode() {
        $data = array('tabcol1' => 'Type ',
            'tabcol2' => 'End Date',
            'tabcol3' => 'Code',
            'page_heading' => 'Display Report Codes');
        return View::make('slcadm03.displayreportcodes');->with('data', $data)
    }*/
    public function getDisplayReportCode()
    {
        $storeno = Input::get('storeno');
        
        $slrep_cd_query=DB::select('SELECT type,tpr_end,report_code 
        FROM slrep_cd where store_number = "'.$storeno.'" order by tpr_end,type ');
        $slrep_cdArray = json_decode(json_encode($slrep_cd_query), true);
        return View::make('slcadm03.displayreportcodes',compact('slrep_cdArray'));
    }
    public function getslItem()
    {
        return View::make('slcadm03.slitem');
    }
    public function getItemLookup() {
        $data = array('tabcol1' => 'Batch',
            'tabcol2' => 'DISPLAY BATCHES Batch Description',
            'tabcol3' => 'Batch Load',
            'tabcol4' => 'Eff Date',
            'tabcol5' => 'Item Count',
            'tabcol6' => 'Tags Print',
            'tabcol7' => 'Regst Apply',
            'tabcol8' => 'Rpt Cd',
            'page_heading' => 'Item Lookup');
        return View::make('slcadm03.displaybatch')->with('data', $data);
    }

    public function batchSignTagCounts() {
        return View::make('slcadm03.displaybatchsigntagcounts');
    }

    public function viewBatchDetails() {
        return View::make('slcadm03.viewbatchdetails');
    }
    public function firstSku()
    {
        $skuno = Input::get('skuno');
        $batch_no = Input::get('batch_no');
        $sign_id = Input::get('sign_id');
        //echo $skuno.' '.$batch_no.' '.$sign_id;exit;
        if($skuno == 0 || $skuno == '' || $skuno == NULL)
        {
                $ws_first_seq = 0;
        }
        else
        {
            $ws_first_seq_query = DB::select('SELECT MIN(hdr_seq_no)
                           as ws_first_seq
                           FROM ssigndtl
                          WHERE print_text="'.$skuno.'" AND hdr_seq_no IN
                             (SELECT seq_no FROM ssignhdr 
                              WHERE batch_no="'.$batch_no.'" AND sign_id="'.$sign_id.'")');
            $ws_first_seq = $ws_first_seq_query[0]->ws_first_seq;
            if($ws_first_seq == '' || $ws_first_seq == NULL)
            {
                $ws_first_seq = 'notfound';
            }
        }
        return $ws_first_seq;
        //exit;
    }
    public function slcadm03_prnt_sh_file(){
        //echo 'sku_number '.Input::get('sku_number');exit;
        $user_id = Input::get('user_id');
        $password = Input::get('password');
        $batch_no = Input::get('batch_no');
        $sign_id = Input::get('sign_id');
        $ws_start_date =  date('Y-m-d', strtotime('-7 day'));
        $ws_end_date = date("Y-m-d");
        $batch_function = Input::get('batch_function');
        if($batch_function =="R")
        {
            $prnt_yet = 'Y';
            $start_seq_no = Input::get('sku_number');
        }
        else
        {
            $prnt_yet = 'N';
            $start_seq_no = 0;
        }
        //echo $user_id.' '.$sign_id.' '.$prnt_yet.' '.$batch_no.' '.$ws_start_date.' '.$ws_end_date.' '.$start_seq_no;exit;
        $command_result = shell_exec('/home/slic/bin/ssignext.sh ' .$user_id.' '.$sign_id.' '.$prnt_yet.' '.$batch_no.' '.$ws_start_date.' '.$ws_end_date.' '.$start_seq_no);
        return 'printed';
    }

    public function printUpdateTagprint()
    {
        $tstore = Input::get('storeno');
        $tbatch_no = Input::get('batch_no');
        $t_userid = Input::get('user_id');
        //echo $t_userid.' '.$tstore.' '.$tbatch_no;exit;
        $query_result = DB::select('SELECT tags_printed as tdate FROM slposhdr 
          WHERE store_number="'.$tstore.'" AND batch="'.$tbatch_no.'" ');
        //echo $query_result[0]->tdate;

       $pn_count_query =  DB::select('SELECT count(*) 
             as pn_count
             FROM ssignhdr
            WHERE printed_sw="N" AND userid="'.$t_userid.'" AND batch_no="'.$tbatch_no.'" ');
       //echo $pn_count_query[0]->pn_count;

       $py_count_query =  DB::select('SELECT count(*) 
             as py_count
             FROM ssignhdr
            WHERE printed_sw="Y" AND userid="'.$t_userid.'" AND batch_no="'.$tbatch_no.'" ');
       $pn_count = $pn_count_query[0]->pn_count;
       $py_count = $py_count_query[0]->py_count;
       switch (true) {
           case ($pn_count == 0 && $py_count == 0):
               DB::update('UPDATE slposhdr SET tags_printed="1899-12-31" 
          WHERE store_number="'.$tstore.'" AND batch="'.$tbatch_no.'"');
               break;
           case ($pn_count == 0 && $py_count > 0):
           $today = date('Y-m-d');
               DB::update('UPDATE slposhdr SET tags_printed="'.$today.'" 
          WHERE store_number="'.$tstore.'" AND batch="'.$tbatch_no.'"');
               break;
           case ($pn_count > 0 && $py_count == 0):
               DB::update('UPDATE slposhdr SET tags_printed=NULL 
          WHERE store_number="'.$tstore.'" AND batch="'.$tbatch_no.'"'); 
               break;
           default:
               DB::update('UPDATE slposhdr SET tags_printed="1900-01-01" 
          WHERE store_number="'.$tstore.'" AND batch="'.$tbatch_no.'"');
               break;
       }
       return 'updated';
    }

}