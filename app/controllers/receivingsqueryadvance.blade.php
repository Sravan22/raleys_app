@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>
<style type="text/css">
   .select2-container
   {
   width: 76%;
   float: left;
   }
</style>
<form action="{{URL::route('mktmgr-post-queryresultadvance')}}" class="form-horizontal" method="post" role="form" style="display: block;">
   <div class="container" style="">
      <div class="flash-message">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Advance Query</div>
            </div>
            <div style="padding-top:10px" class="panel-body" >
               <div class="form-group">
                  <label for="storeid" class="control-label col-sm-4">Store</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="storeid" name="storeid" placeholder="Store ID" value="{{{ Session::get('storeid') }}}" readonly />
                  </div>
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Department</label>
                  <div class="col-sm-8">
                     <select class="form-control select2" id="dept_name_number" name="dept_name_number" value="dept_name_number">
                        <option value="">Select</option>
                        @foreach ($dept_list as $key => $value) 
                        <option value="{{ $value->retail_dept }}">{{ $value->retail_dept }} : {{ $value->name }}</option>
                        @endforeach
                     </select>
                     <!--  <input type="hidden" class="form-control" id="departmentnum" placeholder="departmentnum" /> -->
                  </div>
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Invoice Number</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" name="invoice_number"{{ (Input::old('invoice_number'))?' 
                     value="'.Input::old('invoice_number').'"':''}} 
                     id="invoice_number" placeholder="Invoice Number" />
                     @if($errors->has('invoice_number'))
                     {{ $errors->first('invoice_number')}}
                     @endif 
                  </div>
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Invoice Start Date</label>
                  <div class="col-sm-8">
                     <input type="date" class="form-control" id="invoice_date" name="invoice_date"{{ (Input::old('invoice_date'))?'
                     value="'.Input::old('invoice_date').'"':''}} placeholder="MM-DD-YY" />
                     @if($errors->has('invoice_date'))
                     {{ $errors->first('invoice_date')}}
                     @endif       
                  </div>
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Invoice End Date</label>
                  <div class="col-sm-8">
                     <input type="date" class="form-control" id="invoice_end_date" 
                     name="invoice_end_date"{{ (Input::old('invoice_end_date'))?'
                     value="'.Input::old('invoice_end_date').'"':''}} placeholder="MM-DD-YY" />
                     @if($errors->has('invoice_end_date'))
                     {{ $errors->first('invoice_end_date')}}
                     @endif       
                  </div>
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Invoice Type</label>
                  <div class="col-sm-8">
                     <!-- <input type="text" class="form-control" id="invoice_type" name="invoice_type" placeholder="Invoice Type" /> -->
                     <select class="form-control" id="invoice_type" name="invoice_type">
                        <option value="">Select One Invoice Type</option>
                        <option value="D">D - Delivery</option>
                        <option value="R">R - Return</option>
                        <option value="S">S - Store Supplies</option>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Status</label>
                  <div class="col-sm-8">
                     <!--  <input type="text" class="form-control" id="status" name="status" /> -->
                     <select class="form-control" id="status" name="status" placeholder="Status" >
                        <option value="">Select One Status</option>
                        <option value="A">A - Accepted</option>
                        {{--                     
                        <option value="B">B</option>
                        --}}                    
                        <option value="I">I - Incompleted</option>
                        <option value="O">O - Open</option>
                        
                    <!-- <option value="P">P</option> -->
                   
                        <option value="R">R - Reviewed</option>
                        <option value="U">U - Unknown</option>
                        <option value="V">V - Voided</option>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Vendor</label>
                  <div class="col-sm-8">
                     <select class="form-control select2" id="vendorname" name="vendorname" value="vendorname">
                        <option value="">Select One Vendor</option>
                     </select>
                     @if($errors->has('vendor'))
                     {{ $errors->first('vendor')}}
                     @endif                 
                  </div>
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Number</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="vendornumber" name="vendornumber"{{ (Input::old('vendornumber'))?'
                     value="'.Input::old('vendornumber').'"':''}}  placeholder="Number" readonly />
                     @if($errors->has('vendornumber'))
                     {{ $errors->first('vendornumber')}}
                     @endif 
                  </div>
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Transporter</label>
                  <div class="col-sm-8">
                     <select class="form-control select2" id="transporter" name="transporter">
                        <option value="">Select</option>
                        @foreach ($transporter_list as $key => $valuedb) 
                        <option value="{{ $valuedb->trns_number }}">{{ $valuedb->name }}</option>
                        @endforeach
                     </select>
                     <!--  <input type="hidden" class="form-control" id="departmentnum" placeholder="departmentnum" /> -->
                  </div>
                  <!-- <div class="col-sm-8">
                     <input type="text" class="form-control" id="transporter" name="transporter"{{ (Input::old('transporter'))?'
                            value="'.Input::old('transporter').'"':''}}  placeholder="Transporter" />
                     @if($errors->has('transporter'))
                                   {{ $errors->first('transporter')}}
                                   @endif 
                     </div> -->
               </div>
               <div class="form-group">
                  <label for="#" class="col-sm-4 control-label">Number</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="number_2" name="number_2"{{ (Input::old('number_2'))?' 
                     value="'.Input::old('number_2').'"':''}}   placeholder="Number" readonly="readonly" />
                     @if($errors->has('number_2'))
                     {{ $errors->first('number_2')}}
                     @endif 
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12 topspace" align="center">
                     <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
   </div>
</form>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<style type="text/css">
   /*.form-horizontal .control-label {
   text-align: right; 
   padding-left: 60px; 
   }*/
   .font_weight{
   font-weight: normal !important;
   }
</style>
<script type="text/javascript">
   $(document).ready(function(){
   $("#dept_name_number").change(function (e) {
      e.preventDefault(); deptNumber = $("#dept_name_number").val();
      $('#vendorname option').remove();
      $('#vendorname').append("<option>Select Vendor</option>");
      if(deptNumber != '')
      {
   
      $.post("vendorlist", {deptnumber: deptNumber}, function(result){ //alert(deptNumber);
                   var arrayvendorlist = result.split(',');
                 //$makedropdown ='<option value=""></option>';
                 $.each(arrayvendorlist, function (index, value) {
                      var vendor = value.split('_');
   
                      $('#vendorname').append($("<option></option>")
                       .attr("value",vendor[1]+'_'+vendor[0])
                       .text(vendor[0])); 
                     //$makedropdown +='<option value='+vendor[1]+'_'+vendor[0]+'>'+vendor[0]+'</option>';
                   });
                 
                // $makedropdown +='</select>';
   
                 //$("#placevendorlist").html($makedropdown);
               });
     }
   
   });
   
   $("#transporter").change(function (e){
   e.preventDefault(); transporter_number = $("#transporter").val(); //alert(deptNumber);
      if(transporter_number != '')
      {
         $("#number_2").val(transporter_number); return true;
      }
   });
   
   $("#vendorname").change(function(e){ 
     e.preventDefault(); vendorname_number = $("#vendorname").val(); //alert(vendorname_number);
     var vendo0r_numname = vendorname_number.split('_');
      $("#vendornumber").val(vendo0r_numname[0]);
   });
   
   $("#qty_vendor").blur(function(){
        qty_vendor=$("#qty_vendor").val();
        $("#qty_store").val(qty_vendor); return true;
         /*tot=parseInt(qty_vendor)+parseInt(store_inventory_vendor)+parseInt(store_inventory_vendor1)+parseInt(store_inventory_vendor2)+parseInt(selling_supplies_vendor)+parseInt(store_supplies_vendor);
        calnet(); return true;*/
     });
   
     $("#store_inventory_vendor").blur(function(){
       
        store_inventory_vendor=$("#store_inventory_vendor").val();
        $("#store_inventory_store").val(store_inventory_vendor);
        calnet(); return true;
     });
   
     $("#store_inventory_vendor1").blur(function(){
       
        store_inventory_vendor1=$("#store_inventory_vendor1").val();
       $("#store_inventory_store1").val(store_inventory_vendor1);
       calnet(); return true;
     });
   
     $("#store_inventory_vendor2").blur(function(){
        store_inventory_vendor2=$("#store_inventory_vendor2").val();
        $("#store_inventory_store2").val(store_inventory_vendor2);
        calnet(); return true;
     });
   
     $("#selling_supplies_vendor").blur(function(){
        selling_supplies_vendor=$("#selling_supplies_vendor").val();
        $("#selling_supplies_store").val(selling_supplies_vendor);
        calnet(); return true;
     });
   
     $("#store_supplies_vendor").blur(function(){
       
       store_supplies_vendor=$("#store_supplies_vendor").val();
       $("#store_supplies_store").val(store_supplies_vendor);
       calnet(); return true;
     });
   
   });
   
   
   
   function calnet() {
           
        //qty_vendor=$("#qty_vendor").val();
        store_inventory_vendor=$("#store_inventory_vendor").val();
        store_inventory_vendor1=$("#store_inventory_vendor1").val();
        store_inventory_vendor2=$("#store_inventory_vendor2").val();
        selling_supplies_vendor=$("#selling_supplies_vendor").val();
        store_supplies_vendor=$("#store_supplies_vendor").val();
   
        //if(qty_vendor == ''){qty_vendor  = 0; }
        if(store_inventory_vendor == ''){store_inventory_vendor  = 0; }
        if(store_inventory_vendor1 == ''){store_inventory_vendor1  = 0; }
        if(store_inventory_vendor2 == ''){store_inventory_vendor2  = 0; }
        if(selling_supplies_vendor == ''){selling_supplies_vendor  = 0; }
        if(store_supplies_vendor == ''){store_supplies_vendor  = 0; }
   
        tot=parseInt(store_inventory_vendor)+parseInt(store_inventory_vendor1)+parseInt(store_inventory_vendor2)+parseInt(selling_supplies_vendor)+parseInt(store_supplies_vendor);
        $("#net_vendor").val(tot); $("#net_store").val(tot);
   
       }
   
     /*$("#dept_name_number").change(function () {
          deptNumber=$("#dept_name_number").val(); 
          $.post('vendorlist', {deptnumber: deptNumber}, function(result){
                   alert(result);
               });
       });*/
   
     
   $(function() {
           //Initialize Select2 Elements
           $(".select2").select2();
       });
</script>
@stop
