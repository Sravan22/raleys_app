<?php

class Slcadm05UserController extends BaseController
{		

	public function getModiReport()
	{
		return View::make('slcadm05.modi_report');
	}

	public function postSlicReport()
	{
		//
        // $validator = Validator::make(Input::all(),
        //     array(
        //            'slic_report' =>'required'
        //         ),array(
        //            'slic_report.required' =>'Slic Report is Required'
        //         ));
        // if($validator->fails())
        // {          
        //     return Redirect::route('home')
        //             ->withErrors($validator)
        //             ->withInput();
        // }
        // else
        // {
		$slic_report = Input::get('slic_report');
		if($slic_report == 1)
		{	
			return View::make('slcadm05.modi_report');			
		}
		elseif($slic_report == 2)
		{
			return View::make('slcadm05.fm_report'); 
		}
		elseif($slic_report == 3)
		{
			return View::make('slcadm05.admin_report');
		}
		else
		{	

            // Session::flash('alert-danger', " No Store Transfers meet Query criteria.  ");
            // return Redirect::route('socadm00-storetransmission-query')->withInput();
			return View::make('slcadm05.homepage');

                    
		}
   // }
	}

	public function postModiQuery()
	{	
		$work_data = $results = new StdClass();
        $this->userid = Input::get('userid');
		$this->update_date = Input::get('update_date');
		$this->update_time = Input::get('update_time');
		$this->action_cd = Input::get('action_cd');
		$this->maint_type_cd = Input::get('maint_type_cd');
		$this->item_code = Input::get('item_code');
		$this->mstr_fld_no = Input::get('mstr_fld_no');
		$this->orig_value = Input::get('orig_value');
		$this->new_value = Input::get('new_value');

        

        $paginate_array = array();
      	$sql = "SELECT * FROM slhistry WHERE 1=1 ";

      	if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }
        if ($this->update_date != '') {
            $sql.=" AND update_date='" . $this->update_date . "' ";
            $paginate_array['update_date'] = $this->update_date;
        }
        if ($this->update_time != '') {
            $sql.=" AND update_time='" . $this->update_time . "' ";
            $paginate_array['update_time'] = $this->update_time;
        }
        if ($this->action_cd != '') {
            $sql.=" AND action_cd='" . $this->action_cd . "' ";
            $paginate_array['action_cd'] = $this->action_cd;
        }
        if ($this->maint_type_cd != '') {
            $sql.=" AND maint_type_cd='" . $this->maint_type_cd . "' ";
            $paginate_array['maint_type_cd'] = $this->maint_type_cd;
        }
        if ($this->item_code != '') {
            $sql.=" AND item_code='" . $this->item_code . "' ";
            $paginate_array['item_code'] = $this->item_code;
        }
        if ($this->mstr_fld_no != '') {
            $sql.=" AND mstr_fld_no='" . $this->mstr_fld_no . "' ";
            $paginate_array['mstr_fld_no'] = $this->mstr_fld_no;
        }
        if ($this->orig_value != '') {
            $sql.=" AND orig_value='" . $this->orig_value . "' ";
            $paginate_array['orig_value'] = $this->orig_value;
        }
        if ($this->new_value != '') {
            $sql.=" AND new_value='" . $this->new_value . "' ";
            $paginate_array['new_value'] = $this->new_value;
        }

        $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));

        $report_data=array(
                          'userid' => $this->userid,
                          'update_date' => $this->update_date,
                          'update_time' => $this->update_time,
                          'action_cd' => $this->action_cd,
                          'maint_type_cd' => $this->maint_type_cd,
                          'item_code' => $this->item_code,
                          'mstr_fld_no' => $this->mstr_fld_no,
                          'orig_value' =>$this->orig_value,
                          'new_value' => $this->new_value
                          );
        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " There is no Scan History satisfying the query conditions.  ");
            return Redirect::route('slcadm05-query')->withInput();
        }

        $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('slcadm05.modi_browser')->with(array('data' => $work_data, 'pagination' => $pagination, 'report_data' =>$report_data));       
	}
    public function postFMQuery()
    {
        $work_data = $results = new StdClass();
        $this->userid = Input::get('userid');
        $this->update_date = Input::get('update_date');
        $this->update_time = Input::get('update_time');
        $this->item_code = Input::get('item_code');
        $this->new_value = Input::get('new_value');

        $report_data=array(
                          'userid' => $this->userid,
                          'update_date' => $this->update_date,
                          'update_time' => $this->update_time,
                          'item_code' => $this->item_code,
                          'new_value' => $this->new_value
                          );

        $paginate_array = array();
        $sql = "SELECT * FROM slhistry WHERE 1=1  and action_cd = 'F' ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }
        if ($this->update_date != '') {
            $sql.=" AND update_date='" . $this->update_date . "' ";
            $paginate_array['update_date'] = $this->update_date;
        }
        if ($this->update_time != '') {
            $sql.=" AND update_time='" . $this->update_time . "' ";
            $paginate_array['update_time'] = $this->update_time;
        }
        if ($this->item_code != '') {
            $sql.=" AND item_code='" . $this->item_code . "' ";
            $paginate_array['item_code'] = $this->item_code;
        }
        if ($this->new_value != '') {
            $sql.=" AND new_value='" . $this->new_value . "' ";
            $paginate_array['new_value'] = $this->new_value;
        }
        $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));
        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " There are no FM Notes satisfying the query conditions.  ");
            return Redirect::route('slcadm05-fm-query')->withInput();
        }

        $work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('slcadm05.fm_browser')->with(array('data' => $work_data, 'pagination' => $pagination, 'report_data' =>$report_data));   
    }
    public function getModiAuditView()
    {
        $seq_no = Input::get('seq_no');
        //$qeury= ;
        $query = DB::select('select * from slhistry where seq_no = "'.$seq_no.'" ');
         $query_array = json_decode(json_encode($query), true);
        return View::make('slcadm05.auditview',compact('query_array'));
    }
	public function getModiSummary()
	{
        $message = '';
		return View::make('slcadm05.summary',compact('message'));
	}
	public function postModiSummary()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'userid' =>'required',
                   'update_date' =>'required'
                ),array(
                   'userid.required' =>'User ID is Required',
                   'update_date.required' =>'Update Date is Required'
                ));
		if($validator->fails())
        {          
		    return Redirect::route('slcadm05-modi-summary')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$userid = Input::get('userid');
            $update_date = Input::get('update_date');
            //echo $userid.' ',$update_date;
            $sluser_rec = DB::select('SELECT * FROM sluser WHERE userid = "'.$userid.'" ');
            // if($sluser_rec)
            // {
            //     echo $sluser_rec[0]->name;
            // }
            // else
            // {

            // }
            if (!empty($sluser_rec)) 
            {
                 $summary_report_query = DB::select('SELECT * FROM slhistry WHERE slhistry.userid = "'.$userid.'" AND slhistry.update_date = "'.$update_date.'" ORDER BY slhistry.seq_no');
                $summary_report_query_array = json_decode(json_encode($summary_report_query), true);
                //echo '<pre>';print_r($summary_report_query_array);
                $username = $sluser_rec[0]->name;
                return View::make('slcadm05.summary_report_browser',compact('username','summary_report_query_array','userid','update_date'));
            } 
            else 
            {
                $message =  'Unknown User Id';
                return View::make('slcadm05.summary',compact('message'));
            }
        }	
	}
	public function getQuery()
	{
		return View::make('slcadm05.query');
	}
	public function returnHome()
	{
		return View::make('slcadm05.homepage');
	}
	public function getFmQuery()
	{
		return View::make('slcadm05.fm_report');
	}
























}
?>
