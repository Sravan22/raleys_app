<?php

class AccountController extends BaseController
{
	public function getSignIn()
	{
             $user = Auth::user();
        
            if($user){
                return Redirect::route('user-home');
            }
		
		return View::make('accounts.signin');
	}

	public function postSignIn()
	{
            
           
        
		$validator = Validator::make(Input::all(),
			array(
				   'username' =>'required|max:30',
				   'userpassword'=>'required|min:6'
				),array('username.required' =>'User Name is Required',
					'userpassword.required' =>'Password is Required'));

		//print_r($validator);

		if($validator->fails())
		{
			return Redirect::route('account-sign-in')
					->withErrors($validator)
					->withInput();
		}
		else
		{
			// echo 'email-'.Input::get('email'); echo "<br />";
			// echo 'password-'.Input::get('userpassword'); echo "<br />";
			// echo 'password-'.$password=Hash::make(Input::get('userpassword')); exit;
			$auth = Auth::attempt(array(
					'username'=>Input::get('username'),
					'password'=>Input::get('userpassword'),
					'active'=>1
			));	

			if($auth)
			{
				// redirect to user home page
				//print_r($auth); exit;
				//$str_id = 305;
				$str_id = DB::select('select groc_num  as strId from misc');
				if($str_id)
				{
					$str_id = $str_id[0]->strId;
					//echo $str_id;exit;
				}
				else
				{
					$str_id = 305;
				}
				$storeType = DB::select('select value as storeType from config where `key` = "STORE_TYPE" ');	
				if($storeType)
				{
					$storeType = $storeType[0]->storeType;
					//echo $storeType;exit;
				}	
				Session::put('storeid', $str_id);
				Session::put('storeType', $storeType);
				return Redirect::intended('/home');
			}
			else
			{
				return Redirect::route('account-sign-in')
						->with('global', 'Email/Password is worng or accout not activated');			

			}	
		}
		return Redirect::route('account-sign-in')
						->with('global', 'Problam in signing in');	

	}

	public function getNewPassword()
	{
		return View::make('accounts.forgotpassword');
	}

	public function postNewPassword()
	{
		$validator = Validator::make(Input::all(),
			array(
				   'email' =>'required|max:50|email'
				));

		//print_r($validator);

		if($validator->fails())
		{
			return Redirect::route('account-forgotpassword')
					->withErrors($validator)
					->withInput();
		}
		else
		{
			$email=Input::get('email');
			$user = User::where('email','=', $email)->where('active', '=', 1);
			if($user->count())
			{
				$user = $user->first();
				//echo '<pre>',print_r($user),'</pre>'; exit;
				$code = str_random(60);
				//update user to active user state
				$user->active = 0;
				$user->code = $code;

				$user->save();

				if($user->save())
				{
					return Redirect::route('home')
						->with('global', 'We ask admin for password reset!');
				}
			}

			return Redirect::route('home')
						->with('global', 'We could not reset you password. Try again');
		}	
	}

	public function getSignOut() {
			/*	echo "hit"; exit;*/
		    Auth::logout(); Session::flush();
		    return Redirect::route('account-sign-in');

	}

	
	public function getCreate()
	{
		return View::make("accounts.create");
	}

	public function postCreate()
	{
		$validator = Validator::make(Input::all(),
			array(
				    'firstname' =>'required|max:30',
				    'lastname' =>'required|max:30',
				    'username' =>'required|max:30|username|unique:users',
				    'email' =>'required|max:50|email|unique:users',
				   	'username'=>'required|max:20|min:3|unique:users',
				   	'passwrod'=>'required|min:6',
				   	'retypepassword'=>'required|same:passwrod'
				)
		);

		//print_r($validator);

		if($validator->fails())
		{ 
			/* $failedRules = $validator->failed(); 
			  print_r($failedRules);
exit;*/
			return Redirect::route('account-create')
					->withErrors($validator)
					->withInput();
		}
		else
		{
			 $email = Input::get('email'); 
			 $username = Input::get('username');
			 $firstname = Input::get('firstname');
			 $lastname = Input::get('lastname');
			 $password = Input::get('passwrod');
			//exit;
			// Activaion code

			$code = str_random(60);
			$user = User::create(array(
				    'firstname'  => $firstname,
				    'lastname'   => $lastname,
				    'username'   => $username,
					'email'      => $email,
					'password'   => Hash::make($password),
					'code'		 => $code,
					'active'	 =>0	
				));

			if($user)
			{
				 // sent email funcationality

				/*Mail::send('emails.auth.activate', array('link' =>URL::route('/account-activate/', $code), 'username' => $username), function($message) use ($user){
						$message->to($user->email, $user->username)->subject('Activate your account');
				});	*/

				return Redirect::route('home')
						->with('global', 'Yout account has been created! We have sent you an email');	
			}
		}
	}

	public function getActivate($code)
	{
		//return $code;
			$user = User::where('code','=', $code)->where('active', '=', 0);
			if($user->count())
			{
				$user = $user->first();
				/*echo '<pre>',print_r($user),'</pre>';*/

				//update user to active user state
				$user->active = 1;
				$user->code = '';

				$user->save();

				if($user->save())
				{
					return Redirect::route('home')
						->with('global', 'You are an active user now');
				}
			}

			return Redirect::route('home')
						->with('global', 'We could not activate account. Try again');
	}
}