<?php
class BkpfstController extends BaseController
{
	
	// get functions for slc user start
public function getBookkeeper1st()
	{

		return View::make('bkpfst.bkpfstbookkeeper1st');
	}
	
	public function getReturnHome()
	{
		return View::make('bkpfst.homepage');
	}
	public function getFsaisle1()
	{
		return View::make('bkpfst.bkpfstfsaisle1');
	}
	public function getFsmainstore()
	{
		return View::make('bkpfst.bkpfstfsmainstore');
	}
	public function getLogintoaisle1()
	{
		return View::make('bkpfst.bkpfstLogintoaisle1');
	}
	public function getReceivings()
	{
		return View::make('bkpfst.bkpfstreceivings');
	}



	// get functions for slc user end
}

?>