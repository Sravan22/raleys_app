<?php
class BookSafeController extends BaseController
{
	public function getSafeCount()
	{
		return View::make('mktmgr.safecountdate'); 
	}
	public function postSafeCount()
	{
		$validator = Validator::make(Input::all(),
	            array(
	                   'safe_date'=>'required'
	                ),array('safe_date.required'=>'Date is required'));
			if($validator->fails())
	        {          
			    return Redirect::route('bookspro-safecount')
	                    ->withErrors($validator)
	                    ->withInput();
	        }
	        else
	        {
	        	//echo '<pre>';print_r(Input::all());exit;
	        	$safe_date = Input::get('safe_date');
	        	$coin_tot = $this->init_safecoin($safe_date);
	        	$curr_tot = $this->init_safecurr($safe_date);
	        	$other_tot = $this->init_safeother($safe_date);
	        	
	        	if($coin_tot || $curr_tot || $other_tot)
	        	{
	        		$grand_tot = $coin_tot + $curr_tot + $other_tot;
	        	}
	        	else
	        	{
	        		$grand_tot = 0.00;
	        	}
	        	$approvalsOk = $this->approvalsOk($safe_date);
	        	if($approvalsOk)
				{	
					$lastAprvlDate = date('m/d/Y',strtotime($approvalsOk));
					Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
		            return Redirect::route('bookspro-safecount');
				}
				$dayIsLocked = $this->approvalsOk1($safe_date);
				//echo $dayIsLocked;exit;
	        	$file_locked = $this->file_locked($safe_date);
	        	//echo $file_locked;exit;
	        	if($file_locked)
	        	{
	        		$file_locked = "Y";
	        	}
	        	else
	        	{
	        		$file_locked = "N";
	        		// $next_day =  date('Y-m-d', strtotime('+1 day', strtotime($safe_date)));
	        		// echo $next_day.'<br>';
	        		// echo $grand_tot;exit;
	        	}
	        	
	        	$safe_coin=DB::select('SELECT * FROM safe_coin  WHERE safe_coin.safe_date = "'.$safe_date.'" ');
	        	 
	        	if($safe_coin)
	        	{
	        		$safetype = 'update';
	        		$safe_coin_array = json_decode(json_encode($safe_coin), true);
	        	}
	        	else
	        	{
	        		$safetype = 'insert';
	        		$safe_coin_array = array(array(
	        						'pennies_box' => 0,
	        						'pennies_roll' => 0,
	        						'nickles_box' => 0,
	        						'nickles_roll' => 0,
	        						'dimes_box' => 0,
	        						'dimes_roll' => 0,
	        						'quarters_box' => 0,
	        						'quarters_roll' => 0,
	        						'misc_box' => 0,
	        						'misc_roll' => 0,
	        						'misc_desc' => 0,

	        			));
	        	}
	        	//echo '<pre>';print_r($safe_coin_array);exit;
	        	$safe_curr=DB::select('SELECT * FROM safe_curr  WHERE safe_curr.safe_date = "'.$safe_date.'" ');
	        	if($safe_curr)
	        	{
	        		$currencytype = 'update';
	        		$safe_curr_array = json_decode(json_encode($safe_curr), true);
	        	}
	        	else
	        	{
	        		$currencytype = 'insert';
	        		$safe_curr_array = array(array(
	        						'ones_box' => 0,
	        						'ones_roll' => 0,
	        						'ones_held' => 0,
	        						'fives_box' => 0,
	        						'fives_roll' => 0,
	        						'fives_held' => 0,
	        						'tens_box' => 0,
	        						'tens_roll' => 0,
	        						'tens_held' => 0,
	        						'twenties_box' => 0,
	        						'twenties_roll' => 0,
	        						'twenties_held' => 0,
	        						'misc1_box' => 0,
	        						'misc1_roll' => 0,
	        						'misc1_held' => 0,
	        						'misc1_desc' => 0,
	        						'misc2_box' => 0,
	        						'misc2_roll' => 0,
	        						'misc2_held' => 0,
	        						'misc2_desc' => 0

	        			));
	        	}
	        	//$safe_curr_array = json_decode(json_encode($safe_curr), true);
	        	//echo '<pre>';print_r($safe_curr_array);exit; 
	        	$safe_other=DB::select('SELECT * FROM safe_other  WHERE safe_other.safe_date = "'.$safe_date.'" ');
	        	if($safe_other)
	        	{
	        		$othertype = 'update';
	        		$safe_other_array = json_decode(json_encode($safe_other), true); 
	        	}
	        	else
	        	{
	        		$othertype = 'insert';
	        		$safe_other_array = array(array(
	        						'food_stamps' => 0,
	        						'merch_fives' => 0,
	        						'merch_tens' => 0,
	        						'merch_twenties' => 0,
	        						'merch_misc' => 0,
	        						'script1' => 0,
	        						'script2' => 0,
	        						'script3' => 0,
	        						'script4' => 0,
	        						'script5' => 0,
	        						'script6' => 0,
	        						'script7' => 0,
	        						'script8' => 0,
	        						'script9' => 0,
	        						'script10' => 0,
	        						'begin_loans' => 0,
	        						'misc1_amt' => 0,
	        						'misc1_desc' => 0,
	        						'misc2_amt' => 0,
	        						'misc2_desc' => 0,
	        						'instr_chrg' => 0,
	        						'pay_adv' => 0,
	        						'night_crew' => 0,
	        						'debit' => 0,
	        						'credit' => 0,
	        						'ebt' => 0,
	        						'giftcard' => 0,
	        						'misc3_amt' => 0,
	        						'misc3_desc' => 0,

	        			));
	        	}
	        	//echo '<pre>';print_r($safe_other_array);exit; 
	        	return View::make('mktmgr.safe_count_details',compact('safe_date','coin_tot','curr_tot','other_tot','grand_tot','safe_coin_array','safe_curr_array','safe_other_array','safetype','currencytype','othertype','dayIsLocked','file_locked')); 
	        }
	}
	public function postSafeCountPostData()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$safe_date = Input::get('safe_date');
		$file_locked = Input::get('file_locked');
		$grand_tot = Input::get('grand_tot')*100;
		//echo $grand_tot;exit;
		$next_day =  date('Y-m-d', strtotime('+1 day', strtotime($safe_date)));
		//echo 'SELECT * FROM mainsafe  WHERE mainsafe.safe_date = "'.$safe_date.'"';exit;
		if($file_locked == "N")
		{
			$mainsafe_entry_safe_date = DB::select('SELECT * FROM mainsafe  WHERE mainsafe.safe_date = "'.$safe_date.'"');
			if($mainsafe_entry_safe_date)
			{
				DB::update('UPDATE mainsafe  SET mainsafe.safe_cnt = "'.$grand_tot.'"
             WHERE mainsafe.safe_date = "'.$safe_date.'" ');
			}
			else
			{
				DB::insert('INSERT INTO mainsafe
              VALUES (0,0,0,0,0,0,"'.$grand_tot.'",0,0,0,0,"'.$safe_date.'") ');
			}


			$mainsafe_entry_next_date = DB::select('SELECT * FROM mainsafe  WHERE mainsafe.safe_date = "'.$next_day.'"');
			if($mainsafe_entry_next_date)
			{
				DB::update('UPDATE mainsafe  SET mainsafe.safe_cnt = "'.$grand_tot.'"
             WHERE mainsafe.safe_date = "'.$next_day.'" ');
			}
			else
			{
				DB::insert('INSERT INTO mainsafe
              VALUES (0,0,0,0,0,0,"'.$grand_tot.'",0,0,0,0,"'.$next_day.'") ');
			}
		}
		return Redirect::route('bookspro-safecount');
	}
	public function approvalsOk($safe_date)
	{
		$prvDate = date('Y-m-d', strtotime($safe_date .' -1 day'));
		//echo $prvDate.'<br>';//exit;
		//echo 'select max(date_stamp) as lastAprvlDate from mgmt where  mgmt.date_stamp < "'.$safe_date.'" ';exit;
		$lastAprvlDate=DB::select('select max(date_stamp) as lastAprvlDate from mgmt where  mgmt.date_stamp < "'.$safe_date.'" ');
		$lastAprvlDate = $lastAprvlDate[0]->lastAprvlDate;
		//echo $lastAprvlDate.'<br>';
		//$lastAprvlDate = date("m/d/Y", strtotime($lastAprvlDate));
		//echo $lastAprvlDate.'<br>';exit;
		if($prvDate <> $lastAprvlDate)
		{
			return $lastAprvlDate;//exit;
		}


		//if($msg != ''){ Session::flash('alert-danger',$msg); }
		//return $retVal;
	}
	public function approvalsOk1($safe_date)
	{
		$dayIsLocked = "N";
		$now = time(); // or your date as well
		$datediff = $now - strtotime($safe_date);
		//echo $datediff;exit;
		$numdays=floor($datediff / (60 * 60 * 24));
		//echo $numdays;exit;
		//echo 'select * from mgmt where  mgmt.date_stamp ="'.$safe_date.'"';exit;
		if($numdays > 1)
		{
			//echo $numdays;exit;
			$mgmt=DB::select('select * from mgmt where  mgmt.date_stamp ="'.$safe_date.'"');
			$check_mgmt = json_decode(json_encode($mgmt), true);
			//echo count($sql);exit;
			if($check_mgmt)
			{
				$dayIsLocked = "Y";
			}
		}
		return $dayIsLocked;
	}
	public function file_locked($stamp_date)
	{
		$file_locked=DB::select('SELECT * from locks  WHERE locks.date_stamp = "'.$stamp_date.'" ');
		if($file_locked)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	/*
	public function getSafeCoin()
	{
		$safe_date = Input::get('safe_date');
		$safe_coin=DB::select('SELECT * FROM safe_coin WHERE safe_coin.safe_date = "'.$safe_date.'" ');
		$safe_coin_array = json_decode(json_encode($safe_coin), true); 
		return View::make('mktmgr.safecoin',compact('safe_date','safe_coin_array')); 
		// $safe_date = Input::get('safe_date');
		// echo $safe_date;exit;
	}
	*/
	public function init_safecoin($safe_date)
	{
		$coin_tot=DB::select('SELECT ((safe_coin.pennies_box +
				           safe_coin.pennies_roll +
				           safe_coin.nickles_box +
				           safe_coin.nickles_roll +
				           safe_coin.dimes_box +
				           safe_coin.dimes_roll +
				           safe_coin.quarters_box +
				           safe_coin.quarters_roll +
				           safe_coin.misc_box +
				           safe_coin.misc_roll) / 100)
				      as  coin_tot
				      FROM safe_coin
				     WHERE safe_coin.safe_date = "'.$safe_date.'" ');
	        	if($coin_tot)
	        	{
	        		$coin_tot = $coin_tot[0]->coin_tot;
	        	}
	        	else
	        	{
	        		$coin_tot = 0.00;
	        	}
	    return $coin_tot;    	
	}
	public function init_safecurr($safe_date)
	{
		$curr_tot=DB::select('SELECT ((safe_curr.ones_box +
					           safe_curr.ones_roll +
					           safe_curr.ones_held +
					           safe_curr.fives_box +
					           safe_curr.fives_roll +
					           safe_curr.fives_held +
					           safe_curr.tens_box +
					           safe_curr.tens_roll +
					           safe_curr.tens_held +
					           safe_curr.twenties_box +
					           safe_curr.twenties_roll +
					           safe_curr.twenties_held +
					           safe_curr.misc1_box +
					           safe_curr.misc1_roll +
					           safe_curr.misc1_held +
					           safe_curr.misc2_box +
					           safe_curr.misc2_roll +
					           safe_curr.misc2_held) / 100) 
					      as  curr_tot
							FROM safe_curr
					     WHERE safe_curr.safe_date = "'.$safe_date.'" ');
	        	if($curr_tot)
	        	{
	        		$curr_tot = $curr_tot[0]->curr_tot;
	        	}
	        	else
	        	{
	        		$curr_tot = 0.00;
	        	}
	    return $curr_tot;    	
	}
	public function init_safeother($safe_date)
	{
		$other_tot=DB::select('SELECT ((safe_other.food_stamps +
						           safe_other.merch_fives +
						           safe_other.merch_tens +
						           safe_other.merch_twenties +
						           safe_other.merch_misc +
						           safe_other.script1 +
						           safe_other.script2 +
						           safe_other.script3 +
						           safe_other.script4 +
						           safe_other.script5 +
						           safe_other.script6 +
						           safe_other.script7 +
						           safe_other.script8 +
						           safe_other.script9 +
						           safe_other.script10 +
						           safe_other.begin_loans +
						           safe_other.misc1_amt +
						           safe_other.misc2_amt +
						           safe_other.instr_chrg +
						           safe_other.pay_adv +
						           safe_other.night_crew +
						           safe_other.debit +
						           safe_other.credit +
						           safe_other.ebt +
						           safe_other.giftcard +
						           safe_other.misc3_amt) / 100) 
						      as other_tot
							FROM safe_other
					     WHERE safe_other.safe_date = "'.$safe_date.'" ');
	        	if($other_tot)
	        	{
	        		$other_tot = $other_tot[0]->other_tot;
	        	}
	        	else
	        	{
	        		$other_tot = 0.00;
	        	}
	    return $other_tot;    	
	}
	public function SafeCoinSubmit()
	{
		//$coindata = serialize(Input::get('pennies_box'));
		$data =  Input::get('data');
		$type = Input::get('type');
		$safe_date = Input::get('safe_date');
		
		$safecoindata = DB::select('select * from safe_coin where safe_date = "'.$safe_date.'"');
		$safecoindata = json_decode(json_encode($safecoindata), true);
		
		$pennies_box =  $data[0]['value']*100;
		$pennies_roll =  $data[1]['value']*100;
		$nickles_box =  $data[2]['value']*100;
		$nickles_roll =  $data[3]['value']*100;
		$dimes_box =  $data[4]['value']*100;
		$dimes_roll =  $data[5]['value']*100;
		$quarters_box =  $data[6]['value']*100;
		$quarters_roll =  $data[7]['value']*100;
		$misc_desc =  $data[8]['value'];
		$misc_box =  $data[9]['value']*100;
		$misc_roll =  $data[10]['value']*100;
		if(empty($safecoindata))
		{
			if($type == 'insert')
			{	
				DB::table('safe_coin')->insert(
				    array(
				    	'pennies_box' => $pennies_box,
				    	'pennies_roll' => $pennies_roll,
				    	'nickles_box' => $nickles_box,
				    	'nickles_roll' => $nickles_roll,
				    	'dimes_box' => $dimes_box,
				    	'dimes_roll' => $dimes_roll,
				    	'quarters_box' => $quarters_box,
				    	'quarters_roll' => $quarters_roll,
				    	'misc_desc' => $misc_desc,
				    	'misc_box' => $misc_box,
				    	'misc_roll' => $misc_roll,
				    	'safe_date' => $safe_date
				    		 )
				);
				return 'Row Inserted';
			}
		}
		else
		{
				DB::table('safe_coin')
	            ->where('safe_date', $safe_date)
	            ->update(
	            	 array(
				    	'pennies_box' => $pennies_box,
				    	'pennies_roll' => $pennies_roll,
				    	'nickles_box' => $nickles_box,
				    	'nickles_roll' => $nickles_roll,
				    	'dimes_box' => $dimes_box,
				    	'dimes_roll' => $dimes_roll,
				    	'quarters_box' => $quarters_box,
				    	'quarters_roll' => $quarters_roll,
				    	'misc_desc' => $misc_desc,
				    	'misc_box' => $misc_box,
				    	'misc_roll' => $misc_roll
				    		 )
	            	);
	            return 'Row Updated';
		}
	}
	public function SafeCurrencySubmit()
	{
		$data = Input::get('data');
		$type = Input::get('type');
		$safe_date = Input::get('safe_date');
		//echo $type;
		//echo '<pre>';print_r($data);
		//echo 'select * from safe_curr where safe_date = "'.$safe_date.'"';exit;
		$safecurrencydata = DB::select('select * from safe_curr where safe_date = "'.$safe_date.'"');
		$safecurrencydata = json_decode(json_encode($safecurrencydata), true);
		//echo '<pre>';print_r($data);exit;
		$ones_box =  $data[0]['value']*100;
		$ones_roll =  $data[1]['value']*100;
		$ones_held =  $data[2]['value']*100;
		$fives_box =  $data[3]['value']*100;
		$fives_roll =  $data[4]['value']*100;
		$fives_held =  $data[5]['value']*100;
		$tens_box =  $data[6]['value']*100;
		$tens_roll =  $data[7]['value']*100;
		$tens_held =  $data[8]['value']*100;
		$twenties_box =  $data[9]['value']*100;
		$twenties_roll =  $data[10]['value']*100;
		$twenties_held =  $data[11]['value']*100;
		$misc1_desc =  $data[12]['value'];
		$misc1_box =  $data[13]['value']*100;
		$misc1_roll =  $data[14]['value']*100;
		$misc1_held =  $data[15]['value']*100;
		$misc2_desc =  $data[16]['value'];
		$misc2_box =  $data[17]['value']*100;
		$misc2_roll =  $data[18]['value']*100;
		$misc2_held =  $data[19]['value']*100;

		if(empty($safecurrencydata))
		{
			if($type == 'insert')
			{	
				DB::table('safe_curr')->insert(
				    array(
				    	'ones_box' => $ones_box,
				    	'ones_roll' => $ones_roll,
				    	'ones_held' => $ones_held,
				    	'fives_box' => $fives_box,
				    	'fives_roll' => $fives_roll,
				    	'fives_held' => $fives_held,
				    	'tens_box' => $tens_box,
				    	'tens_roll' => $tens_roll,
				    	'tens_held' => $tens_held,
				    	'twenties_box' => $twenties_box,
				    	'twenties_roll' => $twenties_roll,
				    	'twenties_held' => $twenties_held,
				    	'misc1_desc' => $misc1_desc,
				    	'misc1_box' => $misc1_box,
				    	'misc1_roll' => $misc1_roll,
				    	'misc1_held' => $misc1_held,
				    	'misc2_desc' => $misc2_desc,
				    	'misc2_box' => $misc2_box,
				    	'misc2_roll' => $misc2_roll,
				    	'misc2_held' => $misc2_held,
				    	'safe_date' => $safe_date
				    		 )
				);
				return 'Row Inserted';
			}
		}
		else
		{
			DB::table('safe_curr')
            ->where('safe_date', $safe_date)
            ->update(
            	 array(
			    	'ones_box' => $ones_box,
			    	'ones_roll' => $ones_roll,
			    	'ones_held' => $ones_held,
			    	'fives_box' => $fives_box,
			    	'fives_roll' => $fives_roll,
			    	'fives_held' => $fives_held,
			    	'tens_box' => $tens_box,
			    	'tens_roll' => $tens_roll,
			    	'tens_held' => $tens_held,
			    	'twenties_box' => $twenties_box,
			    	'twenties_roll' => $twenties_roll,
			    	'twenties_held' => $twenties_held,
			    	'misc1_desc' => $misc1_desc,
			    	'misc1_box' => $misc1_box,
			    	'misc1_roll' => $misc1_roll,
			    	'misc1_held' => $misc1_held,
			    	'misc2_desc' => $misc2_desc,
			    	'misc2_box' => $misc2_box,
			    	'misc2_roll' => $misc2_roll,
			    	'misc2_held' => $misc2_held
			    		 )
            	);
            return 'Row Updated';
		}
	}
	public function SafeOtherSubmit()
	{
		$data = Input::get('data');
		$type = Input::get('type');
		$safe_date = Input::get('safe_date');
		//echo $type;
		//echo '<pre>';print_r($data);
		$safe_otherdata = DB::select('select * from safe_other where safe_date = "'.$safe_date.'"');
		$safe_otherdata = json_decode(json_encode($safe_otherdata), true);
		//echo '<pre>';print_r($data);

		$food_stamps =  $data[0]['value']*100;
		$begin_loans =  $data[1]['value']*100;
		$misc1_desc =  $data[2]['value'];
		$misc1_amt =  $data[3]['value']*100;
		$misc2_desc =  $data[4]['value'];
		$misc2_amt =  $data[5]['value']*100;
		$instr_chrg =  $data[6]['value']*100;
		$pay_adv =  $data[7]['value']*100;
		$night_crew =  $data[8]['value']*100;
		$debit =  $data[9]['value']*100;
		$credit =  $data[10]['value']*100;
		$ebt =  $data[11]['value']*100;
		$giftcard =  $data[12]['value'] * 100;
		$misc3_desc =  $data[13]['value'];
		$misc3_amt =  $data[14]['value']*100;
		$merch_fives =  $data[15]['value']*100;
		$merch_tens =  $data[16]['value']*100;
		$merch_twenties =  $data[17]['value']*100;
		$merch_misc =  $data[18]['value']*100;
		$script1 =  $data[19]['value']*100;
		$script2 =  $data[20]['value']*100;
		$script3 =  $data[21]['value']*100;
		$script4 =  $data[22]['value']*100;
		$script5 =  $data[23]['value']*100;
		$script6 =  $data[24]['value']*100;
		$script7 =  $data[25]['value']*100;
		$script8 =  $data[26]['value']*100;
		$script9 =  $data[27]['value']*100;
		$script10 =  $data[28]['value']*100;

		if(empty($safe_otherdata))
		{
			if($type == 'insert')
			{	
				DB::table('safe_other')->insert(
				    array(
				    	'food_stamps' => $food_stamps,
				    	'begin_loans' => $begin_loans,
				    	'misc1_desc' => $misc1_desc,
				    	'misc1_amt' => $misc1_amt,
				    	'misc2_desc' => $misc2_desc,
				    	'misc2_amt' => $misc2_amt,
				    	'instr_chrg' => $instr_chrg,
				    	'pay_adv' => $pay_adv,
				    	'night_crew' => $night_crew,
				    	'debit' => $debit,
				    	'credit' => $credit,
				    	'ebt' => $ebt,
				    	'giftcard' => $giftcard,
				    	'misc3_desc' => $misc3_desc,
				    	'misc3_amt' => $misc3_amt,
				    	'merch_fives' => $merch_fives,
				    	'merch_tens' => $merch_tens,
				    	'merch_twenties' => $merch_twenties,
				    	'merch_misc' => $merch_misc,
				    	'script1' => $script1,
				    	'script2' => $script2,
				    	'script3' => $script3,
				    	'script4' => $script4,
				    	'script5' => $script5,
				    	'script6' => $script6,
				    	'script7' => $script7,
				    	'script8' => $script8,
				    	'script9' => $script9,
				    	'script10' => $script10,
				    	'safe_date' => $safe_date
				    		 )
				);
				return 'Row Inserted';
			}
		}
		else
		{
			DB::table('safe_other')
            ->where('safe_date', $safe_date)
            ->update(
            	 array(
			    	'food_stamps' => $food_stamps,
				    	'begin_loans' => $begin_loans,
				    	'misc1_desc' => $misc1_desc,
				    	'misc1_amt' => $misc1_amt,
				    	'misc2_desc' => $misc2_desc,
				    	'misc2_amt' => $misc2_amt,
				    	'instr_chrg' => $instr_chrg,
				    	'pay_adv' => $pay_adv,
				    	'night_crew' => $night_crew,
				    	'debit' => $debit,
				    	'credit' => $credit,
				    	'ebt' => $ebt,
				    	'giftcard' => $giftcard,
				    	'misc3_desc' => $misc3_desc,
				    	'misc3_amt' => $misc3_amt,
				    	'merch_fives' => $merch_fives,
				    	'merch_tens' => $merch_tens,
				    	'merch_twenties' => $merch_twenties,
				    	'merch_misc' => $merch_misc,
				    	'script1' => $script1,
				    	'script2' => $script2,
				    	'script3' => $script3,
				    	'script4' => $script4,
				    	'script5' => $script5,
				    	'script6' => $script6,
				    	'script7' => $script7,
				    	'script8' => $script8,
				    	'script9' => $script9,
				    	'script10' => $script10
			    		 )
            	);
            return 'Row Updated';
		}
	}
	public function getSafeReportDate()
	{
		return View::make('mktmgr.safereportdate'); 
	}
	public function postSafeReport()
	{
		$validator = Validator::make(Input::all(),
	            array(
	                   'safe_date'=>'required'
	                ),array('safe_date.required'=>'Date is required'));
			if($validator->fails())
	        {          
			    return Redirect::route('bookspro-safereportdate')
	                    ->withErrors($validator)
	                    ->withInput();
	        }
	        else
	        {
	        	$safe_date = Input::get('safe_date');
	        	$approvalsOk = $this->approvalsOk($safe_date);
	        	//echo $approvalsOk;exit;
	        	if($approvalsOk)
				{	
					$lastAprvlDate = date('m/d/Y',strtotime($approvalsOk));
					Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
		            return Redirect::route('bookspro-safereportdate');
				}
				$dayIsLocked = $this->approvalsOk1($safe_date);
				//echo $dayIsLocked;exit;
				// $dayIsLocked = "N";
	        	$file_locked = $this->file_locked($safe_date);
	        	
	        	if($file_locked)
	        	{
	        		$file_locked = "Y";
	        	}
	        	else
	        	{
	        		$file_locked = "N";
	        		$calc_debtot = $this->calc_debtot($safe_date);
	        	}//exit;
	        	// $file_locked = "N";
	        	$mainsafe_records = DB::select('SELECT * FROM mainsafe WHERE mainsafe.safe_date = "'.$safe_date.'" ');
	        	$mainsafe_array = json_decode(json_encode($mainsafe_records), true);
	        	if(!empty($mainsafe_array))
	        	{
	        		$begin_safe = $mainsafe_array[0]['begin_safe'];
	        	}
	        	else
	        	{
	        		$begin_safe = 0;
	        	}
	        	if(!empty($mainsafe_array))
	        	{
	        		$safe_cnt = $mainsafe_array[0]['safe_cnt'];
	        	}
	        	else
	        	{
	        		$safe_cnt = 0;
	        	}
	        	//echo $begin_safe;exit;
	        	$del_tranfs = $this->total_amt("T",$safe_date);
	        	if(!($del_tranfs))
	        	{
	        		$del_tranfs = 0;;
	        	}
	        	$deposits = $this->total_amt("P",$safe_date);
	        	if(!($deposits))
	        	{
	        		$deposits = 0;;
	        	}
	        	$store_tx = $this->total_amt("S",$safe_date);
	        	if(!($store_tx))
	        	{
	        		$store_tx = 0;;
	        	}
	        	$po_types = $this->total_amt("O",$safe_date);
	        	if(!($po_types))
	        	{
	        		$po_types = 0;;
	        	}
	        	$find_os = $this->find_os($safe_date);
	        	//echo '<pre>';print_r($find_os);exit;
	        	$calc_totals = $this->calc_totals($begin_safe,$del_tranfs,$deposits,$store_tx,$po_types,$safe_cnt,$find_os['groc_os'],$find_os['drug_os']);
	        	//echo '<pre>';print_r($calc_totals);exit;
	        	$resultArray = array(
	        		'begin_safe' => $begin_safe/100,
	        		'del_tranfs' => $del_tranfs/100,
	        		'deposits' => $deposits/100,
	        		'store_tx' => $store_tx/100,
	        		'po_types' => $po_types/100,
	        		'tot_acct' => $calc_totals['tot_acct']/100,
	        		'safe_cnt' => $safe_cnt/100,
	        		'safe_os' => $calc_totals['safe_os']/100,
	        		'groc_os' => $find_os['groc_os']/100,
	        		'drug_os' => $find_os['drug_os']/100,
	        		'total' => $calc_totals['total']/100 
	        		);
	        	//echo '<pre>';print_r($resultArray);exit;
	        	$disp_del = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt
						 from safeitems left outer join safeentry
						 on safeitems.item_id = safeentry.item_id
						 and safeentry.entry_date = "'.$safe_date.'"
						 and safeentry.item_amt is not null
						 where safeitems.item_type = "T"
						 and safeitems.item_id NOT LIKE "%T%"
						 and safeitems.disable not in ("y", "Y")
						 order by safeitems.item_desc ');
	        	$disp_del_array = json_decode(json_encode($disp_del), true);

	        	$disp_trf = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt,safeentry.source_id from safeitems left outer join safeentry on safeitems.item_id = safeentry.item_id and safeentry.entry_date = "'.$safe_date.'" and safeentry.item_amt is not null where safeitems.item_type = "T" and safeitems.item_id LIKE "%T%" and safeitems.disable not in ("y", "Y") order by safeitems.item_desc');
	        	$disp_trf_array = json_decode(json_encode($disp_trf), true);
	        	//echo '<pre>';print_r($disp_trf_array);exit;
	        	// for($i = 0;$i<count($disp_trf_array);$i++)
	        	// {
	        	// 	$get_trfamt = $this->get_trfamt($disp_trf_array[$i]['item_id']);
	        	// }
	        	$disp_deposits = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt
						 from safeitems left outer join safeentry
						 on safeitems.item_id = safeentry.item_id
						 and safeentry.entry_date = "'.$safe_date.'"
						 and safeentry.item_amt is not null
						 where safeitems.item_type = "P"
						 and safeitems.disable not in ("y", "Y")
						 order by safeitems.item_id asc ');
	        	$disp_deposits_array = json_decode(json_encode($disp_deposits), true);

	        	$disp_storetx = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt
						 from safeitems left outer join safeentry
						 on safeitems.item_id = safeentry.item_id
						 and safeentry.entry_date = "'.$safe_date.'"
						 and safeentry.item_amt is not null
						 where safeitems.item_type = "S"
						 and safeitems.disable not in ("y", "Y")
						 order by safeitems.item_id asc ');
	        	$disp_storetx_array = json_decode(json_encode($disp_storetx), true);


	        	$disp_potypes = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt
						 from safeitems left outer join safeentry
						 on safeitems.item_id = safeentry.item_id
						 and safeentry.entry_date = "'.$safe_date.'"
						 and safeentry.item_amt is not null
						 where safeitems.item_type = "O"
						 and safeitems.disable not in ("y", "Y")
						 order by safeitems.item_id asc ');
	        	$disp_potypes_array  = json_decode(json_encode($disp_potypes), true);
	        	//echo '<pre>';print_r($disp_deposits_array);exit;
	        	return View::make('mktmgr.safe_report_details',compact('resultArray','safe_date','disp_deposits_array','disp_storetx_array','disp_potypes_array','disp_del_array','disp_trf_array','dayIsLocked','file_locked')); 
	        }
	}
	public function total_amt($code,$safe_date)
	{
		$total_amt = DB::select('SELECT SUM(safeentry.item_amt) as tot
            FROM safeentry,safeitems
           WHERE safeentry.item_id = safeitems.item_id
             AND safeitems.item_type = "'.$code.'"
             AND safeentry.entry_date = "'.$safe_date.'" ');
		if($total_amt)
		{
			$tot = $total_amt[0]->tot;
		}
		else
		{
			$tot = 0;
		}
		return $tot; 
	}
	public function find_os($safe_date)
	{
		$arrtot = array();
		$get_dogo = $this->get_dogo($safe_date);
		//echo '<pre>';print_r($get_dogo);exit;
		$groc_os = $get_dogo['groctot'];
		$drug_os = $get_dogo['drugtot'];
		if(!($groc_os))
    	{
    		$groc_os = 0;;
    	}
    	if(!($drug_os))
    	{
    		$drug_os = 0;;
    	}
    	$arrtot['groc_os'] = $groc_os;
        $arrtot['drug_os'] = $drug_os; 
        return $arrtot;
		//echo $groc_os.' '.$drug_os;exit;
	}
	public function get_dogo($safe_date)
	{
		//echo $safe_date;exit;
		$arrtot = array();
		$groctot = 0;
		$drugtot = 0;
		$subtot1 = 0;
		$subtot2 = 0;
		$reg_cur = DB::select('SELECT registers.reg_num, registers.reg_type FROM registers ORDER BY registers.reg_num');
		$reg_cur_array = json_decode(json_encode($reg_cur), true);
		for($i=0;$i<count($reg_cur_array);$i++) 
		{
			$subtot1_query = DB::select('SELECT SUM(entry.item_amt) as subtot1 FROM entry, items  WHERE entry.reg_num = "'.$reg_cur_array[$i]['reg_num'].'"
          	AND entry.item_id = items.item_id
          	AND items.item_type = "D"
        	AND entry.entry_date = "'.$safe_date.'" ');
			if($subtot1_query)
			{
				$subtot1 = $subtot1_query[0]->subtot1;
			}
			else
			{
				$subtot1 = 0;
			}
			$subtot2_query = DB::select('SELECT SUM(entry.item_amt) as subtot2 FROM entry, items  WHERE entry.reg_num = "'.$reg_cur_array[$i]['reg_num'].'"
          	AND entry.item_id = items.item_id
          	AND items.item_type = "C"
        	AND entry.entry_date = "'.$safe_date.'" ');
			if($subtot2_query)
			{
				$subtot2 = $subtot2_query[0]->subtot2;
			}
			else
			{
				$subtot2 = 0;
			}
			if($reg_cur_array[$i]['reg_type'] == "G")
			{
				$groctot = $groctot + ($subtot1 - $subtot2);
			}
			elseif($reg_cur_array[$i]['reg_type'] == "D")
			{
				$drugtot = $drugtot + ($subtot1 - $subtot2);
			}
			//echo $subtot1;exit();
			//echo $groctot.' '.$drugtot;exit;
			$subtot1 = 0;
			$subtot2 = 0;
		}
		$arrtot['groctot'] = $groctot;
        $arrtot['drugtot'] = $drugtot;
		return $arrtot;	
	}
	public function calc_debtot($safe_date)
	{
		//echo $safe_date;exit;
		$gsubtot = 0;
		$gsubtot_query = DB::select('SELECT SUM(entry.item_amt) as gsubtot
         FROM entry,items WHERE entry.entry_date = "'.$safe_date.'" AND 
         entry.item_id = items.item_id
          AND items.item_type = "D" ');
		if($gsubtot_query)
		{
			$gsubtot = $gsubtot_query[0]->gsubtot;
		}
		else
		{
			$gsubtot = 0;
		}
		//echo $gsubtot;exit;
		$td_amt_query = DB::select('SELECT safeentry.item_amt  FROM safeentry
        WHERE safeentry.entry_date = "'.$safe_date.'"
          AND safeentry.item_id = "TD" ');
		if($td_amt_query)
		{
			DB::update('UPDATE safeentry SET safeentry.item_amt = "'.$gsubtot.'"     WHERE safeentry.item_id = "TD" AND 
				safeentry.entry_date = "'.$safe_date.'" ');
		}
		else
		{
			DB::insert('INSERT INTO safeentry
            VALUES ("TD","'.$gsubtot.'","book1","'.$safe_date.'")');
		}
		$this->updateSafeEntryItems("LR",$safe_date);
		$this->updateSafeEntryItems("PC",$safe_date);
		$this->updateSafeEntryItems("VR",$safe_date);
	}
	public function updateSafeEntryItems($itemId,$safe_date)
	{
		//echo $itemId;//exit;
		$tot = 0;
		$td_amt_query = DB::select('SELECT SUM(entry.item_amt) as tot FROM entry WHERE entry.entry_date = "'.$safe_date.'"
       AND entry.item_id = "'.$itemId.'" ');
		if($td_amt_query)
		{
			$tot = -1*($td_amt_query[0]->tot);
		}
		else
		{
			$tot = 0;
		}
		//echo $tot;exit;
		$safeentry_query = DB::select('SELECT * from safeentry  WHERE safeentry.entry_date = "'.$safe_date.'"
       AND safeentry.item_id = "'.$itemId.'" ');
		if($safeentry_query)
		{
			DB::update('UPDATE safeentry SET safeentry.item_amt = "'.$tot.'" WHERE safeentry.item_id = "'.$itemId.'" AND 
				safeentry.entry_date = "'.$safe_date.'" ');
		}
		else
		{
			DB::insert('INSERT INTO safeentry
            VALUES ("'.$itemId.'","'.$tot.'","book1","'.$safe_date.'")');
		}
	}
	public function calc_totals($begin_safe,$del_tranfs,$deposits,$store_tx,$po_types,$safe_cnt,$groc_os,$drug_os)
	{
		$calc_totals = array();
		$tot_acct = $begin_safe +$del_tranfs +$deposits + $store_tx + $po_types;
		$safe_os =  $safe_cnt - $tot_acct;
		$total = $safe_os + $groc_os + $drug_os;
		//echo $total;exit;
		$calc_totals['tot_acct'] = $tot_acct;
		$calc_totals['safe_os'] = $safe_os;
		$calc_totals['total'] = $total;
		return $calc_totals;
	}
	public function stBoxSubmit()
	{
		$data =  Input::get('data');
		$safe_date =  Input::get('safe_date');
		for($i=0;$i<count($data);$i++)
		{
			//echo $data[$i]['name'].' '.$data[$i]['value'].'<br>';
			$subtot1_query = DB::select('select * from safeentry where safeentry.item_id =  "'.$data[$i]['name'].'"  AND safeentry.entry_date = "'.$safe_date.'" ');
			$item_amt = $data[$i]['value']*100;
			if($subtot1_query)
			{
				DB::update('update safeentry SET  safeentry.item_amt = "'.$item_amt.'",safeentry.source_id  = "book1" WHERE safeentry.item_id = "'.$data[$i]['name'].'" AND safeentry.entry_date = "'.$safe_date.'"');
			}
			else
			{
				DB::insert(' INSERT INTO safeentry VALUES ("'.$data[$i]['name'].'", "'.$item_amt.'", "book1", "'.$safe_date.'")');
			}
		}
	}
	public function potBoxSubmit()
	{
		$data =  Input::get('data');
		//echo '<pre>';print_r($data);exit;
		$safe_date =  Input::get('safe_date');
		for($i=0;$i<count($data);$i++)
		{
			//echo $data[$i]['name'].' '.$data[$i]['value'].'<br>';
			$subtot1_query = DB::select('select * from safeentry where safeentry.item_id =  "'.$data[$i]['name'].'"  AND safeentry.entry_date = "'.$safe_date.'" ');
			$item_amt = $data[$i]['value']*100;
			if($subtot1_query)
			{
				DB::update('update safeentry SET  safeentry.item_amt = "'.$item_amt.'",safeentry.source_id  = "book1" WHERE safeentry.item_id = "'.$data[$i]['name'].'" AND safeentry.entry_date = "'.$safe_date.'"');
			}
			else
			{
				DB::insert(' INSERT INTO safeentry VALUES ("'.$data[$i]['name'].'", "'.$item_amt.'", "book1", "'.$safe_date.'")');
			}
		}
	}
	public function depositsBoxSubmit()
	{
		$data =  Input::get('data');
		//echo '<pre>';print_r($data);exit;
		$safe_date =  Input::get('safe_date');
		for($i=0;$i<count($data);$i++)
		{
			//echo $data[$i]['name'].' '.$data[$i]['value'].'<br>';
			$subtot1_query = DB::select('select * from safeentry where safeentry.item_id =  "'.$data[$i]['name'].'"  AND safeentry.entry_date = "'.$safe_date.'" ');
			$item_amt = $data[$i]['value']*100;
			if($subtot1_query)
			{
				DB::update('update safeentry SET  safeentry.item_amt = "'.$item_amt.'",safeentry.source_id  = "book1" WHERE safeentry.item_id = "'.$data[$i]['name'].'" AND safeentry.entry_date = "'.$safe_date.'"');
			}
			else
			{
				DB::insert(' INSERT INTO safeentry VALUES ("'.$data[$i]['name'].'", "'.$item_amt.'", "book1", "'.$safe_date.'")');
			}
		}
	}
	public function deliveriesBoxSubmit()
	{
		$data =  Input::get('data');
		//echo '<pre>';print_r($data);exit;
		$safe_date =  Input::get('safe_date');
		for($i=0;$i<count($data);$i++)
		{
			//echo $data[$i]['name'].' '.$data[$i]['value'].'<br>';
			$subtot1_query = DB::select('select * from safeentry where safeentry.item_id =  "'.$data[$i]['name'].'"  AND safeentry.entry_date = "'.$safe_date.'" ');
			$item_amt = $data[$i]['value']*100;
			if($subtot1_query)
			{
				DB::update('update safeentry SET  safeentry.item_amt = "'.$item_amt.'",safeentry.source_id  = "book1" WHERE safeentry.item_id = "'.$data[$i]['name'].'" AND safeentry.entry_date = "'.$safe_date.'"');
			}
			else
			{
				DB::insert(' INSERT INTO safeentry VALUES ("'.$data[$i]['name'].'", "'.$item_amt.'", "book1", "'.$safe_date.'")');
			}
		}
	}
	public function transfersBoxSubmit()
	{
		$data =  Input::get('data');
		//echo '<pre>';print_r($data);//exit;
		//echo $data[0]['name'];exit;
		$evenarray = array();
		$oddarray = array();
		foreach($data as $key=>$values)
        {
        	 if($key%2==0)
                {
                	if(is_array($values))
		            {
		                $evenarray[] = $values;
		            }
                }
                else
                {
                	if(is_array($values))
		            {
		                $oddarray[] = $values;
		            }
                }
      	}
      	//echo '<pre>';print_r($evenarray);//exit;
      	//echo '<pre>';print_r($oddarray);exit;
        $safe_date =  Input::get('safe_date');
		for($i=0;$i<count($evenarray);$i++)
		{
			//echo $data[$i]['name'].' '.$data[$i]['value'].'<br>';
			$subtot1_query = DB::select('select * from safeentry where safeentry.item_id =  "'.$evenarray[$i]['name'].'"  AND safeentry.entry_date = "'.$safe_date.'" ');
			$item_amt = $evenarray[$i]['value']*100;
			if($subtot1_query)
			{
				DB::update('update safeentry SET  safeentry.item_amt = "'.$item_amt.'" WHERE safeentry.item_id = "'.$evenarray[$i]['name'].'" AND safeentry.entry_date = "'.$safe_date.'"');
			}
			else
			{
				DB::insert(' INSERT INTO safeentry VALUES ("'.$evenarray[$i]['name'].'", "'.$item_amt.'", "book1", "'.$safe_date.'")');
			}
		}
		for($i=0;$i<count($oddarray);$i++)
		{
			//echo $data[$i]['name'].' '.$data[$i]['value'].'<br>';
			$subtot1_query = DB::select('select * from safeentry where safeentry.item_id =  "'.$oddarray[$i]['name'].'"  AND safeentry.entry_date = "'.$safe_date.'" ');
			$item_amt = $evenarray[$i]['value']*100;
			if($subtot1_query)
			{
				DB::update('update safeentry SET  safeentry.source_id = "'.$oddarray[$i]['value'].'" WHERE safeentry.item_id = "'.$oddarray[$i]['name'].'" AND safeentry.entry_date = "'.$safe_date.'"');
			}
			// else
			// {
			// 	DB::insert(' INSERT INTO safeentry VALUES ("'.$oddarray[$i]['name'].'", "'.$item_amt.'", "book1", "'.$safe_date.'")');
			// }
		}
	}
	public function getSafeCountPrintDate()
	{
		return View::make('mktmgr.safecountprintdate'); 
	}
	public function postSafeCountPrint()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$validator = Validator::make(Input::all(),
            array(
                   'rpt_date'=>'required'
                ),array('rpt_date.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('bookspro-safecountprintdate')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	//echo '<pre>';print_r(Input::all());exit;
	        $rpt_date = Input::get('rpt_date');
	        $str_num_query = DB::select('SELECT misc.groc_num  as str_num FROM misc');
	    	$str_num = $str_num_query[0]->str_num;
	    	$coin_tot = $this->init_safecoin($rpt_date);
        	$curr_tot = $this->init_safecurr($rpt_date);
        	$other_tot = $this->init_safeother($rpt_date);
        	
        	if($coin_tot || $curr_tot || $other_tot)
        	{
        		$grand_tot = $coin_tot + $curr_tot + $other_tot;
        	}
        	else
        	{
        		$grand_tot = 0.00;
        	}
	    	$safe_coin=DB::select('SELECT * FROM safe_coin  WHERE safe_coin.safe_date = "'.$rpt_date.'" ');
	    	$safe_coin_array = json_decode(json_encode($safe_coin), true);
	    	if($safe_coin)
	        	{
	        		$safe_coin_array = json_decode(json_encode($safe_coin), true);
	        	}
	        	else
	        	{
	        		$safe_coin_array = array(array(
	        						'pennies_box' => 0,
	        						'pennies_roll' => 0,
	        						'nickles_box' => 0,
	        						'nickles_roll' => 0,
	        						'dimes_box' => 0,
	        						'dimes_roll' => 0,
	        						'quarters_box' => 0,
	        						'quarters_roll' => 0,
	        						'misc_box' => 0,
	        						'misc_roll' => 0,
	        						'misc_desc' => 0,

	        			));
	        	}
	        	$safe_curr=DB::select('SELECT * FROM safe_curr  WHERE safe_curr.safe_date = "'.$rpt_date.'" ');
	        	if($safe_curr)
	        	{
	        		$safe_curr_array = json_decode(json_encode($safe_curr), true);
	        	}
	        	else
	        	{
	        		$safe_curr_array = array(array(
	        						'ones_box' => 0,
	        						'ones_roll' => 0,
	        						'ones_held' => 0,
	        						'fives_box' => 0,
	        						'fives_roll' => 0,
	        						'fives_held' => 0,
	        						'tens_box' => 0,
	        						'tens_roll' => 0,
	        						'tens_held' => 0,
	        						'twenties_box' => 0,
	        						'twenties_roll' => 0,
	        						'twenties_held' => 0,
	        						'misc1_box' => 0,
	        						'misc1_roll' => 0,
	        						'misc1_held' => 0,
	        						'misc1_desc' => 0,
	        						'misc2_box' => 0,
	        						'misc2_roll' => 0,
	        						'misc2_held' => 0,
	        						'misc2_desc' => 0

	        			));
	        	}
	        	$safe_other=DB::select('SELECT * FROM safe_other  WHERE safe_other.safe_date = "'.$rpt_date.'" ');
	        	if($safe_other)
	        	{
	        		$safe_other_array = json_decode(json_encode($safe_other), true); 
	        	}
	        	else
	        	{
	        		$safe_other_array = array(array(
	        						'food_stamps' => 0,
	        						'merch_fives' => 0,
	        						'merch_tens' => 0,
	        						'merch_twenties' => 0,
	        						'merch_misc' => 0,
	        						'script1' => 0,
	        						'script2' => 0,
	        						'script3' => 0,
	        						'script4' => 0,
	        						'script5' => 0,
	        						'script6' => 0,
	        						'script7' => 0,
	        						'script8' => 0,
	        						'script9' => 0,
	        						'script10' => 0,
	        						'begin_loans' => 0,
	        						'misc1_amt' => 0,
	        						'misc1_desc' => 0,
	        						'misc2_amt' => 0,
	        						'misc2_desc' => 0,
	        						'instr_chrg' => 0,
	        						'pay_adv' => 0,
	        						'night_crew' => 0,
	        						'debit' => 0,
	        						'credit' => 0,
	        						'ebt' => 0,
	        						'giftcard' => 0,
	        						'misc3_amt' => 0,
	        						'misc3_desc' => 0,

	        			));
	        	}
	        //echo '<pre>';print_r($safe_other_array);exit;	 
	        return View::make('mktmgr.safecountprintdata',compact('safe_coin_array','safe_curr_array','safe_other_array','grand_tot','str_num','rpt_date')); 
		}
	}
	public function getSafeReportPrintDate()
	{
		return View::make('mktmgr.safereportprintdate'); 
	}
	public function postSafeReportPrintData()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$validator = Validator::make(Input::all(),
            array(
                   'rpt_date'=>'required'
                ),array('rpt_date.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('bookspro-safereportprintdate')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	//echo '<pre>';print_r(Input::all());exit;
        	$rpt_date = Input::get('rpt_date');
	        $str_num_query = DB::select('SELECT misc.groc_num  as str_num,misc.drug_num  as drug_num  FROM misc');
	    	$str_num = $str_num_query[0]->str_num;
	    	$drug_num = $str_num_query[0]->drug_num;
	    	$safe_bal = $this->get_safe_bal($rpt_date); 
	    	$out_comments = $this->out_comments($rpt_date);
	    	//echo '<pre>';print_r($out_comments);exit;
	    	//$begin_safe = $safe_bal[0]['begin_safe'];
        	$rpt_safe_curs = DB::select('SELECT safeentry.item_id, safeentry.item_amt, safeitems.item_desc, safeitems.item_type FROM safeentry,safeitems WHERE safeentry.item_amt <> 0 AND safeentry.entry_date =  "'.$rpt_date.'" AND safeentry.item_id = safeitems.item_id ORDER BY safeitems.item_type asc,safeitems.item_desc asc');
        	$rpt_safe_array = json_decode(json_encode($rpt_safe_curs), true);
        	//echo '<pre>';print_r($rpt_safe_array);exit;
        	return View::make('mktmgr.safereportprintdata',compact('str_num','drug_num','rpt_date','safe_bal','rpt_safe_array','out_comments'));
        }
	}
	public function get_safe_bal($rpt_date)
	{
		//echo $rpt_date;exit;
		$mainsafe_query = DB::select('SELECT * FROM mainsafe
       WHERE mainsafe.safe_date = "'.$rpt_date.'"');
		if($mainsafe_query)
    	{
    		$mainsafe_array = json_decode(json_encode($mainsafe_query), true);
    	}
    	else
    	{
    		$mainsafe_array = array(array(
		    			'begin_safe' => 0,
		    			'tot_acct' => 0,
		    			'safe_cnt' => 0,
		    			'safe_os' => 0,
		    			'groc_os' => 0,
		    			'drug_os' => 0,
		    			'total' => 0
		    			));
		}
		return $mainsafe_array;
	}
	public function out_comments($end_date)
    {
    	$comment_array = array();
    	$comment_array_final = array();
    	$out_comments = DB::select('SELECT * FROM comment
            WHERE comment.date_stamp = "'.$end_date.'" 
           ORDER BY  comment.date_stamp');
    	$out_comments_array = json_decode(json_encode($out_comments), true);
    	for($i=0;$i<count($out_comments_array);$i++)
    	{
    		$comment_array['date_stamp'] = $out_comments_array[$i]['date_stamp'];
    		$comment_array['comment_line'] = $out_comments_array[$i]['comment_line1'].' '.$out_comments_array[$i]['comment_line2'].' '.$out_comments_array[$i]['comment_line3'].' '.$out_comments_array[$i]['comment_line4'].' '.$out_comments_array[$i]['comment_line5'].' '.$out_comments_array[$i]['comment_line6'].' '.$out_comments_array[$i]['comment_line7'].' '.$out_comments_array[$i]['comment_line8'].' '.$out_comments_array[$i]['comment_line9'];
    		$comment_array_final[] = $comment_array;
    	}
		return $comment_array_final;
    }
}


