<?php

class SignsController extends BaseController
{
	public $buy_more_sign_sw = '';
	public $save_pct_off_save_amt = '';
	public function getSigns()
	{
		return View::make('sign1.signs');
	}
	public function signOptionForStockCode()
	{
		$stock_code = Input::get('stock_code');
		$stock_code = strtoupper($stock_code);
		switch ($stock_code)
	 	  	{
	 	  		case "N" :
	 	  		$gl_dept_query = DB::select('SELECT gl_dept.gl_dept_no, gl_dept.description FROM gl_dept WHERE active_sw = "Y" ORDER BY gl_dept_no');
	 	  		$gl_dept_array = json_decode(json_encode($gl_dept_query), true); 
	 	  		$ad_begin_date_query = DB::select('SELECT DISTINCT(date_requested) FROM ssignrsr WHERE ssignrsr.userid = "AD" ORDER BY ssignrsr.date_requested');
	 	  		$ad_begin_date_array = json_decode(json_encode($ad_begin_date_query), true); 
				    return View::make('sign1.price_change_signs',compact('gl_dept_array','ad_begin_date_array'));
				    break;
			    case "P" :
				    return View::make('sign1.case_signs');
				    break;
			    case "S" :
				    return View::make('sign1.call_remote');
				    break;
			    case "T" :
			    	return View::make('sign1.queued_to_print');
			    	break;
			    case "Z" :
			    	return View::make('sign1.call_reports');
			    	break;
			}	    
		Validator::extend('check_stock_code', function($attribute, $value, $parameters) {
		 	$stock_code = Input::get('stock_code');
			$stock_code = strtoupper($stock_code);
		 	$ssstock = DB::select('SELECT * FROM ssstock WHERE ssstock.active_sw = "Y" AND stock_code!="T" and stock_code = "'.$stock_code.'"');
		$ssstock_array = json_decode(json_encode($ssstock), true); 
		
            if(!empty($ssstock_array)){
                return true;
               }            
            else{
            	return false;
             }
        },'Invalid Stock Code');
        
         $validator = Validator::make(Input::all(),
        array(
           'stock_code'=>'required|check_stock_code',
        ),
        array(
           'stock_code'=>'required|check_sssign_code',
        ));
        
        if($validator->fails())
		{
			return Redirect::route('user-home')
					->withErrors($validator)
					->withInput();
		}
		else
		{
			Validator::extend('check_stock_code', function($attribute, $value, $parameters) {
		 	$stock_code = Input::get('stock_code');
			$stock_code = strtoupper($stock_code);
		 	$sssign = DB::select('SELECT *
          FROM sssign
         WHERE sssign.stock_code = "'.$stock_code.'"
           AND sssign.active_sw        = "Y"
      ORDER BY sssign.sign_id ');
		$sssign_array = json_decode(json_encode($sssign), true);
		
            if(!empty($sssign_array)){
                return true;
               }            
            else{
            	return false;
             }
        },'There are No defined sign formats for the Sign Stock selected.');
        
         $validator = Validator::make(Input::all(),
        array(
           'stock_code'=>'required|check_stock_code',
        ));
			if($validator->fails())
			{
				return Redirect::route('user-home')
						->withErrors($validator)
						->withInput();
			}
			else
			{
				$stock_code = Input::get('stock_code');
				$stock_code = strtoupper($stock_code);
				//echo $stock_code; 
				$sssign = DB::select('SELECT *
          FROM sssign
         WHERE sssign.stock_code = "'.$stock_code.'"
           AND sssign.active_sw        = "Y"
      ORDER BY sssign.sign_id');
		$sssign_array = json_decode(json_encode($sssign), true); 
            return View::make('sign1.sssign_page',compact('sssign_array',$stock_code));
			}
		}
	}
	public function gettagssigns(){
		$user_id = Input::get('user_id');
		$password = Input::get('password');
		$prnt_yet = Input::get('prnt_yet');
		$dayinfo = Input::get('dayinfo');
		$batch_no=0;
	    $today=date("Y-m-d");
	    $start_date=date("Y-m-d");
		$end_date=date("Y-m-d");
		$unixTimestamp = strtotime($today);
        $dayOfWeek = date("l", $unixTimestamp);
        // echo "SELECT * FROM ssignhdr WHERE userid ='".$user_id."'
        //    AND passwd         = '".$password."'
        //    AND date_requested >= '".$start_date."'
        //    AND date_requested <= '".$end_date."'
        //    AND printed_sw     = '".$prnt_yet."'           
        //    AND batch_no       ='".$batch_no."'";exit;
         $usercheck=DB::select("SELECT * FROM ssignhdr WHERE userid ='".$user_id."'
           AND passwd         = '".$password."'
           AND date_requested >= '".$start_date."'
           AND date_requested <= '".$end_date."'
           AND printed_sw     = '".$prnt_yet."'           
           AND batch_no       ='".$batch_no."'");
            if(empty($usercheck)){
               Session::flash("not_print","Invalid UserID or Password  ");
                return View::make('sign1.queued_to_print');
            }

		 // if($prnt_yet == "N"){
   //         Session::flash("not_print","No Tags/Signs for UserId ".$user_id);
   //          return View::make('sign1.queued_to_print');
		 // }
		 else{
             if($dayinfo == "B"){
             	if($dayOfWeek == 'Monday'){
             		$start_date=date('Y-m-d',strtotime($today . "-3 days"));
             	}
             	elseif($dayOfWeek == 'Sunday'){
             		$start_date=date('Y-m-d',strtotime($today . "-2 days"));
             	}
             	else{
             		$start_date=date('Y-m-d',strtotime($today . "-1 days"));
             	}
             }
             	elseif($dayinfo == "T"){
             		$start_date=date("Y-m-d");
			        $end_date=date("Y-m-d");
             	}
             	elseif($dayinfo == "Y"){
                    if($dayOfWeek == 'Monday'){
             		$start_date=date('Y-m-d',strtotime($today . "-3 days"));
             	}
             	else{
             		$start_date=date('Y-m-d',strtotime($today . "-1 days"));
			        
             	}
             	}
             	$login_cur['login_cur']=DB::select("SELECT ssignhdr.userid,ssstock.sign_count,ssignhdr.stock_code, ssignhdr.sign_id,
             		ssignhdr.batch_no,sssign.sign_name,SUM(copies) as sum FROM ssignhdr as ssignhdr  left join sssign as sssign on 
sssign.sign_id=ssignhdr.sign_id left join ssstock as ssstock on ssstock.stock_code=ssignhdr.stock_code WHERE ssignhdr.userid = '".$user_id."'
AND ssignhdr.passwd =  '".$password."' AND ssignhdr.date_requested >= '".$start_date."' 
AND ssignhdr.date_requested <= '".$end_date."' AND ssignhdr.printed_sw
= '".$prnt_yet."' AND ssignhdr.batch_no = '".$batch_no."' GROUP BY ssignhdr.userid,
ssignhdr.stock_code,ssignhdr.sign_id ORDER BY ssignhdr.sign_id");
             	// $login_cur_array = json_decode(json_encode($login_cur), true); 
             	// echo '<pre>';print_r($login_cur_array);exit;
            return View::make('sign1.view_tag_signs',$login_cur,array('today' => $today,'user_id'=> $user_id,'password'=> $password,'prnt_yet'=> $prnt_yet,'start_date'=> $start_date,'end_date'=> $end_date));
             }
         
		}
	

	public function signStockInnerPage($sign_id,$stock_code)
	{		
		//echo 'signStockInnerPage';exit;
   		$work_data = new StdClass();
    	$seq_no = Input::get('seq_no');
        $inp_sign_id = $sign_id;
        $stock_code = $stock_code;
        //echo $inp_sign_id.' '.$stock_code;exit;
                
             
                 
             
            
             
             
                 
             
                $sign_line = DB::select('SELECT * FROM ssformat WHERE ssformat.sign_id = "'.$inp_sign_id.'"  AND ssformat.element_type_cd!= "F" AND ssformat.active_sw = "Y" ORDER BY ssformat.seq_on_screen');
                $sign_line_array = json_decode(json_encode($sign_line), true);
                
             //echo '<pre>';print_r($sign_line_array);exit;
               
                 return $sign_line_array;
                 
      
                        
		
	}
	public function signDir()
	{
		//echo 'signDir';exit;
		return View::make('sign1.signdir');
	}
	public function getEverydayValue()
	{
		return View::make('sign1.everydayvalue');
	}
	public function getDepartmentHour()
	{
		return View::make('sign1.departmenthour');
	}
	public function getWeekday()
	{
		return View::make('sign1.weekday');
	}
	public function getWeekdayHour()
	{
		return View::make('sign1.weekdayhour');
	}
	public function getWeekdaySatHour()
	{
		return View::make('sign1.weekdaysathour');
	}
	public function getFloralSale()
	{
		return View::make('sign1.sale');
	}
	public function getDropPrice()
	{
		return View::make('sign1.dropprice');
	}
	public function getBlank()
	{
		return View::make('sign1.blank');
	}
	public function getBlankBrand()
	{
		return View::make('sign1.blankbrand');
	}
	public function getBogo()
	{
		return View::make('sign1.bogo');
	}
	public function getSaleAD()
	{
		return View::make('sign1.salead');
	}
	public function getDPriceDrop()
	{
		return View::make('sign1.dpricedrop');
	}
	public function getDBlank()
	{
		return View::make('sign1.dblank');
	}
	public function getSeasonal()
	{
		return View::make('sign1.seasonal');
	}
	public function getRaleysDailies()
	{
		return View::make('sign1.raleysdailies');
	}
	public function getPctOff()
	{
		return View::make('sign1.pctoff');
	}
	public function getLocal()
	{
		return View::make('sign1.local');
	}
	public function getBuySave()
	{
		return View::make('sign1.buysave');
	}
	public function getBogo2()
	{
		return View::make('sign1.bogo2');
	}
	public function getB2go()
	{
		return View::make('sign.b2go');
	}
	public function getBuyGet()
	{
		return View::make('sign1.buyget');
	}
	public function getBuyGetFree()
	{
		return View::make('sign1.buygetfree');
	}
	public function getSpecialPromo()
	{
		return View::make('sign1.specialpromo');
	}
	public function getPercentOff()
	{
		return View::make('sign1.percentoff');
	}
	public function getSpecialOff()
	{
		return View::make('sign1.specialoff');
	}
	public function getTextOnly()
	{
		return View::make('sign1.textonly');
	}
	public function getFifthSale()
	{
		return View::make('sign1.fifthsale');
	}
	public function getFifthPriceDrop()
	{
		return View::make('sign1.fifthpricedrop');
	}
	public function geFifthBlank()
	{
		return View::make('sign1.fifthblank');
	}
	public function geFifthSeasonalPriceDrop()
	{
		return View::make('sign1.fifthseasonalpricedrop');
	}
	public function geFifthRaleysDailies()
	{
		return View::make('sign1.fifthraleysdailies');
	}
	public function geFifthPctOff()
	{
		return View::make('sign1.fifthpctoff');
	}
	public function geFifthBuyMoreSave()
	{
		return View::make('sign1.fifthbuymoresave');
	}
	public function geFifthPercentOff()
	{
		return View::make('sign1.fifthpercentoff');
	}
	public function geFifthTextOnly()
	{
		return View::make('sign1.fifthtextonly');
	}
	public function geFifthBogo()
	{
		return View::make('sign1.fifthbogo');
	}
	public function geFifthBuyGet()
	{
		return View::make('sign1.fifthbuyget');
	}
	public function getFifthBuyGetFree()
	{
		return View::make('sign1.fifthbuygetfree');
	}
	public function getSixSale()
	{
		return View::make('sign1.sixsale');
	}
	public function getSixPriceDrop()
	{
		return View::make('sign1.sixpricedrop');
	}
	public function getSixBlank()
	{
		return View::make('sign1.sixblank');
	}
	public function getSixLocal()
	{
		return View::make('sign1.sixlocal');
	}
	public function getSixRaleysDailies()
	{
		return View::make('sign1.sixraleysdailies');
	}

	public function getSixPctOff()
	{
		return View::make('sign1.sixpctoff');
	}
	public function getSixSeasonalPriceDrop()
	{
		return View::make('sign1.sixseasonalpricedrop');
	}

	public function getSixBuySaveMore()
	{
		return View::make('sign1.sixbuysavemore');
	}
	public function getSixBogo()
	{
		return View::make('sign1.sixbogo');
	}
	public function getSixB2go()
	{
		return View::make('sign1.sixb2go');
	}
	public function getSixBuyGet()
	{
		return View::make('sign1.sixbuyget');
	}
	public function getSixBuyGetFree()
	{
		return View::make('sign1.sixbuygetfree');
	}
	public function getSixSpecialPromo()
	{
		return View::make('sign1.sixspecialpromo');
	}
	public function getSixPercentOff()
	{
		return View::make('sign1.sixpercentoff');
	}
	public function getSixTextOnly()
	{
		return View::make('sign1.sixtextonly');
	}
	public function getSevenSale()
	{
		return View::make('sign1.sevensale');
	}
	public function getSevenPriceDrop()
	{
		return View::make('sign1.sevenpricedrop');
	}
	public function getSevenBlank()
	{
		return View::make('sign1.sevenblank');
	}
	public function getSevenRaleysDailies()
	{
		return View::make('sign1.sevenraleysdailies');
	}
	public function getSevenRaleys()
	{
		return View::make('sign1.sevenraleys');
	}
	public function getSevenPctOff()
	{
		return View::make('sign1.sevenpctoff');
	}
	public function getSevenClearance()
	{
		return View::make('sign1.sevenclearance');
	}
	public function getSevenPctOffBeer()
	{
		return View::make('sign1.sevenpctoffbeer');
	}
	public function getSevenBogo()
	{
		return View::make('sign1.sevenbogo');
	}
	public function getSevenB2go()
	{
		return View::make('sign1.sevenb2go');
	}
	public function getSevenBuySave()
	{
		return View::make('sign1.sevenbuysave');
	}
	public function getSeven25PctOff()
	{
		return View::make('sign1.seven25pctoff');
	}
	public function getSeven50PctOff()
	{
		return View::make('sign1.seven50pctoff');
	}
	public function getSeven75PctOff()
	{
		return View::make('sign1.seven75pctoff');
	}
	public function getSevenBlankBrands()
	{
		return View::make('sign1.sevenblankbrands');
	}
	public function getSevenLocal()
	{
		return View::make('sign1.sevenlocal');
	}
	public function getSevenSeasonalPriceDrop()
	{
		return View::make('sign1.sevenseasonalpricedrop');
	}
	public function getYSale()
	{
		return View::make('sign1.ysale');
	}
	public function getYPriceDrop()
	{
		return View::make('sign1.ypricedrop');
	}
	public function getYBlankBrands()
	{
		return View::make('sign1.yblankbrands');
	}
	public function getYRaleysDailys()
	{
		return View::make('sign1.yraleysdailys');
	}
	public function getYRaley()
	{
		return View::make('sign1.yraley');
	}
	public function getYClearance()
	{
		return View::make('sign1.yclearance');
	}
	public function getYNewItem()
	{
		return View::make('sign1.ynewitem');
	}
	public function getYBlank()
	{
		return View::make('sign1.yblank');
	}
	public function getYBogo()
	{
		return View::make('sign1.ybogo');
	}
	public function getYWasNow()
	{
		return View::make('sign1.ywasnow');
	}
	public function getPrintTags()
	{
		return View::make('sign1.printtags');
	}
	public function getRemotelySigns()
	{
		return View::make('sign1.remotelysigns');
	}
	public function getMeat()
	{
		return View::make('sign1.meat');
	}
	public function getSeafood()
	{
		return View::make('sign1.seafood');
	}
	public function getNewSigns()
	{
		$results = DB::select('SELECT type_code,name,gl_dept_number FROM sodept WHERE active_switch = "Y" ORDER BY gl_dept_number');
			$departments = json_decode(json_encode($results), true); 
			return View::make('sign1.newsigns',compact('departments'));
	}
	public function PostNewSigns()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'dept_name_number' =>'required'	
                 ),array(
                   'dept_name_number.required' =>'Department is Required'
                  ));
		if($validator->fails())
        {          
		    return Redirect::route('sign1-new-signs')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {	
        	
			return View::make('sign1.adsignfor');
		}
	}

         public function getReportModQuery() {


        return View::make('sign1.reportmodquery');
    }

    public function getRepModBrowser() {




        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->update_date = trim(Input::get('update_date'));
        $this->update_time = trim(Input::get('update_time'));
        $this->action_cd = trim(Input::get('action_cd'));
        $this->maint_type_cd = trim(Input::get('maint_type_cd'));

        $this->item_code = trim(Input::get('item_code'));
        $this->mstr_fld_no = trim(Input::get('mstr_fld_no'));
        $this->orig_value = trim(Input::get('orig_value'));
        $this->new_value = trim(Input::get('new_value'));

        $paginate_array = array();




        $sql = "SELECT maint_type_cd,slhistry.userid,slhistry.update_date, slhistry.update_time, slhistry.item_code, slhistry.item_desc, slhistry.action_cd, slhistry.mstr_fld_no,seq_no  FROM slhistry WHERE 1=1 ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->update_date != '') {
            $sql.=" AND DATE(update_date)='" . date('Y-m-d', strtotime($this->update_date)) . "' ";
            $paginate_array['update_date'] = $this->update_date;
        }


        if ($this->update_time != '') {
            $sql.=" AND update_time='" . $this->update_time . "' ";
            $paginate_array['update_time'] = $this->update_time;
        }

        if ($this->action_cd != '') {
            $sql.=" AND action_cd='" . strtoupper($this->action_cd) . "' ";
            $paginate_array['action_cd'] = $this->action_cd;
        }

        if ($this->maint_type_cd != '') {
            $sql.=" AND maint_type_cd='" . strtoupper($this->maint_type_cd) . "' ";
            $paginate_array['maint_type_cd'] = $this->maint_type_cd;
        }

        if ($this->item_code != '') {
            $sql.=" AND item_code='" . $this->item_code . "' ";
            $paginate_array['item_code'] = $this->item_code;
        }
        if ($this->orig_value != '') {
            $sql.=" AND orig_value='" . $this->orig_value . "' ";
            $paginate_array['orig_value'] = $this->orig_value;
        }
        if ($this->new_value != '') {
            $sql.=" AND new_value='" . $this->new_value . "' ";
            $paginate_array['new_value'] = $this->new_value;
        }




        $sql.=" ORDER BY userid, update_date, update_time";



        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));



        if (!empty($count_results)) {




            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;

                switch ($action_cd) {
                    case "L":
                        $action_desc = "SIGNIN";
                        break;
                    case "O":
                        $action_desc = "SIGNOUT";
                        break;
                    case "C":
                        $action_desc = "SCANNED";
                        break;
                    case "S":
                        $action_desc = "AUDIT SCAN";
                        break;
                    case "N":
                        $action_desc = "NOT FOUND";
                        break;
                    case "A":
                        $action_desc = "ADD";
                        break;
                    case "U":
                        $action_desc = "UPDATE";
                        break;
                    case "D":
                        $action_desc = "DELETE";
                        break;
                    case "P":
                        $action_desc = "PRICE CHNG";
                        break;
                    case "F":
                        $action_desc = "FM NOTE";
                        break;
                    case "W":
                        $action_desc = "AISLE CHNG";
                        break;
                    case "V":
                        $action_desc = "TAG QTY CHNG";
                        break;
                    default:
                        $action_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->action_desc = $action_desc;

                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;



                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;
            }

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There is no Scan History satisfying the query conditions  ");
            return Redirect::route('sign1-report-mod-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;



        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);


        return View::make('sign1.reportmodbrowser')->with(array('data' => $work_data, 'pagination' => $pagination, 'paginate_array' => $paginate_array));
    }

    public function getRepModView() {
        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $seqn = Input::get('seq_no');

        if ($usrid == '' || $usrid <= 0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('sign1-report-mod-query');
        }


        $std_curs = DB::select(DB::raw("SELECT * FROM slhistry WHERE slhistry.userid = '" . $usrid . "' AND slhistry.seq_no = '" . $seqn . "' "));




        if (empty($std_curs)) {
            Session::flash('alert-danger', "There is no Scan History satisfying the query conditions");
            return Redirect::route('sign1-report-mod-query');
        } else {
            $row_data = $std_curs[0];




            $work_data->userid = $row_data->userid;
            $work_data->update_date = $row_data->update_date;
            $work_data->update_time = $row_data->update_time;
            $work_data->action_cd = $row_data->action_cd;
            $work_data->maint_type_cd = $row_data->maint_type_cd;
            $work_data->item_code = $row_data->item_code;
            $work_data->orig_value = $row_data->orig_value;
            $work_data->new_value = $row_data->new_value;
            $work_data->mstr_fld_no = $row_data->mstr_fld_no;



            $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

            $sql = "SELECT name FROM sluser WHERE userid = '" . $row_data->userid . "'";

            $userdata = DB::select(DB::raw($sql));

            if (!empty($userdata)) {
                $rowdata = $userdata[0];

                $user_name = $rowdata->name;
            }

            $work_data->user_name = $user_name;


            $action_cd = $row_data->action_cd;

            switch ($action_cd) {
                case "L":
                    $action_desc = "SIGNIN";
                    break;
                case "O":
                    $action_desc = "SIGNOUT";
                    break;
                case "C":
                    $action_desc = "SCANNED";
                    break;
                case "S":
                    $action_desc = "AUDIT SCAN";
                    break;
                case "N":
                    $action_desc = "NOT FOUND";
                    break;
                case "A":
                    $action_desc = "ADD";
                    break;
                case "U":
                    $action_desc = "UPDATE";
                    break;
                case "D":
                    $action_desc = "DELETE";
                    break;
                case "P":
                    $action_desc = "PRICE CHNG";
                    break;
                case "F":
                    $action_desc = "FM NOTE";
                    break;
                case "W":
                    $action_desc = "AISLE CHNG";
                    break;
                case "V":
                    $action_desc = "TAG QTY CHNG";
                    break;
                default:
                    $action_desc = "UNKNOWN";
                    break;
            }

            $work_data->action_desc = $action_desc;

            if ($row_data->maint_type_cd == "I") {
                $maint_desc = "IMMEDIATE";
            } else {
                if ($row_data->maint_type_cd == "B") {
                    $maint_desc = "BATCH";
                } else {
                    $maint_desc = " ";
                }
            }

            $work_data->maint_desc = $maint_desc;



            $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row_data->mstr_fld_no . "'";

            $mstflddata = DB::select(DB::raw($sql));

            if (!empty($mstflddata)) {
                $rowdata = $mstflddata[0];

                $mstr_fld_desc = $rowdata->description;

                if ($mstr_fld_desc == '') {
                    $mstr_fld_desc = "N/A";
                }
            }

            $work_data->mstr_fld_desc = $mstr_fld_desc;


            $work_data->usrid = $usrid;
            $work_data->sqno = $seqn;


            //====
        }




        return View::make('sign1.reportmodview')->with('data', $work_data);
    }

    public function getReportModSummaryQuery() {

        $work_data = new StdClass();

        return View::make('sign1.reportmodsummarydetail')->with('data', $work_data);
    }

    public function getReportModSummarybrowse() {


        $work_data = new StdClass();

        $this->userid = trim(Input::get('userid'));
        $this->update_date = trim(Input::get('update_date'));


        Validator::extend('checkuserexists', function($attribute, $value, $parameters) {




            $std_curs = DB::select(DB::raw("SELECT * FROM sluser WHERE userid = '" . $value . "'"));


            if (!empty($std_curs)) {
                return true;
            }


            return false;
        }, 'User Id is not exists');


        $validator = Validator::make(Input::all(), array(
                    'userid' => 'required|integer|checkuserexists',
                    'update_date' => 'required',
        ));




        //print_r($validator);

        if ($validator->fails()) {

            return Redirect::route('sign1-report-mod-summary-query')
                            ->withErrors($validator)
                            ->withInput();
        } else {

            $paginate_array = array();




            $sql = "SELECT * FROM slhistry WHERE 1=1 ";

            if ($this->userid != '') {
                $sql.=" AND userid='" . $this->userid . "' ";
                $paginate_array['userid'] = $this->userid;
            }

            if ($this->update_date != '') {
                $sql.=" AND DATE(update_date)='" . date('Y-m-d', strtotime($this->update_date)) . "' ";
                $paginate_array['update_date'] = $this->update_date;
            }


            $sql.=" ORDER BY seq_no asc";



            $results = DB::select(DB::raw($sql));

            if (!empty($results)) {

                $previous_item = 0;
                $previous_act = "";


                $tot_scanned = 0;
                $tot_aisle_scanned = 0;
                $tot_add = 0;
                $tot_upd = 0;
                $tot_del = 0;
                $tot_updprice = 0;
                $tot_updsprice = 0;
                $tot_fm_notes = 0;
                $tot_notfound = 0;
                $tot_signin = 0;
                $tot_signout = 0;
                $tot_unknown = 0;



                $get_user = DB::select(DB::raw("SELECT * FROM sluser WHERE userid = '" . $this->userid . "'"));


                $user_name = '';
                if (isset($get_user[0])) {
                    if (!empty($get_user[0])) {
                        $rowuser = $get_user[0];
                        $user_name = $rowuser->name;
                    }
                }



                foreach ($results as $row) {

                    $action_cd = $row->action_cd;
                    $item_code = $row->item_code;

                    if ($action_cd == "C" OR $action_cd == "N") {
                        
                    } else {
                        if ($previous_item == $item_code) {
                            
                        } else {
                            $previous_act = "";
                        }
                    }


                    switch ($action_cd) {
                        CASE "L":
                            $tot_signin = $tot_signin + 1;
                        CASE "O":
                            $tot_signout = $tot_signout + 1;
                        CASE "C":
                            $tot_scanned = $tot_scanned + 1;
                        CASE "S":
                            $tot_scanned = $tot_scanned + 1;
                        CASE "W":
                            $tot_aisle_scanned = $tot_aisle_scanned + 1;
                        CASE "N":
                            $tot_scanned = $tot_scanned + 1;
                            $tot_notfound = $tot_notfound + 1;
                        default:
                            if ($action_cd == $previous_act) {
                                
                            } else {
                                switch ($action_cd) {
                                    CASE "A":
                                        $tot_add = $tot_add + 1;
                                    CASE "U":
                                        $tot_upd = $tot_upd + 1;
                                    CASE "D":
                                        $tot_del = $tot_del + 1;
                                    CASE "P":
                                        $tot_updprice = $tot_updprice + 1;
                                    # CASE "S":
                                    #    $tot_updsprice = $tot_updsprice + 1;
                                    CASE "F":
                                        $tot_fm_notes = $tot_fm_notes + 1;
                                    default:
                                        $tot_unknown = $tot_unknown + 1;
                                }
                            }
                    }


                    # s"C"anned and "N"otFound audit records do not affect
                    # previous item logic
                    if ($action_cd == "C" OR $action_cd == "N") {
                        
                    } else {
                        $previous_item = $item_code;
                        $previous_act = $action_cd;
                    }
                }


                $work_data->userid = $this->userid;
                $work_data->update_date = $this->update_date;
                $work_data->user_name = $user_name;
                $work_data->tot_scanned = $tot_scanned;
                $work_data->tot_aisle_scanned = $tot_aisle_scanned;
                $work_data->tot_add = $tot_add;
                $work_data->tot_upd = $tot_upd;
                $work_data->tot_del = $tot_del;
                $work_data->tot_updprice = $tot_updprice;
                $work_data->tot_updsprice = $tot_updsprice;
                $work_data->tot_fm_notes = $tot_fm_notes;
                $work_data->tot_notfound = $tot_notfound;
                $work_data->tot_signin = $tot_signin;
                $work_data->tot_signout = $tot_signout;
                $work_data->tot_unknown = $tot_unknown;
            } else {

                Session::flash('alert-danger', " There is NO Scan History for this User and Date to be printed.");
                return Redirect::route('sign1-report-mod-summary-query')->withInput();
            }



            //}
            //echo "<pre>"; print_r($results); die;
        }


        return View::make('sign1.reportmodsummarybrowser')->with(array('data' => $work_data));
    }

    public function getReportModDetailbrowse() {


        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->update_date = trim(Input::get('update_date'));
        $this->update_time = trim(Input::get('update_time'));
        $this->action_cd = trim(Input::get('action_cd'));
        $this->maint_type_cd = trim(Input::get('maint_type_cd'));

        $this->item_code = trim(Input::get('item_code'));
        $this->mstr_fld_no = trim(Input::get('mstr_fld_no'));
        $this->orig_value = trim(Input::get('orig_value'));
        $this->new_value = trim(Input::get('new_value'));




        $sql = "SELECT maint_type_cd,slhistry.userid,slhistry.update_date, slhistry.update_time, slhistry.item_code, slhistry.item_desc, slhistry.action_cd, slhistry.mstr_fld_no,seq_no,new_value,orig_value  FROM slhistry WHERE 1=1 ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->update_date != '') {
            $sql.=" AND DATE(update_date)='" . date('Y-m-d', strtotime($this->update_date)) . "' ";
            $paginate_array['update_date'] = $this->update_date;
        }


        if ($this->update_time != '') {
            $sql.=" AND update_time='" . $this->update_time . "' ";
            $paginate_array['update_time'] = $this->update_time;
        }

        if ($this->action_cd != '') {
            $sql.=" AND action_cd='" . strtoupper($this->action_cd) . "' ";
            $paginate_array['action_cd'] = $this->action_cd;
        }

        if ($this->maint_type_cd != '') {
            $sql.=" AND maint_type_cd='" . strtoupper($this->maint_type_cd) . "' ";
            $paginate_array['maint_type_cd'] = $this->maint_type_cd;
        }

        if ($this->item_code != '') {
            $sql.=" AND item_code='" . $this->item_code . "' ";
            $paginate_array['item_code'] = $this->item_code;
        }
        if ($this->orig_value != '') {
            $sql.=" AND orig_value='" . $this->orig_value . "' ";
            $paginate_array['orig_value'] = $this->orig_value;
        }
        if ($this->new_value != '') {
            $sql.=" AND new_value='" . $this->new_value . "' ";
            $paginate_array['new_value'] = $this->new_value;
        }




        $sql.=" ORDER BY userid, update_date, update_time";





        $results = DB::select(DB::raw($sql));



        if (!empty($results)) {


            $previous_item = 0;
            $previous_act = "";
            $previous_time = 0;

            $tot_scanned = 0;
            $tot_aisle_scanned = 0;
            $tot_add = 0;
            $tot_upd = 0;
            $tot_del = 0;
            $tot_updprice = 0;
            $tot_updsprice = 0;
            $tot_fm_notes = 0;
            $tot_notfound = 0;
            $tot_signin = 0;
            $tot_signout = 0;
            $tot_unknown = 0;


            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;





                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;



                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;


                $item_code = $row->item_code;

                if ($action_cd == "C" OR $action_cd == "N") {
                    
                } else {
                    if ($previous_item == $item_code) {
                        
                    } else {
                        $previous_act = "";
                        $previous_time = 0;
                    }
                }

                $action_desc = '';

                switch ($action_cd) {
                    CASE "L":
                        $tot_signin = $tot_signin + 1;
                    CASE "O":
                        $tot_signout = $tot_signout + 1;
                    CASE "C":
                        $tot_scanned = $tot_scanned + 1;
                    CASE "S":
                        $tot_scanned = $tot_scanned + 1;
                    CASE "W":
                        $tot_aisle_scanned = $tot_aisle_scanned + 1;
                    CASE "N":
                        $tot_scanned = $tot_scanned + 1;
                        $tot_notfound = $tot_notfound + 1;
                    default:
                        if ($action_cd == $previous_act) {
                            
                        } else {
                            switch ($action_cd) {
                                CASE "A":
                                    $tot_add = $tot_add + 1;
                                    $action_desc = "ADD";
                                CASE "U":
                                    $tot_upd = $tot_upd + 1;
                                    $action_desc = "UPDATE";
                                CASE "D":
                                    $tot_del = $tot_del + 1;
                                    $action_desc = "DELETE";
                                CASE "P":
                                    $tot_updprice = $tot_updprice + 1;
                                    $action_desc = "PRICE CHANGE";
                                # CASE "S":
                                #    $tot_updsprice = $tot_updsprice + 1;
                                CASE "F":
                                    $tot_fm_notes = $tot_fm_notes + 1;
                                    $action_desc = "FM NOTE";
                                default:
                                    $tot_unknown = $tot_unknown + 1;
                                    $action_desc = "UNKNOWN";
                            }
                        }
                }


                if ($item_code == $previous_time AND $action_cd == $previous_act AND $row->update_time == $previous_time) {
                    $action_desc = "";
                }

                $results[$key]->action_desc = $action_desc;

                # Because s"C"anned and "N"otFound audit records are only tallied 
                # and not detailed on the report, do not note them as a previous
                # item
                if ($action_cd == "C" OR $action_cd == "N") {
                    
                } else {
                    $previous_item = $item_code;
                    $previous_act = $action_cd;
                    $previous_time = $row->update_time;
                }
            }

            $work_data->tot_scanned = $tot_scanned;
            $work_data->tot_aisle_scanned = $tot_aisle_scanned;
            $work_data->tot_add = $tot_add;
            $work_data->tot_upd = $tot_upd;
            $work_data->tot_del = $tot_del;
            $work_data->tot_updprice = $tot_updprice;
            $work_data->tot_updsprice = $tot_updsprice;
            $work_data->tot_fm_notes = $tot_fm_notes;
            $work_data->tot_notfound = $tot_notfound;
            $work_data->tot_signin = $tot_signin;
            $work_data->tot_signout = $tot_signout;
            $work_data->tot_unknown = $tot_unknown;
            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There is no Scan History satisfying the query conditions  ");
            return Redirect::route('sign1-report-mod-query')->withInput();
        }





        $work_data->results = $results;

        //echo "<pre>"; print_r($work_data); die;

        return View::make('sign1.reportmoddetailbrowser')->with(array('data' => $work_data));
    }


        public function getReportFmQuery() {
        return View::make('sign1.reportfmquery');
    }

    public function getReportFmBrowser() {


        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->from_date = trim(Input::get('from_date'));
        $this->to_date = trim(Input::get('to_date'));


        $paginate_array = array();




        $sql = "SELECT * FROM slhistry WHERE slhistry.action_cd = 'F'  ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->from_date != '' && $this->to_date != '') {
            $sql.=" AND ( DATE(update_date)  BETWEEN '" . date('Y-m-d', strtotime($this->from_date)) . "' AND '" . date('Y-m-d', strtotime($this->to_date)) . "' ) ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = $this->to_date;
        } elseif ($this->from_date != '') {
            $sql.=" AND DATE(update_date)>='" . date('Y-m-d', strtotime($this->from_date)) . "' ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = '';
        } elseif ($this->to_date != '') {
            $sql.=" AND DATE(update_date)<='" . date('Y-m-d', strtotime($this->to_date)) . "' ";
            $paginate_array['to_date'] = $this->to_date;
            $paginate_array['from_date'] = '';
        }




        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));



        if (!empty($count_results)) {




            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;

                switch ($action_cd) {
                    case "L":
                        $action_desc = "SIGNIN";
                        break;
                    case "O":
                        $action_desc = "SIGNOUT";
                        break;
                    case "C":
                        $action_desc = "SCANNED";
                        break;
                    case "S":
                        $action_desc = "AUDIT SCAN";
                        break;
                    case "N":
                        $action_desc = "NOT FOUND";
                        break;
                    case "A":
                        $action_desc = "ADD";
                        break;
                    case "U":
                        $action_desc = "UPDATE";
                        break;
                    case "D":
                        $action_desc = "DELETE";
                        break;
                    case "P":
                        $action_desc = "PRICE CHNG";
                        break;
                    case "F":
                        $action_desc = "FM NOTE";
                        break;
                    case "W":
                        $action_desc = "AISLE CHNG";
                        break;
                    case "V":
                        $action_desc = "TAG QTY CHNG";
                        break;
                    default:
                        $action_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->action_desc = $action_desc;

                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;



                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;
            }

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There are no FM Notes satisfying the query conditions    ");
            return Redirect::route('sign1-report-fm-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;



        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);


        return View::make('sign1.reportfmbrowser')->with(array('data' => $work_data, 'pagination' => $pagination, 'paginate_array' => $paginate_array));
    }

    public function getReportFmView() {

        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $seqn = Input::get('seq_no');

        if ($usrid == '' || $usrid <= 0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('sign1-report-fm-query');
        }


        $std_curs = DB::select(DB::raw("SELECT * FROM slhistry WHERE slhistry.action_cd = 'F' and slhistry.userid = '" . $usrid . "' AND slhistry.seq_no = '" . $seqn . "' "));




        if (empty($std_curs)) {
            Session::flash('alert-danger', "There are no FM Notes satisfying the query conditions");
            return Redirect::route('sign1-report-fm-query');
        } else {
            $row_data = $std_curs[0];




            $work_data->userid = $row_data->userid;
            $work_data->update_date = $row_data->update_date;
            $work_data->update_time = $row_data->update_time;
            $work_data->action_cd = $row_data->action_cd;
            $work_data->maint_type_cd = $row_data->maint_type_cd;
            $work_data->item_code = $row_data->item_code;
            $work_data->orig_value = $row_data->orig_value;
            $work_data->new_value = $row_data->new_value;
            $work_data->mstr_fld_no = $row_data->mstr_fld_no;



            $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

            $sql = "SELECT name FROM sluser WHERE userid = '" . $row_data->userid . "'";

            $userdata = DB::select(DB::raw($sql));

            if (!empty($userdata)) {
                $rowdata = $userdata[0];

                $user_name = $rowdata->name;
            }

            $work_data->user_name = $user_name;


            $action_cd = $row_data->action_cd;

            switch ($action_cd) {
                case "L":
                    $action_desc = "SIGNIN";
                    break;
                case "O":
                    $action_desc = "SIGNOUT";
                    break;
                case "C":
                    $action_desc = "SCANNED";
                    break;
                case "S":
                    $action_desc = "AUDIT SCAN";
                    break;
                case "N":
                    $action_desc = "NOT FOUND";
                    break;
                case "A":
                    $action_desc = "ADD";
                    break;
                case "U":
                    $action_desc = "UPDATE";
                    break;
                case "D":
                    $action_desc = "DELETE";
                    break;
                case "P":
                    $action_desc = "PRICE CHNG";
                    break;
                case "F":
                    $action_desc = "FM NOTE";
                    break;
                case "W":
                    $action_desc = "AISLE CHNG";
                    break;
                case "V":
                    $action_desc = "TAG QTY CHNG";
                    break;
                default:
                    $action_desc = "UNKNOWN";
                    break;
            }

            $work_data->action_desc = $action_desc;

            if ($row_data->maint_type_cd == "I") {
                $maint_desc = "IMMEDIATE";
            } else {
                if ($row_data->maint_type_cd == "B") {
                    $maint_desc = "BATCH";
                } else {
                    $maint_desc = " ";
                }
            }

            $work_data->maint_desc = $maint_desc;



            $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row_data->mstr_fld_no . "'";

            $mstflddata = DB::select(DB::raw($sql));

            if (!empty($mstflddata)) {
                $rowdata = $mstflddata[0];

                $mstr_fld_desc = $rowdata->description;

                if ($mstr_fld_desc == '') {
                    $mstr_fld_desc = "N/A";
                }
            }

            $work_data->mstr_fld_desc = $mstr_fld_desc;


            $work_data->usrid = $usrid;
            $work_data->sqno = $seqn;


            //====
        }
        return View::make('sign1.reportfmview')->with('data', $work_data);
    }

    public function getReportFmReport() {
        return View::make('sign1.reportfmreportquery');
    }

    public function getReportFmReportBrowser() {


        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->userid = trim(Input::get('userid'));
        $this->from_date = trim(Input::get('from_date'));
        $this->to_date = trim(Input::get('to_date'));


        $paginate_array = array();




        $sql = "SELECT * FROM slhistry WHERE slhistry.action_cd = 'F'  ";

        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }

        if ($this->from_date != '' && $this->to_date != '') {
            $sql.=" AND ( DATE(update_date)  BETWEEN '" . date('Y-m-d', strtotime($this->from_date)) . "' AND '" . date('Y-m-d', strtotime($this->to_date)) . "' ) ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = $this->to_date;
        } elseif ($this->from_date != '') {
            $sql.=" AND DATE(update_date)>='" . date('Y-m-d', strtotime($this->from_date)) . "' ";
            $paginate_array['from_date'] = $this->from_date;
            $paginate_array['to_date'] = '';
        } elseif ($this->to_date != '') {
            $sql.=" AND DATE(update_date)<='" . date('Y-m-d', strtotime($this->to_date)) . "' ";
            $paginate_array['to_date'] = $this->to_date;
            $paginate_array['from_date'] = '';
        }






        $results = DB::select(DB::raw($sql));



        $tot_fm_notes = 0;


        if (!empty($results)) {




            foreach ($results as $key => $row) {


                $user_name = $mstr_fld_desc = $action_desc = $maint_desc = '';

                $sql = "SELECT name FROM sluser WHERE userid = '" . $row->userid . "'";

                $userdata = DB::select(DB::raw($sql));

                if (!empty($userdata)) {
                    $rowdata = $userdata[0];

                    $user_name = $rowdata->name;
                }

                $results[$key]->user_name = $user_name;


                $action_cd = $row->action_cd;

                switch ($action_cd) {
                    case "L":
                        $action_desc = "SIGNIN";
                        break;
                    case "O":
                        $action_desc = "SIGNOUT";
                        break;
                    case "C":
                        $action_desc = "SCANNED";
                        break;
                    case "S":
                        $action_desc = "AUDIT SCAN";
                        break;
                    case "N":
                        $action_desc = "NOT FOUND";
                        break;
                    case "A":
                        $action_desc = "ADD";
                        break;
                    case "U":
                        $action_desc = "UPDATE";
                        break;
                    case "D":
                        $action_desc = "DELETE";
                        break;
                    case "P":
                        $action_desc = "PRICE CHNG";
                        break;
                    case "F":
                        $action_desc = "FM NOTE";
                        break;
                    case "W":
                        $action_desc = "AISLE CHNG";
                        break;
                    case "V":
                        $action_desc = "TAG QTY CHNG";
                        break;
                    default:
                        $action_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->action_desc = $action_desc;

                if ($row->maint_type_cd == "I") {
                    $maint_desc = "IMMEDIATE";
                } else {
                    if ($row->maint_type_cd == "B") {
                        $maint_desc = "BATCH";
                    } else {
                        $maint_desc = " ";
                    }
                }

                $results[$key]->maint_desc = $maint_desc;

                $tot_fm_notes = $tot_fm_notes + 1;

                $sql = "SELECT description FROM slmstfld WHERE field_no = '" . $row->mstr_fld_no . "'";

                $mstflddata = DB::select(DB::raw($sql));

                if (!empty($mstflddata)) {
                    $rowdata = $mstflddata[0];

                    $mstr_fld_desc = $rowdata->description;

                    if ($mstr_fld_desc == '') {
                        $mstr_fld_desc = "N/A";
                    }
                }

                $results[$key]->mstr_fld_desc = $mstr_fld_desc;
            }

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " There are no FM Notes satisfying the query conditions    ");
            return Redirect::route('sign1-report-fm-report-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data->results = $results;

        $work_data->tot_fm_notes = $tot_fm_notes;





        return View::make('sign1.reportfmreportbrowser')->with(array('data' => $work_data, 'paginate_array' => $paginate_array));
    }

    
    
    
      public function getReportBatchQuery() {
        return View::make('sign1.reportbatchquery');
    }

    public function getReportBatchBrowser() {


        $work_data = $results = new StdClass();
        //if (Input::has('accept-submit')) {



        $this->store_no = trim(Input::get('store_no'));
        $this->userid = trim(Input::get('userid'));
        $this->batch_id = trim(Input::get('batch_id'));
        
        $this->status_cd = trim(Input::get('status_cd'));
        $this->batch_type = trim(Input::get('batch_type'));
        
        $this->created_date = trim(Input::get('created_date'));
        $this->last_update_by = trim(Input::get('last_update_by'));
        $this->last_update = trim(Input::get('last_update'));


        $paginate_array = array();




        $sql = "SELECT * FROM slbathdr WHERE 1=1   ";
        
         if ($this->store_no != '') {
            $sql.=" AND store_no='" . $this->store_no . "' ";
            $paginate_array['store_no'] = $this->store_no;
        }

        
        if ($this->userid != '') {
            $sql.=" AND userid='" . $this->userid . "' ";
            $paginate_array['userid'] = $this->userid;
        }
        
        
        if ($this->batch_id != '') {
            $sql.=" AND batch_id='" . $this->batch_id . "' ";
            $paginate_array['batch_id'] = $this->batch_id;
        }
        
        
        
        
        if ($this->status_cd != '') {
            $sql.=" AND status_cd='" . strtoupper($this->status_cd) . "' ";
            $paginate_array['status_cd'] = strtoupper($this->status_cd);
        }
        if ($this->batch_type != '') {
            $sql.=" AND batch_type='" . strtoupper($this->batch_type) . "' ";
            $paginate_array['batch_type'] = strtoupper($this->batch_type);
        }
        
         if ($this->created_date != '') {
            $sql.=" AND DATE(created_date)='" . date('Y-m-d', strtotime($this->created_date)) . "' ";
            $paginate_array['created_date'] = $this->created_date;
        }


        if ($this->last_update_by != '') {
            $sql.=" AND last_update_by='" . strtoupper($this->last_update_by) . "' ";
            $paginate_array['last_update_by'] = $this->last_update_by;
        }

        if ($this->last_update != '') {
            $sql.=" AND DATE(last_update)='" . date('Y-m-d', strtotime($this->last_update)) . "' ";
            $paginate_array['last_update'] = $this->last_update;
        }




        $count_results = DB::select(DB::raw($sql));

        $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 15;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));



        if (!empty($count_results)) {




            foreach ($results as $key => $row) {

                
                        
                        
               $stat_desc = '';

                

                $status_cd = $row->status_cd;

                switch ($status_cd) {
                    case "P":
                        $stat_desc = "PENDING";
                        break;
                    case "A":
                        $stat_desc = "APPLY";
                        break;
                    case "C":
                        $stat_desc = "COMPLETED";
                        break;
                    case "D":
                        $stat_desc = "DELETED";
                        break;
                    case "B":
                        $stat_desc = "BACKOUT";
                        break;
                    case "R":
                        $stat_desc = "REVERSED";
                        break;
                    case "H":
                        $stat_desc = "HOLD";
                        break;
                    case "E":
                        $stat_desc = "ERROR";
                        break;                   
                    default:
                        $stat_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->stat_desc = $stat_desc;

              
            }

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " No Batch records found that match Query value(s) ");
            return Redirect::route('sign1-report-batch-query')->withInput();
        }



        //}
        //echo "<pre>"; print_r($results); die;

        $work_data = $results;



        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);


        return View::make('sign1.reportbatchbrowser')->with(array('data' => $work_data, 'pagination' => $pagination, 'paginate_array' => $paginate_array));
    }
    
    public function getReportBatchView() {

        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $seqn = Input::get('seq_no');

        if ($usrid == '' || $usrid <= 0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('sign1-report-batch-query');
        }


        $std_curs = DB::select(DB::raw("SELECT * FROM slbathdr WHERE userid = '" . $usrid . "' AND seq_no = '" . $seqn . "' "));

      



        if (empty($std_curs)) {
            Session::flash('alert-danger', "No Batch records found that match Query value(s)");
            return Redirect::route('sign1-report-batch-query');
        } else {
            $row_data = $std_curs[0];




            $work_data->store_no = $row_data->store_no;
            $work_data->userid = $row_data->userid;
            $work_data->status_cd = $row_data->status_cd;
            $work_data->created_date = $row_data->created_date;
            $work_data->last_update = $row_data->last_update;
            $work_data->last_update_by = $row_data->last_update_by;
            $work_data->pos_type = $row_data->pos_type;
            $work_data->batch_type = $row_data->batch_type;
            $work_data->batch_id = $row_data->batch_id;
            
                $stat_desc = '';
                $status_cd = $row_data->status_cd;

                switch ($status_cd) {
                    case "P":
                        $stat_desc = "PENDING";
                        break;
                    case "A":
                        $stat_desc = "APPLY";
                        break;
                    case "C":
                        $stat_desc = "COMPLETED";
                        break;
                    case "D":
                        $stat_desc = "DELETED";
                        break;
                    case "B":
                        $stat_desc = "BACKOUT";
                        break;
                    case "R":
                        $stat_desc = "REVERSED";
                        break;
                    case "H":
                        $stat_desc = "HOLD";
                        break;
                    case "E":
                        $stat_desc = "ERROR";
                        break;                   
                    default:
                        $stat_desc = "UNKNOWN";
                        break;
                }

                $work_data->stat_desc = $stat_desc;

                $str_format='';
                
                if($work_data->pos_type== "pos_fuel"){
                    $str_format = "FUEL   "; 
                } else {
                    if($work_data->pos_type=="pos_prim"){
                       $str_format = "GROCERY";
                    }
                }

                     $work_data->str_format = $str_format;

            $work_data->usrid = $usrid;
            $work_data->sqno = $seqn;


            //====
        }
        return View::make('sign1.reportbatchview')->with('data', $work_data);
    }
    
    
    public function getReportBatchUpdate() {


        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $seqn = Input::get('seq_no');

        if ($usrid == '' || $usrid <= 0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('sign1-report-batch-query');
        }


        $std_curs = DB::select(DB::raw("SELECT * FROM slbathdr WHERE userid = '" . $usrid . "' AND seq_no = '" . $seqn . "' FOR UPDATE "));

      



        if (empty($std_curs)) {
            Session::flash('alert-danger', "No Batch records found that match Query value(s)");
            return Redirect::route('sign1-report-batch-query');
        } else {
            $row_data = $std_curs[0];

           


            $work_data->store_no = $row_data->store_no;
            $work_data->userid = $row_data->userid;
            $work_data->status_cd = $row_data->status_cd;
            $work_data->created_date = $row_data->created_date;
            $work_data->last_update = $row_data->last_update;
            $work_data->last_update_by = $row_data->last_update_by;
            $work_data->pos_type = $row_data->pos_type;
            $work_data->batch_type = $row_data->batch_type;
            $work_data->batch_id = $row_data->batch_id;
            
                $stat_desc = '';
                $status_cd1 = $row_data->status_cd;

                switch ($status_cd1) {
                    case "P":
                        $stat_desc = "PENDING";
                        break;
                    case "A":
                        $stat_desc = "APPLY";
                        break;
                    case "C":
                        $stat_desc = "COMPLETED";
                        break;
                    case "D":
                        $stat_desc = "DELETED";
                        break;
                    case "B":
                        $stat_desc = "BACKOUT";
                        break;
                    case "R":
                        $stat_desc = "REVERSED";
                        break;
                    case "H":
                        $stat_desc = "HOLD";
                        break;
                    case "E":
                        $stat_desc = "ERROR";
                        break;                   
                    default:
                        $stat_desc = "UNKNOWN";
                        break;
                }

                $work_data->stat_desc = $stat_desc;

                $str_format='';
                
                if($work_data->pos_type== "pos_fuel"){
                    $str_format = "FUEL   "; 
                } else {
                    if($work_data->pos_type=="pos_prim"){
                       $str_format = "GROCERY";
                    }
                }

                     $work_data->str_format = $str_format;

            $work_data->usrid = $usrid;
            $work_data->sqno = $seqn;


            //====
        }

        
        if(Input::has('change-submit')){
            
             $batch_id = Input::get('batch_id');
             $seq_no= Input::get('seq_no');
             $msg='';
          if($work_data->batch_id!=$batch_id){
           
                DB::table('slbathdr')                                           
                        ->where('seq_no', $seqn)
                        ->update(
                              ['batch_id' => $batch_id]
                );
                $msg='UPDATE SUCCESSFUL';
               
          }
              Session::flash('alert-success', $msg);
                return Redirect::route('sign1-report-batch-browse', array('userid' => $usrid));
            
        }
      

        if (Input::has('update-submit')) {
            
            $msg='Status Code cannot be changed to this Value';
            
            
            Validator::extend('checkstatuscode', function($attribute, $value, $parameters) {

                

                $status_cd = Input::get('status_cd');
                $orig_status_cd = Input::get('status_cd_orig');
                
                $done_sw='';
                
                if($status_cd == "P"){
                    if($orig_status_cd == "D" || $orig_status_cd == "H"){
                       $done_sw = "Y";                          
                     } else { 
                       $done_sw = "N";
                     }
                }
                
                
                if($status_cd == "D"){
                    if($orig_status_cd == "A" || $orig_status_cd == "P"){
                       $done_sw = "Y";                          
                     } else { 
                       $done_sw = "N";
                     }
                }
 
            if($status_cd == "A"){
                    if($orig_status_cd == "P" || $orig_status_cd == "D" || $orig_status_cd == "B"){
                       $done_sw = "Y";                          
                     } else { 
                       $done_sw = "N";
                     }
                }
               
                
                if($status_cd == "R" || $status_cd == "C"){
                    
                       $done_sw = "N";
                    $msg=" Only System can change to R or C ";
                }
           
            
           if($status_cd == "B"){
                    if($orig_status_cd == "C"){
                       $done_sw = "Y";                          
                     } else { 
                       $done_sw = "N";
                     }
                }
 
            
           if($status_cd == "H"){
                    if($orig_status_cd == "P" || $orig_status_cd == "D" || $orig_status_cd == "A"){
                       $done_sw = "Y";                          
                     } else { 
                       $done_sw = "N";
                     }
                }
          
            
                if($done_sw == "N"){
                    return false;   
                }
                
                return true;
            }, $msg);

            $validator = Validator::make(Input::all(), array(
                        'userid' => 'required|integer',
                        'seq_no'=> 'required|integer',
                        'status_cd'=>'required|checkstatuscode'
            ));




            //print_r($validator);

            if ($validator->fails()) {

                return Redirect::route('sign1-report-batch-view', array('userid' => $usrid, 'seq_no' => $seqn))
                                ->withErrors($validator)
                                ->withInput();
            } else {

                
                $status_cd = Input::get('status_cd');
                $batch_id = Input::get('batch_id');
               
                $msg='Status Updated';
                
                if($status_cd=='A'){
                    
                            DB::table('slposhdr')
                        ->where('store_number', 305)                       
                        ->where('batch', $batch_id)
                        ->update(
                              ['reg_applied' => date('Y-m-d')]
                        );
                            $msg='Batch Apply Successful';
                }


                DB::table('slbathdr')
                        ->where('userid', $usrid)                       
                        ->where('seq_no', $seqn)
                        ->update(
                              ['last_update_by' => 'SIGN1', 'status_cd' => $status_cd, 'last_update' => date('Y-m-d'),'last_update_time'=>date('H:i:s')]
                );








                Session::flash('alert-success', $msg);
                return Redirect::route('sign1-report-batch-browse', array('userid' => $usrid));
                
                
                
            }
        }







        return View::make('sign1.reportbatchview')->with('data', $work_data);
    }


    public function postRemotlyRequest()
    {


    		  		
 		 $date_requested = Input::get('Date_requested');
    	 $date_requested = date('Y-m-d',strtotime($date_requested));
    	 $quit_display = "N";
    	 $new_sign_queued = "N";
    	
    		$ws_userid = Input::get('ws_userid');
    		$ws_passwd = Input::get('ws_passwd');
    		$ws_printed_sw = Input::get('ws_printed_sw');
    		
			$status = DB::select("SELECT userid FROM sluser
			WHERE userid='{$ws_userid}' AND passwd='{$ws_passwd}'");
			
			$ws_sign_cnt= DB::select("SELECT COUNT(`userid`)
                 as ws_sign_cnt
                 FROM ssignrsr
                WHERE userid         = '{$ws_userid}'
                  AND passwd         = '{$ws_passwd}'
                  AND date_requested = '{$date_requested}'
                  AND printed_sw     = '{$ws_printed_sw}'");

    	$result1 = json_decode(json_encode($ws_sign_cnt), true);
    		
    	$ws_queue_cnt = DB::select("SELECT COUNT(`userid`)
                 as ws_queue_cnt
                 FROM ssignhdr
                WHERE userid         = '{$ws_userid}'
                  AND passwd         = '{$ws_passwd}'
                  AND date_requested = '{$date_requested}'
                  AND printed_sw     = '{$ws_printed_sw}'");
    	
    	$result2 = json_decode(json_encode($ws_queue_cnt), true);


		if($status  == NULL)
		{
			Session::flash('alert-danger', 'Invalid Userid or Password !');
             //return Redirect::route('sign_option_for_stock_code');
             return View::make('sign1.call_remote');
		}

    	elseif($result1[0]['ws_sign_cnt'] == 0 || $result2[0]['ws_queue_cnt'] == 0)
    	{
    		Session::flash('alert-danger', 'No Signs exist for UserID ! $ws_userid');
                 //return Redirect::route('sign_option_for_stock_code');
                 return View::make('sign1.call_remote');	
    	}
    	else

    	{

    		If($result1[0]['ws_sign_cnt'] > 0 || $result2[0]['ws_queue_cnt'] > 0)
    		{

    			
				$sign_cur	= DB::select("SELECT seq_no, stock_code, upc_number, description, printed_sw, sign_id 
				FROM ssignrsr
				WHERE userid         = '{$ws_userid}'
				AND passwd         = '{$ws_passwd}'
				AND date_requested = '{$date_requested}'
				AND printed_sw     = '{$ws_printed_sw}'
				ORDER BY sign_id, upc_number");

				return View::make('sign1.postRemoteSignRequest',compact('sign_cur','ws_userid','date_requested'));

    		}
    	
    	}	
    }

    public function getRemotlyRequestInfo()
    {
    	//echo '<pre>';print_r(Input::all());exit;
    	$seq_no = Input::get('seq_no');
    	//echo $seq_no;exit;
    	$num_args =  count(Input::all());//exit;
		$sign_id = Input::get('sign_id');
		$sssign_rec = DB::select('SELECT sign_name FROM sssign WHERE sign_id = "'.$sign_id.'"');
		$sign_name = $sssign_rec[0]->sign_name;
		$stock_code = Input::get('stock_code');
    	$value = DB::SELECT("SELECT * FROM ssstock WHERE stock_code = '{$stock_code}'");
    	$final_form_array = $this->do_sign($sign_id,"R",$seq_no,$num_args);
    	//echo '<pre>';print_r($final_form_array);exit;
    	if($num_args == 2)
    	{
    		//return View::make('sign1.printdynamicform',compact('final_form_array','seq_no','sign_name','stock_code','sign_id'));
    		//echo $num_args;exit;
    		$sign_line_array = $this->signStockInnerPage($sign_id,$stock_code);
    		//echo '<pre>';print_r($sign_line_array);exit;
    		return View::make('sign1.printdynamicform',compact('sign_line_array','sign_id','sign_name','stock_code'));
    	}
    	else
    	{
    		return View::make('sign1.dynamicformforlabels',compact('final_form_array','seq_no','sign_name','stock_code','sign_id'));
    	}
    	
    }
    
    public function do_sign($sign_id,$test_sw,$seq_no,$num_args)
    {	
    	//echo $sign_id.' '.$test_sw.' '.$seq_no;exit;

    	$this->GLOBALS['buy_more_sign_sw'] = "N";
		$GLOBALS['buy_get_sign_sw']  = "N";

    	//echo $num_args;exit;
    	$this->buy_more_sign_sw = "N";
		$buy_get_sign_sw  = "N";

		$remote_sign_sw   = "N";

		$inp_test_sw;
		$inp_sign_id;
		$remote_sign_sw;
		$remote_seq_no;
		$ssignrsr_rec = [];
		$ssigntxt_rec = [];

    	//$num_args = func_num_args();
    	if($num_args > 0)
    	{
    		$inp_sign_id = $sign_id;
    		if($num_args == 2)
    		{
    			$inp_test_sw  = 'Y';	
    		}
    	}
    	if($num_args >= 3)
    	{	
    		$inp_test_sw       = $test_sw;
    		$inp_sign_id       = $sign_id;
    		$remote_sign_sw    = "Y";
            $remote_seq_no     = $seq_no;
    	}
    	if($remote_sign_sw == "Y") 
	 	{
 
 
	 		//echo $remote_seq_no;exit;
 
	 		//echo "SELECT * FROM ssignrsr  WHERE seq_no  = '{$remote_seq_no}'";exit;
	 		$ssignrsr_rec = DB::select("SELECT * FROM ssignrsr  WHERE seq_no  = '{$remote_seq_no}'");
		 	$ssignrsr_rec = json_decode(json_encode($ssignrsr_rec), true);
		 	//echo '<pre>';print_r($ssignrsr_rec);exit;
 
		 	$userid = @$row->$ssignrsr_rec[0]["userid"];    
	  		$passwd = @$row->$ssignrsr_rec[0]["passwd"];
	 		$sku_no = @$row->$ssignrsr_rec[0]["item_number"];
 
		 	$userid = $ssignrsr_rec[0]["userid"];    
	  		$passwd = $ssignrsr_rec[0]["passwd"];
	 		$sku_no = $ssignrsr_rec[0]["item_number"];
 
	 		//echo $userid;echo $passwd;echo $sku_no;exit;
			
			if($sku_no > 0 )
     		{
     		$ssigntxt_rec = DB::Select("SELECT tag_line_1, tag_line_2, sign_line_1, sign_line_2, sign_line_3, sign_line_4 FROM ssigntxt
                    WHERE sku_no  = '{$sku_no}'");
     		$ssigntxt_rec = json_decode(json_encode($ssigntxt_rec), true);
     		//print_r($ssigntxt_rec);exit;
     		//echo '<pre>';print_r($ssigntxt_rec);exit;
     		//echo '<pre>';print_r($ssigntxt_rec);exit;
			if($ssigntxt_rec)
			{

				//echo "n";exit;
				//$ssigntxt_fnd_sw = "N";
			}
		 	/*else
			{
				echo "y";exit;
				//$ssigntxt_fnd_sw = "Y";

				//echo "n";exit;
				$ssigntxt_fnd_sw = "N";
			}*/
		 	else
			{
				//echo "y";exit;
				$ssigntxt_fnd_sw = "Y";

			}
			//echo $ssigntxt_fnd_sw;exit;
			// Update prt if Remote_sign_sw is Y
			DB::update('UPDATE ssignrsr
		         SET printed_sw = "Y"
		         WHERE ssignrsr.seq_no = "'.$remote_seq_no.'" ');
     	}

	 	}
	 	else
	 	{
             
			/*SELECT user INTO ssignhdr_rec.userid
			FROM   systables
			WHERE  tabid = 1

			SELECT passwd
			INTO ssignhdr_rec.passwd
			FROM sluser
			WHERE userid=ws_user_id

			IF status = NOTFOUND THEN
			LET ssignhdr_rec.userid = upshift(ssignhdr_rec.userid)
			LET ssignhdr_rec.passwd = ssignhdr_rec.userid
			END IF*/
	 	}

			# get all sign elements that are NOT a fixed message
			$signs_line = DB::select("SELECT *
			        FROM ssformat 
			       WHERE sign_id            = '{$inp_sign_id}'
			         AND element_type_cd   != 'F'
			         AND active_sw = 'Y'
			    ORDER BY seq_on_screen
			");
			 $signs_line = json_decode(json_encode($signs_line), true); 
			 //echo '<pre>';print_r($signs_line);exit;
			 $final_form_array = array();
			foreach($signs_line as $sign_line)
    	 	{
    	 		//echo '<pre>';print_r($sign_line);exit;
    	 		$screen_rec["prompt"] = trim($sign_line["element_prompt"]);
    	 		$prompt[] = trim($sign_line["element_prompt"]); 
    	 		$form_array['prompt'] = trim($sign_line["element_prompt"]);
    	 		$form_array['max_length'] = $sign_line["max_length"];
    	 		$form_array['required_sw'] = $sign_line["required_sw"];
    	 		$form_array['element_type_cd'] = $sign_line["element_type_cd"];
    	 		$form_array['special_proc_cd'] = $sign_line["special_proc_cd"]; 
    	 		//$form_array['ssigntxt_fnd_sw'] = $ssigntxt_fnd_sw; 
    	 		if($inp_test_sw == "Y")  
				{
					
					$this->load_test_sign($sign_line);	
				} 
				else{
					if($remote_sign_sw == "Y")
					{

						$form_array['sign_data'] = implode('',$this->load_sign($sign_line,$ssigntxt_rec,$ssignrsr_rec));
					}
				}
				$final_form_array[] = $form_array;
    	 	}
    	 	//echo '<pre>';print_r($prompt);
    	 	//echo '<pre>';print_r($final_form_array);exit;

			 # get all sign elements that are fixed messages
			  $printed_msg = DB::Select("SELECT * FROM ssformat 
			       	 WHERE sign_id          = '{$inp_sign_id}'
			         AND element_type_cd     = 'F'
			         AND active_sw  = 'Y'
			    ORDER BY seq_on_screen");
    	 	 $printed_msg = json_decode(json_encode($printed_msg), true);
    	 	 //echo '<pre>';print_r($printed_msg);exit;
    	return $final_form_array;
    }
    # Load data from remote sign request
    public function load_sign($w_record,$ssigntxt_rec,$ssignrsr_rec)
    {
    	// echo '<pre>';print_r($w_record);
    	// echo '<pre>';print_r($ssigntxt_rec);
    	// echo '<pre>';print_r($ssignrsr_rec);
    	//echo $ssigntxt_fnd_sw;exit;
    	//exit; 
    		$screen_rec = array();
		
			if($w_record['special_proc_cd'] == "T1")
			{
				 if($ssigntxt_rec)
				 {
				 	if(strlen($ssigntxt_rec[0]["tag_line_1"]) > 1)
					 {
					 	$screen_rec['sign_data'] = $ssigntxt_rec[0]["tag_line_1"];
					 }
					 else
					 {
					 	$desc_len_err_sw = "N";
					 	$screen_rec['sign_data'] = $ssignrsr_rec[0]['description'];
					 	//echo $screen_rec['sign_data'];exit;
					 	$data_len = strlen($ssignrsr_rec[0]['description']);
					 	if($data_len > $w_record['max_length'])
					 	{
					 		$desc_len_err_sw = "Y";
					 		$ws_max_desc_len = $w_record['max_length'];
					 	}
					 }
				} 
					 else
					 {
					 	$desc_len_err_sw = "N";
					 	$screen_rec['sign_data'] = $ssignrsr_rec[0]['description'];
					 	//echo $screen_rec['sign_data'];exit;
					 	$data_len = strlen($ssignrsr_rec[0]['description']);
					 	if($data_len > $w_record['max_length'])
					 	{
					 		$desc_len_err_sw = "Y";
					 		$ws_max_desc_len = $w_record['max_length'];
					 	}
					 }
				
				 
			}

			if($w_record['special_proc_cd'] == "T2")
			{
				if($ssigntxt_rec)
				{
					if(strlen($ssigntxt_rec[0]["tag_line_2"]) > 1)
					 {
					 	$screen_rec['sign_data'] = $ssigntxt_rec[0]["tag_line_2"];
					 }
				}
				 	
			}

			if($w_record['special_proc_cd'] == "D1")
			{
				//echo '<pre>';print_r($ssigntxt_rec).'<br>';//exit; 
				if($ssigntxt_rec)
				{
				if(strlen($ssigntxt_rec[0]["sign_line_1"]) > 1)
				 {
				 	$screen_rec['sign_data'] = $ssigntxt_rec[0]["sign_line_1"];
				 }
				} 	
			}

			if($w_record['special_proc_cd'] == "D2")
			{
				if($ssigntxt_rec)
				{
				if(strlen($ssigntxt_rec[0]["sign_line_2"]) > 1)
				 {
				 	$screen_rec['sign_data'] = $ssigntxt_rec[0]["sign_line_2"];
				 }
				 else
				 {
				 	$desc_len_err_sw = "N";
				 	$screen_rec['sign_data'] = $ssignrsr_rec[0]['description'];
				 	//echo $screen_rec['sign_data'];exit;
				 	$data_len = strlen($ssignrsr_rec[0]['description']);
				 	if($data_len > $w_record['max_length'])
				 	{
				 		$desc_len_err_sw = "Y";
				 		$ws_max_desc_len = $w_record['max_length'];
				 	}

				 }
				 
				} 
				 else
				 {
				 	$desc_len_err_sw = "N";
				 	$screen_rec['sign_data'] = $ssignrsr_rec[0]['description'];
				 	//echo $screen_rec['sign_data'];exit;
				 	$data_len = strlen($ssignrsr_rec[0]['description']);
				 	if($data_len > $w_record['max_length'])
				 	{
				 		$desc_len_err_sw = "Y";
				 		$ws_max_desc_len = $w_record['max_length'];
				 	}

				 }
				 	
			}

			if($w_record['special_proc_cd'] == "D3")
			{
				if($ssigntxt_rec)
				{
				if(strlen($ssigntxt_rec[0]["sign_line_3"]) > 1)
				 {
				 	$screen_rec['sign_data'] = $ssigntxt_rec[0]["sign_line_3"];
				 }
				 }	
			}

			if($w_record['special_proc_cd'] == "D4")
			{
				if($ssigntxt_rec)
				{
				if(strlen($ssigntxt_rec[0]["sign_line_4"]) > 1)
				 {
				 	$screen_rec['sign_data'] = $ssigntxt_rec[0]["sign_line_4"];
				 }
				} 	
			}

			if($w_record['special_proc_cd'] == "SZ")
			{
				
				$screen_rec['sign_data'] = $ssignrsr_rec[0]["size"]; 	
			}

			if($w_record['special_proc_cd'] == "PC")
			{
				
				$screen_rec['sign_data'] = $ssignrsr_rec[0]["upc_number"]; 	
			}

			if($w_record['special_proc_cd'] == "BQ")
			{
				
				$this->buy_more_sign_sw = "Y"; 	
			}

			if($w_record['special_proc_cd'] == "for_qty")
			{
				
				if($ssignrsr_rec[0]["for_qty"] == 1)
				{
				  $screen_rec['sign_data'] = ''; 
				}
				else
				{
					if($this->buy_more_sign_sw == "Y")
					{
					    # On Buy More Save More signs, if the current price has a
				        # For Qty greater than 1, divide sale price by for qty and
				        # and set for qty to 1
				        $orig_for_qty =  $ssignrsr_rec[0]["for_qty"];
				        $screen_rec['sign_data'] = '';
					}
					else
					{
						$screen_rec['sign_data'] = $ssignrsr_rec[0]["for_qty"];

					}
				} 	
			}

			 # "SP" = sale price
   			 # "GS" = price for two gallon of milk

			if($w_record['special_proc_cd'] == "SP" ||
			   $w_record['special_proc_cd'] == "S1" ||
			   $w_record['special_proc_cd'] == "GS")
			{
				if($this->buy_more_sign_sw == "Y")
				{
					if($orig_for_qty > 0)
					{
				   # On Buy More Save More signs, set sale price to a single unit price
					$orig_sale_price = 	$ssignrsr_rec[0]['sale_price'];	
					$single_sale_price = $ssignrsr_rec[0]['sale_price']/$orig_for_qty;
					$screen_rec["sign_data"] = $single_sale_price;
					}
					else
					{
						$screen_rec["sign_data"] = $ssignrsr_rec[0]['sale_price'];	
					}
				}
				else
				{
					$screen_rec["sign_data"] = $ssignrsr_rec[0]['sale_price'];	
				}
			}
			# "RP" = reg or msrp price
   			# "GP" = price for one gallon of milk
   			# "WP" = WAS price, which used to be the regular price
			if($w_record['special_proc_cd'] == "RP" ||
			   $w_record['special_proc_cd'] == "GP" ||
			   $w_record['special_proc_cd'] == "WP")
			{
				if($ssignrsr_rec[0]['regular_price'] > 0 )
				{
					$screen_rec["sign_data"] = $ssignrsr_rec[0]['regular_price'];
				}
			}

			if ($w_record['special_proc_cd'] == "SA") {

				if($ssignrsr_rec[0]['regular_price'] !== " " ){

					if($ssignrsr_rec[0]['for_qty'] == 0){

							$ssignrsr_rec[0]['for_qty'] = 1;	
					}
						
						//echo $ssignrsr_rec[0]['sale_price'];exit;
						$screen_rec["sign_data"] = (($ssignrsr_rec[0]['regular_price']*$ssignrsr_rec[0]['for_qty']) - $ssignrsr_rec[0]['sale_price'] );
						if($screen_rec["sign_data"]<0)
						{
							$screen_rec["sign_data"] = (-1)*($screen_rec["sign_data"]);
						}
					# set the following fields so user does not have to tag down to 
         			# the save amount field to get these populated.  If these dont get

         			# populated, Save Amount does not print correctly on sign	
         				//echo $ssignrsr_rec[0]['for_qty'];exit;	
						$save_for_qty    = $ssignrsr_rec[0]['for_qty'];
         				$save_reg_price  = $ssignrsr_rec[0]['regular_price']; 
        			    $save_sale_price = $ssignrsr_rec[0]['sale_price'];

         			# populated, Save Amount does not print correctly on sign		
					
					 $GLOBALS['save_for_qty']   = $ssignrsr_rec[0]['for_qty'];
					 	$screen_rec['save_for_qty'] = $ssignrsr_rec[0]['for_qty'];
					 //echo  $GLOBALS['save_for_qty'] ;exit;
         			# populated, Save Amount does not print correctly on sign	
         				//echo $ssignrsr_rec[0]['for_qty'];exit;	
						

         				$GLOBALS['save_reg_price']  = $ssignrsr_rec[0]['regular_price']; 
        			    $GLOBALS['save_sale_price'] = $ssignrsr_rec[0]['sale_price'];
				}
				
			}

			if ($w_record['special_proc_cd'] == 10 ||
				$w_record['special_proc_cd'] == 30 || 
				$w_record['special_proc_cd'] == 25 ||
				$w_record['special_proc_cd'] == 50 || 
				$w_record['special_proc_cd'] == 75 ) {
				//echo 'special_proc_cd_30';exit;
				# This logic can handle both...
				# 1. Price of each item when 6 are purchased at 10% or 30% off
				#    The 10% and 30% price is based off the sale price, but the total
				#    save amount is based off of the Everyday (reg) price
				# 2. 25,50,75 percent of regular price to calculate clearance price
				$work_pct = $w_record['special_proc_cd'];
				//echo $work_pct.'<br>'; 
				$pct_off  = $work_pct/100;
				//echo $pct_off.'<br>'; exit;
				if ($w_record['special_proc_cd'] == 10 ||
					$w_record['special_proc_cd'] == 30 ) {
					//echo 'special_proc_cd_30';exit;
						# Because we multiply by 6, no rounding on these prices
					$save_pct_off_price = (($ssignrsr_rec[0]['sale_price'])-($ssignrsr_rec[0]['sale_price'] * $pct_off));

					$work_price = $ssignrsr_rec[0]['regular_price'] - $save_pct_off_price;
					if($work_price<0)
					{
						$work_price = (-1)*($work_price);
					}
					$this->save_pct_off_save_amt = $work_price * 6;
					//echo $this->save_pct_off_save_amt;exit;
					}
					else{
						if ($w_record['special_proc_cd'] == 50 ) {
							$save_pct_off_price = (($ssignrsr_rec[0]['regular_price'] * $pct_off) + .005);
							$this->save_pct_off_save_amt = ($ssignrsr_rec[0]['regular_price'] - $save_pct_off_price);
							//echo $this->save_pct_off_save_amt;exit;
						}
						else{
							$this->save_pct_off_save_amt = ($ssignrsr_rec[0]['regular_price'] * $pct_off);
							$save_pct_off_price    = ($ssignrsr_rec[0]['regular_price'] - $this->save_pct_off_save_amt);
							
							//echo $this->save_pct_off_save_amt;exit;
                                        
						}

					}

					$screen_rec["sign_data"] = number_format((float)$save_pct_off_price, 2, '.', '');	
			}

			if ($w_record['special_proc_cd'] == "S3") {
					$screen_rec["sign_data"] = number_format((float)$this->save_pct_off_save_amt, 2, '.', '');	
			}
			if ($w_record['special_proc_cd'] == "CR" ||
				$w_record['special_proc_cd'] == "CV" ) {
				$screen_rec["sign_data"] = $ssignrsr_rec[0]['crv_sw'];	
			}
			if ($w_record['special_proc_cd'] == "IC") {
					$screen_rec["sign_data"] = $ssignrsr_rec[0]['item_number'];	
			}
			if ($w_record['special_proc_cd'] == "DS") {
					$screen_rec["sign_data"] = $ssignrsr_rec[0]['distributor'];	
			}
			if ($w_record['special_proc_cd'] == "DP") {
					$screen_rec["sign_data"] = $ssignrsr_rec[0]['gl_dept_no'];	
			}
			if ($w_record['special_proc_cd'] == "SD") {
					$screen_rec["sign_data"] = $ssignrsr_rec[0]['sign_date'];	
			}
			if ($w_record['special_proc_cd'] == "LB") {
					$screen_rec["sign_data"] = "N";	
			}
			//$screen_rec['ssigntxt_fnd_sw'] = '^'.$ssigntxt_fnd_sw;
			//$screen_rec["ssigntxt_fnd_sw"] = $ssigntxt_fnd_sw;
			return $screen_rec;
			//$screen_rec['screen_rec'] = $screen_rec;
			
			//$screen_rec1[] = $screen_rec;
			//echo '<pre>';print_r($screen_rec);exit;
	}
	public function load_test_sign()
	{

	}
	//This function outputs the current list to the printer.save_sign_data().
	public function save_sign_data()
	{
		//exit;
		// if(Input::has('seq_no'))
		// {
		// 	echo '<pre>';print_r(Input::all());
		// }
		// echo 'Exit';exit;
		$seq_no = Input::get('seq_no');
		$hdr_seq_no = $this->insert_ssignhdr($seq_no);
		
		//$use_prcxy_data_sw = "N";
		$special_proc_cd = Input::get('special_proc_cd');
		$print_data = Input::get('sign_line');

		$ssigntxt_fnd_sw = Input::get('ssigntxt_fnd_sw');

		$stock_code = Input::get('stock_code');	
		$seq_no = Input::get('seq_no');
		$stock_code = Input::get('stock_code');
		//echo $stock_code;exit;
		$sign_id = Input::get('sign_id');
		//echo $sign_id;exit;
		//echo '<pre>';print_r($special_proc_cd);exit;
		$ssigntxt_rec['sign_line_1'] = '';
		$ssigntxt_rec['sign_line_2'] = '';
		$ssigntxt_rec['sign_line_3'] = '';
		$ssigntxt_rec['sign_line_4'] = '';
		$ssigntxt_rec['tag_line_1'] = '';
		$ssigntxt_rec['tag_line_2'] = '';
		/*echo "SELECT *
			        FROM ssformat 
			       WHERE sign_id            = '{$sign_id}'
			         AND element_type_cd   != 'F'
			         AND active_sw = 'Y'
			    ORDER BY seq_on_screen";exit;*/
		$signs_line = DB::select("SELECT *
			        FROM ssformat 
			       WHERE sign_id            = '{$sign_id}'
			         AND element_type_cd   != 'F'
			         AND active_sw = 'Y'
			    ORDER BY seq_on_screen
			");
		$signs_line = json_decode(json_encode($signs_line), true);
		//echo '<pre>';print_r($signs_line);exit;
	 	for($i = 0; $i < count($special_proc_cd); $i++) 
	 	{
	 		# Item Description
            # Set line 1 thru 4 in sign text master table (ssigntxt)
            # for re-use the next time a sign or tag is printed for current sku
	 		//echo $i;exit;
	 		switch ($special_proc_cd[$i]) 
	 		{
	 			case 'D1':
	 				$ssigntxt_rec['sign_line_1'] = $print_data[$i];
	 				break;
	 			case 'D2':
	 				$ssigntxt_rec['sign_line_2'] = $print_data[$i];
	 				break;
	 			case 'D3':
	 				$ssigntxt_rec['sign_line_3'] = $print_data[$i];
	 				break;
	 			case 'D4':
	 				$ssigntxt_rec['sign_line_4'] = $print_data[$i];
	 				break;
	 			case 'T1':
	 				$ssigntxt_rec['tag_line_1'] = $print_data[$i];
	 				break;
	 			case 'T2':
	 				$ssigntxt_rec['tag_line_2'] = $print_data[$i];
	 				break;
				default:
	 				# code...
	 				break;
	 		}
	 		//echo '<pre>';print_r($signs_line[$i]);exit;
	 		//echo '<pre>';print_r($print_data[$i]);exit;

	 		

	 		$this->config_n_print($signs_line[$i], $print_data[$i],$hdr_seq_no,$special_proc_cd ,$print_data,$sign_id);

	 	}
	 	$printed_msg = DB::Select("SELECT * FROM ssformat 
			       	 WHERE sign_id          = '{$sign_id}'
			         AND element_type_cd     = 'F'
			         AND active_sw  = 'Y'
			    ORDER BY seq_on_screen");
    	$prt_msg_rec = json_decode(json_encode($printed_msg), true);
    	//echo '<pre>';print_r($prt_msg_rec);exit;
    	for($i = 0;$i<count($prt_msg_rec);$i++)
    	{
    		$print_data = $prt_msg_rec[$i]['element_prompt'];
    		//echo '<pre>';print_r($special_proc_cd);exit;
    		//echo $sign_id;exit;
    		//$this->config_n_print($prt_msg_rec[$i], $print_data,$hdr_seq_no,$special_proc_cd ,NULL,$sign_id );
    	}
	 	//$stock_code = str_split($special_proc_cd[$i]);
        if($stock_code == "N" || $stock_code == "S")
        {
	 		$ssignrsr_rec = DB::select("SELECT * FROM ssignrsr  WHERE seq_no  = '{$seq_no}'");
		 	$ssignrsr_rec = json_decode(json_encode($ssignrsr_rec), true);
		 	//echo '<pre>';print_r($ssignrsr_rec);exit;
		 	// $userid = $ssignrsr_rec[0]["userid"];    
	  		// 	$passwd = $ssignrsr_rec[0]["passwd"];
	 		$sku_no = $ssignrsr_rec[0]["item_number"];
	 		$ssigntxt_rec['sku_no'] = $sku_no;
			$ssigntxt_rec['last_updated_by'] = "SSTYPSGN";
			$ssigntxt_rec['last_update'] = date('Y-m-d H:i:s');
			//echo '<pre>';print_r($ssigntxt_rec);exit;
			//echo $sku_no;exit;
	    	if($sku_no > 0 )
	 		{
	     		$ssigntxt_query = DB::Select("SELECT tag_line_1, tag_line_2, sign_line_1, sign_line_2, sign_line_3, sign_line_4 FROM ssigntxt
	                    WHERE sku_no  = '{$sku_no}'");
	     		$ssigntxt_query = json_decode(json_encode($ssigntxt_query), true);
	 			//echo '<pre>';print_r($ssigntxt_rec);exit;
				if($ssigntxt_query)
				{
					//$this->update_ssigntxt($ssigntxt_rec,$sku_no);					
				}
			 	else
				{
					//$this->insert_ssigntxt($ssigntxt_rec,$sku_no);
				}
			}

		}
		
	 		$ssignrsr_rec = DB::select("SELECT * FROM ssignrsr  WHERE seq_no  = '{$seq_no}'");
		 	$ssignrsr_rec = json_decode(json_encode($ssignrsr_rec), true);
		 	//echo '<pre>';print_r($ssignrsr_rec);exit;
		 	if(!empty($ssignrsr_rec))
		 	{
		 		//echo '<pre>';print_r($ssignrsr_rec);exit;
		 		$sku_no = $ssignrsr_rec[0]["item_number"];
				$ssigntxt_rec['sku_no'] = $sku_no;
				$ssigntxt_rec['last_updated_by'] = "SSTYPSGN";
				$ssigntxt_rec['last_update'] = date('Y-m-d H:i:s');
				if($sku_no > 0 )
		 		{
		     		$ssigntxt_query = DB::Select("SELECT tag_line_1, tag_line_2, sign_line_1, sign_line_2, sign_line_3, sign_line_4 FROM ssigntxt
		                    WHERE sku_no  = '{$sku_no}'");
		     		$ssigntxt_query = json_decode(json_encode($ssigntxt_query), true);
		 			if($ssigntxt_query)
					{
						$this->update_ssigntxt($ssigntxt_rec,$sku_no);					
					}
				 	else
					{
						$this->insert_ssigntxt($ssigntxt_rec,$sku_no);
					}
				}
		 	}	
		 	//exit;
		 	// $userid = $ssignrsr_rec[0]["userid"];    
	  		// 	$passwd = $ssignrsr_rec[0]["passwd"];

	 			
		//}
		// else
		// {
		// 	//echo 'Stock Code A to G and Y';
		// 	// for($i = 0; $i < count($special_proc_cd); $i++) 
	 // 	// 	{
	 // 	// 		$this->config_n_print($signs_line[$i], $print_data[$i]);
	 // 	// 	}
		// }
		 	// Need to redirect to same dynamic form with parameters.
		 	return Redirect::route('user-home');
 
	}
	public function insert_ssigntxt($ssigntxt_rec,$sku_no)
	{
		//echo '<pre>';print_r($ssigntxt_rec);exit;
		//echo 'Insert';
		DB::table('ssigntxt')->insert($ssigntxt_rec);
		//$ssigntxt_fnd_sw = "Y";
		//echo $ssigntxt_fnd_sw;exit;
	}
	public function update_ssigntxt($ssigntxt_rec,$sku_no)
	{
		//echo 'update';
		//echo $sku_no;exit;
		//echo '<pre>';print_r($ssigntxt_rec);exit;
		$ssigntxt_update = DB::table('ssigntxt')
            //->where('item_id', $pot_item_id[$i])
            ->where('sku_no', $sku_no)
           ->update($ssigntxt_rec);  
		//$ssigntxt_fnd_sw = "Y";
		//echo $ssigntxt_fnd_sw;exit;
	}



public function config_n_print($w_formats, $w_scrn_data,$hdr_seq_no,$special_proc_cd,$print_data = NULL,$sign_id){
	
		
		 
		 $key = array_search('FQ', $special_proc_cd);
         
                 $save_for_qty = $print_data[$key];

              if(in_array('FQ', $w_formats)) {   
                	//	echo 'im here with ';
                 $save_for_qty =  $w_scrn_data;
	                //echo $save_for_qty.'value';
	                 if ($save_for_qty < 2) {
	                 	$save_for_qty = 0;
	                //echo 'assign to 0'.$save_for_qty;exit;
	                 }
	            }
               elseif (in_array('GS', $w_formats)) {
                 		
                 		$save_for_qty = 2;
                 		//echo 'finally im here ';exit;               
                 }

         $key = array_search('BQ', $special_proc_cd);
     	$save_buy_qty = $print_data[$key];
		//echo '<pre>';print_r($print_data);exit;
		$ssigndtl_rec['print_text'] = $w_scrn_data;
		$sssign_rec= DB::select("SELECT * FROM sssign WHERE sign_id = '{$sign_id}'");
        $sssign_rec = json_decode(json_encode($sssign_rec), true);
		//$seq_no = 0;
		// $seq_no_query = DB::select('SELECT max(seq_no) as seq_no FROM ssigndtl order by seq_no desc');	
		// $seq_no = $seq_no_query[0]->seq_no+1;
		//$ssigndtl_rec['seq_no'] = $seq_no+$i;
		//echo $hdr_seq_no.'<br>';
		//echo $w_formats['y_coord'];exit;
		$ssigndtl_rec['hdr_seq_no'] = $hdr_seq_no;
		$ssigndtl_rec['x_coord'] = $w_formats['x_coord'];
		$ssigndtl_rec['y_coord'] = $w_formats['y_coord'];
        $ssigndtl_rec['font_size'] = $w_formats['font_size'];
        $ssigndtl_rec['font_style'] = $w_formats['style_cd'];
       	
        $ws_bold        = 3; 
     	$ws_not_bold    = 0;
        if($w_formats['bold_sw'] == "Y")
        {
        	$ssigndtl_rec['font_wgt'] = $ws_bold;
        }
        else
        {
        	$ssigndtl_rec['font_wgt'] = $ws_not_bold;//0
        }
       	$ssigndtl_rec['font_type'] = $w_formats['font_type'];
        $ssigndtl_rec['compression'] = $w_formats['compression']; 
        $ssigndtl_rec['font_just'] = $w_formats['justify_cd']; 
        
        # default action_code to Just Print -- the default may be over
       	# written by code below  
       	$ssigndtl_rec['action_code'] = "JP";
       	$ssigndtl_rec['last_updated_by'] = "SSTYPSGN";
       	$ssigndtl_rec['last_update'] = date('Y-m-d H:i:s');
        //echo '<pre>';print_r($ssigndtl_rec).'<br>';//exit;

        //echo 'seq_no '.$seq_no;exit;
		// $save_for_qty = 0;


    	switch ($w_formats['special_proc_cd']){
       	case 'NS':
       		# No Special Processing
            $this->insert_ssigndtl($ssigndtl_rec);
       		break;

		case 'D1':
       		 # Item Description
		
			$this->insert_ssigndtl($ssigndtl_rec);
       			
       		break;

       	case 'D2':
       		 # Item Description
			$this->insert_ssigndtl($ssigndtl_rec);

       		break;
       	case 'D3':
       		 # Item Description
			$this->insert_ssigndtl($ssigndtl_rec);
       		break;

       	case 'D4':
       		 # Item Description
			$this->insert_ssigndtl($ssigndtl_rec);
			break;

       	case 'T1':
       		 # Item Description
			$this->insert_ssigndtl($ssigndtl_rec);
			break;
       		
       	case 'T2':
       		# Item Description
			$this->insert_ssigndtl($ssigndtl_rec);
       		break;

       	case 'SZ':
       		#  Item Size

			$this->insert_ssigndtl($ssigndtl_rec);
       		break;

       	case 'GP':
       		  # Buy 1 Price
             $save_reg_price = $w_scrn_data;
             $this->insert_ssigndtl($ssigndtl_rec);
       		break;
       		
      	 case 'GS':
       		 # Buy 2 Price
            $value = $this->get_price_xy($w_scrn_data,$sign_id);
            // echo '<pre>',print_r($value);exit;
             if ($value[1] == "Y") {
             	  
                   	 $ssigndtl_rec['x_coord']   =$value[0][0]['price_x_coord']; 
                     $signdtl_rec['y_coord']   = $value[0][0]['price_y_coord']; 
                     $ssigndtl_rec['font_size'] = $value[0][0]['price_font_size'];
             }
             $this->insert_ssigndtl($ssigndtl_rec);
		         # because we will calculate the saving of buying 2 vs. 1 
		         # set save_for_qty to 2
		         
		         $save_for_qty = 2;

                 $save_reg_price = $w_scrn_data;

       		break; 

       	case 'PQ':
       		 # Purchase Qty for Buy, Get
       			$save_purch_qty = $w_scrn_data;
       			
       			if($save_purch_qty == 1){
       				 $ssigndtl_rec['print_text']  = "BUY ONE";
       			}
       			
       			elseif($save_purch_qty == 2){
       				 $ssigndtl_rec['print_text']  = "BUY TWO";
       			}
                
                 $this->insert_ssigndtl($ssigndtl_rec);
       			break; 

       	case 'QF':
        		# Qty Free for Buy, Get
        		$save_free_qty = $w_scrn_data;
           		
           		if($save_free_qty == 3){

           			$ssigndtl_rec['print_text'] = "GET THREE";
           			# Squeeze THREE down to the size of TWO
           			$ssigndtl_rec['compression'] = 18;
           		}
           		
           		else{

           			if($save_free_qty == 2){
           				$ssigndtl_rec['print_text'] = "GET TWO";
           			}
           			
           			else{
           				$ssigndtl_rec['print_text'] = "GET ONE";
           			}
           		}

         		$this->insert_ssigndtl($ssigndtl_rec);
       	  		
       	  		break;

       	case 'BQ':
       				# Buy More Qty, save to calculate Buy More Save Amount
                    # we do not save of the x and y coord and font size
                 	$this->insert_ssigndtl($ssigndtl_rec);
       	  		
       	  		break;

       	case 'FQ':
       	  	  	 # For Qty
       				$save_for_qty =  $w_scrn_data;
       			 	//echo $ssigndtl_rec['font_just'];exit;
                
                 	# we do not save of the x and y coord and font size
                    # for "for qty" here because these are stored in
                    # the ssprcxy table and retrieved when sale price is
                    # processed
                    $save_for_qty_font_type  = $ssigndtl_rec['font_type'];
                    $save_for_qty_compression  =$ssigndtl_rec['compression']; 
                    $save_for_qty_font_wgt   = $ssigndtl_rec['font_wgt'];
                    $save_for_qty_font_just  = $ssigndtl_rec['font_just'];
                    $save_for_qty_font_style = $ssigndtl_rec['font_style'];	
                  
       	  	  	break; 

       	case 'BP':
       	 	  	  # Print Buy More Price
       			  $save_buy_more_price = $w_scrn_data;
       			  
       			  if ($save_buy_more_price < 1.00) {
       			  
       			  $ws_int = (($save_buy_more_price * 100) % 100);
       			  # move cents to char field to remove leading spaces
       			  		$ws_string  =	$ws_int ;
       			  		$ssigndtl_rec['print_text'] = $ws_string;
       			  		$ssigndtl_rec['action_code'] = "CA";

       			  }
       			
       			 $font_just 	= 		  $ssigndtl_rec['font_just']; 
       			 $save_price_x_coord    = $ssigndtl_rec['x_coord'];
                 $save_price_y_coord     = $ssigndtl_rec['y_coord'];
                 $save_price_font_size   = $ssigndtl_rec['font_size'];
                 $save_price_font_type   = $ssigndtl_rec['font_type'];
                 $save_price_compression = $ssigndtl_rec['compression'];
                 $save_price_font_style  = $ssigndtl_rec['font_style'];
                 $save_price_font_wgt    = $ssigndtl_rec['font_wgt'];
				
				$this->insert_ssigndtl($ssigndtl_rec);
				
				 if( $save_buy_more_price >  .99){
				 	$ws_string  = $w_scrn_data;

				 	# if buy more price includes decimal amount, dont print "$"
                    # so print $ for the likes of $5, $24, etc...
                    if(strlen($ws_string) < 3){
                       
                       $this->print_dollar_sign($save_price_font_size ,$font_just,$ws_string,$save_price_x_coord,$save_price_y_coord,$save_price_font_type,$save_price_compression,$save_price_font_wgt );
                    }
                   
				 } 
       	 	  	break;

       	case 'SP':
       	 	  	# Print Sale Price
       			$save_sale_price = $w_scrn_data;
       			
       			if ($save_sale_price < 1.00) {
       				
       				$ws_int = (($save_sale_price * 100) % 100);
       				# move cents to char field to remove leading spaces
                    $ws_string        = $ws_int;
                    
                    $ssigndtl_rec['print_text']  = $ws_string;
                    
                    $ssigndtl_rec['action_code'] = "CA";
                    #LET ssigndtl_rec.action_code = "CI"
       			}

       				//$value =  $this->get_price_xy($w_scrn_data,$sign_id,$special_proc_cd,$print_data);

       				
       			$value =  $this->get_price_xy($w_scrn_data,$sign_id);
       			//echo '<pre>',print_r($value);exit;

       			 #IF GLOBALS['save_sale_price'] < 1.00 THEN
                 #LET ssigndtl_rec.print_text  =
                 # ssigndtl_rec.print_text CLIPPED, "~"
                 #END IF 
       				 if ($value[1] == "Y") {
             	  
                  	 $ssigndtl_rec['x_coord']   =$value[0][0]['price_x_coord']; 
                     $ssigndtl_rec['y_coord']   = $value[0][0]['price_y_coord']; 
                     $ssigndtl_rec['font_size'] = $value[0][0]['price_font_size'];
            		
            		 }
            		 //echo $ssigndtl_rec['font_just'];exit;
            	 $save_price_x_coord     = $ssigndtl_rec['x_coord'];
                 $save_price_y_coord     = $ssigndtl_rec['y_coord'];
                 $save_price_font_size   = $ssigndtl_rec['font_size'];
                 $save_price_font_type   = $ssigndtl_rec['font_type'];
                 $save_price_compression = $ssigndtl_rec['compression'];
                 $save_price_font_style  = $ssigndtl_rec['font_style'];
                 $save_price_font_wgt    = $ssigndtl_rec['font_wgt'];
                 $font_just 			 = $ssigndtl_rec['font_just'];

                 $this->insert_ssigndtl($ssigndtl_rec);

                
                 
               if($save_for_qty > 1){
                 	$ssigndtl_rec['action_code'] = "JP";
                    $ssigndtl_rec['print_text']  = $save_for_qty;
                     
                     if($value[1] == "Y" ){
                     									
                     									
                     									
                       $ssigndtl_rec['x_coord']   = $value[0][0]['forqty_x_coord'];
                       $ssigndtl_rec['y_coord']   = $value[0][0]['forqty_y_coord'];
                       $ssigndtl_rec['font_size'] = $value[0][0]['forqty_font_size'];
                       $save_for_qty_x_coord   = $value[0][0]['forqty_x_coord'];
                       $save_for_qty_y_coord   = $value[0][0]['forqty_y_coord']; 
                       $save_for_qty_font_size = $value[0][0]['forqty_font_size'];	
                     
                     }
                    
                    else{
                    
                       $ssigndtl_rec['x_coord']   = $save_for_qty_x_coord;
                       $ssigndtl_rec['y_coord']   = $save_for_qty_y_coord;
                       $ssigndtl_rec['font_size'] = $save_for_qty_font_size;
                    }
                       //echo $ssigndtl_rec['font_just'];exit;
                    $save_for_qty_font_type  = $ssigndtl_rec['font_type'];
                    $save_for_qty_compression  =$ssigndtl_rec['compression']; 
                    $save_for_qty_font_wgt   = $ssigndtl_rec['font_wgt'];
                    $save_for_qty_font_just  = $ssigndtl_rec['font_just'];
                    $save_for_qty_font_style = $ssigndtl_rec['font_style'];

                    if(in_array('FQ', $special_proc_cd)) {
                    $ssigndtl_rec['font_type']  = $save_for_qty_font_type;
                    $ssigndtl_rec['compression']  = $save_for_qty_compression;
                    $ssigndtl_rec['font_wgt']   = $save_for_qty_font_wgt;
                    $ssigndtl_rec['font_just']  = $save_for_qty_font_just;
                    $ssigndtl_rec['font_style'] = $save_for_qty_font_style;
                    }

                   
                     
                    $this->insert_ssigndtl($ssigndtl_rec);
                 }
                   if($save_sale_price > .99){
                    
                    $ws_string = $w_scrn_data;
                   	
                   	 # if sale price includes decimal amount, dont print "$"
                     # so print $ for the likes of $5, $24, etc...
                    
                     if(strlen($ws_string) < 3 ){
                     	
                     	$this->print_dollar_sign($save_price_font_size,$font_just ,$ws_string,$save_price_x_coord,$save_price_y_coord,$save_price_font_type ,$save_price_compression,$save_price_font_wgt);
                     }
                   }
                 
       	 	  	break; 

       	case 'S1':
       	 	  	
       	 	  	# Print Sale Price on 30% off signs.
       	 	  	
       	 	  	 $save_sale_price = $w_scrn_data;
                 
                 if($save_sale_price < 1.00){
                
                 	$ws_int = (($save_sale_price * 100) % 100);
                    # move cents to char field to remove leading spaces
                    $ws_string  =  $ws_int;
                    $ssigndtl_rec['print_text']  = $ws_string;
                    $ssigndtl_rec['action_code'] = "CA";
                
                 }

                 // $value= $this->get_price_xy($w_scrn_data,$sign_id,$special_proc_cd,$print_data);
                 // echo '<pre>';print_r($value);exit;
                 // if($use_prcxy_data_sw == "Y"){

                
                 $value= $this->get_price_xy($w_scrn_data,$sign_id);
                
                 //echo '<pre>',print_r($value);exit;
                
                 if($value[1] == "Y"){


                    $ssigndtl_rec['x_coord']   = $value[0][0]['price_x_coord'];
                    $ssigndtl_rec['y_coord']   = $value[0][0]['price_y_coord'];
                    $ssigndtl_rec['font_size'] = $value[0][0]['price_font_size'];
                }
            //}
                
                $this-> insert_ssigndtl($ssigndtl_rec);
       	 	  	
       	 	  	break;

       	case 'R1':
       	 	  	
       	 	  	# Print Copy of Reg Price
                 $ssigndtl_rec['print_text'] = $w_scrn_data; 
                
                 $this->insert_ssigndtl($ssigndtl_rec);
       	 	  	
       	 	  	break;

       	case 10:
       	case 30:
       	case 25:
       	case 50:
       	case 75:
       			# Print pct off Price
       			
       			$save_pct_off_price = $w_scrn_data;
       			
       			#LET ssigndtl_rec.print_text  = save_pct_off_price
       			
       			if($save_pct_off_price < 1.00) {
       			
       				$ws_int = (($save_pct_off_price * 100) % 100);
                    # move cents to char field to remove leading spaces
                    $ws_string     = $ws_int;
                    $ssigndtl_rec['print_text']  = $ws_string;
                    $ssigndtl_rec['action_code'] = "CA";
	       		
	       		}

                 else{

                 	if($save_pct_off_price > 99.99) {
					
					   # for $100's and up, drop font by 33%
                 	   $ssigndtl_rec['font_size']  = $ssigndtl_rec['font_size'] -
                 	                                ($ssigndtl_rec['font_size'] * .09); 	                   		
                 	}

                 } 
                 $save_price_x_coord     = $ssigndtl_rec['x_coord'];
                 $save_price_y_coord     = $ssigndtl_rec['y_coord'];
                 $save_price_font_size   = $ssigndtl_rec['font_size'];
                 $save_price_font_type   = $ssigndtl_rec['font_type'];
                 $save_price_compression = $ssigndtl_rec['compression'];
                 $save_price_font_style  = $ssigndtl_rec['font_style'];
                 $save_price_font_wgt    = $ssigndtl_rec['font_wgt'];
                 $font_just              =$ssigndtl_rec['font_just'];
                 
                 $this->insert_ssigndtl($ssigndtl_rec);

                   if($w_formats['special_proc_cd'] = "25" ||  
                    $w_formats['special_proc_cd'] = "50" ||  
                    $w_formats['special_proc_cd'] = "75") 
                    {
                    	if($save_pct_off_price >  .99){
						$ws_string = $w_scrn_data;
                       # if pct off price includes decimal amount, dont print "$"
                       # so print $ for the likes of $5, $24, etc... 
		                    if(strlen($ws_string) < 3){
		                    	$this->print_dollar_sign($save_price_font_size,$font_just , $ws_string, $save_price_x_coord,$save_price_y_coord,$save_price_font_type, $save_price_compression, $save_price_font_wgt);	
		                    } 
		                      
                    	}

                    } 
                    
                        		  	  		  
                 break;
    	/*
		case 'FO':
		          # print the word "for" next to for qty

                  if($save_for_qty  > 1){
                  	//echo 'w_scrn_data '.$w_scrn_data;exit;
                  	if($use_prcxy_data_sw == "Y"){
                  		$value= $this->get_price_xy($w_scrn_data,$sign_id);
                
                 	//echo '<pre>',print_r($value);exit;
                
                 if($value[1] == "Y"){
                    	$ssigndtl_rec['x_coord']   = $value[0][0]['for_x_coord']; 
                       $ssigndtl_rec['y_coord']   = $value[0][0]['for_y_coord']; 
                       $ssigndtl_rec['font_size'] = $value[0][0]['for_font_size']; 
                   }
                    }
                  }
                    $ssigndtl_rec['print_text'] = "for";
                     $this->insert_ssigndtl($ssigndtl_rec);
		            break;
		            */

		case 'WP':
		             # Print Was Price
				//echo 'ws_scrn_data '.$ws_scrn_data;exit; 
                $data_len = strlen($w_scrn_data);
                
                 	if($data_len > 0)
                 	{
                 		$ws_money = $w_scrn_data;

                 		if($ws_money < 1.00){
                 			 $ws_int= (($ws_money * 100) % 100);
                 			# move cents to char field to remove leading spaces
                 			 	$ws_string = $ws_int;
                 			 	 # Is WAS pre-printed on sign
                 			
							
                 			 	if($sssign_rec[0]['save_pre_prntd_sw'] == "Y"){
                 			 	$ssigndtl_rec['print_text'] = $ws_string;
                 			 	}
                 			 	else{
                 			 		$ssigndtl_rec['print_text'] ="WAS".$ws_string;
                 			 	}  
                 			 	$ssigndtl_rec['action_code'] = "CA";
                 		}
                 		else{
                 				 # Is WAS pre-printed on sign 
                 			 if($sssign_rec[0]['save_pre_prntd_sw']== "Y"){
                 			 	$ssigndtl_rec['print_text'] = "$".$w_scrn_data;
                 			 }   
                             else{
                         	$ssigndtl_rec['print_text'] = "WAS  $".$w_scrn_data ;
                                                        
                             }
                 		}
                 		$this->insert_ssigndtl($ssigndtl_rec);
                 	}
		            break;

		case 'EP':
		           # Print Everyday Price

                 $ws_money  = $w_scrn_data;

                 if($ws_money < 1.00){
                 	$ws_int = (($ws_money * 100) %  100 );
                    # move cents to char field to remove leading spaces
                    $ws_string                = $ws_int;
                    $ws_price_string          = $ws_string;
                    $ssigndtl_rec['action_code'] = "CA";
                 } 
                 else{
                 	 $len_of_price = strlen($w_scrn_data);
                 	 if($len_of_price < 3){
						# whole dollar amts get a "$" like $5 and $24
                       $ws_price_string = "$". $w_scrn_data;                  	 	
                 	 }
                 	 else{
                 	 	$ws_price_string = $w_scrn_data;
                 	 }
                        	
                 }
                   /*$key = array_search('FQ', $special_proc_cd);
      			   $save_for_qty = $print_data[$key];*/   
                 
                 	if($save_for_qty > 1){
                 	
                 	$ws_string = $save_for_qty; 
                    $ssigndtl_rec['print_text'] = $ws_string."/".
                                                  $ws_price_string ;
              		}
              		else{
              			$ssigndtl_rec['print_text'] = $ws_price_string ;  
              		}
                    $this->insert_ssigndtl($ssigndtl_rec);
		            break;

		case 'CP':
		         # Print Competitor Price
                 $ws_money = $w_scrn_data;

                 if($ws_money < 1.00){
                 
                 	$ws_int = (($ws_money * 100) % 100);
                    # move cents to char field to remove leading spaces
                    $ws_string  = $ws_int;
                    $ssigndtl_rec['print_text'] = $ws_string;
                    $ssigndtl_rec['action_code'] = "CA";

                 }
                 else{
                 	
                 	$ssigndtl_rec['print_text'] = "$". $w_scrn_data;
                 }
                  $this->insert_ssigndtl($ssigndtl_rec);
		        break;


		case 'CV':
		         # Print +CRV at a specific location
                 $ws_crv_sw = $w_scrn_data;
                 if($ws_crv_sw == "Y" || $ws_crv_sw == "y"){
                 	$ssigndtl_rec['print_text'] = "+CRV";
                 }                    
                    $this->insert_ssigndtl($ssigndtl_rec);
		        break;


		case 'MW':
		         # To distinguish between a sign for a weekly ad item and a
                 # sign for an item on ad for more than one week, scanners
                 # need a dot to be printed on the sign to prevent the sign
                 # from being pulled at the end of the weekly ad.

                 # Print dot on sign to indicate price is for Multi-Week sign
                $ws_mw_sw = $w_scrn_data;
                if($ws_mw_sw == "Y"){
	                 	$ssigndtl_rec['print_text'] = ".";
	                    $this->insert_ssigndtl($ssigndtl_rec);
		          }
                break;

		case 'LB':
		          # Print lbs next to for qty value
                 $ws_pounds_sw = $w_scrn_data;
                  if($ws_pounds_sw == "Y"){
                  		$key = array_search('FQ', $special_proc_cd);
     				 	$save_for_qty = $print_data[$key];
                  	if($save_for_qty > 1){
                  		//$this->print_lbs();
                  	}  
                  }
                break;


		case 'RP':
		          # Special processing for Regular Price 
                 $ws_money = $w_scrn_data;
                 if($ws_money > 0){
                 	if($ws_money < 1.00){
                 	   $ws_int = (($ws_money * 100) % 100);
                       # move cents to char field to remove leading spaces
                       $ws_string = $ws_int;
                       $ssigndtl_rec['print_text'] = $ws_string;
                       $ssigndtl_rec['action_code'] = "CA";
                    
                 	}
                       
                 }
                  
                	 $this->insert_ssigndtl($ssigndtl_rec);
		        break;

		case 'SA':
		case 'S3':
		# Special processing for Save Amount 
					
					$key = array_search('PQ', $special_proc_cd);
					$save_purch_qty= $print_data[$key];

                 if($w_formats['special_proc_cd'] = "SA"){
                 	
                 	$ws_save_amt = $w_scrn_data;
                 }
                   
                 if($ws_save_amt > 0)
                 {
                 	if($ws_save_amt < 1.00)
                 	{
                 		$ws_int = (($ws_save_amt * 100) % 100);
                       # move cents to char field to remove leading spaces
                       $ws_save_string = $ws_int; 
                       
                       if($sssign_rec[0]['save_pre_prntd_sw'] == "N") {

                          $ws_string  = "Save ".$ws_save_string;
                       }
                       else{

                       	   $ws_string = $ws_save_string;
                       }
                       	

                         if($save_for_qty > 1){
                        
                        $ws_save_string = $save_for_qty;
                        $ws_string      = 	$ws_string. " on ".$ws_save_string ;
                        
                          # set action_code so cent sign is imbedded after
                          # cent amount...the tilda (~) will be replaced
                          # with the cent sign in the ssignext.prl script
                        
                          $ssigndtl_rec['action_code'] = "CI";
                       } 
                          
                       else{

                       }
                          if($save_buy_qty > 1){
                          	$ws_save_string = $save_buy_qty;
                            $ws_string = $ws_string." ~ When You Buy ".$ws_save_string;
                             # set action_code so cent sign is imbedded after
                             # cent amount...the tilda (~) will be replaced
                             # with the cent sign in the ssignext.prl script
                            $ssigndtl_rec['action_code'] = "CI";
                          } 
                          else{

                             $ssigndtl_rec['action_code'] = "CA";
                          }
                   
                 	}
                 	else{

                 		if($sssign_rec[0]['save_pre_prntd_sw'] == "N") 
                 		{
                 			$ws_save_string = $ws_save_amt;
                 			$ws_string = "Save ". $ws_save_string;
                 		}
                 		else{

                 			$ws_string = $ws_save_amt;
                 		}
                 		if($save_for_qty > 1){
                 			$ws_save_string = $save_for_qty;
                            $ws_string = $ws_string ." on ".$ws_save_string ;
                 		}
                 		else{
                 			if($save_buy_qty > 1){

                 				$ws_save_string = $save_buy_qty;
                 				 $ws_string = $ws_string ." When You Buy ".$ws_save_string ;
                 			}  
                 		}	
                 	}
                 	$ssigndtl_rec['print_text'] = $ws_string; 

                 	$this->insert_ssigndtl($ssigndtl_rec);
                 }    
                      
                        
                    
		            break;
		case 'PM':
		          # Fixed message printed on Sign
                 $ssigndtl_rec['print_text'] = $w_formats['element_prompt'];
                 $this->insert_ssigndtl($ssigndtl_rec);

		         break;
		case 'IC':
		         # SKU Number loaded from a remote sign request
                 # Save off SKU in case we need it for a bar code
                 $save_sku_no = $w_scrn_data;
                 $l_sku_no    = $w_scrn_data;
                 $this->insert_ssigndtl($ssigndtl_rec);
		            break;
		
		case 'BC':
		        # Bar Code for SKU
				$key = array_search('IC', $special_proc_cd);
     			$save_sku_no = $print_data[$key];

                $ssigndtl_rec['print_text']  = $save_sku_no;
                $ssigndtl_rec['action_code'] = "BC";
                 if($save_sku_no != " ") {

                 	$this->insert_ssigndtl($ssigndtl_rec);
                }
		            break;

		case 'AL':
		         # Aisle Location
				  $key = array_search('IC', $special_proc_cd);
      			  $l_sku_no = $print_data[$key];

                  $save_sku_no = $w_scrn_data;
                
                  $aisle_location =  DB::select("SELECT aisle_loc  FROM sltagmst
                  WHERE sku_number = '{$l_sku_no}'");
               	  
               	   # IF sqlca.sqlcode = 0 THEN (need to check)
                    $ssigndtl_rec['print_text'] = $aisle_location[0]['aisle_loc'];
                    $this->insert_ssigndtl($ssigndtl_rec);
		            break;
		case 'DS':
		             # Distributor loaded from a remote sign request
		            $this->insert_ssigndtl($ssigndtl_rec);
		            break;

		case 'DP':
		            # GL Dept loaded from a remote sign request
		            $this->insert_ssigndtl($ssigndtl_rec);
		            break;
		case 'PC':
		            # UPC Number loaded from a remote sign request
		            $this->insert_ssigndtl($ssigndtl_rec);
		            break;
		
		case 'SD':
		             # Sign Date loaded from a remote sign request
					$this->insert_ssigndtl($ssigndtl_rec);	
		            break;                                                                                                                                                                                                                                                               	        
        }
}
#**************************************************************************
# Print Dollar Sign next to sale price
#**************************************************************************
public function  print_dollar_sign($save_price_font_size,$font_just,$print_text,$save_price_x_coord,$save_price_y_coord,$save_price_font_type,$save_price_compression,$save_price_font_wgt)
{
	# Set dollar sign font size to 40% of price font size
	 
	 $ws_int      = $save_price_font_size * .4;
   	 $font_size   = $ws_int;
   	 $font_just   = $font_just;
   	 $ssigndtl_rec['print_text'] = $print_text; 

	# If price is Center justified, move more to the left of price
   	 
   	 if( $font_just == "C")
   	 {
   	 	 if(strlen($ssigndtl_rec['print_text']) == 1){
   	 	
   	 	 	# If price that was just printed is 1 digit, i.e. $5, $4, etc...
   	 	 	 $ws_int = $save_price_font_size * .2 ;
   	 	 }
        else{
        	 
        	# If price that was just printed is 2 digits, i.e. $10, $20, etc...
        	$ws_int = $save_price_font_size * .4 ;
        }
        $ssigndtl_rec['x_coord'] = $save_price_x_coord - $ws_int;  
        # left of price
   	 }
   	 else{
   	 	
   	 	$ssigndtl_rec['x_coord']    = $save_price_x_coord - 3;  

   	 	# -3 100's of an in.
   	 }
	   # In Post Script, .38 times the font size moves to the correct y coord
	   $ws_int  =   $save_price_font_size * .38 ;
	   $ssigndtl_rec['y_coord']    = $save_price_y_coord + $ws_int;

	   $ssigndtl_rec['font_just']  = "R";
	   $ssigndtl_rec['font_type']  = $save_price_font_type;
	   $ssigndtl_rec['compression']  = $save_price_compression;
	   $ssigndtl_rec['font_wgt']   = $save_price_font_wgt;
	   $ssigndtl_rec['print_text'] = "$";

	    $this->insert_ssigndtl($ssigndtl_rec);

}
#**************************************************************************
# Get x y coord's for price, for qty, and the word "for" from ssprcxy table
#
# Each of the letter codes below is used as a key to the ssprcxy table.
# The ssprcxy table stores the different x/y coordinates and font size
# for the various combinations of for qty and price.  For example, "99cents"
# will have a different x/y coordinate and font size compared to             
# "3 for 9.99".  The table enables us to best format the size of the price
# on the sign.
#**************************************************************************
	public function get_price_xy($w_scrn_data,$sign_id)
	{
		
         $print_data = Input::get('sign_line');
         
         $special_proc_cd = Input::get('special_proc_cd');
         //print_r($special_proc_cd);exit;
		 $data_len = strlen($w_scrn_data);
		 
		 $key = array_search('FQ', $special_proc_cd);
		 
		 $key1 = array_search('SP', $special_proc_cd);
		 
		 $save_for_qty=$print_data[$key];
		 
		 $save_sale_price= $print_data[$key1];
		 $w_price_type_cd;
		if($save_for_qty > 1) # for qty printed 
    	   {
    	   		 if($save_for_qty > 9)# for qty printed
    	   		 {
    	   		 	switch ($data_len) {
    	   		 		case 1:    # like 10 for $5 
    	   		 			$w_price_type_cd = "M";
    	   		 			break;
    	   		 		
    	   		 		case 2:    # like 10 for $5 
    	   		 			if($save_sale_price < 1.00){
    	   		 				$w_price_type_cd  = "F";
    	   		 			} 
                    		else $w_price_type_cd  = "N";
                 			break;

    	   		 		case 4:    # like 10 for $5 
    	   		 			$w_price_type_cd = "O";
    	   		 			break;
    	   		 		
    	   		 		case 5:    # like 10 for $5 
    	   		 			$w_price_type_cd = "J";
    	   		 			break;		

    	   		 		default:
    	   		 			$w_price_type_cd = "L"; # like 100.99
    	   		 			break;
    	   		 	}
    	   		 }
    	   		else{
    	   			switch ($data_len) {
    	   		 		case 1:    # like 10 for $5 
    	   		 			$w_price_type_cd = "B";
    	   		 			break;
    	   		 		
    	   		 		case 2:  # like 99 cents or 24 dollars 
    	   		 			if($save_sale_price < 1.00){
    	   		 				$w_price_type_cd  = "F";
    	   		 			} 
                    		else $w_price_type_cd  = "D";
                 			break;

    	   		 		case 4:    # like 2.99 
    	   		 			$w_price_type_cd = "H";
    	   		 			break;
    	   		 		
    	   		 		case 5:    # like 12.99
    	   		 			$w_price_type_cd = "J";
    	   		 			break;		

    	   		 		default:
    	   		 			$w_price_type_cd = "L"; # like 100.99
    	   		 			break;
    	   		 	}

    	   		}
    	   }
    	   else{
    	   		switch ($data_len) {
    	   		 		case 1:    # like  $5 
    	   		 			$w_price_type_cd = "A";
    	   		 			break;
    	   		 		
    	   		 		case 2:  # like 99 cents or 24 dollars 
    	   		 			if($save_sale_price < 1.00){
    	   		 				$w_price_type_cd  = "E";
    	   		 			} 
                    		else $w_price_type_cd  = "C";
                 			break;

    	   		 		case 4:    # like 2.99 
    	   		 			$w_price_type_cd = "G";
    	   		 			break;
    	   		 		
    	   		 		case 5:    # like 12.99
    	   		 			$w_price_type_cd = "I";
    	   		 			break;		

    	   		 		default:
    	   		 			$w_price_type_cd = "K"; # like 100.99
    	   		 			break;
    	   		 	}
    	   }   
      
      $prcxy_rec = DB::select("SELECT * FROM ssprcxy
     			WHERE sign_id        = '".$sign_id."'
       			AND price_type_cd  = '".$w_price_type_cd."'"); 

     $prcxy_rec = json_decode(json_encode($prcxy_rec), true);

     //echo '<pre>',print_r($prcxy_rec);
      	$GLOBALS['use_prcxy_data_sw'] = "Y";
     //return $prcxy_rec;

     //echo '<pre>',print_r($prcxy_rec);exit;
     if($prcxy_rec){
      	$use_prcxy_data_sw = "Y";
      	//$prcxy_rec = json_decode(json_encode($prcxy_rec), true);
      	
      }
      
     return array($prcxy_rec,$use_prcxy_data_sw);

	}
		

	public function insert_ssigndtl($ssigndtl_rec)
	{
		//echo '<pre>';print_r($ssigndtl_rec).'<br>';//exit;
		$result = DB::table('ssigndtl')->insert($ssigndtl_rec);
		

	}
	public function insert_ssignhdr($remote_seq_no)
	{	
		//echo '<pre>';print_r(Input::all());;exit;
		//echo date('Y-m-d H:i:s');exit;
		$ssignhdr_rec = array();
		$inp_sign_id = Input::get('sign_id');
		$stock_code = Input::get('stock_code');
		$sign_id = Input::get('sign_id');
		$signs_line = DB::select("SELECT *
			        FROM ssformat 
			       WHERE sign_id            = '{$inp_sign_id}'
			         AND element_type_cd   != 'F'
			         AND active_sw = 'Y'
			    ORDER BY seq_on_screen
			");
		$signs_line = json_decode(json_encode($signs_line), true);
		//echo '<pre>';print_r($signs_line);//exit;
		//echo '<pre>';print_r(Input::all());exit; 
		$seq_no_query = DB::select('SELECT max(seq_no) as seq_no FROM ssignhdr order by seq_no desc');	
		$seq_no = $seq_no_query[0]->seq_no+1;
		$batch_no = 0;
		$date_requested = date('Y-m-d');
		$stock_code = $stock_code;
		$sign_id = $sign_id;  
		$print_qty = 1;//Need to check
		$printed_sw = "N";
		$last_updated_by = "SSTYPSGN";
		$last_update = date('Y-m-d H:i:s');
		$ssignrsr_rec =  DB::select('SELECT * FROM ssignrsr WHERE seq_no  = "'.$remote_seq_no.'"');
 		$ssignrsr_rec_array = json_decode(json_encode($ssignrsr_rec), true);
 		//echo '<pre>';print_r($ssignrsr_rec_array);exit;
 		if(!empty($ssignrsr_rec_array))
 		{
 			//echo '<pre>';print_r($ssignrsr_rec_array);exit;
 			$userid = $ssignrsr_rec_array[0]['userid'];
 			$passwd = $ssignrsr_rec_array[0]['passwd'];
 		}
 		else
 		{
 			$userid = 'SIGN1';
 			$passwd = 'SIGN1';
 		}
		$insert = DB::insert('insert into ssignhdr values ("'.$seq_no.'","'.$batch_no.'","'.$userid.'","'.$passwd.'","'.$date_requested.'","'.$stock_code.'","'.$sign_id.'","'.$print_qty.'","'.$printed_sw.'","'.$last_updated_by.'","'.$last_update.'")');
		
		// DB::insert('insert into ssignhdr values ("'.$batch_no.'","","","'.$date_requested.'","'.$stock_code.'","'.$sign_id.'","'.$print_qty.'","'.$printed_sw.'","'.$last_updated_by.'","'.$last_update.'")');
		$seq_no_query = DB::select('SELECT max(seq_no) as seq_no FROM ssignhdr order by seq_no desc');
		$seq_no = $seq_no_query[0]->seq_no;
		//echo $seq_no;exit;
		return $seq_no;
	}
	public function editSignData($i)
	{	
		// echo $i.'<br>';
		$err_msg = '';
		$required_sw = Input::get('required_sw');
		$element_type_cd = Input::get('element_type_cd');
		$special_proc_cd =  Input::get('special_proc_cd');
		$max_length =  Input::get('max_length');
		$sign_data =  Input::get('sign_data');
		//echo $sign_data;exit;
		$seq_no =  Input::get('seq_no');//exit;
		$ssignrsr_rec = DB::select("SELECT * FROM ssignrsr  WHERE seq_no  = '{$seq_no}'");
		$ssignrsr_rec = json_decode(json_encode($ssignrsr_rec), true);

		if($element_type_cd == "G")
		{
		}
		else
		{
			$max_len = $max_length;
			//echo $max_len;exit;
			$data_len = strlen($sign_data);
			//echo $data_len;exit;
			if($data_len == 0)
			{
				if($required_sw == "Y")
				{
					return 'This information is required...';
				}
			}
			else
			{	
				if($data_len > $max_len)
				{
					return "Maximum Length for this is ".$max_len;	
				}
				
			}
		}
		switch ($element_type_cd)
	 	  	{	
	 	  		case "I" :
	 	  			$sign_data =  Input::get('sign_data');
	 	  			if(empty($sign_data))
	 	  			{
	 	  				return "";
	 	  			}
	 	  			$is_numeric = is_numeric($sign_data);
	 	  			if(!$is_numeric || $sign_data < 0)
	 	  			{
	 	  				return "Must be numeric and greater than zero";
	 	  			}
	 	  			//echo $element_type_cd;exit;    
				    break;	
				case "M" :
					$sign_data =  Input::get('sign_data');
	 	  			if(empty($sign_data))
	 	  			{
	 	  				return "";
	 	  			}
	 	  			$is_numeric = is_numeric($sign_data);
	 	  			if(!$is_numeric || $sign_data < 0)
	 	  			{
	 	  				return "Must be numeric, greater than zero, and decimal is required";
	 	  			}
	 	  			else
	 	  			{
	 	  				if($sign_data > 999.99)
	 	  				{
	 	  					return "Value must be less than $1000.00";
	 	  				}
	 	  			}
					//echo $element_type_cd;exit;    
				    break; 
				case "S" :
					$sign_data =  Input::get('sign_data');
	 	  			//echo $sign_data;exit;
	 	  			$sign_data = strtoupper($sign_data);
	 	  			//echo 'sign_data '.$sign_data;exit;
	 	  			if(empty($sign_data))
	 	  			{
	 	  				return "";
	 	  			}
	 	  			if(!($sign_data == "Y" || $sign_data == "N"))
	 	  			{	
	 	  				//echo $sign_data;exit;
	 	  				return "Must be  Y  or  N";
	 	  			}
	 	  			else
	 	  			{
	 	  				return "";
	 	  			}
	 	  			
					//echo $element_type_cd;exit;    
				    break;				       
		    } 
		switch ($special_proc_cd)
	 	  	{	

	 	  		case "PQ" :
	 	  			//echo "inside switch casses PQ";exit;
	 	  			$buy_get_sign_sw = "Y";

	 	  			$sign_data =  Input::get('sign_data');
	 	  			if(strlen($sign_data) > 0)
	 	  			{
	 	  				$save_purch_qty =  $sign_data;
	 	  			}
	 	  			else
	 	  			{
	 	  				$save_purch_qty= 0;
	 	  			}
	 	  			if(($save_purch_qty< 1) || ($save_purch_qty > 2))
	 	  			{
	 	  				$save_purch_qty = 0;
	 	  				return "Buy Qty must be 1 or 2";
	 	  			}
				    break;	
				case "QF" :
				//echo "inside switch casses PQ";exit;
	 	  			$buy_get_sign_sw = "Y";

	 	  			if(strlen($sign_data) > 0)
	 	  			{
	 	  				$save_free_qty =  $sign_data;
	 	  			}
	 	  			else
	 	  			{
	 	  				$save_free_qty = 0;
	 	  			}
	 	  			if(($save_free_qty < 1) || ($save_free_qty > 3))
	 	  			{
	 	  				$save_free_qty = 0;
	 	  				return "Get Qty must be 1, 2, or 3";
	 	  			}
				    break;    
				case "BQ" :
				//echo "inside switch casses BQ";exit;
	 	  			$buy_more_sign_sw = "Y";

	 	  			if(strlen($sign_data) > 0)
	 	  			{
	 	  				$save_buy_qty=  $sign_data;
	 	  			}
	 	  			else
	 	  			{
	 	  				$save_buy_qty = 0;
	 	  			}
	 	  			if(($save_buy_qty < 2))
	 	  			{
	 	  				$save_buy_qty = 0;
	 	  				return "Buy More Save More Qty must be greater than 1";
	 	  			}
				    break; 
				case "BP" :
					//echo "inside switch casses BP";exit;
	 	  			
	 	  			if(strlen($sign_data) > 0)
	 	  			{
	 	  				$save_buy_more_price =  $sign_data;
	 	  				$ws_int=$sign_data;
	 	  				$price_no_pennies=$ws_int;
                    if(($save_buy_more_price = $price_no_pennies))
	 	  			{
	 	  				$sign_data = $ws_int;
	 	  				
	 	  			}
	 	  		    }
				    break;
				case "FQ" :
				   // echo "inside switch casses FQ";exit;
	 	  			$buy_more_sign_sw = "Y";
	 	  			if(strlen($sign_data) > 0)
	 	  			{
	 	  				$save_for_qty=  $sign_data;
	 	  			}
	 	  			else
	 	  			{
	 	  				$save_for_qty = 0;
	 	  			}
	 	  			if($buy_more_sign_sw = 'Y')
	 	  			{
	 	  				$save_for_qty = 0;
	 	  			}
	 	  			if($save_for_qty < 2){
	 	  				$save_for_qty = 0;
	 	  				$sign_data="";

	 	  			}
	 	  			else{
	 	  				$ws_string= trim($save_for_qty);
	 	  			}
				    break; 
				case "SP" :
					//echo "inside switch casses SP";exit;
	 	  			if(strlen($sign_data) > 0)
	 	  			{
	 	  				$save_sale_price=  $sign_data;
	 	  				$ws_int=$sign_data;
	 	  				$price_no_pennies=$ws_int;
                    if(($save_sale_price= $price_no_pennies))
	 	  			{
	 	  				$sign_data = $ws_int;
	 	  				
	 	  			}
	 	  		    }
				    break;
				case "EP" :
				   // echo "inside switch casses EP";exit;
	 	  			$save_sale_price=  $sign_data;
	 	  				$ws_int=$save_sale_price;
	 	  				$price_no_pennies=$ws_int;
                    if(($save_sale_price = $price_no_pennies))
	 	  			{
	 	  				$sign_data = $ws_int;
	 	  				
	 	  			}
	 	  			else{
	 	  				$sign_data=$save_sale_price;
	 	  			}
				    break; 
				case "RP" :
					// echo "inside switch casses RP";exit;
				    // $save_reg_price=0;
	 	  			
	 	  			// if(strlen($sign_data) > 0)
	 	  			// {
	 	  			// 	$save_reg_price = $sign_data;
	 	  			// 	if($ssignrsr_rec[0]['sale_price'] > 0){

	 	  			// 	}else{
	 	  			// 		if($ssignrsr_rec[0]['regular_price'] > 0){
	 	  			// 			$err_msg="Sale Price required prior to Regular Price's entry";
	 	  			// 			$err_sw = "Y";
	 	  			// 			return($err_msg);
	 	  			// 		}

	 	  			// 	}
	 	  			// }
	 	  		    break;
	 	  		case "S1" :
	 	  			//echo "inside switch casses S1";exit;
                    $save_sale_price=$sign_data;
	 	  		    break;
	 	  		case "GP" :

	 	  		//echo "inside switch casses GP";exit;
                    $save_for_qty= 2;

	 	  			//echo "inside switch casses GP";exit;
                   /* $save_for_qty    = 2;*/

                    $save_reg_price=$sign_data;
	 	  		    break;

	 	  		case "GS" :
	 	  			//echo "inside switch casses GS";exit;
                    $save_sale_price=$sign_data;
	 	  		    break;

	 	  		case "WP" :
	 	  			//echo "inside switch casses WP";exit;
	 	  			// $save_was_price=0;
	 	  			//  //$GLOBALS['save_reg_price']  = $ssignrsr_rec[0]['regular_price']; 
	 	  			// if(strlen($sign_data) > 0)
	 	  			// {
	 	  			// 	$save_was_price =  $sign_data;
	 	  				
        //             if(($ssignrsr_rec[0]['sale_price'] > 0))
	 	  			// {
	 	  			// 	if($GLOBALS['save_for_qty'] > 1){
        //                    $ws_qty = $GLOBALS['save_for_qty'];
        //                	}
	 	  			// 	if($ssignrsr_rec[0]['for_qty'] > 1){
        //                    $ws_qty = $GLOBALS['save_for_qty'];

	 	  			// 	}
	 	  			// 	else{
	 	  			// 		$ws_qty=1;
	 	  			// 	}
	 	  			// $GLOBALS['save_sale_price'] = $ssignrsr_rec[0]['sale_price'];
	 	  			// $ws_unit_price = $GLOBALS['save_sale_price'] / $ws_qty;
	 	  		 //       if($save_was_price > $ws_unit_price){
        //                }
        //                else{
        //                	$err_msg="WAS Price must be larger than Sale Price";
        //                	$err_sw = "Y";
        //                	return($err_msg);
        //                }
        //            }
        //             else{
        //             	if($save_was_price > 0){
        //             		$err_msg="Sale Price required prior to WAS Price's entry";
        //             		$err_sw = "Y";
        //                	    return($err_msg);
        //             	} 

        //                }
	 	  		    
                     
	 	  		 //    }
	 	  		 //    $GLOBALS['save_reg_price'] = $save_was_price;
				    break;

				case "SA" :
				//echo "inside switch casses SA";exit;
				   // $GLOBALS['save_for_qty']    = $ssignrsr_rec[0]['for_qty'];
       //   		   $GLOBALS['save_reg_price']  = $ssignrsr_rec[0]['regular_price']; 
       //  		   $GLOBALS['save_sale_price'] = $ssignrsr_rec[0]['sale_price'];
				   // if($GLOBALS['save_for_qty'] > 0){
       //              }
				   // else{
				   // 	$GLOBALS['save_for_qty']=1;
				   // }
				   // if($GLOBALS['save_for_qty'] > 0){
				   // 	$GLOBALS['ws_save_amt'] =  $GLOBALS['save_sale_price'] * $GLOBALS['save_free_qty'];

				   // }
				   // else
				   // {
				   // 	if($GLOBALS['save_for_qty'] > 0){
				   // 		$GLOBALS['ws_save_amt'] = ( $GLOBALS['save_reg_price'] - $GLOBALS['save_buy_more_price'] ) * $GLOBALS['save_for_qty'];
				   // 	}
				   // 	else{
				   // 		$GLOBALS['ws_save_amt'] = ( $GLOBALS['save_for_qty'] * $GLOBALS['save_reg_price']) - $GLOBALS['save_sale_price'];
				   // 	}
				   // }
				   // if($GLOBALS['ws_save_amt'] >= 0){
				   // 	   $sign_data=$GLOBALS['ws_save_amt'];
				   // }
				   // else{
				   // 	 $sign_data="";
				   // if($GLOBALS['save_reg_price'] = 0){
				   // }
				   // else{
       //                // $error_msg="Reg Price("$GLOBALS['save_reg_price']") and Sale Price("$GLOBALS['save_sale_price']") at For Qty("$GLOBALS['save_for_qty']") create negative Save Amount";
				   // return($GLOBALS['ws_save_amt']);
				   // 	//$error_msg="Reg Price and Sale Price at For Qty create negative Save Amount";
				   // }
				   // }
                   break;

                case 10 || 30 || 25 || 50 || 75  :
                //echo "inside switch casses 10,30,25,50,75";exit;
                    $work_pct = $special_proc_cd;
                    $pct_off  = $work_pct / 100;
                    $GLOBALS['save_for_qty']    = $ssignrsr_rec[0]['for_qty'];
         		    $GLOBALS['save_reg_price']  = $ssignrsr_rec[0]['regular_price']; 
        		    $GLOBALS['save_sale_price'] = $ssignrsr_rec[0]['sale_price'];
        		    if($GLOBALS['save_sale_price'] > 0){
        		    	$work_price = $GLOBALS['save_sale_price'] * $pct_off;
        		    	$GLOBALS['save_pct_off_price'] = $GLOBALS['save_sale_price'] - $work_price;
        		    	$sign_data=$GLOBALS['save_pct_off_price'];
        		    	if($special_proc_cd == 25 || $special_proc_cd == 50 || $special_proc_cd == 75){
        		    		$GLOBALS['save_reg_price'] = $GLOBALS['save_sale_price'];
        		    		$GLOBALS['ws_int']=$sign_data;
        		    		$price_no_pennies=$GLOBALS['ws_int'];
        		    		if($GLOBALS['save_pct_off_price'] == $price_no_pennies){
                                $sign_data=$GLOBALS['ws_int'];
        		    		}

        		    	}
        		    	$work_price = $GLOBALS['save_reg_price'] - $GLOBALS['save_pct_off_price'];
        		    	if($special_proc_cd == 10 || $special_proc_cd == 30){
        		    		$GLOBALS['save_pct_off_save_amt'] = $work_price*6;
        		    	}
        		    	else{
        		    		$GLOBALS['save_pct_off_save_amt'] = $work_price;
        		    	}
        		    }
        		    else{
        		    		$sign_data = "";
        		    	}
        		    
	 	  		    break;

	 	  		case "S3" :
	 	  		//echo "inside switch casses S3";exit;
                    $GLOBALS['save_for_qty']    = $ssignrsr_rec[0]['for_qty'];
         		    $GLOBALS['save_reg_price']  = $ssignrsr_rec[0]['regular_price']; 
        		    $GLOBALS['save_sale_price'] = $ssignrsr_rec[0]['sale_price'];
        		    if($GLOBALS['save_for_qty'] > 0){

        		    }
        		    else{
        		    	$GLOBALS['save_for_qty'] =1;
        		    }
        		    $GLOBALS['ws_save_amt'] = ( $GLOBALS['save_for_qty'] * $GLOBALS['save_reg_price']) - $GLOBALS['save_sale_price'];
        		    if($GLOBALS['ws_save_amt'] < 0){
                       // $err_msg="Invalid Everyday Price(",$GLOBALS['save_reg_price'],") and Sale Price(",$GLOBALS['save_sale_price'],") - creates a negative Save Amount";
                       $err_msg="Invalid Everyday Price and Sale Price - creates a negative Save Amount";
                       return($err_msg);
        		    }
        		    if($GLOBALS['save_sale_price'] > 0){
        		    	$sign_data=$GLOBALS['save_pct_off_save_amt'];
        		    }
        		    else{
        		    	$sign_data="";
        		    }
	 	  		    break;



			}    
	}
	public function editSignDatafromAtoG($i)
	{
		//echo $i.'<br>';
		
		$required_sw = Input::get('required_sw');
		$element_type_cd = Input::get('element_type_cd');
		$special_proc_cd =  Input::get('special_proc_cd');
		//echo $special_proc_cd;exit;
		$sign_data =  Input::get('sign_data');
		$max_length =  Input::get('max_length');
		if($element_type_cd == "G")
		{
		}
		else
		{
			$max_len = $max_length;
			//echo $max_len;exit;
			$data_len = strlen($sign_data);
			//echo $data_len;exit;
			if($data_len == 0)
			{
				if($required_sw == "Y")
				{
					return 'This information is required...';
				}
			}
			else
			{	
				if($data_len > $max_len)
				{
					return "Maximum Length for this is ".$max_len;	
				}
				
			}
		}
		switch ($element_type_cd)
	 	  	{	
	 	  		case "I" :
	 	  			$sign_data =  Input::get('sign_data');
	 	  			if(empty($sign_data))
	 	  			{
	 	  				return "";
	 	  			}
	 	  			$is_numeric = is_numeric($sign_data);
	 	  			if(!$is_numeric || $sign_data < 0)
	 	  			{
	 	  				return "Must be numeric and greater than zero";
	 	  			}
	 	  			//echo $element_type_cd;exit;    
				    break;	
				case "M" :
					$sign_data =  Input::get('sign_data');
	 	  			if(empty($sign_data))
	 	  			{
	 	  				return "";
	 	  			}
	 	  			$is_numeric = is_numeric($sign_data);
	 	  			if(!$is_numeric || $sign_data < 0)
	 	  			{
	 	  				return "Must be numeric, greater than zero, and decimal is required";
	 	  			}
	 	  			else
	 	  			{
	 	  				if($sign_data > 999.99)
	 	  				{
	 	  					return "Value must be less than $1000.00";
	 	  				}
	 	  			}
					//echo $element_type_cd;exit;    
				    break; 
				case "S" :
					$sign_data =  Input::get('sign_data');
	 	  			//echo $sign_data;exit;
	 	  			$sign_data = strtoupper($sign_data);
	 	  			//echo 'sign_data '.$sign_data;exit;
	 	  			if(empty($sign_data))
	 	  			{
	 	  				return "";
	 	  			}
	 	  			if(!($sign_data == "Y" || $sign_data == "N"))
	 	  			{	
	 	  				//echo $sign_data;exit;
	 	  				return "Must be  Y  or  N";
	 	  			}
	 	  			else
	 	  			{
	 	  				return "";
	 	  			}
	 	  			
					//echo $element_type_cd;exit;    
				    break;				       
		    }
		switch ($special_proc_cd)
	 	  	{	
			case "PQ" :
 	  			//echo "inside switch casses PQ";exit;
 	  			$buy_get_sign_sw = "Y";
 	  			$sign_data =  Input::get('sign_data');
 	  			if(strlen($sign_data) > 0)
 	  			{
 	  				$save_purch_qty =  $sign_data;
 	  			}
 	  			else
 	  			{
 	  				$save_purch_qty = 0;
 	  			}
 	  			if(($save_purch_qty < 1) || ($save_purch_qty > 2))
 	  			{
 	  				$save_purch_qty = 0;
 	  				return "Buy Qty must be 1 or 2";
 	  			}
			    break;	
			case "QF" :
				//echo "inside switch casses PQ";exit;
 	  			$buy_get_sign_sw = "Y";
 	  			$sign_data =  Input::get('sign_data');
 	  			if(strlen($sign_data) > 0)
 	  			{
 	  				$save_free_qty =  $sign_data;
 	  			}
 	  			else
 	  			{
 	  				$save_free_qty = 0;
 	  			}
 	  			if(($save_free_qty < 1) || ($save_free_qty > 3))
 	  			{
 	  				$save_free_qty = 0;
 	  				return "Get Qty must be 1, 2, or 3";
 	  			}
			    break;    
			case "BQ" :
				//echo "inside switch casses BQ";exit;
 	  			$buy_more_sign_sw = "Y";
 	  			$sign_data =  Input::get('sign_data');
 	  			if(strlen($sign_data) > 0)
 	  			{
 	  				$save_buy_qty =  $sign_data;
 	  			}
 	  			else
 	  			{
 	  				$save_buy_qty = 0;
 	  			}
 	  			if(($save_buy_qty < 2))
 	  			{
 	  				$save_buy_qty = 0;
 	  				return "Buy More Save More Qty must be greater than 1";
 	  			}
			    break; 
			case "BP" :
				//echo "inside switch casses BP";exit;
 	  			$sign_data =  Input::get('sign_data');
 	  			if(strlen($sign_data) > 0)
 	  			{
 	  				$save_buy_more_price =  $sign_data;
 	  				$ws_int=$sign_data;
 	  				$price_no_pennies=$ws_int;
                if(($save_buy_more_price = $price_no_pennies))
 	  			{
 	  				$sign_data = $ws_int;
 	  				
 	  			}
 	  		    }
			    break;
			case "FQ" :
			    //echo "inside switch casses FQ";exit;
 	  			$buy_more_sign_sw = "Y";
 	  			$sign_data =  Input::get('sign_data');
 	  			if(strlen($sign_data) > 0)
 	  			{
 	  				$save_for_qty =  $sign_data;
 	  			}
 	  			else
 	  			{
 	  				$save_for_qty = 0;
 	  			}
 	  			if($buy_more_sign_sw = 'Y')
 	  			{
 	  				$save_for_qty = 0;
 	  			}
 	  			if($save_for_qty < 2){
 	  				$save_for_qty = 0;
 	  				$sign_data="";

 	  			}
 	  			else{
 	  				$ws_string=trim($save_for_qty);
 	  			}
			    break; 
			case "SP" :
				//echo "inside switch casses SP";exit;
 	  			//$sign_data =  Input::get('sign_data');
				//echo $sign_data;exit;
 	  			if(strlen($sign_data) > 0)
 	  			{
 	  				$save_sale_price =  $sign_data;
 	  				$ws_int=$sign_data;
 	  				$price_no_pennies=$ws_int;
                if(($save_sale_price = $price_no_pennies))
 	  			{
 	  				$sign_data = $ws_int;
 	  				
 	  			}
 	  		    }
			    break;
			case "EP" :
			   // echo "inside switch casses EP";exit;
 	  			$sign_data =  Input::get('sign_data');
 	  			
 	  				$save_sale_price =  $sign_data;
 	  				$ws_int=$save_sale_price;
 	  				$price_no_pennies=$ws_int;
                if(($save_sale_price = $price_no_pennies))
 	  			{
 	  				$sign_data = $ws_int;
 	  				
 	  			}
 	  			else{
 	  				$sign_data=$save_sale_price;
 	  			}
			    break; 
			case "RP" :
				// echo "inside switch casses RP";exit;
			    $sign_data =  Input::get('sign_data');
 	  			$save_reg_price=0;
 	  			
 	  			if(strlen($sign_data) > 0)
 	  			{
 	  				$save_reg_price =  $sign_data;
 	  				if($ssignrsr_rec[0]['sale_price'] > 0){

 	  				}else{
 	  					if($ssignrsr_rec[0]['regular_price'] > 0){
 	  						$err_msg="Sale Price required prior to Regular Price's entry";
 	  						$err_sw = "Y";
 	  						return($err_msg);
 	  					}

 	  				}
 	  			}
 	  		    break;
 	  		case "S1" :
 	  			//echo "inside switch casses S1";exit;
                $save_sale_price=$sign_data;
 	  		    break;
 	  		case "GP" :
 	  			//echo "inside switch casses GP";exit;
                $save_for_qty    = 2;
                $ssignrsr_rec[0]['regular_price']=$sign_data;
 	  		    break;
 	  		case "GS" :
 	  			//echo "inside switch casses GS";exit;
                $ssignrsr_rec[0]['sale_price']=$sign_data;
 	  		    break;
 	  		case "WP" :
 	  			//echo "inside switch casses WP".$sign_data;exit;
 	  			$save_was_price = 0;
 	  			//$save_reg_price  = $ssignrsr_rec[0]['regular_price']; 
 	  			//echo $save_reg_price;exit;
 	  			if(strlen($sign_data) > 0)
 	  			{
 	  				$save_was_price =  $sign_data;
 	  				//echo $save_was_price;exit;
 	  				
                if(($ssignrsr_rec[0]['sale_price'] > 0))
 	  			{
 	  				if($save_for_qty > 1){
                       $ws_qty = $save_for_qty;
 	  				}
 	  				else{
 	  					$ws_qty=1;
 	  				}
 	  				$save_sale_price = $ssignrsr_rec[0]['sale_price'];

 	  				$ws_unit_price = $save_sale_price / $ws_qty;
 	  		       if($save_was_price > $ws_unit_price){
                   }
                   else{
                   	//$err_msg="WAS Price must be larger than Sale Price";
                   	$err_sw = "Y";
                   	return "WAS Price must be larger than Sale Price";
                   }
               }
                else{
                	if($save_was_price > 0){
                		$err_msg="Sale Price required prior to WAS Price's entry";
                		$err_sw = "Y";
                   	    return($err_msg);
                	} 

                   }
 	  		    
                 
 	  		    }
 	  		    $save_reg_price = $save_was_price;
			    break;
			} 
	}
    public function getReportBatchSummary(){
        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $seqn = Input::get('seq_no');

        if ($usrid == '' || $usrid <= 0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('sign1-report-batch-query');
        }
        
        
         $std_curs = DB::select(DB::raw("SELECT * FROM slbathdr WHERE userid = '" . $usrid . "' AND seq_no = '" . $seqn . "' "));

      
        



        if (empty($std_curs)) {
            Session::flash('alert-danger', "No Batch records found that match Query value(s)");
            return Redirect::route('sign1-report-batch-query');
        } else {
            $row_data = $std_curs[0];




            $work_data->store_no = $row_data->store_no;
            $work_data->userid = $row_data->userid;
            $work_data->status_cd = $row_data->status_cd;
            $work_data->created_date = $row_data->created_date;
            $work_data->last_update = $row_data->last_update;
            $work_data->last_update_by = $row_data->last_update_by;
            $work_data->pos_type = $row_data->pos_type;
            $work_data->batch_type = $row_data->batch_type;
            $work_data->batch_id = $row_data->batch_id;
            
                $stat_desc = '';
                $status_cd = $row_data->status_cd;

                switch ($status_cd) {
                    case "P":
                        $stat_desc = "PENDING";
                        break;
                    case "A":
                        $stat_desc = "APPLY";
                        break;
                    case "C":
                        $stat_desc = "COMPLETED";
                        break;
                    case "D":
                        $stat_desc = "DELETED";
                        break;
                    case "B":
                        $stat_desc = "BACKOUT";
                        break;
                    case "R":
                        $stat_desc = "REVERSED";
                        break;
                    case "H":
                        $stat_desc = "HOLD";
                        break;
                    case "E":
                        $stat_desc = "ERROR";
                        break;                   
                    default:
                        $stat_desc = "UNKNOWN";
                        break;
                }

                $work_data->stat_desc = $stat_desc;

                $str_format='';
                
                if($work_data->pos_type== "pos_fuel"){
                    $str_format = "FUEL   "; 
                } else {
                    if($work_data->pos_type=="pos_prim"){
                       $str_format = "GROCERY";
                    }
                }

                     $work_data->str_format = $str_format;

            $work_data->usrid = $usrid;
            $work_data->sqno = $seqn;


            //====
        }
        

        $results = DB::select(DB::raw("SELECT * FROM slbatdtl WHERE hdr_seq_no = '" . $seqn . "' "));
        
        if (!empty($results)) {




            foreach ($results as $key => $row) {

                
                        
                        
               $stat_desc = '';

                

                $change_cd = $row->change_cd;

                switch ($change_cd) {
                    case "P":
                        $stat_desc = "PRICE";
                        break;
                    case "A":
                        $stat_desc = "ADD";                        
                        break;                   
                    case "D":
                        $stat_desc = "DELETE";
                        break;
                    case "U":
                        $stat_desc = "UPDATE";
                        break;                   
                    case "E":
                        $stat_desc = "ERROR";
                        break;                   
                    default:
                        $stat_desc = "UNKNOWN";
                        break;
                }

                $results[$key]->stat_desc = $stat_desc;

              
            }

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " No Batch records found that match Query value(s) ");
            return Redirect::route('sign1-report-batch-view',array('userid'=>$usrid,'seq_no'=>$seqn))->withInput();
        }

        $work_data->results=$results;
        

        return View::make('sign1.reportbatchsummary')->with('data', $work_data);

    }
    
        public function getReportBatchSummaryView(){
        $work_data = new StdClass();

        $usrid = Input::get('userid');
        $hdrseqn = Input::get('hdr_seq_no');
        $seqn = Input::get('seq_no');

        if ($usrid == '' || $usrid <= 0 || $hdrseqn=='' || $hdrseqn<=0 || $seqn == '' || $seqn <= 0) {

            Session::flash('alert-danger', 'Requested record cannot found.');
            return Redirect::route('sign1-report-batch-query');
        }
        
        $work_data->usrid = $usrid;
           $work_data->hdrseqn = $hdrseqn; 
            $work_data->sqno = $seqn;
         

        $std_curs = DB::select(DB::raw("SELECT * FROM slbatdtl WHERE seq_no = '" . $seqn . "' "));
        
        if (!empty($std_curs)) {

            $row_data = $std_curs[0];

           


            $work_data->item_cd = $row_data->item_cd;
            $work_data->field_no = $row_data->field_no;
            $work_data->change_cd = $row_data->change_cd;
            $work_data->new_value = $row_data->new_value;
            $work_data->org_value = $row_data->org_value;
            $work_data->last_update = $row_data->last_update;
            $work_data->last_update_by = $row_data->last_update_by;
            $work_data->active_sw = $row_data->active_sw;
           

                
                        
                        
               $stat_desc = '';

                

                $change_cd = $row_data->change_cd;

                switch ($change_cd) {
                    case "P":
                        $stat_desc = "PRICE";
                        break;
                    case "A":
                        $stat_desc = "ADD";                        
                        break;                   
                    case "D":
                        $stat_desc = "DELETE";
                        break;
                    case "U":
                        $stat_desc = "UPDATE";
                        break;                   
                    case "E":
                        $stat_desc = "ERROR";
                        break;                   
                    default:
                        $stat_desc = "UNKNOWN";
                        break;
                }

                $work_data->stat_desc = $stat_desc;

              
            

            //echo "<pre>"; print_r($results); die;
        } else {

            Session::flash('alert-danger', " No Batch records found that match Query value(s) ");
            return Redirect::route('sign1-report-batch-view',array('userid'=>$usrid,'seq_no'=>$hdrseqn))->withInput();
        }

    	return View::make('sign1.reportbatchsummaryview')->with('data', $work_data);

    }

    

    //Sravan Methods for Stock Code N
    public function postPriceChangeSignsStockN()
    {
    	//echo '<pre>';print_r(Input::all());exit;
    	$ad_begin_date = Input::get('ad_begin_date');
    	$gl_dept_no1 = Input::get('gl_dept_no');
    	$gl_dept_no = explode('^',$gl_dept_no1);
    	$department_name = DB::select('SELECT  gl_dept.description  FROM gl_dept WHERE active_sw = "Y" and gl_dept_no = "'.$gl_dept_no1.'" ');
    	$department_name = $department_name[0]->description; 
    	//echo '<pre>';print_r($department_name);exit;
    	$gl_dept_no = $gl_dept_no[0];
    	//echo $gl_dept_no;exit;
    	$sql = 'SELECT ssignrsr.seq_no, ssignrsr.date_requested, ssignrsr.upc_number,
               ssignrsr.description, ssignrsr.size,   
               ssignrsr.for_qty, ssignrsr.sale_price, ssignrsr.regular_price,   
               ssignrsr.printed_sw, ssignrsr.sign_date
          FROM ssignrsr
         WHERE userid         = "AD"
           AND gl_dept_no     = "'.$gl_dept_no.'"
           AND date_requested = "'.$ad_begin_date.'"
      ORDER BY date_requested, description';
      //echo $sql;exit;
		//$count_results = DB::select(DB::raw($sql));
		//$perPage = 5;
        //$page = Input::get('page', 1);
        //$offset = ($page * $perPage) - $perPage;
		//$sql.=" limit " . $perPage . " offset " . $offset;
		$results = DB::select(DB::raw($sql));
        //$pagination = Paginator::make($results, count($count_results), $perPage);

        $stock_option = DB::select('SELECT * FROM ssremote WHERE ssremote.active_sw = "Y" ORDER BY ssremote.stock_code');
        $stock_option_array = json_decode(json_encode($stock_option), true); 
        return View::make('sign1.poststockNassign',compact('results','department_name','stock_option_array','ad_begin_date','gl_dept_no'));
    	//return View::make('sign1.poststockNassign');
    }
    public function signOption()
    {
    	$stock_code = Input::get('stock_option');
    	$stock_code = explode('^',$stock_code);
    	$stock_code = $stock_code[0];
    	//echo $stock_code;exit;
    	$sign_option = DB::select('SELECT *
	          FROM sssign
	         WHERE sssign.stock_code = "'.$stock_code.'"
	           AND sssign.active_sw  = "Y"
	     	 ORDER BY sssign.sign_id');
    	$sign_option_array = json_decode(json_encode($sign_option), true); 
    	//echo '<pre>';print_r($sign_option_array);exit;
    	return $sign_option_array;
    	
    } 
    public function sstypsgnDynamicForm()
    {
    	//echo '<pre>';print_r(Input::all());exit;
    	$seq_no =  Input::get('seq_no');
    	$ad_begin_date =  Input::get('ad_begin_date');
    	$gl_dept_no = Input::get('gl_dept_no');
    	$stock_or_upc = Input::get('stock_or_upc');
    	$stock_option = DB::select('SELECT * FROM ssremote WHERE ssremote.active_sw = "Y" ORDER BY ssremote.stock_code');
        $stock_option_array = json_decode(json_encode($stock_option), true); 
    	return View::make('sign1.sstypsgnDynamicForm',compact('stock_option_array','gl_dept_no','ad_begin_date','stock_or_upc','seq_no'));
    }
    public function storeSignStocksNames()
    {
    	//echo '<pre>';print_r(Input::all());exit;
    	$ad_begin_date =  Input::get('ad_begin_date');
    	$gl_dept_no = Input::get('gl_dept_no');
    	$stock_option =  Input::get('stock_option');
    	$stock_name = explode('^',$stock_option);
    	$stock_code = $stock_name[0];
    	$stock_name = $stock_name[1];
    	$sign_option = Input::get('sign_option');
    	$sign_option = explode('^',$sign_option);
    	$sign_id = $sign_option[0];
    	$sign_name = $sign_option[1];
    	//echo $sign_name;exit;
    	$department_name = DB::select('SELECT  gl_dept.description  FROM gl_dept WHERE active_sw = "Y" and gl_dept_no = "'.$gl_dept_no.'" ');
    	$department_name = $department_name[0]->description; 
    	$sql = 'SELECT ssignrsr.seq_no, ssignrsr.date_requested, ssignrsr.upc_number,
               ssignrsr.description, ssignrsr.size,   
               ssignrsr.for_qty, ssignrsr.sale_price, ssignrsr.regular_price,   
               ssignrsr.printed_sw, ssignrsr.sign_date
          FROM ssignrsr
         WHERE userid         = "AD"
           AND gl_dept_no     = "'.$gl_dept_no.'"
           AND date_requested = "'.$ad_begin_date.'"
      ORDER BY date_requested, description';
		$results = DB::select(DB::raw($sql));
       
        return View::make('sign1.poststockNassign2',compact('results','ad_begin_date','gl_dept_no','department_name','sign_id','sign_name','stock_name','stock_code'));

    }
    public function sstypsgnDynamicForm2()
    {

    }
    public function prnt_sh_file(){
      $user_id = Input::get('user_id');
      $sign_id = Input::get('sign_id');
      $prnt_yet = Input::get('prnt_yet');
      $batch_no = Input::get('batch_no');
      $start_date = Input::get('start_date');
      $end_date = Input::get('end_date');

      // echo $user_id;
      // echo $sign_id;
      // echo $prnt_yet;
      // echo $start_date;exit;
      //$contents = file_get_contents('/var/www/html/raley/public/sign/sign.sh');
      $command_result = shell_exec('/home/slic/bin/ssignext.sh ' .$user_id.' '.$sign_id.' '.$prnt_yet.' '.$batch_no.' '.$start_date.' '.$end_date);
     	return $command_result; 
      echo 'printed';
    }
}	