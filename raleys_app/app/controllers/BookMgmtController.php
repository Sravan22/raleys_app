<?php
class BookMgmtController extends BaseController
{
	public function getMgmtDailyDate()
	{
		return View::make('mktmgr.bookmngmntdaily');
	}
	public function checkMgmtApproval()
	{
		$safe_date = Input::get('safe_date');//exit;
		$lastApprovalDate=DB::select('select max(date_stamp) as lastApprovalDate from mgmt where  mgmt.date_stamp <= "'.$safe_date.'" ');
		 $safe_date1 = date('m/d/Y', strtotime($safe_date));
		$lastApprovalDate = $lastApprovalDate[0]->lastApprovalDate;
		//$lastApprovalDatemsg = date('m/d/Y', strtotime($lastApprovalDate));
		if($safe_date == $lastApprovalDate)
		{	
			$approvedarray = array();
			$approvedarray[0] = '';
			$msg =  ''.$safe_date1.' HAS ALREADY BEEN APPROVED. ';
			$approvedarray[1] = $msg;
			return $approvedarray;
		}
		$calcDate = date('Y-m-d', strtotime($safe_date .' -1 day'));
		$nexttolastApprovalDate = date('m/d/Y', strtotime($lastApprovalDate .' +1 day'));
		if($calcDate <> $lastApprovalDate)
		{
			$needtoaprvearray = array();
			$needtoaprvearray[0] = '';
			$msg =  'Need to approve '.$nexttolastApprovalDate.' first. <br>'.$safe_date1.' NOT APPROVED';
			$needtoaprvearray[1] = $msg;
			return $needtoaprvearray;
		}
		//echo 'SELECT * FROM mgmt WHERE mgmt.date_stamp = "'.$safe_date.'" ';exit;
		$mgmtinsert =DB::select('SELECT * FROM mgmt WHERE mgmt.date_stamp = "'.$safe_date.'" ');
		if(empty($mgmtinsert))
		{
			$checkHolidayarray = array();
			DB::insert('INSERT INTO mgmt
              VALUES("'.$safe_date.'","bkpmkt")');
			$checkHoliday = $this->checkHoliday($safe_date);
			//echo '<pre>';print_r($checkHoliday);exit;
			$checkHolidayarray[0] = $checkHoliday;
			$msg = 'THE DAILYS HAVE BEEN APPROVED, '.$safe_date1.' ';
			$checkHolidayarray[1] = $msg;
			//echo '<pre>';print_r($checkHolidayarray);exit;
			return $checkHolidayarray;
		}
	}
	public function checkHoliday($safe_date)
	{
		//echo $safe_date;exit;
		$nextDate = date('Y-m-d',strtotime($safe_date . "+1 days"));
		//echo 'select count(*) as holicnt from holidays where holidate = "'.$nextDate.'"';exit;
		$holicnt = DB::select('select count(*) as holicnt from holidays where holidate = "'.$nextDate.'"');
		return $holicnt[0]->holicnt;
	}
	public function skipHoliday()
	{
		//$data = Input::all();
		//echo '<pre>';print_r($data).'<br>';
		//echo $data['nextDate'];exit;
		$curr_day = Input::get('nextDate');
		//echo $curr_day;exit;
		$curr_day = date('Y-m-d',strtotime($curr_day));
		$upd_mgmt = $this->upd_mgmt($curr_day);
	    $upd_loans = $this->upd_loans($curr_day);
	    $upd_safe = $this->upd_safe($curr_day);
	    $curr_day1 = date('m/d/Y',strtotime($curr_day));
	    $msg = "Balance transfered to ".$curr_day1;
	    //echo $msg;exit;
	    return $msg;
	}
	public function upd_mgmt($curr_day)
	{
		$mgmtinsert =DB::select('SELECT * FROM mgmt WHERE mgmt.date_stamp = "'.$curr_day.'" ');
		if(empty($mgmtinsert))
		{
			DB::insert('INSERT INTO mgmt
              VALUES("'.$curr_day.'","cron")');
		}
	}
	public function upd_loans($curr_day)
	{
		$next_day = date('Y-m-d',strtotime($curr_day . "+1 days"));
		//echo $next_day;exit;
		$regcur =DB::select('SELECT reg_num FROM registers');
		$regcur_array = json_decode(json_encode($regcur), true);
		//echo '<pre>';print_r($regcur_array);exit;
		for($i=0;$i<count($regcur_array);$i++)
		{
			$curr_amt_query = DB::select('SELECT  loans.item_amt as curr_amt FROM loans  WHERE loans.item_id = "BL"
		        AND loans.entry_date = "'.$curr_day.'"
		        AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'" ');
			if(empty($curr_amt_query))
			{
 				$curr_amt = 0;
			}
			else
			{
				$curr_amt = $curr_amt_query[0]->curr_amt;
			}
			//echo $curr_amt.'<br>';
			$curr_amt_next_day_query = DB::select('SELECT *  FROM loans WHERE 
				loans.item_id = "BL"
		        AND loans.entry_date = "'.$next_day.'"
		        AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'" ');
			if(empty($curr_amt_next_day_query))
			{
 				DB::insert('INSERT INTO loans
         			VALUES("BL","'.$curr_amt.'","'.$regcur_array[$i]['reg_num'].'","cron","'.$next_day.'")');
			}
			else
			{
				DB::update('UPDATE loans
		           SET loans.item_amt = "'.$curr_amt.'"
		         WHERE loans.item_id = "BL"
		           AND loans.entry_date = "'.$next_day.'"
		           AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'"');
			}
			$negcurr_amt = ($curr_amt * (-1) );
			//echo $negcurr_amt.'<br>';
			$negcurr_amt_query = DB::select('SELECT *  FROM loans WHERE 
				loans.item_id = "M1"
		        AND loans.entry_date = "'.$curr_day.'"
		        AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'" ');
			if(empty($negcurr_amt_query))
			{
 				DB::insert('INSERT INTO loans
         			VALUES("M1","'.$negcurr_amt.'","'.$regcur_array[$i]['reg_num'].'","cron","'.$curr_day.'")');
			}
			else
			{
				DB::update('UPDATE loans
		           SET loans.item_amt = "'.$negcurr_amt.'"
		         WHERE loans.item_id = "M1"
		           AND loans.entry_date = "'.$curr_day.'"
		           AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'"');
			}
			$LR_loans_query1 = DB::select('SELECT *  FROM loans WHERE 
				loans.item_id = "LR"
		        AND loans.entry_date = "'.$curr_day.'"
		        AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'" ');
			if(empty($LR_loans_query1))
			{
 				DB::insert('INSERT INTO loans
         			VALUES("LR",0,"'.$regcur_array[$i]['reg_num'].'","cron","'.$curr_day.'")');
			}
			else
			{
				DB::update('UPDATE loans
		           SET loans.item_amt = "0"
		         WHERE loans.item_id = "LR"
		           AND loans.entry_date = "'.$curr_day.'"
		           AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'"');
			}
			
			$LR_loans_query2 = DB::select('SELECT *  FROM loans WHERE 
				loans.item_id = "LR"
		        AND loans.entry_date = "'.$next_day.'"
		        AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'" ');
			if(empty($LR_loans_query2))
			{
 				DB::insert('INSERT INTO loans
         			VALUES("LR","'.$curr_amt.'","'.$regcur_array[$i]['reg_num'].'","cron","'.$next_day.'")');
			}
			else
			{
				DB::update('UPDATE loans
		           SET loans.item_amt = "'.$curr_amt.'"
		         WHERE loans.item_id = "LR"
		           AND loans.entry_date = "'.$next_day.'"
		           AND loans.reg_num = "'.$regcur_array[$i]['reg_num'].'" ');
			}
			//echo $negcurr_amt.'<br>';
		} //exit;
	}
	public function upd_safe($curr_day)
	{
		$next_day = date('Y-m-d',strtotime($curr_day . "+1 days"));
		//echo $next_day;exit;
		$curr_amt_next_day_query = DB::select('SELECT * FROM safeentry 
			      WHERE safeentry.entry_date = "'.$curr_day.'"
			        AND safeentry.item_id = "LR" ');
			if(empty($curr_amt_next_day_query))
			{
				//echo 'Insert';exit;
 				DB::insert('INSERT INTO safeentry
         			VALUES("LR","0","cron","'.$curr_day.'")');
			}
			else
			{
				//echo 'Update';exit;
				DB::update('UPDATE safeentry
		           SET safeentry.item_amt = 0
		         WHERE safeentry.entry_date = "'.$curr_day.'" ');
			}
			$mainsafe_query = DB::select('SELECT  mainsafe.begin_safe as curr_amt FROM mainsafe  WHERE mainsafe.safe_date  = "'.$curr_day.'" ');
			if(empty($mainsafe_query))
			{
 				$curr_amt = 0;
 				DB::insert('INSERT INTO mainsafe  VALUES(0,0,0,0,0,0,0,0,0,0,0,"'.$curr_day.'")');
			}
			else
			{
				$curr_amt = $mainsafe_query[0]->curr_amt;
				DB::update('UPDATE mainsafe
           		SET mainsafe.safe_cnt = "'.$curr_amt.'",mainsafe.safe_os = 0,mainsafe.total = 0
                  WHERE mainsafe.safe_date = "'.$curr_day.'" ');
			}

	}
	public function postMgmtDailyDate()
	{
		$validator = Validator::make(Input::all(),
	            array(
	                   'safe_date'=>'required'
	                ),array('safe_date.required'=>'Date is required'));
			if($validator->fails())
	        {          
			    return Redirect::route('bookspro-safereportdate')
	                    ->withErrors($validator)
	                    ->withInput();
	        }
	        else
	        {
	        	$safe_date = Input::get('safe_date');
	        	//$approvalsOk = $this->approvalsOk($safe_date);
	        	//echo $approvalsOk;exit;
	   //      	if($approvalsOk)
				// {	
				// 	$lastAprvlDate = date('m/d/Y',strtotime($approvalsOk));
				// 	Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
		  //           return Redirect::route('bookspro-safereportdate');
				// }
				$dayIsLocked = $this->approvalsOk1($safe_date);
				//echo $dayIsLocked;exit;
				// $dayIsLocked = "N";
	        	$file_locked = $this->file_locked($safe_date);
	        	
	        	if($file_locked)
	        	{
	        		$file_locked = "Y";
	        	}
	        	else
	        	{
	        		$file_locked = "N";
	        		$calc_debtot = $this->calc_debtot($safe_date);
	        	}//exit;
	        	// $file_locked = "N";
	        	$mainsafe_records = DB::select('SELECT * FROM mainsafe WHERE mainsafe.safe_date = "'.$safe_date.'" ');
	        	$mainsafe_array = json_decode(json_encode($mainsafe_records), true);
	        	if(!empty($mainsafe_array))
	        	{
	        		$begin_safe = $mainsafe_array[0]['begin_safe'];
	        	}
	        	else
	        	{
	        		$begin_safe = 0;
	        	}
	        	if(!empty($mainsafe_array))
	        	{
	        		$safe_cnt = $mainsafe_array[0]['safe_cnt'];
	        	}
	        	else
	        	{
	        		$safe_cnt = 0;
	        	}
	        	//echo $begin_safe;exit;
	        	$del_tranfs = $this->total_amt("T",$safe_date);
	        	if(!($del_tranfs))
	        	{
	        		$del_tranfs = 0;;
	        	}
	        	$deposits = $this->total_amt("P",$safe_date);
	        	if(!($deposits))
	        	{
	        		$deposits = 0;;
	        	}
	        	$store_tx = $this->total_amt("S",$safe_date);
	        	if(!($store_tx))
	        	{
	        		$store_tx = 0;;
	        	}
	        	$po_types = $this->total_amt("O",$safe_date);
	        	if(!($po_types))
	        	{
	        		$po_types = 0;;
	        	}
	        	$find_os = $this->find_os($safe_date);
	        	//echo '<pre>';print_r($find_os);exit;
	        	$calc_totals = $this->calc_totals($begin_safe,$del_tranfs,$deposits,$store_tx,$po_types,$safe_cnt,$find_os['groc_os'],$find_os['drug_os']);
	        	//echo '<pre>';print_r($calc_totals);exit;
	        	$resultArray = array(
	        		'begin_safe' => $begin_safe/100,
	        		'del_tranfs' => $del_tranfs/100,
	        		'deposits' => $deposits/100,
	        		'store_tx' => $store_tx/100,
	        		'po_types' => $po_types/100,
	        		'tot_acct' => $calc_totals['tot_acct']/100,
	        		'safe_cnt' => $safe_cnt/100,
	        		'safe_os' => $calc_totals['safe_os']/100,
	        		'groc_os' => $find_os['groc_os']/100,
	        		'drug_os' => $find_os['drug_os']/100,
	        		'total' => $calc_totals['total']/100 
	        		);
	        	//echo '<pre>';print_r($resultArray);exit;
	        	$disp_del = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt
						 from safeitems left outer join safeentry
						 on safeitems.item_id = safeentry.item_id
						 and safeentry.entry_date = "'.$safe_date.'"
						 and safeentry.item_amt is not null
						 where safeitems.item_type = "T"
						 and safeitems.item_id NOT LIKE "%T%"
						 and safeitems.disable not in ("y", "Y")
						 order by safeitems.item_desc ');
	        	$disp_del_array = json_decode(json_encode($disp_del), true);

	        	$disp_trf = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt,safeentry.source_id from safeitems left outer join safeentry on safeitems.item_id = safeentry.item_id and safeentry.entry_date = "'.$safe_date.'" and safeentry.item_amt is not null where safeitems.item_type = "T" and safeitems.item_id LIKE "%T%" and safeitems.disable not in ("y", "Y") order by safeitems.item_desc');
	        	$disp_trf_array = json_decode(json_encode($disp_trf), true);
	        	//echo '<pre>';print_r($disp_trf_array);exit;
	        	// for($i = 0;$i<count($disp_trf_array);$i++)
	        	// {
	        	// 	$get_trfamt = $this->get_trfamt($disp_trf_array[$i]['item_id']);
	        	// }
	        	$disp_deposits = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt
						 from safeitems left outer join safeentry
						 on safeitems.item_id = safeentry.item_id
						 and safeentry.entry_date = "'.$safe_date.'"
						 and safeentry.item_amt is not null
						 where safeitems.item_type = "P"
						 and safeitems.disable not in ("y", "Y")
						 order by safeitems.item_id asc ');
	        	$disp_deposits_array = json_decode(json_encode($disp_deposits), true);

	        	$disp_storetx = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt
						 from safeitems left outer join safeentry
						 on safeitems.item_id = safeentry.item_id
						 and safeentry.entry_date = "'.$safe_date.'"
						 and safeentry.item_amt is not null
						 where safeitems.item_type = "S"
						 and safeitems.disable not in ("y", "Y")
						 order by safeitems.item_id asc ');
	        	$disp_storetx_array = json_decode(json_encode($disp_storetx), true);


	        	$disp_potypes = DB::select('select safeitems.item_id, safeitems.item_desc, safeentry.item_amt/100 as item_amt
						 from safeitems left outer join safeentry
						 on safeitems.item_id = safeentry.item_id
						 and safeentry.entry_date = "'.$safe_date.'"
						 and safeentry.item_amt is not null
						 where safeitems.item_type = "O"
						 and safeitems.disable not in ("y", "Y")
						 order by safeitems.item_id asc ');
	        	$disp_potypes_array  = json_decode(json_encode($disp_potypes), true);
	        	//echo '<pre>';print_r($disp_deposits_array);exit;
	        	return View::make('mktmgr.mgmtdaily',compact('resultArray','safe_date','disp_deposits_array','disp_storetx_array','disp_potypes_array','disp_del_array','disp_trf_array','dayIsLocked','file_locked')); 
	        }
	}
	public function approvalsOk($safe_date)
	{
		$prvDate = date('Y-m-d', strtotime($safe_date .' -1 day'));
		//echo $prvDate.'<br>';//exit;
		//echo 'select max(date_stamp) as lastAprvlDate from mgmt where  mgmt.date_stamp < "'.$safe_date.'" ';exit;
		$lastAprvlDate=DB::select('select max(date_stamp) as lastAprvlDate from mgmt where  mgmt.date_stamp < "'.$safe_date.'" ');
		$lastAprvlDate = $lastAprvlDate[0]->lastAprvlDate;
		//echo $lastAprvlDate.'<br>';
		//$lastAprvlDate = date("m/d/Y", strtotime($lastAprvlDate));
		//echo $lastAprvlDate.'<br>';exit;
		if($prvDate <> $lastAprvlDate)
		{
			return $lastAprvlDate;//exit;
		}


		//if($msg != ''){ Session::flash('alert-danger',$msg); }
		//return $retVal;
	}
	public function approvalsOk1($safe_date)
	{
		$dayIsLocked = "N";
		$now = time(); // or your date as well
		$datediff = $now - strtotime($safe_date);
		//echo $datediff;exit;
		$numdays=floor($datediff / (60 * 60 * 24));
		//echo $numdays;exit;
		//echo 'select * from mgmt where  mgmt.date_stamp ="'.$safe_date.'"';exit;
		if($numdays > 1)
		{
			//echo $numdays;exit;
			$mgmt=DB::select('select * from mgmt where  mgmt.date_stamp ="'.$safe_date.'"');
			$check_mgmt = json_decode(json_encode($mgmt), true);
			//echo count($sql);exit;
			if($check_mgmt)
			{
				$dayIsLocked = "Y";
			}
		}
		return $dayIsLocked;
	}
	public function file_locked($stamp_date)
	{
		$file_locked=DB::select('SELECT * from locks  WHERE locks.date_stamp = "'.$stamp_date.'" ');
		if($file_locked)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	public function total_amt($code,$safe_date)
	{
		$total_amt = DB::select('SELECT SUM(safeentry.item_amt) as tot
            FROM safeentry,safeitems
           WHERE safeentry.item_id = safeitems.item_id
             AND safeitems.item_type = "'.$code.'"
             AND safeentry.entry_date = "'.$safe_date.'" ');
		if($total_amt)
		{
			$tot = $total_amt[0]->tot;
		}
		else
		{
			$tot = 0;
		}
		return $tot; 
	}
	public function find_os($safe_date)
	{
		$arrtot = array();
		$get_dogo = $this->get_dogo($safe_date);
		//echo '<pre>';print_r($get_dogo);exit;
		$groc_os = $get_dogo['groctot'];
		$drug_os = $get_dogo['drugtot'];
		if(!($groc_os))
    	{
    		$groc_os = 0;;
    	}
    	if(!($drug_os))
    	{
    		$drug_os = 0;;
    	}
    	$arrtot['groc_os'] = $groc_os;
        $arrtot['drug_os'] = $drug_os; 
        return $arrtot;
		//echo $groc_os.' '.$drug_os;exit;
	}
	public function get_dogo($safe_date)
	{
		//echo $safe_date;exit;
		$arrtot = array();
		$groctot = 0;
		$drugtot = 0;
		$subtot1 = 0;
		$subtot2 = 0;
		$reg_cur = DB::select('SELECT registers.reg_num, registers.reg_type FROM registers ORDER BY registers.reg_num');
		$reg_cur_array = json_decode(json_encode($reg_cur), true);
		for($i=0;$i<count($reg_cur_array);$i++) 
		{
			$subtot1_query = DB::select('SELECT SUM(entry.item_amt) as subtot1 FROM entry, items  WHERE entry.reg_num = "'.$reg_cur_array[$i]['reg_num'].'"
          	AND entry.item_id = items.item_id
          	AND items.item_type = "D"
        	AND entry.entry_date = "'.$safe_date.'" ');
			if($subtot1_query)
			{
				$subtot1 = $subtot1_query[0]->subtot1;
			}
			else
			{
				$subtot1 = 0;
			}
			$subtot2_query = DB::select('SELECT SUM(entry.item_amt) as subtot2 FROM entry, items  WHERE entry.reg_num = "'.$reg_cur_array[$i]['reg_num'].'"
          	AND entry.item_id = items.item_id
          	AND items.item_type = "C"
        	AND entry.entry_date = "'.$safe_date.'" ');
			if($subtot2_query)
			{
				$subtot2 = $subtot2_query[0]->subtot2;
			}
			else
			{
				$subtot2 = 0;
			}
			if($reg_cur_array[$i]['reg_type'] == "G")
			{
				$groctot = $groctot + ($subtot1 - $subtot2);
			}
			elseif($reg_cur_array[$i]['reg_type'] == "D")
			{
				$drugtot = $drugtot + ($subtot1 - $subtot2);
			}
			//echo $subtot1;exit();
			//echo $groctot.' '.$drugtot;exit;
			$subtot1 = 0;
			$subtot2 = 0;
		}
		$arrtot['groctot'] = $groctot;
        $arrtot['drugtot'] = $drugtot;
		return $arrtot;	
	}
	public function calc_debtot($safe_date)
	{
		//echo $safe_date;exit;
		$gsubtot = 0;
		$gsubtot_query = DB::select('SELECT SUM(entry.item_amt) as gsubtot
         FROM entry,items WHERE entry.entry_date = "'.$safe_date.'" AND 
         entry.item_id = items.item_id
          AND items.item_type = "D" ');
		if($gsubtot_query)
		{
			$gsubtot = $gsubtot_query[0]->gsubtot;
		}
		else
		{
			$gsubtot = 0;
		}
		//echo $gsubtot;exit;
		$td_amt_query = DB::select('SELECT safeentry.item_amt  FROM safeentry
        WHERE safeentry.entry_date = "'.$safe_date.'"
          AND safeentry.item_id = "TD" ');
		if($td_amt_query)
		{
			DB::update('UPDATE safeentry SET safeentry.item_amt = "'.$gsubtot.'"     WHERE safeentry.item_id = "TD" AND 
				safeentry.entry_date = "'.$safe_date.'" ');
		}
		else
		{
			DB::insert('INSERT INTO safeentry
            VALUES ("TD","'.$gsubtot.'","book1","'.$safe_date.'")');
		}
		$this->updateSafeEntryItems("LR",$safe_date);
		$this->updateSafeEntryItems("PC",$safe_date);
		$this->updateSafeEntryItems("VR",$safe_date);
	}
	public function updateSafeEntryItems($itemId,$safe_date)
	{
		//echo $itemId;//exit;
		$tot = 0;
		$td_amt_query = DB::select('SELECT SUM(entry.item_amt) as tot FROM entry WHERE entry.entry_date = "'.$safe_date.'"
       AND entry.item_id = "'.$itemId.'" ');
		if($td_amt_query)
		{
			$tot = -1*($td_amt_query[0]->tot);
		}
		else
		{
			$tot = 0;
		}
		//echo $tot;exit;
		$safeentry_query = DB::select('SELECT * from safeentry  WHERE safeentry.entry_date = "'.$safe_date.'"
       AND safeentry.item_id = "'.$itemId.'" ');
		if($safeentry_query)
		{
			DB::update('UPDATE safeentry SET safeentry.item_amt = "'.$tot.'" WHERE safeentry.item_id = "'.$itemId.'" AND 
				safeentry.entry_date = "'.$safe_date.'" ');
		}
		else
		{
			DB::insert('INSERT INTO safeentry
            VALUES ("'.$itemId.'","'.$tot.'","book1","'.$safe_date.'")');
		}
	}
	public function calc_totals($begin_safe,$del_tranfs,$deposits,$store_tx,$po_types,$safe_cnt,$groc_os,$drug_os)
	{
		$calc_totals = array();
		$tot_acct = $begin_safe +$del_tranfs +$deposits + $store_tx + $po_types;
		$safe_os =  $safe_cnt - $tot_acct;
		$total = $safe_os + $groc_os + $drug_os;
		//echo $total;exit;
		$calc_totals['tot_acct'] = $tot_acct;
		$calc_totals['safe_os'] = $safe_os;
		$calc_totals['total'] = $total;
		return $calc_totals;
	}
	
	public function getMgmtWeeklyDate()
	{
		return View::make('mktmgr.bookmngmntweekly');
	}
	public function postMgmtWeeklyDate()
	{
		Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
        $validator = Validator::make(Input::all(),
            array(
	                   'recap_date'=>'required|date|weekenddatecheck'
	                ),array('recap_date.required'=>'Recap Date is required'));
        if($validator->fails())
        {
           
            return Redirect::route('bookspro-mgmtweeklydate')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	//echo '<pre>';print_r(Input::all());exit;
        	$recap_date = Input::get('recap_date');
        	$init_rec = $this->init_rec($recap_date);
        	$init_maindep = $this->init_maindep($recap_date);
        	$disp_deptsales = $this->disp_deptsales($recap_date);
        	$disp_netsafe = $this->disp_netsafe($recap_date);
        	$disp_tranfs = $this->disp_tranfs($recap_date);
        	$disp_del = $this->disp_del($recap_date);
        	$disp_storetx = $this->disp_storetx($recap_date);
        	$disp_potypes = $this->disp_potypes($recap_date);
			$disp_currency = $this->disp_currency($recap_date);
			$disp_checks = $this->disp_checks($recap_date);
        	$disp_express_deb = $this->disp_express_deb($recap_date);
        	$disp_express_cred = $this->disp_express_cred($recap_date);
        	$get_ov = $this->get_ov($recap_date);
			//echo '<pre>';print_r($disp_del);exit;
        	return View::make('mktmgr.mgmtweekly',compact('recap_date','init_rec','init_maindep','get_ov','disp_deptsales','disp_netsafe','disp_storetx','disp_tranfs','disp_del','disp_currency','disp_checks','disp_express_deb','disp_express_cred','disp_potypes'));
        }
	}
	public function checkMgmtWeeklyApproval()
	{
		$recap_date = Input::get('recap_date');
		//echo $recap_date;exit;
		
		$end_date = $recap_date;
		$end_date1 = date("m/d/Y", strtotime($end_date));
		$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
		for($i = 0;$i<=6;$i++)
		{
			$next_day = date('Y-m-d',strtotime($begin_date . $i. "days"));
			//echo $next_day.'<br>';
			$check_locks = DB::select('SELECT * from locks  WHERE locks.date_stamp = "'.$next_day.'"');
			if($check_locks)
			{
				$next_day1 = date("m/d/Y", strtotime($next_day));
				
				return '<span style="color:#6f5454;"><b>FILES HAVE ALREADY BEEN LOCKED FOR <br/> '.$next_day1.' THRU '. $end_date1.'<b></span>';
				break;
			}
			else
			{
				DB::insert('INSERT INTO locks VALUES ("'.$next_day.'","mktmgr")');
			}
		}//exit();
		return '<span style="color:#6f5454;"><b>Files can no longer be updated for <br> week ending '.$end_date1.'<b></span>';
	}
	public function checkmgmtweeklyapprovalprint()
	{
		$recap_date = Input::get('recap_date');
		$check_bkpsig_print = DB::select('select count(*) as sigCount from bkpsig where recap_date = "'.$recap_date.'" ');
		return $check_bkpsig_print[0]->sigCount;//exit;
	}
	public function init_rec($recap_date)
	{
		$found_recap = array();
		$begin_date = date('Y-m-d',strtotime($recap_date . "-6 days"));
		$net_safe_query = DB::select('SELECT SUM(recapmain.dept_sales)/100 
	       as net_safe
	      FROM recapmain,recapitems
	     WHERE recapmain.wk_end_date = "'.$recap_date.'"
	       AND recapitems.item_type IN ("I")
	       AND recapmain.dept_id = recapitems.item_id'); 
		if($net_safe_query[0]->net_safe)
		{
			$net_safe = $net_safe_query[0]->net_safe;
			$found_recap['net_safe'] = $net_safe;
		}
		else
		{
			$net_safe = 0;
			$found_recap['net_safe'] = $net_safe;
		}

		$dept_sales_query = DB::select('SELECT SUM(recapmain.dept_sales)/100 
	      as dept_sales
	      FROM recapmain,recapitems
	     WHERE recapmain.wk_end_date = "'.$recap_date.'"
	       AND recapitems.item_type IN ("M","A","J")
	       AND recapmain.dept_id = recapitems.item_id'); 
		if($dept_sales_query[0]->dept_sales)
		{
			$dept_sales = $dept_sales_query[0]->dept_sales;
			$found_recap['dept_sales'] = $dept_sales;
		}
		else
		{
			$dept_sales = 0;
			$found_recap['dept_sales'] = $dept_sales;
		}


		$deposits_query = DB::select('SELECT SUM(recap.item_amt)/100 
	       as deposits
	      FROM recap,safeitems
	     WHERE recap.wk_end_date >= "'.$begin_date.'"
	       AND recap.wk_end_date <= "'.$recap_date.'"
	       AND safeitems.item_type  = "P"
	       AND recap.item_id  = safeitems.item_id'); 
		if($deposits_query[0]->deposits)
		{
			$deposits = $deposits_query[0]->deposits;
			$found_recap['deposits'] = $deposits;
		}
		else
		{
			$deposits = 0;
			$found_recap['deposits'] = $deposits;
		}

		
		$del_tranfs_query = DB::select('SELECT SUM(recap.item_amt)/100 
	     as del_tranfs
	      FROM recap,recapitems
	     WHERE recap.wk_end_date >= "'.$begin_date.'"
	       AND recap.wk_end_date <= "'.$recap_date.'"
	       AND recapitems.item_type  = "T"
	       AND recap.item_id  = recapitems.item_id'); 
		if($del_tranfs_query[0]->del_tranfs)
		{
			$del_tranfs = $del_tranfs_query[0]->del_tranfs;
			$found_recap['del_tranfs'] = $del_tranfs;
		}
		else
		{
			$del_tranfs = 0;
			$found_recap['del_tranfs'] = $del_tranfs;
		}

		$store_tx_query = DB::select('SELECT SUM(recap.item_amt)/100 
	      as store_tx
	      FROM recap,recapitems
	     WHERE recap.wk_end_date = "'.$recap_date.'"
	       AND recapitems.item_type  = "S"
	       AND recapitems.item_id  NOT IN ("RR","LR","TD")
	       AND recap.item_id  = recapitems.item_id'); 
		if($store_tx_query[0]->store_tx)
		{
			$store_tx = $store_tx_query[0]->store_tx;
			$found_recap['store_tx'] = $store_tx;
		}
		else
		{
			$store_tx = 0;
			$found_recap['store_tx'] = $store_tx;
		}

		$po_types_query = DB::select('SELECT SUM(recap.item_amt)/100 
	       as po_types
	      FROM recap,recapitems
	     WHERE recap.wk_end_date = "'.$recap_date.'"
	       AND recapitems.item_type  = "O"
	       AND recap.item_id  = recapitems.item_id'); 
		if($po_types_query[0]->po_types)
		{
			$po_types = $po_types_query[0]->po_types;
			$found_recap['po_types'] = $po_types;
		}
		else
		{
			$po_types = 0;
			$found_recap['po_types'] = $po_types;
		}
		$found_recap['summary'] =  $net_safe + $dept_sales + $deposits + $del_tranfs + $store_tx + $po_types;
		return $found_recap;
		//echo '<pre>';print_r($found_recap);exit;
	}
	public function init_maindep($recap_date)
	{
		$maindep_rec = array();
		$end_date = $recap_date;
		$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));

		$currency_query = DB::select('SELECT sum(recap.item_amt)/100
          as  currency
          FROM recap
         WHERE recap.item_id IN ("C1","C2", "C3", "C4")
           AND recap.wk_end_date >= "'.$begin_date.'" 
           AND recap.wk_end_date <= "'.$end_date.'" ');
		if($currency_query[0]->currency)
		{
			$currency = $currency_query[0]->currency;
			$maindep_rec['currency'] = $currency;
		}
		else
		{
			$currency = 0;
			$maindep_rec['currency'] = $currency;
		}


		$checks_query = DB::select('SELECT sum(recap.item_amt)/100
          as  checks
          FROM recap
         WHERE recap.item_id IN ("K1","K2")
           AND recap.wk_end_date >= "'.$begin_date.'"  
           AND recap.wk_end_date <= "'.$end_date.'" ');
		if($checks_query[0]->checks)
		{
			$checks = $checks_query[0]->checks;
			$maindep_rec['checks'] = $checks;
		}
		else
		{
			$checks = 0;
			$maindep_rec['checks'] = $checks;
		}

		$express_cred_query = DB::select('SELECT sum(recap.item_amt)/100
          as  express_cred
          FROM recap
         WHERE recap.item_id IN ("CD")
           AND recap.wk_end_date >= "'.$begin_date.'"  
           AND recap.wk_end_date <= "'.$end_date.'" ');
		if($express_cred_query[0]->express_cred)
		{
			$express_cred = $express_cred_query[0]->express_cred;
			$maindep_rec['express_cred'] = $express_cred;
		}
		else
		{
			$express_cred = 0;
			$maindep_rec['express_cred'] = $express_cred;
		}


		$express_deb_query = DB::select('SELECT sum(recap.item_amt)/100
          as  express_deb
          FROM recap
         WHERE recap.item_id IN ("DD")
           AND recap.wk_end_date >= "'.$begin_date.'"  
           AND recap.wk_end_date <= "'.$end_date.'" ');
		if($express_deb_query[0]->express_deb)
		{
			$express_deb = $express_deb_query[0]->express_deb;
			$maindep_rec['express_deb'] = $express_deb;
		}
		else
		{
			$express_deb = 0;
			$maindep_rec['express_deb'] = $express_deb;
		}
		$maindep_rec['total'] = $currency + $checks +  $express_cred + $express_deb;
		return $maindep_rec;
		//echo '<pre>';print_r($maindep_rec);exit;
	}
	public function disp_deptsales($recap_date)
	{
		$disp_deptsales = DB::select('select recapitems.item_id, recapitems.item_desc, recapmain.dept_sales/100 as dept_sales
		 from recapitems left outer join recapmain
		  on recapitems.item_id = recapmain.dept_id
		   and recapmain.wk_end_date = "'.$recap_date.'"
		    and recapmain.dept_sales <> 0
		     where recapmain.dept_sales IS NOT NULL
		      AND recapitems.item_type IN ("M","J", "A")
		       order by recapitems.item_id');
		$disp_deptsales_array = json_decode(json_encode($disp_deptsales), true);
		return $disp_deptsales_array;
	}
	public function disp_netsafe($recap_date)
	{
		$disp_netsafe = DB::select('select recapitems.item_id, recapitems.item_desc, recapmain.dept_sales/100 as dept_sales
		 from recapitems left outer join recapmain
		  on recapitems.item_id = recapmain.dept_id
		   and recapmain.wk_end_date = "'.$recap_date.'"
		    and recapmain.dept_sales <> 0
		     where recapmain.dept_sales IS NOT NULL
		      AND recapitems.item_type = "I"
		       order by recapitems.item_id');
		$disp_netsafe_array = json_decode(json_encode($disp_netsafe), true);
		return $disp_netsafe_array;
	}
	public function disp_tranfs($end_date)
	{
		$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
        $disp_tranfs = DB::select('SELECT recap.item_id,
                (recap.item_amt / 100) as item_amt,
                recap.wk_end_date
		           FROM recap
		          WHERE recap.item_id IN  ("T1","T2","T3","T4","T5","T6","T7")
		            AND recap.wk_end_date >= "'.$begin_date.'"
		            AND recap.wk_end_date <= "'.$end_date.'"
		            ORDER BY wk_end_date, item_id');
        $disp_tranfs_array = json_decode(json_encode($disp_tranfs), true);
		return $disp_tranfs_array;	
	}
	public function disp_del($end_date)
	{
		$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
        $disp_del = DB::select('SELECT recap.item_id,
                (recap.item_amt / 100) as item_amt,
                recap.wk_end_date
		           FROM recap
		          WHERE recap.item_id IN  ("D1","D2","D3","D4","D5","D6")
		            AND recap.wk_end_date >= "'.$begin_date.'"
		            AND recap.wk_end_date <= "'.$end_date.'"
		            AND recap.item_amt <> 0
		            ORDER BY wk_end_date, item_id');
        $disp_del_array = json_decode(json_encode($disp_del), true);
		return $disp_del_array;	
	}
	public function disp_storetx($recap_date)
	{
		$disp_storetx = DB::select('select safeitems.item_id, safeitems.item_desc, 
             recap.item_amt/100 as item_amt 
        from safeitems, recap 
         where safeitems.item_type = "S" 
           and safeitems.item_id NOT IN ("RR","LR","TD") 
          and safeitems.item_id = recap.item_id 
          and recap.wk_end_date = "'.$recap_date.'"
            and recap.item_amt <> 0 
          order by safeitems.item_id');
		$disp_storetx_array = json_decode(json_encode($disp_storetx), true);
		return $disp_storetx_array;
	}
	public function disp_potypes($recap_date)
	{
		$disp_potypes = DB::select('select safeitems.item_id, safeitems.item_desc, recap.item_amt/100 as item_amt 
       from safeitems left outer join recap 
       on safeitems.item_id = recap.item_id 
         and recap.wk_end_date = "'.$recap_date.'"
           and recap.item_amt <> 0 
           where recap.item_amt IS NOT NULL
          and  safeitems.item_type = "O" 
         order by safeitems.item_id ');
		$disp_potypes_array = json_decode(json_encode($disp_potypes), true);
		return $disp_potypes_array;
	}
	public function disp_currency($end_date)
	{
		$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
        $disp_currency = DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("C1","C2", "C3", "C4")
							 and recap.wk_end_date >= "'.$begin_date.'"
							 and recap.wk_end_date <= "'.$end_date.'"
							 and recap.item_amt <> 0
							 order by wk_end_date, item_id');
        $disp_currency_array = json_decode(json_encode($disp_currency), true);
		return $disp_currency_array;
	}
	public function disp_checks($end_date)
	{
		$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
        $disp_checks = DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN ("K1","K2") 	
							 and recap.wk_end_date >= "'.$begin_date.'"
							 and recap.wk_end_date <= "'.$end_date.'"
							 and recap.item_amt <> 0
							 order by wk_end_date, item_id');
        $disp_checks_array = json_decode(json_encode($disp_checks), true);
		return $disp_checks_array;
	}
	public function disp_express_deb($end_date)
	{
		$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
        $disp_express_deb = DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("DD")
							 and recap.wk_end_date >= "'.$begin_date.'"
							 and recap.wk_end_date <= "'.$end_date.'"
							 order by wk_end_date, item_id');
        $disp_express_deb_array = json_decode(json_encode($disp_express_deb), true);
		return $disp_express_deb_array;
	}
	public function disp_express_cred($end_date)
	{
		$begin_date = date('Y-m-d',strtotime($end_date . "-6 days"));
        $disp_express_cred = DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("CD")
							 and recap.wk_end_date >= "'.$begin_date.'"
							 and recap.wk_end_date <= "'.$end_date.'"
							 order by wk_end_date, item_id');
        $disp_express_cred_array = json_decode(json_encode($disp_express_cred), true);
		return $disp_express_cred_array;
	}
	public function get_ov($dt)
	{
		$get_ov = array();
		$drgov = 0;
		$get_ov_query = DB::select('SELECT recapmain.dept_sales as grov FROM recapmain
          WHERE recapmain.dept_id = "GO"
            AND recapmain.wk_end_date = "'.$dt.'"');
		if($get_ov_query)
		{
			$grov = $get_ov_query[0]->grov;
		}
		else
		{
			$grov = 0;
		}
		$drgov = $drgov/100 * -1;
		$grov = $grov/100 * -1;
		$get_ov['drgov'] = $drgov;
		$get_ov['grov'] = $grov;
		return $get_ov;
	}
}