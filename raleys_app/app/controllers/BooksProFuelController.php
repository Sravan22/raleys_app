<?php
class BooksProFuelController extends BaseController
{
	public function getdeliveryList()
	{
		$sql = 'select a.invoice_date, a.invoice_no, b.descr, a.gallon_count,
		        a.gallon_price, a.grade_id
		 from gas_delivery a, gas_grades b
		 where a.grade_id = b.grade_id
		 order by a.invoice_date desc, a.invoice_no desc, a.grade_id';
		$count_results = DB::select(DB::raw($sql));
		$perPage = 10;
        $page = Input::get('page', 1);
        $offset = ($page * $perPage) - $perPage;
		$sql.=" limit " . $perPage . " offset " . $offset;
		$results = DB::select(DB::raw($sql));
        $pagination = Paginator::make($results, count($count_results), $perPage);
        return View::make('mktmgr.deliverylist',compact('results','pagination')); 
	}
	public function getSalesList()
	{
		$sql = 'select a.tx_date, b.descr, a.sales, b.disp_order
				 from gas_inventory a, gas_grades b
				 where a.grade_id = b.grade_id
				 order by a.tx_date desc, b.disp_order';
		$count_results = DB::select(DB::raw($sql));
		$perPage = 10;
        $page = Input::get('page', 1);
        $offset = ($page * $perPage) - $perPage;
		$sql.=" limit " . $perPage . " offset " . $offset;
		$results = DB::select(DB::raw($sql));
        $pagination = Paginator::make($results, count($count_results), $perPage);
        return View::make('mktmgr.saleslist',compact('results','pagination')); 
	}
	public function getEndingInvList()
	{
		$sql = 'select a.tx_date, b.descr, a.act_end_inv, b.disp_order
				 from gas_inventory a, gas_grades b
				 where a.grade_id = b.grade_id
				   and b.grade_id <> "M"
				 order by a.tx_date desc, b.disp_order';
		$count_results = DB::select(DB::raw($sql));
		$perPage = 10;
        $page = Input::get('page', 1);
        $offset = ($page * $perPage) - $perPage;
		$sql.=" limit " . $perPage . " offset " . $offset;
		$results = DB::select(DB::raw($sql));
        $pagination = Paginator::make($results, count($count_results), $perPage);
        return View::make('mktmgr.endinginvlist',compact('results','pagination')); 
	}
	public function deliveryAddUpdate()
	{
		$previewssixtwodays =  date('Y-m-d', strtotime('-62 days'));
		$invoice_no_date = DB::select('select distinct invoice_date, invoice_no from gas_delivery where invoice_date >  "'.$previewssixtwodays.'"   order by 1 desc, 2');
		return View::make('mktmgr.delivery_add_update',compact('invoice_no_date')); 
	}
	public function postdeliveryAddUpdate()
	{
		$validator = Validator::make(Input::all(),
	            array(
	                   'invoice_no'=>'required',
	                   'invoice_date'=>'required'
	                ),array('invoice_no.required'=>'Invoice Number is required',
	                		'invoice_date.required'=>'Invoice Date is required'));
			if($validator->fails())
	        {          
			    return Redirect::route('bookspro-deliveryadd_update')
	                    ->withErrors($validator)
	                    ->withInput();
	        }
	        else
	        {
	        	//echo '<pre>';print_r(Input::all());
	        	$invoice_no = Input::get('invoice_no');
	        	$invoice_date = Input::get('invoice_date');
	        	$dataLocked = $this->dataLocked($invoice_date);
	        	//echo 'dataLocked '.$dataLocked;exit;
	        	//find out if another delivery invoice number has been entered
				$anzrInvNo = '';
				$anzrInvNo_sql = DB::select('select distinct invoice_no as  anzrInvNo from   gas_delivery
				 where  invoice_date = "'.$invoice_date.'"
				   and  invoice_no <> "'.$invoice_no.'" ');
				if($anzrInvNo_sql)
				{
					$invoice_date = date('m/d/Y',strtotime($invoice_date));
					$anzrInvNo = $anzrInvNo_sql[0]->anzrInvNo;
					Session::flash('alert-info','A delivery for '.$invoice_date.' already made with invoice no '.$anzrInvNo.' . Duplicate delivery');
		            return Redirect::route('bookspro-deliveryadd_update');
				}
				$invCount = DB::select('select distinct invoice_no as  anzrInvNo from   gas_delivery
				 where  invoice_date = "'.$invoice_no.'"
				   and  invoice_no <> "'.$invoice_date.'" ');
	        	if($dataLocked)
	        	{
	        		//echo 'dataLocked '.$dataLocked;exit;
	        		$invoice_date = date('m/d/Y',strtotime($invoice_date));
					Session::flash('alert-info','Data is locked for date'.' '.$invoice_date.'.'.' '.' ');
		            return Redirect::route('bookspro-deliveryadd_update');
	        	}
	        	$invCount = DB::select('select count(distinct invoice_no) 
									as invCount
									from gas_delivery
									where invoice_date = "'.$invoice_date.'" ');
	        	$invCount =  $invCount[0]->invCount;
	        	$resultsArray = $this->popDlvryArray($invoice_no,$invoice_date);
	        	$recalcAvgCost = $this->recalcAvgCost($invoice_date);
	        	$warnIfDiffPrice = $this->warnIfDiffPrice($invoice_date);
	        	return View::make('mktmgr.delivery_add_update_form',compact('resultsArray','invoice_no','invoice_date','invCount'));
	        }	
	}
	public function popDlvryArray($invoiceNo, $invoiceDate)
	{
		//echo $invoiceNo.' '.$invoiceDate;
		$resultArray = array();
		// echo 'select b.descr, a.gallon_count, a.gallon_price, b.grade_id, b.disp_order from gas_delivery a, gas_grades b where a.grade_id = b.grade_id and a.invoice_date = "'.$invoiceDate.'" and a.invoice_no = "'.$invoiceNo.'" and b.grade_id <> "M" order by b.disp_order';exit;
		$results = DB::select('select b.descr, a.gallon_count, a.gallon_price, b.grade_id, b.disp_order from gas_delivery a right outer join gas_grades b on a.grade_id = b.grade_id and a.invoice_date = "'.$invoiceDate.'" and a.invoice_no = "'.$invoiceNo.'" and b.grade_id <> "M" order by b.disp_order');
		return $resultsArray = json_decode(json_encode($results), true); 
	}
	public function warnIfDiffPrice($invoiceDate)
	{
		$diffFound = 0;
		$results1 = DB::insert('insert into tmpPriceCheck1 ( select distinct grade_id, gallon_price from gas_delivery where invoice_date = "'.$invoiceDate.'" )'  );

		$results2 = DB::insert('insert into tmpPriceCheck2 ( select grade_id, count(*) cnt from tmpPriceCheck1 group by 1 having count(*) > 1 ) ' );

		$diff_found = DB::select('select count(*) as diffFound from tmpPriceCheck2');//exit;
		DB::delete('delete from tmpPriceCheck1');
		DB::delete('delete from tmpPriceCheck2');
		$diffFound = $diff_found[0]->diffFound;
		if($diffFound > 0)
		{
			Session::flash('alert-danger','Different Gallon Price Error. The gallon price should not change on deliveries made on the same day.!.');
	        return Redirect::route('bookspro-deliveryadd_update');
		}
	}
	public function dataLocked($dt)
	{	
		$i = 0;
		$results = DB::select('select count(*) as i from locks where date_stamp = "'.$dt.'"');
		$resultsArray = json_decode(json_encode($results), true); 
		if(!empty($results))
		{
			$i = $resultsArray[0]['i'];
		}
		//echo 'returning i value '.$i;exit;
		if($i == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
		//return $i;
		//echo '<pre>';print_r($resultsArray);exit;
	}
	public function postDatadeliveryAddUpdate()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$invoice_no = Input::get('invoice_no');
		$invoice_date = Input::get('invoice_date');
		DB::delete('delete from gas_delivery where invoice_date = "'.$invoice_date.'"
				    and invoice_no   = "'.$invoice_no.'" ');
		$grade_id = Input::get('grade_id');
		//echo '<pre>';print_r($grade_id);exit; 
		//$grade_id = Input::get('grade_id');
		$gallon_count = Input::get('gallon_count');
		$gallon_price = Input::get('gallon_price');
		for($i=0;$i<count($grade_id);$i++)
		{
			DB::insert('INSERT INTO gas_delivery (invoice_date, invoice_no, grade_id, gallon_count,gallon_price) VALUES("'.$invoice_date.'","'.$invoice_no.'","'.$grade_id[$i].'","'.$gallon_count[$i].'","'.$gallon_price[$i].'")');
		}
		Session::flash('alert-success', 'Gas Delivery Details added successfully!'); 
        return Redirect::route('bookspro-deliveryadd_update');
	}
	public function getSalesDate()
	{
		return View::make('mktmgr.sales_date');
	}
	public function postSalesDate()
	{
		$validator = Validator::make(Input::all(),
	            array(
	                   'txDate'=>'required'
	                ),array('txDate.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('bookspro-salesdate')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	//echo '<pre>';print_r(Input::all());exit;
        	$txDate = Input::get('txDate');
        	$dataLocked = $this->dataLocked($txDate);
	        	//echo 'dataLocked '.$dataLocked;exit;
	        	if($dataLocked)
	        	{
	        		//echo 'dataLocked '.$dataLocked;exit;
	        		$txDate = date('m/d/Y',strtotime($txDate));
					Session::flash('alert-info','Data is locked for date'.' '.$txDate.'.'.' '.' ');
		            return Redirect::route('bookspro-salesdate');
	        	}
	        	$resultsArray = $this->popSalesArray($txDate);
	        	$recalcAvgCost = $this->recalcAvgCost($txDate);
	        	return View::make('mktmgr.sales_add_update_form',compact('resultsArray','txDate'));
        }
	}
	public function popSalesArray($txDate)
	{
		$results = DB::select('select b.descr, a.sales, b.grade_id, "N" new_entry, b.disp_order from gas_inventory a right outer join gas_grades b on a.grade_id = b.grade_id and a.tx_date = "'.$txDate.'" order by b.disp_order');
			//echo '<pre>';print_r($resultsArray);exit;		
		return $resultsArray = json_decode(json_encode($results), true);
	}
	public function postDataSalesAddUpdate()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$txDate = Input::get('txDate');
		$grade_id = Input::get('grade_id');
		$new_entry = Input::get('new_entry');
		$sales = Input::get('sales');
		for($i=0;$i<count($grade_id);$i++)
		{
			//echo $new_entry[$i].'<br>';
			if($new_entry[$i] == 'N')
			{
				//echo 'update<br>';
				//echo 'UPDATE gas_inventory SET sales = "'.$sales[$i].'" WHERE tx_date = "'.$txDate.'" AND grade_id = "'.$grade_id[$i].'" '.'<br>';
				DB::update('UPDATE gas_inventory SET sales = "'.$sales[$i].'" WHERE tx_date = "'.$txDate.'" AND grade_id = "'.$grade_id[$i].'" ');
			}
			else
			{
				//echo 'insert<br>';
				//echo 'INSERT INTO gas_inventory (tx_date, grade_id,    avg_gallon_cost,act_end_inv, sales) VALUES("'.$txDate.'","'.$grade_id[$i].'",0,0,"'.$sales[$i].'")'.'<br>';
				DB::insert('INSERT INTO gas_inventory (tx_date, grade_id,    avg_gallon_cost,act_end_inv, sales) VALUES("'.$txDate.'","'.$grade_id[$i].'",0,0,"'.$sales[$i].'")');
			}
		}
		Session::flash('alert-success', 'Sales Details Added/Updated Successfully!');
		return Redirect::route('bookspro-salesdate');
	}
	public function getEndInvDate()
	{
		return View::make('mktmgr.endinvdate');
	}
	public function postEndInvDate()
	{
		//echo '<pre>';print_r(Input::all());
		$validator = Validator::make(Input::all(),
	            array(
	                   'txDate'=>'required'
	                ),array('txDate.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('bookspro-salesdate')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$txDate = Input::get('txDate');
        	$prevEndInvExist = $this->prevEndInvExist($txDate);
	        	//echo 'prevEndInvExist '.$prevEndInvExist;exit;
	        	if($prevEndInvExist == 0)
	        	{
	        		//echo 'dataLocked '.$dataLocked;exit;
	        		$txDate = date('m/d/Y',strtotime($txDate));
					Session::flash('alert-info','Ending inventory for a previous day does not exist. Please correct!');
		            return Redirect::route('bookspro-endinvdate');
	        	}
	        	$dataLocked = $this->dataLocked($txDate);
	        	//echo 'dataLocked '.$dataLocked;exit;
	        	if($dataLocked)
	        	{
	        		//echo 'dataLocked '.$dataLocked;exit;
	        		$txDate = date('m/d/Y',strtotime($txDate));
					Session::flash('alert-info','Data is locked for date'.' '.$txDate.'.'.' '.' ');
		            return Redirect::route('bookspro-endinvdate');
	        	}
	        	$resultsArray = $this->popEndInvArray($txDate);
	        	$recalcAvgCost = $this->recalcAvgCost($txDate);
	        	return View::make('mktmgr.endinvd_add_update_form',compact('resultsArray','txDate'));
        }
	}
	public function prevEndInvExist($txDate)
	{
		//$previewsdate =  date('Y-m-d', strtotime('-1 day'));
		$previewsdate = date('Y-m-d', strtotime($txDate .' -1 day'));
		//echo $previewsdate;
		$results = DB::select('select count(*) as cntEndInv from gas_inventory where tx_date = "'.$previewsdate.'" ');
		$cntEndInv = $results[0]->cntEndInv;
		return $cntEndInv;
	}
	public function popEndInvArray($txDate)
	{
		$results = DB::select('select b.descr, a.act_end_inv, b.grade_id, "N" new_entry, b.disp_order from gas_inventory a right outer join gas_grades b on a.grade_id = b.grade_id and a.tx_date = "'.$txDate.'" and b.grade_id <> "M" order by b.disp_order');
		return $resultsArray = json_decode(json_encode($results), true);
		//echo '<pre>';print_r($resultsArray);exit;
	}
	public function postDataEndInvAddUpdate()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$txDate = Input::get('txDate');
		$grade_id = Input::get('grade_id');
		$new_entry = Input::get('new_entry');
		$act_end_inv = Input::get('act_end_inv');
		for($i=0;$i<count($grade_id);$i++)
		{
			//echo $new_entry[$i].'<br>';
			if($new_entry[$i] == 'N')
			{
				//echo 'update<br>';
				//echo 'UPDATE gas_inventory SET act_end_inv = "'.$act_end_inv[$i].'" WHERE tx_date = "'.$txDate.'" AND grade_id = "'.$grade_id[$i].'" '.'<br>';
				DB::update('UPDATE gas_inventory SET act_end_inv = "'.$act_end_inv[$i].'" WHERE tx_date = "'.$txDate.'" AND grade_id = "'.$grade_id[$i].'" ');
			}
			else
			{
				//echo 'insert<br>';
				//echo 'INSERT INTO gas_inventory (tx_date, grade_id,    avg_gallon_cost,act_end_inv, sales) VALUES("'.$txDate.'","'.$grade_id[$i].'",0,"'.$act_end_inv[$i].'",0)'.'<br>';
				DB::insert('INSERT INTO gas_inventory (tx_date, grade_id,    avg_gallon_cost,act_end_inv, sales) VALUES("'.$txDate.'","'.$grade_id[$i].'",0,"'.$act_end_inv[$i].'",0)');
			}
		}
		Session::flash('alert-success', 'Ending Inventory Details Added/Updated Successfully!');
		return Redirect::route('bookspro-endinvdate');
	}
	// public function recalcAvgCost($txDate)
	// {
	// 	$results = DB::select('select distinct tx_date from gas_inventory where tx_date >= "'.$txDate.'" ');
	// 	if($results)
	// 	{
	// 		$resultsArray = json_decode(json_encode($results), true);
	// 		for($i=0;$i<count($resultsArray);$i++)
	// 		{
	// 			$txDate =  $resultsArray[$i]['tx_date'];
	// 			$this->getBegInv($txDate);
	// 			$this->getTodaysInfo($txDate);
	// 		}
	// 	}
	// }
	public function recalcAvgCost($txDate)
	{
		//echo $txDate;exit;
		$results = DB::select('select distinct tx_date from gas_inventory where tx_date >= "'.$txDate.'" ');
		//$txDate_to_update = $txDate; 
		//echo $results[0]->tx_date;
		if($results)
		{
			//echo $results[0]->tx_date;
			$resultsArray = json_decode(json_encode($results), true);
			//echo '<pre>';print_r($resultsArray);
			for($i=0;$i<count($resultsArray);$i++)
			{
				$txDate =  $resultsArray[$i]['tx_date'];
				$this->getBegInv($txDate);
				$this->getTodaysInfo($txDate);
				//exit;
				//echo 'recalcAvgCost '.$txDate.'<br>';
				$tmpGasCalc3_results = DB::select('select * from tmpGasCalc3 order by grade_id desc ');
				
				$count = DB::select('select count(*) as total_rows from tmpGasCalc3');
				//echo 'total_rows '.$count[0]->total_rows.'<br>';
				if($count[0]->total_rows > 0)
				{
					//echo 'Tx_date Not Empty '.$txDate.' '.$count[0]->total_rows.'<br>';
					$tmpGasCalc3_resultsArray = json_decode(json_encode($tmpGasCalc3_results), true);
					//echo '<pre>';print_r($tmpGasCalc3_resultsArray);exit;
					for($j=0;$j<count($tmpGasCalc3_resultsArray);$j++)
					{
						$tmpGasCalc3_resultsArray[$j]['beg_act_inv'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$j]['beg_act_inv'],0);

						$tmpGasCalc3_resultsArray[$j]['prv_gallon_cost'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$j]['prv_gallon_cost'],$tmpGasCalc3_resultsArray[$j]['tdy_price']);
						
						$tmpGasCalc3_resultsArray[$j]['tdy_delivery'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$j]['tdy_delivery'],0);
						$tmpGasCalc3_resultsArray[$j]['tdy_price'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$j]['tdy_price'],$tmpGasCalc3_resultsArray[$j]['prv_gallon_cost']);
						//echo $tmpGasCalc3_resultsArray[$j]['tdy_price'];exit;
						//echo $tmpGasCalc3_resultsArray[$j]['tdy_delivery'];exit;
						if($tmpGasCalc3_resultsArray[$j]['grade_id'] == "M")
						{
							$tdyGallonCost = ($tmpGasCalc3_resultsArray[1]['tdy_gallon_cost']+$tmpGasCalc3_resultsArray[2]['tdy_gallon_cost'])/2;
						}
						else
						{
							// echo $tmpGasCalc3_resultsArray[$j]['prv_gallon_cost'].'<br>';
							// echo $tmpGasCalc3_resultsArray[$j]['beg_act_inv'].'<br>';
							// echo $tmpGasCalc3_resultsArray[$j]['tdy_delivery'].'<br>';
							// echo $tmpGasCalc3_resultsArray[$j]['tdy_price'].'<br>';exit;
							if($tmpGasCalc3_resultsArray[$j]['prv_gallon_cost'] == 0 || $tmpGasCalc3_resultsArray[$j]['beg_act_inv'] == 0 || $tmpGasCalc3_resultsArray[$j]['tdy_delivery'] == 0 || $tmpGasCalc3_resultsArray[$j]['tdy_price'] == 0)
							{
								$tdyGallonCost  = 0;
							}
							else
							{
								$tdyGallonCost = ((($tmpGasCalc3_resultsArray[$j]['beg_act_inv'])*($tmpGasCalc3_resultsArray[$j]['prv_gallon_cost'])) +
								(($tmpGasCalc3_resultsArray[$j]['tdy_delivery'])*($tmpGasCalc3_resultsArray[$j]['tdy_price']))) /
								(($tmpGasCalc3_resultsArray[$j]['beg_act_inv'])+($tmpGasCalc3_resultsArray[$j]['tdy_delivery']));
							}
							
						}	
						//exit;
						//echo $tdyGallonCost.'<br>';exit;
						$tdyGallonCost = number_format($tdyGallonCost, 5, '.', '');
						DB::update('update gas_inventory
				         set avg_gallon_cost = "'.$tdyGallonCost.'"
				         where tx_date = "'.$txDate.'" 
				           and grade_id = "'.$tmpGasCalc3_resultsArray[$j]['grade_id'].'" ');//exit();

					}
					DB::delete('delete from tmpGasCalc3');	
				}
				else
				{
					//echo 'Tx_date Empty '.$txDate.' '.$count[0]->total_rows.'<br>';
					DB::delete('delete from tmpGasCalc3');
				}
				
				
			}
			//exit;
		}
	}
	public function getBegInv($txDate)
	{
		//echo 'getBegInv '.$txDate.'<br>';
		DB::insert('insert into tmpGasCalc1 ( select "'.$txDate.'"  as   tx_date,
         g.grade_id     as      grade_id,
         i.tx_date      as      beg_inv_date,
         i.act_end_inv   as     beg_act_inv,
         i.avg_gallon_cost  as  prv_gallon_cost
		  from gas_inventory i   right outer join gas_grades g
		  on i.grade_id = g.grade_id
		    and i.tx_date = ( select max(tx_date) from gas_inventory
		                      where tx_date < "'.$txDate.'" ) )');
		$beg_inv_date = DB::select('select max(tx_date) as beg_inv_date from gas_inventory where tx_date < "'.$txDate.'" ');
		if($beg_inv_date)
		{
			$resultsArray = json_decode(json_encode($beg_inv_date), true);
			$beg_inv_date =  $resultsArray[0]['beg_inv_date'];
			$beg_inv_date = date('Y-m-d', strtotime($beg_inv_date .' +1 day'));
			$missingEndInvCount = DB::select('select count(*) as missingEndInvCount
			  from tmpGasCalc1 
			  where tx_date <> "'.$beg_inv_date.'" ');
			$missingEndInvCount = $missingEndInvCount[0]->missingEndInvCount;
			//echo 'missingEndInvCount '.$missingEndInvCount;
			if($missingEndInvCount > 0)
			{
				//echo 'if missingEndInvCount '.$missingEndInvCount;
				$missingDate = date('m/d/Y', strtotime($txDate .' -1 day'));
				//echo $missingDate;
				return $missingDate;
				// Session::flash('alert-danger','You MUST enter the ending inventory for '.$missingDate.',. Please fix before proceeding!.');
	   //      	return Redirect::route('bookspro-dailyreportdate');
			}
			else
			{
				return 0;
			}
		}
	}
	// public function getTodaysInfo($txDate)
	// {
	// 	//echo 'getTodaysInfo '.$txDate.'<br>';
	// 	// DB::select('create table tmpGasCalc2 as select t.*,
 //  //       i.act_end_inv  as    tdy_act_inv,
 //  //       i.avg_gallon_cost as tdy_gallon_cost,
 //  //       -1*i.sales    as     tdy_sales
	// 	//    from   gas_inventory i right outer join tmpGasCalc1 t  
	// 	//    on t.grade_id = i.grade_id and
	// 	//          i.tx_date = "'.$txDate.'" ');
	// 	// DB::select('create table tmpGasDelivery as select grade_id, sum(gallon_count) gallon_count, sum(gallon_price * gallon_count)/sum(gallon_count) gallon_price from gas_delivery where invoice_date = "'.$txDate.'" and gallon_count <> 0 and gallon_price <> 0 group by grade_id ');

	// 	// DB::select('create table tmpGasCalc3 as select t.*, d.gallon_count tdy_delivery, d.gallon_price tdy_price from tmpGasCalc2 t right outer join tmpGasDelivery d on d.grade_id = t.grade_id');
	// 	// DB::statement('drop table tmpGasCalc1');
	// 	// DB::statement('drop table tmpGasCalc2');
	// 	// DB::statement('drop table tmpGasDelivery');
	// }
	public function getTodaysInfo($txDate)
	{
		//echo 'getTodaysInfo '.$txDate.'<br>';
		DB::insert('insert into tmpGasCalc2 ( select t.*,
        i.act_end_inv  as    tdy_act_inv,
        i.avg_gallon_cost as tdy_gallon_cost,
        -1*i.sales    as     tdy_sales
		   from   gas_inventory i right outer join tmpGasCalc1 t  
		   on t.grade_id = i.grade_id and
		         i.tx_date = "'.$txDate.'" ) ');
		DB::insert('insert into tmpGasDelivery ( select grade_id, sum(gallon_count) gallon_count, sum(gallon_price * gallon_count)/sum(gallon_count) gallon_price from gas_delivery where invoice_date = "'.$txDate.'" and gallon_count <> 0 and gallon_price <> 0 group by grade_id ) ');

		DB::insert('insert into tmpGasCalc3 ( select t.*, d.gallon_count tdy_delivery, d.gallon_price tdy_price from tmpGasCalc2 t left outer join tmpGasDelivery d on d.grade_id = t.grade_id )');//exit;
		DB::delete('delete  from tmpGasCalc1');
		DB::delete('delete  from tmpGasCalc2');
		DB::delete('delete  from tmpGasDelivery');
	}
	public function valueIfNull($oldNum, $newNum)
	{
		if($oldNum == '' or $oldNum === NULL)
		{
			return $newNum;
		}
		else
		{
		  	return $oldNum;
		}
	}
	/* Fuel Reports */
	public function getDailyReportDate()
	{
		$message  = '';
		return View::make('mktmgr.daily_report_date',compact('message'));
	}
	public function postDailyReportDate()
	{
		//echo '<pre>';print_r(Input::all());
		$validator = Validator::make(Input::all(),
	            array(
	                   'txDate'=>'required'
	                ),array('txDate.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('bookspro-dailyreportdate')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
			$txDate = Input::get('txDate');
			$getBegInv = $this->getBegInv($txDate);
			$this->getTodaysInfo($txDate);
			if($getBegInv != 0)
			{	
				$msg = 'You MUST enter the ending inventory for '.$getBegInv.'. Please fix before proceeding!';
				return Redirect::route('bookspro-dailyreportdate')->with('message', $msg);;
			}

			$feesVal = 0;
			$feesVal_sql = DB::select("select value as feesVal from config where `key` = 'GAS_FEES' ");
			if($feesVal_sql)
			{
				$feesVal = $feesVal_sql[0]->feesVal;
			}
			$midGradeSales = DB::select('select tdy_sales as midGradeSales from tmpGasCalc3 where grade_id = "M" ');
			$midGradeSales = $midGradeSales[0]->midGradeSales;
			//echo 'midGradeSales '.$midGradeSales;
			$tmpGasCalc3_results = DB::select('select * from tmpGasCalc3 order by grade_id desc ');
			$count = DB::select('select count(*) as total_rows from tmpGasCalc3');
			// if($count[0]->total_rows > 0)
			// {
				$tmpGasCalc3_resultsArray = json_decode(json_encode($tmpGasCalc3_results), true);
				//echo '<pre>';print_r($tmpGasCalc3_resultsArray);
				$resultsArray = array();
				for($i=0;$i<count($tmpGasCalc3_resultsArray);$i++)
				{
					$resultsArray['grade_id'] = $tmpGasCalc3_resultsArray[$i]['grade_id'];
					$resultsArray['beg_act_inv'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$i]['beg_act_inv'], 0);
					$resultsArray['prv_gallon_cost'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$i]['prv_gallon_cost'], $tmpGasCalc3_resultsArray[$i]['tdy_price']);
					$resultsArray['tdy_gallon_cost'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$i]['tdy_gallon_cost'], $tmpGasCalc3_resultsArray[$i]['prv_gallon_cost']);
					$resultsArray['tdy_delivery'] = $tmpGasCalc3_resultsArray[$i]['tdy_delivery'];
					if($resultsArray['tdy_gallon_cost'] == 0 or $resultsArray['tdy_gallon_cost'] == '')
					{
						$resultsArray['tdy_gallon_cost'] = $resultsArray['prv_gallon_cost'];
					}
					// $totGallons = ($resultsArray['beg_act_inv']) + ($resultsArray['tdy_delivery']);
					// if($totGallons = 0 && $resultsArray['grade_id']!='M')
					// {
					// 	$resultsArray['tdy_gallon_cost'] = 0;
					// }
					
					$resultsArray['tdy_price'] = $tmpGasCalc3_resultsArray[$i]['tdy_price'];
					$resultsArray['tdy_sales'] = $tmpGasCalc3_resultsArray[$i]['tdy_sales'];
					$resultsArray['tdy_act_inv'] = $tmpGasCalc3_resultsArray[$i]['tdy_act_inv'];;
					$resultsArray['deliveries'] = 0;
					$resultsArray['sales'] = 0;
					$resultsArray['cost_of_sales'] = 0;
					$resultsArray['calc_ending_inv'] = 0;
					//$resultsArray['tdy_act_inv'] = 0;
					$resultsArray['shrink'] = 0;
					$resultsArray['shrink_cost'] = 0;
					$resultsArray['storage_fee'] = 0;
					$resultsArray1[] = $resultsArray;
				}

				$resultsArray2 = array();
				for($i=0;$i<count($resultsArray1);$i++)
				{
					$resultsArray2['grade_id'] = $resultsArray1[$i]['grade_id'];
					$resultsArray2['beg_act_inv'] = $resultsArray1[$i]['beg_act_inv'];
					$resultsArray2['prv_gallon_cost'] = $resultsArray1[$i]['prv_gallon_cost'];
					$resultsArray2['tdy_delivery'] = $resultsArray1[$i]['tdy_delivery'];
					$resultsArray2['tdy_price'] = $resultsArray1[$i]['tdy_price'];
					$resultsArray2['tdy_gallon_cost'] = $resultsArray1[$i]['tdy_gallon_cost'];
					$resultsArray2['tdy_sales'] = $resultsArray1[$i]['tdy_sales'];
					$resultsArray2['cost_of_sales'] = (-1) * ($resultsArray1[$i]['tdy_sales']) * ($resultsArray1[$i]['tdy_gallon_cost']);
					$resultsArray2['calc_ending_inv'] = 0.00;
					if($resultsArray1[$i]['grade_id'] == 'U' or $resultsArray1[$i]['grade_id'] == 'P')
			 	  	{
			 	  		$resultsArray2['calc_ending_inv'] = (($resultsArray1[$i]['beg_act_inv'] + $resultsArray1[$i]['tdy_delivery'] + $resultsArray1[$i]['tdy_sales']) + ($midGradeSales / 2)); 
			 	  	}
			 	  	if($resultsArray1[$i]['grade_id'] == 'D')
			 	  	{
			 	  		$resultsArray2['calc_ending_inv'] = (($resultsArray1[$i]['beg_act_inv'] + $resultsArray1[$i]['tdy_delivery'] + $resultsArray1[$i]['tdy_sales'])); 
			 	  	}
			 	  	$resultsArray2['tdy_act_inv'] = $this->valueIfNull($resultsArray1[$i]['tdy_act_inv'], $resultsArray2['calc_ending_inv']);

			 	  	$resultsArray2['shrink'] = $resultsArray2['tdy_act_inv'] - $resultsArray2['calc_ending_inv'];

			 	  	$resultsArray2['shrink_cost'] = (-1) * ($resultsArray2['shrink']) * ($resultsArray1[$i]['tdy_gallon_cost']);

			 	  	$resultsArray2['storage_fee'] = (-1) * ($resultsArray1[$i]['tdy_sales']) * ($feesVal);
			 	  	if($resultsArray2['shrink'] > 100 || $resultsArray2['shrink'] < -100)
			 	  	{
			 	  		$msg = 'The shrink is too high for one of the gas grades. Please fix before proceeding.';
			 	  	}
			 	  	else
			 	  	{
			 	  		$msg = '';
			 	  	}
			 	  	$resultsArray3[] = $resultsArray2;        
				}
				
				

				DB::delete('delete from tmpGasCalc3');	
			
			return View::make('mktmgr.gas_daily_report',compact('resultsArray1','resultsArray3','txDate','midGradeSales','msg'));
		}
	}
	public function getFuelReportWeeklyDate()
	{
		$message  = '';
		return View::make('mktmgr.fuel_weekly_report_date',compact('message'));
	}
	public function postFuelReportWeeklyDate()
	{
		//echo '<pre>';print_r(Input::all());exit;
		Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
            $weekday = date('N', strtotime($value));      
            if($weekday==6){
                return true;
            }            
            return false;
        },'Please enter a correct weekend date');
        
        $validator = Validator::make(Input::all(),
        array(
           'wkEndDate'=>'required|date|weekenddatecheck',
        ),array('wkEndDate.required'=>'Date is required'));
        
        if($validator->fails())
		{
			return Redirect::route('bookspro-fuel_weekly_report_date')
					->withErrors($validator)
					->withInput();
		}
		else
		{
			$wkEndDate = Input::get('wkEndDate');
			$txDate = date('Y-m-d', strtotime($wkEndDate .' -6 day'));
			//echo $wkEndDate.'<br>';
			//echo $txDate;
			$resultsArray = array();
			$resultsArray2 = array();
			$resultsArray4 = array();
			//$deliveries = 0;
			while ($txDate  <= $wkEndDate)
			{
				//echo 'while txDate '.$txDate.'<br>';
				$getBegInv = $this->getBegInv($txDate);
				$this->getTodaysInfo($txDate);
					$feesVal = 0;
				$feesVal_sql = DB::select("select value as feesVal from config where `key` = 'GAS_FEES' ");
				if($feesVal_sql)
				{
					$feesVal = $feesVal_sql[0]->feesVal;
				}
				$midGradeSales = DB::select('select tdy_sales as midGradeSales from tmpGasCalc3 where grade_id = "M" ');
				$midGradeSales = $midGradeSales[0]->midGradeSales;
				 $tmpGasCalc3_results = DB::select('select * from tmpGasCalc3 order by grade_id desc ');
				$count = DB::select('select count(*) as total_rows from tmpGasCalc3');
				$tmpGasCalc3_resultsArray = json_decode(json_encode($tmpGasCalc3_results), true);
				//echo count($tmpGasCalc3_resultsArray).'<br>';
				for($i=0;$i<count($tmpGasCalc3_resultsArray);$i++)
				{
					//echo 'tmpGasCalc3_resultsArray '.$tmpGasCalc3_resultsArray[$i]['tx_date'].'<br>';
					$resultsArray['tx_date'] = $tmpGasCalc3_resultsArray[$i]['tx_date'];
					$resultsArray['grade_id'] = $tmpGasCalc3_resultsArray[$i]['grade_id'];
					$resultsArray['beg_act_inv'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$i]['beg_act_inv'], 0);
					$resultsArray['prv_gallon_cost'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$i]['prv_gallon_cost'], $tmpGasCalc3_resultsArray[$i]['tdy_price']);
					$resultsArray['tdy_gallon_cost'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$i]['tdy_gallon_cost'], $tmpGasCalc3_resultsArray[$i]['prv_gallon_cost']);
					$resultsArray['tdy_delivery'] = $tmpGasCalc3_resultsArray[$i]['tdy_delivery'];
					if($resultsArray['tdy_gallon_cost'] == 0 or $resultsArray['tdy_gallon_cost'] == '')
					{
						$resultsArray['tdy_gallon_cost'] = $resultsArray['prv_gallon_cost'];
					}
					$resultsArray['tdy_price'] = $tmpGasCalc3_resultsArray[$i]['tdy_price'];
					$resultsArray['tdy_sales'] = $tmpGasCalc3_resultsArray[$i]['tdy_sales'];
					$resultsArray['tdy_act_inv'] = $tmpGasCalc3_resultsArray[$i]['tdy_act_inv'];;
					//$resultsArray['deliveries'] = $deliveries + $resultsArray['tdy_delivery'];
					//echo 'tdy_delivery '.$tmpGasCalc3_resultsArray[$i]['tdy_delivery'].'<br>';
					//echo 'deliveries in loop '.$deliveries.'<br>';	
					//$resultsArray['deliveries'] = 0;
					$resultsArray['sales'] = 0;
					$resultsArray['cost_of_sales'] = (-1) * ($tmpGasCalc3_resultsArray[$i]['tdy_sales']) * ($tmpGasCalc3_resultsArray[$i]['tdy_gallon_cost']);;
					
					$resultsArray['calc_ending_inv'] = 0.00;
					if($tmpGasCalc3_resultsArray[$i]['grade_id'] == 'U' or $tmpGasCalc3_resultsArray[$i]['grade_id'] == 'P')
			 	  	{
			 	  		$resultsArray['calc_ending_inv'] = (($tmpGasCalc3_resultsArray[$i]['beg_act_inv'] + $tmpGasCalc3_resultsArray[$i]['tdy_delivery'] + $tmpGasCalc3_resultsArray[$i]['tdy_sales']) + ($midGradeSales / 2)); 
			 	  	}
			 	  	if($tmpGasCalc3_resultsArray[$i]['grade_id'] == 'D')
			 	  	{
			 	  		$resultsArray['calc_ending_inv'] = (($tmpGasCalc3_resultsArray[$i]['beg_act_inv'] + $tmpGasCalc3_resultsArray[$i]['tdy_delivery'] + $tmpGasCalc3_resultsArray[$i]['tdy_sales'])); 
			 	  	}
			 	  	$resultsArray['tdy_act_inv'] = $this->valueIfNull($tmpGasCalc3_resultsArray[$i]['tdy_act_inv'], $resultsArray['calc_ending_inv']);

			 	  	$resultsArray['shrink'] = $resultsArray['tdy_act_inv'] - $resultsArray['calc_ending_inv'];

			 	  	$resultsArray['shrink_cost'] = (-1) * ($resultsArray['shrink']) * ($tmpGasCalc3_resultsArray[$i]['tdy_gallon_cost']);

			 	  	$resultsArray['storage_fee'] = (-1) * ($tmpGasCalc3_resultsArray[$i]['tdy_sales']) * ($feesVal);
					/**/
					// $resultsArray['calc_ending_inv'] = 0;
					// $resultsArray['shrink'] = 0;
					// $resultsArray['shrink_cost'] = 0;
					// $resultsArray['storage_fee'] = 0;
					$resultsArray1[] = $resultsArray;
					//unset ($resultsArray);
				}
				
				$resultsArray3[] = $resultsArray1;
				unset ($resultsArray1);
				$txDate = date('Y-m-d', strtotime($txDate .' +1 day'));
				DB::delete('delete from tmpGasCalc3');	//exit;
				//echo 'deliveries '.$deliveries.'<br>';	

			}
			/*
			$deliveries = 0;
			$sales = 0;
			$cost_of_sales = 0;
			$shrink = 0;
			$shrink_cost = 0;
			$storage_fee = 0;
			for($i=0;$i<count($resultsArray3);$i++)
			{
				//echo '<pre>';print_r($resultsArray3[$i]);
				$deliveries = $deliveries + $resultsArray3[$i][0]['tdy_delivery'];
				$sales = $sales + $resultsArray3[$i][0]['tdy_sales'];
				$cost_of_sales = $cost_of_sales + $resultsArray3[$i][0]['cost_of_sales'];
				$shrink = $shrink + $resultsArray3[$i][0]['shrink'];
				$shrink_cost = $shrink_cost + $resultsArray3[$i][0]['shrink_cost'];
				$storage_fee = $storage_fee + $resultsArray3[$i][0]['storage_fee'];
			}
			echo $deliveries.' '.$sales.' '.$cost_of_sales.' '.$shrink.' '.$shrink_cost.' '.$storage_fee;	
			*/
			

			
			//echo count($tmpGasCalc3_resultsArray).'<br>';
			//echo 'resultsArray3 '.'<pre>',print_r($resultsArray3);
			$deliveries_array = array();
			$sales_array = array();
			$cost_of_sales_array = array();
			$shrink_array = array();
			$shrink_cost_array = array();
			$storage_fee_array = array();
			$finalarray =array();
			for($j=0;$j<count($tmpGasCalc3_resultsArray);$j++)
			{
				$deliveries = 0;
				$sales = 0;
				$cost_of_sales = 0;
				$shrink = 0;
				$shrink_cost = 0;
				$storage_fee = 0;
				
				for($i=0;$i<count($resultsArray3);$i++)
				{
					//echo 'I Value '.$i.' J Value '.$j.'<br>';//exit;
					//echo '<pre>';print_r($resultsArray3[$i]);
					$deliveries = $deliveries + $resultsArray3[$i][$j]['tdy_delivery'];
					//echo $deliveries.'<br>';
					$sales = $sales + $resultsArray3[$i][$j]['tdy_sales'];
					$cost_of_sales = $cost_of_sales + $resultsArray3[$i][$j]['cost_of_sales'];
					$shrink = $shrink + $resultsArray3[$i][$j]['shrink'];
					$shrink_cost = $shrink_cost + $resultsArray3[$i][$j]['shrink_cost'];
					$storage_fee = $storage_fee + $resultsArray3[$i][$j]['storage_fee'];
				}
				//echo $deliveries.'<br>';//exit;
				//echo 'deliveries_array '.$deliveries.'<br>';exit;
				$deliveries_array[] = $deliveries;
				$sales_array[] = $sales;
				$cost_of_sales_array[] = $cost_of_sales;
				$shrink_array[] = $shrink;
				$shrink_cost_array[] = $shrink_cost;
				$storage_fee_array[] = $storage_fee;
				//unset($resultsArray3[$i]);
				// $finalarray[] = $deliveries_array1;
				// $finalarray[] = $sales_array1;
				// $finalarray[] = $cost_of_sales_array1;
				// $finalarray[] = $shrink_array1;
				// $finalarray[] = $shrink_cost_array1;
				// $finalarray[] = $storage_fee_array1;

			}

			// echo '<pre>';print_r($deliveries_array);
			// echo '<pre>';print_r($sales_array);
			// echo '<pre>';print_r($cost_of_sales_array);
			// echo '<pre>';print_r($shrink_array);
			// echo '<pre>';print_r($shrink_cost_array);
			// echo '<pre>';print_r($storage_fee_array);
			//echo '<pre>';print_r($finalarray);
			//echo $deliveries.' '.$sales.' '.$cost_of_sales.' '.$shrink.' '.$shrink_cost.' '.$storage_fee;

			//exit;
			return View::make('mktmgr.gas_weekly_report',compact('wkEndDate','deliveries_array','sales_array','cost_of_sales_array','shrink_array','shrink_cost_array','storage_fee_array'));
		}
	}
}