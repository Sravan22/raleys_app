<?php
class ActSupController extends BaseController
{
	public function unlockTableforDate()
	{
		$msg = '';
		$unlockdate = Input::get('unlockdate');
		//echo $unlockdate.'<br>';$weekday = date('l', strtotime($row['wk_end_date']));
		
		$todaysdate = date('Y-m-d');
		$previewsoneday =  date('Y-m-d', strtotime('-1 day'));
		$previewstwodays =  date('Y-m-d', strtotime('-2 days'));
		$day_of_week_today = date('N', strtotime($todaysdate));
		//echo $day_of_week_today;
		//echo $todaysdate;
		
		if($unlockdate > $todaysdate)
		{
			//$msg = 'DATE ENTERED CANNOT BE GREATER THAN TODAY';
			Session::flash('alert-info','DATE ENTERED CANNOT BE GREATER THAN TODAY');
	            	return Redirect::route('user-home');
		}
			if($day_of_week_today == 0)
			{
				if($unlockdate < $todaysdate)
				{
					Session::flash('alert-info','TODAY is SUNDAY, you cannot UNLOCK prior WEEK');
	            	return Redirect::route('user-home');
				}
			}
			else
			{
				if($day_of_week_today == 1)
				{
					if($unlockdate < $previewsoneday)
					{
						//$msg = 'You cannot UNLOCK prior WEEK.';
						Session::flash('alert-info','You cannot UNLOCK prior WEEK.');
	            		return Redirect::route('user-home');
					}
				}
				else
				{
					if($unlockdate < $previewstwodays)
					{
						//$msg = 'You can only UNLOCK the last 2 days.';
						Session::flash('alert-info','You can only UNLOCK the last 2 days.');
	            		return Redirect::route('user-home');
					}
				}
			//}
				//echo 'SELECT count(*)  as reccount  FROM mgmt WHERE mgmt.date_stamp = "'.$unlockdate.'" ';exit;
		$results = DB::select('SELECT count(*)  as reccount  FROM mgmt WHERE mgmt.date_stamp = "'.$unlockdate.'" ');	
		$reccount = json_decode(json_encode($results), true); 
		//echo '<pre>';print_r($reccount);exit;
		$reccount = $reccount[0]['reccount'];
			if($reccount == 0)
			{
				Session::flash('alert-info','DATE ENTERED is not on the TABLE, select another date to UNLOCK.');
	            		return Redirect::route('user-home');
			}
			else
			{
				$whereArray = array('date_stamp'=>$unlockdate);
				DB::table('mgmt')->where($whereArray)->delete();
				Session::flash('alert-info','TABLE is successfully UNLOCKED.');
	            		return Redirect::route('user-home');
			}
		}
		//return View::make('actsup.homepage',compact('msg'));
	}
}