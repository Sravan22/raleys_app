<?php

class EpsPrntController extends BaseController {

public function getepsprnt(){
	return View::make('mktmgr.bookepsprnt');
}
public function getepsprntdatesubmit(){
	$work_data = array();
    
    $date = Input::get('end_date');
    
    $begin_date = date('Y-m-d',strtotime($date . "-6 days"));
    
    $result_set['result_set']=DB::select('SELECT *
      FROM eps
      WHERE eps_date <= "'.$date.'"
        AND eps_date >= ("'.$date.'" - INTERVAL 6 DAY)
       ORDER BY eps_date');
    

    $total_prev_pend = DB::select("SELECT  SUM(prev_pend_deb +  prev_pend_ebt +prev_pend_gc  +  prev_pend_sc  +  prev_pend_ec  +prev_pend_cr)/100 
    							 as total_prev FROM eps
							      WHERE eps_date <='{$date}'
							      AND eps_date >= '{$begin_date}'
							      ORDER BY eps_date");

    	$work_data['total_prev'] = $total_prev_pend[0]->total_prev;
    
     $total_net_sales = DB::select("SELECT  SUM(net_sales_deb +  net_sales_ebt +
       net_sales_gc  +  net_sales_sc  +  net_sales_ec  +   net_sales_cr)/100 
    							 as total_net FROM eps
							      WHERE eps_date <='{$date}'
							      AND eps_date >= '{$begin_date}'
							      ");
     $work_data['total_net'] = $total_net_sales[0]->total_net;

     $total_curr_pend = DB::select("SELECT  SUM(curr_pend_deb +  curr_pend_ebt +
       curr_pend_gc  +  curr_pend_sc  +  curr_pend_ec  +   curr_pend_cr)/100 
    							 as total_curr FROM eps
							      WHERE eps_date <='{$date}'
							      AND eps_date >= '{$begin_date}'
							      ");
     $work_data['total_curr'] = $total_curr_pend[0]->total_curr;
    
     $total_offline_rej= DB::select("SELECT  SUM(offline_rej_deb +  offline_rej_ebt +
       offline_rej_gc  +  offline_rej_sc  +  offline_rej_ec  +   offline_rej_cr)/100 
    							 as total_rej FROM eps
							      WHERE eps_date <='{$date}'
							      AND eps_date >= '{$begin_date}'
							      ");

     $work_data['total_rej'] = $total_offline_rej[0]->total_rej;
    
     $total_dep_sum= DB::select("SELECT  SUM(dep_sum_deb +  dep_sum_ebt +
       dep_sum_gc  +  dep_sum_sc  +  dep_sum_ec  +   dep_sum_cr)/100 
    							 as total_dep FROM eps
							      WHERE eps_date <='{$date}'
							      AND eps_date >= '{$begin_date}'
							      ");
     $work_data['total_dep'] = $total_dep_sum[0]->total_dep;
     $total_host= DB::select("SELECT SUM(
					(prev_pend_deb +  net_sales_deb - curr_pend_deb - offline_rej_deb) + 
					(prev_pend_ebt +net_sales_ebt -curr_pend_ebt -offline_rej_ebt) +
					(prev_pend_gc  +net_sales_gc  -curr_pend_gc  -offline_rej_gc ) +
					(prev_pend_sc  +net_sales_sc  -curr_pend_sc  -offline_rej_sc ) +
					(prev_pend_ec  +net_sales_ec  -curr_pend_ec  -offline_rej_ec ) +
					(prev_pend_cr + net_sales_cr - curr_pend_cr - offline_rej_cr)) / 100 
    							 as total_host FROM eps
							      WHERE eps_date <='{$date}'
							      AND eps_date >= '{$begin_date}'
							      ");	
     $work_data['total_host'] = $total_host[0]->total_host;
     $total_calc_host= DB::select("SELECT SUM(
					(prev_pend_deb + net_sales_deb - curr_pend_deb - offline_rej_deb) + 
					(prev_pend_ebt +net_sales_ebt -curr_pend_ebt -offline_rej_ebt) +
					(prev_pend_gc  +net_sales_gc  -curr_pend_gc  -offline_rej_gc ) +
					(prev_pend_sc  +net_sales_sc  -curr_pend_sc  -offline_rej_sc ) +
					(prev_pend_ec  +net_sales_ec  -curr_pend_ec  -offline_rej_ec ) +
					(prev_pend_cr + net_sales_cr - curr_pend_cr - offline_rej_cr)) / 100 
    							 as total_cal_host FROM eps
							      WHERE eps_date <='{$date}'
							      AND eps_date >= '{$begin_date}'
							      ");	
       $work_data['total_cal_host'] = $total_calc_host[0]->total_cal_host;
      $total_diff= DB::select("SELECT SUM(
(dep_sum_deb - (prev_pend_deb + net_sales_deb - curr_pend_deb - offline_rej_deb)) + 
(dep_sum_ebt -(prev_pend_ebt +net_sales_ebt -curr_pend_ebt -offline_rej_ebt)) +
(dep_sum_gc  -(prev_pend_gc  +net_sales_gc  -curr_pend_gc  -offline_rej_gc )) +
(dep_sum_sc  -(prev_pend_sc  +net_sales_sc  -curr_pend_sc  -offline_rej_sc )) +
(dep_sum_ec  -(prev_pend_ec  +net_sales_ec  -curr_pend_ec  -offline_rej_ec )) +
(dep_sum_cr - (prev_pend_cr + net_sales_cr - curr_pend_cr - offline_rej_cr))) / 100
    							 as total_diff FROM eps
							      WHERE eps_date <='{$date}'
							      AND eps_date >= '{$begin_date}'
							      ");
      
      $work_data['total_diff'] = $total_diff[0]->total_diff;
	return View::make('mktmgr.bookepsprntdatesubmit',$result_set,compact('work_data'))->with('date',$date);
}
}
