<?php

class StoreTransfersControler extends  BaseController
{
	
	public function storeTransfersview()
	{
		return View::make('mktmgr.storetransfersquery');
	}

	public function getStoreTransfersQuery()
	{
		return View::make('mktmgr.storetransfersquery');
	}

	public function getStoreTransfersBrowse()
	{
		return View::make('mktmgr.storetransfersbrowse');
	}

	public function getStoreTransfersUpdate()
	{
		return View::make('mktmgr.storetransfersupdate');
	}

	public function getStoreTransfersItems()
	{
		return View::make('mktmgr.storetransfersitems');
	}

	public function getStoreTransfersOperators()
	{
		return View::make('mktmgr.storetransfersoperators');
	}
	public function StoreTransferItemMsg()
	{
		return View::make('mktmgr.storetransfersitemsmsg');
	}
	public function getStoreTransferHardcopy()
	{
		return View::make('mktmgr.storetransfersitemsmsg');
	}
	public function StoreTransferUpdateMsg()
	{
		return View::make('mktmgr.storetransfersitemsmsg');
	}
	public function getStoreTransfersWeeklyreport()
	{
		$sodsdcwk_rec = DB::select('SELECT * FROM sodsdcwk where seq_number = 1 ');
		$sodsdcwk_rec_array = json_decode(json_encode($sodsdcwk_rec), true);	
		return View::make('mktmgr.storetransferweeklyreport',compact('sodsdcwk_rec_array'));	
	}
	public function postStoreWeeklyRepot()
	{	
		//echo 'postStoreWeeklyRepot';exit;
		$current_date =  date('Y-m-d');
		//echo $current_date."<br>";
		$start_date = Input::get('cur_wk_bgn_date');
		$start_datec =date("m-d-Y",strtotime(trim(Input::get('cur_wk_bgn_date'))));
		//echo $cur_wk_bgn_date;exit;
		$end_date = Input::get('cur_wk_end_date');
		$end_datec =date("m-d-Y",strtotime(trim(Input::get('cur_wk_end_date'))));
		//$copies = Input::get('copies');
		if(!($start_date<=$current_date))
		{
		 	Session::flash('alert-danger', 'Invalid Start Date!'); 
          	return Redirect::route('mktmgr-storetransferweeklyreport');
		}
		if($end_date == $start_date || $end_date > $start_date)
		{
			// $reportquery = DB::select('select * from sodsdhdr where create_datetime >= "'.$start_date.'" and create_datetime <= "'.$end_date.'"');
			// $reportquery_array = json_decode(json_encode($reportquery), true);
			// return View::make('mktmgr.storetransfersweeklyreport',compact('reportquery_array'));
			//echo 'Work in Progress!!';

		/*$reportquery =DB::select('select * from soxfrhdr where soxfrhdr.create_datetime >= "'.$start_date.'"  and 
				        soxfrhdr.create_datetime <= "'.$end_date.'" and soxfrhdr.status not in ("D", "V")
						order by soxfrhdr.transfer_type, soxfrhdr.create_date,soxfrhdr.transfer_number, soxfrhdr.to_dept_no;'); */
		

		$out_dept_charges =DB::select('select * from soxfrhdr where soxfrhdr.create_datetime >= "'.$start_date.'" 
						and soxfrhdr.create_datetime <= "'.$end_date.'"
						and soxfrhdr.from_store_no = "305"
						and soxfrhdr.from_dept_no in (select gl_dept from soxfrdpt
						where soxfrdpt.gl_dept >= "0"
						and soxfrdpt.gl_dept <= "100")
						and soxfrhdr.from_account in (17100, 65100, 66000, 65000)
						and soxfrhdr.status not in ("D", "V")
						order by soxfrhdr.transfer_type, soxfrhdr.create_date, 
						soxfrhdr.transfer_number, soxfrhdr.from_dept_no');

		$out_dept_credits =DB::select('select * from soxfrhdr where soxfrhdr.create_datetime >= "'.$start_date.'" 
						and soxfrhdr.create_datetime <= "'.$end_date.'"
						and soxfrhdr.from_store_no = "305"
						and soxfrhdr.from_dept_no in (select gl_dept from soxfrdpt
						where soxfrdpt.gl_dept >= "0"
						and soxfrdpt.gl_dept <= "100")
						and soxfrhdr.from_account in (17100, 65100, 66000, 65000)
						and soxfrhdr.status not in ("D", "V")
						order by soxfrhdr.transfer_type, soxfrhdr.create_date, 
						soxfrhdr.transfer_number, soxfrhdr.from_dept_no');
			$reportquery_array = array_merge($out_dept_charges, $out_dept_credits);
        	$reportquery_array = json_decode(json_encode($reportquery_array), true);
	return View::make('mktmgr.storetransfersweeklyreport',compact('reportquery_array', 'start_date', 'end_date'));
	}
		else
		{
			Session::flash('alert-danger', 'Invalid Date Range. Start Date should be less than End date!'); 
          	return Redirect::route('mktmgr-storetransferweeklyreport');
		}
		
	}

	

	public function getStoreTransfersWeeklyStatus()
	{
		return View::make('mktmgr.storetransferweeklystatus');
	}

	public function postdateWeeklyTrans()
	{
		$fromdate = Input::get('fromdate'); $todate = Input::get('todate');
		$validator = Validator::make(Input::all(),
            array(
                   'fromdate'=>'required|date|before:todate',
                   'todate'=>'required|date|after:fromdate',
                ),array('fromdate.required'=>'Date is required','todate.required'=>'Date is required'));
        if($validator->fails())
        {
            
            return Redirect::route('mktmgr-storetransfersweeklystatus')
                    ->withErrors($validator)
                    ->withInput();
        }

        else
        { 
			$reportquery =DB::select('select * from soxfrhdr where soxfrhdr.create_datetime >= "'.$fromdate.'" 
						and soxfrhdr.create_datetime <= "'.$todate.'"
						and soxfrhdr.to_store_no = "305"
						and soxfrhdr.to_dept_no in (select gl_dept from soxfrdpt
						where soxfrdpt.gl_dept >= "0"
						and soxfrdpt.gl_dept <= "100")
						and soxfrhdr.from_account in (17100, 65100, 66000, 65000)
						and soxfrhdr.status not in ("D", "V")
						order by soxfrhdr.transfer_type, soxfrhdr.create_date, 
						soxfrhdr.transfer_number, soxfrhdr.from_dept_no');
			$reportquery_array = json_decode(json_encode($reportquery), true);
		return View::make('mktmgr.storetransfersweeklystatusreport',compact('reportquery_array', 'fromdate', 'todate'));
        }
	}

	/*public function postStoreTransfer()
	{
		//$store_transfer = new StoreTransfer;
		$validator = Validator::make(Input::all(),
            array(
                   'fromstorenumber' =>'required'
                ),array(
                   'fromstorenumber.required' =>'From Store Number is Required'
                ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-storetransfersquery')
                    ->withErrors($validator)
                    ->withInput();
        }

        else
        {
        $inputdata = Input::all();
		$fromstorenumber = Input::get('fromstorenumber');
		$fromdept = Input::get('fromdept');
		$acct_credit = Input::get('acct_credit');
		$to_store = Input::get('to_store');
		$to_dept = Input::get('to_dept');
		$acct_charge = Input::get('acct_charge');
		$transfer_date = Input::get('transfer_date');
		$transfer_number = Input::get('transfer_number');
		$transfer_type = Input::get('transfer_type');
		$emp_id = Input::get('emp_id');
		$status = Input::get('status');
		
		$select = "select transfer_number,seq_number,create_date,seq_number,from_store_no,employee_id,transfer_type,from_dept_no,to_store_no,to_dept_no,from_account,from_account_desc,to_account,to_account_desc,tot_line_items,tot_value,status from soxfrhdr ";
        $where = 'where 1 = 1 ';
	            	if(Input::has('fromstorenumber'))
	            	{
	            		$where.="and  from_store_no = '$fromstorenumber' ";
                    }
                	if(Input::has('fromdept'))
            		{
            			$where.=" and from_dept_no = '$fromdept' ";
                	}
                	if(Input::has('acct_credit'))
            		{
            			$where.=" and from_account = '$acct_credit' ";
                	}
                	if(Input::has('to_store'))
            		{
            			$where.=" and to_store_no = '$to_store' ";
                	}
                	if(Input::has('to_dept'))
            		{
            			$where.=" and to_dept_no = '$to_dept' ";
                	}
                	if(Input::has('acct_charge'))
            		{
            			$where.=" and to_account = '$acct_charge' ";
                	}
                	if(Input::has('transfer_date'))
            		{
            			$where.=" and create_date = '$transfer_date' ";
                	}
                	if(Input::has('transfer_number'))
            		{
            			$where.=" and transfer_number = '$transfer_number' ";
                	}
                	if(Input::has('transfer_type'))
            		{
            			$where.=" and transfer_type = '$transfer_type' ";
                	}
                	if(Input::has('emp_id'))
            		{
            			$where.=" and employee_id = '$emp_id' ";
                	}
                	if(Input::has('status'))
            		{
            			$where.=" and status = '$status' ";
                	}
                	$order="ORDER BY create_date desc";
                $query=$select.$where.$order;
            	$store_transfer_result = DB::select($query);
            	//echo count($store_transfer_result);exit;
            	//$pagination = Paginator::make($store_transfer_result, count($store_transfer_result), 10);
            	
          	return View::make('mktmgr.storetransfersbrowse', compact('store_transfer_result'));
        }
        //echo $validator;exit;
		
	}*/
	public function postStoreTransfer()
	{
		$work_data = $results = new StdClass();

		$this->from_store_no = Input::get('from_store_no');
		$this->from_dept_no = Input::get('from_dept_no');
		$this->from_account = Input::get('from_account');
		$this->to_store_no = Input::get('to_store_no');
		$this->to_dept_no = Input::get('to_dept_no');
		$this->to_account = Input::get('to_account');
		$this->create_date = Input::get('create_date');
		$this->transfer_number = Input::get('transfer_number');
		$this->transfer_type = Input::get('transfer_type');
		$this->employee_id = Input::get('employee_id');
		$this->status = Input::get('status');

		$paginate_array = array();
      $sql = "SELECT * FROM soxfrhdr WHERE 1=1 ";

  if ($this->from_store_no != '') {
        $sql.=" AND from_store_no='" . $this->from_store_no . "' ";
        $paginate_array['from_store_no'] = $this->from_store_no;
    }
    if ($this->from_dept_no != '') {
        $sql.=" AND from_dept_no='" . $this->from_dept_no . "' ";
        $paginate_array['from_dept_no'] = $this->from_dept_no;
    }
    if ($this->from_account != '') {
        $sql.=" AND from_account='" . $this->from_account . "' ";
        $paginate_array['from_account'] = $this->from_account;
    }
    if ($this->to_store_no != '') {
        $sql.=" AND to_store_no='" . $this->to_store_no . "' ";
        $paginate_array['to_store_no'] = $this->to_store_no;
    }
        if ($this->to_dept_no != '') {
            $sql.=" AND to_dept_no='" . $this->to_dept_no . "' ";
            $paginate_array['to_dept_no'] = $this->to_dept_no;
        }
        if ($this->to_account != '') {
            $sql.=" AND to_account='" . $this->to_account . "' ";
            $paginate_array['to_account'] = $this->to_account;
        }
        if ($this->create_date != '') {
            $sql.=" AND create_date='" . $this->create_date . "' ";
            $paginate_array['create_date'] = $this->create_date;
        }
        if ($this->transfer_number != '') {
            $sql.=" AND transfer_number='" . $this->transfer_number . "' ";
            $paginate_array['transfer_number'] = $this->transfer_number;
        }
        if ($this->transfer_type != '') {
            $sql.=" AND transfer_type='" . $this->transfer_type . "' ";
            $paginate_array['transfer_type'] = $this->transfer_type;
        }
        if ($this->employee_id != '') {
            $sql.=" AND employee_id='" . $this->employee_id . "' ";
            $paginate_array['employee_id'] = $this->employee_id;
        }
        if ($this->status != '') {
            $sql.=" AND status='" . $this->status . "' ";
            $paginate_array['status'] = $this->status;
        }
        $sql.=" order by create_datetime desc,transfer_number desc";
        $count_results = DB::select(DB::raw($sql));
         $page = Input::get('page', 1); // Get the current page or default to 1, this is what you miss!
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        $sql.=" limit " . $perPage . " offset " . $offset;

        $results = DB::select(DB::raw($sql));
$report_data=array(
                          'from_store_no' => $this->from_store_no,
                          'from_dept_no' => $this->from_dept_no,
                          'from_account' => $this->from_account,
                          'to_store_no' => $this->to_store_no,
                          'to_dept_no' => $this->to_dept_no,
                          'to_account' => $this->to_account,
                          'create_date' => $this->create_date,
                          'transfer_number' =>$this->transfer_number,
                          'transfer_type' => $this->transfer_type,
                          'employee_id' => $this->employee_id,
                          'status' => $this->status
                          );

        if (!empty($count_results)) {
            
        } else {

            Session::flash('alert-danger', " No Store Transfers meet Query criteria.  ");
            return Redirect::route('mktmgr-storetransfersquery')->withInput();
        }

		$work_data = $results;

        $pagination = Paginator::make($results, count($count_results), $perPage);

        $pagination->appends($paginate_array);

        return View::make('mktmgr.storetransfersbrowse')->with(array('data' => $work_data, 'pagination' => $pagination, 'report_data' => $report_data));
	}
	public function postStoreTransfer1()
	{
		//$store_transfer = new StoreTransfer;
		$inputdata = Input::all();
		$fromstorenumber = Input::get('fromstorenumber');
		$fromdept = Input::get('fromdept');
		$acct_credit = Input::get('acct_credit');
		$to_store = Input::get('to_store');
		$to_dept = Input::get('to_dept');
		$acct_charge = Input::get('acct_charge');
		$transfer_date = Input::get('transfer_date');
		//echo $transfer_date;exit;
		$transfer_number = Input::get('transfer_number');
		$transfer_type = Input::get('transfer_type');
		$emp_id = Input::get('emp_id');
		$status = Input::get('status');
		
		$select = "select * from soxfrhdr ";
        $where = 'where 1 = 1 ';
	            	if(Input::has('fromstorenumber'))
	            	{
	            		$where.="and  from_store_no = '$fromstorenumber' ";
                    }
                	if(Input::has('fromdept'))
            		{
            			$where.=" and from_dept_no = '$fromdept' ";
                	}
                	if(Input::has('acct_credit'))
            		{
            			$where.=" and from_account = '$acct_credit' ";
                	}
                	if(Input::has('to_store'))
            		{
            			$where.=" and to_store_no = '$to_store' ";
                	}
                	if(Input::has('to_dept'))
            		{
            			$where.=" and to_dept_no = '$to_dept' ";
                	}
                	if(Input::has('acct_charge'))
            		{
            			$where.=" and to_account = '$acct_charge' ";
                	}
                	if(Input::has('transfer_date'))
            		{
            			$where.=" and create_date = '$transfer_date' ";
                	}
                	if(Input::has('transfer_number'))
            		{
            			$where.=" and transfer_number = '$transfer_number' ";
                	}
                	if(Input::has('transfer_type'))
            		{
            			$where.=" and transfer_type = '$transfer_type' ";
                	}
                	if(Input::has('emp_id'))
            		{
            			$where.=" and employee_id = '$emp_id' ";
                	}
                	if(Input::has('status'))
            		{
            			$where.=" and status = '$status' ";
                	}
                	$order="ORDER BY create_date desc";
                $query=$select.$where.$order;
            	$store_transfer_result = DB::select($query);
            	$vendors = DB::table('sodsddtl')->select('store_qty', 'upc_number', 'delvry_unt_ovrd', 'store_cost', 'store_allow', 'vendor_cost', 'vendor_allow')->orderBy('upc_number', 'asc')->paginate(10);
          	return View::make('mktmgr.storetransfersbrowse', compact('store_transfer_result'));
	}
	public function postStoreItemView()
	{
		$seq_number = Input::get('seq_number');

		// $transfer_number = Input::get('transfer_number');
		// $from_store_no = Input::get('from_store_no');
		// $from_dept_no = Input::get('from_dept_no');
		// $to_store_no = Input::get('to_store_no');
		// $to_dept_no = Input::get('to_dept_no');
		// $from_account = Input::get('from_account');
		// $from_account_desc = Input::get('from_account_desc');
		// $to_account = Input::get('to_account');
		// $to_account_desc = Input::get('to_account_desc');
		// $tot_value = Input::get('tot_value');
		$item_upper_part_details = array(
                        'transfer_number' =>  Input::get('transfer_number'),
                        'from_store_no' => Input::get('from_store_no'),
                        'from_dept_no' => Input::get('from_dept_no'),
                        'to_store_no' => Input::get('to_store_no'),
                        'to_dept_no' => Input::get('to_dept_no'),
                        'from_account' => Input::get('from_account'),
                        'from_account_desc' => Input::get('from_account_desc'),
                        'to_account' => Input::get('to_account'),
                        'to_account_desc' => Input::get('to_account_desc'),
                        'tot_value' => Input::get('tot_value')
                        ); 
		
        $store_transfer_result = DB::select('select soxfrdtl.upc_number, soxfrdtl.item_number, soxfrdtl.item_desc,
             soxfrdtl.quantity, soxfrdtl.qty_units, soxfrdtl.upc_cost, soxfrdtl.case_pack, soxfrdtl.rw_rtl_amt, soxfrdtl.rtl_amt, soxfrdtl.discount_rate, upc_cost*quantity as ExtCost 
             from soxfrdtl where soxfrdtl.hdr_seq_number = "'.$seq_number.'"
         AND soxfrdtl.status = "I" ');
		 $StoreItemArray = json_decode(json_encode($store_transfer_result), true); 
		 return View::make('mktmgr.storetransfersitems', compact('StoreItemArray','item_upper_part_details'));
		//echo $seq_number;
		
	}
	public function StoreTransferUpdate()
	{
		$seq_number = Input::get('seq_number');
		$store_transfer_update_data = DB::select('SELECT * FROM soxfrhdr where seq_number = "'.$seq_number.'" ');
		$StoreUpdateArray = json_decode(json_encode($store_transfer_update_data), true); 
		return View::make('mktmgr.storetransfersupdate', compact('StoreUpdateArray'));
	}
	public function postStoreTransferUpdate()
	{	
		//echo 'Sravan';exit;
		$input_data = Input::all();
		$create_date = Input::get('create_date');
		$transfer_type = Input::get('transfer_type');
		$seq_number = Input::get('seq_number');
		//echo $transfer_type."<br>";
		//echo $create_date."<br>";
		$sodsdcwk_rec = DB::select('SELECT * FROM sodsdcwk where seq_number = 1 ');
		$cur_wk_bgn_date = json_decode(json_encode($sodsdcwk_rec), true); 
		//echo $cur_wk_bgn_date[0]['cur_wk_bgn_date']."<br>";
		$cur_wk_bgn_date = $cur_wk_bgn_date[0]['cur_wk_bgn_date'];
		if($create_date<$cur_wk_bgn_date)
		{	
			Session::flash('alert-danger', 'Can not update transfer created prior to Current Week!'); 
          	return Redirect::route('mktmgr-storetransfersquery');
		}
		elseif ($transfer_type=="RX") {
			//echo 'Can not update a transfer from another store';
			Session::flash('alert-danger', 'Can not update a transfer from another store!'); 
          	return Redirect::route('mktmgr-storetransfersquery');
		}
		else
		{
			echo 'Update Code Here';
		}
		//echo '<pre>';print_r($cur_wk_bgn_date);exit;
		//echo '<pre>';print_r($input_data);exit;
	}
}	