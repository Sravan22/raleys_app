<?php 

/**
* 
*/
/*namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;*/

class ReceivingsMenuController extends BaseController
{
	
	public function receivings()
	{
		return View::make('mktmgr.receivings');
	}

	public function receivingsQuery()
	{
        $dept_list = DB::table('sodsddpt')->get();
        $transporter_list = DB::table('sodsdtrn')->get();
        //return View::make('mktmgr.receivingsofflinereceiving', compact('dept_list', 'transporter_list'));
        return View::make('mktmgr.receivingsquery', compact('dept_list', 'transporter_list'));//->with('data', $dept_list);
		//return View::make('mktmgr.receivingsquery');
	}

    public function receivingsQueryAdvance()
    {
        // $vendor_list =DB::table('sodsdvnd')
        // ->orderBy('name','asc')
        // ->get(); 
        $vendor_list = DB::select('select * from sodsdvnd order by name asc');
         $dept_list = DB::table('sodsddpt')->get();
        $transporter_list = DB::table('sodsdtrn')->get();
        return View::make('mktmgr.receivingsqueryadvance', compact('dept_list', 'transporter_list','vendor_list'));
    }
    // public function totalVendors()
    // {
    //     //$vendor_list =DB::table('sodsdvnd')->get(); 
    //     $vendor_list = DB::select('select * from sodsdvnd');
    //     $vendor_list = json_decode(json_encode($vendor_list), true);
    //     //echo '<pre>';print_r($vendor_list);exit;
    //     return $vendor_list;
    // }
	public function receivingsBrowse()
	{
	   return View::make('mktmgr.receivingsbrowse');	
	}

	public function receivingsUpdate()
	{
		return View::make('mktmgr.receivingsupdate');
	}

	public function receivingsItems()
	{
		 $vendors = DB::table('sodsddtl')->select('store_qty', 'upc_number', 'delvry_unt_ovrd', 'store_cost', 'store_allow', 'vendor_cost', 'vendor_allow')->orderBy('upc_number', 'asc')->paginate(10);;
       
        return View::make('mktmgr.receivingsitems', compact('vendors'));
	}

    public function receivingsItemsMsg()
    {
        //Session::flash('alert-info', 'A Query must be performed prior to using this option');
        if(Session::get('itemsbyid') == '')
        { return View::make('mktmgr.receivingsmsg'); }
        else
        {
          $vendors = DB::table('sodsddtl')->select('store_qty', 'upc_number', 'delvry_unt_ovrd', 'store_cost', 'store_allow', 'vendor_cost', 'vendor_allow')
             ->where('hdr_seq_number',Session::get('itemsbyid'))
             ->orderBy('upc_number', 'asc')
             ->paginate(10);
        return View::make('mktmgr.receivingsitems', compact('vendors'));
        }
        /*$dept_list = DB::table('sodsddpt')->get();
        $transporter_list = DB::table('sodsdtrn')->get();
        return View::make('mktmgr.receivingsquery', compact('dept_list', 'transporter_list'));*/
    }

	public function receivingsOfflinereceiving()
	{
		//SELECT * FROM raleys.;
		$dept_list = DB::table('sodsddpt')->get();
        $transporter_list = DB::table('sodsdtrn')->get();
		return View::make('mktmgr.receivingsofflinereceiving', compact('dept_list', 'transporter_list'));//->with('data', );
	}
    public function crvSwitch()
    {
         $vendor_no = Input::get('vendorno');
        $result =DB::select('SELECT crv_switch as ws_crv_switch FROM sodsdvdv WHERE vendor_number = "'.$vendor_no.'"');
        $ws_crv_switch = $result[0]->ws_crv_switch;
        return $ws_crv_switch;
    }

	public function receivingsSeQuery()
	{
    $results = DB::select('SELECT sodsdvnd.name, sodsdvnd.vendor_number, sodsdvnd.type_code FROM sodsdvnd WHERE active_switch = "Y" AND type_code = "EXP" ORDER BY name');
    $vendor_details = json_decode(json_encode($results), true); 
	return View::make('mktmgr.sequery', compact('vendor_details'));
	}

	public function receivingsSeBrowse()
	{
		return View::make('mktmgr.sebrowse');
	}
    
    public function viewReceivingsSeQuery()
	{ 
       // echo Input::get('id');exit;
        if(Input::has('id')) {
            $rcv_data = Socadm00Sodsdhdr::where('seq_number',Input::get('id'))->leftJoin('sodsdvnd', 'sodsdhdr.vendor_number', '=', 'sodsdvnd.vendor_number')->select('sodsdhdr.*','sodsdvnd.name')->first();
            return View::make('mktmgr.viewsequery')->with('rcv_data',$rcv_data);
        }
        else {
            return Redirect::to('/mktmgr/receivings-sequery');
        }
        return View::make('mktmgr.viewsequery');

	}

	public function updateReceivingsSeQuery()
	{
        if(Input::has('id')) { //echo "Hit ID"; exit;
        // $id = Input::get('id');//exit;
        // echo 'SELECT sodsdvnd.name, sodsdvnd.vendor_number, sodsdvnd.type_code FROM sodsdvnd WHERE active_switch = "Y" AND type_code = "EXP"  ORDER BY name';exit;



  

        $results = DB::select('SELECT sodsdvnd.name, sodsdvnd.vendor_number, sodsdvnd.type_code FROM sodsdvnd WHERE active_switch = "Y" AND type_code = "EXP"  ORDER BY name');
        $vendor_details = json_decode(json_encode($results), true); 
            $rcv_data = Socadm00Sodsdhdr::where('seq_number',Input::get('id'))->leftJoin('sodsdvnd', 'sodsdhdr.vendor_number', '=', 'sodsdvnd.vendor_number')->select('sodsdhdr.*','sodsdvnd.name')->first();

              //$rcv_data = json_decode(json_encode($rcv_data), true);
            return View::make('mktmgr.updatesequery',compact('rcv_data','vendor_details'));
        }
        //echo "Not Hit ID"; exit;
		return View::make('mktmgr.updatesequery');
	}


	public function receivingsSeUpdate()
	{
		return View::make('mktmgr.receivingsseupdate');
	}

	public function shrinkCaptureReview()
	{
		return View::make('mktmgr.shrinkcapturereview');
	}

	public function postshrinkCaptureReviewDate()
	{
		echo $shrinkdate=Input::get('shrinkdate'); //exit;
		/*return Redirect::intended('/user/user');*/

		return Redirect::to('/mktmgr/shrinkcapturereviewdate');

		//return View::make('mktmgr/shrinkcapturereviewdate');
		//return Redirect::to('mktmgr/shrinkcapturereviewdate')->with('values', $shrinkdate);

		//echo $shrinkdate=Input::get('shrinkdate');
		//return Redirect::to('mktmgr.shrinkcapturereviewdate');
		/*return View::make('mktmgr.shrinkcapturereviewdate');*/
		/*return Redirect::to('/shrinkcapturereviewdate')->with('values', $shrinkdate);*/
	   /* return Redirect::to('/shrinkcapturereviewdate');*/
		/*return Redirect::intended('/shrinkcapturereviewdate');*/
		/*return Redirect::route('mktmgr-shrinkcapturereviewdate');*/
		/*return View::make('mktmgr.shrinkcapturereviewdate');*/
	}

	public function getReceivingsLogExpense()
	{
		$results = DB::select('SELECT sodsdvnd.name, sodsdvnd.vendor_number, sodsdvnd.type_code FROM sodsdvnd WHERE active_switch = "Y" AND type_code = "EXP" ORDER BY name');
        $vendor_details = json_decode(json_encode($results), true); 

        return View::make('mktmgr.receivingslogexpense',compact('vendor_details'));
	}

	public function postReceivingsLogExpense()
	{  
        $validator = Validator::make(Input::all(),
            array(
                   'vendor' =>'required',
                   'invnum' =>'required',
                   'invdate' =>'required',
                   'expensetype' =>'required',
                   'amount' =>'required'
                ),array(
                   'vendor.required' =>'Vendor Name is Required',
                   'invnum.required' =>'Invoice Number is Required',
                   'invdate.required' =>'Invoice Date is Required',
                   'expensetype.required' =>'Expense Type is Required',
                   'amount.required' =>'Amount is Required'
                ));
        if($validator->fails())
        {          
            return Redirect::route('mktmgr-receivings-receivingslogexpense')
                    ->withErrors($validator)
                    ->withInput();
        }
        // $validator = Validator::make(Input::all(),
        //     array(
        //            'vendornum'=>'required',
        //            'vendor'=>'required',
        //            'invnum'=>'required',
        //            'amount'=>'required',
        //         ));
        // if($validator->fails())
        // {
        //     return Redirect::route('mktmgr-receivings-receivingslogexpense')
        //             ->withErrors($validator)
        //             ->withInput();
        // }
        
        else
        {
    		$addLE = new Socadm00Sodsdhdr;
            if(Input::has('store')) {
                $addLE->store_number = Input::get('store');
            }
            if(Input::has('vendornum')) {
                $addLE->vendor_number = Input::get('vendornum');
            }
            if(Input::has('invdate')) {
                $convert_date = date("Y-m-d", strtotime(Input::get('invdate')));
                $addLE->invoice_date = $convert_date;
            }
            if(Input::has('invnum')) {
                $addLE->id = Input::get('invnum');
            }
            if(Input::has('expensetype')) {
                $addLE->type_code = Input::get('expensetype');
            }
            if(Input::has('amount')) {
                $addLE->tot_vend_cost = Input::get('amount');
            }
            if(Input::has('status')) {
                $addLE->status_code = 'O';
            }
            $addLE->method_rcvd = "E";
            $addLE->last_updated_by = 'MKTMGR';
            $addLE->last_update = date('Y-m-d G:i:s');
            $addLE->save();
            //,array('id'=>$addLE->id)
            Session::flash('alert-success', 'Record added <strong> successfully!</strong>'); 
            return Redirect::route('mktmgr-receivings-receivingslogexpense');
            //return Redirect::route('mktmgr-view-receivings-sequery',array('id'=>$addLE->id));
        }    
	}
    public function getVendorNumber()
    {
        return Input::get('vendor');
    }
    public function checkInvoiceNumber()
    {
       $invnum = Input::get('invnum');
       $vendornum = Input::get('vendornum');
       // echo 'SELECT COUNT(*) 
       //  as ws_id_cnt
       //  FROM sodsdhdr
       // WHERE vendor_number   = "'.$vendornum.'"
       //   AND id              = "'.$invnum.'"  
       //   AND (status_code    = "O"
       //    OR  status_code    = "R"
       //    OR  status_code    = "A")';exit;
       $results = DB::select('SELECT COUNT(*) 
        as ws_id_cnt
        FROM sodsdhdr
       WHERE vendor_number   = "'.$vendornum.'"
         AND id              = "'.$invnum.'"   
         AND (status_code    = "O"
          OR  status_code    = "R"
          OR  status_code    = "A")');
       $ws_id_cnt = json_decode(json_encode($results), true);
       if($ws_id_cnt[0]['ws_id_cnt'] > 0)
       {
        return 'Invoice Number already used by this vendor';
       }
       //return ;
    }
	public function getWeeklyReport()
	{
        $dept_list = DB::table('sodsddpt')->get();
		return View::make('mktmgr.weeklyreport', compact('dept_list'));
	}
	public function postWeeklyReport()
	{
        $validator = Validator::make(Input::all(),
            array(
                   'department'=>'required',
                   'fromdate'=>'required|date|before:todate',
                   'todate'=>'required|date|after:fromdate',
                ),array('fromdate.required'=>'Date is required', 'todate.required'=>'Date is required'));

        //print_r($validator);

        if($validator->fails())
        {
            $dept_list = DB::table('sodsddpt')->get();
            return Redirect::route('mktmgr-report-weeklyreport', compact('dept_list'))
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
            if(Input::has('storeno')) {
                $storeno = Input::get('storeno');
            }
            if(Input::has('department')) {
                $department = Input::get('department');
            }
            if(Input::has('fromdate')) {
                $fromdate = Input::get('fromdate');
            }
            if(Input::has('todate')) {
                $todate = Input::get('todate');
            }
            /*if(Input::has('copies')) {
                $copies = Input::get('copies');
            }*/
            $results = DB::select('SELECT sodsdhdr . seq_number , sodsdhdr . method_rcvd , sodsdhdr . type_code , sodsdhdr . dept_number , sodsdhdr . delivery_unit , sodsdhdr . id , sodsdhdr . vendor_number , sodsdhdr . tot_vend_cost , sodsdhdr . tot_vend_allow , sodsdhdr . tot_vend_crv , sodsdhdr . invoice_date , sodsdhdr . tot_sell_supply , sodsdhdr . tot_store_supply , sodsdvnd . name FROM sodsdhdr , sodsdvnd WHERE sodsdhdr . store_number = ? AND sodsdhdr . dept_number = ? AND ( sodsdhdr . status_code = "O" OR sodsdhdr . status_code = "A" ) AND sodsdhdr . create_datetime >= ? AND sodsdhdr . create_datetime <= ? AND sodsdvnd . vendor_number = sodsdhdr . vendor_number ORDER BY sodsdvnd . name , sodsdhdr . invoice_date , sodsdhdr . id', array($storeno, $department, $fromdate, $todate));
            return View::make('mktmgr.browsewrr', compact('results', 'storeno', 'department', 'fromdate', 'todate'));
        }
	}
    public function browseWeeklyReport()
    {
    	return View::make('mktmgr.browsewrr');
    }

    public function getStoreInvoice() {
    	return View::make('mktmgr.reportstoreinvoice');
    }
    public function postStoreInvoice() {
        if(Input::has('invoiceno')) {
            $invno = Input::get('invoiceno');
            //$storeinv = new Socadm00Sodsddtl;
            /*$storeinv = $storeinv->select('seq_number' , 'upc_number' , 'case_pack' , 'vendor_qty' , 'vendor_cost' , 'vendor_allow' , 'vendor_crv' , 'store_qty' , 'store_cost' , 'store_allow' , 'store_crv' , 'free_qty' , 'delvry_unt_ovrd' , 'spec_note_cd' , 'active_sw')->where('hdr_seq_number',$invno)->orderBy('seq_number', 'asc')->paginate(10);*/
            $storeinv = DB::select('select * from sodsddtl WHERE hdr_seq_number = "'.$invno.'"');
            $storeinv = json_decode(json_encode($storeinv), true);
            return View::make('mktmgr.browsesir', compact('storeinv', 'invno'));
        }
    	return View::make('mktmgr.reportstoreinvoice');
    }

    public function getCostDiscrepancyReport()
    {
    	return View::make('mktmgr.costdiscrepancyreport');
    }
    public function postCostDiscrepancyReport() {
         $validator = Validator::make(Input::all(),
            array(
                   'keydate'=>'required',
                ));

        //print_r($validator);

        if($validator->fails())
        {
            return Redirect::route('mktmgr-report-costdiscrepancyreport')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
            if(Input::has('strno')) {
                $strno = Input::get('strno');
            }
            else {
                $strno = '305';
            }
            if(Input::has('keydate')) {
                $keydate = Input::get('keydate');
                $cdreports = DB::select('SELECT seq_number , store_number , method_rcvd , type_code , dept_number , delivery_unit , id , vendor_number , cost_discrep_sw , status_code , invoice_date , create_datetime FROM sodsdhdr WHERE store_number = ? AND invoice_date = ? AND type_code = "D" AND ( status_code = "O" OR status_code = "A" ) ', array($strno, $keydate));
                return View::make('mktmgr.browsecdr', compact('cdreports', 'keydate', 'strno'));
            }
            return View::make('mktmgr.costdiscrepancyreport');
       }
    } 

    public function getOfflineReport()
    {
        $dept_list = DB::table('sodsddpt')->get();
    	return View::make('mktmgr.offlinereport', compact('dept_list'));
    }
    public function postOfflineReport()
    {
        $validator = Validator::make(Input::all(),
            array(
                   'department'=>'required',
                   'fromdate'=>'required|date|before:todate',
                   'todate'=>'required|date|after:fromdate',
                ),array('department.required'=>'Department is required','fromdate.required'=>'Date is required','todate.required'=>'Date is required'));

        //print_r($validator);

        if($validator->fails())
        {
            $dept_list = DB::table('sodsddpt')->get();
            return Redirect::route('mktmgr-report-offlinereport', compact('dept_list'))
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
	        if(Input::has('storeno')) {
	            $storeno = Input::get('storeno');
	        }
	        if(Input::has('department')) {
	            $dept = Input::get('department');
	        }
	        if(Input::has('fromdate')) {
	            $fromdate = Input::get('fromdate');
	        }
	        if(Input::has('todate')) {
	            $todate = Input::get('todate');
	        }
	        if(Input::has('copies')) {
	            $copies = Input::get('copies');
	        }
	        $results = DB::select('SELECT sodsdhdr . seq_number , sodsdhdr . method_rcvd , sodsdhdr . type_code , sodsdhdr . dept_number , sodsdhdr . id , sodsdhdr . vendor_number , sodsdhdr . tot_vend_cost , sodsdhdr . invoice_date , sodsdhdr . tot_sell_supply , sodsdhdr . tot_store_supply , sodsdvnd . name FROM sodsdhdr , sodsdvnd WHERE sodsdhdr . store_number = ? AND sodsdhdr . dept_number = ? AND sodsdhdr . method_rcvd = "O" AND sodsdhdr . status_code = "O" AND sodsdhdr . create_datetime >= ? AND sodsdhdr . create_datetime <= ? AND sodsdvnd . vendor_number = sodsdhdr . vendor_number ORDER BY sodsdhdr . dept_number , sodsdvnd . name , sodsdhdr . invoice_date , sodsdhdr . id', array($storeno, $dept, $fromdate, $todate));
	        return View::make('mktmgr.browsepor', compact('results', 'storeno', 'dept', 'fromdate', 'todate'));
	    }
    }
    public function browseOfflineReport()
    {
    	return View::make('mktmgr.browsepor');
    }


    public function getTransportlistingReport()
    {
    	$transports = new Socadm00Sodsdtrn;
        $transports = $transports->select('trns_number' , 'name' , 'address' , 'city' , 'state' , 'zip_code' , 'phone')->orderBy('name', 'asc')->paginate(10);
        return View::make('mktmgr.transportlisting', compact('transports'));
    }
	public function getDexInvoiceDate()
	{
		return View::make('mktmgr.invoicedate');
	}
	public function postDexInvoiceDate()
	{
         $validator = Validator::make(Input::all(),
            array(
                   'keydate'=>'required',
                ));

        //print_r($validator);

        if($validator->fails())
        {
            return Redirect::route('mktmgr-DEX-invoicedate')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
    		if(Input::has('keydate')) {
                $keydate = Input::get('keydate');
                $enddate = date('Y-m-d', strtotime("+1 day",strtotime($keydate)));
                
                $dexact = Socadm00Sodexlog::query();
                $dexact->whereBetween('create_datetime', array($keydate, $enddate));
                $dexact->orderBy('create_datetime', 'desc');
                $dexact = $dexact->paginate(10);//get();
                
                return View::make('mktmgr.dexlisting', compact('dexact', 'keydate'));
    		}
            return View::make('mktmgr.invoicedate');
        }
	}
    
    public function receivingsSeReports()
	{
		return View::make('mktmgr.sereports');
	}

    public function postReceivingsSeReports() {
          $validator = Validator::make(Input::all(),
            array(
                   'fromdate'=>'required|date|before:todate',
                   'todate'=>'required|date|after:fromdate',
                ));
        if($validator->fails())
        {
            
            return Redirect::route('mktmgr-receivings-sereports')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        { 
          $receivings = Socadm00Sodsdhdr::query();
            if(Input::has('fromdate')) {

                $convert_date = date("Y-m-d", strtotime(Input::get('fromdate')));
                $frmdt = $convert_date;
            }
            else {
                $frmdt = date('Y-m-d', time());
            }
            if(Input::has('todate')) {
                $convert_date = date("Y-m-d", strtotime(Input::get('todate')));
                $todt = $convert_date;
                //$todt = Input::get('todate');
            }
            else {
                $todt = date('Y-m-d', time());
            }
            $receivings->whereBetween('invoice_date', array($frmdt, $todt));
            $receivings->leftJoin('sodsdvnd', 'sodsdhdr.vendor_number', '=', 'sodsdvnd.vendor_number');
            $receivings = $receivings->paginate(10);//get();//$receivings = $receivings->get();
            //var_dump($receivings); exit;
            return View::make('mktmgr.sebrowse_reports', compact('receivings', 'frmdt', 'todt'));
        }    
    }
    
    public function searchReceivingsSeQuery() {
         $allpostval=Input::all();
        // echo '<pre>', print_r($allpostval); exit();









         if(Input::has('store')) {
            $storeID=Input::get('store');

                   
                $query  = 'SELECT t1.*, t4. name as vname FROM 
                           sodsdhdr t1, sodsdvnd t4 ';

                $where = ' where t1.vendor_number = t4.vendor_number AND t1.method_rcvd = "E" AND
                           t1. store_number ="'.$storeID.'" ';


            }
             if(Input::has('vendor')) {
                $vennum = Input::get('vendor');
                $where.= ' AND t1. vendor_number = "'. $vennum .'"';
            }
             if(Input::has('invdate')) {
            $convert_date = date("Y-m-d", strtotime(Input::get('invdate')));
            $frmdt = $convert_date; $todt = $convert_date;
           /* $receivings->where('invoice_date', $convert_date);*/
              $where.=' AND t1.invoice_date = "'.$convert_date .'"';
        }
         if(Input::has('invoice_number')) {
                    $where.= ' AND t1.id = "'.Input::get('invnum').'"';
            }
            if(Input::has('expensetype')) {
            
             $where.= ' AND t1.type_code = "'.Input::get('expensetype').'"';
        }
        if(Input::has('status')) {
         
             $where.= ' AND t1.status_code = "'.Input::get('status').'"';
        }
       /* $receivings = Socadm00Sodsdhdr::query();
        $frmdt = ''; $todt = '';
        
        if(Input::has('store')) {
            $receivings->where('store_number', Input::get('store'));
        }
        if(Input::has('vendornum')) {
            $receivings->where('sodsdhdr.vendor_number', Input::get('vendornum'));
           
        }
        if(Input::has('invdate')) {
            $convert_date = date("Y-m-d", strtotime(Input::get('invdate')));
			$frmdt = $convert_date; $todt = $convert_date;
            $receivings->where('invoice_date', $convert_date);
        }
        if(Input::has('invnum')) {
            $receivings->where('id', Input::get('invnum'));
        }
        if(Input::has('expensetype')) {
            $receivings->where('type_code', Input::get('expensetype'));
        }
        if(Input::has('amount')) {
            $receivings->where('tot_vend_cost', Input::get('amount'));
        }
        if(Input::has('status')) {
            $receivings->where('status_code', Input::get('status'));
        }
        if(Input::has('by')) {
            $receivings->where('last_updated_by', Input::get('by'));
        }
         $receivings->leftJoin('sodsdvnd', 'sodsdhdr.vendor_number', '=', 'sodsdvnd.vendor_number');
        $receivings = $receivings->get();
        //echo '<pre>',print_r($receivings);exit();
        //$receivings = $receivings->orderBy('name', 'asc')->paginate(10);*/

          $order= 'ORDER BY vname asc';
            $query_result = $query.$where.$order;
           //echo $query_result;exit();
            $receivings = DB::select($query_result);
        
        return View::make('mktmgr.sebrowse', compact('receivings'));
        //return View::make('mktmgr.sebrowse', compact('receivings'));
    }
    public function viewBrowseReceivingsSeQuery()
    {
        $rcv_data = Socadm00Sodsdhdr::where('seq_number',Input::get('id'))->leftJoin('sodsdvnd', 'sodsdhdr.vendor_number', '=', 'sodsdvnd.vendor_number')->select('sodsdhdr.*','sodsdvnd.name')->first();
        return View::make('mktmgr.store_expense_view', compact('rcv_data'));
    }
    public function postReceivingsSeQuery() {
        $inputdata = Input::all();
         //echo '<pre>';print_r($inputdata);exit();
         $invoice_date = Input::get('invdate');
         $invoice_number = Input::get('invoice_number');
         $vendor_number = Input::get('vendornum');
       $results = DB::select('SELECT cur_wk_bgn_date,cur_wk_end_date FROM sodsdlck WHERE seq_number = 1'); 
        $rec = json_decode(json_encode($results), true);
       $cur_wk_bgn_date = date('m-d-Y', strtotime($rec[0]['cur_wk_bgn_date']));
      //echo $cur_wk_bgn_date;//exit();
         $datetime = Input::get('create_datetime'); 
         
       $splitTimeStamp = explode(" ",$datetime);
       $create_datetime = $splitTimeStamp[0];
       $ws_60_days_ago = date( "m-d-Y", strtotime( " -2 month" ) );   
       $ws_180_days_ago = date( "Y-m-d", strtotime( " -6 month" ) );
       //$date = new DateTime($create_datetime);
       //echo $date->format('d-F-Y');
       //echo date( "Y-m-d", strtotime( $create_datetime ) ); 
       //echo $ws_60_days_ago;exit();

       $id_cnt = DB::select('SELECT COUNT(*) as ws_id_cnt FROM sodsdhdr WHERE 
        vendor_number = "'.$vendor_number.'"  AND id = "'.$invoice_number.'" 
        AND "'.$create_datetime.'" > "'.$ws_60_days_ago.'" AND (status_code = "O" OR  status_code = "R" OR  status_code  = "A")');
       //echo print_r($id_cnt);exit()  ;
          $ws_id_cnt = $id_cnt[0]->ws_id_cnt;


       
        if($create_datetime < $cur_wk_bgn_date)
        {
            Session::flash('alert-danger', 'Cannot update expense created prior to Current Week!'); 
        return Redirect::to('/mktmgr/receivings-sequery');
        }
        elseif( $invoice_date < $ws_180_days_ago)
        {
        Session::flash('alert-danger', 'Invoice Date can not be over 6 months back...!'); 
        return Redirect::to('/mktmgr/receivings-sequery');
        }
        elseif($ws_id_cnt > 0)
        {
            Session::flash('alert-danger', 'Invoice Number already used by this vendor...');
             
            return Redirect::to('/mktmgr/receivings-sequery');

        }
        else
        {
            //echo 'Date format issue';exit;
        // echo '<pre>';print_r($inputdata);exit();
        $vendor_number = Input::get('vendornum');
        $invoice_date = Input::get('invdate');
        $id = Input::get('invoice_number');
        $type_code = Input::get('expensetype');
        $tot_vend_cost = Input::get('amount');
        $status_code = Input::get('status');
        // echo 'update sodsdhdr set  invoice_date = "'.$invoice_date.'" and id = "'.$id.'" and type_code = "'.$type_code.'" and tot_vend_cost = "'.$tot_vend_cost.'" and status_code = "'.$status_code.'"  where vendor_number = "'.$vendor_number.'" and method_rcvd = "E" ';exit;
        // DB::update('update sodsdhdr set  invoice_date = "'.$invoice_date.'" and id = "'.$id.'" and type_code = "'.$type_code.'" and tot_vend_cost = "'.$tot_vend_cost.'" and status_code = "'.$status_code.'"  where vendor_number = "'.$vendor_number.'" and method_rcvd = "E" ');

         $updatedsodsdhdr = DB::table('sodsdhdr')
            ->where('vendor_number', $vendor_number)
            ->where('method_rcvd', "E")
            ->update(array('invoice_date' => $invoice_date,
                            'id' => $id,
                            'type_code' => $type_code,
                            'tot_vend_cost' => $tot_vend_cost,
                            'status_code' => $status_code
                            ));  
        // $updRcv = new Socadm00Sodsdhdr;
        // if(Input::has('store')) {
        //     $updRcv->store_number = Input::get('store');
        // }
        // if(Input::has('vendornum')) {
        //     $updRcv->vendor_number = Input::get('vendornum');
        // }
        // if(Input::has('invdate')) {
        //     $convert_date = date("Y-m-d", strtotime(Input::get('invdate')));
        //     $updRcv->invoice_date = $convert_date;
        // }
        // if(Input::has('invoice_number')) {
        //     $updRcv->id = Input::get('invoice_number');
        // }
        // if(Input::has('expensetype')) {
        //     $updRcv->type_code = Input::get('expensetype');
        // }
        // if(Input::has('amount')) {
        //     $updRcv->tot_vend_cost = Input::get('amount');
        // }
        // if(Input::has('status')) {
        //     $updRcv->status_code = Input::get('status');
        // }
        // $updRcv->last_updated_by = 'MKTMGR';
        // $updRcv->create_datetime = date('Y-m-d G:i:s');
        // //echo '<pre>';print_r($updRcv);exit();
        // $updRcv->save();
        Session::flash('alert-success', 'Record updated <strong>successfully!</strong>'); 
        return Redirect::to('/mktmgr/receivings-sequery');

        }
    }

    public function browseVendor() {
        $vendors = new Socadm00Sodsdvnd;
        $vendors = $vendors->orderBy('name', 'asc')->paginate(10);
        return View::make('mktmgr.browsevendor', compact('vendors'));
    }

	public function vendorListByDeptID()
	{
		$updRcv = new Socadm00Sodsdhdr;
		$deptnum=Input::get('deptnumber');
        $vendorlist = '';
        $results = DB::select('SELECT a.vendor_number,a.name FROM sodsdvnd a, sodsditm b
                                WHERE b.active_switch = "Y"
                                AND a.vendor_number = b.vendor_number and a.type_code != "STA"
                                AND b.dept_number   = "'.$deptnum.'" GROUP BY a.vendor_number order by a.name asc');
        //$result = $result->toArray();
        foreach ($results as $key) {
        	 $vendorlist .= $key->name.'_'.$key->vendor_number.'+';
        }
        return $vendorlist;
	}
    public function totalVendors()
    {
         $vendorlist = '';
        $results = DB::select('SELECT * from sodsdvnd ');
        //$result = $result->toArray();
        foreach ($results as $key) {
             $vendorlist .= $key->name.'_'.$key->vendor_number.'+';
        }
        return $vendorlist;
    }
    public function transporterListByVenderNo()
    {
        $vendorlist = '';
        $vendornumber = Input::get('vendornumber');
      //   echo 'SELECT sodsdtrn.name as vendorname, sodsdvdv.trns_number as vendornumber
      //   FROM sodsdtrn,sodsdvdv
      // WHERE sodsdvdv.vendor_number = "'.$vendornumber.'" AND
      //       sodsdtrn.trns_number = sodsdvdv.trns_number';exit;
         $results = DB::select('SELECT sodsdtrn.name as vendorname, sodsdvdv.trns_number as vendornumber
        FROM sodsdtrn,sodsdvdv WHERE sodsdvdv.vendor_number = "'.$vendornumber.'" AND 
          sodsdtrn.trns_number = sodsdvdv.trns_number');
          //$resultArray = json_decode(json_encode($results), true); 
         //echo '<pre>';print_r($resultArray);exit;
         if($results)
         {
            foreach ($results as $key) {
             $vendorlist .= $key->vendorname.'_'.$key->vendornumber;
        }
         }
         else
         {
            $vendorlist = 'empty';
         }
        return $vendorlist;
    }
    public function crvStatusByVenderNo()
    {
        $vendorlist = '';
        $vendornumber = Input::get('vendornumber');
        
      
         $results = DB::select('SELECT sodsdhdr.name as vendorname, sodsdvdv.trns_number as vendornumber
        FROM sodsdhdr t1,sodsdvdv t2 WHERE t1.vendor_number = "'.$vendornumber.'" AND t2.crv_switch = "Y" OR 
        t1.type_code="R" OR  method_rcvd = "D" OR method_rcvd = "N"   ');
          //$resultArray = json_decode(json_encode($results), true); 
         //echo '<pre>';print_r($resultArray);exit;
         if($results)
         {
            foreach ($results as $key) {
             $vendorlist .= $key->vendorname.'_'.$key->vendornumber.',';
        }
         }
         else
         {
            $vendorlist = 'empty';
         }
        return $vendorlist;
    }
    public function autocomplete()
    {
        $updRcv = new Socadm00Sodsdhdr;
        $vendorlist= '';
        $input = Input::get('term'); $results = array();
        $results = DB::select('SELECT * FROM sodsdvnd a, sodsditm b
                                WHERE b.active_switch = "Y"
                                AND a.vendor_number = b.vendor_number
                                AND b.dept_number   = "'.$input.'" GROUP BY a.vendor_number');
        foreach ($results as $query)
                        {
                           $results[] = [ 'id' => $query->vendor_number, 'value' => $query->name ];
                        }
                    return Response::json($results);
    }

	public function postOfflineReceiving()
	{

        //echo '<pre>',print_r(Input::all());exit();
        $validator = Validator::make(Input::all(),
            array(
                   //'storeid' =>'required',
                   //'dept_name_number'=>'required',
                   'invoice_date'=>'required',
                   'invoice_number'=>'required',
                   //'status'=>'required',
                   'invoice_type'=>'required',
                   //'method_received'=>'required',
                   
                   //'transporter'=>'required',
                   //'number_2'=>'required',
                   //'cost_discrepancies'=>'required',
                   'qty_vendor'=>'required',
                   //'crv'=>'required',
                   'store_inventory_vendor'=>'required',
                   //'selling_supplies_vendor'=>'required',
                   //'store_supplies_vendor'=>'required',
                   //'vendornumber'=>'required',
                   //'number_2'=>'required',
                ),array('invoice_number.required'=>'Invoice number required',
                  'store_inventory_vendor.required'=>'Required',
                  //'selling_supplies_vendor.required'=>'Required',
                  'qty_vendor.required'=>'Required',
                  //'store_supplies_vendor.required'=>'Required',
                  'invoice_type.required'=>'Invoice type required'
                  ));

        //print_r($validator);

        if($validator->fails())
        {
            return Redirect::route('mktmgr-receivings-receivingsofflinereceiving')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
            //echo 'hii2';exit();
            //echo "Invoice Date : ".Input::get('invoice_date'); exit;
             /*`seq_number`, `store_number`, `method_rcvd` , `type_code`,`dept_number`,`delivery_unit`,`id`,
            `vendor_number`, `cost_discrep_sw`,`status_code`,`tot_vend_qty`,`tot_vend_cost`,`tot_vend_allow`,
            `tot_vend_crv`,`invoice_date`,`credit_memo_id`,`create_datetime`,`last_updated_by`,`last_update`,
            `trns_number`,`tot_sell_supply`,`tot_store_supply`*/
		$allpostval=Input::all();
        $dept_number = Input::get('dept_name_number');
        $vendor_number = Input::get('vendornumber');
        
      //  echo '<pre>',print_r($allpostval);exit(); //vendorname
        $addOR = new Socadm00Sodsdhdr;
         $item_cnt=DB::select('SELECT count(*) as ws_item_cnt FROM sodsditm WHERE dept_number ="'.$dept_number.'"   AND vendor_number = "'.$vendor_number.'"
               AND active_switch = "Y"');

        $ws_item_cnt =  $item_cnt[0]->ws_item_cnt;


        if($ws_item_cnt == 0)
        {
                 Session::flash('alert-danger', 'Department does not match with vendor ');
               return Redirect::to('mktmgr/receivings-receivingsofflinereceiving');
        }
        else
        {
        if(Input::has('storeid')) {
            $addOR->store_number = Input::get('storeid');
        }
        if(Input::has('dept_name_number')) {
            $addOR->dept_number = Input::get('dept_name_number');
        }
         if(Input::has('vendornumber')) {
            $addOR->vendor_number = Input::get('vendornumber');
        }
         if(Input::has('number_2')) {
            $addOR->trns_number = Input::get('number_2');
        }
        if(Input::has('invoice_date')) {
           $convert_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
           $addOR->invoice_date  =  $convert_date;
        }
        if(Input::has('invoice_number')) {
            $addOR->id = Input::get('invoice_number');
        }
           if(Input::has('invoice_type')) {
            $addOR->type_code = Input::get('invoice_type');
        }   
      /*  if(Input::has('number_2')) {
            $addOR->trns_number = Input::get('number_2');
        }*/
        if(Input::has('qty_vendor')) {
            $addOR->tot_vend_qty = Input::get('qty_vendor');
        }
        if(Input::has('store_inventory_vendor')) {
            $addOR->tot_vend_cost = Input::get('store_inventory_vendor');
        }
       /* if(Input::has('store_inventory_vendor')) {
            $addOR->tot_store_supply = Input::get('store_inventory_vendor');
        }*/
         if(Input::has('crv')) {
            $addOR->tot_vend_crv = Input::get('crv');
        }
         if(Input::has('allowances')) {
            $addOR->tot_vend_allow = Input::get('allowances');
        }   
         if(Input::has('selling_supplies_vendor')) {
            $addOR->tot_sell_supply = Input::get('selling_supplies_vendor');
        }
        if(Input::has('store_supplies_vendor')) {
            $addOR->tot_store_supply = Input::get('store_supplies_vendor');
        }
        
         $addOR->cost_discrep_sw = "N";
         $addOR->method_rcvd = "O";    
         $addOR->status_code = "O";   
        
        /*if(Input::has('vendornumber')) {
            $addOR->vendor_number = Input::get('vendornumber');
        }*/
        
       
        /*if(Input::has('cost_discrepancies')) {
            $addOR->cost_discrep_sw = Input::get('cost_discrepancies');
        }*/
       /* if(Input::has('qty_vendor')) {
            $addOR->tot_vend_qty = Input::get('qty_vendor');
        }*/
       /* if(Input::has('qty_store')) {
            $addOR->tot_store_qty = Input::get('qty_store');
        }*/

        // selling_supplies_vendor == tot_sell_supply

        // store_supplies_vendor == tot_store_supply

        $addOR->delivery_unit = 'E';
        $addOR->credit_memo_id='';
        //$addOR->tot_sell_supply='0.0000';
        //$addOR->tot_store_supply='0.0000';
        $addOR->last_updated_by = 'DSD';
       /* $addOR->last_update = Input::get('created_date');
        $addOR->create_datetime = Input::get('updated_date');*/
        
        
        //mktmgr-Offline-Receiving
		/*$id = Socadm00Sodsdhdr::'sodsdhdr'->insert(
											    array('store_number' ,Input::get('storeid'),
											    	   'dept_number',Input::get('dept_name_number'),
  													  'method_rcvd' ,Input::get('method_received'),
  													  'type_code' ,Input::get('invoice_type'),
  													  'delivery_unit',Input::get('number_2'),
  													   'id','1',
  													   'vendor_number',Input::get('vendornumber'),
													  'cost_discrep_sw',Input::get('cost_discrepancies'),
													  'status_code',Input::get('status'),
													  'tot_vend_qty',Input::get('qty_vendor'),
													  'tot_vend_cost',Input::get('store_inventory_vendor'),
													  'tot_vend_allow',Input::get('store_inventory_vendor'),
													  'tot_vend_crv',Input::get('store_inventory_vendor'),
													  'invoice_date',Input::get('invoice_date'),
													  'credit_memo_id',Input::get('store_inventory_vendor'),
													  'create_datetime',Input::get('created_date'),
													  'trns_number',Input::get('number_2'),
													  'tot_sell_supply',Input::get('net_vendor'),
													  'tot_store_supply',Input::get('net_store'),
											    	  )
											);*/

        if($addOR->save())
        {    
          Session::flash('alert-success', 'Record added <strong>successfully !</strong>'); 
          return Redirect::route('mktmgr-receivings-receivingsofflinereceiving',array('id'=>$addOR->id));
        }
        }   
    }
	}

    public function postQueryReceiving()
    { 
       $allpostval=Input::all();

            $receivings = Socadm00Sodsdhdr::query();
           
            if(Input::has('storeid')) {
                  $storeID=Input::get('storeid');
               
                $query  = 'SELECT t1.*, t2. retail_dept, t2. name,  t1. trns_number, t3. name, t3. name as vname2 , t4. name as vname FROM 
                           sodsdhdr t1, sodsddpt t2, sodsdtrn t3, sodsdvnd t4 ';
                $where = ' where t1.trns_number = t3.trns_number AND t1.dept_number=t2.retail_dept AND t1.vendor_number = t4.vendor_number AND
                           t1. store_number ="'.$storeID.'" ';
            }
            if(Input::has('dept_name_number')) {
                $deptnum = Input::get('dept_name_number');
                $where.= ' AND t1. dept_number = "'. $deptnum .'"';
            }
            if(Input::has('invoice_date')) {
                $invoicedate = date("Y-m-d", strtotime(Input::get('invoice_date')));
                $invoice_end_date = date("Y-m-d", strtotime(Input::get('invoice_end_date')));
                if(Input::has('invoice_end_date'))
                {  //$invoicedate=Input::has('invoice_date'); $invoice_end_date = Input::has('invoice_end_date');
                    if( $invoicedate > $invoice_end_date)
                    {
                        Session::flash('alert-danger', 'Invoice start date should be less then invoice end date!');
                        $dept_list = DB::table('sodsddpt')->get();
                        $transporter_list = DB::table('sodsdtrn')->get();
                        return View::make('mktmgr.receivingsquery', compact('dept_list', 'transporter_list'));
                    }
                    $convert_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
                    $convert_date_end = date("Y-m-d", strtotime(Input::get('invoice_end_date')));
                    $where.=' AND t1.invoice_date BETWEEN "'.$convert_date .'" AND "'.$convert_date_end.'"';
                }
                else
                {
                    $convert_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
                    $where.=' AND t1.invoice_date = "'.$convert_date .'"';
                }
            }
            if(Input::has('invoice_number')) {
                    $where.= ' AND t1. id = "'.Input::get('invoice_number').'"';
            }

            if(Input::has('vendornumber')) {
                    $where.= ' AND t1. vendor_number = "'.Input::get('vendornumber').'"';
            }
           
            if(Input::has('invoice_type')) {
                    $where.= ' AND t1. type_code = "'.Input::get('invoice_type').'"';
            }
             if(Input::has('status')) {
                    $where.= ' AND t1. status_code = "'.Input::get('status').'"';
            }

            if(Input::has('number_2')) {
               
                 $receivings = DB::select('SELECT t1.* , t2. retail_dept, t2. name,  t1. trns_number, t3. trns_number, 
                                           t3. name as vname FROM sodsdhdr t1, sodsddpt t2, sodsdtrn t3 
                                           WHERE t1. trns_number = t3. trns_number AND t1. dept_number=t2. retail_dept AND
                                                 t1. store_number ='.$storeID .' AND 
                                                 t1. trns_number='.Input::get('number_2'));

            }
            $order= 'ORDER BY vname asc';
            $query_result = $query.$where.$order;
            
            $receivings = DB::select($query_result);
                       
            return View::make('mktmgr.queryresult', compact('receivings'));
    }

    public function postQueryReceivingAdvance()
    {
         $allpostval=Input::all();
           // echo '<pre>',print_r($allpostval);exit();
            $receivings = Socadm00Sodsdhdr::query(); //echo Input::has('storeid'); exit;

            $storeID=Input::get('storeid'); 
            
             $deptnum = Input::get('dept_name_number');
            //$invoice_end_date=Input::has('invoice_end_date');
            $invoice_date =Input::get('invoice_date'); 
            $invoice_number= Input::get('invoice_number');
             //$vendornumber=Input::get('vendornumber');
            $invoice_type=Input::get('invoice_type');
             $status=Input::get('status'); 
             $method_received=Input::get('method_received');

            if(Input::has('storeid')) {
                  $storeID=Input::get('storeid');

                   
                $query  = 'SELECT t1.*, t2.retail_dept, t2.name, t3. name as vname2 , t4. name as vname FROM 
                           sodsdhdr t1, sodsddpt t2, sodsdtrn t3, sodsdvnd t4 ';

                $where = ' where t1.trns_number = t3.trns_number AND t1.dept_number=t2.retail_dept AND t1.vendor_number = t4.vendor_number AND t1.method_rcvd != "E" AND
                           t1. store_number ="'.$storeID.'" ';


            }
            if(Input::has('dept_name_number')) {
               //$deptnum = Input::get('dept_name_number');
                $where.= ' AND t1. dept_number = "'. $deptnum .'"';
            }
            if(Input::has('vendorname')) {
                $vennum = Input::get('vendorname');
                $where.= ' AND t1. vendor_number = "'. $vennum .'"';
            }
             if(Input::has('invoice_date')) {
                $invoice_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
                $where.=' AND t1.invoice_date = "'.$invoice_date .'"';
                //echo $invoicedate;exit();
                /*$invoice_end_date = date("Y-m-d", strtotime(Input::get('invoice_end_date')));*/
               // if(Input::has('invoice_end_date'))
                //{  //$invoicedate=Input::has('invoice_date'); $invoice_end_date = Input::has('invoice_end_date');
                   /* if( $invoicedate > $invoice_end_date)
                    {
                        Session::flash('alert-danger', 'Invoice start date should be less then invoice end date!');
                        $dept_list = DB::table('sodsddpt')->get();
                        $transporter_list = DB::table('sodsdtrn')->get();
                        return View::make('mktmgr.receivingsqueryadvance', compact('dept_list', 'transporter_list'));
                    }*/
                 /*   $convert_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
                    $convert_date_end = date("Y-m-d", strtotime(Input::get('invoice_end_date')));
                    $where.=' AND t1.invoice_date BETWEEN "'.$convert_date .'" AND "'.$convert_date_end.'"';
                }
                else
                {
                    $convert_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
                    $where.=' AND t1.invoice_date = "'.$convert_date .'"';
                }*/
            }
            if(Input::has('invoice_number')) {
                    $where.= ' AND t1.id = "'.Input::get('invoice_number').'"';
            }

            /*if(Input::has('vendornumber')) {
                    $where.= ' AND t1. vendor_number = "'.Input::get('vendornumber').'"';
            }*/
           
            if(Input::has('invoice_type')) {
                    $where.= ' AND t1. type_code = "'.Input::get('invoice_type').'"';
            }
             if(Input::has('status')) {
                    $where.= ' AND t1. status_code = "'.Input::get('status').'"';
            }

            if(Input::has('method_received')) {
                    $where.= ' AND t1. method_rcvd = "'.Input::get('method_received').'"';
            }

            

            /*if(Input::has('number_2')) {
               
                 $receivings = DB::select('SELECT t1.* , t2. retail_dept, t2. name,  t1. trns_number, t3. trns_number, 
                                           t3. name as vname FROM sodsdhdr t1, sodsddpt t2, sodsdtrn t3 
                                           WHERE t1. trns_number = t3. trns_number AND t1. dept_number=t2. retail_dept AND
                                                 t1. store_number ='.$storeID .' AND 
                                                 t1. trns_number='.Input::get('number_2'));

            }*/
            $order= 'ORDER BY vname asc';
            $query_result = $query.$where.$order;
           // echo $query_result;exit();
            $receivings = DB::select($query_result);
            //echo '<pre>',print_r($receivings);exit();
            $invoice_date=Input::get('invoice_date'); 
            $invoice_end_date=Input::get('invoice_end_date');
            return View::make('mktmgr.queryresult', compact('receivings','storeID', 'deptnum', 'invoice_date', 'invoice_end_date', 
                                                            'invoice_number', 'vendornumber', 'invoice_type', 'status', 'method_received'));
    }

    public function postCostDiscrepancySearchReport()
    {
        return View::make('mktmgr.costdiscrepancysearchreport');
    }
    
    function postVendorDataByDate()
    {
        $selectedate= Input::has('invoice_date');
       // Session::flash('alert-success', 'Inserted Date'.$selectedate); 
        return Redirect::route('mktmgr-DEX-invoicedate');
    }

    function getAllQueryInformation()
    {
        //echo 'SELECT t1.* , t2. retail_dept, t2. name,  t1. trns_number, t3. name as tname,t4. name as vname FROM sodsdhdr t1, sodsddpt t2, sodsdtrn t3,sodsdvnd t4 WHERE t1. trns_number = t3. trns_number AND t1.vendor_number = t4.vendor_number AND t1. dept_number=t2. retail_dept AND t1. seq_number='.Input::get('seq_number');exit();
        $dept_list = DB::table('sodsddpt')->get();
        $transporter_list = DB::table('sodsdtrn')->get();
        $check_date= DB::select('SELECT cur_wk_bgn_date, cur_wk_end_date FROM sodsdlck WHERE seq_number = 1');   
        $receivings = DB::select('SELECT t1.* , t2. retail_dept, t2. name,  t1. trns_number, t3. name as vname2,t4. name as vname  
                                        FROM sodsdhdr t1, sodsddpt t2, sodsdtrn t3,sodsdvnd t4 
                        WHERE t1. trns_number = t3. trns_number AND t1.vendor_number = t4.vendor_number

                                        AND t1. dept_number=t2. retail_dept AND
                                             t1. seq_number='.Input::get('seq_number'));
        $receiving = json_decode(json_encode($receivings), true);
       

       /*Crv input enable or disable */ 
        $result =DB::select('SELECT crv_switch as ws_crv_switch FROM sodsdvdv WHERE vendor_number = "'.$receiving[0]['vendor_number'].'"');
        $ws_crv_switch = $result[0]->ws_crv_switch;
       
       
        return View::make('mktmgr.selectedquery1', compact('receivings','check_date','dept_list','transporter_list','ws_crv_switch'));
    }

    public function receivingsQueryUpdate()
    {

        //echo 'hii'; exit()  ;
        //SELECT * FROM raleys.sodsddtl where hdr_seq_number = 94173
        /* $validator = Validator::make(Input::all(),
            array(
                   'storeid' =>'required',
                   'dept_name_number'=>'required',
                   'invoice_date'=>'required',
                   'invoice_number'=>'required',
                   'status'=>'required',
                   'invoice_type'=>'required',
                   'method_received'=>'required',
                   'vendornumber'=>'required',
                   'transporter'=>'required',
                   'number_2'=>'required',
                   'cost_discrepancies'=>'required',
                   'qty_vendor'=>'required',
                   'store_inventory_vendor'=>'required',
                   'selling_supplies_vendor'=>'required',
                   'store_supplies_vendor'=>'required',
                   'vendornumber'=>'required',
                   'number_2'=>'required',
                ));*/

       /* if($validator->fails())
        {
            $receivings = DB::table('sodsdhdr')
            ->join('sodsddpt', 'sodsdhdr.trns_number', '=', 'sodsdtrn.trns_number')
            ->join('sodsdtrn', 'sodsdhdr.dept_number', '=', 'sodsddpt.retail_dept')
            ->select('sodsdhdr.*', 'sodsddpt.retail_dept', 'sodsddpt.name',  'sodsdhdr.trns_number', 'sodsdtrn.name vname')
            ->get();
            return Redirect::route('mktmgr-post-updatequeryresult', compact('receivings'))
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {*/
            $allpostval=Input::all();
           // echo '<pre>',print_r($allpostval);
        $dept_list = DB::table('sodsddpt')->get();
        $receivings = DB::select('SELECT t1.* , t2. retail_dept, t2. name,  t1. trns_number, t3. name vname, 
                                       t3. name as vname FROM sodsdhdr t1, sodsddpt t2, sodsdtrn t3 
                                       WHERE t1. trns_number = t3. trns_number AND t1. dept_number=t2. retail_dept AND
               
                                             t1. seq_number='.Input::get('seq_number'));

            $rec = json_decode(json_encode($receivings), true);
            //echo print_r($rec[0]['id'],true);
        $results = DB::select('SELECT cur_wk_bgn_date,cur_wk_end_date FROM sodsdlck WHERE seq_number = 1'); 
        
        $sodsdlck_rec = json_decode(json_encode($results), true);
        
        $create_datetime = Input::get('created_date_to_update');
        
        $cur_wk_bgn_date = $sodsdlck_rec[0]['cur_wk_bgn_date'];
        
        $dept_number    = Input::get('dept_name_number');    
        
        $vendor_number= Input::get('vendornumber');
        
        $invoice_number= Input::get('invoice_number');
        
        //$create_datetime = date("Y-m-d");
       
        $status = Input::get('status');
        
        $ws_60_days_ago = date( "Y-m-d", strtotime( "-2 month" ) );
        
  
        $item_cnt = DB::select('SELECT count(*) as ws_item_cnt FROM sodsditm 
                    WHERE dept_number ="'.$dept_number.'"  
                    AND vendor_number = "'.$vendor_number.'"
                    AND active_switch = "Y"');

        $ws_item_cnt =  $item_cnt[0]->ws_item_cnt;
/*
            echo 'SELECT COUNT(*) as ws_id_cnt FROM sodsdhdr WHERE 
                                     vendor_number = "'.$vendor_number.'" 
                                     AND id = "'.$invoice_number.'" 
                                     AND "'.$create_datetime.'" > "'.$ws_60_days_ago.'" 
                  AND (status_code = "O" OR  status_code = "R" OR  status_code  = "A")';exit;*/
        
        $ws_id_cnt = 0;
        
         if($rec[0]['id'] == $invoice_number)
         {
                $ws_id_cnt = 0;
         }
         else
         {
            $id_cnt = DB::select('SELECT COUNT(*) as ws_id_cnt FROM sodsdhdr WHERE 
                                     vendor_number = "'.$vendor_number.'" 
                                     AND id = "'.$invoice_number.'" 
                                     AND "'.$create_datetime.'" > "'.$ws_60_days_ago.'" 
                  AND (status_code = "O" OR  status_code = "R" OR  status_code  = "A")');
     
          $ws_id_cnt = $id_cnt[0]->ws_id_cnt;
         }
        
       
       
        if($ws_item_cnt == 0)
        {
                 Session::flash('alert-danger', 'Department does not match with vendor ');
                 return Redirect::to('/mktmgr/receivingsqueryadvance-query');
        }
        
        elseif($ws_id_cnt != 0)
        {
            Session::flash('alert-danger', 'Invoice Number already used by this vendor...');
             
            return Redirect::to('/mktmgr/receivingsqueryadvance-query');

        }
        
     
        elseif($create_datetime < $cur_wk_bgn_date)
        {
            Session::flash('alert-danger', 'Cannot update invoice created prior to Current Week!'); 
            
            return Redirect::to('/mktmgr/receivingsqueryadvance-query');
        }
        else
        {
            
            $allpostval=Input::all(); //vendorname
            $addOR = new Socadm00Sodsdhdr;
            $convert_date = date("Y-m-d", strtotime(Input::get('invoice_date')));
            $updated_date = date("Y-m-d", strtotime(Input::get('updated_date')));
            
            $queryupdate=DB::update('UPDATE sodsdhdr SET 
                        
                        type_code="'.Input::get('invoice_type').'",
                        
                        dept_number ="'.Input::get('dept_name_number').'",
                        
                        id="'.Input::get('invoice_number').'",
                        
                        vendor_number= "'.Input::get('vendornumber').'",
                        
                        status_code="'.Input::get('status').'",
                        
                        tot_vend_qty ="'.Input::get('tot_vend_qty').'",

                        tot_vend_cost ="'.Input::get('tot_vend_cost').'",

                        tot_vend_crv ="'.Input::get('tot_vend_crv').'",

                        invoice_date="'.$convert_date.'",
                        
                        last_update="'.$updated_date.'",

                        trns_number = "'.Input::get('number_2').'",
                      
                        tot_sell_supply ="'.Input::get('tot_sell_supply').'",

                        tot_store_supply ="'.Input::get('tot_store_supply').'"
                        
                        WHERE seq_number ='.Input::get('seq_number'));

           //------- selling_supplies_vendor
            if($queryupdate)
            {
                Session::flash('alert-success', 'Record updated <strong>successfully!</strong>'); 
                
                 return Redirect::to('/mktmgr/receivingsqueryadvance-query');
               
            }else{

                return Redirect::to('/mktmgr/receivingsqueryadvance-query');
            }
        }
    }

    public function getItemsById()
    {
        Session::set('itemsbyid',Input::get('seq_number')); 
         $vendors = DB::table('sodsddtl')->select('store_qty', 'upc_number', 'delvry_unt_ovrd', 'store_cost', 'store_allow', 'vendor_cost', 'vendor_allow')
             ->where('hdr_seq_number',Input::get('seq_number'))
             ->orderBy('upc_number', 'asc')
             ->paginate(10);
        return View::make('mktmgr.receivingsitems', compact('vendors'));
    }
}