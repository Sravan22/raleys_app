<?php

class StoreOrderViewController extends  BaseController
{
	
	public function storeOrderview()
	{
		return View::make('mktmgr.storeorderview');
	}

	public function getAdItemOrdering()
	{
		$results = DB::select('SELECT sogoatyp.gl_dept_number, sogoatyp.name FROM sogoatyp WHERE sogoatyp.menu_code= "MM" AND sogoatyp.active_switch = "Y" ORDER BY sogoatyp.gl_dept_number');
		$departments = json_decode(json_encode($results), true); 
	return View::make('mktmgr.aditemordering',compact('departments'));
	}
	public function getAdItemPrint()
	{
		$results = DB::select('SELECT sogoatyp.gl_dept_number, sogoatyp.name FROM sogoatyp WHERE sogoatyp.menu_code= "MM" AND sogoatyp.active_switch = "Y" ORDER BY sogoatyp.gl_dept_number');
		$departments = json_decode(json_encode($results), true); 
		return View::make('mktmgr.aditemprint',compact('departments'));
	}


	public function getAdItemRelease()
	{
		return View::make('mktmgr.aditemrelease');
	}
	public function selectOrderType()
	{
		$deptnumber=Input::get('deptnumber');
		$ordertype = '';
		$results = DB::select('SELECT * from sogoatyp WHERE gl_dept_number = "'.$deptnumber.'" AND menu_position  > 0 AND active_switch  = "Y"
            ORDER BY menu_position');



		foreach ($results as $key) {
        	if(!($key->subtype_code == "DS")){
        		//$ordertype .= $key->name.'_'.$key->type_code.'_'.$key->subtype_code.',';
        		$ordertype .= $key->type_code.'-'.$key->subtype_code.'_'.$key->name.',';
        	}
        	else
        	{
        		$ordertype .='';
        	}
        }
        return $ordertype;
	}
	public function postStoreOrderWeekly()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'dept_name_number' =>'required',
                   'type_subtype_code' =>'required'
                ),array(
                   'dept_name_number.required' =>'Department is Required',
                   'type_subtype_code.required' =>'Order Type is Required'
                ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-aditemordering')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$sort_by_sw  = "T";
        	$done_sw = "N";
        	$sort_by_desc = "PRODUCT TYPE";
        	$inputdata = Input::all();
        	$store_number=Input::get('store_number');
        	$dept_name_number=Input::get('dept_name_number');
        	$type_subtype_code=Input::get('type_subtype_code');
			$type_subtype_code = explode("-",$type_subtype_code);
			$type_code = $type_subtype_code[0];
			$subtype_code = $type_subtype_code[1];
			//echo '<pre>';print_r($inputdata);exit;
			$subtype_code_name = DB::select('select sogoatyp.name, sogoatyp.type_code, sogoatyp.subtype_code, sogoatyp.menu_position from sogoatyp where sogoatyp.gl_dept_number = "'.$dept_name_number.'" 
				and sogoatyp.type_code = "'.$type_code.'"
				and sogoatyp.subtype_code = "'.$subtype_code.'"
				and sogoatyp.menu_position  > 0
			    and sogoatyp.active_switch  = "Y"
			    Order by sogoatyp.menu_position');
			$subtype_code_array = json_decode(json_encode($subtype_code_name), true);
			// $today = date('Y-m-d');
			// $afterweek = date('Y-m-d', strtotime(' +7 day'));
			$dept_name = DB::select('SELECT sogoatyp.gl_dept_number, sogoatyp.name FROM sogoatyp WHERE sogoatyp.menu_code= "MM" AND sogoatyp.active_switch = "Y" and sogoatyp.gl_dept_number="'.$dept_name_number.'" ORDER BY sogoatyp.gl_dept_number');
			$dept_name_array = json_decode(json_encode($dept_name), true);
			$today = "2015-09-14";
			$afterweek = "2015-09-19";
			$by_desc_curs = DB::select('SELECT  sogoonad.ad_bgn_date,
					                sogoonad.store_number,
					                sogoonad.dcl_dept_number,
					                sogoonad.sku_number,
					                sogoonad.vendor_item_number,
					                sogoonad.reg_retail,
					                sogoonad.ad_for_qty,
					                sogoonad.ad_retail,
					                sogoonad.prev_ad_retail,
					                sogoonad.case_pack,
					                sogoonad.description,
					                sogoonad.upc_number,
					                sogoonad.prev_ad_sold,
					                sogoonad.order_qty,
					                sogoonad.cont_size,
					                sogoonad.prev_ad_date
					           FROM sogoonad
					          WHERE sogoonad.store_number  = "'.$store_number.'"
					            AND sogoonad.type_code     = "'.$type_code.'"  
					            AND sogoonad.subtype_code  = "'.$subtype_code.'"  
					            AND sogoonad.ad_bgn_date   > "'.$today.'"
					            AND sogoonad.ad_bgn_date  <= "'.$afterweek.'"
					       ORDER BY sogoonad.description');
			$by_desc_curs_array = json_decode(json_encode($by_desc_curs), true); 
			$by_dcl_curs = DB::select('SELECT  sogoonad.ad_bgn_date,
					                sogoonad.store_number,
					                sogoonad.dcl_dept_number,
					                sogoonad.dcl_class_number,
					                sogoonad.sku_number,
					                sogoonad.vendor_item_number,
					                sogoonad.reg_retail,
					                sogoonad.ad_for_qty,
					                sogoonad.ad_retail,
					                sogoonad.prev_ad_retail,
					                sogoonad.case_pack,
					                sogoonad.description,
					                sogoonad.upc_number,
					                sogoonad.prev_ad_sold,
					                sogoonad.order_qty,
					                sogoonad.cont_size,
					                sogoonad.prev_ad_date
					           FROM sogoonad
					          WHERE sogoonad.store_number  = "'.$store_number.'"
					            AND sogoonad.type_code     = "'.$type_code.'"  
					            AND sogoonad.subtype_code  = "'.$subtype_code.'"  
					            AND sogoonad.ad_bgn_date   > "'.$today.'"
					            AND sogoonad.ad_bgn_date  <= "'.$afterweek.'"
					       ORDER BY sogoonad.dcl_dept_number,sogoonad.dcl_class_number,
                sogoonad.description');
			$by_dcl_curs_array = json_decode(json_encode($by_dcl_curs), true); 
			return View::make('mktmgr.poststoreorderweekly',compact('by_desc_curs_array','by_dcl_curs_array','sort_by_sw','dept_name_array','subtype_code_array'));
        }
		
	}
	public function postdsditemad()
	{								
		$departments = Input::get('dept_name_number');


		$sql = DB::select('SELECT name FROM sogoatyp WHERE   gl_dept_number = "'.$departments.'" AND menu_code= "MM" AND active_switch = "Y"');
		$results = json_decode(json_encode($sql), true);
		//echo "<pre>"; print_r($results);exit();
		
		$validator = Validator::make(Input::all(),
            array(
                   'dept_name_number' =>'required',
                  
                ),array(
                   'dept_name_number.required' =>'Department is Required'
                ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-aditemdsd')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
			return View::make('mktmgr.postdsditemadd',compact('results'));
        }
		
	}
	public function getAdItemDsd()
	{
		$results = DB::select('SELECT sogoatyp.gl_dept_number, sogoatyp.name FROM sogoatyp WHERE sogoatyp.menu_code= "MM" AND sogoatyp.active_switch = "Y" ORDER BY sogoatyp.gl_dept_number');
		$departments = json_decode(json_encode($results), true); 
		return View::make('mktmgr.aditemdsd',compact('departments'));
	}

	public function getCodedItemQuery()
	{
		return View::make('mktmgr.codeditemquery');
	}

	public function getCodedItemBrowse()
	{
		return View::make('mktmgr.codeditembrowser');
	}

	public function getEditWarningLimits()
	{
		
			$results = DB::select('SELECT type_code,name,gl_dept_number FROM sodept WHERE active_switch = "Y" ORDER BY gl_dept_number');
			$departments = json_decode(json_encode($results), true); 
			return View::make('mktmgr.editwarninglimits',compact('departments'));
		
	}
	public function postEditWarningLimits()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'dept_name_number' =>'required'
                 ),array(
                   'dept_name_number.required' =>'Department is Required'
                  ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-editwarninglimits')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {	
        	$department = Input::get('dept_name_number');
        	//echo $department."<br>";
        	//echo 'SELECT type_code,name,gl_dept_number FROM sodept WHERE type_code = "'.$department.'" and active_switch = "Y" ORDER BY gl_dept_number';
        	//echo '<br>';
        	//echo 'select entry_number, description, max_qty from sotrncfg where type_code = "'.$department.'" and subtype_code = "00" ';exit;
        	$department_name = DB::select('SELECT type_code,name,gl_dept_number FROM sodept WHERE type_code = "'.$department.'" and active_switch = "Y" ORDER BY gl_dept_number' );
			$department_name_array = json_decode(json_encode($department_name), true); 
        	$current_limit = DB::select('select entry_number, description, max_qty from sotrncfg where type_code = "'.$department.'" and subtype_code = "00" ');
        	$current_limit_array = json_decode(json_encode($current_limit), true);
			return View::make('mktmgr.posteditwarninglimits',compact('department_name_array','current_limit_array'));
		}
	}
	public function postEditWarningLimitsData()
	{
		$inputdata = Input::all();
		$new_limit = Input::get('new_limit');
		$entry_number = Input::get('entry_number');
        $update_limit = DB::table('sotrncfg')
		            ->where('entry_number', $entry_number)
		            ->update(array('max_qty' => $new_limit));
        if($update_limit)
        {
        	Session::flash('alert-success', 'Limit Updated!'); 
          	return Redirect::route('mktmgr-editwarninglimits');
        }
        else
        {
        	Session::flash('alert-danger', 'Limit Not Updated!'); 
          	return Redirect::route('mktmgr-editwarninglimits');
        }
       
	}
	public function postPrintOrderdetails()
	{
		//$inputdata = Input::all();
		$validator = Validator::make(Input::all(),
            array(
                   'dept_number' =>'required'
                 ),array(
                   'dept_number.required' =>'Department is Required'
                  ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-printorderquery')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {	
		//echo '<pre>',print_r($inputdata);
		$dept_number = Input::get('dept_number');
		$order_date = Input::get('order_date');
		$select = "select * from soordhdr ";
        $where = 'where 1 = 1 ';
	            	if(Input::has('dept_number'))
	            	{
	            		$where.="and  dept_number = '$dept_number' ";
                    }
                	if(Input::has('order_date'))
            		{
            			$where.=" and order_date = '$order_date' ";
                	}

                $order="ORDER BY order_date asc";	
              	$query=$select.$where.$order;
              //	echo $query;exit;
            	$print_order_result = DB::select($query);
            	$print_order_result_array = json_decode(json_encode($print_order_result), true);
            	return View::make('mktmgr.printorderbrowser',compact('print_order_result_array', 'dept_number','order_date'));
            	//echo '<pre>';print_r($print_order_result);exit;
	}
}
	public function getStoreHolidayOrders()
	{
		return View::make('mktmgr.storeholidayorders');
	}

	public function getOutOfStockQuery()
	{
		return View::make('mktmgr.outofstockquery');
	}
	public function postOutOfStock()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'datascannedtodate' =>'required',
                   'datascannedfromdate' =>'required'
                ),array(
                   'datascannedfromdate.required' =>' Scanned from date is required',
                   'datascannedtodate.required' =>' Scanned to date is required'
                ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-outofstockquery')
                    ->withErrors($validator)
                    ->withInput();
        }

        else
        {
        	$datascannedfromdate = Input::get('datascannedfromdate');
			$datascannedtodate = Input::get('datascannedtodate');
			$skunumber = Input::get('skunumber');
			$gl_department_number = Input::get('gl_department_number');
			$upc_number = Input::get('upc_number');
			if(!($datascannedfromdate<=$datascannedtodate))
			{
			 	Session::flash('alert-danger', 'Invalid Date Range. Start Date should be less than End date!');
	          	return Redirect::route('mktmgr-outofstockquery');
			}
		else
		{
			$select = "select * from soooslst ";
        	$where = 'where 1 = 1 ';
	            	if(Input::has('datascannedfromdate') && Input::has('datascannedtodate'))
	            	{
	            		$where.="and  create_date >= '$datascannedfromdate' and  create_date <= '$datascannedtodate' ";
                    }
                	if(Input::has('skunumber'))
            		{
            			$where.=" and sku_number = '$skunumber' ";
                	}
                	if(Input::has('gl_department_number'))
            		{
            			$where.=" and gl_dept_number = '$gl_department_number' ";
                	}
                	if(Input::has('upc_number'))
            		{
            			$where.=" and upc_number = '$upc_number' ";
                	}
                	$order="ORDER BY create_date desc";
                $query=$select.$where.$order;
                $out_of_stock_result = DB::select($query);
                return View::make('mktmgr.outofstockbrowse',compact('out_of_stock_result'));
		}
		}
	}
	public function getOutOfStockBrowse()
	{
		return View::make('mktmgr.outofstockbrowse');
	}

	public function getPlusOutBrowse()
	{
		$sotrnhdr_rec = DB::select('select sotrnhdr.seq_number,soordhdr.order_date,sotrncfg.type_code,sotrnhdr.type_code,sotrncfg.subtype_code,sotrnhdr.subtype_code from sotrnhdr,soordhdr,sotrncfg where sotrnhdr.seq_number = soordhdr.seq_number and sotrnhdr.type_code = sotrncfg.type_code and sotrnhdr.subtype_code = sotrncfg.subtype_code order by order_date desc');
		return View::make('mktmgr.plusoutbrowse',compact('sotrnhdr_rec'));
	}
	public function getPlusOutItems()
	{
		$seq_number = Input::get('seq_number');
		//echo $seq_number;exit;
		// echo 'SELECT soorditm.sku_number, soorditm.quantity FROM soorditm WHERE soorditm.seq_number = "'.$seq_number.'" ';exit;
		$soorditm_rec = DB::select('SELECT soorditm.sku_number, soorditm.quantity FROM soorditm WHERE soorditm.seq_number = "'.$seq_number.'" ');
		return View::make('mktmgr.plusoutsitems',compact('soorditm_rec','seq_number'));

		//return View::make('mktmgr.plusoutsitems');
	}
	public function getPlusOutItem()
	{
		return View::make('mktmgr.plusoutitem');
	}

	public function getPrintOrderQuery()
	{
		return View::make('mktmgr.printorderquery');
	}

	public function getPrintOrderBrowse()
	{
		return View::make('mktmgr.printorderbrowser');
	}
	public function printorderbrowsermsg()
	{
		return View::make('mktmgr.printordermsg');
	}
	public function getOutOfStockBrowsemsg()
	{
		return View::make('mktmgr.outofstockbrowsemsg');
	}
	public function getPrintOrderdetails()
	{
		$seq_number = Input::get('seq_number');
		$order_date = Input::get('order_date');
		$description = Input::get('description');
		$status_code = Input::get('status_code');
		
		//echo $order_date.'<br>';
		//echo $status_code.'<br>';exit;
		// select soorditm.sku_number, soorditm.item_number, soitem.description, soitem.case_pack, soitem.cont_size where soorditm.seq_number = "'.$seq_number.'" and soitem.sku_number
		$seq_number_result = DB::select('SELECT * FROM soorditm  WHERE seq_number = "'.$seq_number.'"');
       	$seq_number_result_array = json_decode(json_encode($seq_number_result), true);
       	
       
       //	$seq_number_result_array = json_decode(json_encode($seq_number_result), true);
		return View::make('mktmgr.printorderdetails',compact('seq_number_result_array','order_date','description','status_code','seq_number'));
	}


}	