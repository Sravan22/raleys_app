<?php

/**
 * 
 */
class UserHomeController extends BaseController {

    public $date_stamp = '';
    public $tot_acct = 0;
    public $begin_safe = 0;
    public $del_tranfs = 0;
    public $deposits = 0;
    public $store_tx = 0;
    public $po_types = 0;
    public $safe_os = 0;
    public $safe_cnt = 0;
    public $total = 0;
    public $groc_os = 0;
    public $drug_os = 0;
    
    public $wk_recap_date = '';
    public $wk_summary = 0;
    public $wk_dept_sales = 0;
    public $wk_net_safe =0;
    public $wk_deposits =0;
    public $wk_del_tranfs =0;
    public $wk_store_tx =0;
    public $wk_po_types =0;
    public $wk_drgov = 0;
    public $wk_grov = 0;
    
    public $wk_maindep_total = 0;
    public $wk_maindep_currency = 0;
    public $wk_maindep_checks = 0;
    public $wk_maindep_express_cred = 0;
    public $wk_maindep_express_deb = 0;
    
    public $wk_maintrf_total = 0;
    public $wk_maintrf_tranfs = 0;
    public $wk_maintrf_del = 0;
    
    public $errMsg = '';

    public function userHome() {
        $user = Auth::user();
        
        if($user){
        $username = Auth::user()->username; //exit;
        if ($username === 'mktmgr') {
            return View::make('homepage');
        } else if ($username === 'sign1') {
        	$stock_array = array();
        	$stock_cnt = 0;
        	$ix = 1;
        	$ssstock = DB::select('SELECT * FROM ssstock WHERE ssstock.active_sw = "Y" AND stock_code!="T" ORDER BY ssstock.stock_code');
		$ssstock_array = json_decode(json_encode($ssstock), true);
		$stock_cnt = count($ssstock_array);
		//echo '<pre>';print_r($ssstock);exit; 
		foreach($ssstock as $row)
		{
			$stock_array[$ix]['stock_code'] = $row->stock_code;
			$stock_array[$ix]['stock_desc'] = $row->stock_desc;
			$stock_cnt = $stock_cnt + 1;
			$ix = $ix + 1;
		}//exit;

		// $stock_array[$ix]['stock_code']=" ";
	 //    $stock_array[$ix]['stock_desc']=" ";
	 //    $stock_cnt = $stock_cnt + 1;
	 //    $ix = $ix + 1;

		$stock_array[$ix]['stock_code']="S";
	    $stock_array[$ix]['stock_desc']="Remotely Requested Signs";
	    $stock_cnt = $stock_cnt + 1;
	    $ix = $ix + 1;

	    $stock_array[$ix]['stock_code']="T";
	    $stock_array[$ix]['stock_desc']="Print Queued Tags & Signs";
	    $stock_cnt = $stock_cnt + 1;
	    $ix = $ix + 1;


	    $stock_array[$ix]['stock_code']="Z";
	    $stock_array[$ix]['stock_desc']="Audit/FM Notes";
	    $stock_cnt = $stock_cnt + 1;
	    $ix = $ix + 1;
		
		//echo '<pre>';print_r($stock_array);//exit; 

            return View::make('sign1.homepage',compact('stock_array'));
        } else if ($username === 'socscn01') {
            return View::make('socscn01.homepage');
        } else if ($username === 'slcscn01') {
            return View::make('slcscn01.homepage');
        } else if ($username === 'slcadm03') {
            //return View::make('slcadm03.homepage');
            //return View::make('slcadm03.landingpage');
            return View::make('slcadm03.storepage');
        } else if ($username === 'bkpfst') {
            return View::make('bkpfst.homepage');
        }else if ($username === 'bkpsnd') {
            return View::make('bkpsnd.homepage');
        }else if ($username === 'bkpthd') {
            return View::make('bkpthd.homepage');
        } else if ($username === 'slcadm00') {
            return View::make('slcadm00.homepage');
        } else if ($username === 'socadm00') {
            return View::make('socadm00.homepage');
        }
        else if($username === 'slcadm05'){
               return View::make('slcadm05.homepage');
       }else if ($username === 'actsup'){
       		$msg = '';
            return View::make('actsup.homepage',compact('msg'));
       } else {
            return Redirect::route('account-sign-out');
        }
        } else {
            return Redirect::route('account-sign-in');
        }
    }

    public function bookkeeper() {
    	$msg = $this->disp_start_win();
        return View::make('mktmgr.bookkeeper',compact('msg'));
    }
    public function disp_start_win()
    {
    	$start_date = date('Y-m-d');
    	//echo $start_date;exit;
    	//$start_date = '2017-03-08';
    	$weekday =  date('N', strtotime($start_date)); 
    	//echo $weekday;exit;
    	if($weekday == '3')
    	{
    		$prev_saturday = date('Y-m-d',strtotime($start_date . "-4 days"));
    		//$prev_saturday = "2017-03-18";
    		$prev_saturday_recap = DB::select('SELECT * FROM locks WHERE locks.date_stamp = "'.$prev_saturday.'" ');
    		$prev_saturday_recap_array = json_decode(json_encode($prev_saturday_recap), true);
    		//echo '<pre>';print_r($prev_saturday_recap_array);exit;
    		if (empty($prev_saturday_recap_array)) 
    		{
    			$prev_saturday1 = date('m/d/Y',strtotime($prev_saturday));
    		 	return "THE RECAP FOR  ".$prev_saturday1."  HAS NOT BEEN APPROVED, PLEASE APPROVE IMMEDIATELY.";//exit;
    		}
    	}
    	if($weekday == '2')
    	{
    		$prev_saturday = date('Y-m-d',strtotime($start_date . "-3 days"));
    		//$prev_saturday = "2017-03-18";
    		$prev_saturday_recap = DB::select('SELECT * FROM locks WHERE locks.date_stamp = "'.$prev_saturday.'" ');
    		$prev_saturday_recap_array = json_decode(json_encode($prev_saturday_recap), true);
    		//echo '<pre>';print_r($prev_saturday_recap_array);exit;
    		if (empty($prev_saturday_recap_array)) 
    		{
    			$prev_saturday1 = date('m/d/Y',strtotime($prev_saturday));
    		 	return "THE RECAP FOR, ".$prev_saturday1.", HAS NOT BEEN APPROVED, PLEASE APPROVE IMMEDIATELY.";//exit;
    		}
    	}	
    	//echo $start_date;exit;
    }
    public function getbookchgquery()
    {
    	return View::make('mktmgr.bookchgquery');
    }
    public function getbookchgadd()
   {         
       return View::make('mktmgr.bookchgadd');
   }
    public function getbookkeeperRegister() {
    		$registers_num=DB::select("SELECT * FROM registers order by 1");
        return View::make('mktmgr.bookkeeperregister', compact('registers_num'));
    }

    public function getbookkeeperRegistertotal() {
        return View::make('mktmgr.bookkeeperregistertotal');
    }

    public function getbookkeeperRegistercmt() {
        return View::make('mktmgr.bookkeeperregistercmt');
    }

    public function getbookloanRegister() {
    	$registers_num=DB::select("SELECT * FROM registers order by 1");
        return View::make('mktmgr.bookloanrregister', compact('registers_num'));
    }

    public function postBookLoanRRegister()
    {
    	//reg_num
	    //dateofinfo
    	
    	$validator = Validator::make(Input::all(),
            array(
                   'reg_num' =>'required',
                   'dateofinfo'=>'required',
                ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-bookloanrregistervalidation')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
          $dateofinfo = Input::get('dateofinfo');
          $date1 = str_replace('-', '/', $dateofinfo);
		  						$yesterday = date('Y-m-d',strtotime($date1 . "-1 days"));
		  						$bktodays = date('Y-m-d',strtotime($date1 . "-2 days"));
		  						$results = DB::select('SELECT max(date_stamp) as lastAprvlDate FROM mgmt');
		  						$resultArray = json_decode(json_encode($results), true); 
		  						//echo "<pre>";print_r($resultArray);exit;
						    $datesel=$resultArray[0]['lastAprvlDate'];// echo "<br />";//2016-06-01
						    $lastAprvlDate1day = date('Y-m-d',strtotime($datesel . "+1 days"));  //echo "<br />";
						    //$dateofinfo.'<= '.$datesel;
						    $reg_num = Input::get('reg_num');
            			    $convert_date = date("Y-m-d", strtotime(Input::get('dateofinfo')));
						    if($dateofinfo < $datesel)
						    {
						      Session::flash('alert-info','Data is locked for'.' '.$convert_date.'.'.' '.'Any changes won\'t be saved.');
						      
						      $item_id = DB::select('SELECT l.*, itid.item_desc FROM loans l, loanitems itid 
            	                   WHERE l.item_id = itid.item_id AND l.entry_Date="'.$dateofinfo.'"
            	                   AND l.reg_num ="'.$reg_num.'"');
						      if(count($item_id) <= 0)
						    		{
						    			 $dayIsLocked = "Y";
						    			 $item_id = DB::select("SELECT * FROM loanitems ORDER BY 1"); 
              return View::make('mktmgr.postbookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked'));
						    		}
						    		{
						    		 $dayIsLocked = "N";
						    			return View::make('mktmgr.updatebookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked')); 
						    		}
						    }
						    else
						    { 
						    	if($dateofinfo == $datesel)
						    	{
						    		$item_id = DB::select('SELECT l.*, itid.item_desc FROM loans l, loanitems itid 
            	                   WHERE l.item_id = itid.item_id AND l.entry_Date="'.$dateofinfo.'"
            	                   AND l.reg_num ="'.$reg_num.'"');
						    		if(count($item_id) <= 0)
						    		{
						    			 $dayIsLocked = "N";
						    			 $item_id = DB::select("SELECT * FROM loanitems ORDER BY 1"); 
              		return View::make('mktmgr.postbookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked'));
						    		}
						    		else
						    		{
						    			 $dayIsLocked = "N";
						    			 return View::make('mktmgr.updatebookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked')); 
						    		} 			
						    		}
						    		else
						    		{
						    			if($dateofinfo == $lastAprvlDate1day)
						    			{
						    				  $item_id = DB::select('SELECT l.*, itid.item_desc FROM loans l, loanitems itid 
            	                   WHERE l.item_id = itid.item_id AND l.entry_Date="'.$dateofinfo.'"
            	                   AND l.reg_num ="'.$reg_num.'"');
						    				  if(count($item_id) <= 0)
						    					 {
						    					 	 $item_id = DB::select("SELECT * FROM loanitems ORDER BY 1"); 
						    					 	 $dayIsLocked = "N";
              		  return View::make('mktmgr.postbookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked'));
						    					 }	
						    				  else
						    				  { $dayIsLocked = "Y";
						    				  	 return View::make('mktmgr.updatebookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked'));
						    				  }
						    			}
						    			else if($lastAprvlDate1day < $dateofinfo)
						    			{
						    				Session::flash('alert-danger','A manager need to approve daily work');
						    				$registers_num=DB::select("SELECT * FROM registers order by 1");
        						return View::make('mktmgr.bookloanrregister', compact('registers_num'));
						    			}
						    		}
						    	}
						    }
						   
    }

    public function postSaveBookLoanRRegister()
    {
    	$tot = Input::get('tot');
    	$dateofinfo = Input::get('dateofinfo');
    	$reg_num = Input::get('reg_num');
    	$convert_date = date("Y-m-d", strtotime(Input::get('dateofinfo')));
    	for($i=1; $i<$tot; $i++)
    	{
    		$allpostval=Input::all(); //vendorname
           // $loans = new Loans;
        	$elementamtid='val'.$i;
        	$elementid='id'.$i;
        	$itemid=Input::get($elementid);
        	$itemamt=Input::get($elementamtid);
        /*	if(Input::get($elementamtid) != '')
        	{
*/        		/*if(Input::get($elementid))
	    		{
	 			  $loans->item_id = Input::get($elementid);
	        	}
	        	if(Input::get($elementamtid))
	    		{
	 			  $loans->item_amt = Input::get($elementamtid);
	        	}
        		if(Input::get('reg_num'))
	    		{
	 			  $loans->reg_num = Input::get('reg_num');
	        	}
	        	if(Input::get('dateofinfo'))
	    		{
	    		  $convert_date = date("Y-m-d", strtotime(Input::get('dateofinfo')));
	 			  $loans->reg_num = $convert_date;
	        	}*/

	$receivings = DB::select('INSERT INTO loans(`item_id`, `item_amt`, `reg_num`, `source_id`, `entry_date`)
			      VALUES("'.$itemid.'","'.$itemamt.'","'.$reg_num.'","book1","'.$convert_date.'")');
        	//}
        			//$loans->save();
        	}
        	$gtamt=Input::get('gt');
            $receivings = DB::select('INSERT INTO loans(`item_id`, `item_amt`, `reg_num`, `source_id`, `entry_date`)
			          VALUES("GT","'.$gtamt.'","'.$reg_num.'","book1","'.$convert_date.'")');
        	Session::flash('alert-success', 'Record added successfully!');
        	$dayIsLocked ='Y';
        	$item_id = DB::select("SELECT * FROM loanitems ORDER BY 1");
         return View::make('mktmgr.postbookloanrregister',compact('item_id', 'reg_num', 'dateofinfo', 'dayIsLocked')); 
    }
    

	 public function getbookloanregTotal(){
		   return View::make('mktmgr.bookloanrregtotal'); 
	 }

	 public function postBookLoanRRegTotal()
	 {
	 	$work_data = new StdClass();
	 	$itemid = Input::get('totcode');
	 	$dateofinfo = Input::get('dateofinfo');
	 	$tot_date = Input::get('dateofinfo');
	 	$itemname = 'Grand Total';
	 	if($itemid == 'LR')
	 	{
	 	  $itemname = 'LOANS TO REG TOTAL';
	 	  /*$regnum = Input::get('reg_num');*/
	 	  $regnum='';
	 	  $dayIsLocked = 'Y';
	      $dateofinfo = Input::get('dateofinfo');
          $convert_date = date("Y-m-d", strtotime(Input::get('dateofinfo')));	
          $getdata = DB::select("SELECT * FROM loans WHERE entry_date = '".$convert_date."' AND 
          	                     item_id='".$itemid."'");
         
          $resultArray = json_decode(json_encode($getdata), true);

          if(count($resultArray) > 0)
          { $itemid == 'LR';
          	$dateofinfo = date("m-d-Y", strtotime(Input::get('dateofinfo')));
          	Session::flash('alert-info', 'Data is locked for'.' '.$dateofinfo.'.'.' '.'Any changes won\'t be saved.');
            return View::make('mktmgr.bookloanrregtotlist',compact('dateofinfo', 'itemid', 'itemname', 'dayIsLocked', 'resultArray')); 	
           // return View::make('mktmgr.bookloanrregtotlist',compact('itemid', 'itemname','regnum', 'dateofinfo'));  	
          }
          else
          {
          	//Session::flash('alert-danger', 'No data found!'); 
   	 		return View::make('mktmgr.bookloanrregtotal');
          }	
	 	}
	 	else
	 	{
	 	  $reg_num = Input::get('reg_num');
	      $dateofinfo = Input::get('dateofinfo');
          $convert_date = date("Y-m-d", strtotime(Input::get('dateofinfo')));	
          $item_id = DB::select("SELECT * FROM loans WHERE entry_date = '".$convert_date."'");
          if(count($item_id) > 0)
          {
            return View::make('mktmgr.bookloanrregtotlist',compact('item_id', 'reg_num', 'dateofinfo'));  	
          }
          else
          {
          	Session::flash('alert-danger', 'A manager need to approve daily work'); 
   	 		return View::make('mktmgr.bookloanrregtotal');
          }	
	 	}	

	 //}
	}

	 function get_totdbcr($type,$tot_date)
	 {
	 	$itemamt = DB::select("SELECT  SUM(e.item_amt) subtot FROM  entry e,items i
	 		                    WHERE e.entry_date = '".$tot_date."' 
					            AND  e.item_id = i.item_id
					            AND  i.item_type = '".$type."'");
	 	$itemamt = json_decode(json_encode($itemamt), true);
	 	if(count($itemamt) > 0)
	 	{
	 		foreach ($itemamt as $key)
	 		{
	 		  $itemamt=$itemamt[0]['subtot'];
	 	    }	
	 	}	
	 	return ($itemamt);
	 }

	 function get_itemtot($type,$tot_date)
	 {
	    $itemamt = DB::select("SELECT sum(item_amt) as gsubtot FROM entry WHERE 
	    	                    entry.item_id =  '".$type."' 
	    	                    AND entry.entry_date = '".$tot_date."'");
	 	$itemamt = json_decode(json_encode($itemamt), true);
	 	if(count($itemamt) > 0)
	 	{
	 		foreach ($itemamt as $key)
	 		{
	 		  $itemamt=$itemamt[0]['gsubtot'];
	 	    }	
	 	}	
	 	return ($itemamt);
	 	//return 1;
	 }

	 function get_os($tot_date)
	 { $i=0; $arrayname = array(); $subarray = array();
	 	$itemlist = DB::select('select items.* from items where items.item_id not in ("GT","DS","#C") 	
								and items.item_type <> "X" and items.disable = "N"
								order by items.item_type,items.item_desc');
	 	$itemlist = json_decode(json_encode($itemlist), true);
	 	foreach ($itemlist as $key) { //echo "<br />";
	 		$total = 0;
	 		switch ($key['item_id'])
	 	  	{
		 	  	case "TC" :
		 	  		 $tot_cred = $this->get_totdbcr('C',$tot_date); //echo "<br />";
		 	  		 $arrayname['tot_cred'] = $tot_cred; 
		 	  	break;
		     case "TD" :
		     	    $tot_deb = $this->get_totdbcr('D',$tot_date); //echo "<br />";
		     	    $arrayname['tot_deb'] = $tot_deb;
		 	  	break;
		     case "OS" :
		     		 $subtot1=$this->get_tot_os($tot_date , 'D'); $subtot2=$this->get_tot_os($tot_date , 'C');
		     	     $gsubtot = $subtot1 - $subtot2; $tot_os = $gsubtot/100; //echo "<br />";
		     	     $arrayname['tot_os'] = $tot_os;
		     break;
		     default:
		           $alldata=$this->gettotothers($key['item_id'], $tot_date);
		           $subarray[$i] = new stdClass();
		           if($alldata == '0')
		           {
			            $subarray[$i]->item_id = 0;
			            $subarray[$i]->gsubtot = 0;
			            $subarray[$i]->item_desc = 0;
		           }
		           else
		           {
		           	   $iteamid_name = explode("^", $alldata); 
			           $subarray[$i]->item_id = $iteamid_name[0];
			           $subarray[$i]->gsubtot = $iteamid_name[1];
			           $subarray[$i]->item_desc = $iteamid_name[2];
		           }

		       break;
		    }  
		     $i=$i+1;        
	 	}
	 	
	 	return array($arrayname,$subarray); //$arrayname;
	 }

	 
	 public function get_tot_os($tot_date, $itemid)
	 {
	 	 $itemamtval = 0;
	 	 $sql = DB::select('select sum(entry.item_amt) as subtot from entry, items where 
	 	 	                entry.entry_date = "'.$tot_date.'" and items.item_type = "'.$itemid.'"
	 	 	                and entry.item_id = items.item_id');
			$itemamt = json_decode(json_encode($sql), true);
	 	if(count($itemamt) > 0)
	 	{
	 		foreach ($itemamt as $key)
	 		  {
	 		    $itemamtval=$itemamt[0]['subtot'];
	 	   }	
	 	}	
	 	return ($itemamtval);
	 }

	 public function gettotothers($item_id, $tot_date)
	 {
	 	$apenddata = 0;
	 	$sql = DB::select('select sum(entry.item_amt) as gsubtot, items.item_desc as item_desc from entry, items where 
	 						entry.item_id=items.item_id and entry.entry_date = "'.$tot_date.'" 
	 						and entry.item_id = "'.$item_id.'"');
		$itemamt = json_decode(json_encode($sql), true);
		if($itemamt[0]['gsubtot'] != '')
		{
		  $apenddata=$item_id.'^'.$itemamt[0]['gsubtot'].'^'.$itemamt[0]['item_desc'];
		}
		else
		{
			$apenddata=$item_id.'^'.'00.00'.'^'.$itemamt[0]['item_desc'];
		}
		return $apenddata;
	 }

	 public function valid_id($temp_id)
	 {
$itemamt = DB::select("SELECT * FROM items WHERE item_id = '".$temp_id."' AND items.disable ='N'");
	 	if(count($itemamt) == 0)
	 	{
	 		return false;
	 	}
	 	else
	 	{
	 		return true;
	 	}
	 } 
	 public function getbookloanregCmt(){
		    return View::make('mktmgr.bookloanrregcmt'); 
	 }
	/*public function receivings()
	{
		return View::make('mktmgr.receivings');
	}*/

	 public function getbookregcheckoutItem(){
	 	//$itemslist = DB::table('items')->get();
	 $itemslist = DB::select('SELECT * FROM items WHERE item_id NOT IN ("#C","DS","GT","OS","LR","BL","TD","TC")');	
		return View::make('mktmgr.bookregcheckoutitem', compact('itemslist')); 
	 }

	 public function postSaveBookRegCheckoutItem()
	 {
	 	$validator = Validator::make(Input::all(),
            array(
                   'iteamid_name' =>'required',
                   'dateofinfo'=>'required'
                ),array('iteamid_name.required'=>'Item code is required',
                		'dateofinfo.required'=>'Date is required'));
	 	
	 	if($validator->fails())
        {
            return Redirect::route('mktmgr-bookregcheckoutitem')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        { 
        	$iteamid_name = Input::get('iteamid_name');
	 							$dateofinfo = Input::get('dateofinfo');
   						$iteamid_name = explode("^", $iteamid_name); $itemid = $iteamid_name[0]; $itemname = $iteamid_name[1];
        	$prvDate = date('m-d-Y', strtotime(date( $dateofinfo, strtotime('-1 day')))); 
        	$convert_date = date("Y-m-d", strtotime(Input::get('dateofinfo')));
        	$results = DB::select('select max(date_stamp) as lastAprvlDate from mgmt');
        	$resultArray = json_decode(json_encode($results), true); $lastAprvlDate = $resultArray[0]['lastAprvlDate'];

        	//$dateofinfo = Input::get('selecteddate');
									/*$itemid = Input::get('selecteditemsid');
									$itemname = Input::get('selecteditemsname'); */

									$lastAprvlDate=date_create($lastAprvlDate);
									date_add($lastAprvlDate,date_interval_create_from_date_string("1 days"));
									$maxdate1day=date_format($lastAprvlDate,"Y-m-d");
									//echo $maxdate1day.'>=<'.$dateofinfo;  echo "<br />";//exit;
        	if($maxdate1day < $dateofinfo)
        	{
        			Session::flash('alert-danger','A manager needs to approve daily work.');
        		 $itemslist = DB::select('select * from items where item_id not in ("#C","DS","GT","OS","LR","BL","TD","TC")');	
									  return View::make('mktmgr.bookregcheckoutitem', compact('itemslist')); 
        	}
        	else if($maxdate1day == $dateofinfo)
        	{
        		/*echo "SELECT * FROM entry WHERE entry_date = '".$dateofinfo."' 
        	    	                   AND item_id='".$itemid."' order by reg_num"; exit;*/
        		  $getdata = DB::select("select item_amt/100 as item_amt,item_id, reg_num, source_id, source_id, entry_date 
        		  	                     from entry WHERE entry_date = '".$dateofinfo."' 
        	    	                     and item_id='".$itemid."' order by reg_num");
        			 $resultArray = json_decode(json_encode($getdata), true);
        		  if(count($resultArray) > 0)
        		  { //echo "date is equal then max date in DB new updates!"; exit; //only updates are possiable
        		  	 // Session::flash('alert-info','only updates');
        		  	$dayIsLocked = 'N'; $dateofinfo = date("m-d-Y", strtotime(Input::get('dateofinfo')));
   return View::make('mktmgr.bookregcheckoutitemsaveupdate',compact('dateofinfo', 'itemid', 'itemname', 'dayIsLocked', 'resultArray'));
        		  }
        		  else
        		  { //echo "date is equal then max date in DB allready inserted!"; exit; //only insert are possiable
        		     //Session::flash('alert-info','new insert');

        		  	$dayIsLocked = 'N'; $dateofinfo = date("m-d-Y", strtotime(Input::get('dateofinfo')));
   return View::make('mktmgr.bookregcheckoutitemsave',compact('dateofinfo', 'itemid', 'itemname', 'dayIsLocked', 'resultArray'));
        		  }
        	}
        	else if($maxdate1day > $dateofinfo)
        	{	//echo "date is less then max date in DB"; exit; //show only
        		 $dayIsLocked = 'Y';
        		 Session::flash('alert-info','Date is locked. Changes will not be saved.');
        		 $getdata = DB::select("select item_amt/100 as item_amt,item_id, reg_num, source_id, source_id, entry_date from entry 
        								   where entry_date = '".$dateofinfo."' 
        	    	                   	   and item_id='".$itemid."' order by reg_num");
        		$resultArray = json_decode(json_encode($getdata), true);
        		if(count($resultArray) > 0)
        		{
  					return View::make('mktmgr.bookregcheckoutitemsaveupdate',compact('dateofinfo', 'itemid', 'itemname', 'dayIsLocked', 'resultArray'));       			
        		}
        		else
        		{
        		  $results = array();
        		  //$i = array('1','2','3','4','5','6', '7', '8','9','12','13','14','15','17','29','30');
				  for($i = 0; $i<=15; $i++)
				  {
				  	$results[$i]['item_amt']='00.00';
				  }
				  //var_dump($results); exit;
				  $resultArray = json_decode(json_encode($results), true);
        		  return View::make('mktmgr.bookregcheckoutitemsaveupdate',compact('dateofinfo', 'itemid', 'itemname', 'dayIsLocked', 'resultArray')); 
        		}
  
        	}

        	#-- check previous day approval ------ 2016-07-10 ---- 10-03-2016 // WHERE date_stamp <"'.$dateofinfo.'"
        }    	
	 }

	 public function postbookregcheckoutitemupdateall()
	 {
	    $dateofinfo = Input::get('selecteddate');
		$itemid = Input::get('selecteditemsid');
		$itemname = Input::get('selecteditemsname');
		$convert_date = date("Y-d-m",strtotime(trim($dateofinfo)));
		$i = array('1','2','3','4','5','6', '7', '8','9','12','13','14','15','17','29','30');
		$dayIsLocked= 'N';
		//item_id, item_amt, reg_num, source_id, entry_date
		//$entry = new entry;
		foreach ($i as $key) {
			$itemid=Input::get('selecteditemsid'); $regnum = Input::get('rg'.$key);
			$itemamt = Input::get('amt'.$key); $source_id = 'book1';
			$itemamt=str_replace(".","",$itemamt);
		/*echo 'select * from entry where `item_id`  ="'.$itemid.'"  and `entry_date`="'.$convert_date.'"'; echo "<br />";

		$dtchk=DB::select('select * from entry where `item_id`  ="'.$itemid.'"  and `entry_date`="'.$convert_date.'"');
		$dtchk = json_decode(json_encode($dtchk), true);
		if(count($dtchk) > 0)
		{
			 echo "update data : ".$itemid; echo "<br />";
		}
		else
		{
			echo "insert data : ".$itemid; echo "<br />";
		}*/
			
			/*echo 'UPDATE entry
			 	                       SET `item_amt`   ="'.$itemamt.'"
			 	                       WHERE `item_id`  ="'.$itemid.'" AND
			 	                             `reg_num`  ="'.$regnum.'" AND
			 	                             `source_id`="'.$source_id.'" AND
			 	                             `entry_date`="'.$convert_date.'"'; echo "<br />";*/
			 $receivings = DB::update('update entry
			 	                       set `item_amt`   ="'.$itemamt.'"
			 	                       where `item_id`  ="'.$itemid.'" and
			 	                             `reg_num`  ="'.$regnum.'" and
			 	                             `source_id`="'.$source_id.'" and
			 	                             `entry_date`="'.$convert_date.'"');
        	   } 
         Session::flash('alert-success', 'Record updated successfully!');		   
		 $getdata = DB::select("select item_amt/100 as item_amt,item_id, reg_num, source_id, source_id, entry_date  from 
		 	                    entry where entry_date = '".$convert_date."' and item_id='".$itemid."' order by reg_num");	
         $resultArray = json_decode(json_encode($getdata), true);
         $insetednot = 'inserted'; 	   
        return View::make('mktmgr.bookregcheckoutitemsaveupdate',compact('dateofinfo', 'itemid', 'itemname', 'dayIsLocked', 'resultArray'));	   
	 }

	 public function postUpdateBookRegCheckOutItem()
	 {
	 	$dateofinfo = Input::get('selecteddate');
		$itemid = Input::get('selecteditemsid');
		$itemname = Input::get('selecteditemsname');
		$convert_date = date("Y-d-m",strtotime(trim($dateofinfo)));
		$i = array('1','2','3','4','5','6', '7', '8','9','12','13','14','15','17','29','30');
		$dayIsLocked= 'N';
		//item_id, item_amt, reg_num, source_id, entry_date
		//$entry = new entry;
		foreach ($i as $key) {
			/*$entry->item_id = Input::get('selecteditemsid');
			$entry->item_amt = Input::get('amt'.$key);
			$entry->reg_num = Input::get('rg'.$key);
			$entry->source_id = 'book1';
			$entry->entry_date = $convert_date;
			$entry->save();*/
			$itemid=Input::get('selecteditemsid'); $regnum = Input::get('rg'.$key);
			$itemamt = Input::get('amt'.$key); $source_id = 'book1';

			/*echo 'INSERT INTO entry(`item_id`, `item_amt`, `reg_num`, `source_id`, `entry_date`)
			 						   VALUES("'.$itemid.'","'.$itemamt.'","'.$regnum.'","'.$source_id.'","'.$convert_date.'")';
			 						   echo "<br />";*/
			 $receivings = DB::select('INSERT INTO entry(`item_id`, `item_amt`, `reg_num`, `source_id`, `entry_date`)
			 						   VALUES("'.$itemid.'","'.$itemamt.'","'.$regnum.'","'.$source_id.'","'.$convert_date.'")');
		}

	//$convert_date = date("Y-m-d",strtotime(trim($dateofinfo)));
	Session::flash('alert-success', 'Record updated successfully!');	
	return View::make('mktmgr.bookregcheckoutitemsave',compact('dateofinfo', 'itemid', 'itemname', 'dayIsLocked'));
		
	 }

	  public function getbookregcheckoutReg(){
	  	$registers_num=DB::select("SELECT * FROM registers order by 1");
		return View::make('mktmgr.bookregcheckoutreg', compact('registers_num'));
	 }

	 public function postBookRegCheckoutRefSave()
	 {
	 	$validator = Validator::make(Input::all(),
            array(
                   'regnumber' =>'required',
                   'dateofinfo'=>'required',
                ));
	 	$regnumber = Input::get('regnumber');
	 	$dateofinfo = Input::get('dateofinfo');
        
		
		if($validator->fails() || $iteamid_name == '')
        {
            return Redirect::route('mktmgr-bookregcheckoutreg')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {

        }
	 }

	  public function getbookregcheckoutTotl(){
	  	$itemslist = DB::select('SELECT * FROM items WHERE item_id IN ("#C","DS","GT","OS","LR","BL","TD","TC")');	
		return View::make('mktmgr.bookregcheckouttotl', compact('itemslist')); 
	 }

	 public function getViewDataRegChkOut()
	 {

	 }

	 public function postBookregChkOutTot(){

	 		$iteamid_name = Input::get('iteamid_name'); $dateofinfo = Input::get('dateofinfo');
   	        $iteamid_name = explode("^", $iteamid_name); $itemid = $iteamid_name[0]; $itemname = $iteamid_name[1];

   			switch ($itemid)
	 	  	{
		 	  	case "#C" :
		              $resultArray=$this->add_automate($dateofinfo,"#C");
		              $dayIsLocked= 'Y'; $itemname=$iteamid_name[1]; $itemid=$iteamid_name[0];
	       			  return View::make('mktmgr.bookregcheckouttotlviewall',compact('resultArray', 'dateofinfo', 'itemid', 'itemname','dayIsLocked'));
		              //return View::make('mktmgr.bookregcheckouttotlviewall', compact('itemid','itemname','tableset')); 
		              break;
		        case "DS" :
		              $tot_deb=$this->get_totdbcr("D",$dateofinfo);
		              break;
		        case "GT" :
		              $tot_os=$this->get_os($dateofinfo);
		              $totarray = $tot_os[0]; $alldata = $tot_os[1];

		              //$alldata = (object) $alldata;
		              //echo '<pre>'; print_r($totarray); echo '</pre>'; exit;
		              $convert_date = date("m/d/Y", strtotime(Input::get('dateofinfo')));
		              return View::make('mktmgr.bookregcheckouttotlgt',compact('totarray', 'alldata', 'convert_date', 'itemid', 'itemname'));
		              break;
		        case "OS" :
		               $resultArray=$this->add_automate($dateofinfo,"OS");
		              $dayIsLocked= 'Y'; $itemname=$iteamid_name[1]; $itemid=$iteamid_name[0];
	       			  return View::make('mktmgr.bookregcheckouttotlviewall',compact('resultArray', 'dateofinfo', 'itemid', 'itemname','dayIsLocked'));
		              break;
		        case "LR" :
		              $resultArray=$this->add_automate($dateofinfo,"LR");
		              $dayIsLocked= 'Y'; $itemname=$iteamid_name[1]; $itemid=$iteamid_name[0];
	       			  return View::make('mktmgr.bookregcheckouttotlviewall',compact('resultArray', 'dateofinfo', 'itemid', 'itemname','dayIsLocked'));
		              break;
		        case "BL" :
		              $resultArray=$this->add_automate($dateofinfo,"BL");
		              $dayIsLocked= 'Y'; $itemname=$iteamid_name[1]; $itemid=$iteamid_name[0];
	       			  return View::make('mktmgr.bookregcheckouttotlviewall',compact('resultArray', 'dateofinfo', 'itemid', 'itemname','dayIsLocked'));
		              break;
		        case "TD" :
		              $resultArray=$this->add_automate($dateofinfo,"TD");
		              $dayIsLocked= 'Y'; $itemname=$iteamid_name[1]; $itemid=$iteamid_name[0];
	       			  return View::make('mktmgr.bookregcheckouttotlviewall',compact('resultArray', 'dateofinfo', 'itemid', 'itemname','dayIsLocked'));
		              break;
		        case "TC" :
		              $resultArray=$this->add_automate($dateofinfo,"TC");
		              $dayIsLocked= 'Y'; $itemname=$iteamid_name[1]; $itemid=$iteamid_name[0];
	       			  return View::make('mktmgr.bookregcheckouttotlviewall',compact('resultArray', 'dateofinfo', 'itemid', 'itemname','dayIsLocked'));
		              break;
	       } 
	 }

	 public function add_automate($form_date, $choice)
	 {
	 	$sql=DB::select('select r.reg_num, e.item_amt from registers r, entry e where r.reg_num = e.reg_num 
	 					AND e.item_id = "'.$choice.'" AND e.entry_date = "'.$form_date.'" order by r.reg_num ASC');
	 	return $resultArray = json_decode(json_encode($sql), true);
	 }
	 public function getbookregcheckoutPrnt(){
		return View::make('mktmgr.bookregcheckoutprnt'); 
	 }
	 public function getbookregcheckoutCmt(){
		return View::make('mktmgr.bookregcheckoutcmt'); 
	 }
	  public function getbookregCmt(){
		return View::make('mktmgr.bookregcmt'); 
	 }
	 public function getbooksafeCnt(){
		return View::make('mktmgr.booksafecnt'); 
	 }
	 public function getbooksafePrnt(){
		return View::make('mktmgr.booksafeprnt'); 
	 }
	  public function getbooksafeReport(){
	  	//echo date('Y-m-d') - 1;exit;

		return View::make('mktmgr.booksafereport'); 
	 }
	  public function getbooksafereportPrnt(){
		return View::make('mktmgr.booksafereportprnt'); 
	 }
	  public function getbooksafereportCmt() {
		return View::make('mktmgr.booksafereportcmt'); 
	 }
	  public function getbooksafeCmt() {
		return View::make('mktmgr.booksafecmt'); 
	 }
	  public function getbookwklyUnlock(){
		return View::make('mktmgr.bookwklyunlock'); 
	 }
	  public function getbookwklyCmt(){
		return View::make('mktmgr.bookwklycmt'); 
	 }
	  public function getwklyPrnt(){
		return View::make('mktmgr.bookwklyprnt'); 
	 }
	  public function getbookwklyRecap()
	 {
		return View::make('mktmgr.bookwklyrecap'); 
	 }
	  public function getbookwklySales()	 {
		return View::make('mktmgr.bookwklysales'); 
	 }
	  public function getbookwklyView(){
		return View::make('mktmgr.bookwklyview'); 
	 }
	  public function getbookpostageAdd()
	 {
	 	$work_data = new StdClass();
        $work_data->dateofinfo=date('m/d/Y', strtotime("last Saturday"));
        return View::make('mktmgr.bookpostageadd')->with('data',$work_data);
	 }
	 
	
	  public function getbookmngmntCmt()
	 {
		return View::make('mktmgr.bookmngmntcmt'); 
	 }
	 //  public function getbookutilitypymntCnt()
	 // {
		// return View::make('mktmgr.bookutilitypymntcnt'); 
	 // }
	 public function getbookcommentList()
	 {
		$comments = new BookkeepComment;
        $comments = $comments->orderBy('date_stamp', 'desc')->paginate(20);
        return View::make('mktmgr.bookcommentlist', compact('comments')); 
	 }
	 public function getcommentList()
	 {
		return View::make('mktmgr.commentlist'); 
	 }
	 public function postcommentDate()
	 {
	 	$validator = Validator::make(Input::all(),
            array(
                   'comment_date'=>'required',
                ),array('comment_date.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-commentlist')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
	 	$comment_date = Input::get('comment_date');
	 	//echo $comment_date;exit;
	 	$results = DB::select('SELECT * from comment WHERE date_stamp ="'.$comment_date.'" ');
		$comments = json_decode(json_encode($results), true); 
		//echo '<pre>';print_r($comments);exit;
		if($comments)
		{
			return View::make('mktmgr.updatecomment',compact('comment_date','comments'));
		}
		else
		{
			return View::make('mktmgr.addcomment',compact('comment_date'));	
		}
		}
	 }
	 public function postAddcommentData()
	 {
	 	$BookkeeperComment = new BookkeeperComment;
	 	//echo Input::get('date_stamp');exit;
		if(Input::has('date_stamp')) {
            $BookkeeperComment->date_stamp = Input::get('date_stamp');
        }
        if(Input::has('comment_line1')) {
            $BookkeeperComment->comment_line1 = Input::get('comment_line1');
        }
        if(Input::has('comment_line2')) {
            $BookkeeperComment->comment_line2 = Input::get('comment_line2');
        }
        if(Input::has('comment_line3')) {
            $BookkeeperComment->comment_line3 = Input::get('comment_line3');
        }
        if(Input::has('comment_line4')) {
            $BookkeeperComment->comment_line4 = Input::get('comment_line4');
        }
        if(Input::has('comment_line5')) {
            $BookkeeperComment->comment_line5 = Input::get('comment_line5');
        }
        if(Input::has('comment_line6')) {
            $BookkeeperComment->comment_line6 = Input::get('comment_line6');
        }
        if(Input::has('comment_line7')) {
            $BookkeeperComment->comment_line7 = Input::get('comment_line7');
        }
        if(Input::has('comment_line8')) {
            $BookkeeperComment->comment_line8 = Input::get('comment_line8');
        }
        if(Input::has('comment_line9')) {
            $BookkeeperComment->comment_line9 = Input::get('comment_line9');
        }
        if($BookkeeperComment->save())
        {
        	Session::flash('alert-success', 'Comment added Successfully!'); 
	        return Redirect::route('mktmgr-commentlist');
        }
	 }
	 public function postUpdatecommentData()
	 {
	 	$date_stamp = Input::get('date_stamp');
      	$UpdatecommentData = DB::table('comment')
                ->where('date_stamp', $date_stamp)
                ->update(array(
                  'comment_line1' =>Input::get('comment_line1'),
                  'comment_line2' => Input::get('comment_line2'),
                  'comment_line3' => Input::get('comment_line3'),
                  'comment_line4' => Input::get('comment_line4'),
                  'comment_line5' => Input::get('comment_line5'),
                  'comment_line6' => Input::get('comment_line6'),
                  'comment_line7' => Input::get('comment_line7'),
                  'comment_line8' => Input::get('comment_line8'),
                  'comment_line9' => Input::get('comment_line9')
                  ));
        if($UpdatecommentData)
        {
          Session::flash('alert-success', 'Comment Updated Successfully!'); 
            return Redirect::route('mktmgr-commentlist');
        }
        else
        {
          Session::flash('alert-danger', 'Comment Update Failure!'); 
            return Redirect::route('mktmgr-commentlist');
        }
	 }
	 public function commentAdd()
	 {
	 	return View::make('mktmgr.addcomment'); 
	 }
	 public function bookcommentAdd()
	 {
        if(Input::has('dateofinfo')) {
            $date_stamp = Input::get('dateofinfo');
        }
        else {
            $date_stamp = date('Y-m-d');
        }
        if(!is_null($cmt_data = BookkeepComment::where('date_stamp',$date_stamp)->first())) {
            $cmt_data = BookkeepComment::where('date_stamp',$date_stamp)->first();
            $cmt_data->exists = true;
        }
        else {
            $cmt_data = new BookkeepComment;
            $cmt_data->date_stamp = $date_stamp;
            $cmt_data->exists = false;
            $cmt_data->comment_line1 = '';
        }
        
        return View::make('mktmgr.bookcommentadd')->with('cmt_data',$cmt_data);
	 }
	 public function postBookcomment12()
	 {
        $validator = Validator::make(Input::all(),
    		array(
    			   'dateofinfo' =>'required',
    			   'commenttext'=>'required'
    			));
		if($validator->fails())
		{
			return Redirect::route('mktmgr-bookcommentadd')
					->withErrors($validator)
					->withInput();
		}
		else
		{
            if(is_null($cmt_data = BookkeepComment::where('date_stamp',Input::get('dateofinfo'))->first())) {
                $addComment = new BookkeepComment;
                if(Input::has('dateofinfo')) {
                    $addComment->date_stamp = Input::get('dateofinfo');
                }
                if(Input::has('commenttext')) {
                    $addComment->comment_line1 = trim(Input::get('commenttext'));
                }
                $addComment->comment_line2 = '';
                $addComment->comment_line3 = '';
                $addComment->comment_line4 = '';
                $addComment->comment_line5 = '';
                $addComment->comment_line6 = '';
                $addComment->comment_line7 = '';
                $addComment->comment_line8 = '';
                $addComment->comment_line9 = '';
                $addComment->save();
                //var_dump($addComment); exit;
            }
            else {
                if(Input::has('dateofinfo')) {
                    $arr['date_stamp'] = Input::get('dateofinfo');
                }
                if(Input::has('commenttext')) {
                    $arr['comment_line1'] = trim(Input::get('commenttext'));
                }
                $arr['comment_line2'] = '';
                $arr['comment_line3'] = '';
                $arr['comment_line4'] = '';
                $arr['comment_line5'] = '';
                $arr['comment_line6'] = '';
                $arr['comment_line7'] = '';
                $arr['comment_line8'] = '';
                $arr['comment_line9'] = '';
    
                BookkeepComment::where('date_stamp',Input::get('dateofinfo'))->update($arr);
            }
            return Redirect::route('mktmgr-bookcommentlist');
        }
    }

	 
        public function getbookpostagePost() {

        $work_data = new StdClass();
        
        
          Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
      
        
        $validator = Validator::make(Input::all(),
            array(                   
                   'dateofinfo'=>'required|date|weekenddatecheck',
                ),array('dateofinfo.required'=>'Date is required'));
        
        

        //print_r($validator);

        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-bookpostageadd')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {

            if (Input::has('dateofinfo')) {
                $date_stamp = Input::get('dateofinfo');
                //$date_stamp = '1989-11-16';
                $dt = new DateTime($date_stamp);
                // $datecheck = new DateTime('1990-01-01');
                // if ($dt < $datecheck) {
                //     $dt = new DateTime("yesterday");
                //     $date_stamp = $dt->format('Y-m-d');
                // }
                $this->date_stamp = date('Y-m-d',strtotime($date_stamp)); 
                //echo $this->date_stamp;exit;
                //==put validation for weekending date only Saturday
                //date('w', strtotime($date));


                $work_data->dateofinfo = date('Y-m-d',strtotime($date_stamp)); 

                $file_locked = $this->file_lock($date_stamp);
	        	
	        	if($file_locked)
	        	{
	        		$file_locked = "Y";
	        	}
	        	else
	        	{
	        		$file_locked = "N";
	        	}
	        	
                //===init_rec
                $work_data->ck_no_1 = 0;
                $work_data->ck_no_2 = 0;
                $work_data->begin_inv = 0;
                $work_data->purchases = 0;
                $work_data->end_inv_safe = 0;
                $work_data->end_inv_tills = 0;
                $work_data->end_inv_ca = 0;
                $work_data->ps_unity_sales = 0;

                
                
                $work_data->last_inventory_date=$date_stamp;
                
               
                
         $last_inventory_row = DB::select(DB::raw("SELECT postage.ps_date,(postage.end_inv_safe + postage.end_inv_tills + postage.end_inv_ca) as work_begin_inv FROM postage WHERE postage.ps_date < ? ORDER BY postage.ps_date DESC "), array($this->date_stamp));
                 if (!empty($last_inventory_row)) {
                        if (isset($last_inventory_row[0])) {
                            $last_row_data = $last_inventory_row[0];
                            $work_data->last_inventory_date=$last_row_data->ps_date; 
                            $work_data->begin_inv = $last_row_data->work_begin_inv; 
                           
                        }
                 }

                
                $results = DB::select(DB::raw("SELECT * FROM postage WHERE postage.ps_date = ?"),array($this->date_stamp));
                
               
            if(!empty($results)){
                if(isset($results[0])){
                    $row_data=$results[0];
                    $work_data->ck_no_1 = $row_data->ck_no_1;
                    $work_data->ck_no_2 = $row_data->ck_no_2;
                    //$work_data->begin_inv = $row_data->begin_inv;
                    $work_data->purchases = $row_data->purchases;
                    $work_data->end_inv_safe = $row_data->end_inv_safe;
                    $work_data->end_inv_tills = $row_data->end_inv_tills;
                    $work_data->end_inv_ca = $row_data->end_inv_ca;
                    $work_data->ps_unity_sales = $row_data->ps_unity_sales;
                }
            }
           
           //===init_formrec
                 $work_data->calc_sales =   ($work_data->begin_inv + $work_data->purchases -  $work_data->end_inv_safe - $work_data->end_inv_tills - $work_data->end_inv_ca) ;
                 $work_data->calc_sales = $work_data->calc_sales / 100;
          
                
                $work_data->sales_diff =   (($work_data->calc_sales*100) - $work_data->ps_unity_sales); 
                $work_data->sales_diff = $work_data->sales_diff /100;
                
                
                $work_data->calc_sales = number_format($work_data->calc_sales ,2, '.', '');
                $work_data->sales_diff = number_format($work_data->sales_diff ,2, '.', '');
                
                //==disp_postage
              
                
                
                $work_data->begin_inv = $work_data->begin_inv/100;
                $work_data->purchases = $work_data->purchases/100;
                $work_data->end_inv_safe = $work_data->end_inv_safe/100;
                $work_data->end_inv_tills = $work_data->end_inv_tills/100;
                $work_data->end_inv_ca = $work_data->end_inv_ca/100;
                $work_data->ps_unity_sales = $work_data->ps_unity_sales/100;
                
                $work_data->begin_inv = number_format($work_data->begin_inv ,2, '.', '');
                $work_data->purchases = number_format($work_data->purchases ,2, '.', '');
                $work_data->end_inv_safe = number_format($work_data->end_inv_safe ,2, '.', '');
                $work_data->end_inv_tills = number_format($work_data->end_inv_tills ,2, '.', '');
                $work_data->end_inv_ca = number_format($work_data->end_inv_ca ,2, '.', '');
                $work_data->ps_unity_sales = number_format($work_data->ps_unity_sales ,2, '.', '');
         
               
               
                
            }
              return View::make('mktmgr.bookpostagepost',compact('work_data','file_locked'));

            /* */
        }
    }
    

    
        
        
    public function postbookpostageData() {



        if (Input::has('ps_date') && Input::has('accept-submit')) {
           
            $date_stamp = Input::get('ps_date');
            //$date_stamp = '1989-11-16';
            $dt = new DateTime($date_stamp);
            // $datecheck = new DateTime('1990-01-01');
            // if ($dt < $datecheck) {
            //     $dt = new DateTime("yesterday");
            //     $date_stamp = $dt->format('Y-m-d');
            // }
            $this->date_stamp = date('Y-m-d',strtotime($date_stamp)); 


            $results = DB::select(DB::raw("SELECT * FROM postage WHERE postage.ps_date = ?"), array($this->date_stamp));

            $action = "insert";
            if (!empty($results)) {
                if (isset($results[0])) {
                    $action = "update";
                }
             }
             
             
             $begin_inv = Input::get('begin_inv')*100;
             $purchases = Input::get('purchases')*100;
             $end_inv_safe = Input::get('end_inv_safe')*100;
             $end_inv_tills = Input::get('end_inv_tills')*100;
             $end_inv_ca = Input::get('end_inv_ca')*100; 
             $ps_unity_sales = Input::get('ps_unity_sales')*100;
             
             $ps_date=$this->date_stamp;
             
             
             if($action=="insert"){
                 DB::table('postage')->insert(
                    ['ps_date' => $ps_date, 'ck_no_1' => Input::get('ck_no_1'), 'ck_no_2' => Input::get('ck_no_2'), 'begin_inv' => $begin_inv, 'purchases' => $purchases, 'end_inv_safe' => $end_inv_safe, 'end_inv_tills' => $end_inv_tills, 'end_inv_ca' => $end_inv_ca, 'ps_unity_sales' => $ps_unity_sales]
                );
                $msg = 'Record has been added!';
            }

            if ($action == "update") {

                $results = DB::update("UPDATE postage set ck_no_1 ='" . Input::get('ck_no_1') . "', ck_no_2 ='" . Input::get('ck_no_2') . "', begin_inv ='" . $begin_inv . "', purchases = '" . $purchases . "', end_inv_safe ='" . $end_inv_safe . "', end_inv_tills ='" . $end_inv_tills . "', end_inv_ca ='" . $end_inv_ca . "', ps_unity_sales = '" . $ps_unity_sales . "' WHERE postage.ps_date = '" . $ps_date . "'");


                $msg = 'Record has been updated!';
            }

            Session::flash('alert-success', $msg);
        }

        return Redirect::route('mktmgr-bookpostageadd');
    }

    public function getbookpostagePrnt() {
        return View::make('mktmgr.bookpostageprnt');
    }

     public function getbooklotteryAdd() {
	
	
	        $work_data = new StdClass();
	        
        	$d=strtotime("last Saturday"); 
            $work_data->dateofinfo= date("Y-m-d", $d);

        

        return View::make('mktmgr.booklotteryadd')->with('data', $work_data);
	
	
    }

    public function postbooklotteryAddData() {
	
	
	$work_data = new StdClass();
        
        
          Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
      
        
        $validator = Validator::make(Input::all(),
            array(                   
                   'game_no'=>'required',
				   'dateofinfo'=>'required|date|weekenddatecheck',
                ),array('game_no.required'=>'Game number is required','dateofinfo.required'=>'Date is required'));
        
        

        //print_r($validator);

        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-booklotteryadd')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
		
		if (Input::has('dateofinfo')) {
                $date_stamp = Input::get('dateofinfo');
                //$date_stamp = '1989-11-16';
                $dt = new DateTime($date_stamp);
                // $datecheck = new DateTime('1990-01-01');
                // if ($dt < $datecheck) {
                //     $dt = new DateTime("yesterday");
                //     $date_stamp = $dt->format('Y-m-d');
                // }
                $this->date_stamp = date('Y-m-d',strtotime($date_stamp)); 
                //echo $this->date_stamp;exit;
                $file_locked = $this->file_lock($date_stamp);

                if($file_locked)
	        	{
	        	$work_data->file_locked = "Y";
	        	}
	        	else
	        	{
	            $work_data->file_locked = "N";
	        	}

               $this->game_no = Input::get('game_no');


                $work_data->lottery_date = date('Y-m-d',strtotime($date_stamp)); 
				


				//===init_rec
				$work_data->game_no=$this->game_no;
				$work_data->ticket_value=0;
				$work_data->begin_inv   =0;
				$work_data->deliveries =0;
				$work_data->returns    =0;
				$work_data->end_inv   =0;
				$work_data->calc_sales=0;
				$work_data->sales_unity=0;
				$work_data->sales_diff  =0;
				
	  
				
                                
              // $work_data->bi_di_date="12/31/1899";
               //echo  $work_data->last_inventory_date;exit()	;
               $work_data->bi_di_date=$work_data->last_inventory_date=$date_stamp;
                
                
         $last_inventory_row = DB::select(DB::raw("SELECT * FROM lottery WHERE lottery.lottery_date < ? AND lottery.game_no = ? ORDER BY lottery.lottery_date DESC "), array($this->date_stamp,$this->game_no));
                 if (!empty($last_inventory_row)) {
                        if (isset($last_inventory_row[0])) {
                            $last_row_data = $last_inventory_row[0];
                            $work_data->last_inventory_date=$last_row_data->lottery_date; 
                            $work_data->begin_inv = $last_row_data->end_inv; 
                            
                        }
                 }
                                
                       // $work_data->last_inventory_date;
                     $work_data->last_inventory_date = date('m-d-Y', strtotime($work_data->last_inventory_date)); 
                    // echo $work_data->last_inventory_date;exit;             
                                
				
				$results = DB::select(DB::raw("SELECT * FROM lottery WHERE lottery.lottery_date = ? AND lottery.game_no = ?"),array($this->date_stamp,$this->game_no));
                
               
				if(!empty($results)){
					if(isset($results[0])){
						$row_data=$results[0];
						$work_data->game_no = $row_data->game_no;
						$work_data->ticket_value = $row_data->ticket_value;
						//$work_data->begin_inv = $row_data->begin_inv;
						$work_data->deliveries = $row_data->deliveries;
						$work_data->returns = $row_data->returns;
						$work_data->end_inv = $row_data->end_inv;
						$work_data->sales_unity = $row_data->sales_unity;
					}
				}
				
				//===init_formrec
				$work_data->ticket_value = ($work_data->ticket_value / 100);            
				$work_data->begin_inv = ($work_data->begin_inv / 100);
				$work_data->deliveries = ($work_data->deliveries / 100);
				$work_data->returns = ($work_data->returns / 100);
				$work_data->end_inv = ($work_data->end_inv / 100);
				$work_data->sales_unity = ($work_data->sales_unity / 100);
				
				
				 $work_data->calc_sales = (($work_data->begin_inv + $work_data->deliveries - $work_data->returns - $work_data->end_inv)  ); 
				
                                $work_data->ticket_value = number_format($work_data->ticket_value, 2, '.', '');
				$work_data->begin_inv = number_format($work_data->begin_inv, 2, '.', '');
				$work_data->deliveries = number_format($work_data->deliveries, 2, '.', '');  
				$work_data->returns = number_format($work_data->returns, 2, '.', '');
				$work_data->end_inv = number_format($work_data->end_inv, 2, '.', '');
				$work_data->sales_unity = number_format($work_data->sales_unity, 2, '.', '');
				
				$work_data->calc_sales = number_format($work_data->calc_sales, 2, '.', '');
				
				
				}
        return View::make('mktmgr.booklotteryadddata')->with('data', $work_data);
		}
    }

    public function postbooklotteryFormData() {
	
	
       if (Input::has('lottery_date') && Input::has('accept-submit')) {
           
            $date_stamp = Input::get('lottery_date');
            //$date_stamp = '1989-11-16';
            $dt = new DateTime($date_stamp);
            // $datecheck = new DateTime('1990-01-01');
            // if ($dt < $datecheck) {
            //     $dt = new DateTime("yesterday");
            //     $date_stamp = $dt->format('Y-m-d');
            // }
            $this->date_stamp = date('Y-m-d',strtotime($date_stamp)); 
            $game_no = $this->game_no = Input::get('game_no');

            $results = DB::select(DB::raw("SELECT * FROM lottery WHERE lottery.lottery_date = ? AND lottery.game_no = ?"), array($this->date_stamp,$this->game_no));
            //echo '<pre>',print_r($results);exit;
          //  echo mysql_result($results, 0);exit;
            $action = "insert";
            if (!empty($results)) {
                if (isset($results[0])) {
                    $action = "update";
                    //echo $action;exit;
                }
             }
             
             $ticket_value= Input::get('ticket_value')*100;
             $begin_inv = Input::get('begin_inv')*100;
             $deliveries = Input::get('deliveries')*100;
             $returns = Input::get('returns')*100;
             $end_inv = Input::get('end_inv')*100;
			 
			 $lottery_date=$this->date_stamp;
             
             
             if($action=="insert"){
                 DB::table('lottery')->insert(
                    ['lottery_date' => $lottery_date, 'game_no' => $game_no, 'ticket_value' => $ticket_value, 'begin_inv' => $begin_inv, 
					'deliveries' => $deliveries, 'returns' => $returns, 'end_inv' => $end_inv ]
                );
                $msg = 'Record has been added!';
            }

            if ($action == "update") {

                $results = DB::update("UPDATE lottery set ticket_value ='" . $ticket_value . "',  begin_inv ='" . $begin_inv . "', deliveries = '" . $deliveries . "', returns ='" . $returns . "', end_inv ='" . $end_inv . "' WHERE lottery.lottery_date = '" . $lottery_date . "' AND lottery.game_no = '".$game_no."'");


                $msg = 'Record has been updated!';
            }

            Session::flash('alert-success', $msg);
        }
	
        return Redirect::route('mktmgr-booklotteryadd');
    }

   

    public function getbooklotteryTotl() {
        
        $work_data = new StdClass();
        	$d=strtotime("last Saturday"); 
            $work_data->dateofinfo= date("Y-m-d", $d);

        

        return View::make('mktmgr.booklotterytotl')->with('data', $work_data);
        
    }

    public function postbooklotteryTotalData() {
        
        $work_data = new StdClass();
        
        
         
        
        $validator = Validator::make(Input::all(),
            array(                   
                 
				   'dateofinfo'=>'required|date',
                ),array('dateofinfo.required'=>'Date is required'));
        
        

        //print_r($validator);

        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-booklotterytotl')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
		
		if (Input::has('dateofinfo')) {
                $date_stamp = Input::get('dateofinfo');
                //$date_stamp = '1989-11-16';
                $dt = new DateTime($date_stamp);
                // $datecheck = new DateTime('1990-01-01');
                // if ($dt < $datecheck) {
                //     $dt = new DateTime("yesterday");
                //     $date_stamp = $dt->format('Y-m-d');
                // }
                $this->date_stamp = date('Y-m-d',strtotime($date_stamp)); 

                $file_locked = $this->file_lock($date_stamp);
	        	
	        	if($file_locked)
	        	{
	        		$work_data->file_locked = "Y";
	        	}
	        	else
	        	{
	        		$work_data->file_locked = "N";
	        	}

              	


                $work_data->lottery_date = date('Y-m-d',strtotime($date_stamp)); 
				


				//===init_rec
				/*$work_data->game_no=0;
				$work_data->ticket_value=0;
				$work_data->begin_inv   =0;
				$work_data->deliveries =0;
				$work_data->returns    =0;
				$work_data->end_inv   =0;*/
				$work_data->calc_sales=0;
				$work_data->sales_unity=0;
				$work_data->sales_diff  =0;
				
	  
				//$work_data->checkdate = $work_data->lottery_date - 7 UNITS DAY;
				
				$sales_unity_val = DB::select(DB::raw("SELECT sales_unity FROM lottery WHERE lottery.lottery_date = ? AND lottery.game_no = 'ZZZZ'"),array($this->date_stamp));
                
               $results = DB::select(DB::raw('SELECT SUM((lottery.begin_inv + lottery.deliveries - lottery.returns - lottery.end_inv)/100) as calc_sales, sales_unity  FROM lottery WHERE lottery.lottery_date = ?'),
                 array($this->date_stamp));
				
					   if(!empty($results)){
						if(isset($results[0])){
							$row_data=$results[0];
							$work_data->calc_sales = $row_data->calc_sales;	
						}
					}
					 if(!empty($sales_unity_val)){
						if(isset($sales_unity_val[0])){
							$row_data=$sales_unity_val[0];
							$work_data->sales_unity = $row_data->sales_unity;	
						}
					}
				
				//===init_formrec
				//$work_data->ticket_value = ($work_data->ticket_value / 100);            
				/*$work_data->begin_inv = ($work_data->begin_inv / 100);
				$work_data->deliveries = ($work_data->deliveries / 100);
				$work_data->returns = ($work_data->returns / 100);
				$work_data->end_inv = ($work_data->end_inv / 100);
				$work_data->sales_unity = ($work_data->sales_unity / 100);
				
				
				$work_data->begin_inv = number_format($work_data->begin_inv, 2, '.', '');
				$work_data->deliveries = number_format($work_data->deliveries, 2, '.', '');  
				$work_data->returns = number_format($work_data->returns, 2, '.', '');
				$work_data->end_inv = number_format($work_data->end_inv, 2, '.', '');
				$work_data->sales_unity = number_format($work_data->sales_unity, 2, '.', '');
				
            
				$work_data->calc_sales = (($work_data->begin_inv + $work_data->deliveries - $work_data->returns - $work_data->end_inv)  / 100);*/
				
				
				$work_data->calc_sales = number_format($work_data->calc_sales, 2, '.', '');
                
	            $work_data->sales_diff = (($work_data->calc_sales * 100) - $work_data->sales_unity ); 
	             
	             $work_data->sales_diff = $work_data->sales_diff / 100;
                                
               /* $work_data->sales_diff = number_format($work_data->sales_diff, 2, '.', '');*/
				
				
				}
        return View::make('mktmgr.booklotterytotaldata')->with('data', $work_data);
		}
        
    }

    public function postbooklotteryTotalFormData() {
         if (Input::has('lottery_date') && Input::has('accept-submit')) {
           
            $date_stamp = Input::get('lottery_date');
            //$date_stamp = '1989-11-16';
            $dt = new DateTime($date_stamp);
            // $datecheck = new DateTime('1990-01-01');
            // if ($dt < $datecheck) {
            //     $dt = new DateTime("yesterday");
            //     $date_stamp = $dt->format('Y-m-d');
            // }
            $this->date_stamp = date('Y-m-d',strtotime($date_stamp)); 
           

            $results = DB::select(DB::raw("SELECT * FROM lottery WHERE lottery.lottery_date = ?  AND lottery.game_no = 'ZZZZ' "), array($this->date_stamp));

            $action = "insert";
            if (!empty($results)) {
                if (isset($results[0])) {
                    $action = "update";
                }
             }
             
             
			 $sales_unity=  Input::get('sales_unity')*100;

			 $lottery_date=$this->date_stamp;
                         $game_no = 'ZZZZ';
             
             if($action=="insert"){
                 DB::table('lottery')->insert(
                    ['lottery_date' => $lottery_date,  'game_no'=>$game_no,
					'sales_unity' => $sales_unity]
                );
                $msg = 'Record has been added!';
            }

            if ($action == "update") {

                $results = DB::update("UPDATE lottery set  sales_unity ='" . $sales_unity . "' WHERE lottery.lottery_date = '" . $lottery_date . "'  AND lottery.game_no = 'ZZZZ' ");


                $msg = 'Record has been updated!';
            }

            Session::flash('alert-success', $msg);
        }
	
       
        return Redirect::route('mktmgr-booklotterytotl');
    }

    public function getbooklotteryPrnt() {
        return View::make('mktmgr.booklotteryprnt');
    }

    public function getbooklotteryView() {
         $work_data = new StdClass();
        
        $work_data->dateofinfo=date('m/d/Y', strtotime("last Saturday"));

        

        return View::make('mktmgr.booklotteryview')->with('data', $work_data);
        
    }

    public function postbooklotteryViewData() 
    {
        $work_data = new StdClass();
        Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
        $validator = Validator::make(Input::all(),
            array( 
				   'dateofinfo'=>'required|date|weekenddatecheck',
                ));
        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-booklotterytotl')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
			$end_date = Input::get('dateofinfo'); 
            $ticket_value = 0;
            $tot_begin_inv = 0;
            $tot_end_inv = 0;
            $tot_deliveries = 0;
            $tot_returns = 0;
            $tot_calc_sales = 0;
            $unity = 0;
            $cnt = 1;
            $lot_array=array();
            $results_cnt = 0;
            $cnt = 1;           
            $lot_ar = DB::select(DB::raw("SELECT lottery.game_no as game_no,lottery.ticket_value/100 as ticket_value,lottery.begin_inv/100 as begin_inv,lottery.deliveries/100 as deliveries,lottery.returns/100 as returns,lottery.end_inv/100 as end_inv, lottery.sales_unity/100 as sales_unity FROM  lottery WHERE  lottery.lottery_date = ? ORDER BY lottery.game_no"), array($end_date));
                $lot_ar_array = json_decode(json_encode($lot_ar), true);
                //echo '<pre>';print_r($lot_array);exit;
                $results_cnt = count($lot_array);
                //echo $results_cnt;exit;
                foreach($lot_ar as $row)
                {
                    $lot_array[$cnt]['game_no'] = $row->game_no;
                    $lot_array[$cnt]['ticket_value'] = $row->ticket_value;
                    $ticket_value = $lot_array[$cnt]['ticket_value'];
                    $lot_array[$cnt]['begin_inv'] = $row->begin_inv;
                    $lot_array[$cnt]['deliveries'] = $row->deliveries;
                    $lot_array[$cnt]['returns'] = $row->returns;
                    $lot_array[$cnt]['end_inv'] = $row->end_inv;
                    $lot_array[$cnt]['sales_unity'] = $row->sales_unity;
                    if($lot_array[$cnt]['game_no'] == "ZZZZ")
                    {
                        $unity = $lot_array[$cnt]['sales_unity'];
                    }
                    
                    $lot_array[$cnt]['calc_sales'] = ($lot_array[$cnt]['begin_inv'] + $lot_array[$cnt]['deliveries'] -
                    $lot_array[$cnt]['returns'] - $lot_array[$cnt]['end_inv']);

                    $tot_begin_inv = $tot_begin_inv + $lot_array[$cnt]['begin_inv'];
                    $tot_end_inv = $tot_end_inv + $lot_array[$cnt]['end_inv'];
                    $tot_deliveries = $tot_deliveries + $lot_array[$cnt]['deliveries'];
                    $tot_returns = $tot_returns + $lot_array[$cnt]['returns'];
                    $tot_calc_sales = $tot_calc_sales + $lot_array[$cnt]['calc_sales'];
                    $lot_array[$cnt]['sales_diff'] = '';
                    $cnt = $cnt + 1;
                }
                //echo $tot_calc_sales.' '.$unity.'<br>';exit;
                $lot_array[$cnt]['game_no'] = "TOTAL";
                $lot_array[$cnt]['begin_inv'] = $tot_begin_inv;
                $lot_array[$cnt]['ticket_value'] = $ticket_value;
                $lot_array[$cnt]['end_inv'] = $tot_end_inv;
                $lot_array[$cnt]['deliveries'] = $tot_deliveries;
                $lot_array[$cnt]['returns'] = $tot_returns;
                $lot_array[$cnt]['calc_sales'] = $tot_calc_sales;
                $lot_array[$cnt]['sales_unity'] = $unity;
                $lot_array[$cnt]['sales_diff'] = $tot_calc_sales - $unity;
                //echo '<pre>';print_r($lot_array);exit;
                return View::make('mktmgr.booklotteryviewdata',compact('lot_array','end_date'));
        }
	}
    
    
    
    
      public function getrecapitemsdesc() {

        $invdescitemlist = array();
        
        $inv_type = Input::get('inv_type');


       $code=$this->get_itemcode_inv_type($inv_type);

        if ($code != '') {
           
            
        
        $sql = "SELECT recapitems.item_id, recapitems.item_desc FROM recapitems WHERE recapitems.disable = 'N' and recapitems.item_type = '".$code."' ";

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {

                foreach ($results as $key) {
                    $invdescitemlist[] = [ 'id' => $key->item_id, 'value' => $key->item_desc];
                }
            }
        }

        return Response::json($invdescitemlist);
         
        }
    }
    
    function get_recapitems_desc($id){

        $sql = "SELECT recapitems.item_desc FROM recapitems WHERE recapitems.item_id = '".$id."' ";

        $results = DB::select(DB::raw($sql));
        
       

        if (!empty($results)) {
            if (isset($results[0])) {
                $row=$results[0];
                return $row->item_desc;
            }
        }
        return '';
   
}

    function get_itemcode_inv_type($inv_type = '') {
        $c = '';
        switch ($inv_type) {
            case "EP":
                $c = "B";
                break;
            case "BT":
                $c = "B";
                break;
            case "KE":
                $c = "B";
                break;
            case "Y1":
                $c = "B";
                break;
            case "U1":
                $c = "B";
                break;
            case "FD":
                $c = "9";
                break;
            case "AQ":
                $c = "E";
                break;
            case "WH":
                $c = "W";
                break;
            case "CL":
                $c = "L";
                break;
            case "DY":
                $c = "D";
                break;
            case "YR":
                $c = "Y";
                break;
            case "AT":
                $c = "R";
                break;
            case "YB":
                $c = "Z";
                break;
            case "SN":
                $c = "Q";
                break;
            case "SP":
                $c = "G";
                break;
            case "FR":
                $c = "F";
                break;
            case "R5":
                $c = "H";
                break;
            default:
        }


        return $c;
    }

      

    public function getbookcommuteradd() {
        //disp_items

        $work_data = new StdClass();
        
        $work_data->dateofinfo=date('m/d/Y', strtotime("last Saturday"));

        $invtypeitemlist = array();
        $sql = "SELECT recapitems.item_id, recapitems.item_desc FROM recapitems WHERE recapitems.disable = 'N' and recapitems.item_type = 'C' ";

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {

                foreach ($results as $key) {
                    $invtypeitemlist[] = [ 'id' => $key->item_id, 'value' => $key->item_desc];
                }
            }
        }

        $work_data->invtypeitemlist = $invtypeitemlist;
        
        


        return View::make('mktmgr.bookcommuteradd')->with('data', $work_data);
    }



    public function getbookcommuterShowdata() {
        $work_data = new StdClass();
        
        
          Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
        Validator::extend('valid_type', function($attribute, $value, $parameters) {           
            
            $results = DB::select(DB::raw("SELECT * FROM recapitems WHERE recapitems.item_type = 'C' AND recapitems.item_id = ?"), array($value));

            if (!empty($results)) {
                if (isset($results[0])) {
                    return true;
                }
            }

            return false;
        
        },'NOT A VALID INVENTORY ID...');
        
        Validator::extend('valid_desc', function($attribute, $value, $parameters) {           
            
            $inv_type= Input::get('inv_type');
            
            $results = DB::select(DB::raw("SELECT recapitems.item_type FROM recapitems WHERE recapitems.item_id = ?"), array($value));

            if (!empty($results)) {
                if (isset($results[0])) {
                    $row_data = $results[0];
                    $item_type = $row_data->item_type;
                    if ($this->get_itemcode_inv_type($inv_type) == $item_type) {
                        return true;
                    }
                }
            }

            return false;
        
        
        },'NOT A VALID INVENTORY DESC ID...');
        
        $validator = Validator::make(Input::all(),
            array(
                   'inv_type'=>'required|valid_type',
                   'desc'=>'required|valid_desc',
                   'dateofinfo'=>'required|date|weekenddatecheck',
                ),array('inv_type.required'=>'Inventory Type is required','desc.required'=>'Description is required','dateofinfo.required'=>'Date is required'));
        
        

        //print_r($validator);

        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-bookcommuteradd')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {

                if (Input::has('dateofinfo')) {
                    $date_stamp = Input::get('dateofinfo');
                     $stamp_date = Input::get('dateofinfo');
                    //$date_stamp = '1989-11-16';
                    $dt = new DateTime($date_stamp);
                    // $datecheck = new DateTime('1990-01-01');
                    // if ($dt < $datecheck) {
                    //     $dt = new DateTime("yesterday");
                    //     $date_stamp = $dt->format('Y-m-d');
                    // }
                    $this->date_stamp = date('Y-m-d',strtotime($date_stamp));
                    $stamp_date = date('Y-m-d',strtotime($stamp_date)); 
                    			
                    //==put validation for weekending date only Saturday
                    //date('w', strtotime($date));
               $file_locked = $this->file_lock($stamp_date);
	        	
	        	if($file_locked)
	        	{
	        		$file_locked = "Y";
	        	}
	        	else
	        	{
	        		$file_locked = "N";
	        	}
	        	//echo $file_locked;exit;
	        	
                    //echo $date_stamp; exit;
                    $work_data->dateofinfo = $date_stamp;
                    $this->inv_type = Input::get('inv_type');
                    $this->desc = Input::get('desc');


                    $work_data->int_flag = false;
                    $work_data->price = 0;
                    $work_data->trf_no_1 = 0;
                    $work_data->trf_no_2 = 0;
                    $work_data->deliveries = 0;
                    $work_data->returns = 0;
                    $work_data->end_inv = 0;
                    $work_data->begin_inv = 0;
                    $work_data->desc = '';
                    $work_data->inv_type = '';
                    $work_data->desc_desc = '';
                    $work_data->inv_desc = '';


                    
                    $last_inventory_row = DB::select(DB::raw("SELECT commuter.comm_date, commuter.end_inv,commuter.begin_inv FROM commuter WHERE commuter.inv_type = ? AND commuter.desc = ?  ORDER BY commuter.comm_date DESC"), array($this->inv_type, $this->desc));

                 if (!empty($last_inventory_row)) {
                        if (isset($last_inventory_row[0])) {
                            $last_row_data = $last_inventory_row[0];
                            
                            $work_data->begin_inv = $last_row_data->begin_inv; 
                            
                        }
                 }
                 

                    $results = DB::select(DB::raw("SELECT * FROM commuter WHERE commuter.comm_date = ? AND commuter.inv_type = ? AND commuter.desc = ?"), array($this->date_stamp, $this->inv_type, $this->desc));
                    
                    
                    
                    
                   // echo '<pre>',print_r($results,true);exit;

                    if (!empty($results)) {
                        if (isset($results[0])) {
                            $row_data = $results[0];
                            $work_data->price = $row_data->price;
                            $work_data->trf_no_1 = $row_data->trf_no_1;
                            $work_data->trf_no_2 = $row_data->trf_no_2;
                            $work_data->deliveries = $row_data->deliveries;
                            $work_data->returns = $row_data->returns;
                            $work_data->end_inv = $row_data->end_inv;
                            //$work_data->begin_inv = $row_data->begin_inv;
                            $work_data->desc = $row_data->desc;
                            $work_data->inv_type = $row_data->inv_type;
                        }
                    }
                    
                    
                    if($work_data->desc=='' || $work_data->inv_type==''){
                        
                        $work_data->inv_type=$this->inv_type;
                        $work_data->desc=$this->desc;
                        
                    }

                    	//echo '<pre>',print_r($work_data);exit;

                    $work_data->price = (($work_data->price) / 100);
                    /*$work_data->deliveries_amt = (($work_data->deliveries * $work_data->price) / 100);
                    $work_data->returns_amt = (($work_data->returns * $work_data->price) / 100);
                    $work_data->end_inv_amt = (($work_data->end_inv * $work_data->price) / 100);
                    $work_data->begin_inv_amt = (($work_data->begin_inv * $work_data->price) / 100);*/
                    
                    
                    //echo '<pre>',print_r($work_data);exit;
                    //file_locked()


                    $work_data->deliveries_amt = (($work_data->deliveries * $work_data->price) );
                    $work_data->returns_amt = (($work_data->returns * $work_data->price) );
                    $work_data->end_inv_amt = (($work_data->end_inv * $work_data->price) );
                    $work_data->begin_inv_amt = (($work_data->begin_inv * $work_data->price) );

                    $work_data->sales_amt = ($work_data->begin_inv_amt + $work_data->deliveries_amt - $work_data->returns_amt - $work_data->end_inv_amt);

                    if ($work_data->desc != '') {
                        $work_data->desc_desc = $this->get_recapitems_desc($work_data->desc);
                    } 
                    if ($work_data->inv_type != '') {
                        $work_data->inv_desc = $this->get_recapitems_desc($work_data->inv_type);
                    } 
                    
                    
                    
                    $work_data->price = number_format($work_data->price, 2, '.', '');
                    $work_data->deliveries_amt = number_format($work_data->deliveries_amt, 2, '.', '');
                    $work_data->returns_amt = number_format($work_data->returns_amt, 2, '.', '');
                    $work_data->end_inv_amt = number_format($work_data->end_inv_amt, 2, '.', '');
                    $work_data->begin_inv_amt = number_format($work_data->begin_inv_amt, 2, '.', '');
                    $work_data->sales_amt = number_format($work_data->sales_amt, 2, '.', '');
                    
                    
                    
                    
                }
                return View::make('mktmgr.bookcommutershowdata',compact('work_data','file_locked'));
               /* return View::make('mktmgr.bookcommutershowdata')->with('data', $work_data);*/
        }
    }

    	//code for file locked
    public function file_lock($stamp_date)
	{  
		
		$file_locked=DB::select('SELECT count(date_stamp) as stamp from locks  WHERE locks.date_stamp = "'.$stamp_date.'" ');
		
		return ($file_locked[0]->stamp == 1) ? true :false;

	}	



    public function getbookcommuterPostdata() {
       

        if (Input::has('comm_date') && Input::has('accept-submit')) {
            
          
            $date_stamp = Input::get('comm_date');
            //$date_stamp = '1989-11-16';
            $dt = new DateTime($date_stamp);
            // $datecheck = new DateTime('1990-01-01');
            // if ($dt < $datecheck) {
            //     $dt = new DateTime("yesterday");
            //     $date_stamp = $dt->format('Y-m-d');
            // }
            
            $this->date_stamp = date('Y-m-d',strtotime($date_stamp)); 
            
            
            $this->inv_type = Input::get('inv_type');
            $this->desc = Input::get('desc');

            
           

            $results = DB::select(DB::raw("SELECT * FROM commuter WHERE commuter.comm_date = ? AND commuter.inv_type = ? AND commuter.desc = ?"), array($this->date_stamp,$this->inv_type,$this->desc));
            //print_r($results); die;
            
            $action = "insert";
            if (!empty($results)) {
                if (isset($results[0])) {
                    $action = "update";
                }
            }

            $price = Input::get('price') * 100;
            $trf_no_1 = Input::get('trf_no_1');
            $trf_no_2 = Input::get('trf_no_2');
            $begin_inv = Input::get('begin_inv');
            
            $deliveries = Input::get('deliveries');
            $returns = Input::get('returns');
            $end_inv = Input::get('end_inv');
           

            


            if ($action == "insert") {
                DB::table('commuter')->insert(
                        ['comm_date' => $this->date_stamp,'price'=>$price,'inv_type'=>$this->inv_type,'desc'=>$this->desc, 'trf_no_1' => $trf_no_1, 'trf_no_2' => $trf_no_2, 'begin_inv' => $begin_inv, 'deliveries' => $deliveries, 'returns' => $returns, 'end_inv' => $end_inv]
                );
                $msg = 'Record has been added!';
            }

            if ($action == "update") {

                $results = DB::update("UPDATE commuter set price='".$price."',trf_no_1 ='" . $trf_no_1 . "', trf_no_2 ='" . $trf_no_2 . "', begin_inv ='" . $begin_inv . "', deliveries = '" . $deliveries . "', returns ='" . $returns . "', end_inv ='" . $end_inv . "' WHERE commuter.comm_date = '".$this->date_stamp."' AND commuter.inv_type = '".$this->inv_type."' AND commuter.desc = '".$this->desc."' ");


                $msg = 'Record has been updated!';
            }
            
            
            Session::flash('alert-success', $msg);
        }



        return Redirect::route('mktmgr-bookcommuteradd');
    }

   

    
    public function getrecapitemsdescmonthly() {

        $invdescitemlist = array();

        $inv_type = Input::get('inv_type');


        $code = $this->get_itemcode_inv_type_monthly($inv_type);

        if ($code != '') {



            $sql = "SELECT recapitems.item_id, recapitems.item_desc FROM recapitems WHERE recapitems.disable = 'N' and recapitems.item_type = '" . $code . "' ";

            $results = DB::select(DB::raw($sql));

            if (!empty($results)) {
                if (isset($results[0])) {

                    foreach ($results as $key) {
                        $invdescitemlist[] = [ 'id' => $key->item_id, 'value' => $key->item_desc];
                    }
                }
            }

            return Response::json($invdescitemlist);
        }
    }

    function get_itemcode_inv_type_monthly($inv_type = '') {
        $c = '';
        switch ($inv_type) {

            case "BT":
                $c = "B";
                break;

            case "Y1":
                $c = "B";
                break;

            case "FD":
                $c = "9";
                break;
            case "AS":
                $c = "N";
                break;
            case "AQ":
                $c = "E";
                break;
            case "WH":
                $c = "W";
                break;
            case "CL":
                $c = "L";
                break;
            case "DY":
                $c = "D";
                break;
            case "YR":
                $c = "Y";
                break;
            case "AT":
                $c = "R";
                break;
            case "YB":
                $c = "Z";
                break;
            case "SN":
                $c = "Q";
                break;
            case "SP":
                $c = "G";
                break;
            case "FR":
                $c = "F";
                break;
            case "R5":
                $c = "H";
                break;
            default:
        }


        return $c;
    }

     
	 public function postBookmngmntWeekly()
	 {
        $work_data = new StdClass();
        Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
            $weekday = date('N', strtotime($value));      
            if($weekday==6){
                return true;
            }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
        
        $validator = Validator::make(Input::all(),
        array(
           'dateofinfo'=>'required|date|weekenddatecheck',
        ),array('dateofinfo.required'=>'Date is required'));
        
        if($validator->fails())
		{
			return Redirect::route('mktmgr-bookmngmntweekly')
					->withErrors($validator)
					->withInput();
		}
        $this->wk_recap_date = Input::get('dateofinfo');
        $this->wk_recap_date = UserHomeController::convertDateToYMD($this->wk_recap_date);
        UserHomeController::wk_init_rec();
        
        if($this->errMsg != '') {
            Session::flash('alert-danger', $this->errMsg);
            return View::make('mktmgr.bookmngmntweekly');
        }
        
        /*if($this->wk_recap_date != null && $this->wk_recap_date != '') {
            UserHomeController::wk_approve($this->wk_recap_date);
        }*/
        $arr = UserHomeController::wk_get_ov($this->wk_recap_date);
        $this->wk_drgov = $arr['drgov'];
        $this->wk_grov = $arr['grov'];
        
        $date_stamp = $this->wk_recap_date;
        $wk_summary =$this->wk_summary;
        $wk_dept_sales =$this->wk_dept_sales;
        $wk_net_safe =$this->wk_net_safe;
        $wk_deposits =$this->wk_deposits;
        $wk_del_tranfs =$this->wk_del_tranfs;
        $wk_store_tx =$this->wk_store_tx;
        $wk_po_types =$this->wk_po_types;
        $wk_drgov =$this->wk_drgov;
        $wk_grov =$this->wk_grov;

        return View::make('mktmgr.bookmngmntweeklypostview', compact('wk_summary','wk_dept_sales','wk_net_safe','wk_deposits','wk_del_tranfs','wk_store_tx','wk_po_types','wk_drgov','wk_grov','date_stamp'));
	 }
	 public function postBookmngmntWeeklyShowDetails()
	 {		
        $date_stamp = Input::get('date_stamp');
        $date_stamp=date("m/d/Y", strtotime($date_stamp));
        $title = Input::get('t');
        $totSum=0;
        
        if($title == 'DEPARTMENT SALES') {
            $q = 'select recapitems.item_id, recapitems.item_desc, recapmain.dept_sales as item_amt from recapitems, recapmain where recapitems.item_type IN ("M","J", "A") and recapitems.item_id = recapmain.dept_id and recapmain.wk_end_date = ? and recapmain.dept_sales <> 0 order by recapitems.item_id';
        }
        else if($title == 'NET SAFE') {
        	$title = 'DEPARTMENT SALES';
            $q = 'select recapitems.item_id, recapitems.item_desc, recapmain.dept_sales as item_amt from recapitems, recapmain where recapitems.item_type = "I" and recapitems.item_id = recapmain.dept_id and recapmain.wk_end_date = ? and recapmain.dept_sales <> 0 order by recapitems.item_id';
        }
        else if($title == 'DEPOSIT DETAIL') {

            UserHomeController::wk_dep_rpt($date_stamp);
            
            $wk_maindep_total = $this->wk_maindep_total;
            $wk_maindep_currency = $this->wk_maindep_currency;
            $wk_maindep_checks = $this->wk_maindep_checks;
            $wk_maindep_express_cred = $this->wk_maindep_express_cred;
            $wk_maindep_express_deb = $this->wk_maindep_express_deb;
            
            return View::make('mktmgr.bookmngmntweeklydepositpostview', compact('date_stamp','wk_maindep_total','wk_maindep_currency','wk_maindep_checks','wk_maindep_express_cred','wk_maindep_express_deb'));
        }
        else if($title == 'DELIVERY AND TRANSFERS') {
            UserHomeController::wk_dltrf_rpt($date_stamp);
            
            $wk_maintrf_total = $this->wk_maintrf_total;
            $wk_maintrf_tranfs = $this->wk_maintrf_tranfs;
            $wk_maintrf_del = $this->wk_maintrf_del;
            
            return View::make('mktmgr.bookmngmntweeklytrfpostview', compact('date_stamp','wk_maintrf_total','wk_maintrf_tranfs','wk_maintrf_del'));
        }
        else if($title == 'STORE TRANSACTIONS') {
            $q = 'select safeitems.item_id, safeitems.item_desc, recap.item_amt from safeitems, recap where safeitems.item_type = "S" and safeitems.item_id NOT IN ("RR","LR","TD") and safeitems.item_id = recap.item_id and recap.wk_end_date = ? and recap.item_amt <> 0 order by safeitems.item_id';
        }
        else if($title == 'PAID OUT TYPES') {
            $q = 'select safeitems.item_id, safeitems.item_desc, recap.item_amt from safeitems, recap where safeitems.item_type = "O" and safeitems.item_id = recap.item_id and recap.wk_end_date = ? and recap.item_amt <> 0 order by safeitems.item_id';
        }
        else if($title == 'CURRENCY DEPOSITS') {
            $this->wk_recap_date = $date_stamp;
            $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($date_stamp)));
            $total = 0;
            
            $q_curr = DB::select('SELECT recap.item_id,
                (recap.item_amt / 100) as amt,
                recap.wk_end_date as recap_date
                FROM recap
                WHERE recap.item_id IN  ("C1","C2", "C3", "C4")
                AND recap.wk_end_date >= ?
                AND recap.wk_end_date <= ?
                ORDER BY wk_end_date, item_id', array($this->wk_begin_date,$this->wk_recap_date));
            
            $result = array();
            for($i=0,$cnt=0; $i<count($q_curr); $i++) {
                if($q_curr[$i]->amt == null || $q_curr[$i]->amt == '') {
                    $q_curr[$i]->amt = 0;
                }
                if($q_curr[$i]->amt != 0) {
                    $result[$i] = new stdClass();
                    $result[$i]->desc = UserHomeController::get_itemdesc($q_curr[$i]->item_id);
                    $result[$i]->wkday = UserHomeController::get_weekday($q_curr[$i]->recap_date);
                    $result[$i]->item_id = $q_curr[$i]->item_id;
                    $result[$i]->recap_date = $q_curr[$i]->recap_date;
                    $result[$i]->amt = $q_curr[$i]->amt;
                    $total = $total + $q_curr[$i]->amt;
                    $cnt ++;
                }
                if($cnt > 15) break;
            }
            $title = "CURRENCY";
            return View::make('mktmgr.bookmngmntweeklydepositshowdetails', compact('result', 'title', 'date_stamp','total'));
        }
        else if($title == 'CHECK DEPOSITS') {
            $this->wk_recap_date = $date_stamp;
            $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($date_stamp)));
            $total = 0;
            
            $q_curr = DB::select('SELECT recap.item_id,
                (recap.item_amt / 100) as amt,
                recap.wk_end_date as recap_date
                FROM recap
                WHERE recap.item_id IN  ("K1","K2")
                AND recap.wk_end_date >= ?
                AND recap.wk_end_date <= ?
                ORDER BY wk_end_date, item_id', array($this->wk_begin_date,$this->wk_recap_date));
            
            $result = array();
            for($i=0,$cnt=0; $i<count($q_curr); $i++) {
                if($q_curr[$i]->amt == null || $q_curr[$i]->amt == '') {
                    $q_curr[$i]->amt = 0;
                }
                if($q_curr[$i]->amt != 0) {
                    $result[$i] = new stdClass();
                    $result[$i]->desc = UserHomeController::get_itemdesc($q_curr[$i]->item_id);
                    $result[$i]->wkday = UserHomeController::get_weekday($q_curr[$i]->recap_date);
                    $result[$i]->item_id = $q_curr[$i]->item_id;
                    $result[$i]->recap_date = $q_curr[$i]->recap_date;
                    $result[$i]->amt = $q_curr[$i]->amt;
                    $total = $total + $q_curr[$i]->amt;
                    $cnt ++;
                }
                if($cnt > 15) break;
            }
            $title = "CHECKS";
            return View::make('mktmgr.bookmngmntweeklydepositshowdetails', compact('result', 'title', 'date_stamp','total'));
        }
        else if($title == 'VISA DEPOSITS') {
            $this->wk_recap_date = $date_stamp;
            $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($date_stamp)));
            $total = 0;
            
            $q_curr = DB::select('SELECT recap.item_id,
                (recap.item_amt / 100) as amt,
                recap.wk_end_date as recap_date
                FROM recap
                WHERE recap.item_id IN  ("CD")
                AND recap.wk_end_date >= ?
                AND recap.wk_end_date <= ?
                ORDER BY wk_end_date, item_id', array($this->wk_begin_date,$this->wk_recap_date));
            
            $result = array();
            for($i=0,$cnt=0; $i<count($q_curr); $i++) {
                if($q_curr[$i]->amt == null || $q_curr[$i]->amt == '') {
                    $q_curr[$i]->amt = 0;
                }
                if($q_curr[$i]->amt != 0) {
                    $result[$i] = new stdClass();
                    $result[$i]->desc = UserHomeController::get_itemdesc($q_curr[$i]->item_id);
                    $result[$i]->wkday = UserHomeController::get_weekday($q_curr[$i]->recap_date);
                    $result[$i]->item_id = $q_curr[$i]->item_id;
                    $result[$i]->recap_date = $q_curr[$i]->recap_date;
                    $result[$i]->amt = $q_curr[$i]->amt;
                    $total = $total + $q_curr[$i]->amt;
                    $cnt ++;
                }
                if($cnt > 15) break;
            }
            $title = "EXPRESS CREDIT";
            return View::make('mktmgr.bookmngmntweeklydepositshowdetails', compact('result', 'title', 'date_stamp','total'));
        }
        else if($title == 'EXPRESS DEBIT') {
            $this->wk_recap_date = $date_stamp;
            $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($date_stamp)));
            $total = 0;
            
            $q_curr = DB::select('SELECT recap.item_id,
                (recap.item_amt / 100) as amt,
                recap.wk_end_date as recap_date
                FROM recap
                WHERE recap.item_id IN  ("DD")
                AND recap.wk_end_date >= ?
                AND recap.wk_end_date <= ?
                ORDER BY wk_end_date, item_id', array($this->wk_begin_date,$this->wk_recap_date));
            
            $result = array();
            for($i=0,$cnt=0; $i<count($q_curr); $i++) {
                if($q_curr[$i]->amt == null || $q_curr[$i]->amt == '') {
                    $q_curr[$i]->amt = 0;
                }
                if($q_curr[$i]->amt != 0) {
                    $result[$i] = new stdClass();
                    $result[$i]->desc = UserHomeController::get_itemdesc($q_curr[$i]->item_id);
                    $result[$i]->wkday = UserHomeController::get_weekday($q_curr[$i]->recap_date);
                    $result[$i]->item_id = $q_curr[$i]->item_id;
                    $result[$i]->recap_date = $q_curr[$i]->recap_date;
                    $result[$i]->amt = $q_curr[$i]->amt;
                    $total = $total + $q_curr[$i]->amt;
                    $cnt ++;
                }
                if($cnt > 15) break;
            }
            return View::make('mktmgr.bookmngmntweeklydepositshowdetails', compact('result', 'title', 'date_stamp','total'));
        }
        else if($title == 'TRANSFERS') {
            $this->wk_recap_date = $date_stamp;
            $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($date_stamp)));
            $total = 0;
            
            $q_curr = DB::select('SELECT recap.item_id,
                (recap.item_amt / 100) as amt,
                recap.wk_end_date as recap_date
                FROM recap
                WHERE recap.item_id IN  ("T1","T2","T3","T4","T5","T6","T7")
                AND recap.wk_end_date >= ?
                AND recap.wk_end_date <= ?
                ORDER BY wk_end_date, item_id', array($this->wk_begin_date,$this->wk_recap_date));
            
            $result = array();
            for($i=0,$cnt=0; $i<count($q_curr); $i++) {
                if($q_curr[$i]->amt == null || $q_curr[$i]->amt == '') {
                    $q_curr[$i]->amt = 0;
                }
                if($q_curr[$i]->amt != 0) {
                    $result[$i] = new stdClass();
                    $result[$i]->desc = UserHomeController::get_itemdesc($q_curr[$i]->item_id);
                    $result[$i]->wkday = UserHomeController::get_weekday($q_curr[$i]->recap_date);
                    $result[$i]->item_id = $q_curr[$i]->item_id;
                    $result[$i]->recap_date = $q_curr[$i]->recap_date;
                    $result[$i]->amt = $q_curr[$i]->amt;
                    $total = $total + $q_curr[$i]->amt;
                    $cnt ++;
                }
                if($cnt > 15) break;
            }
            return View::make('mktmgr.bookmngmntweeklydepositshowdetails', compact('result', 'title', 'date_stamp','total'));
        }
        else if($title == 'DELIVERIES') {
            $this->wk_recap_date = $date_stamp;
            $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($date_stamp)));
            $total = 0;
            
            $q_curr = DB::select('SELECT recap.item_id,
                (recap.item_amt / 100) as amt,
                recap.wk_end_date as recap_date
                FROM recap
                WHERE recap.item_id IN  ("D1","D2","D3","D4","D5","D6")
                AND recap.wk_end_date >= ?
                AND recap.wk_end_date <= ?
                ORDER BY wk_end_date, item_id', array($this->wk_begin_date,$this->wk_recap_date));
            
            $result = array();
            for($i=0,$cnt=0; $i<count($q_curr); $i++) {
                if($q_curr[$i]->amt == null || $q_curr[$i]->amt == '') {
                    $q_curr[$i]->amt = 0;
                }
                if($q_curr[$i]->amt != 0) {
                    $result[$i] = new stdClass();
                    $result[$i]->desc = UserHomeController::get_itemdesc($q_curr[$i]->item_id);
                    $result[$i]->wkday = UserHomeController::get_weekday($q_curr[$i]->recap_date);
                    $result[$i]->item_id = $q_curr[$i]->item_id;
                    $result[$i]->recap_date = $q_curr[$i]->recap_date;
                    $result[$i]->amt = $q_curr[$i]->amt;
                    $total = $total + $q_curr[$i]->amt;
                    $cnt ++;
                }
                if($cnt > 15) break;
            }
            return View::make('mktmgr.bookmngmntweeklydepositshowdetails', compact('result', 'title', 'date_stamp','total'));
        }        
        
        if($title != 'DEPOSIT DETAIL' && $title != 'DELIVERY AND TRANSFERS') {
            $results = DB::select($q, array($date_stamp));
            $rec_cnt = count($results);
            for ($i=0; $i<$rec_cnt; $i++) {
                $totSum += $results[$i]->item_amt;
            }
            $decValue = $totSum;
            UserHomeController::populate_safe_array($results, $date_stamp, $for_mgmt='wk');
            return View::make('mktmgr.bookmngmntweeklyshowdetails', compact('results', 'title', 'date_stamp', 'decValue'));
        }
	 }

    public function getbookcommutermonthlyAdd() {


        $work_data = new StdClass();

        $work_data->dateofinfo = date('m/d/Y', strtotime("last Saturday"));

        $invtypeitemlist = array();
        $sql = "SELECT recapitems.item_id, recapitems.item_desc FROM recapitems WHERE recapitems.disable = 'N' and recapitems.item_type = 'K' ";

        $results = DB::select(DB::raw($sql));

        if (!empty($results)) {
            if (isset($results[0])) {

                foreach ($results as $key) {
                    $invtypeitemlist[] = [ 'id' => $key->item_id, 'value' => $key->item_desc];
                }
            }
        }

        $work_data->invtypeitemlist = $invtypeitemlist;

        return View::make('mktmgr.bookcommutermonthlyadd')->with('data', $work_data);
    }

    public function getbookcommutermonthlyShowdata() {
        $work_data = new StdClass();


        Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {
            $weekday = date('N', strtotime($value));
            if ($weekday == 6) {
                return true;
            }
            return false;
        }, 'NOT A VALID WEEKENDING DATE.....');

        Validator::extend('valid_type', function($attribute, $value, $parameters) {

            $results = DB::select(DB::raw("SELECT * FROM recapitems WHERE recapitems.item_type = 'K' AND recapitems.item_id = ?"), array($value));

            if (!empty($results)) {
                if (isset($results[0])) {
                    return true;
                }
            }

            return false;
        }, 'NOT A VALID INVENTORY ID...');

        Validator::extend('valid_desc', function($attribute, $value, $parameters) {

            $inv_type = Input::get('inv_type');

            $results = DB::select(DB::raw("SELECT recapitems.item_type FROM recapitems WHERE recapitems.item_id = ?"), array($value));

            if (!empty($results)) {
                if (isset($results[0])) {
                    $row_data = $results[0];
                    $item_type = $row_data->item_type;
                    if ($this->get_itemcode_inv_type_monthly($inv_type) == $item_type) {
                        return true;
                    }
                }
            }

            return false;
        }, 'NOT A VALID INVENTORY DESC ID...');

        $validator = Validator::make(Input::all(), array(
        			'comm_month'=>'required',		
                    'inv_type' => 'required|valid_type',
                    'desc' => 'required|valid_desc',
                    'dateofinfo' => 'required|date|weekenddatecheck',
        ),array('comm_month.required'=>'Month is required','inv_type.required'=>'Inventory type is required','desc.required'=>'Description is required'));



        //print_r($validator);

        if ($validator->fails()) {

            return Redirect::route('mktmgr-bookcommutermonthlyadd')
                            ->withErrors($validator)
                            ->withInput();
        } else {

            if (Input::has('dateofinfo')) {
                $date_stamp = Input::get('dateofinfo');
                //$date_stamp = '1989-11-16';
                $dt = new DateTime($date_stamp);
                // $datecheck = new DateTime('1990-01-01');
                // if ($dt < $datecheck) {
                //     $dt = new DateTime("yesterday");
                //     $date_stamp = $dt->format('Y-m-d');
                // }
                $this->date_stamp = date('Y-m-d', strtotime($date_stamp));

                //==put validation for weekending date only Saturday
                //date('w', strtotime($date));



                $work_data->dateofinfo = $date_stamp;
                $this->inv_type = Input::get('inv_type');
                $this->desc = Input::get('desc');
                $this->comm_month = Input::get('comm_month');


                $work_data->int_flag = false;
                $work_data->price = 0;
                $work_data->trf_no_1 = 0;
                $work_data->trf_no_2 = 0;
                $work_data->deliveries = 0;
                $work_data->returns = 0;
                $work_data->end_inv = 0;
                $work_data->begin_inv = 0;
                $work_data->desc = '';
                $work_data->inv_type = '';
                $work_data->desc_desc = '';
                $work_data->inv_desc = '';
                $work_data->comm_month = '';



                
                 $last_inventory_row = DB::select(DB::raw("SELECT * FROM monthcom WHERE monthcom.comm_date < ? AND monthcom.inv_type = ? AND monthcom.desc = ? AND monthcom.comm_month= ? ORDER BY monthcom.comm_date DESC"), array($this->date_stamp, $this->inv_type, $this->desc, $this->comm_month));
                 if (!empty($last_inventory_row)) {
                        if (isset($last_inventory_row[0])) {
                            $last_row_data = $last_inventory_row[0];
                            
                            $work_data->begin_inv = $last_row_data->end_inv; 
                            
                        }
                 }



                $results = DB::select(DB::raw("SELECT * FROM monthcom WHERE monthcom.comm_date = ? AND monthcom.inv_type = ? AND monthcom.desc = ? AND monthcom.comm_month= ?"), array($this->date_stamp, $this->inv_type, $this->desc, $this->comm_month));




                if (!empty($results)) {
                    if (isset($results[0])) {
                        $row_data = $results[0];
                        $work_data->price = $row_data->price;
                        $work_data->trf_no_1 = $row_data->trf_no_1;
                        $work_data->trf_no_2 = $row_data->trf_no_2;
                        $work_data->deliveries = $row_data->deliveries;
                        $work_data->returns = $row_data->returns;
                        $work_data->end_inv = $row_data->end_inv;
                        //$work_data->begin_inv = $row_data->begin_inv;
                        $work_data->desc = $row_data->desc;
                        $work_data->inv_type = $row_data->inv_type;
                        $work_data->comm_month = $row_data->comm_month;
                    }
                }


                if ($work_data->desc == '' || $work_data->inv_type == '') {

                    $work_data->inv_type = $this->inv_type;
                    $work_data->desc = $this->desc;
                }


                if ($work_data->comm_month == '') {
                    $work_data->comm_month = $this->comm_month;
                }


                $work_data->price = (($work_data->price) / 100);
                $work_data->deliveries_amt = (($work_data->deliveries * $work_data->price) );
                $work_data->returns_amt = (($work_data->returns * $work_data->price) );
                $work_data->end_inv_amt = (($work_data->end_inv * $work_data->price) );
                $work_data->begin_inv_amt = (($work_data->begin_inv * $work_data->price) );

                $work_data->sales_amt = ($work_data->begin_inv_amt + $work_data->deliveries_amt - $work_data->returns_amt - $work_data->end_inv_amt);

                if ($work_data->desc != '') {
                    $work_data->desc_desc = $this->get_recapitems_desc($work_data->desc);
                }
                if ($work_data->inv_type != '') {
                    $work_data->inv_desc = $this->get_recapitems_desc($work_data->inv_type);
                }



                $work_data->price = number_format($work_data->price, 2, '.', '');
                $work_data->deliveries_amt = number_format($work_data->deliveries_amt, 2, '.', '');
                $work_data->returns_amt = number_format($work_data->returns_amt, 2, '.', '');
                $work_data->end_inv_amt = number_format($work_data->end_inv_amt, 2, '.', '');
                $work_data->begin_inv_amt = number_format($work_data->begin_inv_amt, 2, '.', '');
                $work_data->sales_amt = number_format($work_data->sales_amt, 2, '.', '');
            }
            return View::make('mktmgr.bookcommutermonthlyshowdata')->with('data', $work_data);
        }
    }

    public function getbookcommutermonthlyPostdata() {


        if (Input::has('comm_date') && Input::has('accept-submit')) {


            $date_stamp = Input::get('comm_date');
            //$date_stamp = '1989-11-16';
            $dt = new DateTime($date_stamp);
            // $datecheck = new DateTime('1990-01-01');
            // if ($dt < $datecheck) {
            //     $dt = new DateTime("yesterday");
            //     $date_stamp = $dt->format('Y-m-d');
            // }

            $this->date_stamp = date('Y-m-d', strtotime($date_stamp));


            $this->inv_type = Input::get('inv_type');
            $this->desc = Input::get('desc');
            $this->comm_month = Input::get('comm_month');

            $results = DB::select(DB::raw("SELECT * FROM monthcom WHERE monthcom.comm_date = ? AND monthcom.inv_type = ? AND monthcom.desc = ? AND monthcom.comm_month = ?"), array($this->date_stamp, $this->inv_type, $this->desc, $this->comm_month));

            $action = "insert";
            if (!empty($results)) {
                if (isset($results[0])) {
                    $action = "update";
                }
            }

            $price = Input::get('price') * 100;
            $trf_no_1 = Input::get('trf_no_1');
            $trf_no_2 = Input::get('trf_no_2');
            $begin_inv = Input::get('begin_inv');

            $deliveries = Input::get('deliveries');
            $returns = Input::get('returns');
            $end_inv = Input::get('end_inv');
            $comm_month = Input::get('comm_month');








            if ($action == "insert") {

                DB::table('monthcom')->insert(
                        ['comm_month' => $comm_month, 'comm_date' => $this->date_stamp, 'price' => $price, 'inv_type' => $this->inv_type, 'desc' => $this->desc, 'trf_no_1' => $trf_no_1, 'trf_no_2' => $trf_no_2, 'begin_inv' => $begin_inv, 'deliveries' => $deliveries, 'returns' => $returns, 'end_inv' => $end_inv]
                );
                $msg = 'Record has been added!';
            }

            if ($action == "update") {

                $results = DB::update("UPDATE monthcom set price='" . $price . "',trf_no_1 ='" . $trf_no_1 . "', trf_no_2 ='" . $trf_no_2 . "', begin_inv ='" . $begin_inv . "', deliveries = '" . $deliveries . "', returns ='" . $returns . "', end_inv ='" . $end_inv . "' WHERE monthcom.comm_date = '" . $this->date_stamp . "' AND monthcom.inv_type = '" . $this->inv_type . "' AND monthcom.desc = '" . $this->desc . "' AND monthcom.comm_month = '" . $this->comm_month . "' ");






                $msg = 'Record has been updated!';
            }

            Session::flash('alert-success', $msg);
        }





        return Redirect::route('mktmgr-bookcommutermonthlyadd');
    }

    public function getbookphonecardAdd() {
        
        $work_data = new StdClass();
        
        $d=strtotime("-1 day"); 
        $work_data->dateofinfo= date("Y-m-d", $d);
        
        return View::make('mktmgr.bookphonecardadd')->with('data', $work_data);
    }
    
     public function getbookphonecardShowdata() {
         $work_data = new StdClass();
        
       
        
        $validator = Validator::make(Input::all(),
            array(
                   'dateofinfo'=>'required|date'
                ),array('dateofinfo.required'=>'Date is required'));
        
        

        //print_r($validator);

        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-bookphonecardadd')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        
        
        if (Input::has('dateofinfo')) {
                $date_stamp = Input::get('dateofinfo');
                //$date_stamp = '1989-11-16';
                $dt = new DateTime($date_stamp);
                // $datecheck = new DateTime('1990-01-01');
                // if ($dt < $datecheck) {
                //     $dt = new DateTime("yesterday");
                //     $date_stamp = $dt->format('Y-m-d');
                // }
                $this->date_stamp = date('Y-m-d', strtotime($date_stamp));

                $file_locked = $this->file_lock($date_stamp);
	        	
	        	if($file_locked)
	        	{
	        		$work_data->file_locked = "Y";
	        	}
	        	else
	        	{
	        		$work_data->file_locked = "N";
	        	}



                $work_data->dateofinfo = $date_stamp;
                $work_data->phone_date=$date_stamp;
                
                
                $work_data->last_inventory_date=$date_stamp;
                
                $work_data->five_begin_inv = 0;
                $work_data->ten_begin_inv = 0; 
                $work_data->twenty_begin_inv = 0; 
                
         $last_inventory_row = DB::select(DB::raw("SELECT * FROM phone_card WHERE phone_card.phone_date < ? ORDER BY phone_card.phone_date DESC "), array($this->date_stamp));
                 if (!empty($last_inventory_row)) {
                        if (isset($last_inventory_row[0])) {
                            $last_row_data = $last_inventory_row[0];
                            $work_data->last_inventory_date=$last_row_data->phone_date; 
                            $work_data->five_begin_inv = $last_row_data->five_end_inv; 
                            $work_data->ten_begin_inv = $last_row_data->ten_end_inv; 
                            $work_data->twenty_begin_inv = $last_row_data->twenty_end_inv; 
                        }
                 }
                
                
                
                
                $work_data->five_deliveries = 0; 
                $work_data->ten_deliveries = 0; 
                $work_data->twenty_deliveries = 0; 
                $work_data->five_returns = 0; 
                $work_data->ten_returns = 0; 
                $work_data->twenty_returns = 0; 
                $work_data->five_end_inv = 0; 
                $work_data->ten_end_inv = 0; 
                $work_data->twenty_end_inv = 0; 
                
                $results = DB::select(DB::raw("SELECT * FROM phone_card WHERE phone_card.phone_date = ?"), array($this->date_stamp));


                    if (!empty($results)) {
                        if (isset($results[0])) {
                            $row_data = $results[0];
                            //$work_data->five_begin_inv=$row_data->five_begin_inv;
                            //$work_data->ten_begin_inv=$row_data->ten_begin_inv;
                            //$work_data->twenty_begin_inv=$row_data->twenty_begin_inv;
                            
                            $work_data->five_deliveries=$row_data->five_deliveries;
                            $work_data->ten_deliveries=$row_data->ten_deliveries;
                            $work_data->twenty_deliveries=$row_data->twenty_deliveries;
                            
                            $work_data->five_returns=$row_data->five_returns;
                            $work_data->ten_returns=$row_data->ten_returns;
                            $work_data->twenty_returns=$row_data->twenty_returns;
                            
                            $work_data->five_end_inv=$row_data->five_end_inv;
                            $work_data->ten_end_inv=$row_data->ten_end_inv;
                            $work_data->twenty_end_inv=$row_data->twenty_end_inv;
                        }
                    }
                    
                    $work_data->five_begin_inv_amt = ($work_data->five_begin_inv*5.00);
                    $work_data->ten_begin_inv_amt= ($work_data->ten_begin_inv * 10.00);
                    $work_data->twenty_begin_inv_amt = ($work_data->twenty_begin_inv *20.00);
                    
                    $work_data->five_deliveries_amt = ($work_data->five_deliveries *5.00);
                    $work_data->ten_deliveries_amt = ($work_data->ten_deliveries*10.00);
                    $work_data->twenty_deliveries_amt = ($work_data->twenty_deliveries*20.00);

                    $work_data->five_returns_amt = ($work_data->five_returns*5.00);
                    $work_data->ten_returns_amt = ($work_data->ten_returns *10.00);
                    $work_data->twenty_returns_amt = ($work_data->twenty_returns *20.00);

                    $work_data->five_end_inv_amt = ($work_data->five_end_inv *5.00);
                    $work_data->ten_end_inv_amt= ($work_data->ten_end_inv *10.00);
                    $work_data->twenty_end_inv_amt = ($work_data->twenty_end_inv*20.00);
                    
                    
                    $work_data->five_begin_inv_amt = number_format($work_data->five_begin_inv_amt, 2, '.', '');
                    $work_data->ten_begin_inv_amt = number_format($work_data->ten_begin_inv_amt, 2, '.', '');
                    $work_data->twenty_begin_inv_amt = number_format($work_data->twenty_begin_inv_amt, 2, '.', '');
                    
                    $work_data->five_deliveries_amt = number_format($work_data->five_deliveries_amt, 2, '.', '');
                    $work_data->ten_deliveries_amt = number_format($work_data->ten_deliveries_amt, 2, '.', '');
                    $work_data->twenty_deliveries_amt = number_format($work_data->twenty_deliveries_amt, 2, '.', '');
                    
                    $work_data->five_returns_amt = number_format($work_data->five_returns_amt, 2, '.', '');
                    $work_data->ten_returns_amt = number_format($work_data->ten_returns_amt, 2, '.', '');
                    $work_data->twenty_returns_amt = number_format($work_data->twenty_returns_amt, 2, '.', '');
                    
                    $work_data->five_end_inv_amt = number_format($work_data->five_end_inv_amt, 2, '.', '');
                    $work_data->ten_end_inv_amt = number_format($work_data->ten_end_inv_amt, 2, '.', '');
                    $work_data->twenty_end_inv_amt = number_format($work_data->twenty_end_inv_amt, 2, '.', '');

                    $work_data->five_sales_amt = ($work_data->five_begin_inv_amt + $work_data->five_deliveries_amt - $work_data->five_returns_amt - $work_data->five_end_inv_amt);
                    $work_data->ten_sales_amt =   ($work_data->ten_begin_inv_amt + $work_data->ten_deliveries_amt - $work_data->ten_returns_amt - $work_data->ten_end_inv_amt);
                    $work_data->twenty_sales_amt = ($work_data->twenty_begin_inv_amt + $work_data->twenty_deliveries_amt - $work_data->twenty_returns_amt - $work_data->twenty_end_inv_amt);
                    
                    
                    $work_data->five_sales_amt = number_format($work_data->five_sales_amt, 2, '.', '');
                    $work_data->ten_sales_amt = number_format($work_data->ten_sales_amt, 2, '.', '');
                    $work_data->twenty_sales_amt = number_format($work_data->twenty_sales_amt, 2, '.', '');

                    $work_data->tot_sales =  ($work_data->five_sales_amt + $work_data->ten_sales_amt + $work_data->twenty_sales_amt);
                    $work_data->tot_sales = number_format($work_data->tot_sales, 2, '.', '');

                    
                    
                    //$work_data->prev_pend_deb = number_format($work_data->prev_pend_deb, 2, '.', '');
       
                return View::make('mktmgr.bookphonecardshowdata')->with('data', $work_data);
        }
        }
     }
    
       public function getbookphonecardPostdata() {


        if (Input::has('phone_date') && Input::has('accept-submit')) {
            
            
             $date_stamp = Input::get('phone_date');
            //$date_stamp = '1989-11-16';
            $dt = new DateTime($date_stamp);
            // $datecheck = new DateTime('1990-01-01');
            // if ($dt < $datecheck) {
            //     $dt = new DateTime("yesterday");
            //     $date_stamp = $dt->format('Y-m-d');
            // }

            $this->date_stamp = date('Y-m-d', strtotime($date_stamp));
            
             $results = DB::select(DB::raw("SELECT * FROM phone_card WHERE phone_card.phone_date = ?"), array($this->date_stamp));

            $action = "insert";
            if (!empty($results)) {
                if (isset($results[0])) {
                    $action = "update";
                }
            }
            
            $five_begin_inv=Input::get('five_begin_inv');
            $ten_begin_inv=Input::get('ten_begin_inv');
            $twenty_begin_inv=Input::get('twenty_begin_inv');

            $five_deliveries=Input::get('five_deliveries');
            $ten_deliveries=Input::get('ten_deliveries');
            $twenty_deliveries=Input::get('twenty_deliveries');

            $five_returns=Input::get('five_returns');
            $ten_returns=Input::get('ten_returns');
            $twenty_returns=Input::get('twenty_returns');

            $five_end_inv=Input::get('five_end_inv');
            $ten_end_inv=Input::get('ten_end_inv');
            $twenty_end_inv=Input::get('twenty_end_inv');
            
            
            if ($action == "insert") {

                DB::table('phone_card')->insert(
                        [ 'phone_date' => $this->date_stamp,
                            'five_begin_inv' => $five_begin_inv,
                            'ten_begin_inv' => $ten_begin_inv,
                            'twenty_begin_inv' => $twenty_begin_inv,

                            'five_deliveries' => $five_deliveries,
                            'ten_deliveries' => $ten_deliveries,
                            'twenty_deliveries' => $twenty_deliveries,

                            'five_returns' => $five_returns,
                            'ten_returns' => $ten_returns,
                            'twenty_returns' => $twenty_returns,

                            'five_end_inv' => $five_end_inv,
                            'ten_end_inv' => $ten_end_inv,
                            'twenty_end_inv' => $twenty_end_inv
                          ]
                );
                $msg = 'Record has been added!';
            }

            if ($action == "update") {

                $results = DB::update("UPDATE phone_card set five_begin_inv='" . $five_begin_inv. "',ten_begin_inv='" . $ten_begin_inv. "',twenty_begin_inv='" . $twenty_begin_inv. "',five_deliveries='" . $five_deliveries. "',ten_deliveries='" . $ten_deliveries. "',twenty_deliveries='" . $twenty_deliveries. "',five_returns='" . $five_returns. "',ten_returns='" . $ten_returns. "',twenty_returns='" . $twenty_returns. "',five_end_inv='" . $five_end_inv. "',ten_end_inv='" . $ten_end_inv. "',twenty_end_inv='" . $twenty_end_inv. "' WHERE phone_card.phone_date = '" . $this->date_stamp . "' ");






                $msg = 'Record has been updated!';
            }

            Session::flash('alert-success', $msg);
            
           
             
        }
        
           

            return Redirect::route('mktmgr-bookphonecardadd');
       }

    

    public function getbookepsAdd() {
       
          $work_data = new StdClass();
        	$d=strtotime("-1 day"); 
        $work_data->dateofinfo = date("Y-m-d", $d);
        
        return View::make('mktmgr.bookepsadd')->with('data', $work_data);
    }
    
     public function getbookepsShowdata() {
        $work_data = new StdClass();
        
       
        
        $validator = Validator::make(Input::all(),
            array(
                   'dateofinfo'=>'required|date'
                ),array('dateofinfo.required'=>'Date is required'));
        
        

        //print_r($validator);

        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-bookepsadd')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        
            if (Input::has('dateofinfo')) {
                $date_stamp = Input::get('dateofinfo');
                //$date_stamp = '1989-11-16';
                $dt = new DateTime($date_stamp);
                // $datecheck = new DateTime('1990-01-01');
                // if ($dt < $datecheck) {
                //     $dt = new DateTime("yesterday");
                //     $date_stamp = $dt->format('Y-m-d');
                // }
                $this->date_stamp = date('Y-m-d', strtotime($date_stamp));

                $file_locked = $this->file_lock($date_stamp);
	        	
	        	if($file_locked)
	        	{
	        		$work_data->file_locked = "Y";
	        	}
	        	else
	        	{
	        		$work_data->file_locked = "N";
	        	}

                
                $work_data->dateofinfo = $date_stamp;
                                              
              
                
                $work_data->eps_date=$date_stamp;

                $work_data->prev_pend_deb=0;
                $work_data->net_sales_deb=0;
                $work_data->curr_pend_deb=0;
                $work_data->offline_rej_deb=0;
                $work_data->dep_sum_deb=0;

                $work_data->prev_pend_ebt=0;
                $work_data->net_sales_ebt=0;
                $work_data->curr_pend_ebt=0;
                $work_data->offline_rej_ebt=0;
                $work_data->dep_sum_ebt=0;

                $work_data->prev_pend_cr=0;
                $work_data->net_sales_cr=0;
                $work_data->curr_pend_cr=0;
                $work_data->offline_rej_cr=0;
                $work_data->dep_sum_cr=0;

                $work_data->prev_pend_gc=0;
                $work_data->net_sales_gc=0;
                $work_data->curr_pend_gc=0;
                $work_data->offline_rej_gc=0;
                $work_data->dep_sum_gc=0;

                $work_data->prev_pend_sc=0;
                $work_data->net_sales_sc=0;
                $work_data->curr_pend_sc=0;
                $work_data->offline_rej_sc=0;
                $work_data->dep_sum_sc=0;

                $work_data->prev_pend_ec=0;
                $work_data->net_sales_ec=0;
                $work_data->curr_pend_ec=0;
                $work_data->offline_rej_ec=0;
                $work_data->dep_sum_ec=0;


                $work_data->tot_debits=0;    
                $work_data->debit_diff=0;   

                $work_data->tot_ebt=0;    
                $work_data->ebt_diff=0;   

                $work_data->tot_credits=0; 
                $work_data->credit_diff=0;

                $work_data->tot_gc=0;    
                $work_data->gc_diff=0;   

                $work_data->tot_sc=0;    
                $work_data->sc_diff=0;   

                $work_data->tot_ec=0;    
                $work_data->ec_diff=0;   

                $work_data->tot_deb_cred=0;
                
               
                 $results = DB::select(DB::raw("SELECT * FROM eps WHERE eps.eps_date = ?"), array($this->date_stamp));

                    
                    

                    if (!empty($results)) {
                        if (isset($results[0])) {
                            $row_data = $results[0];
                            $work_data->prev_pend_deb=$row_data->prev_pend_deb;
                            $work_data->net_sales_deb=$row_data->net_sales_deb;
                            $work_data->curr_pend_deb=$row_data->curr_pend_deb;
                            $work_data->offline_rej_deb=$row_data->offline_rej_deb;
                            $work_data->dep_sum_deb=$row_data->dep_sum_deb;

                            $work_data->prev_pend_ebt=$row_data->prev_pend_ebt;
                            $work_data->net_sales_ebt=$row_data->net_sales_ebt;
                            $work_data->curr_pend_ebt=$row_data->curr_pend_ebt;
                            $work_data->offline_rej_ebt=$row_data->offline_rej_ebt;
                            $work_data->dep_sum_ebt=$row_data->dep_sum_ebt;

                            $work_data->prev_pend_cr=$row_data->prev_pend_cr;
                            $work_data->net_sales_cr=$row_data->net_sales_cr;
                            $work_data->curr_pend_cr=$row_data->curr_pend_cr;
                            $work_data->offline_rej_cr=$row_data->offline_rej_cr;
                            $work_data->dep_sum_cr=$row_data->dep_sum_cr;

                            $work_data->prev_pend_gc=$row_data->prev_pend_gc;
                            $work_data->net_sales_gc=$row_data->net_sales_gc;
                            $work_data->curr_pend_gc=$row_data->curr_pend_gc;
                            $work_data->offline_rej_gc=$row_data->offline_rej_gc;
                            $work_data->dep_sum_gc=$row_data->dep_sum_gc;

                            $work_data->prev_pend_sc=$row_data->prev_pend_sc;
                            $work_data->net_sales_sc=$row_data->net_sales_sc;
                            $work_data->curr_pend_sc=$row_data->curr_pend_sc;
                            $work_data->offline_rej_sc=$row_data->offline_rej_sc;
                            $work_data->dep_sum_sc=$row_data->dep_sum_sc;

                            $work_data->prev_pend_ec=$row_data->prev_pend_ec;
                            $work_data->net_sales_ec=$row_data->net_sales_ec;
                            $work_data->curr_pend_ec=$row_data->curr_pend_ec;
                            $work_data->offline_rej_ec=$row_data->offline_rej_ec;
                            $work_data->dep_sum_ec=$row_data->dep_sum_ec;
                        }
                    }
                        
                        
                        
                
            #--- debit
            $work_data->prev_pend_deb = ($work_data->prev_pend_deb / 100);
            $work_data->net_sales_deb = ($work_data->net_sales_deb / 100);  
            $work_data->curr_pend_deb = ($work_data->curr_pend_deb / 100);
            $work_data->offline_rej_deb = ($work_data->offline_rej_deb / 100);
            $work_data->dep_sum_deb = ($work_data->dep_sum_deb / 100);
            

            #--- ebt
            $work_data->prev_pend_ebt = ($work_data->prev_pend_ebt / 100);
            $work_data->net_sales_ebt = ($work_data->net_sales_ebt / 100);
            $work_data->curr_pend_ebt = ($work_data->curr_pend_ebt / 100);
            $work_data->offline_rej_ebt = ($work_data->offline_rej_ebt / 100);
            $work_data->dep_sum_ebt = ($work_data->dep_sum_ebt / 100);
            

            #--- credit
            $work_data->prev_pend_cr = ($work_data->prev_pend_cr / 100);
            $work_data->net_sales_cr = ($work_data->net_sales_cr / 100);
            $work_data->curr_pend_cr = ($work_data->curr_pend_cr / 100);
            $work_data->offline_rej_cr = ($work_data->offline_rej_cr / 100);
            $work_data->dep_sum_cr = ($work_data->dep_sum_cr / 100);

            #--- gc
            $work_data->prev_pend_gc = ($work_data->prev_pend_gc / 100);
            $work_data->net_sales_gc = ($work_data->net_sales_gc / 100);
            $work_data->curr_pend_gc = ($work_data->curr_pend_gc / 100);
            $work_data->offline_rej_gc = ($work_data->offline_rej_gc / 100);
            $work_data->dep_sum_gc = ($work_data->dep_sum_gc / 100);

            #--- sc (store credit)
            $work_data->prev_pend_sc = ($work_data->prev_pend_sc / 100);
            $work_data->net_sales_sc = ($work_data->net_sales_sc / 100);
            $work_data->curr_pend_sc = ($work_data->curr_pend_sc / 100);
            $work_data->offline_rej_sc = ($work_data->offline_rej_sc / 100);
            $work_data->dep_sum_sc = ($work_data->dep_sum_sc / 100);

            #--- ec (E-Check)
            $work_data->prev_pend_ec = ($work_data->prev_pend_ec / 100);
            $work_data->net_sales_ec = ($work_data->net_sales_ec / 100);
            $work_data->curr_pend_ec = ($work_data->curr_pend_ec / 100);
            $work_data->offline_rej_ec = ($work_data->offline_rej_ec / 100);
            $work_data->dep_sum_ec = ($work_data->dep_sum_ec / 100);
            
            
            
            #--- debit
            $work_data->prev_pend_deb = number_format($work_data->prev_pend_deb, 2, '.', '');
            $work_data->net_sales_deb = number_format($work_data->net_sales_deb, 2, '.', '');  
            $work_data->curr_pend_deb = number_format($work_data->curr_pend_deb, 2, '.', '');
            $work_data->offline_rej_deb = number_format($work_data->offline_rej_deb, 2, '.', '');
            $work_data->dep_sum_deb = number_format($work_data->dep_sum_deb, 2, '.', '');
            

            #--- ebt
            $work_data->prev_pend_ebt = number_format($work_data->prev_pend_ebt, 2, '.', '');
            $work_data->net_sales_ebt = number_format($work_data->net_sales_ebt, 2, '.', '');
            $work_data->curr_pend_ebt = number_format($work_data->curr_pend_ebt, 2, '.', '');
            $work_data->offline_rej_ebt = number_format($work_data->offline_rej_ebt, 2, '.', '');
            $work_data->dep_sum_ebt = number_format($work_data->dep_sum_ebt, 2, '.', '');
            

            #--- credit
            $work_data->prev_pend_cr = number_format($work_data->prev_pend_cr, 2, '.', '');
            $work_data->net_sales_cr = number_format($work_data->net_sales_cr, 2, '.', '');
            $work_data->curr_pend_cr = number_format($work_data->curr_pend_cr, 2, '.', '');
            $work_data->offline_rej_cr = number_format($work_data->offline_rej_cr, 2, '.', '');
            $work_data->dep_sum_cr = number_format($work_data->dep_sum_cr, 2, '.', '');

            #--- gc
            $work_data->prev_pend_gc = number_format($work_data->prev_pend_gc, 2, '.', '');
            $work_data->net_sales_gc = number_format($work_data->net_sales_gc, 2, '.', '');
            $work_data->curr_pend_gc = number_format($work_data->curr_pend_gc, 2, '.', '');
            $work_data->offline_rej_gc = number_format($work_data->offline_rej_gc, 2, '.', '');
            $work_data->dep_sum_gc = number_format($work_data->dep_sum_gc, 2, '.', '');

            #--- sc (store credit)
            $work_data->prev_pend_sc = number_format($work_data->prev_pend_sc, 2, '.', '');
            $work_data->net_sales_sc = number_format($work_data->net_sales_sc, 2, '.', '');
            $work_data->curr_pend_sc = number_format($work_data->curr_pend_sc, 2, '.', '');
            $work_data->offline_rej_sc = number_format($work_data->offline_rej_sc, 2, '.', '');
            $work_data->dep_sum_sc = number_format($work_data->dep_sum_sc, 2, '.', '');

            #--- ec (E-Check)
            $work_data->prev_pend_ec = number_format($work_data->prev_pend_ec, 2, '.', '');
            $work_data->net_sales_ec = number_format($work_data->net_sales_ec, 2, '.', '');
            $work_data->curr_pend_ec = number_format($work_data->curr_pend_ec, 2, '.', '');
            $work_data->offline_rej_ec = number_format($work_data->offline_rej_ec, 2, '.', '');
            $work_data->dep_sum_ec = number_format($work_data->dep_sum_ec, 2, '.', '');
            
            
            
            #--- debit
         $work_data->tot_debits = (($work_data->prev_pend_deb + $work_data->net_sales_deb - $work_data->curr_pend_deb - $work_data->offline_rej_deb) ); 

         $work_data->debit_diff = (($work_data->dep_sum_deb - ($work_data->tot_debits)) );
         
         
         
         
         
#--- ebt
         $work_data->tot_ebt = (($work_data->prev_pend_ebt + $work_data->net_sales_ebt - $work_data->curr_pend_ebt - $work_data->offline_rej_ebt) ); 

         $work_data->ebt_diff = (($work_data->dep_sum_ebt - ($work_data->tot_ebt)) ); 
         
         
         

#--- credit
         $work_data->tot_credits =  (($work_data->prev_pend_cr + $work_data->net_sales_cr - $work_data->curr_pend_cr - $work_data->offline_rej_cr) );

         $work_data->credit_diff =  (($work_data->dep_sum_cr - ($work_data->tot_credits)) ); 
         
         
         
#--- gc
         $work_data->tot_gc =(($work_data->prev_pend_gc + $work_data->net_sales_gc -  $work_data->curr_pend_gc - $work_data->offline_rej_gc) );

         $work_data->gc_diff = (($work_data->dep_sum_gc - ($work_data->tot_gc)) );
         
         
      
         

#--- sc (store credit)
         $work_data->tot_sc =(($work_data->prev_pend_sc + $work_data->net_sales_sc - $work_data->curr_pend_sc - $work_data->offline_rej_sc) );

         $work_data->sc_diff = (($work_data->dep_sum_sc - ($work_data->tot_sc)) );
         
         
         

#--- ec (E-Check)
         $work_data->tot_ec = (($work_data->prev_pend_ec + $work_data->net_sales_ec - $work_data->curr_pend_ec - $work_data->offline_rej_ec) );

         $work_data->ec_diff = (($work_data->dep_sum_ec - ($work_data->tot_ec)) );
         
         
         

    #--- tot all
    $work_data->tot_deb_cred = ($work_data->debit_diff + $work_data->ebt_diff + $work_data->gc_diff + $work_data->sc_diff + $work_data->ec_diff + $work_data->credit_diff); 
    
         $work_data->tot_deb_cred = number_format($work_data->tot_deb_cred, 2, '.', '');
    
         $work_data->tot_ec = number_format($work_data->tot_ec, 2, '.', '');
         $work_data->ec_diff = number_format($work_data->ec_diff, 2, '.', '');
         $work_data->tot_sc = number_format($work_data->tot_sc, 2, '.', '');
         $work_data->sc_diff = number_format($work_data->sc_diff, 2, '.', '');
         
         $work_data->tot_gc = number_format($work_data->tot_gc, 2, '.', '');
         $work_data->gc_diff = number_format($work_data->gc_diff, 2, '.', '');
         $work_data->tot_credits = number_format($work_data->tot_credits, 2, '.', '');
         $work_data->credit_diff = number_format($work_data->credit_diff, 2, '.', '');
         
         $work_data->tot_ebt = number_format($work_data->tot_ebt, 2, '.', '');
         $work_data->ebt_diff = number_format($work_data->ebt_diff, 2, '.', '');
         $work_data->tot_debits = number_format($work_data->tot_debits, 2, '.', '');
         $work_data->debit_diff = number_format($work_data->debit_diff, 2, '.', '');
                    
                    
                return View::make('mktmgr.bookepsshowdata')->with('data', $work_data);
        }
        }
     }
    
       public function getbookepsPostdata() {


        if (Input::has('eps_date') && Input::has('accept-submit')) {


            $date_stamp = Input::get('eps_date');
            //$date_stamp = '1989-11-16';
            $dt = new DateTime($date_stamp);
            // $datecheck = new DateTime('1990-01-01');
            // if ($dt < $datecheck) {
            //     $dt = new DateTime("yesterday");
            //     $date_stamp = $dt->format('Y-m-d');
            // }

            $this->date_stamp = date('Y-m-d', strtotime($date_stamp));


           

            $results = DB::select(DB::raw("SELECT * FROM eps WHERE eps.eps_date = ?"), array($this->date_stamp));

            $action = "insert";
            if (!empty($results)) {
                if (isset($results[0])) {
                    $action = "update";
                }
            }

            




            $prev_pend_deb=Input::get('prev_pend_deb') * 100;
            $net_sales_deb=Input::get('net_sales_deb') * 100;
            $curr_pend_deb=Input::get('curr_pend_deb') * 100;
            $offline_rej_deb=Input::get('offline_rej_deb') * 100;
            $dep_sum_deb=Input::get('dep_sum_deb') * 100;

            $prev_pend_ebt=Input::get('prev_pend_ebt') * 100;
            $net_sales_ebt=Input::get('net_sales_ebt') * 100;
            $curr_pend_ebt=Input::get('curr_pend_ebt') * 100;
            $offline_rej_ebt=Input::get('offline_rej_ebt') * 100;
            $dep_sum_ebt=Input::get('dep_sum_ebt') * 100;

            $prev_pend_cr=Input::get('prev_pend_cr') * 100;
            $net_sales_cr=Input::get('net_sales_cr') * 100;
            $curr_pend_cr=Input::get('curr_pend_cr') * 100;
            $offline_rej_cr=Input::get('offline_rej_cr') * 100;
            $dep_sum_cr=Input::get('dep_sum_cr') * 100;

            $prev_pend_gc=Input::get('prev_pend_gc') * 100;
            $net_sales_gc=Input::get('net_sales_gc') * 100;
            $curr_pend_gc=Input::get('curr_pend_gc') * 100;
            $offline_rej_gc=Input::get('offline_rej_gc') * 100;
            $dep_sum_gc=Input::get('dep_sum_gc') * 100;

            $prev_pend_sc=Input::get('prev_pend_sc') * 100;
            $net_sales_sc=Input::get('net_sales_sc') * 100;
            $curr_pend_sc=Input::get('curr_pend_sc') * 100;
            $offline_rej_sc=Input::get('offline_rej_sc') * 100;
            $dep_sum_sc=Input::get('dep_sum_sc') * 100;

            $prev_pend_ec=Input::get('prev_pend_ec') * 100;
            $net_sales_ec=Input::get('net_sales_ec') * 100;
            $curr_pend_ec=Input::get('curr_pend_ec') * 100;
            $offline_rej_ec=Input::get('offline_rej_ec') * 100;
            $dep_sum_ec=Input::get('dep_sum_ec') * 100;






            if ($action == "insert") {

                DB::table('eps')->insert(
                        [ 'eps_date' => $this->date_stamp,
                           'prev_pend_deb' => $prev_pend_deb,
                           'net_sales_deb' => $net_sales_deb,
                           'curr_pend_deb' => $curr_pend_deb,
                           'offline_rej_deb' => $offline_rej_deb,
                           'dep_sum_deb' => $dep_sum_deb,
                           'prev_pend_ebt' => $prev_pend_ebt,
                           'net_sales_ebt' => $net_sales_ebt,
                           'curr_pend_ebt' => $curr_pend_ebt,
                           'offline_rej_ebt' => $offline_rej_ebt,
                           'dep_sum_ebt' => $dep_sum_ebt,
                           'prev_pend_cr' => $prev_pend_cr,
                           'net_sales_cr' => $net_sales_cr,
                           'curr_pend_cr' => $curr_pend_cr,
                           'offline_rej_cr' => $offline_rej_cr,
                           'dep_sum_cr' => $dep_sum_cr,
                           'prev_pend_gc' => $prev_pend_gc,
                           'net_sales_gc' => $net_sales_gc,
                           'curr_pend_gc' => $curr_pend_gc,
                           'offline_rej_gc' => $offline_rej_gc,
                           'dep_sum_gc' => $dep_sum_gc,
                           'prev_pend_sc' => $prev_pend_sc,
                           'net_sales_sc' => $net_sales_sc,
                           'curr_pend_sc' => $curr_pend_sc,
                           'offline_rej_sc' => $offline_rej_sc,
                           'dep_sum_sc' => $dep_sum_sc,
                           'prev_pend_ec' => $prev_pend_ec,
                           'net_sales_ec' => $net_sales_ec,
                           'curr_pend_ec' => $curr_pend_ec,
                           'offline_rej_ec' => $offline_rej_ec,
                           'dep_sum_ec' => $dep_sum_ec ]
                );
                $msg = 'Record has been added!';
            }

            if ($action == "update") {

                $results = DB::update("UPDATE eps set prev_pend_deb='" . $prev_pend_deb. "',net_sales_deb='" . $net_sales_deb. "',curr_pend_deb='" . $curr_pend_deb. "',offline_rej_deb='" . $offline_rej_deb. "',dep_sum_deb='" . $dep_sum_deb. "', prev_pend_ebt='" . $prev_pend_ebt. "',net_sales_ebt='" . $net_sales_ebt. "',curr_pend_ebt='" . $curr_pend_ebt. "',offline_rej_ebt='" . $offline_rej_ebt. "',dep_sum_ebt='" . $dep_sum_ebt. "', prev_pend_cr='" . $prev_pend_cr. "',net_sales_cr='" . $net_sales_cr. "',curr_pend_cr='" . $curr_pend_cr. "',offline_rej_cr='" . $offline_rej_cr. "',dep_sum_cr='" . $dep_sum_cr. "', prev_pend_gc='" . $prev_pend_gc. "',net_sales_gc='" . $net_sales_gc. "',curr_pend_gc='" . $curr_pend_gc. "',offline_rej_gc='" . $offline_rej_gc. "',dep_sum_gc='" . $dep_sum_gc. "', prev_pend_sc='" . $prev_pend_sc. "',net_sales_sc='" . $net_sales_sc. "',curr_pend_sc='" . $curr_pend_sc. "',offline_rej_sc='" . $offline_rej_sc. "',dep_sum_sc='" . $dep_sum_sc. "', prev_pend_ec='" . $prev_pend_ec. "',net_sales_ec='" . $net_sales_ec. "',curr_pend_ec='" . $curr_pend_ec. "',offline_rej_ec='" . $offline_rej_ec. "',dep_sum_ec='" . $dep_sum_ec. "' WHERE eps.eps_date = '" . $this->date_stamp . "' ");






                $msg = 'Record has been updated!';
            }
             //echo $date_stamp;exit;
             $eps_date = Input::get('eps_date'); 
             $tot_deb_cred = Input::get('tot_deb_cred')*100; 
             //echo $eps_date;exit;
            //echo $work_data->tot_deb_cred;exit;       	   
         	$upd_safe = $this->upd_safe($eps_date,$tot_deb_cred);
            Session::flash('alert-success', $msg);
        }
            

            return Redirect::route('mktmgr-bookepsadd');
       }
    public function upd_safe($eps_date,$tot_deb_cred)
    {
    	//echo $tot_deb_cred.' '.$eps_date;exit;
    	$int_amt = $tot_deb_cred;
    	$safeentry_query = DB::select('SELECT safeentry.item_amt FROM safeentry WHERE 
    		safeentry.item_id = "EE" AND safeentry.entry_date = "'.$eps_date.'" ');
    	$safeentry_array = json_decode(json_encode($safeentry_query), true);
    	//echo '<pre>';print_r($safeentry_array);exit;
    	if(empty($safeentry_array))
    	{
    		DB::insert('INSERT INTO safeentry values("EE","'.$int_amt.'","system","'.$eps_date.'")');
    	}
    	else
    	{
    		DB::update('UPDATE safeentry
              SET safeentry.item_amt = "'.$int_amt.'"
            WHERE safeentry.item_id = "EE"
              AND safeentry.entry_date = "'.$eps_date.'" ');
    	}
    }
    public function getbookepsPrnt() {
        return View::make('mktmgr.bookepsprnt');
    }

    public function getbookmngmntDaily() {
        return View::make('mktmgr.bookmngmntdaily');
    }

    public function postBookmngmntDaily() {


    	 $validator = Validator::make(Input::all(),
            array(
                   'dateofinfo'=>'required|date'
                ),array('dateofinfo.required'=>'Date is required'));
        if($validator->fails())
        {
           
            return Redirect::route('mktmgr-bookmngmntdaily')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        

        if (Input::has('dateofinfo')) {

            $date_stamp = Input::get('dateofinfo');
          
            $date_stamp = UserHomeController::convertDateToYMD($date_stamp);

            //$date_stamp = Input::get('dateofinfo');
            //$date_stamp = '1989-11-16';
            $dt = new DateTime($date_stamp);
            $datecheck = new DateTime('1990-01-01');
            if ($dt < $datecheck) {
                $dt = new DateTime("yesterday");
                $date_stamp = $dt->format('Y-m-d');
            }
            $this->date_stamp = $date_stamp;
            if (UserHomeController::file_locked()) {
                UserHomeController::calc_debtot();
            }
            UserHomeController::init_rec();

            UserHomeController::find_os();
            UserHomeController::calc_totals();
            
            $begin_safe =$this->begin_safe;
            $del_tranfs =$this->del_tranfs;
            $deposits =$this->deposits;
            $store_tx =$this->store_tx;
            $safe_cnt =$this->safe_cnt;
            $po_types =$this->po_types;
            $groc_os =$this->groc_os;
            $drug_os =$this->drug_os;
            $tot_acct =$this->tot_acct;
            $safe_os =$this->safe_os;
            $total =$this->total;
    
            return View::make('mktmgr.bookmngmntdailypostview', compact('begin_safe','del_tranfs','deposits','store_tx','po_types','groc_os','drug_os','tot_acct','safe_cnt','safe_os','total','date_stamp'));
        }
        // else {
        //     Session::flash('alert-warning', 'Please select date first!'); 
        //     return Redirect::route('mktmgr-bookmngmntdaily');
        // }
	 }
	}
	 
	 public function postBookmngmntDailyShowDetails(){
        $date_stamp = Input::get('date_stamp');
        $title = Input::get('t');
        $totSum=0;

        $prev_date = date('Y-m-d', strtotime($date_stamp .' -1 day'));
        $mgr_apv = Mgmt::where('date_stamp',$prev_date)->first();
        
        if(is_null($mgr_apv)) {
            echo 'THE MANAGER HAS NOT APPROVED PREVIOUS DAYS WORK.'; echo "<br />";
            echo '<input type="button" value="Cancel" class="btn" id="cancel" name="cancel" onclick="clickCancel()">';
            exit;
        }
        
        if($title == 'DELIVERIES') {
            $q = 'select safeitems.item_id, safeitems.item_desc, safeentry.item_amt from safeitems LEFT JOIN safeentry ON safeitems.item_id = safeentry.item_id where safeitems.item_type = "T" and safeentry.entry_date = ? and safeitems.disable not in ("y", "Y") order by safeitems.item_id';
        }
        else if($title == 'DEPOSITS') {
            $q = 'select safeitems.item_id, safeitems.item_desc, safeentry.item_amt from safeitems LEFT JOIN safeentry ON safeitems.item_id = safeentry.item_id where safeitems.item_type = "P" and safeitems.item_id <> "#C" and safeentry.entry_date = ? and safeitems.disable not in ("y", "Y") order by safeitems.item_id';
        }
        else if($title == 'STORE TRANSACTIONS') {
            $q = 'select safeitems.item_id, safeitems.item_desc, safeentry.item_amt from safeitems LEFT JOIN safeentry ON safeitems.item_id = safeentry.item_id where safeitems.item_type = "S" and safeentry.entry_date = ? and safeitems.disable not in ("y", "Y") order by safeitems.item_id';
        }
        else if($title == 'PAID OUT TYPES') {
            $q = 'select safeitems.item_id, safeitems.item_desc, safeentry.item_amt from safeitems LEFT JOIN safeentry ON safeitems.item_id = safeentry.item_id where safeitems.item_type = "O" and safeentry.entry_date = ? and safeitems.disable not in ("y", "Y") order by safeitems.item_id';
        }
        //echo $q; 
        $results = DB::select($q, array($date_stamp));
        $rec_cnt = count($results);
        
       if($rec_cnt != '')
       { 
        for ($i=0; $i<$rec_cnt; $i++) {
            $totSum += $results[$i]->item_amt;
        }
        $decValue = $totSum;
        UserHomeController::populate_safe_array($results, $date_stamp);
        //UserHomeController::viewEditSafe1($title, $date_stamp, $rec_cnt, TRUE);
        //print_r($results[0]); //exit;
		return View::make('mktmgr.bookmngmntdailyshowdetails', compact('results', 'title', 'date_stamp', 'decValue'));
	   }
	   else
	   {
	   	  echo 'No data found!'; echo "<br />";
          echo '<input type="button" value="Cancel" class="btn" id="cancel" name="cancel" onclick="clickCancel()">';
       
          exit;
	   }	
	 }

	  public function getbookmngmntWeekly()
	 {
		return View::make('mktmgr.bookmngmntweekly'); 
	 }
 // public function postBookmngmntWeekly()
	 // {
		// return View::make('mktmgr.bookmngmntweeklypostview'); 
	 // }
	 // public function postBookmngmntWeeklyShowDetails()
	 // {
		// return View::make('mktmgr.bookmngmntweeklyshowdetails'); 
	 // }
	 
	 /* public function getbookmngmntCmt()
	 {
		return View::make('mktmgr.bookmngmntcmt'); 
	 }*/
	  /*public function getbookutilitypymntCnt()
	 {
		return View::make('mktmgr.bookutilitypymntcnt'); 
	 }*/
	/* public function getbookcommentList()
	 {
		$comments = new BookkeepComment;
        $comments = $comments->orderBy('date_stamp', 'desc')->paginate(20);
        return View::make('mktmgr.bookcommentlist', compact('comments')); 
	 }*/
	 /*public function bookcommentAdd()
	 {
        if(Input::has('dateofinfo')) {
            $date_stamp = Input::get('dateofinfo');
        }
        else {
            $date_stamp = date('Y-m-d');
        }
        if(!is_null($cmt_data = BookkeepComment::where('date_stamp',$date_stamp)->first())) {
            $cmt_data = BookkeepComment::where('date_stamp',$date_stamp)->first();
            $cmt_data->exists = true;
        }
        else {
            $cmt_data = new BookkeepComment;
            $cmt_data->date_stamp = $date_stamp;
            $cmt_data->exists = false;
            $cmt_data->comment_line1 = '';
        }
        
        return View::make('mktmgr.bookcommentadd')->with('cmt_data',$cmt_data);
	 }*/
	 public function postBookcomment()
	 {
        $validator = Validator::make(Input::all(),
    		array(
    			   'dateofinfo' =>'required',
    			   'commenttext'=>'required'
    			));
		if($validator->fails())
		{
			return Redirect::route('mktmgr-bookcommentadd')
					->withErrors($validator)
					->withInput();
		}
		else
		{
            if(is_null($cmt_data = BookkeepComment::where('date_stamp',Input::get('dateofinfo'))->first())) {
                $addComment = new BookkeepComment;
                if(Input::has('dateofinfo')) {
                    $addComment->date_stamp = Input::get('dateofinfo');
                }
                if(Input::has('commenttext')) {
                    $addComment->comment_line1 = trim(Input::get('commenttext'));
                }
                $addComment->comment_line2 = '';
                $addComment->comment_line3 = '';
                $addComment->comment_line4 = '';
                $addComment->comment_line5 = '';
                $addComment->comment_line6 = '';
                $addComment->comment_line7 = '';
                $addComment->comment_line8 = '';
                $addComment->comment_line9 = '';
                $addComment->save();
                //var_dump($addComment); exit;
            } else {
                if (Input::has('dateofinfo')) {
                    $arr['date_stamp'] = Input::get('dateofinfo');
                }
                if (Input::has('commenttext')) {
                    $arr['comment_line1'] = trim(Input::get('commenttext'));
                }
                $arr['comment_line2'] = '';
                $arr['comment_line3'] = '';
                $arr['comment_line4'] = '';
                $arr['comment_line5'] = '';
                $arr['comment_line6'] = '';
                $arr['comment_line7'] = '';
                $arr['comment_line8'] = '';
                $arr['comment_line9'] = '';

                BookkeepComment::where('date_stamp', Input::get('dateofinfo'))->update($arr);
            }
            return Redirect::route('mktmgr-bookcommentlist');
        }
    }

    public function getBackHome() {
        return View::make('homepage');
    }

    public function getSaveBeginLoanregister() {

        /* $data = array('reg_number' => Input::get('reg_number'),
          'reg_date' => Input::get('dateofreg'));
          return View::make('mktmgr.savebeginloanregister')->with('data',$data); */
    }

    public function postBeginLoanRegister() { 
        //$addloanbegin = new User;
        //$addloanbegin = Auth::user(); //exit;

        /* if(Input::has('reg_number')) {
          $data['reg_number'] = Input::get('reg_number');
          }

          if(Input::has('dateofreg')) {
          $data['dateofreg'] = Input::get('dateofreg');
          } */
        $seldate=$this->convertDateToMDY(Input::get('dateofreg'));

        $data = array('reg_number' => Input::get('reg_number'), 'reg_date' => $seldate);
        /*
          $val=DB::table('begin_loan')->insert(
          array('reg_num' => Input::get('reg_number'),
          'food_stamps' => '0',
          'ones' => '0',
          'fives' => '0',
          'tens' => '0',
          'twenties' => '0',
          'rolled_coins' => '0',
          'quarters' => '0',
          'nickles' => '0',
          'dimes' => '0',
          'pennies' => '0',
          'misc' => '0',
          'bl_total' => '0',
          'entry_date' => Input::get('dateofreg')
          )
          );
         */
        //$addloanbegin->save();
        //Session::flash('alert-success', 'Record added successfully!'); 
        //return Redirect::route('mktmgr-bookkeeperregister');
        return View::make('mktmgr.savebeginloanregister')->with('data', $data);
        //table_loanbegin
    }

    public function postBeginLoanReg() {
        $addloanbegin = new User;
        $addloanbegin = Auth::user();
        $data = array('reg_number' => Input::get('reg_number'),
            'reg_date' => Input::get('reg_date'));

        $val = DB::table('begin_loan')->insert(
                array('reg_num' => Input::get('reg_number'),
                    'food_stamps' => Input::get('foodstamps'),
                    'ones' => Input::get('ones'),
                    'fives' => Input::get('fives'),
                    'tens' => Input::get('tens'),
                    'twenties' => Input::get('twenties'),
                    'rolled_coins' => Input::get('quarters'),
                    'quarters' => Input::get('dimes'),
                    'nickles' => Input::get('nickles'),
                    'dimes' => Input::get('dimes'),
                    'pennies' => Input::get('pennies'),
                    'misc' => Input::get('misc'),
                    'bl_total' => Input::get('total'),
                    'entry_date' => Input::get('reg_date')
                )
        );
        $addloanbegin->save();
        //  echo "waht is result ".
        //if($result == 1)
        // {
        /*       		Session::flash('alert-success', 'Record added successfully!'); 
          return Redirect::route('mktmgr-receivings-receivingsofflinereceiving');
         */
        Session::flash('alert-success', 'Record added successfully!');
        return View::make('mktmgr.savebeginloanregister')->with('data', $data);
        //}	        	
    }

    function postDateGetData() {
    	$validator = Validator::make(Input::all(),
            array(
                   'dateofinfo'=>'required',
                ));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-bookkeeperregistertotal')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
	        $addloanbegin = new User;
	        $addloanbegin = Auth::user();
	        $dateofinfo =Input::get('dateofinfo'); $reg_num = ''; $itemid= ''; $itemname = ''; $dayIsLocked = 'N';
	        $results = DB::select('select r.reg_num, b.bl_total as item_amt from registers r, begin_loan b 
	          					   where b.reg_num = r.reg_num and b.entry_date = "' . Input::get('dateofinfo') . '"
	  							   order by r.reg_num');
	        $resultArray = json_decode(json_encode($results), true);
	      if(count($resultArray) > 0)
	      {
	      	$dayIsLocked= 'Y'; $itemname='Begin Loans'; $itemid='BL';
	       return View::make('mktmgr.bookloanrregtotlist',compact('resultArray', 'dateofinfo', 'itemid', 'itemname','dayIsLocked'));
	      }
	      else
	      {

	      	$results=DB::select('select max(date_stamp) as lastAprvlDate from mgmt where 
	      							   date_stamp < "'. Input::get('dateofinfo') .'"');
	      	$resultArray = json_decode(json_encode($results), true);
	      	$datesel=$resultArray[0]['lastAprvlDate'];
	      	$lastAprvlDate = date("m-d-Y", strtotime($datesel));

	      	Session::flash('alert-success', 
	      		           'Last approved date was ,'.$lastAprvlDate.'. A manager need to approve daily work');
	      	return View::make('mktmgr.bookkeeperregistertotal');
	      } 
        }
    }

    public function file_locked() {
        if (is_null(BookkeepLocks::where('date_stamp', $this->date_stamp)->first())) {
            return false;
        } else {
            return true;
        }
    }

    public function calc_debtot() {
        $gsubtot = $lr_tot = 0;
        $totrs = DB::select('SELECT SUM(entry.item_amt) AS gsubtot FROM entry,items WHERE entry.entry_date = ? AND items.item_type = "D" AND entry.item_id = items.item_id', array($this->date_stamp));
        if ($totrs[0]->gsubtot == NULL) {
            $gsubtot = 0;
        } else {
            $gsubtot = $totrs[0]->gsubtot;
        }

        $sfentryrs = DB::select('SELECT safeentry.item_amt AS td_amt FROM safeentry WHERE safeentry.entry_date = ? AND safeentry.item_id = "TD"', array($this->date_stamp));
        if (count($sfentryrs) < 1) {
            $bse = new BookkeepSafeentry;
            $bse->item_id = 'TD';
            $bse->item_amt = $gsubtot;
            $bse->source_id = 'mktmgr';
            $bse->entry_date = $this->date_stamp;
            $bse->save();
        } else {
            $whr = array('item_id' => 'TD', 'entry_date' => $this->date_stamp);
            $arr['item_amt'] = $gsubtot;
            $bse = BookkeepSafeentry::where($whr)->update($arr);
        }

        $lrtotrs = DB::select('SELECT SUM(entry.item_amt) AS lr_tot FROM entry WHERE entry.entry_date = ? AND entry.item_id = "LR"', array($this->date_stamp));
        if ($lrtotrs[0]->lr_tot == NULL) {
            $lr_tot = 0;
        } else {
            $lr_tot = ($lrtotrs[0]->lr_tot * (-1));
        }

        $sfentryrs = DB::select('SELECT * from safeentry WHERE safeentry.entry_date = ? AND safeentry.item_id = "LR"', array($this->date_stamp));
        if (count($sfentryrs) < 1) {
            $bse = new BookkeepSafeentry;
            $bse->item_id = 'LR';
            $bse->item_amt = $lr_tot;
            $bse->source_id = 'mktmgr';
            $bse->entry_date = $this->date_stamp;
            $bse->save();
        } else {
            $whr = array('item_id' => 'LR', 'entry_date' => $this->date_stamp);
            $arr['item_amt'] = $lr_tot;
            $bse = BookkeepSafeentry::where($whr)->update($arr);
        }
    }

    public function init_rec() {
        $init_rec = BookkeepMainsafe::where('safe_date',$this->date_stamp)->first();
        if(is_null($init_rec)) {
            if(!UserHomeController::file_locked()) {
                $addmainsafe = new BookkeepMainsafe;
                $addmainsafe->begin_safe = 0;
                $addmainsafe->del_tranfs = 0;
                $addmainsafe->deposits = 0;
                $addmainsafe->store_tx = 0;
                $addmainsafe->po_types = 0;
                $addmainsafe->tot_acct = 0;
                $addmainsafe->safe_cnt = 0;
                $addmainsafe->safe_os = 0;
                $addmainsafe->groc_os = 0;
                $addmainsafe->drug_os = 0;
                $addmainsafe->total = 0;
                $addmainsafe->safe_date = $this->date_stamp;
                $addmainsafe->save();
            }
        }
        else {
            $this->begin_safe = $init_rec->begin_safe;
            $this->del_tranfs = $init_rec->del_tranfs;
            $this->deposits = $init_rec->deposits;
            $this->store_tx = $init_rec->store_tx;
            $this->po_types = $init_rec->po_types;
            $this->tot_acct = $init_rec->tot_acct;
            $this->safe_cnt = $init_rec->safe_cnt;
            $this->safe_os = $init_rec->safe_os;
            $this->groc_os = $init_rec->groc_os;
            $this->drug_os = $init_rec->drug_os;
            $this->total = $init_rec->total;
        }
        
        $this->del_tranfs = UserHomeController::total_amt('T');
        $this->deposits = UserHomeController::total_amt('P');
        $this->store_tx = UserHomeController::total_amt('S');
        $this->po_types = UserHomeController::total_amt('O');
    }

    public function total_amt($code) {
        $tot = 0;
        $rs = DB::select('SELECT SUM(safeentry.item_amt) AS tot FROM safeentry, safeitems WHERE safeentry.item_id = safeitems.item_id AND safeitems.item_type = ? AND safeentry.entry_date = ?', array($code, $this->date_stamp));
        if ($rs[0]->tot == NULL) {
            $tot = 0;
        } else {
            $tot = $rs[0]->tot;
        }
        return $tot;
    }

    public function find_os() {
        $arrtot = UserHomeController::get_dogo();
        if ($arrtot['groctot'] == NULL) {
            $groctot = 0;
        } else {
            $groctot = $arrtot['groctot'];
        }
        if ($arrtot['drugtot'] == NULL) {
            $drugtot = 0;
        } else {
            $drugtot = $arrtot['drugtot'];
        }
        $this->groc_os = $groctot;
        $this->drug_os = $drugtot;
    }

    public function get_dogo() {
        $subtot1 = $subtot2 = $groctot = $drugtot = 0;

        $results = BookkeepRegisters::orderBy('reg_num')->get();
        foreach ($results as $row) {
            $rs = DB::select('SELECT SUM(entry.item_amt) AS subtot1 FROM entry, items WHERE entry.reg_num = ? AND entry.entry_date = ? AND items.item_id = entry.item_id AND items.item_type = "D"', array($row->reg_num, $this->date_stamp));
            if (is_null($rs[0]->subtot1)) {
                $rs[0]->subtot1 = 0;
            }
            $subtot1 = $rs[0]->subtot1;

            $rs = DB::select('SELECT SUM(entry.item_amt) AS subtot2 FROM entry, items WHERE entry.reg_num = ? AND entry.entry_date = ? AND items.item_id = entry.item_id AND items.item_type = "C"', array($row->reg_num, $this->date_stamp));
            if (is_null($rs[0]->subtot2)) {
                $rs[0]->subtot2 = 0;
            }
            $subtot2 = $rs[0]->subtot2;

            if ($row->reg_type == 'G') {
                $groctot = $groctot + ($subtot1 - $subtot2);
            } else if ($row->reg_type == 'D') {
                $drugtot = $drugtot + ($subtot1 - $subtot2);
            }
            $arrtot['groctot'] = $groctot;
            $arrtot['drugtot'] = $drugtot;
            $subtot1 = $subtot2 = 0;
        }
        return($arrtot);
    }

    public function calc_totals() {
        $this->tot_acct = $this->begin_safe + $this->del_tranfs + $this->deposits + $this->store_tx + $this->po_types;
        $this->safe_os = $this->safe_cnt - $this->tot_acct;
        $this->total = $this->safe_os + $this->groc_os + $this->drug_os;
    }

    public function populate_safe_array($rs, $date_stamp, $for_mgmt='') {
        if($for_mgmt == '') {
            $for_mgmt = 'dl';
        }
        $cntrs = count($rs);
        //print_r($rs);
        for($i=0; $i<$cntrs; $i++) {
            if($rs[$i]->item_amt == null || $rs[$i]->item_amt == ''){
                //echo '<br />'.$i.' == '.$rs[$i]->item_amt; continue;
                if($for_mgmt == 'dl') {
                    $sfentryrs = DB::select('SELECT * from safeentry WHERE safeentry.entry_date = ? AND safeentry.item_id = ?', array($date_stamp,$rs[$i]->item_id));
                    if(count($sfentryrs) < 1) {
                        $bse = new BookkeepSafeentry;
                        $bse->item_id = $rs[$i]->item_id;
                        $bse->item_amt = $rs[$i]->item_amt;
                        $bse->source_id = 'mktmgr';
                        $bse->entry_date = $date_stamp;
                        $bse->save();
                    }
                }
            }
        }
        //exit;
    }
    
    public function postBookmngmntDailyDetailsAcc() {
        $safe_date = Input::get('safe_date');
        foreach($_POST['res'] as $k => $v) {
            $queryupdate=DB::update('update safeentry
                SET safeentry.item_amt='.$v * 100 .', safeentry.source_id="mktmgr"
                WHERE safeentry.item_id = "'.$k.'"
                AND safeentry.entry_date = "'.$safe_date.'"');
        }
        return Redirect::route('mktmgr-post-bookmngmntdaily',['dateofinfo' => $safe_date]);
    }
    
    public function postBookmngmntdailymgrok() {
        $safe_date = Input::get('safe_date');

        $prev_date = date('Y-m-d', strtotime($safe_date .' -1 day'));
       // echo $prev_date;exit();
        $this->successMsg='';
        
        #check if the previous day has been approved
        $chkprv = DB::select('select max(mgmt.date_stamp) as lastApprovalDate from mgmt where mgmt.date_stamp <= ?', array($safe_date));
        //echo $safe_date; exit()	;
        //echo '<pre>'. print_r($chkprv).'<br/>'; exit()	;
        if($safe_date == $chkprv[0]->lastApprovalDate) {
            $this->errMsg = "ERROR".$safe_date."  HAS ALREADY BEEN APPROVED";
            //return false;
        } elseif($prev_date != $chkprv[0]->lastApprovalDate) {
              $this->errMsg = "Need to approve ".date('m/d/Y', strtotime($prev_date)) . " first.<br />".date('m/d/Y', strtotime($safe_date))." Not Approved.";
              //return false;
            
        }
        
        if($this->errMsg == '') {
            $mgr_apv = Mgmt::where('date_stamp',$safe_date)->first();
            if(is_null($mgr_apv)) {
                $mgmt = new Mgmt;
                $mgmt->date_stamp = $safe_date;
                $mgmt->source_id = 'mktmgr';
                $mgmt->save();
                $this->successMsg = "THE DAILYS HAVE BEEN APPROVED FOR ".date('m/d/Y', strtotime($safe_date));
                //return false;
            }
            else {
                $this->errMsg = date('m/d/Y', strtotime($safe_date))." HAS ALREADY BEEN APPROVED";
                //return false;
            }
        }
        
        
        if($this->errMsg != '') {
            Session::flash('alert-danger', $this->errMsg);
        }
        if($this->successMsg != '') {
            Session::flash('alert-success', $this->successMsg);
        }
        return Redirect::route('mktmgr-post-bookmngmntdaily',['dateofinfo' => $safe_date]);
        
        
    }
    
    public function wk_init_rec() {
        $this->wk_recap_date;
        $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($this->wk_recap_date)));
        
        $q_net_safe = DB::select('SELECT SUM(recapmain.dept_sales) 
            AS net_safe
            FROM recapmain,recapitems
            WHERE recapmain.wk_end_date = ?
            AND recapitems.item_type IN ("I")
            AND recapmain.dept_id = recapitems.item_id', array($this->wk_recap_date));
        if ($q_net_safe[0]->net_safe == NULL) {
            $this->wk_net_safe = 0;
        } else {
            $this->wk_net_safe = $q_net_safe[0]->net_safe;
        }

        $q_dept_sales = DB::select('SELECT SUM(recapmain.dept_sales) 
            AS dept_sales
            FROM recapmain,recapitems
            WHERE recapmain.wk_end_date = ?
            AND recapitems.item_type IN ("M","A","J")
            AND recapmain.dept_id = recapitems.item_id', array($this->wk_recap_date));
            
        if ($q_dept_sales[0]->dept_sales == NULL) {
            $this->errMsg = "RECAP NOT FOUND FOR THIS WEEK ENDING DATE";
            return false;
        } else {
            $this->wk_dept_sales = $q_dept_sales[0]->dept_sales;
        }

        $q_deposits = DB::select('SELECT SUM(recap.item_amt) 
            AS deposits
            FROM recap,safeitems
            WHERE recap.wk_end_date >= ?
            AND recap.wk_end_date <= ?
            AND safeitems.item_type  = "P"
            AND recap.item_id  = safeitems.item_id', array($this->wk_begin_date, $this->wk_recap_date));
        if ($q_deposits[0]->deposits == NULL) {
            $this->wk_deposits = 0;
        } else {
            $this->wk_deposits = $q_deposits[0]->deposits;
        }

        $q_del_tranfs = DB::select('SELECT SUM(recap.item_amt) 
            AS del_tranfs
            FROM recap,recapitems
            WHERE recap.wk_end_date >= ?
            AND recap.wk_end_date <= ?
            AND recapitems.item_type  = "T"
            AND recap.item_id  = recapitems.item_id', array($this->wk_begin_date, $this->wk_recap_date));
        if ($q_del_tranfs[0]->del_tranfs == NULL) {
            $this->wk_del_tranfs = 0;
        } else {
            $this->wk_del_tranfs = $q_del_tranfs[0]->del_tranfs;
        }

        $q_store_tx = DB::select('SELECT SUM(recap.item_amt) 
            AS store_tx
            FROM recap,recapitems
            WHERE recap.wk_end_date = ?
            AND recapitems.item_type  = "S"
            AND recapitems.item_id  NOT IN ("RR","LR","TD")
            AND recap.item_id  = recapitems.item_id', array($this->wk_recap_date));
        if ($q_store_tx[0]->store_tx == NULL) {
            $this->wk_store_tx = 0;
        } else {
            $this->wk_store_tx = $q_store_tx[0]->store_tx;
        }

        $q_po_types = DB::select('SELECT SUM(recap.item_amt) 
            AS po_types
            FROM recap,recapitems
            WHERE recap.wk_end_date = ?
            AND recapitems.item_type  = "O"
            AND recap.item_id  = recapitems.item_id', array($this->wk_recap_date));
        if ($q_po_types[0]->po_types == NULL) {
            $this->wk_po_types = 0;
        } else {
            $this->wk_po_types = $q_po_types[0]->po_types;
        }

        $this->wk_summary = $this->wk_dept_sales +
                            $this->wk_net_safe +
                            $this->wk_deposits +
                            $this->wk_del_tranfs +
                            $this->wk_store_tx +
                            $this->wk_po_types;
    }
     
     
    
    public function wk_approve(){
        $dt = Input::get('recap_date');
        $this->wk_recap_date = $dt;
        $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($dt)));
        
        $retVal = 'false';
        if(UserHomeController::wk_isWeekSigned($dt) == 'true') {
            if(UserHomeController::wk_lock_files($dt) == 'true' && UserHomeController::wk_bothmgr_approved($dt) == 'true') {
                UserHomeController::wk_recaptrfjob();
                $retVal = 'true';
            }
        }
        if($this->errMsg != '') {
            Session::flash('alert-danger', $this->errMsg);
        }
        else {
            Session::flash('alert-success', 'Succesfully approved.');
        }
        return Redirect::route('mktmgr-post-bookmngmntweekly',['dateofinfo' => $dt]);
        //Redirect::back()->with('info',$this->errMsg);
    }
    
    public function wk_lock_files($dt) {
        $proceed = "Y";
        $retVal = 'true';
        if($this->wk_recap_date != null && $this->wk_recap_date != '') {
            if($user_id = "bookkeep" || $user_id = "strmngr" || $user_id = "drgmngr" || $user_id = "strdir" || $user_id = "bkpdir05" || $user_id = "bkpmkt" || $user_id = "bkpmkt05" || $user_id = "bkpdrg05") {
                $retVal = 'true';
            }
            else {
                $retVal = 'false';
            }
        }
        if($retVal == 'true') {
            if($proceed == 'y' || $proceed == 'Y') {
                $q_misc = DB::select('SELECT misc.groc_num
                    AS  str_num
                    FROM  misc', array());
                $end_date = $dt;
                $begin_date = $this->wk_begin_date;
                $retVal = UserHomeController::wk_mgr_ok($begin_date, $end_date);
            }
        }
        return $retVal;
    }
    
    public function wk_mgr_ok($begin_date, $end_date) {
        $retVal = 'true';
        for ($i=0; $i<6; $i++) {
            $next_date = date('Y-m-d', strtotime($begin_date .' +'.$i.' day'));
            $q_locks = DB::select('SELECT * from locks
                WHERE locks.date_stamp = ?', array($next_date));
            if(count($q_locks) < 0) {
                $addLocks = new BookkeepLocks;
                $addLocks->date_stamp = $next_date;
                $addLocks->user_id = 'bookkeep';
                $addLocks->save();
            }
            else {
                if($this->wk_recap_date != null && $this->wk_recap_date != '') {
                    $this->errMsg = "FILES HAVE ALREADY BEEN LOCKED FOR $next_date THRU $end_date";
                    $retVal = 'false';
                }
                break;
            }
        }
        if($this->wk_recap_date == null && $this->wk_recap_date == '') {
            $this->errMsg = "Files can no longer be updated for week ending $this->wk_recap_date";
            $retVal = 'false';
        }
        return $retVal;
    }
    
    public function wk_bothmgr_approved($recap_date) {
        $retVal = 'true';
        $q_locks = DB::select('SELECT COUNT(*) 
            AS recap_cnt
            FROM locks
            WHERE locks.date_stamp = ?', array($recap_date));
            
        if(count($q_locks) < 0) {
            $retVal = 'false';
        }
        return $retVal;
    }
    
    public function wk_isWeekSigned($dt){
        $sigCount = 0;
        
        $q_sig_cnt = DB::select('select count(*)
            AS sigCount
            from bkpsig
            where recap_date = ?', array($dt));
        $retVal= 'true';
        
        if($q_sig_cnt[0]->sigCount == 0) {
            if($this->wk_recap_date == null) {
                $retVal= 'false';
            }
            else {
                //UserHomeController::wk_print_recap();
            }
        }
        return $retVal;
    }
    
    public function wk_print_recap() {
        UserHomeController::wk_upd_bkpsig();
    }
    
    public function wk_upd_bkpsig() {
        
    }
    
    public function wk_recaptrfjob() {
        
    }
    
    public function wk_get_ov($dt) {
        $drgov = 0;
        $q = DB::select('SELECT recapmain.dept_sales
                AS grov
                FROM recapmain
                WHERE recapmain.dept_id = "GO"
                AND recapmain.wk_end_date = ?', array($dt));
        $retArr['drgov'] = ($drgov * -1) / 100;
        $retArr['grov'] = ($q[0]->grov * -1) / 100;
        return $retArr;
    }
    
    public function wk_dep_rpt($recap_date) {
        $this->wk_recap_date = $recap_date;
        $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($this->wk_recap_date)));
        UserHomeController::wk_init_maindep();
    }
    
    public function wk_dltrf_rpt($recap_date) {
        $this->wk_recap_date = $recap_date;
        $this->wk_begin_date = date('Y-m-d', strtotime('-6 days', strtotime($this->wk_recap_date)));
        UserHomeController::wk_init_maintrf();
    }
    
    public function wk_init_maindep() {
        $q1 = DB::select('SELECT sum(recap.item_amt)
                AS amt
                FROM recap
                WHERE recap.item_id IN ("C1","C2", "C3", "C4")
                AND recap.wk_end_date >= ? 
                AND recap.wk_end_date <= ?', array($this->wk_begin_date,$this->wk_recap_date));
        if($q1[0]->amt == null || $q1[0]->amt == '' || $q1[0]->amt == ' ') {
            $this->wk_maindep_currency = 0;
        }
        else {
            $this->wk_maindep_currency = $q1[0]->amt / 100;
        }

        $q2 = DB::select('SELECT sum(recap.item_amt)
                AS amt
                FROM recap
                WHERE recap.item_id IN ("K1","K2")
                AND recap.wk_end_date >= ? 
                AND recap.wk_end_date <= ?', array($this->wk_begin_date,$this->wk_recap_date));
        if($q2[0]->amt == null || $q2[0]->amt == '' || $q2[0]->amt == ' ') {
            $this->wk_maindep_checks = 0;
        }
        else {
            $this->wk_maindep_checks = $q2[0]->amt / 100;
        }

        $q3 = DB::select('SELECT sum(recap.item_amt)
                AS amt
                FROM recap
                WHERE recap.item_id IN ("CD")
                AND recap.wk_end_date >= ? 
                AND recap.wk_end_date <= ?', array($this->wk_begin_date,$this->wk_recap_date));
        if($q3[0]->amt == null || $q3[0]->amt == '' || $q3[0]->amt == ' ') {
            $this->wk_maindep_express_cred = 0;
        }
        else {
            $this->wk_maindep_express_cred = $q3[0]->amt / 100;
        }

        $q4 = DB::select('SELECT sum(recap.item_amt)
                AS amt
                FROM recap
                WHERE recap.item_id IN ("DD")
                AND recap.wk_end_date >= ? 
                AND recap.wk_end_date <= ?', array($this->wk_begin_date,$this->wk_recap_date));
        if($q4[0]->amt == null || $q4[0]->amt == '' || $q4[0]->amt == ' ') {
            $this->wk_maindep_express_deb = 0;
        }
        else {
            $this->wk_maindep_express_deb = $q4[0]->amt / 100;
        }
        $this->wk_maindep_total = $this->wk_maindep_currency + $this->wk_maindep_checks + $this->wk_maindep_express_cred + $this->wk_maindep_express_deb;
    }
    
    public function get_itemdesc($item_id){
        $data = Safeitems::where('item_id', $item_id)->select('item_desc')->first();
        return $data->item_desc;
    }
    
    public function get_weekday($ddate) {
        $dayofweek = date('D',strtotime($ddate));
        return $dayofweek;
    }
    
    public function wk_init_maintrf() {
        $q1 = DB::select('SELECT sum(recap.item_amt)
                AS amt
                FROM recap
                WHERE recap.item_id IN ("T1","T2", "T3", "T4", "T5", "T6", "T7")
                AND recap.wk_end_date >= ? 
                AND recap.wk_end_date <= ?', array($this->wk_begin_date,$this->wk_recap_date));
        if($q1[0]->amt == null || $q1[0]->amt == '' || $q1[0]->amt == ' ') {
            $this->wk_maintrf_tranfs = 0;
        }
        else {
            $this->wk_maintrf_tranfs = $q1[0]->amt / 100;
        }

        $q2 = DB::select('SELECT sum(recap.item_amt)
                AS amt
                FROM recap
                WHERE recap.item_id IN ("D1","D2", "D3", "D4", "D5","D6","D7")
                AND recap.wk_end_date >= ?
                AND recap.wk_end_date <= ?', array($this->wk_begin_date,$this->wk_recap_date));
        if($q2[0]->amt == null || $q2[0]->amt == '' || $q2[0]->amt == ' ') {
            $this->wk_maintrf_del = 0;
        }
        else {
            $this->wk_maintrf_del = $q2[0]->amt / 100;
        }
        $this->wk_maintrf_total = $this->wk_maintrf_tranfs + $this->wk_maintrf_del;
    }
  
    public function PostBookwklysalesView()
    {
    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
    	$validator = Validator::make(Input::all(),
            array(                   
                   'dateofinfo'=>'required|date|weekenddatecheck',
                ),array(
                	'dateofinfo.required' => 'This date is required'
                ));
        if($validator->fails())
        {
            //return View::make('mktmgr.bookwklysales'); 
            return Redirect::route('mktmgr-bookwklysales')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
          $dateofinfo = Input::get('dateofinfo');
          $infodate = date("m/d/Y", strtotime(Input::get('dateofinfo')));
          $cust_cnt=$this->get_val_cust_cnt(Input::get('dateofinfo'));
         // echo $cust_cnt;exit;
          $comp_coupon=$this->get_val_cust_cntpc(Input::get('dateofinfo'));
          $totentry = $this->totsales_entry(Input::get('dateofinfo'));
          $totentrya = $this->totsales_entrya(Input::get('dateofinfo'));				
          $sumtot = $totentry + $totentrya;
          $depttot = $this->getdepttotsales(Input::get('dateofinfo'));
          $depttot1 = $this->getdepttotsales1(Input::get('dateofinfo'));
          $depttot = array_merge($depttot,$depttot1);
          $adjsales = $this->getadjustmentsales(Input::get('dateofinfo'));
          $adjsalesinserted = $this->getadjustmentsaleschkinsert(Input::get('dateofinfo'));
          $per = $this->get_per(Input::get('dateofinfo'));
        /*if($depttot)
        {
        return View::make('mktmgr.bookwklysalesdetails_update', compact('depttot','adjsales','infodate'));
        }*/
		return View::make('mktmgr.bookwklysalesdetails', compact('depttot','adjsalesinserted','adjsales','infodate',
						  'cust_cnt','sumtot','comp_coupon','per','dateofinfo'));
        }
    }

    public function postBookwklysdetailssaveAdj()
    {
    	
    }


    public function get_val_cust_cnt($sel_date)
    {
      $sql=DB::select('select cust_cnt as custcnt from recapmain WHERE dept_id = "CC"
       				   AND wk_end_date = "'.$sel_date.'"');

      if(count($sql) !=0){ $resultArray = json_decode(json_encode($sql), true);
      			//echo $resultArray[0]['custcnt'];exit; 
      				return $resultArray[0]['custcnt'];} 
      else { return count($sql);}
   }

   public function get_val_cust_cntpc($sel_date)
    {
      $sql=DB::select('select cust_cnt as custcnt from recapmain WHERE dept_id = "PC"
       				   AND wk_end_date = "'.$sel_date.'"');
      if(count($sql) !=0){
       			    $resultArray = json_decode(json_encode($sql), true); 
      				return $resultArray[0]['custcnt'];} 
      else { return count($sql);}
   }

   public function totsales_entry($sel_date)
   {
   	 $sql=DB::select('select SUM(recapmain.dept_sales)/100 as saletot from recapmain,recapitems 
   	 	                WHERE recapitems.item_id = recapmain.dept_id
     				  AND recapitems.item_type IN ("M","J")
       				  AND recapmain.dept_id NOT IN ("CC")	
       				  AND recapmain.wk_end_date = "'.$sel_date.'"');
   	 if(count($sql) != 0){
   	 					  $resultArray = json_decode(json_encode($sql), true); 
   	 	                  return $resultArray[0]['saletot'];
   	 	                 } else{return $sql=0.00;}
   }

   public function totsales_entrya($sel_date)
   {
   	 $sql=DB::select('select sum(recapmain.dept_sales)/100 as saletot from recapmain,recapitems
   	 	                where recapitems.item_id = recapmain.dept_id and recapitems.item_type IN ("A")	
       				  								 and recapmain.wk_end_date = "'.$sel_date.'"');
   	 if(count($sql) != 0){ $resultArray = json_decode(json_encode($sql), true); 
   	 	                  return $resultArray[0]['saletot'];
   	 	                 } else{return $sql=0.00;}
   }


    public function getdepttotsales($seldate)
    {

  //   	echo 'SELECT i.item_id,i.acct_num, i.item_desc, m.dept_sales as gettot, m.cust_cnt  as custcnt
	 //   FROM recapitems i left outer join recapmain m
	 //    on m.dept_id = i.item_id where i.item_type = "M"
	 //     AND i.item_id NOT IN ("CC")
	 //    and m.wk_end_date = "'.$seldate.'" and i.disable = "N" 
		// UNION
		// SELECT i.item_id,i.acct_num, i.item_desc, m.dept_sales, m.cust_cnt 
	 //   FROM recapitems i RIGHT outer join recapmain m
	 //    on m.dept_id = i.item_id where i.item_type = "M"
	 //     AND i.item_id NOT IN ("CC")
	 //    and m.wk_end_date = "'.$seldate.'"
	 //    and i.disable = "N" 
		// ORDER BY acct_num ';exit;
	    	$depttot = DB::select('SELECT i.item_id,i.acct_num, i.item_desc, m.dept_sales as gettot, m.cust_cnt  as custcnt
	   FROM recapitems i left outer join recapmain m
	    on m.dept_id = i.item_id where i.item_type = "M"
	     AND i.item_id NOT IN ("CC")
	    and m.wk_end_date = "'.$seldate.'" and i.disable = "N" 
		UNION
		SELECT i.item_id,i.acct_num, i.item_desc, m.dept_sales, m.cust_cnt 
	   FROM recapitems i RIGHT outer join recapmain m
	    on m.dept_id = i.item_id where i.item_type = "M"
	     AND i.item_id NOT IN ("CC")
	    and m.wk_end_date = "'.$seldate.'"
	    and i.disable = "N" 
		ORDER BY acct_num ');		

    	return $depttot;
    	
    }
    public function getdepttotsales1($seldate)
    {
    	/*echo "SELECT i.item_id,i.acct_num,i.item_desc,m.dept_sales/100 as gettot, 
    		m.cust_cnt as custcnt FROM recapitems i, recapmain m 
    		WHERE i.item_type = 'J' 
    		and m.dept_id = i.item_id
    		and i.disable = 'N' 
    		and m.wk_end_date = '.$seldate.'     		
    		ORDER BY i.acct_num";exit();*/
    	$depttot1 = DB::select('SELECT i.item_id,i.acct_num,i.item_desc,m.dept_sales/100 as gettot, 
    		m.cust_cnt as custcnt FROM recapitems i, recapmain m 
    		WHERE i.item_type = "J" 
    		and m.dept_id = i.item_id
    		and i.disable = "N" 
    		and m.wk_end_date = "'.$seldate.'"     		
    		ORDER BY i.acct_num');	
    	
    	
    		return $depttot1;
    	
    }

    public function getadjustmentsales($seldate)
    {
    	return $depttot = DB::select('SELECT i.*, m.*  from recapitems i, recapmain m WHERE m.dept_id = i.item_id GROUP BY i.item_desc ORDER BY i.acct_num');	
    }

    public function getadjustmentsaleschkinsert($seldate)
    {
        return $depttot = DB::select('SELECT i.item_id,i.acct_num, i.item_desc, m.dept_sales,m.cust_cnt
	                                 from recapitems i, recapmain m WHERE m.dept_id = i.item_id
	                                 AND i.item_type = "A"
	                                 AND i.item_id IN ("IM","XM","QM","KM","TM","EM","TX")
	                                 AND i.disable = "N"
	                                 AND m.wk_end_date = "'.$seldate.'"
	                                 ORDER BY i.acct_num');
	     /*if(count($depttot) != 0)
	     { return $depttot;
	     }                 
	     else
	     {
	    	return $depttot = DB::select('SELECT i.*, m.* FROM recapitems i, recapmain m 
	    	                              WHERE m.dept_id = i.item_id
                                          GROUP BY i.item_desc ORDER BY i.acct_num');	 	
	     }    */       	
    }

    public function get_per($sel_date)
    {
    	$sql=DB::select("SELECT cur_fiscal_per as per from misc WHERE cur_per_start_date <= '".$sel_date."'
    	      				    AND cur_per_end_date >= '".$sel_date."'");
    	if(count($sql)==0)
    	{
    		$sql=DB::select("SELECT fiscal_period as per from fiscdates WHERE fiscal_per_sdate <= '".$sel_date."'
    	      				    AND fiscal_per_edate >= '".$sel_date."'");	
    		if(count($sql) == 0)
    		{
    			return $per=0;
    		}
    		else
    		{
    		  $resultArray = json_decode(json_encode($sql), true);
      		  return $per=$resultArray[0]['per'];
    		}
    	}
      	else
      	{
      		$resultArray = json_decode(json_encode($sql), true);
      		return $per=$resultArray[0]['per'];
      	}
    }

    public function postBookWklysdetailSave()
    {
    	$dateofinfo = date("Y-m-d", strtotime(Input::get('dateofinfo')));
        $condeitionset="dept_id='CC' AND wk_end_date='".$dateofinfo."'";
        $condeitionset2="dept_id='PC' AND wk_end_date='".$dateofinfo."'";
        $customercnt=Input::get('customercnt');
        $ccoupon=Input::get('comptitorcoupon');
        $totsale=Input::get('totsale');
        $per=Input::get('perval');
        $resval=$this->chkdata("recapmain", $condeitionset); $resval2=$this->chkdata("recapmain", $condeitionset2);
        if($resval == 'make-insert')
        {
         $sql=DB::select('INSERT INTO recapmain(dept_id, cust_cnt, wk_end_date, period)VALUES("CC","'.$customercnt.'","'.$dateofinfo.'","'.$per.'")');
        }
        else
        {
         $sql=DB::select('UPDATE recapmain SET cust_cnt="'.$customercnt.'", period="'.$per.'" WHERE dept_id="CC" AND wk_end_date="'.$dateofinfo.'"');	
        }
        if($resval2 == 'make-insert')
        {
         $sql=DB::select('INSERT INTO recapmain(dept_id, cust_cnt, wk_end_date, period)VALUES("CC","'.$ccoupon.'","'.$dateofinfo.'","'.$per.'")');
        }
        else
        {
         $sql=DB::select('UPDATE recapmain SET cust_cnt="'.$ccoupon.'", period="'.$per.'" WHERE dept_id="CC" AND wk_end_date="'.$dateofinfo.'"');	
        }
    }

    public function chkdata($tabname, $condition)
    {
      $sql="SELECT * from".$tabname."WHERE".$condition;
      if(count($sql) == 0)
      { 
      	$rnt='make-insert';

      	 //INSERT INTO recapmain()VALUES ("CC",0, mainsale_rec.cust_cnt,sales_date,per)
      }
      else
      {
      	$rnt='make-update';
      } 
      return $rnt;
    }

    public function PostBookwklyRecap2()
    {

    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
    	$validator = Validator::make(Input::all(),
            array(                   
                   'dateofinfo'=>'required|date|weekenddatecheck',
                ));
        if($validator->fails())
        {
            //return View::make('mktmgr.bookwklysales'); 
            return Redirect::route('mktmgr-bookwklyrecap')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {   $dateofinfo = Input::get('dateofinfo');
         	$sql=DB::select('select COUNT(*) AS lck from locks where locks.date_stamp = "'.$dateofinfo.'"');
         	if(count($sql) <> 0)
         	{
         		$sql=DB::select('select l.game_no, l.lottery_date, l.end_inv/100, l.ticket_value/100, 0
								from lottery l, (select game_no, max(lottery_date) as maxLottDate from lottery group by game_no) t
								where l.end_inv > 0 and  l.game_no = t.game_no and  l.lottery_date = t.maxLottDate
								order by game_no');
         		if(count($sql) > 0)
         		{
         			
         		}
         		else
         		{
         			
         		}
         	}
         	else
         	{
         	  Session::flash('alert-info','FILES HAVE BEEN LOCKED, CAN NOT PROCESS THIS RECAP REQUEST'); 
         	  return View::make('mktmgr.bookwklyrecap'); 
         	}

        }
    }
    
    public function convertDateToYMD($date) {
    	$date = preg_replace('/\D/','/',$date);
    	return date('Y-m-d',strtotime($date));
    }

    public function convertDateToMDY($date) {
    	return date("m/d/Y", strtotime($date));
    }
    
    public function PostBookwklyRecap()
    {


    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {     
    			

              $weekday = date('N', strtotime($value)); 
                  
              if($weekday==6){
              	$GLOBALS['val'] = $value;
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
    	$validator = Validator::make(Input::all(),
            array(                   
                   'dateofinfo'=>'required|date|weekenddatecheck',
                ));
        if($validator->fails())
        {
            //return View::make('mktmgr.bookwklysales'); 
            return Redirect::route('mktmgr-bookwklyrecap')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {   
        	
        	//$weekday = date('N', strtotime($GLOBALS['val'])); 
        	
        	//$tocurrentdate=($weekday+1);
        	
			//$pdate=strtotime(date('Y-m-d') . -$tocurrentdate."days");
			//echo $pdate;exit();
			$calcdate=$GLOBALS['val'];

			
         	$sql=DB::select('SELECT COUNT(*) AS lck from locks WHERE locks.date_stamp = "'.$calcdate.'"');
         	if(count($sql) != 2)
         	{
         		$msg=$this->checkPostageInv($calcdate);
         		//echo $msg;exit;
         		if($msg !='')
         		{
         			
         			Session::flash('alert-warning',$msg); return View::make('mktmgr.bookwklyrecap');
         		}	
         		
         		$lotterydata=$this->checkLotteryInv($calcdate);
         		if(count($lotterydata) > 0)
											{
         			//$chkdate=checkPostageInv($dateofinfo);
         		  return View::make('mktmgr.bookwklyrecaplotterydate',compact('lotterydata')); 	

         		}
         	}
         	else
         	{
         	  Session::flash('alert-info','FILES HAVE BEEN LOCKED, CAN NOT PROCESS THIS RECAP REQUEST'); 
         	  return View::make('mktmgr.bookwklyrecap'); 
         	}

        }
    }
    public function calcLottery()
    {
    	$calcdate = Input::get('date');
    	//echo $calcdate;
    	$maxLottDate=DB::select('select game_no, max(lottery_date) as maxLottDate from lottery group by game_no');
    	$maxLottDate = json_decode(json_encode($maxLottDate), true);
    	echo '<pre>';print_r($maxLottDate); 

    }
    public function checkPostageInv($calcdate)
    {
    	
    	$sql=DB::select('SELECT end_inv_safe+end_inv_tills+end_inv_ca as endingInv from postage WHERE ps_date="'.$calcdate.'"');

    	if(count($sql) == 0)
    	{   $convert_date = date("m/d/Y", strtotime($calcdate));

    		/*
    		$msg="The POSTAGE ENDING INVENTORY for <span id='calcdate' style='color:red;'>".$convert_date."</span> is zero.
	      			  Please continue if you know this is correct, otherwise, ENTER THE CORRECT ENDING BALANCE AND RECALCULATE THE RECAP <br />Click to <a href='calclottery?date=$convert_date' id='cont' style='color:red;'>Continue</a>&nbsp;&nbsp <a href='#' id='cancelrecap' style='color:red;'>Cancel</a>";
	      			  */
	      			  $msg="The POSTAGE ENDING INVENTORY for <span id='calcdate' style='color:red;'>".$convert_date."</span> is zero.
	      			  Please continue if you know this is correct, otherwise, ENTER THE CORRECT ENDING BALANCE AND RECALCULATE THE RECAP <br />Click to <a href='#' id='cont' style='color:red;'>Continue</a>&nbsp;&nbsp <a href='#' id='cancelrecap' style='color:red;'>Cancel</a>";
         //Session::flash('alert-info',$msg); 
         return $msg;
         //return View::make('mktmgr.bookwklyrecap');
    	}
    	else
    	{
    		$resultArray = json_decode(json_encode($sql), true);
	      	$tot=$resultArray[0]['tot'];
	      	if($tot ==0)
	      	{   $convert_date = date("m-d-Y", strtotime($calcdate));
	      		$msg="The POSTAGE ENDING INVENTORY for ,<span id='calcdate' style='color:red;'>".$convert_date."</span>, is zero.
	      			  Please continue if you know this is correct, otherwise,ENTER THE CORRECT ENDING BALANCE AND RECALCULATE THE RECAP <br />Click to <a href='#' id='cont' style='color:red;'>Continue</a>";
         		return $msg;
         		//Session::flash('alert-info',$msg); 
         		//return View::make('mktmgr.bookwklyrecap');	    
	      	}	
    	}
    	$message = 'FINISHED PROCESSING WEEKLY RECAP';
    	return $message;
    	
    }

    public function checkLotteryInv($calcdate)
    {
    	$sql=DB::select('select l.game_no as gameno, l.lottery_date as lotterydate, l.end_inv/100 as endinv, 
    		             l.ticket_value/100 as ticketvalue, 0 as inhand
    					 from lottery l, (select game_no, max(lottery_date) as maxLottDate from lottery group by game_no) t
						 where l.end_inv > 0 and  l.game_no = t.game_no and  l.lottery_date = t.maxLottDate and  l.lottery_date != "'.$calcdate.'" order by gameno');
    	return $sql;
    }
    
    public function runProcessWeekly()
    {
    	$calcdate = date("Y-d-m", strtotime(Input::get('calcdate')));
    	$data=$this->start_calcrecap($calcdate);
		$this->calc_deposits($calcdate);
		$this->calc_transf($calcdate);
		$this->calc_storetx($calcdate);
		$this->calc_potypes($calcdate);

		//$msg="FINISHED PROCESSING WEEKLY RECAP  For .".$calcdate;
		$msg='<p class="alert alert-success">FINISHED PROCESSING WEEKLY RECAP  For. '.$calcdate.'<a href="#" class="close" data-dismiss="alert" aria-label="close">Ã—</a></p>';
		return $msg;
    }

    public function start_calcrecap($calcdate)
    {
      $groc_os=0; $drug_os=0; $safe_os=0;
      $startdate = date('Y-m-d',strtotime($calcdate . "-6 days")); //exit;
      $sql=DB::select('SELECT SUM(groc_os) as groc_os, SUM(drug_os) as drug_os, SUM(safe_os) as safe_os
					   FROM mainsafe WHERE safe_date >= "'.$startdate.'" AND safe_date <= "'.$calcdate.'"');
      if(count($sql) > 0)
      {
      	$resultArray = json_decode(json_encode($sql), true); 
      	$groc_os=$resultArray[0]['groc_os'];
      	$safe_os=$resultArray[0]['safe_os'];
      	$groc_os=($groc_os + $safe_os) * (-1);
      	$drug_os=$resultArray[0]['drug_os']* (-1);
      }
      
      $sql=DB::select('SELECT * from recapmain WHERE dept_id = "GO" AND wk_end_date ="'.$calcdate.'"');
      if(count($sql) == 0)
      {
      	 $per=$this->get_per($calcdate);
      	 $sql = DB::select('INSERT INTO recapmain(`dept_id`, `dept_sales`, `cust_cnt`, `wk_end_date`, `period`)
			      VALUES("GO","'.$groc_os.'","0","'.$calcdate.'", "'.$per.'")');
      }
      else
      {
      	$sql = DB::select('UPDATE recapmain SET dept_sales="'.$groc_os.'" WHERE dept_id="GO" AND wk_end_date="'.$calcdate.'"');
      }

      $sql=DB::select('SELECT begin_safe * -1 as bs, safe_cnt as es from mainsafe WHERE safe_date ="'.$startdate.'"');
   	  if(count($sql) == 0)
   	  {$bs=0; $es=0;}	
   	  else{
   	  	 $resultArray = json_decode(json_encode($sql), true); 
   	  	 $bs=$resultArray[0]['bs']; $es=$resultArray[0]['es'];
   	  }   

   	  $sql = DB::select('SELECT * from recapmain WHERE dept_id = "ES" AND wk_end_date = "'.$calcdate.'"');
   	  if(count($sql) == 0)
   	  {
   	  	 $sql = DB::select('INSERT INTO recapmain(`dept_id`, `dept_sales`, `cust_cnt`, `wk_end_date`, `period`)
			      VALUES("ES","'.$es.'","0","'.$calcdate.'", "'.$per.'")');
   	  }
   	  else
   	  {
   	  	$sql = DB::select('UPDATE recapmain SET dept_sales="'.$es.'" WHERE dept_id="ES" AND wk_end_date="'.$calcdate.'"');
   	  }

   	  $sql = DB::select('SELECT * from recapmain WHERE dept_id = "BS" AND wk_end_date = "'.$calcdate.'"');
   	  if(count($sql) == 0)
   	  {
   	  	 $sql = DB::select('INSERT INTO recapmain(`dept_id`, `dept_sales`, `cust_cnt`, `wk_end_date`, `period`)
			      VALUES("BS","'.$bs.'","0","'.$calcdate.'", "'.$per.'")');
   	  }
   	  else
   	  {
   	  	$sql = DB::select('UPDATE recapmain SET dept_sales="'.$bs.'" WHERE dept_id="BS" AND wk_end_date="'.$calcdate.'"');
   	  }
      return $tot = $drug_os + $groc_os + $es + $bs;
    }

    public function calc_deposits($calcdate)
    {
    	$startdate = date('Y-m-d',strtotime($calcdate . "-6 days"));
    	$sql=DB::select('SELECT * from safeentry,safeitems WHERE safeentry.entry_date >= "'.$startdate.'"
    		             AND safeentry.entry_date <= "'.$calcdate.'"
					     AND safeitems.item_id <> "#C"
					     AND safeitems.item_id = safeentry.item_id
					     AND safeitems.item_type = "P"
					     ORDER BY safeentry.entry_date');
    	$arrayResult = json_decode(json_encode($sql), true);
    	foreach ($arrayResult as $key) {
    		  $itemid = $key['item_id'];
	 		  $itemamt=$key['item_amt']* (-1);
	 		  $sql=DB::select('SELECT * from recap WHERE item_id = "'.$itemid.'" AND wk_end_date ="'.$calcdate.'"');
	 		  if(count($sql) == 0)
	 		  {
	 		  	 $sql = DB::select('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
			      VALUES("'.$key['item_id'].'","'.$itemamt.'", "'.$calcdate.'")');
		   	  }
		   	  else
		   	  {
		   	 $sql = DB::select('UPDATE recap SET item_amt="'.$itemamt.'" WHERE item_id="'.$key['item_id'].'" AND wk_end_date="'.$calcdate.'"');
		   	  }
    	}
    }

    public function calc_transf($calcdate)
    {
      $startdate = date('Y-m-d',strtotime($calcdate . "-6 days"));
    	$sql=DB::select('SELECT * from safeentry,safeitems WHERE safeentry.entry_date >= "'.$startdate.'"
    		             AND safeentry.entry_date <= "'.$calcdate.'"
					     AND safeitems.item_id = "T"
					     AND safeitems.item_id = safeentry.item_id
					     ORDER BY safeentry.entry_date');
    	$arrayResult = json_decode(json_encode($sql), true);
    	foreach ($arrayResult as $key) {
    		  $itemid = $key['item_id'];
	 		  $itemamt=$key['item_amt']* (-1);
	 		  $sql=DB::select('SELECT * from recap WHERE item_id = "'.$itemid.'" AND wk_end_date ="'.$calcdate.'"');
	 		  if(count($sql) == 0)
	 		  {
	 		  	 $sql = DB::select('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
			      VALUES("'.$key['item_id'].'","'.$itemamt.'", "'.$calcdate.'")');
		   	  }
		   	  else
		   	  {
		   	 $sql = DB::select('UPDATE recap SET item_amt="'.$itemamt.'" WHERE item_id="'.$key['item_id'].'" AND wk_end_date="'.$calcdate.'"');
		   	  }
    	}
    	return 1;
    }

    public function calc_storetx($calcdate)
    {
    	$startdate = date('Y-m-d',strtotime($calcdate . "-6 days"));
    	$sql=DB::select('SELECT SUM(safeentry.item_amt), safeentry.item_id FROM safeentry, safeitems
					     WHERE safeentry.entry_date >= "'.$startdate.'"
					     AND safeentry.entry_date <= "'.$calcdate.'"
					     AND safeitems.item_type = "S"
					     AND safeitems.item_id NOT IN ("PS","RR","LR","TD")
					     AND safeitems.item_id = safeentry.item_id
					     GROUP BY safeentry.item_id');
    	$arrayResult = json_decode(json_encode($sql), true);
    	foreach ($arrayResult as $key) {
    		$itemid = $key['item_id'];
	 		$itemamt=$key['item_amt']* (-1);
	 		$sql=DB::select('SELECT * from recap WHERE item_id = "'.$itemid.'" AND wk_end_date ="'.$calcdate.'"');
	 		  if(count($sql) == 0)
	 		  {
	 		  	 $sql = DB::select('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
			      VALUES("'.$key['item_id'].'","'.$itemamt.'", "'.$calcdate.'")');
		   	  }
		   	  else
		   	  {
		   	 $sql = DB::select('UPDATE recap SET item_amt="'.$itemamt.'" WHERE item_id="'.$key['item_id'].'" AND wk_end_date="'.$calcdate.'"');
		   	  }
		}
		$itemid = "L1";

		$sql=DB::select('SELECT SUM(lottery.deliveries) as item_amt from lottery
				    	 WHERE lottery.lottery_date <= "'.$startdate.'"
				         AND lottery.lottery_date >= "'.$calcdate.'"');
		$arrayResult = json_decode(json_encode($sql), true);
    	foreach ($arrayResult as $key) {
	 		$itemamt=$key['item_amt']* (-1);
	 		$sql=DB::select('SELECT * from recap WHERE item_id = "'.$itemid.'" AND wk_end_date ="'.$calcdate.'"');
	 		  if(count($sql) == 0)
	 		  {
	 		  	 $sql = DB::select('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
			      VALUES("'.$itemid.'","'.$itemamt.'", "'.$calcdate.'")');
		   	  }
		   	  else
		   	  {
		   	 $sql = DB::select('UPDATE recap SET item_amt="'.$itemamt.'" WHERE item_id="'.$itemid.'" AND wk_end_date="'.$calcdate.'"');
		   	  }
		}

		$itemid = "L2";

		$sql=DB::select('SELECT SUM(lottery.deliveries) as item_amt from lottery
				    	 WHERE lottery.lottery_date <= "'.$startdate.'"
				         AND lottery.lottery_date >= "'.$calcdate.'"');
		$arrayResult = json_decode(json_encode($sql), true);
    	foreach ($arrayResult as $key) {
	 		$itemamt=$key['item_amt']* (-1);
	 		$sql=DB::select('SELECT * from recap WHERE item_id = "'.$itemid.'" AND wk_end_date ="'.$calcdate.'"');
	 		  if(count($sql) == 0)
	 		  {
	 		  	 $sql = DB::select('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
			      VALUES("'.$itemid.'","'.$itemamt.'", "'.$calcdate.'")');
		   	  }
		   	  else
		   	  {
		   	 $sql = DB::select('UPDATE recap SET item_amt="'.$itemamt.'" WHERE item_id="'.$itemid.'" AND wk_end_date="'.$calcdate.'"');
		   	  }
		}
		return true;
    }

    public function calc_potypes($calcdate)
    {
    	$startdate = date('Y-m-d',strtotime($calcdate . "-6 days"));
    	$sql=DB::select('SELECT SUM(safeentry.item_amt), safeentry.item_id from safeentry, safeitems
    			   WHERE safeentry.entry_date >= "'.$startdate.'"
			       AND safeentry.entry_date <= "'.$calcdate.'"
			       AND safeitems.item_type = "O"
			       AND safeitems.item_id = safeentry.item_id
			       GROUP BY safeentry.item_id');
    	$arrayResult = json_decode(json_encode($sql), true);
    	if(count($sql) != 0)
    	{
    		foreach ($arrayResult as $key) {
    			$itemamt=$key['item_amt']* (-1); $itemid=$key['item_id'];
    			$this->datapush($itemamt, $calcdate, $itemid);
    		}
    	}
    	$itemid = "LO";
    	$sql=DB::select('SELECT SUM(deliveries) as item_amt from lottery
    					 WHERE lottery_date <= "'.$startdate.'"
					     AND lottery_date >= "'.$calcdate.'"');
    	$arrayResult = json_decode(json_encode($sql), true);
    	if(count($sql) != 0)
    	{
    		foreach ($arrayResult as $key) {
    			$itemamt=$key['item_amt']* (-1); //$itemid=$key['item_id'];
    			$this->datapush($itemamt, $calcdate, $itemid);
    		}
    	}
    	$itemid = "EG";
    	$sql=DB::select('SELECT SUM(returns) as item_amt from lottery
    					 WHERE lottery_date <= "'.$startdate.'"
					     AND lottery_date >= "'.$calcdate.'"');
    	$arrayResult = json_decode(json_encode($sql), true);
    	if(count($sql) != 0)
    	{
    		foreach ($arrayResult as $key) {
    			$itemamt=$key['item_amt']* (-1); //$itemid=$key['item_id'];
    			$this->datapush($itemamt, $calcdate, $itemid);
    		}
    	}

    	return true;
    }

    public function datapush($itemamt, $calcdate, $itemid)
    {
    	$sql=DB::select('SELECT * from recap WHERE item_id = "'.$itemid.'" AND wk_end_date = "'.$calcdate.'"');
    			if(count($sql) == 0)
    			{
    			  $sql = DB::select('INSERT INTO recap(`item_id`, `item_amt`, `wk_end_date`)
			      VALUES("'.$itemid.'","'.$itemamt.'", "'.$calcdate.'")');
		   	  	}
		   	  else
		   	  {
		   	 $sql = DB::select('UPDATE recap SET item_amt="'.$itemamt.'" WHERE item_id="'.$itemid.'" AND wk_end_date="'.$calcdate.'"');
		   	  }
		 return 1;  	  
    }

    public function postSavereWklyRecap()
    {
    	return 1;
    }

    public function postUnlocakWekly()
    {
    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
    	$validator = Validator::make(Input::all(),
            array(                   
                   'dateofinfo'=>'required|date|weekenddatecheck',
                ),array('dateofinfo.required'=>'Date is required'));
        if($validator->fails())
        {
            //return View::make('mktmgr.bookwklysales'); 
            return Redirect::route('mktmgr-bookwklyunlock')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
           $dateofinfo = Input::get('dateofinfo');
           return View::make('mktmgr.passwordform', compact('dateofinfo')); 
        }
    }

    public function getPasswordForm()
    {
      $dateofinfo = Input::get('dateofinfo');
      return View::make('mktmgr.passwordform', compact('dateofinfo')); 
    }

    public function postUnlockPwd()
    {
    	$dateofinfo = Input::get('dateofinfo');
    	$begindate = date('Y-m-d',strtotime($dateofinfo . "-6 days"));
    	$convert_date = date("m-d-Y", strtotime(Input::get('dateofinfo')));
    	$validator = Validator::make(Input::all(),
            array(                   
                   'password'=>'required',
                ));
        if($validator->fails())
        {
            //return View::make('mktmgr.bookwklysales'); 
            return Redirect::route('mktmgr-passwordform')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$pwd=Input::get('password');
	    	$passwrd = "youbetcha";
	    	if($pwd == $passwrd)
	    	{
	    		//echo 'DELETE FROM locks WHERE date_stamp >= "'.$begindate.'" AND date_stamp <= "'.$dateofinfo.'"';exit;
	    		$sql=DB::delete('delete from locks where date_stamp >= "'.$begindate.'" and 
	    															    date_stamp <= "'.$dateofinfo.'"');
	    		Session::flash('alert-info','Files can now be updated for week ending,'.$convert_date);
				return View::make('mktmgr.passwordunlock', compact('dateofinfo'));
	    	}
	    	else
	    	{

	    		Session::flash('alert-danger','INVALID PASSWORD FILES CAN NOT BE UNLOCKED');
				return View::make('mktmgr.passwordunlock', compact('dateofinfo'));
	    	}
           //return View::make('mktmgr.passwordform', compact('registers_num')); 
        }
    	
    }

    public function postBookwklyView()
    {
    	$work_data = new StdClass();
    	Validator::extend('weekenddatecheck', function($attribute, $value, $parameters) {           
              $weekday = date('N', strtotime($value));      
              if($weekday==6){
                  return true;
              }            
            return false;
        },'NOT A VALID WEEKENDING DATE.....');
    	$validator = Validator::make(Input::all(),
            array(                   
                   'dateofinfo'=>'required|date|weekenddatecheck',
                ),array('dateofinfo.required'=>'Date is required'));
        if($validator->fails())
        {
            //return View::make('mktmgr.bookwklysales'); 
            return Redirect::route('mktmgr-bookwklyview')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
           $enddate=Input::get('dateofinfo');
           $begindate = date('Y-m-d',strtotime($enddate . "-6 days")); //itemdesc
           $sql=DB::select('select r.item_id as item_id, r.item_desc as item_desc,rm.dept_sales/100 as dept_sales from recapitems r left join
      	  				   (select dept_sales, dept_id FROM recapmain 
      	  				    where dept_sales != 0 AND wk_end_date = "'.Input::get('dateofinfo').'") rm 
      	  				    ON r.item_id = rm.dept_id 
      	  				    where r.item_type IN ("M","J", "A")
      	  				    and r.disable="N" 
      	  				    order BY r.item_id ');	
           $deptsaleslist = json_decode(json_encode($sql), true);

           $sql2=DB::select('select recapmain.dept_sales/100 as item_amt, recapitems.item_id as item_id, recapitems.item_desc as item_desc 
           					 from recapmain right join 
							(select item_id, item_desc FROM recapitems where item_type IN ("I")) recapitems
							 on recapmain.dept_id = recapitems.item_id
							 and recapmain.wk_end_date = "'.Input::get('dateofinfo').'"');
            $sql3=DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("C1","C2", "C3", "C4")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 order by wk_end_date, item_id');
            $visadepoist=DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("CD")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 order by wk_end_date, item_id');

            $expressdebit=DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("DD")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 order by wk_end_date, item_id');

          $disoverdeposit=DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("ZD")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 order by wk_end_date, item_id');

     $americanexpress=DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("AX")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 order by wk_end_date, item_id');

     $futureitem1=DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("ED")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 and recap.item_amt !=0
							 order by wk_end_date, item_id');

     $futureitem2 = DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("F2")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 and recap.item_amt !=0
							 order by wk_end_date, item_id');

     $futureitem3 = DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("DI")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 and recap.item_amt !=0
							 order by wk_end_date, item_id');

     $disp_checks = DB::select('select recap.item_id as item_id, recap.item_amt/100 as item_amt,recap.wk_end_date as wk_end_date, safeitems.item_desc 
           	                 FROM recap, safeitems where safeitems.item_id = recap.item_id
           	                 and recap.item_id IN  ("K1","K2","1K","2K")
							 and recap.wk_end_date >= "'.$begindate.'"
							 and recap.wk_end_date <= "'.$enddate.'"
							 and recap.item_amt !=0
							 order by wk_end_date, item_id');
     /* Old Query */
      /* $storetrans = DB::select('select safeitems.item_id as item_id, safeitems.item_desc as item_desc,recap.item_amt/100 as item_amt
     	                       from safeitems, recap where safeitems.item_type = "S"
							   and safeitems.item_id NOT IN ("RR","LR","TD")
							   and safeitems.item_id = recap.item_id 
							   and recap.wk_end_date = "'.$enddate.'"
							   and recap.item_amt != 0 
							   order by safeitems.item_id'); */

					$storetrans = DB::select('select recapitems.item_id, recapitems.item_desc,
					recap.item_amt/100 as item_amt 
					from recapitems, recap 
					where recapitems.item_type ="S" 
					and recapitems.item_id not in ("RR","LR","TD") 
					and recapitems.item_id = recap.item_id 
					and recapitems.disable = "N" 
					and recap.wk_end_date = "'.$enddate.'"
					and recap.item_amt <> 0 
					order by recapitems.item_id');

$paidouttypes = DB::select('select recapitems.item_id as item_id, recapitems.item_desc as item_desc,recap.item_amt/100 as item_amt from recapitems, recap where recapitems.item_type ="O" 
								and recapitems.item_id = recap.item_id
								and recap.wk_end_date = "'.$enddate.'"
								and recap.item_amt != 0 
								order by recapitems.item_id;');
						$delivertransfer1 = DB::select('select sum(recap.item_amt) as amt FROM recap where recap.item_id IN ("T1","T2", "T3", "T4", "T5", "T6", "T7")
							and recap.wk_end_date >=  "'.$begindate.'" 
							and recap.wk_end_date <=  "'.$enddate.'"');
						$delivertransfer2 = DB::select('select sum(recap.item_amt) as amt FROM recap where recap.item_id IN ("D1","D2", "D3", "D4", "D5","D6","D7")
								and recap.wk_end_date >=  "'.$begindate.'" and recap.wk_end_date <=  "'.$enddate.'"');

						//, safeitems.item_desc 
      //FROM recap, safeitems where safeitems.item_id = recap.item_id
						$delivertransfertable1 = DB::select('select recap.item_id, (recap.item_amt / 100) as item_amt, recap.wk_end_date, safeitems.item_desc as item_desc 
							 from recap, safeitems where
							 recap.item_id = safeitems.item_id 
												 and recap.item_id IN  ("T1","T2","T3","T4","T5","T6","T7") 
												 and recap.wk_end_date >= "'.$begindate.'"
													and recap.wk_end_date <= "'.$enddate.'"
													order by recap.wk_end_date, recap.item_id');

						$delivertransfertable2 = DB::select('select recap.item_id, (recap.item_amt / 100) as item_amt, recap.wk_end_date , safeitems.item_desc as item_desc 
								from recap, safeitems where 
								recap.item_id = safeitems.item_id 
							 and recap.item_id IN  ("D1","D2","D3","D4","D5","D6")
							 and recap.wk_end_date >= "'.$begindate.'"
								and recap.wk_end_date <= "'.$enddate.'"
								order by recap.wk_end_date, recap.item_id');


        // Main Page Sum:
         //$deposits_sum = 0;  

        $dept_sales_query = DB::select('SELECT SUM(recapmain.dept_sales) as dept_sales_sum FROM recapmain,recapitems WHERE recapmain.wk_end_date = "'.$enddate.'" AND recapitems.item_type IN ("M","A","J") AND recapmain.dept_id = recapitems.item_id');   
        $dept_sales_sum = $dept_sales_query[0]->dept_sales_sum; 
        

        $net_safe_query = DB::select('SELECT SUM(recapmain.dept_sales) 
               net_safe_sum
              FROM recapmain,recapitems
             WHERE recapmain.wk_end_date = "'.$enddate.'"
               AND recapitems.item_type = "I" 
               AND recapmain.dept_id  = recapitems.item_id');   
        $net_safe_sum = $net_safe_query[0]->net_safe_sum; 

        $deposits_sum_query = DB::select('SELECT SUM(recap.item_amt) 
                      as deposits_sum
                      FROM recap,safeitems
                     WHERE recap.wk_end_date >= "'.$begindate.'"
                       AND recap.wk_end_date <= "'.$enddate.'"
                       AND safeitems.item_type  = "P"
                       AND safeitems.item_id  <>  "#C"
                       AND recap.item_id  = safeitems.item_id');
        $deposits_sum = $deposits_sum_query[0]->deposits_sum; 


        $del_tranfs_query = DB::select('SELECT SUM(recap.item_amt) 
               del_tranfs_sum
              FROM recap,recapitems
             WHERE recap.wk_end_date >= "'.$begindate.'"
               AND recap.wk_end_date <= "'.$enddate.'"
               AND recapitems.item_type  = "T"
               AND recap.item_id  = recapitems.item_id');   
        $del_tranfs_sum = $del_tranfs_query[0]->del_tranfs_sum;

        $store_tx_query = DB::select('SELECT SUM(recap.item_amt) 
             as store_tx_sum
              FROM recap,recapitems
             WHERE recap.wk_end_date = "'.$enddate.'"
               AND recapitems.item_type  = "S"
               AND recapitems.item_id  NOT IN ("RR","LR","TD")
               AND recap.item_id  = recapitems.item_id');   
        $store_tx_sum = $store_tx_query[0]->store_tx_sum;

        $po_types_query = DB::select('SELECT SUM(recap.item_amt) 
               po_types_sum
              FROM recap,recapitems
             WHERE recap.wk_end_date = "'.$enddate.'"
               AND recapitems.item_type  = "O"
               AND recap.item_id  = recapitems.item_id');   
        $po_types_sum = $po_types_query[0]->po_types_sum;
        // if($deposits_sum_query)
        // {
        //     $deposits_sum = $deposits_sum_query[0]->deposits_sum;
        // }
        // else
        // {
        //     $deposits_sum = 0;
        // }
        //echo 'deposits_sum '.$deposits_sum;exit;
           $netsafeactive = json_decode(json_encode($sql2), true);
           $currentytot = json_decode(json_encode($sql3), true);
           $visadepoist = json_decode(json_encode($visadepoist), true);
           $expressdebit = json_decode(json_encode($expressdebit), true);
           $disoverdeposit = json_decode(json_encode($disoverdeposit), true);
           $americanexpress=json_decode(json_encode($americanexpress), true);
           $futureitem1=json_decode(json_encode($futureitem1), true);
           $futureitem2=json_decode(json_encode($futureitem2), true);
           $futureitem3=json_decode(json_encode($futureitem3), true);
           $disp_checks=json_decode(json_encode($disp_checks), true);
           //echo '<pre>';print_r($futureitem2);exit;
           $storetrans=json_decode(json_encode($storetrans), true);
           $paidouttypes=json_decode(json_encode($paidouttypes), true);
           $delivertransfer1=json_decode(json_encode($delivertransfer1), true);
           $delivertransfertable1=json_decode(json_encode($delivertransfertable1), true);
           $delivertransfertable2=json_decode(json_encode($delivertransfertable2), true);
           $delivertransfer1=$delivertransfer1[0]['amt'];
           if($delivertransfer1 == '') {$delivertransfer1 = '0';}
           $delivertransfer2=json_decode(json_encode($delivertransfer2), true); 
           $delivertransfer2=$delivertransfer2[0]['amt'];
           if($delivertransfer2 == '') {$delivertransfer2 = '0';}
           $delivertransfertot = $delivertransfer1+$delivertransfer2;
           $dateofinfo = date("m/d/Y", strtotime(Input::get('dateofinfo')));

           return View::make('mktmgr.bookwklyviewfrm',compact('dateofinfo','deptsaleslist','netsafeactive', 'currentytot', 'visadepoist', 
           	'expressdebit', 'disoverdeposit', 'americanexpress', 'futureitem1','futureitem2','futureitem3','disp_checks','storetrans','paidouttypes', 'delivertransfer1', 'delivertransfer2', 'delivertransfertot',
           	'delivertransfertable1','delivertransfertable2','deposits_sum','dept_sales_sum','net_safe_sum','del_tranfs_sum','store_tx_sum','po_types_sum'));                                                                                                                                       
        }
    }

    public function postBookWklyViewData()
    {
    	//$dateofinfo = date("m-d-Y", strtotime(Input::get('dateofinfo')));
    	$dateofinfo = Input::get('dateofinfo');
    	$deptsaleslist=$this->dispdeptsales($dateofinfo);
		return View::make('mktmgr.bookwklyviewfrm', compact('deptsaleslist'));	
    }

    public function dispdeptsales($dateofinfo)
    {
      	/*return 
      	return $arrayResult = json_decode(json_encode($sql), true);*/
    }
   




   
    
}
