<?php

class BookkeeperController extends  BaseController
{
	
	
	public function getSafeCountDetail()
	{
        $dateofinfo = Input::get('info_date');
        //echo $dateofinfo;exit;
        $date1 = str_replace('-', '/', $dateofinfo);
        $yesterday = date('Y-m-d',strtotime($date1 . "-1 days"));
        $bktodays = date('Y-m-d',strtotime($date1 . "-2 days"));
        //echo 'SELECT max(date_stamp) as lastAprvlDate FROM mgmt where date_stamp < "'.$dateofinfo.'"';exit;
        $results = DB::select('SELECT max(date_stamp) as lastAprvlDate FROM mgmt where date_stamp < "'.$dateofinfo.'"');
        //echo '<pre>';print_r($results)."<br>";exit;
        
        $resultArray = json_decode(json_encode($results), true); 
        $lastAprvlDate=$resultArray[0]['lastAprvlDate'];
        //echo $lastAprvlDate;exit;
        $lastAprvlDate1day = date('Y-m-d',strtotime($lastAprvlDate . "+1 days"));  //echo "<br />";
        $convert_date = date("Y-m-d", strtotime(Input::get('dateofinfo')));
        if(empty($lastAprvlDate))
        {
            $infodate = date("Y-m-d", strtotime(Input::get('info_date')));
            $coin_data = DB::select('SELECT * from safe_coin where safe_date = "'.$infodate.'" ');
            $currency_data = DB::select('SELECT * from safe_curr where safe_date = "'.$infodate.'" ');
            $other_data = DB::select('SELECT * from safe_other where safe_date = "'.$infodate.'" ');
            if($coin_data  && $currency_data && $other_data)
            {
            return View::make('mktmgr.safecountdetail_update', compact('coin_data','currency_data','other_data','infodate'));
            }
            return View::make('mktmgr.safecountdetail', compact('coin_data','currency_data','other_data','infodate'));
        }
        if($lastAprvlDate < $yesterday)
        {
            $lastAprvlDate = date('m/d/Y',strtotime($lastAprvlDate));
            Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
            return Redirect::route('mktmgr-booksafecnt');
        }
        else
        {
            $infodate = date("Y-m-d", strtotime(Input::get('info_date')));
            $coin_data = DB::select('SELECT * from safe_coin where safe_date = "'.$infodate.'" ');
            $currency_data = DB::select('SELECT * from safe_curr where safe_date = "'.$infodate.'" ');
            $other_data = DB::select('SELECT * from safe_other where safe_date = "'.$infodate.'" ');
            if($coin_data  && $currency_data && $other_data)
            {
            return View::make('mktmgr.safecountdetail_update', compact('coin_data','currency_data','other_data','infodate'));
            }
            return View::make('mktmgr.safecountdetail', compact('coin_data','currency_data','other_data','infodate'));
        }


		// $infodate = date("Y-m-d", strtotime(Input::get('info_date')));
		// $coin_data = DB::select('SELECT * from safe_coin where safe_date = "'.$infodate.'" ');
		// $currency_data = DB::select('SELECT * from safe_curr where safe_date = "'.$infodate.'" ');
		// $other_data = DB::select('SELECT * from safe_other where safe_date = "'.$infodate.'" ');
  //       if($coin_data  && $currency_data && $other_data)
  //       {
  //       return View::make('mktmgr.safecountdetail_update', compact('coin_data','currency_data','other_data','infodate'));
  //       }
	 //    return View::make('mktmgr.safecountdetail', compact('coin_data','currency_data','other_data','infodate'));
	}
	
	/* Sravan Kumar Sriramula Safe Coin, Safe Current, Safe Total Form Data Starts*/
	public function postSafeCountDetail()
	{
		$data = Input::all();
		$safe_date = date("Y-m-d", strtotime(Input::get('safe_date')));
        //echo $safe_date."<br>";
        $next_day = date('Y-m-d',strtotime($safe_date . "+1 days"));
        //$searchresult = DB::select('select * from registers order by reg_num');
        $begin_safe =  DB::select('select * from mainsafe where safe_date = "'.$next_day.'" ');
        $begin_safe_data = json_decode(json_encode($begin_safe), true);
        //echo '<pre>';print_r($begin_safe_data);exit;
        if(!empty($begin_safe_data))
        {
            $GrandTot = Input::get('GrandTot');
            $delivery_update = DB::table('mainsafe')
            ->where('safe_date', $next_day)
            //->where('entry_date', $entry_date)
           ->update(array(
                'begin_safe' => $GrandTot
                ));

        }
        //echo $next_day;
          // echo 'select ifnull(pennies_box, 0) + ifnull(pennies_roll, 0) + ifnull(nickles_box, 0) + ifnull(nickles_roll, 0) + ifnull(dimes_box, 0) + ifnull(dimes_roll, 0) + ifnull(quarters_box, 0) + ifnull(quarters_roll, 0) + ifnull(misc_box, 0) + ifnull(misc_roll, 0) as SafeCoinSum from safe_coin where safe_date = "'.$safe_date.'"'; exit;
        // $safe_coin_sum = Input::get('pennies_box') + Input::get('pennies_roll') + Input::get('nickles_box') + Input::get('nickles_roll') + Input::get('dimes_box') + Input::get('dimes_roll') + Input::get('quarters_box') + Input::get('quarters_roll') + Input::get('misc_box') + Input::get('misc_roll');
        // echo $safe_coin_sum;//exit;
        
       // echo $GrandTot;exit;
		/* Sravan Kumar Safe Coin Post Data Start*/
		$safeCountCoin = new SafeCoin;
		if(Input::has('pennies_box')) {
            $safeCountCoin->pennies_box = Input::get('pennies_box');
        }
        if(Input::has('pennies_roll')) {
            $safeCountCoin->pennies_roll = Input::get('pennies_roll');
        }
        if(Input::has('nickles_box')) {
            $safeCountCoin->nickles_box = Input::get('nickles_box');
        }
        if(Input::has('nickles_roll')) {
            $safeCountCoin->nickles_roll = Input::get('nickles_roll');
        }
        if(Input::has('dimes_box')) {
            $safeCountCoin->dimes_box = Input::get('dimes_box');
        }
        if(Input::has('dimes_roll')) {
            $safeCountCoin->dimes_roll = Input::get('dimes_roll');
        }
        if(Input::has('quarters_box')) {
            $safeCountCoin->quarters_box = Input::get('quarters_box');
        }
        if(Input::has('quarters_roll')) {
            $safeCountCoin->quarters_roll = Input::get('quarters_roll');
        }
        if(Input::has('misc_box')) {
            $safeCountCoin->misc_box = Input::get('misc_box');
        }
        if(Input::has('misc_roll')) {
            $safeCountCoin->misc_roll = Input::get('misc_roll');
        }
        if(Input::has('misc_desc')) {
            $safeCountCoin->misc_desc = Input::get('misc_desc');
        }
        if(Input::has('safe_date')) {
            $safeCountCoin->safe_date = $safe_date;
        }
        $safeCountCoin->save();

        /* Sravan Kumar Safe Coin Post Data Ends*/
        // if($safeCountCoin->save())
        // {    
        //   	echo $safe_date."<br>";
        // 	echo 'Record Added Successfully';
        // } 



        /* Sravan Kumar Safe Current Post Data Start*/
        $safeCountCurrent = new SafeCurrent;
		if(Input::has('ones_box')) {
            $safeCountCurrent->ones_box = Input::get('ones_box');
        }
        if(Input::has('ones_roll')) {
            $safeCountCurrent->ones_roll = Input::get('ones_roll');
        }
        if(Input::has('ones_held')) {
            $safeCountCurrent->ones_held = Input::get('ones_held');
        }
        if(Input::has('fives_box')) {
            $safeCountCurrent->fives_box = Input::get('fives_box');
        }
        if(Input::has('fives_roll')) {
            $safeCountCurrent->fives_roll = Input::get('fives_roll');
        }
        if(Input::has('fives_held')) {
            $safeCountCurrent->fives_held = Input::get('fives_held');
        }
        if(Input::has('tens_box')) {
            $safeCountCurrent->tens_box = Input::get('tens_box');
        }
        if(Input::has('tens_roll')) {
            $safeCountCurrent->tens_roll = Input::get('tens_roll');
        }
        if(Input::has('tens_held')) {
            $safeCountCurrent->tens_held = Input::get('tens_held');
        }
        if(Input::has('twenties_box')) {
            $safeCountCurrent->twenties_box = Input::get('twenties_box');
        }
        if(Input::has('twenties_roll')) {
            $safeCountCurrent->twenties_roll = Input::get('twenties_roll');
        }
        if(Input::has('twenties_held')) {
            $safeCountCurrent->twenties_held = Input::get('twenties_held');
        }
        if(Input::has('misc1_box')) {
            $safeCountCurrent->misc1_box = Input::get('misc1_box');
        }
        if(Input::has('misc1_roll')) {
            $safeCountCurrent->misc1_roll = Input::get('misc1_roll');
        }
        if(Input::has('misc1_held')) {
            $safeCountCurrent->misc1_held = Input::get('misc1_held');
        }
        if(Input::has('misc1_desc')) {
            $safeCountCurrent->misc1_desc = Input::get('misc1_desc');
        }
        if(Input::has('misc2_box')) {
            $safeCountCurrent->misc2_box = Input::get('misc2_box');
        }
        if(Input::has('misc2_roll')) {
            $safeCountCurrent->misc2_roll = Input::get('misc2_roll');
        }
        if(Input::has('misc2_held')) {
            $safeCountCurrent->misc2_held = Input::get('misc2_held');
        }
        if(Input::has('misc2_desc')) {
            $safeCountCurrent->misc2_desc = Input::get('misc2_desc');
        }
        if(Input::has('safe_date')) {
            $safeCountCurrent->safe_date = $safe_date;
        }
        $safeCountCurrent->save();
        // $safe_current_sum = Input::get('ones_box') + Input::get('ones_roll') + Input::get('ones_held') + Input::get('fives_box') + Input::get('fives_roll') + Input::get('fives_held') + Input::get('tens_box') + Input::get('tens_roll') + Input::get('tens_held') + Input::get('twenties_box') + Input::get('twenties_roll') + Input::get('twenties_held') + Input::get('misc1_box') + Input::get('misc1_roll') + Input::get('misc1_held') + Input::get('misc2_box') + Input::get('misc2_roll') + Input::get('misc2_held');
        /* Sravan Kumar Safe Current Post Data Ends*/
        // if($safeCountCurrent->save())
        // {    
        //   	echo $safe_date."<br>";
        // 	echo 'Record Added Successfully';
        // }	





        /* Sravan Kumar Safe Other Post Data Start*/
        $safeCountOther = new SafeOther;
		if(Input::has('food_stamps')) {
            $safeCountOther->food_stamps = Input::get('food_stamps');
        }
        if(Input::has('merch_fives')) {
            $safeCountOther->merch_fives = Input::get('merch_fives');
        }
        if(Input::has('merch_tens')) {
            $safeCountOther->merch_tens = Input::get('merch_tens');
        }
        if(Input::has('merch_twenties')) {
            $safeCountOther->merch_twenties = Input::get('merch_twenties');
        }
        if(Input::has('merch_misc')) {
            $safeCountOther->merch_misc = Input::get('merch_misc');
        }
        if(Input::has('script1')) {
            $safeCountOther->script1 = Input::get('script1');
        }
        if(Input::has('script2')) {
            $safeCountOther->script2 = Input::get('script2');
        }
        if(Input::has('script3')) {
            $safeCountOther->script3 = Input::get('script3');
        }
        if(Input::has('script4')) {
            $safeCountOther->script4 = Input::get('script4');
        }
        if(Input::has('script5')) {
            $safeCountOther->script5 = Input::get('script5');
        }
        if(Input::has('script6')) {
            $safeCountOther->script6 = Input::get('script6');
        }
        if(Input::has('script7')) {
            $safeCountOther->script7 = Input::get('script7');
        }
        if(Input::has('script8')) {
            $safeCountOther->script8 = Input::get('script8');
        }
        if(Input::has('script9')) {
            $safeCountOther->script9 = Input::get('script9');
        }
        if(Input::has('script10')) {
            $safeCountOther->script10 = Input::get('script10');
        }
        if(Input::has('begin_loans')) {
            $safeCountOther->begin_loans = Input::get('begin_loans');
        }
        if(Input::has('misc1_amt')) {
            $safeCountOther->misc1_amt = Input::get('misc1_amt');
        }
        if(Input::has('misc1_desc')) {
            $safeCountOther->misc1_desc = Input::get('misc1_desc');
        }
        if(Input::has('misc2_amt')) {
            $safeCountOther->misc2_amt = Input::get('misc2_amt');
        }
        if(Input::has('misc2_desc')) {
            $safeCountOther->misc2_desc = Input::get('misc2_desc');
        }
        if(Input::has('instr_chrg')) {
            $safeCountOther->instr_chrg = Input::get('instr_chrg');
        }
        if(Input::has('pay_adv')) {
            $safeCountOther->pay_adv = Input::get('pay_adv');
        }
        if(Input::has('night_crew')) {
            $safeCountOther->night_crew = Input::get('night_crew');
        }
        if(Input::has('debit')) {
            $safeCountOther->debit = Input::get('debit');
        }
        if(Input::has('credit')) {
            $safeCountOther->credit = Input::get('credit');
        }
        if(Input::has('ebt')) {
            $safeCountOther->ebt = Input::get('ebt');
        }
        if(Input::has('giftcard')) {
            $safeCountOther->giftcard = Input::get('giftcard');
        }
        if(Input::has('misc3_amt')) {
            $safeCountOther->misc3_amt = Input::get('misc3_amt');
        }
        if(Input::has('misc3_desc')) {
            $safeCountOther->misc3_desc = Input::get('misc3_desc');
        }
        if(Input::has('safe_date')) {
            $safeCountOther->safe_date = $safe_date;
        }
        // $safe_other_sum = Input::get('food_stamps') + Input::get('merch_fives') + Input::get('merch_tens') + Input::get('merch_twenties') + Input::get('merch_misc') + Input::get('script1') + Input::get('script2') + Input::get('script3') + Input::get('script4') + Input::get('script5') + Input::get('script6') + Input::get('script7') + Input::get('script8') + Input::get('script9') + Input::get('script10')  + Input::get('begin_loans')  + Input::get('misc1_amt')  + Input::get('misc2_amt')  + Input::get('instr_chrg')  + Input::get('pay_adv')  + Input::get('night_crew')  + Input::get('debit')  + Input::get('credit')  + Input::get('ebt')  + Input::get('giftcard')  + Input::get('misc3_amt');
        //$grand_total = 
        if($safeCountOther->save())
        {  
          //echo 'select * from mainsafe where safe_date = "'.$next_day.'" ';exit;
          Session::flash('alert-success', 'Total Safe Count Details added successfully!'); 
          return Redirect::route('mktmgr-booksafecnt');
        }

        /* Sravan Kumar Safe Coin Post Data Ends*/

	}/* Sravan Kumar Sriramula Safe Coin, Safe Current, Safe Total Form Data Ends*/

    public function postSafeCountDetailUpdate()
    {
        $safe_date = date("Y-m-d", strtotime(Input::get('safe_date')));
             $updatecoindata = DB::table('safe_coin')
            ->where('safe_date', $safe_date)
            ->update(array('pennies_box' => Input::get('pennies_box'),
                            'pennies_roll' => Input::get('pennies_roll'),
                            'nickles_box' => Input::get('nickles_box'),
                            'nickles_roll' => Input::get('nickles_roll'),
                            'dimes_box' => Input::get('dimes_box'),
                            'dimes_roll' => Input::get('dimes_roll'),
                            'quarters_box' => Input::get('quarters_box'),
                            'quarters_roll' => Input::get('quarters_roll'),
                            'misc_box' => Input::get('misc_box'),
                            'misc_roll' => Input::get('misc_roll'),
                            'misc_desc' => Input::get('misc_desc')
                            ));  

       
       $updatecurrentdata = DB::table('safe_curr')
            ->where('safe_date', $safe_date)
            ->update(array('ones_box' => Input::get('ones_box'),
                            'ones_roll' => Input::get('ones_roll'),
                            'ones_held' => Input::get('ones_held'),
                            'fives_box' => Input::get('fives_box'),
                            'fives_roll' => Input::get('fives_roll'),
                            'fives_held' => Input::get('fives_held'),
                            'tens_box' => Input::get('tens_box'),
                            'tens_roll' => Input::get('tens_roll'),
                            'tens_held' => Input::get('tens_held'),
                            'twenties_box' => Input::get('twenties_box'),
                            'twenties_roll' => Input::get('twenties_roll'),
                            'twenties_held' => Input::get('twenties_held'),
                            'misc1_box' => Input::get('misc1_box'),
                            'misc1_roll' => Input::get('misc1_roll'),
                            'misc1_held' => Input::get('misc1_held'),
                            'misc1_desc' => Input::get('misc1_desc'),
                            'misc2_box' => Input::get('misc2_box'),
                            'misc2_roll' => Input::get('misc2_roll'),
                            'misc2_held' => Input::get('misc2_held'),
                            'misc2_desc' => Input::get('misc2_desc')
                            )); 

            $updateotherdata = DB::table('safe_other')
            ->where('safe_date', $safe_date)
            ->update(array('food_stamps' => Input::get('food_stamps'),
                            'merch_fives' => Input::get('merch_fives'),
                            'merch_tens' => Input::get('merch_tens'),
                            'merch_twenties' => Input::get('merch_twenties'),
                            'merch_misc' => Input::get('merch_misc'),
                            'begin_loans' => Input::get('begin_loans'),
                            'misc1_amt' => Input::get('misc1_amt'),
                            'misc1_desc' => Input::get('misc1_desc'),
                            'misc2_amt' => Input::get('misc2_amt'),
                            'misc2_desc' => Input::get('misc2_desc'),
                            'instr_chrg' => Input::get('instr_chrg'),
                            'pay_adv' => Input::get('pay_adv'),
                            'night_crew' => Input::get('night_crew'),
                            'debit' => Input::get('debit'),
                            'credit' => Input::get('credit'),
                            'ebt' => Input::get('ebt'),
                            'giftcard' => Input::get('giftcard'),
                            'misc3_amt' => Input::get('misc3_amt'),
                            'misc3_desc' => Input::get('misc3_desc'),
                            'script1' => Input::get('script1'),
                            'script2' => Input::get('script2'),
                            'script3' => Input::get('script3'),
                            'script4' => Input::get('script4'),
                            'script5' => Input::get('script5'),
                            'script6' => Input::get('script6'),
                            'script7' => Input::get('script7'),
                            'script8' => Input::get('script8'),
                            'script9' => Input::get('script9'),
                            'script10' => Input::get('script10')
                            )); 

            // echo $updatecoindata."<br>";
            // echo $updatecurrentdata."<br>";
            // echo $updateotherdata."<br>";
            // exit;
           
        // if($updatecoindata || $updatecurrentdata || $updateotherdata)
        // {    
          Session::flash('alert-success', 'Total Safe Count Details Updated successfully!'); 
          return Redirect::route('mktmgr-booksafecnt');

        //}

    }
    public function postbookchglist()
    {
       //  $validator = Validator::make(Input::all(),
       //  array(
       //     'regnumber'=>'required',
       //     'regtype'=>'required',
       //  ));
       // if($validator->fails())
       // {
       //     return Redirect::route('mktmgr-bookchgquery')
       //             ->withErrors($validator)
       //             ->withInput();
       // }
       // else
       // { 
       //  $regtype = Input::get('regtype');
       //  $regnumber = Input::get('regnumber');
       //  $searchresult = DB::select('select * from registers where reg_num = '.$regnumber.' and reg_type = "'.$regtype.'" ');
       //  return View::make('mktmgr.bookchglist',compact('searchresult'));
       //  }
        $regtype = Input::get('regtype');
        $regnumber = Input::get('regnumber');
        //echo 'select * from registers where reg_num = "'.$regnumber.'" and reg_type = "'.$regtype.'" ';exit;
        $searchresult = DB::select('select * from registers order by reg_num'); 
        //$searchresult = DB::select('select * from registers where reg_num = '.$regnumber.' and reg_type = "'.$regtype.'" ');
        if(Input::has('regtype')) 
        {    
            $searchresult = DB::select('select * from registers where reg_type = "'.$regtype.'" ');
        }
        if(Input::has('regnumber')) 
        {    
            $searchresult = DB::select('select * from registers where reg_num = "'.$regnumber.'" ');
        }
        if(Input::has('regnumber') && Input::has('regtype')) 
        {    
            $searchresult = DB::select('select * from registers where reg_num = "'.$regnumber.'" and reg_type = "'.$regtype.'"');
        }
        return View::make('mktmgr.bookchglist',compact('searchresult','regnumber','regtype'));
    }        

       public function postbookkeeperchgadd()
       {  
       $validator = Validator::make(Input::all(),
           array(
                  'regnumber'=>'required',
                  'regtype'=>'required',
                 ),array(
                   'regnumber.required' =>'Register Number is Required',
                   'regtype.required' =>'Registration Type is Required'
                ));
       if($validator->fails())
       {
           return Redirect::route('mktmgr-bookchgadd')
                   ->withErrors($validator)
                   ->withInput();
       }
       
       else

       {
               $allpostval=Input::all();
                               //echo '<pre>';
                               //dd($allpostval);
                //vendorname
               
       $ChgAdd = new ChgReg;
       if(Input::has('regnumber')) {
           $ChgAdd->reg_num = Input::get('regnumber');
       }
       if(Input::has('regtype')) 
       {
     if((Input::get('regtype') == 'g') || (Input::get('regtype') == 'G'))
        {
        $ChgAdd->reg_type = strtoupper(Input::get('regtype'));
        }
         else{
            Session::flash('alert-danger', 'Registration Type must be G!');
            return Redirect::route('mktmgr-bookchgadd');
             }
           
       
       }
       $regnumber = Input::get('regnumber');
       $checkRegNo = DB::select('SELECT * from registers where reg_num = "'.$regnumber.'" ');
       if($checkRegNo)
       {
            Session::flash('alert-danger', 'Registration Number Already Exists!');
        return Redirect::route('mktmgr-bookchgadd');
       }
       if($ChgAdd->save())
       {        

               //return View::make('mktmgr.bookchgadd');
        Session::flash('alert-success', 'Record added successfully!');
        return Redirect::route('mktmgr-bookchgadd');
               
       }

        }
       }


    public function viewchgregnum($reg_num)
    {
      // echo 'viewchgregnum'."<br>";
       //echo $reg_num;
       //exit;
       if($reg_num) {
            $chg_data = ChgReg::where('reg_num',$reg_num)->first();
            return View::make('mktmgr.viewchgregnum')->with('chg_data',$chg_data);
        }
        // if(Input::get('reg_num')) {
        //     $chg_data = ChgReg::where('reg_num',Input::get('reg_num'))->first();
        //     return View::make('mktmgr.viewchgregnum')->with('chg_data',$chg_data);
        // }
       
    } 
    public function updatechgregnum()
    {
        //echo 'updatechgregnum';exit;
        if(Input::has('reg_num')) {
            $chg_data = ChgReg::where('reg_num',Input::get('reg_num'))->first();
            return View::make('mktmgr.updatechgregnum')->with('chg_data',$chg_data);
        }
    } 
    public function postupdatechgreg()
    {
        $reg_num = Input::get('reg_num');
        $reg_type = strtoupper(Input::get('reg_type'));
        // echo $reg_num."<br>";strtoupper(Input::get('regtype'));
        // echo $reg_type."<br>";
        $updateChgReg = DB::table('registers')
            ->where('reg_num', $reg_num)
            ->limit(1)
            ->update(array( 'reg_num' => $reg_num,
                            'reg_type' => $reg_type
                            )); 
        //     print_r($updateChgReg);exit;
//         echo "UPDATE registers SET registers.reg_type = '".$reg_type."' where registers.reg_num = '".$reg_num."'";exit;
// DB::statement("UPDATE registers SET registers.reg_type = '".$reg_type."' where registers.reg_num = '".$reg_num."'");
        // if($updatecoindata || $updatecurrentdata || $updateotherdata)
        // {    
          Session::flash('alert-success', 'ChgReg Updated successfully!'); 
          return Redirect::route('mktmgr-bookchgquery')->with('reg_num', $reg_num);
    } 
    public function chgregdel()
    {
        $reg_num = Input::get('reg_num');
        //echo $reg_num;
        DB::table('registers')->where('reg_num', $reg_num)->delete();
         Session::flash('alert-success', 'ChgReg Record successfully Deleted!'); 
          return Redirect::route('mktmgr-bookchgquery')->with('reg_num', $reg_num);
    }
    public function getSafeReportDetail()
    {
        $dateofinfo = Input::get('dateofinfo');
        $date1 = str_replace('-', '/', $dateofinfo);
        $yesterday = date('Y-m-d',strtotime($date1 . "-1 days"));
        $bktodays = date('Y-m-d',strtotime($date1 . "-2 days"));
        //echo 'SELECT max(date_stamp) as lastAprvlDate FROM mgmt where date_stamp < "'.$dateofinfo.'"';exit;
        $results = DB::select('SELECT max(date_stamp) as lastAprvlDate FROM mgmt where date_stamp < "'.$dateofinfo.'"');
        
        
        $resultArray = json_decode(json_encode($results), true); 
        $lastAprvlDate=$resultArray[0]['lastAprvlDate'];
        //echo '<pre>';print_r($lastAprvlDate)."<br>";exit;
         $lock_safeentry = DB::select('SELECT entry_date FROM safeentry where entry_date = "'.$dateofinfo.'" ');
            $checklock_safeentry = json_decode(json_encode($lock_safeentry), true); 
            if($checklock_safeentry)
            {
                $lockeddate = date('m/d/Y',strtotime($dateofinfo));
                Session::flash('alert-info',"Data is locked for  $lockeddate. Any changes won't be saved.");
                 return Redirect::route('mktmgr-booksafereport');
            }
        if(empty($lastAprvlDate))
        {
            //echo 'Not Empty';exit;
            $lock_safeentry = DB::select('SELECT entry_date FROM safeentry where entry_date = "'.$dateofinfo.'" ');
            $checklock_safeentry = json_decode(json_encode($lock_safeentry), true); 
            if($checklock_safeentry)
            {
                $lockeddate = date('m/d/Y',strtotime($dateofinfo));
                Session::flash('alert-info',"Data is locked for  $lockeddate. Any changes won't be saved.");
                 return Redirect::route('mktmgr-booksafereport');
            }
            //echo '<pre>';print_r(Input::all())."<br>";exit;
            $deliveries = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("D1","D2","D3","D4","D5","D6","D7")');
            $deliveriesArray = json_decode(json_encode($deliveries), true); 
            //echo '<pre>';print_r($deliveriesArray)."<br>";exit;
            $transfers = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("T1","T2","T3","T4","T5","T6","T7")');
            $transfersArray = json_decode(json_encode($transfers), true);
            $deposits_data = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("K1","K2","C1","C2","C3","C4","DD","ZD","CD","AX","DI","ED","F2","E9")');
            $depositsArray = json_decode(json_encode($deposits_data), true);
            $store_tranaction_data = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("55","86","AR","CS","EE","GE","L9","NV","OB","OD","OG","OI","OK","OT","PG","RC","TD","VR","XP","56","AN","BZ","CV","FC","HD","LR","09","OC","OE","OH","OJ","OO","PC","R5","RY","VC","WC")');
            $store_tranactionArray = json_decode(json_encode($store_tranaction_data), true);
            $POT_data = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("AB","CX","ER","GA","M1","OM","P1","PZ","RB","RE","RG","RM","RO","RQ","SG","SM","TV","WI","CB","EA","FU","GH","MM","OZ","PV","RA","RD","RF","RH","RN","RP","RS","SL","T9","UI")');
            $potArray = json_decode(json_encode($POT_data), true);
            /* Grocery and Drug over */    
            $dateofinfo = date("Y-m-d", strtotime(Input::get('dateofinfo')));
            $subtot1 = $subtot2 = $groctot = $drugtot = 0;
            $results = BookkeepRegisters::orderBy('reg_num')->get();

            foreach ($results as $row) 
            {
                $rs = DB::select('SELECT SUM(entry.item_amt) AS subtot1 FROM entry, items WHERE entry.reg_num = ? AND entry.entry_date = ? AND items.item_id = entry.item_id AND items.item_type = "D"', array($row->reg_num, $dateofinfo));
                if (is_null($rs[0]->subtot1)) 
                    {
                    $rs[0]->subtot1 = 0;
                    }
                $subtot1 = $rs[0]->subtot1;

                $rs = DB::select('SELECT SUM(entry.item_amt) AS subtot2 FROM entry, items WHERE entry.reg_num = ? AND entry.entry_date = ? AND items.item_id = entry.item_id AND items.item_type = "C"', array($row->reg_num, $dateofinfo));
                if (is_null($rs[0]->subtot2)) 
                    {
                    $rs[0]->subtot2 = 0;
                    }
                $subtot2 = $rs[0]->subtot2;

                if ($row->reg_type == 'G') 
                {
                    $groctot = $groctot + ($subtot1 - $subtot2);
                } 
                else if ($row->reg_type == 'D') 
                {
                    $drugtot = $drugtot + ($subtot1 - $subtot2);
                }
                $arrtot['groctot'] = $groctot;
                $arrtot['drugtot'] = $drugtot;
                $subtot1 = $subtot2 = 0;
            }
            /* Grocery and Drug over Ends */  

            $dateofinfo = date("Y-m-d", strtotime(Input::get('dateofinfo')));
            $coin_data = DB::select('SELECT * from safe_coin where safe_date = "'.$dateofinfo.'" ');
            $currency_data = DB::select('SELECT * from safe_curr where safe_date = "'.$dateofinfo.'" ');
            $other_data = DB::select('SELECT * from safe_other where safe_date = "'.$dateofinfo.'" ');
            $begin_safe = DB::select('SELECT * from mainsafe where safe_date = "'.$dateofinfo.'" ');
                if($deliveriesArray || $transfersArray || $depositsArray || $store_tranactionArray || $potArray)
                {
                    return View::make('mktmgr.safereportsdetail_update', compact('coin_data','currency_data','other_data','dateofinfo','deliveriesArray','transfersArray','depositsArray','store_tranactionArray','potArray','arrtot','begin_safe'));
                }
            $dateofinfo = date("Y-m-d", strtotime(Input::get('dateofinfo')));
            $coin_data = DB::select('SELECT * from safe_coin where safe_date = "'.$dateofinfo.'" ');
            $currency_data = DB::select('SELECT * from safe_curr where safe_date = "'.$dateofinfo.'" ');
            $other_data = DB::select('SELECT * from safe_other where safe_date = "'.$dateofinfo.'" ');
            return View::make('mktmgr.safereportsdetail', compact('coin_data','currency_data','other_data','dateofinfo','arrtot'));
            return View::make('mktmgr.safereportsdetail', compact('dateofinfo'));
        }
        $lastAprvlDate1day = date('Y-m-d',strtotime($lastAprvlDate . "+1 days"));  //echo "<br />";
        $convert_date = date("Y-m-d", strtotime(Input::get('dateofinfo')));
        if($lastAprvlDate < $yesterday)
        {       

            Session::flash('alert-info','Last approved date was'.' '.date("m-d-Y", strtotime($lastAprvlDate)).'.'.' '.' A manager need to approve daily work.');
            return Redirect::route('mktmgr-booksafereport');
        }
        else
        {   
            $datalocked = DB::select('select * from  mgmt where  mgmt.date_stamp = "'.$dateofinfo.'" ');
            $checkdatalocked = json_decode(json_encode($datalocked), true); 
            //echo '<pre>';print_r($checkdatalocked);exit;
            //echo 'Safe Not Empty';exit;
            if($checkdatalocked)
            {
                $lockeddate = date('m/d/Y',strtotime($dateofinfo));
                Session::flash('alert-info',"Data is locked for  $lockeddate. Any changes won't be saved.");
                 return Redirect::route('mktmgr-booksafereport');
            }
            $dateofinfo = date("Y-m-d", strtotime(Input::get('dateofinfo')));
            //echo $dateofinfo;exit;
            //$registers_num=DB::select("SELECT * FROM safe_coin where safe_date = '".$dateofinfo."' ");
            $deliveries = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("D1","D2","D3","D4","D5","D6","D7")');
            $deliveriesArray = json_decode(json_encode($deliveries), true); 
            //echo '<pre>';print_r($deliveriesArray)."<br>";
            $transfers = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("T1","T2","T3","T4","T5","T6","T7")');
            $transfersArray = json_decode(json_encode($transfers), true);
            //echo '<pre>';print_r($transfersArray)."<br>";
            $deposits_data = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("K1","K2","C1","C2","C3","C4","DD","ZD","CD","AX","DI","ED","F2","E9")');
            $depositsArray = json_decode(json_encode($deposits_data), true);
            //echo '<pre>';print_r($depositsArray)."<br>";exit;
            $store_tranaction_data = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("55","86","AR","CS","EE","GE","L9","NV","OB","OD","OG","OI","OK","OT","PG","RC","TD","VR","XP","56","AN","BZ","CV","FC","HD","LR","09","OC","OE","OH","OJ","OO","PC","R5","RY","VC","WC")');
            $store_tranactionArray = json_decode(json_encode($store_tranaction_data), true);
            $POT_data = DB::select('SELECT * from safeentry WHERE entry_date =  "'.$dateofinfo.'" and  item_id in ("AB","CX","ER","GA","M1","OM","P1","PZ","RB","RE","RG","RM","RO","RQ","SG","SM","TV","WI","CB","EA","FU","GH","MM","OZ","PV","RA","RD","RF","RH","RN","RP","RS","SL","T9","UI")');
            $potArray = json_decode(json_encode($POT_data), true);
            
            $dateofinfo = date("Y-m-d", strtotime(Input::get('dateofinfo')));
            $subtot1 = $subtot2 = $groctot = $drugtot = 0;
            $results = BookkeepRegisters::orderBy('reg_num')->get();

            foreach ($results as $row) 
            {
                $rs = DB::select('SELECT SUM(entry.item_amt) AS subtot1 FROM entry, items WHERE entry.reg_num = ? AND entry.entry_date = ? AND items.item_id = entry.item_id AND items.item_type = "D"', array($row->reg_num, $dateofinfo));
                if (is_null($rs[0]->subtot1)) 
                    {
                    $rs[0]->subtot1 = 0;
                    }
                $subtot1 = $rs[0]->subtot1;

                $rs = DB::select('SELECT SUM(entry.item_amt) AS subtot2 FROM entry, items WHERE entry.reg_num = ? AND entry.entry_date = ? AND items.item_id = entry.item_id AND items.item_type = "C"', array($row->reg_num, $dateofinfo));
                if (is_null($rs[0]->subtot2)) 
                    {
                    $rs[0]->subtot2 = 0;
                    }
                $subtot2 = $rs[0]->subtot2;

                if ($row->reg_type == 'G') 
                {
                    $groctot = $groctot + ($subtot1 - $subtot2);
                } 
                else if ($row->reg_type == 'D') 
                {
                    $drugtot = $drugtot + ($subtot1 - $subtot2);
                }
                $arrtot['groctot'] = $groctot;
                $arrtot['drugtot'] = $drugtot;
                $subtot1 = $subtot2 = 0;
            }
            $coin_data = DB::select('SELECT * from safe_coin where safe_date = "'.$dateofinfo.'" ');
            $currency_data = DB::select('SELECT * from safe_curr where safe_date = "'.$dateofinfo.'" ');
            $other_data = DB::select('SELECT * from safe_other where safe_date = "'.$dateofinfo.'" ');
            $begin_safe = DB::select('SELECT * from mainsafe where safe_date = "'.$dateofinfo.'" ');
                if($deliveriesArray || $transfersArray || $depositsArray || $store_tranactionArray || $potArray)
                {   
                    //echo 'Done';exit;
                    return View::make('mktmgr.safereportsdetail_update', compact('coin_data','currency_data','other_data','dateofinfo','deliveriesArray','transfersArray','depositsArray','store_tranactionArray','potArray','arrtot','begin_safe'));
                }
            return View::make('mktmgr.safereportsdetail', compact('coin_data','currency_data','other_data','dateofinfo','arrtot'));
        }
        
    }
    public function postSafeReportDetail()
    {
        $post_data = Input::all();
        //echo '<pre>';print_r($post_data);exit;
        $deliver_item_id = Input::get('deliver_item_id');
        $deliver_item_amt = Input::get('deliver_item_amt');
        $entry_date = date("Y-m-d", strtotime(Input::get('entry_date')));
        $deliver_item_id = Input::get('deliver_item_id');
        $delivery_amount = 0;
        for($i=0;$i<count($deliver_item_id);$i++)
        {   
            $delivery_amount  +=  $deliver_item_amt[$i];
            $deliver_id_entry = array(
                'item_id' => $deliver_item_id[$i],
                'item_amt' => $deliver_item_amt[$i],
                'source_id' => '',
                'entry_date'=>$entry_date
                );
            DB::table('safeentry')->insert($deliver_id_entry);
        }
        //echo 'Delivery Amount '.$delivery_amount."<br>";
        //echo '<pre>';print_r($deliver_id_entry)."<br>";
        $transfer_item_id = Input::get('transfer_item_id');
        $transfer_item_amt = Input::get('transfer_item_amt');
        $transfer_source_id = Input::get('transfer_source_id');
        $transfer_amount = 0;
        for($i=0;$i<count($transfer_item_id);$i++)
        {
            $transfer_amount  +=  $transfer_item_amt[$i];
            $transfer_entry = array(
                'item_id' => $transfer_item_id[$i],
                'item_amt' => $transfer_item_amt[$i],
                'source_id' => $transfer_source_id[$i],
                'entry_date'=>$entry_date
                );
            DB::table('safeentry')->insert($transfer_entry);
        }
        

        $deposits_item_id = Input::get('deposits_item_id');
        $deposits_item_amt = Input::get('deposits_item_amt');
        $deposit_amount = 0;
        for($i=0;$i<count($deposits_item_id);$i++)
        {
            $deposit_amount  +=  $deposits_item_amt[$i];
            $deposit_entry = array(
                'item_id' => $deposits_item_id[$i],
                'item_amt' => $deposits_item_amt[$i],
                'source_id' => '',
                'entry_date'=>$entry_date
                );
            DB::table('safeentry')->insert($deposit_entry);
        }
        //echo 'Deposit Amount '.$deposit_amount;exit;
        //echo '<pre>';print_r($deposit_entry)."<br>";exit;
        
        
        $store_item_id = Input::get('store_item_id');
        $store_item_amt = Input::get('store_item_amt');
        $store_amount = 0;
        for($i=0;$i<count($store_item_id);$i++)
        {
            $store_amount  +=  $store_item_amt[$i];
            $store_entry = array(
                'item_id' => $store_item_id[$i],
                'item_amt' => $store_item_amt[$i],
                'source_id' => '',
                'entry_date'=>$entry_date
                );
            DB::table('safeentry')->insert($store_entry);
        }


        $pot_item_id = Input::get('pot_item_id');
        $pot_item_amt = Input::get('pot_item_amt');
        $pot_amount = 0;
        for($i=0;$i<count($pot_item_id);$i++)
        {
            $pot_amount  +=  $pot_item_amt[$i];
            $pot_entry = array(
                'item_id' => $pot_item_id[$i],
                'item_amt' => $pot_item_amt[$i],
                'source_id' => '',
                'entry_date'=>$entry_date
                );
            DB::table('safeentry')->insert($pot_entry);
        }

        $del_tranfs = $delivery_amount + $transfer_amount; 
        $tot_acct = Input::get('final_total');
        $groc_os = Input::get('groc_os');
        $drug_os = Input::get('drug_os');
        $begin_safe = Input::get('beginningsafe');
        if(!empty($begin_safe))
        {
            $begin_safe = Input::get('beginningsafe');
        }
        else
        {
            $begin_safe = 0;   
        }
        if(!empty($groc_os))
        {
            $groc_os = Input::get('beginningsafe');
        }
        else
        {
            $groc_os = 0;   
        }
        if(!empty($drug_os))
        {
            $drug_os = Input::get('beginningsafe');
        }
        else
        {
            $drug_os = 0;   
        }
        $safe_cnt = Input::get('safe_count_total');
        $safe_os = $safe_cnt - $tot_acct;
        //$store_total_over = Input::get('store_total_over');
        $store_total_over = $safe_os + $groc_os + $drug_os;
        $mainsafe = array(
                'begin_safe' => $begin_safe,
                'del_tranfs' => $del_tranfs,
                'deposits' => $deposit_amount,
                'store_tx' => $store_amount,
                'po_types' => $pot_amount,
                'tot_acct' => $tot_acct,
                'safe_cnt' => $safe_cnt,
                'safe_os' => $safe_os,
                'groc_os' => $groc_os,
                'drug_os' => $drug_os,
                'total' => $store_total_over,
                'safe_date' => $entry_date
            );
      $mainsafe_entry =  DB::table('mainsafe')->insert($mainsafe);
      if($mainsafe_entry)
      {
        Session::flash('alert-success', 'Total Safe Report Details added successfully!'); 
        return Redirect::route('mktmgr-booksafereport');
      }


    }
    public function postSafeReportDetailUpdate()
    {
        $deliver_item_id = Input::get('deliver_item_id');
        $deliver_item_amt = Input::get('deliver_item_amt');
        $entry_date = date("Y-m-d", strtotime(Input::get('entry_date')));
        $deliver_item_id = Input::get('deliver_item_id');
        $delivery_amount = 0;
        for($i=0;$i<count($deliver_item_id);$i++)
        {   
            $delivery_amount  +=  $deliver_item_amt[$i];
            $delivery_update = DB::table('safeentry')
            ->where('item_id', $deliver_item_id[$i])
            ->where('entry_date', $entry_date)
           ->update(array(
                'item_amt' => $deliver_item_amt[$i],
                'source_id' => ''
                ));
         }  

           
        $transfer_item_id = Input::get('transfer_item_id');
        $transfer_item_amt = Input::get('transfer_item_amt');
        $transfer_source_id = Input::get('transfer_source_id');
        $entry_date = date("Y-m-d", strtotime(Input::get('entry_date')));
        $transfer_item_id = Input::get('transfer_item_id');
        $transfer_amount = 0;
        for($i=0;$i<count($transfer_item_id);$i++)
        {   
            $transfer_amount  +=  $transfer_item_amt[$i];
            $transfer_update = DB::table('safeentry')
            ->where('item_id', $transfer_item_id[$i])
            ->where('entry_date', $entry_date)
           ->update(array(
                'item_amt' => $transfer_item_amt[$i],
                'source_id' => $transfer_source_id[$i]
                ));  
            
        }


        $deposits_item_id = Input::get('deposits_item_id');
        $deposits_item_amt = Input::get('deposits_item_amt');
        $entry_date = date("Y-m-d", strtotime(Input::get('entry_date')));
        $deposit_amount = 0;
        for($i=0;$i<count($deposits_item_id);$i++)
        {   
            $deposit_amount  +=  $deposits_item_amt[$i];
            $deposit_update = DB::table('safeentry')
            ->where('item_id', $deposits_item_id[$i])
            ->where('entry_date', $entry_date)
           ->update(array(
                'item_amt' => $deposits_item_amt[$i],
                'source_id' => ''
                ));  
        }


        $store_item_id = Input::get('store_item_id');
        $store_item_amt = Input::get('store_item_amt');
        $entry_date = date("Y-m-d", strtotime(Input::get('entry_date')));
        $store_amount = 0;
        for($i=0;$i<count($store_item_id);$i++)
        {   
            $store_amount  +=  $store_item_amt[$i];
            $store_update = DB::table('safeentry')
            ->where('item_id', $store_item_id[$i])
            ->where('entry_date', $entry_date)
           ->update(array(
                'item_amt' => $store_item_amt[$i],
                'source_id' => ''
                ));  
        }


        $pot_item_id = Input::get('pot_item_id');
        $pot_item_amt = Input::get('pot_item_amt');
        $entry_date = date("Y-m-d", strtotime(Input::get('entry_date')));
        $pot_amount = 0;
        for($i=0;$i<count($pot_item_id);$i++)
        {   
            $pot_amount  +=  $pot_item_amt[$i];
            $pot_update = DB::table('safeentry')
            ->where('item_id', $pot_item_id[$i])
            ->where('entry_date', $entry_date)
           ->update(array(
                'item_amt' => $pot_item_amt[$i],
                'source_id' => ''
                ));  
        }
        $del_tranfs = $delivery_amount + $transfer_amount;
        $beginningsafe = Input::get('beginningsafe');
        $tot_acct = Input::get('final_total') + $beginningsafe;
        $safe_cnt = Input::get('safe_count_total');
        $safe_os = $safe_cnt - $tot_acct;
        $pot_update = DB::table('mainsafe')
            //->where('item_id', $pot_item_id[$i])
            ->where('safe_date', $entry_date)
           ->update(array(
                'begin_safe' => '',
                'del_tranfs' => $del_tranfs,
                'deposits' => $deposit_amount,
                'store_tx' => $store_amount,
                'po_types' => $pot_amount,
                'tot_acct' => $tot_acct,
                'safe_cnt' => $safe_cnt,
                'safe_os' => $safe_os,
                'groc_os' => '',
                'drug_os' => '',
                'total' => ''
                ));  
        // $mainsafe = array(
        //         'begin_safe' => '',
        //         'del_tranfs' => $del_tranfs,
        //         'deposits' => $deposit_amount,
        //         'store_tx' => $store_amount,
        //         'po_types' => $pot_amount,
        //         'tot_acct' => $tot_acct,
        //         'safe_cnt' => $safe_cnt,
        //         'safe_os' => $safe_os,
        //         'groc_os' => '',
        //         'drug_os' => '',
        //         'total' => '',
        //         'safe_date' => $entry_date
        //     );
      //$mainsafe_entry =  DB::table('mainsafe')->insert($mainsafe);
    //if($mainsafe_entry)
    //  {
        Session::flash('alert-success', 'Total Safe Report Details Updated successfully!'); 
        return Redirect::route('mktmgr-booksafereport');
    //  }

    }
    
    public function getSafeReportDetail1()
    {
        return View::make('mktmgr.safereportsdetail');
    }
}	