<?php
class UserSlcscn01Controller extends BaseController
{
	
	// get functions for slc user start
public function getslcSlic()
	{

		return View::make('slcscn01.slcslic');
	}
	public function getslcAislescan()
	{

		return View::make('slcscn01.slcaislescan');
	}
	public function getslcQueryitem()
	{

		return View::make('slcscn01.slcqueryitem');
	}
	public function getslcTagupdtqry()
	{

		return View::make('slcscn01.slctagupdtqry');
	}
	public function getslcApplybatch()
	{

		return View::make('slcscn01.slcapplybatch');
	}

	// get functions for slc user end
}

?>