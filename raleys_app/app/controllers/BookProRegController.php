<?php
class BookProRegController extends BaseController
{
	public function getBookProRegister()
	{
		$registers_num=DB::select("SELECT * FROM registers order by 1");
        return View::make('mktmgr.bookkeeperregister', compact('registers_num'));
	}
	public function postBookProRegister()
	{
		$validator = Validator::make(Input::all(),
	            array(
	                   'dateofreg'=>'required'
	                ),array('dateofreg.required'=>'Date is required'));
			if($validator->fails())
	        {          
			    return Redirect::route('mktmgr-bookkeeperregister')
	                    ->withErrors($validator)
	                    ->withInput();
	        }
	        else
	        {
	        		

			$reg_number = Input::get('reg_number');
			$loandate = Input::get('dateofreg');
			
			$approvalsOk = $this->approvalsOk($loandate);
			if($approvalsOk)
			{	
				$lastAprvlDate = date('m/d/Y',strtotime($approvalsOk));
				Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
	            return Redirect::route('mktmgr-bookkeeperregister');
			}

			$dayIsLocked = $this->approvalsOk1($loandate);
			$file_locked = $this->file_locked($loandate);
			if($file_locked)
	    	{
	    		$file_locked = "Y";
	    	}
	    	else
	    	{
	    		$file_locked = "N";
	    	}
			$begin_loan_data = DB::select('SELECT reg_num, food_stamps/100 as food_stamps, ones/100 as ones, fives/100 as fives, tens/100 as tens, 
									   twenties/100 as twenties, rolled_coins/100 as rolled_coins, quarters/100 as quarters,
									   nickles/100 as nickles, dimes/100 as dimes, pennies/100 as pennies, misc/100 as misc, 
									   bl_total/100 as bl_total, entry_date from begin_loan 
										WHERE begin_loan.reg_num = "'.$reg_number.'" 
										AND begin_loan.entry_date = "'.$loandate.'" ');
			if($begin_loan_data)
			{	
				$bl_total = $this->sum_bl_total($begin_loan_data);

				$upd_check_out = $this->upd_check_out($loandate,$reg_number,$bl_total);
				$upd_loans = $this->upd_loans($loandate,$reg_number,$bl_total);
				//echo $bl_total;exit;
				return View::make('mktmgr.bookproregisterform_update', compact('loandate','reg_number','begin_loan_data','bl_total','dayIsLocked','file_locked','msg'));
			}
			else
			{
				$file_locked = $this->file_locked($loandate);
				//echo $file_locked;exit;
				if(!($file_locked))
				{
					//echo 'Insert Data';exit;
					$bl_total = $this->sum_bl_total($begin_loan_data);
					$sql=DB::insert('INSERT INTO begin_loan(reg_num, food_stamps, ones, fives, tens, twenties, rolled_coins, quarters,nickles, dimes, pennies, misc, bl_total, entry_date)VALUES("'.$reg_number.'",0,0,0,0,0,0,0,0,0,0,0,0,"'.$loandate.'")');
					$beginloandata['food_stamps']='0.00';
					$beginloandata['ones']='0.00';
					$beginloandata['fives']='0.00';
					$beginloandata['tens']='0.00';
					$beginloandata['twenties']='0.00';
					$beginloandata['rolled_coins']='0.00';
					$beginloandata['quarters']='0.00';
					$beginloandata['nickles']='0.00';
					$beginloandata['dimes']='0.00';
					$beginloandata['pennies']='0.00';
					$beginloandata['misc']='0.00';
					$beginloandata['bl_total']='0.00';
					//echo $sql;exit;
					return View::make('mktmgr.bookproregisterform', compact('loandate','reg_number','beginloandata','dayIsLocked','msg'));
				}
				$beginloandata['food_stamps']='0.00';
					$beginloandata['ones']='0.00';
					$beginloandata['fives']='0.00';
					$beginloandata['tens']='0.00';
					$beginloandata['twenties']='0.00';
					$beginloandata['rolled_coins']='0.00';
					$beginloandata['quarters']='0.00';
					$beginloandata['nickles']='0.00';
					$beginloandata['dimes']='0.00';
					$beginloandata['pennies']='0.00';
					$beginloandata['misc']='0.00';
					$beginloandata['bl_total']='0.00';
				return View::make('mktmgr.bookproregisterform', compact('loandate','reg_number','beginloandata','dayIsLocked','msg'));
			}
		}	
	}
	public function convertDateToMDY($date) 
	{
    	return date("m/d/Y", strtotime($date));
    }
	public function approvalsOk($loandate)
	{
		//echo 'select max(date_stamp) as lastAprvlDate from mgmt where  mgmt.date_stamp < "'.$loandate.'" ';exit;
		$previewsdate = date('Y-m-d', strtotime($loandate .' -1 day'));
		//echo $previewsdate.'<br>';

		$results = DB::select('select max(date_stamp) as lastAprvlDate from mgmt where  mgmt.date_stamp < "'.$loandate.'" ');
		$resultArray = json_decode(json_encode($results), true); 
        $lastAprvlDate=$resultArray[0]['lastAprvlDate'];
        //echo $lastAprvlDate.'<br>';
        if($previewsdate <> $lastAprvlDate)
        {
        	return $lastAprvlDate;
        }
        // else
        // {
        // 	echo 'Ok';exit;
        // }
	}
	/*
	public function approvalsOk1($date)
	{
		$now = time(); // or your date as well
		$datediff = $now - strtotime($date);
		$numdays=floor($datediff / (60 * 60 * 24));
		if($numdays > 1)
		{	
			//echo 'select * from mgmt where  mgmt.date_stamp ="'.$date.'"';exit;
			$sql=DB::select('select * from mgmt where  mgmt.date_stamp ="'.$date.'"');
			$check_mgmt = json_decode(json_encode($sql), true);
			if(empty($check_mgmt))
			{
				return true;
			}
			else
			{
				return false;;
			}
		}
		else
		{
			return false;
		}
	}
	*/
	public function approvalsOk1($date)
	{
		$dayIsLocked = "N";
		$now = time(); // or your date as well
		$datediff = $now - strtotime($date);
		//echo $datediff;exit;
		$numdays=floor($datediff / (60 * 60 * 24));
		//echo $numdays;exit;
		//echo 'select * from mgmt where  mgmt.date_stamp ="'.$safe_date.'"';exit;
		if($numdays > 1)
		{
			//echo $numdays;exit;
			$mgmt=DB::select('select * from mgmt where  mgmt.date_stamp ="'.$date.'"');
			$check_mgmt = json_decode(json_encode($mgmt), true);
			//echo count($sql);exit;
			if($check_mgmt)
			{
				$dayIsLocked = "Y";
			}
		}
		return $dayIsLocked;
	}
	public function dayIsLocked($loandate)
	{
		$now = time(); 
		$datediff = $now - strtotime($loandate);
		$numdays=floor($datediff / (60 * 60 * 24));
		if($numdays > 1)
			{
				$sql=DB::select('select * from mgmt where  mgmt.date_stamp ="'.$loandate.'" ');
				if(empty($sql))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
	}
	/*
	public function file_locked($loandate)
	{
		//echo $loandate;exit;
		$sql=DB::select('SELECT *  FROM locks  WHERE locks.date_stamp = "'.$loandate.'" ');
		if(empty($sql))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	*/
	public function file_locked($stamp_date)
	{
		$file_locked=DB::select('SELECT * from locks  WHERE locks.date_stamp = "'.$stamp_date.'" ');
		if($file_locked)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	public function upd_check_out($loandate,$reg_number,$bl_total)
	{
		//echo $bl_total;exit;
		$totreg = DB::select('SELECT reg_num FROM registers');
		$totreg_count = json_decode(json_encode($totreg), true); 
		for($i=0;$i<count($totreg_count);$i++)
		{
			$entry_table = DB::select('SELECT * FROM entry WHERE entry.item_id = "BL"      AND entry.entry_date = "'.$loandate.'" AND entry.reg_num = '.$totreg_count[$i]['reg_num'].' ');
			 $entry_table = json_decode(json_encode($entry_table), true); 
			 if(empty($entry_table))
			 {
			 	DB::insert('INSERT INTO entry VALUES("BL",0,'.$totreg_count[$i]['reg_num'].',"book1","'.$loandate.'")');
			 }
		}
		//echo 'SELECT * FROM entry WHERE entry.item_id = "BL"   AND entry.entry_date = "'.$loandate.'"
        //AND entry.reg_num = "'.$reg_number.'" ';exit;
		$reg_number_check = DB::select('SELECT * FROM entry WHERE entry.item_id = "BL"   AND entry.entry_date = "'.$loandate.'"
        AND entry.reg_num = "'.$reg_number.'" ');
        $reg_number_check = json_decode(json_encode($reg_number_check), true);
        if(empty($reg_number_check))
        {
        	DB::insert('INSERT INTO entry VALUES("BL","'.$bl_total.'",
                 "'.$reg_number.'","book1","'.$loandate.'")');
        }
        else
        {
        	DB::update('UPDATE entry SET entry.item_amt = "'.$bl_total.'" WHERE entry.item_id = "BL" AND entry.entry_date = "'.$loandate.'"
           AND entry.reg_num = "'.$reg_number.'"');
        }
        //echo 'SELECT SUM(entry.item_amt) as total FROM entry  WHERE entry.entry_date = "'.$loandate.'"  AND entry.item_id = "BL" ';exit();
         $entry_itemamt_sum = DB::select('SELECT SUM(entry.item_amt) as total FROM entry  WHERE entry.entry_date = "'.$loandate.'"  AND entry.item_id = "BL" ');
          $entry_itemamt_total = json_decode(json_encode($entry_itemamt_sum), true);
         $entry_itmamt_total = $entry_itemamt_total[0]['total'];
         //echo $entry_itmamt_total;exit;
        $safe_other_query = DB::select('SELECT * FROM safe_other  WHERE safe_other.safe_date = "'.$loandate.'" ');
        $safe_other_check = json_decode(json_encode($safe_other_query), true);
        if(empty($safe_other_check))
        {
        	DB::insert('INSERT INTO safe_other VALUES(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"'.$entry_itmamt_total.'",
                0,0,0,0,0,0,0,0,0,0,0,0,0,"'.$loandate.'")');
        }	
        else
        {
        	DB::update('UPDATE safe_other SET safe_other.begin_loans = "'.$entry_itmamt_total.'"
         		WHERE safe_other.safe_date = "'.$loandate.'" ');
        }
    }


  	public function upd_loans($loandate,$reg_number,$bl_total)
    {
    	$next_day =  date('Y-m-d', strtotime('+1 day', strtotime($loandate)));
    	// echo 'SELECT * FROM loans WHERE loans.item_id = "BL" AND loans.entry_date = "'.$next_day.'"
     //    AND loans.reg_num = "'.$reg_number.'" ';exit;
    	//echo 'SELECT * FROM loans WHERE loans.item_id = "BL" AND loans.entry_date = "'.$next_day.'"
        //AND loans.reg_num = "'.$reg_number.'" ';exit;
    	$check_loans = DB::select('SELECT * FROM loans WHERE loans.item_id = "BL" AND loans.entry_date = "'.$next_day.'"
        AND loans.reg_num = "'.$reg_number.'" ');
        $check_loans_array = json_decode(json_encode($check_loans), true);
        if(empty($check_loans_array))
        {
        	DB::insert('INSERT INTO loans VALUES("BL","'.$bl_total.'",
                 "'.$reg_number.'","book1","'.$next_day.'")');
        }
        else
        {
        	DB::update('UPDATE loans SET loans.item_amt = "'.$bl_total.'" WHERE loans.item_id = "BL" AND loans.entry_date = "'.$next_day.'"
           AND loans.reg_num = "'.$reg_number.'"');
        }
        $check_loans_LR = DB::select('SELECT * FROM loans WHERE loans.item_id = "LR" AND loans.entry_date = "'.$next_day.'"
        AND loans.reg_num = "'.$reg_number.'" ');
        $check_loans_LR_array = json_decode(json_encode($check_loans_LR), true);
        if(empty($check_loans_LR_array))
        {
        	DB::insert('INSERT INTO loans VALUES("LR","'.$bl_total.'",
                 "'.$reg_number.'","book1","'.$next_day.'")');
        }
        else
        {
        	$temp_loan_amount = DB::select('SELECT SUM(loans.item_amt) as temp FROM loans WHERE loans.entry_date = "'.$next_day.'" AND loans.reg_num = "'.$reg_number.'" AND loans.item_id not in ("LR", "GT") AND loans.item_id <> "LR" AND loans.item_id <> "GT" ');
        	$temp_loan_amount_array = json_decode(json_encode($temp_loan_amount), true);
        	if(!empty($temp_loan_amount_array))
	        {	
	        	$temp_item_amt = $temp_loan_amount_array[0]['temp'];
	        	DB::update('UPDATE loans SET loans.item_amt = "'.$temp_item_amt.'"
				        	 	WHERE loans.item_id = "LR"
				           		AND loans.entry_date = "'.$next_day.'"
				           		AND loans.reg_num = "'.$reg_number.'" ');
	        }
        } 

    }
	public function RegBeginLoanTotal()
	{	
		//echo '<pre>';print_r(Input::all());exit;
		$validator = Validator::make(Input::all(),
            array(
                   'dateofinfo'=>'required',
                ),array('dateofinfo.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-bookkeeperregistertotal')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$dateofinfo =Input::get('dateofinfo');
        	//$reg_num = ''; 
        	//$itemid= ''; 
        	$approvalsOk = $this->approvalsOk($dateofinfo);
			if($approvalsOk)
			{	
				$lastAprvlDate = date('m/d/Y',strtotime($approvalsOk));
				Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
	            return Redirect::route('mktmgr-bookkeeperregistertotal');
			}
			// $approvalsOk1 = $this->approvalsOk1($dateofinfo);
			// if(!empty($approvalsOk1))
			// {	
			// 	//echo 'Data is Locked';exit;
			// 	$dayIsLocked = "Y";
			// 	$seldate=$this->convertDateToMDY($dateofinfo);
			// 	$msg='Data is locked for '.$seldate.'.Any changes won\'t be saved.';
			// }
			// else
	  //   	{
	  //   		$dayIsLocked = "N";
	  //   		$msg = '';
	  //   	}
        	//$itemname = ''; 
        	//$dayIsLocked = 'N';
			//$dateofinfo = Input::get('dateofinfo');
			$reg_begintotal = DB::select('select r.reg_num, b.bl_total/100 as bl_total from   registers r  LEFT OUTER JOIN begin_loan b
	  			on   b.reg_num = r.reg_num
	    		and   b.entry_date = "'.$dateofinfo.'"
	  			order by r.reg_num');
			$reg_begintotal_data = json_decode(json_encode($reg_begintotal), true);
			return View::make('mktmgr.regbeginloantotal',compact('dateofinfo','reg_begintotal_data'));
		}
	}
	public function sum_bl_total($begin_loan_data)
	{
	 	if(!empty($begin_loan_data))
	 	{
	 			$tot = $begin_loan_data[0]->food_stamps + $begin_loan_data[0]->ones
	 	 			+ $begin_loan_data[0]->fives + $begin_loan_data[0]->tens
		    	    + $begin_loan_data[0]->twenties + $begin_loan_data[0]->rolled_coins
	 	   			+ $begin_loan_data[0]->quarters + $begin_loan_data[0]->dimes
	 	   			+ $begin_loan_data[0]->nickles + $begin_loan_data[0]->pennies
	 	   			+ $begin_loan_data[0]->misc;
	 	
	 		return $tot*100;
	 	}
	 	else
	 	{
	 		return '0.00';
	 	}
 	}
 	public function postBookProRegisterAdd()
 	{
 		//echo '<pre>';print_r(Input::all());exit;
 	}
	public function postBookProRegisterUpdate()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$reg_num = Input::get('reg_num');
		$entry_date = Input::get('entry_date');
		$food_stamps = Input::get('food_stamps')*100;
		$ones = Input::get('ones')*100;
		$fives = Input::get('fives')*100;
		$tens = Input::get('tens')*100;
		$twenties = Input::get('twenties')*100;
		$rolled_coins = Input::get('rolled_coins')*100;
		$quarters = Input::get('quarters')*100;
		$dimes = Input::get('dimes')*100;
		$nickles = Input::get('nickles')*100;
		$pennies = Input::get('pennies')*100;
		$misc = Input::get('misc')*100;
		$bl_total = Input::get('bl_total')*100;
		
		$register_update = DB::table('begin_loan')
            ->where('reg_num', $reg_num)
            ->where('entry_date', $entry_date)
           	->update(array(
                'food_stamps' => $food_stamps,
                'ones' => $ones,
                'fives' => $fives,
                'tens' => $tens,
                'twenties' => $twenties,
                'rolled_coins' => $rolled_coins,
                'quarters' => $quarters,
                'dimes' => $dimes,
                'nickles' => $nickles,
                'pennies' => $pennies,
                'misc' => $misc,
                'bl_total' =>$bl_total
                ));
           	//echo '<pre>';print_r($register_update);exit;
        // if($register_update)
        // {
        	Session::flash('alert-success', 'Register Details Updated Successfully!'); 
          	return Redirect::route('mktmgr-bookkeeperregister');
       // }   	
	}
	public function RegCheckOutItem()
	{
		$itemslist = DB::select('SELECT items.item_id, items.item_desc,items.item_type FROM items WHERE items.item_id NOT IN ("#C","DS","GT","OS","LR","BL","TD","TC") AND items.disable = "N" AND items.item_type IN ("D","C") order by items.item_id');	
		return View::make('mktmgr.bookregcheckoutitem', compact('itemslist')); 
	}
	public function RegisterCheckOutItems()
	{

		$validator = Validator::make(Input::all(),
            array(
                   'itemid_name' =>'required',
                   'dateofinfo'=>'required'
                ),array('itemid_name.required'=>'Category code is required',
                		'dateofinfo.required'=>'Date is required'));
	 	
	 	if($validator->fails())
        {
            return Redirect::route('mktmgr-regcheckoutitem')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        { 
        	$itemid_name = Input::get('itemid_name');
        	$dateofinfo = Input::get('dateofinfo');
        	$itemid_name = explode('^',$itemid_name);
        	$item_id = $itemid_name[0];
        	$item_name = $itemid_name[1];
        	$approvalsOk = $this->approvalsOk($dateofinfo);
			if($approvalsOk)
			{	
				$lastAprvlDate = date('m/d/Y',strtotime($approvalsOk));
				Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
	            return Redirect::route('mktmgr-regcheckoutitem');
			}
        	$dayIsLocked = $this->approvalsOk1($dateofinfo);
        	//echo $dayLocked;exit;
        	$file_locked = $this->file_locked($dateofinfo);
	        if($file_locked)
        	{
        		$file_locked = "Y";
        	}
        	else
        	{
        		$file_locked = "N";
        	}
        	//echo $file_locked;exit;
        	//echo $dayIsLocked;exit;
        	$resultArray=$this->add_automate1($item_id,$dateofinfo);
        	return View::make('mktmgr.regcheckoutitems', compact('resultArray','dateofinfo','item_name','dayIsLocked','file_locked','msg','item_id')); 
		}	
	}
	public function updateRegCheckOutItem()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$reg_num = Input::get('reg_num');
		$entry_date = Input::get('entry_date');
		$item_id = Input::get('item_id');
		$item_desc = Input::get('item_desc');
		$item_amt = Input::get('item_amt');
		for($i = 0;$i<count($reg_num);$i++)
		{
			$checkdata = DB::select('select * from entry where  item_id = "'.$item_id.'" and reg_num = '.$reg_num[$i].' and entry_date = "'.$entry_date.'" ');
			if($checkdata)
			{
				DB::update('update entry set item_amt = '.($item_amt[$i]*100).' where item_id = "'.$item_id.'" and reg_num = '.$reg_num[$i].' and entry_date = "'.$entry_date.'" ' );
			}
			else
			{
				DB::insert('insert into entry (item_id, item_amt, reg_num, source_id, entry_date)
                    values ("'.$item_id.'", '.($item_amt[$i]*100).','.$reg_num[$i].',
                           "book1",  "'.$entry_date.'") ');	
			}
			
		}//exit;
		Session::flash('alert-success', 'Items Details Updated Successfully!'); 
          	return Redirect::route('mktmgr-regcheckoutitem');
	}
	public function getRegCheckOutTotal()
	{
		$itemslist = DB::select('SELECT items.item_id, items.item_desc 
         FROM items
        WHERE items.item_id IN ("#C","GT","OS","LR","BL","TD","TC")
          AND items.disable = "N"
        order by items.item_id');	
		return View::make('mktmgr.bookregcheckouttotl', compact('itemslist')); 
	}
	public function postRegCheckOutTotal()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$validator = Validator::make(Input::all(),
            array(
                   'itemid_name'=>'required',
                   'dateofinfo'=>'required'
                ),array('itemid_name.required'=>'Category code is required',
                'dateofinfo.required'=>'Date is required'));
		if($validator->fails())
        {          
					return Redirect::route('mktmgr-regcheckouttotl')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	//echo '<pre>';print_r(Input::all());exit;
        	$itemid_name = Input::get('itemid_name'); 
        	$itemid_name = explode('^',$itemid_name);
        	$dateofinfo = Input::get('dateofinfo');
        	$itemid = $itemid_name[0]; 
        	$itemname = $itemid_name[1];
        	switch ($itemid)
	 	  	{
	 	  		case "TD" :
				    $resultArray=$this->calc_totals("D",$dateofinfo);
				    $itemname=$itemid_name[1];
				    return View::make('mktmgr.regcheckouttotal',compact('resultArray', 'dateofinfo', 'itemid', 'itemname'));
				    break;	
		        case "TC" :
			        $resultArray=$this->calc_totals("C",$dateofinfo);
			        $itemname=$itemid_name[1];
			        return View::make('mktmgr.regcheckouttotal',compact('resultArray', 'dateofinfo', 'itemid', 'itemname'));
			        break;
		        case "OS" :
			        $overshort=$this->calc_ovshrt($dateofinfo);
			        $resultArray = $overshort[0];
			        $groctot = $overshort[1];
			        $drugtot = $overshort[2];
			        $itemname=$itemid_name[1];
			        return View::make('mktmgr.regcheckouttotal_os',compact('resultArray', 'groctot', 'drugtot', 'dateofinfo', 'itemid', 'itemname'));
			        break;
		        case "GT" :
			        $resultArray=$this->calc_grandtot($dateofinfo);
			        return View::make('mktmgr.regcheckouttotalgt',compact('resultArray', 'dateofinfo', 'itemid', 'itemname'));
			        break;
		        case "#C" :
			        $resultArray=$this->add_automate('#C',$dateofinfo);
			        $itemname=$itemid_name[1];
			        return View::make('mktmgr.regcheckouttotalremitems',compact('resultArray', 'dateofinfo', 'itemid', 'itemname'));
			        break;
		        case "BL" :
		        $resultArray=$this->add_automate('BL',$dateofinfo);
		        $itemname=$itemid_name[1];
		        return View::make('mktmgr.regcheckouttotalremitems',compact('resultArray', 'dateofinfo', 'itemid', 'itemname'));
		        break;
		        case "LR" :
		        $resultArray=$this->add_automate('LR',$dateofinfo);
		        $itemname=$itemid_name[1];
		        return View::make('mktmgr.regcheckouttotalremitems',compact('resultArray', 'dateofinfo', 'itemid', 'itemname'));
		        break;
	 	  	}
        }
	}
	public function calc_totals($type,$tot_date)
	{
		$sql=DB::select('SELECT DISTINCT entry.reg_num FROM entry WHERE entry.entry_date= "'.$tot_date.'"
      		ORDER BY entry.reg_num');
		$totreg_count = json_decode(json_encode($sql), true);
		$resultsArray = array(); 
		for($i=0;$i<count($totreg_count);$i++)
		{
			$subtot = 0;
			$sql = DB::select('SELECT SUM(entry.item_amt)/100 as subtot,entry.reg_num  FROM entry,items
                WHERE entry.reg_num = '.$totreg_count[$i]['reg_num'].'
                  AND items.item_type = "'.$type.'"
                  AND entry.entry_date = "'.$tot_date.'" 
                  AND entry.item_id = items.item_id ');
			$results = json_decode(json_encode($sql), true);
			$resultsArray1['subtot'] = $results[0]['subtot'];
			$resultsArray1['reg_num'] = $results[0]['reg_num'];
			$checkempty = array_filter($resultsArray1);
			if (!empty($checkempty))
			{
			    $resultsArray[] = $resultsArray1;
			}
		}//exit;
		return $resultsArray;//exit;
	}
	public function calc_ovshrt($tot_date)
	{
		$sql=DB::select('SELECT DISTINCT entry.reg_num,registers.reg_type  FROM entry,registers WHERE entry.entry_date="'.$tot_date.'" AND entry.reg_num=registers.reg_num ORDER BY entry.reg_num');
		$totreg_count = json_decode(json_encode($sql), true);
		
		$resultsArray = array(); 
		$gsubtot = 0;
		$groctot = 0;
		$drugtot = 0;
		for($i=0;$i<count($totreg_count);$i++)
		{
			$subtot1 = 0;
			$subtot2 = 0;
			$sql1 = DB::select('SELECT SUM(entry.item_amt)/100 as subtot1,entry.reg_num FROM entry,items
		        WHERE entry.reg_num = '.$totreg_count[$i]['reg_num'].'
		          AND entry.entry_date = "'.$tot_date.'" 
		          AND items.item_id = entry.item_id
		          AND items.item_type = "D" ');
			$results1 = json_decode(json_encode($sql1), true);
			if (empty($results1))
			{
			    $resultsArray[] = 0;
			}
			$sql2 = DB::select('SELECT SUM(entry.item_amt)/100 as subtot2,entry.reg_num FROM entry,items
		        WHERE entry.reg_num = '.$totreg_count[$i]['reg_num'].'
		          AND entry.entry_date = "'.$tot_date.'" 
		          AND items.item_id = entry.item_id
		          AND items.item_type = "C" ');
			$results2 = json_decode(json_encode($sql2), true);
			if (empty($results2))
			{
			    $resultsArray[] = 0;
			}
			//$resultsArray1['gsubtot'] = $gsubtot + ($results1[0]['subtot1'] - $results2[0]['subtot2']);
			//$gsubtot = $gsubtot + ($results1[0]['subtot1'] - $results2[0]['subtot2']);
			$resultsArray1['reg_num'] = $totreg_count[$i]['reg_num'];
			$resultsArray1['item_amt'] = $results1[0]['subtot1'] - $results2[0]['subtot2'];
			if($totreg_count[$i]['reg_type'] == 'G')
			{
				$groctot = $groctot + ($results1[0]['subtot1'] - $results2[0]['subtot2']);
			}
			elseif($totreg_count[$i]['reg_type'] == 'D')
			{
				$drugtot = $drugtot + ($results1[0]['subtot1'] - $results2[0]['subtot2']);
			}
			//$results = json_decode(json_encode($sql1), true);
			//$resultsArray1['subtot1'] = $results[0]['subtot1'];
			//$resultsArray1['reg_num'] = $results[0]['reg_num'];
			$checkempty = array_filter($resultsArray1);
			if (!empty($checkempty))
			{
			    $resultsArray[] = $resultsArray1;
			}
		}//exit;
		return array($resultsArray,$groctot,$drugtot);
		//return $groctot;
		//return $drugtot;//exit;
	}
	public function calc_grandtot($tot_date)
	{
		$debit_credit_os_total = array();
		$resultsArray = array();
		$tot_items = DB::select('SELECT COUNT(*) as tot_items FROM items WHERE items.disable = "N"
     		AND items.item_type <> "X" ');

		$d_tot = DB::select('SELECT items.* FROM items WHERE items.item_id NOT IN ("GT","DS","#C") AND items.item_type <> "X" AND items.disable = "N" ORDER BY  items.item_type,items.item_desc');
		$d_tot = json_decode(json_encode($d_tot), true);
		//echo '<pre>';print_r($d_tot);exit;
		//if()	
		foreach ($d_tot as $key) 
		{
	 		$total = 0;
	 		switch ($key['item_id'])
	 	  	{
		 	  	case "TC" :
		 	  		$tot_cred = $this->get_totdbcr('C',$tot_date);
		 	  		$debit_credit_os_total['tot_cred'] = $tot_cred; 
		 	  		break;
			    case "TD" :
			     	$tot_deb = $this->get_totdbcr('D',$tot_date);
			     	$debit_credit_os_total['tot_deb'] = $tot_deb;
			 	  	break;
			    case "OS" :
			     	$gsubtot=$this->get_os($tot_date);
			     	$debit_credit_os_total['tot_os'] = $gsubtot;
			     	break;
			    default:
			    		$valid_id = $this->valid_id($key['item_id']);
			    		if($valid_id === TRUE)
			    		{
			    			$total = $this->get_itemtot($key['item_id'], $tot_date);
					        if(empty($total))
					        {
					        	$total = 0;
					        }
					        else
					        {
					        	$msg = 'NOT A VALID ID...';
					        }	
			    		}
			         $resultsArray1['item_id'] = $key['item_id'];
			         $resultsArray1['item_desc'] = $key['item_desc'];  
			         $resultsArray1['item_amt'] = $total/100;
			         $resultsArray[] = $resultsArray1;
		           // $resultsArray[$i] = new stdClass();
		           // if($alldata == '0')
		           // {
			          //   $resultsArray[$i]->item_id = 0;
			          //   $resultsArray[$i]->gsubtot = 0;
			          //   $resultsArray[$i]->item_desc = 0;
		           // }
		           // else
		           // {
		           // 	   $itemid_name = explode("^", $alldata); 
			          //  $resultsArray[$i]->item_id = $itemid_name[0];
			          //  $resultsArray[$i]->gsubtot = $itemid_name[1];
			          //  $resultsArray[$i]->item_desc = $itemid_name[2];
		           // }

		       break;
		    }  
		    // $i=$i+1;        
	 	}
	 	
	 	return array($debit_credit_os_total,$resultsArray);
	}
	function get_totdbcr($type,$tot_date)
	 {	
	 	//echo $type.' '.$tot_date;exit;
	 	$sql = DB::select("SELECT  SUM(entry.item_amt)/100 as subtot
	 						FROM  entry,items
        					WHERE  entry.entry_date = '".$tot_date."' 
          					AND  entry.item_id = items.item_id 
          					AND  items.item_type = '".$type."' ");
	 	
	 	$results = json_decode(json_encode($sql), true);
	 	$subtot = $results[0]['subtot'];
	 	return $subtot;
	 }
	 public function get_os($tot_date)
	 {
	 	$gsubtot = 0;
		$subtot1 = 0;
		$subtot2 = 0;

		$sql1 = DB::select("SELECT SUM(entry.item_amt) as subtot1 FROM entry,items
       				WHERE entry.entry_date = '".$tot_date."'  
          			AND items.item_type = 'D'
          			AND entry.item_id = items.item_id ");
		$results1 = json_decode(json_encode($sql1), true);

        $sql2 = DB::select("SELECT SUM(entry.item_amt) as subtot2 FROM entry,items
       				WHERE entry.entry_date = '".$tot_date."'  
          			AND items.item_type = 'C'
          			AND entry.item_id = items.item_id ");
		$results2 = json_decode(json_encode($sql2), true);
		$gsubtot = ($results1[0]['subtot1'] - $results2[0]['subtot2'])/100;
		return $gsubtot;
	 }
	 public function get_itemtot($item_id,$tot_date)
	 {
	 	$gsubtot = 0;
	 	$sql = DB::select('SELECT sum(entry.item_amt) as gsubtot  FROM entry
        		WHERE entry.item_id = "'.$item_id.'"
          		AND entry.entry_date = "'.$tot_date.'" ');
	 	$results = json_decode(json_encode($sql), true);
		$gsubtot = $results[0]['gsubtot'];
		return $gsubtot;
	 }
	 public function valid_id($temp_id)
	 {
	 	$sql = DB::select('SELECT * FROM items WHERE items.item_id = "'.$temp_id.'" AND items.disable = "N" ');
	 	$results = json_decode(json_encode($sql), true);
	 	if(empty($results))
	 	{
	 		return false;
	 	}
	 	else
	 	{
	 		return true;
	 	}
	}
	public function add_automate($item_id,$tot_date)
	{
	 	$regdol_array = array();
		$sql = DB::select('select r.reg_num, e.item_amt/100 as item_amt 
	 							from registers r left outer join entry e
			 					on r.reg_num = e.reg_num
		    					and e.item_id = "'.$item_id.'"
		    					and e.entry_date = "'.$tot_date.'"
		  						order by 1 ');
	  	return	$itemAmtByRegCrs = json_decode(json_encode($sql), true);
	}
	public function add_automate1($item_id,$tot_date)
	{
	 	$regdol_array = array();
	 	$sql = DB::select('select r.reg_num, e.item_amt/100 as item_amt 
	 							from registers r left outer join entry e
			 					on r.reg_num = e.reg_num
		    					and e.item_id = "'.$item_id.'"
		    					and e.entry_date = "'.$tot_date.'"
		  						order by 1 ');
	 	$itemAmtByRegCrs = json_decode(json_encode($sql), true);
		return $itemAmtByRegCrs;
	}
	public function getLoantoRegTotal()
	{
		$loanitems = DB::select('SELECT loanitems.item_id, loanitems.item_desc FROM loanitems WHERE loanitems.item_type = "T" AND loanitems.item_id <> "BL" ');
		//$loanitems = json_decode(json_encode($loanitems), true);
		return View::make('mktmgr.bookloanrregtotal',compact('loanitems')); 
	}
	public function postLoantoRegTotal()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$validator = Validator::make(Input::all(),
            array(
                   'totcode' =>'required',
                   'dateofinfo'=>'required',
                ),array(
	                'totcode.required'=>'Total Code is required',
	                'dateofinfo.required'=>'Date is required'
                ));
		if($validator->fails())
	    {          
		  return Redirect::route('mktmgr-loantoregtotal')
	                    ->withErrors($validator)
	                    ->withInput();
	    }
	    else
	    {
	    	//echo '<pre>';print_r(Input::all());exit;
	    	$totcode = Input::get('totcode');
	    	$dateofinfo = Input::get('dateofinfo');
	    	$totcode = explode('^',$totcode);
	    	$item_id = $totcode[0];
	    	$item_name = $totcode[1];
	    	//echo $item_id.' '.$item_name;
	    	switch ($item_id)
	 	  	{
	 	  		case "LR" :
				    $resultArray=$this->disp_lrtotals("LR",$dateofinfo);
				    $itemname=$totcode[1];
				    return View::make('mktmgr.regloantoregtotal',compact('resultArray', 'dateofinfo', 'itemname'));
				    break;	
		        case "BL" :
			        $resultArray=$this->disp_bltot("BL",$dateofinfo);
			        $itemname=$totcode[1];
			        return View::make('mktmgr.regloantoregtotal',compact('resultArray', 'dateofinfo', 'itemname'));
			        break;
			    case "GT":
			    	 $resultArray=$this->calc_loangt($dateofinfo);   
			    	 $itemname=$totcode[1];
			        return View::make('mktmgr.loantoreg_gt_total',compact('resultArray', 'dateofinfo', 'itemname'));
			        break;
		    }

	    }
	}
	public function disp_lrtotals($id,$loandate)
	{
		$resultsArray = array();
		$registers = DB::select('SELECT registers.reg_num as reg_num FROM registers
	     							ORDER BY registers.reg_num ');
		$registers_array = json_decode(json_encode($registers), true);
		for($i=0;$i<count($registers_array);$i++)
		{	
			$resultArray1['reg_num'] = $registers_array[$i]['reg_num'];

			$item_amt_lr = DB::select('SELECT loans.item_amt/100 as item_amt FROM loans 
	           				WHERE  loans.item_id =  "LR"
	             			AND  loans.reg_num = '.$registers_array[$i]['reg_num'].'
							AND  loans.entry_date = "'.$loandate.'" ');

			$item_amt_lr_array = json_decode(json_encode($item_amt_lr), true);
			if(!empty($item_amt_lr_array))
			{
				$resultArray1['item_amt'] = $item_amt_lr_array[0]['item_amt'];
			}
			else
			{
				$item_amt_lr = DB::select('SELECT loans.item_amt/100 as item_amt FROM loans 
	           				WHERE  loans.item_id =  "BL"
	             			AND  loans.reg_num = '.$registers_array[$i]['reg_num'].'
							AND  loans.entry_date = "'.$loandate.'" ');
				$item_amt_bl_array = json_decode(json_encode($item_amt_lr), true);
				if(!empty($item_amt_bl_array))
				{
					$resultArray1['item_amt'] = $item_amt_bl_array[0]['item_amt'];
				}
				else
				{
					$resultArray1['item_amt'] = 0;
					DB::insert('INSERT INTO loans 
                    VALUES ("LR",
                            0,
                            '.$registers_array[$i]['reg_num'].',
                            "book1",
                            "'.$loandate.'")');
				}
			}
				$checkempty = array_filter($resultArray1);
				if (!empty($checkempty))
				{
				   $resultsArray[] = $resultArray1; 
				}	
		}
		return $resultsArray;
	}
	public function disp_bltot($id,$loandate)
	{
		$resultsArray = array();
		$registers = DB::select('SELECT registers.reg_num as reg_num FROM registers
	     							ORDER BY registers.reg_num ');
		$registers_array = json_decode(json_encode($registers), true);
		for($i=0;$i<count($registers_array);$i++)
		{	
			$resultArray1['reg_num'] = $registers_array[$i]['reg_num'];

			$item_amt_bl = DB::select('SELECT loans.item_amt/100 as item_amt FROM loans 
	           				WHERE  loans.item_id =  "BL"
	             			AND  loans.reg_num = '.$registers_array[$i]['reg_num'].'
							AND  loans.entry_date = "'.$loandate.'" ');
			$item_amt_bl_array = json_decode(json_encode($item_amt_bl), true);

			if(empty($item_amt_bl_array))
			{	
				if(!($this->file_locked($loandate)))
				{
					$resultArray1['item_amt'] = 0;
					DB::insert('INSERT INTO loans 
                    VALUES ("LR",
                            0,
                            '.$registers_array[$i]['reg_num'].',
                            "book1",
                            "'.$loandate.'")');
				}
				else
				{
					$resultArray1['item_amt'] = $item_amt_bl_array[0]['item_amt'];
					DB::update('UPDATE  loans 
                        SET  loans.item_amt = "'.$item_amt_bl_array[0]['item_amt'].'"
                      WHERE  loans.item_id =  "BL"
                        AND  loans.reg_num = '.$registers_array[$i]['reg_num'].'
                        AND  loans.entry_date = "'.$loandate.'" ');
				}
				
			}
				$checkempty = array_filter($resultArray1);
				if (!empty($checkempty))
				{
				   $resultsArray[] = $resultArray1; 
				}
		}
		//echo '<pre>';print_r($resultsArray);exit;
		return $resultsArray;
	}
	public function calc_loangt($loandate)
	{
		$resultsArray = array();
		$bl_tot_return_array = array();
		$lr_tot_return_array = array();
		$tot_items = DB::select('SELECT COUNT(*) as tot_items FROM loanitems
					          WHERE loanitems.item_type <> "T" ');
		$tot_items_array = json_decode(json_encode($tot_items), true);

		$loanitems = DB::select('SELECT loanitems.item_id, loanitems.item_desc  FROM loanitems  WHERE loanitems.item_id <> "GT" ');
		$loanitems_array = json_decode(json_encode($loanitems), true);
		//echo '<pre>';print_r($loanitems_array);exit;
		for($i=0;$i<count($loanitems_array);$i++)
		{
			$resultArray1['item_id'] = $loanitems_array[$i]['item_id'];
			$resultArray1['item_desc'] = $loanitems_array[$i]['item_desc'];
			//$total = 0;
			$loans_total = DB::select('SELECT  SUM(loans.item_amt)/100 as total FROM loans WHERE loans.item_id = "'.$loanitems_array[$i]['item_id'].'"
                  AND loans.entry_date = "'.$loandate.'" ');	
			$loans_total_array = json_decode(json_encode($loans_total), true);
			if($loanitems_array[$i]['item_id']=='BL')
			{	
				$bl_tot_return_array['item_id'] = $loanitems_array[$i]['item_id'];
				$bl_tot_return_array['item_desc'] = $loanitems_array[$i]['item_desc'];
				$bl_total = DB::select('SELECT  SUM(loans.item_amt)/100 as total FROM loans WHERE loans.item_id = "BL"
                  AND loans.entry_date = "'.$loandate.'" ');	
					$bl_total_array = json_decode(json_encode($bl_total), true);
				$bl_tot_return_array['item_amt'] = $bl_total_array[0]['total'];
			}
			elseif($loanitems_array[$i]['item_id']=='LR')
			{	
				$lr_tot_return_array['item_id'] = $loanitems_array[$i]['item_id'];
				$lr_tot_return_array['item_desc'] = $loanitems_array[$i]['item_desc'];
				$lr_total = DB::select('SELECT  SUM(loans.item_amt)/100 as total FROM loans WHERE loans.item_id = "LR"
                  AND loans.entry_date = "'.$loandate.'" ');	
					$lr_total_array = json_decode(json_encode($lr_total), true);
				$lr_tot_return_array['item_amt'] = $lr_total_array[0]['total'];
			}
			$resultArray1['item_amt'] = $loans_total_array[0]['total'];
			$checkempty = array_filter($resultArray1);
			if (!empty($checkempty))
			{
			   $resultsArray[] = $resultArray1; 
			}
		}	//echo '<pre>';print_r($resultsArray);exit;
		return array($resultsArray,$bl_tot_return_array,$lr_tot_return_array);
	}
	public function getLoantoRegRegister()
	{
		$registers_num=DB::select("select * from registers order by 1");
		return View::make('mktmgr.bookloanrregister', compact('registers_num'));
	}
	public function postLoantoRegRegister()
	{
		//echo '<pre>';print_r(Input::all());exit;
		$validator = Validator::make(Input::all(),
            array(
                   'reg_num' =>'required',
                   'dateofinfo'=>'required',
                ),array('reg_num.required'=>'Register Number is required','dateofinfo.required'=>'required'));
		if($validator->fails())
	    {          
		  return Redirect::route('mktmgr-loantoregregister')
	                    ->withErrors($validator)
	                    ->withInput();
        }
        else
        {
        	$loan_date = Input::get('dateofinfo');
        	$reg_num=Input::get('reg_num');


           	$approvalsOk = $this->approvalsOk($loan_date);
			if($approvalsOk)
			{	
				$lastAprvlDate = date('m/d/Y',strtotime($approvalsOk));
				Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
	            return Redirect::route('mktmgr-loantoregregister');
			}
        	$dayIsLocked = $this->approvalsOk1($loan_date);
        	$file_locked = $this->file_locked($loan_date);
	        	
        	if($file_locked)
        	{
        		$file_locked = "Y";
        	}
        	else
        	{
        		$file_locked = "N";
        	}
        	
        	$previewsdate =  date('Y-m-d', strtotime('-1 day'));
            
		    $backDays = DB::select('select value as backDays from config where `key` = "NO-OF-DAYS" ');
        	$backDays = json_decode(json_encode($backDays), true);
        	$backDays = $backDays[0]['backDays'];
        	if($backDays <> 0)
        	{
        		$backDays = 1;
        	}
        	//echo $backDays;exit;
        	$bl_tot = 0;	
        	$noOfDays = 0;
        	$x = 1; 

			while(($bl_tot == 0) && ($noOfDays < $backDays)) 
			{
			   $noOfDaystocmp = "-".$noOfDays." days";
			   $entry_date = date('Y-m-d',strtotime($loan_date . $noOfDaystocmp));
			   $bl_tot_query = DB::select('SELECT loans.item_amt/100 as bl_tot FROM loans 
		        							WHERE loans.item_id = "BL"
		           							AND loans.entry_date = "'.$entry_date.'"
		          							AND loans.reg_num = "'.$reg_num.'" ');
			    $bl_tot_array = json_decode(json_encode($bl_tot_query), true);
			    //echo "<pre>";print_r($bl_tot_array);exit;
			    if(!empty($bl_tot_array))
				{
					$bl_tot = $bl_tot_array[0]['bl_tot'];
				}
				else
				{
					$bl_tot = 0;
				}
				$noOfDays++;
			} 
			$noOfDays = $noOfDays - 1;
			$lr_tot_query = DB::select('SELECT loans.item_amt as lr_tot from loans 
		      								WHERE loans.item_id = "LR"
		        							AND loans.entry_date = "'.$loan_date.'"
		        							AND loans.reg_num = "'.$reg_num.'" ');
			$lr_tot_array = json_decode(json_encode($lr_tot_query), true);
				
				if(empty($lr_tot_array))
				{
					$lr_tot = 0;
					if($dayIsLocked == "N")
					{	

						$checkdata = DB::select('select * from loans where item_id = "LR" and reg_num = "'.$reg_num.'" and entry_date = "'.$loan_date.'" ');
		        		if(!$checkdata)
		        		{
							$lr_tot_query = DB::insert('INSERT INTO loans 
	              				VALUES ("LR", "'.$lr_tot.'", "'.$reg_num.'","book1","'.$loan_date.'") ');
						}	
					}
				}
				else
				{	
					$lr_tot = $lr_tot_array[0]['lr_tot'];
				}
				// echo 'SELECT li.item_id, li.item_desc, l.item_amt/100 as 			item_amt
			 //      FROM loanitems li left outer join loans l
			 //     	on li.item_id    = l.item_id and li.item_type <> "T"
			 //       and l.reg_num        = "'.$reg_num.'"
			 //       and l.entry_date     = "'.$loan_date.'"	';exit;
		$loanitems = DB::select('SELECT li.item_id, li.item_desc, l.item_amt/100 as 			item_amt
			      FROM loanitems li left outer join loans l
			     	on li.item_id    = l.item_id and li.item_type <> "T"
			       and l.reg_num        = "'.$reg_num.'"
			       and l.entry_date     = "'.$loan_date.'"	');	 
		$loanitems_array = json_decode(json_encode($loanitems), true);
		//echo "<pre>";print_r($loanitems_array);exit;
		//$loanitems_BL_LR_GT = DB::select("SELECT * FROM loanitems WHERE item_id in ('BL','LR','GT') ");
		//$loanitems_BL_LR_GT_array = json_decode(json_encode($loanitems_BL_LR_GT), true);
		for($i=0;$i<count($loanitems_array);$i++)
		{
			$item_amt = $loanitems_array[$i]['item_amt'];
			if(empty($item_amt))
			{
				$item_amt = 0;
				if($dayIsLocked == "N")
	        	{
	        		//echo $dayIsLocked;exit;
	        		$checkdata = DB::select('select * from loans where item_id = "'.$loanitems_array[$i]['item_id'].'" and reg_num = "'.$reg_num.'" and entry_date = "'.$loan_date.'" ');
	        		if(!$checkdata)
	        		{
	        			DB::insert('INSERT INTO loans 
          				VALUES ("'.$loanitems_array[$i]['item_id'].'", 0, "'.$reg_num.'","book1","'.$loan_date.'") ');
	        		}
	        	}
			}
			//$this->upd_loanamt($loanitems_array[$i]['item_id'],$item_amt,$loan_date,$reg_num);
		}//exit;
		//echo 'lr_tot '.$lr_tot;exit;
		//$this->upd_loanamt("LR", $lr_tot, $loan_date, $reg_num);
		//Session::flash('alert-danger','FILES ARE LOCKED');
		
		return View::make('mktmgr.loantoreg_data',compact('loanitems_array','loanitems_BL_LR_GT_array', 'lr_tot', 'bl_tot','loan_date','reg_num','dayIsLocked','file_locked','msg'));
		}
	}
	public function upd_loanamt($id, $amt, $loan_date, $reg)
	{
		//echo $id;exit;
		DB::update('UPDATE loans SET loans.item_amt = "'.$amt.'"
				 	WHERE loans.item_id = "'.$id.'" 
				   AND loans.entry_date = "'.$loan_date.'"
				   AND loans.reg_num = "'.$reg.'" ');
		return true;
	}
	public function updateLoantoRegRegister()
	{
		//echo "<pre>";print_r(Input::all());//exit;
		$reg_num = Input::get('reg_num');
		$entry_date = Input::get('entry_date');
		$item_id = Input::get('item_id');
		$item_desc = Input::get('item_desc');
		$item_amt = Input::get('item_amt');
		for($i = 0;$i<count($item_id);$i++)
		{
			DB::update('update loans set item_amt = '.($item_amt[$i]*100).' where item_id = "'.$item_id[$i].'" and reg_num = '.$reg_num.' and entry_date = "'.$entry_date.'" ' );
		}
		Session::flash('alert-success', 'Loans Details Updated Successfully!'); 
          	return Redirect::route('mktmgr-loantoregregister');
	}
	public function regCheckOutRegister()
	{
		$validator = Validator::make(Input::all(),
            array(
                   'reg_num' =>'required',
                   'dateofinfo'=>'required',
                ),array('dateofinfo.required'=>'Date is required','reg_num.required'=>'Register number is required'));
	 	$reg_num = Input::get('reg_num');
	 	$dateofinfo = Input::get('dateofinfo');
		
		if($validator->fails() || $reg_num == '')
        {
            return Redirect::route('mktmgr-bookregcheckoutreg')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	//echo "<pre>";print_r(Input::all());exit;
    		//$dayIsLocked = "Y";
    		$reg_num = Input::get('reg_num');
    		$reg_date = Input::get('dateofinfo'); 
    		$approvalsOk = $this->approvalsOk($reg_date);
    		//echo $approvalsOk;exit;
			if($approvalsOk)
			{	
				$lastAprvlDate = date('m/d/Y',strtotime($approvalsOk));
				Session::flash('alert-info','Last approved date was'.' '.$lastAprvlDate.'.'.' '.' A manager need to approve daily work.');
	            return Redirect::route('mktmgr-bookregcheckoutreg');
			}
        	//$dayIsLocked = 'N';
        	$dayIsLocked = $this->approvalsOk1($reg_date);
				//echo $dayIsLocked;exit;
	        	$file_locked = $this->file_locked($reg_date);
	        	//echo $file_locked;exit;
	        	if($file_locked)
	        	{
	        		$file_locked = "Y";
	        	}
	        	else
	        	{
	        		$file_locked = "N";
	        	}
    		$resultArray =array();
    		$redo_totals = array();
    		$regdtl_cur = DB::select('SELECT items.item_id,items.item_desc,items.item_type
			      FROM items 
			      WHERE items.item_type NOT IN ("T","X")
			      AND items.disable = "N"
			      ORDER BY items.item_type,items.item_desc');
    		$regdtl_cur_array = json_decode(json_encode($regdtl_cur), true);
    		//echo "<pre>";print_r($regdtl_cur_array);exit;
    			$tot_cred = 0;
		    	$tot_deb  = 0;
		      	$tot_os   = 0;
    		for($i = 0;$i<count($regdtl_cur_array);$i++)
			{
				$resultArray1['item_id'] = $regdtl_cur_array[$i]['item_id'];
				$resultArray1['item_desc'] = $regdtl_cur_array[$i]['item_desc'];
				$entry_item_amt = DB::select('SELECT (entry.item_amt)/100 as item_amt FROM entry WHERE entry.item_id = "'.$regdtl_cur_array[$i]['item_id'].'" AND entry.reg_num = '.$reg_num.' AND entry.entry_date = "'.$reg_date.'" ');
				$entry_item_amt_array = json_decode(json_encode($entry_item_amt), true);
				 //echo "<pre>";print_r($entry_item_amt_array);exit;
				if(empty($entry_item_amt_array))
				{
					$resultArray1['item_amt'] = 0;
					DB::insert('INSERT INTO entry 
          			VALUES ("'.$regdtl_cur_array[$i]['item_id'].'",0,
                  "'.$reg_num.'", "book1","'.$reg_date.'") ');
				}
				else
				{
				 	$resultArray1['item_amt'] = $entry_item_amt_array[0]['item_amt'];
				}
				
				$items_item_type = DB::select('SELECT items.item_type as type FROM items
            			WHERE items.item_id = "'.$regdtl_cur_array[$i]['item_id'].'" ');
				$items_item_type_array = json_decode(json_encode($items_item_type), true);
				 $type = $items_item_type_array[0]['type'];//exit;
				 if($type == 'D')
				 {
				 	$tot_deb = $tot_deb  + $resultArray1['item_amt'];
				 	$tot_os  = $tot_os + $resultArray1['item_amt'];
				 }
				 elseif($type == 'C')
				 {
				 	$tot_cred = $tot_cred  + $resultArray1['item_amt'];
				 	$tot_os  = $tot_os - $resultArray1['item_amt'];
				 }
				 $resultArray1['type'] = $type;
				 //echo $item_amt.'<br>';
				 $resultArray[] = $resultArray1;
				 //$redo_totals[] = $resultArray2;
			}
			//exit;
			$resultArray2['tot_deb'] = $tot_deb;
			$resultArray2['tot_cred'] = $tot_cred;
			$resultArray2['tot_os'] = $tot_os;
			$redo_totals[] = $resultArray2;
		}
		//echo "<pre>";print_r($resultArray);
		//echo "<pre>";print_r($redo_totals);
		return View::make('mktmgr.regcheckoutregister',compact('resultArray', 'redo_totals','dayIsLocked','file_locked','reg_num','reg_date','msg'));
	}
	public function updateRegCheckOutRegister()
	{
		$reg_num = Input::get('reg_num');
		$entry_date = Input::get('entry_date');
		$item_amt_to_update = Input::get('item_amt');
		$item_id = Input::get('item_id');
		for($i = 0;$i<count($item_id);$i++)
		{	
			$item_amt1 = $item_amt_to_update[$i]*100;
				DB::update('UPDATE entry 
		        SET entry.item_amt  = 
		            "'.$item_amt1.'"
		          WHERE entry.reg_num = "'.$reg_num.'" AND entry.entry_date="'.$entry_date.'" AND
		               entry.item_id = "'.$item_id[$i].'" ');
		}
		Session::flash('alert-success', 'Register Details Updated Successfully!'); 
          	return Redirect::route('mktmgr-bookregcheckoutreg');
		
	}
	public function getRegCheckOutPrint()
	{
		return View::make('mktmgr.regcheckoutprintdate');
	}
	public function postRegCheckOutPrint()
	{
		$validator = Validator::make(Input::all(),
	            array(
	                   'rpt_date'=>'required'
	                ),array('rpt_date.required'=>'Date is required'));
		if($validator->fails())
        {          
		    return Redirect::route('mktmgr-regcheckoutprint')
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
        	$rpt_date = Input::get('rpt_date');
        	$str_num_query = DB::select('SELECT misc.groc_num  as str_num FROM misc');
        	$str_num = $str_num_query[0]->str_num;
        	$reg_array = array();
        	$item_type_C_array =array();
        	$item_type_C_array_final =array();
        	$item_type_D_array =array();
        	$item_type_D_array_final =array();
        	$total_over_short = array();
        	$total_over_short_final = array();
        	$reg_array_final = array();
        	$reg_array_final1 = array();
        	$reg_num = DB::select('SELECT DISTINCT entry.reg_num
		      FROM entry
		      WHERE entry.entry_date = "'.$rpt_date.'"
		      and entry.item_amt > 0
		      ORDER BY entry.reg_num ');
        	$reg_curs = json_decode(json_encode($reg_num), true);
        	for($i=0;$i<count($reg_curs);$i++)
        	{
        		$item_type_C_sum_query = DB::select('SELECT SUM(entry.item_amt)
			         as  item_type_C
			         FROM entry,items
			        WHERE entry.entry_date = "'.$rpt_date.'" 
			          AND items.item_type = "C"
			          AND entry.item_id = items.item_id
			          AND entry.reg_num = "'.$reg_curs[$i]['reg_num'].'" ');
        		
        		if($item_type_C_sum_query)
        		{
        			$item_type_C_array['item_type_C'] = ($item_type_C_sum_query[0]->item_type_C)/100;
        		}
        		else
        		{
        			$item_type_C_array['item_type_C'] = 0;
        		}
        		$item_type_C_array_final[] = $item_type_C_array;


        		$item_type_D_sum_query = DB::select('SELECT SUM(entry.item_amt)
			         as  item_type_D
			         FROM entry,items
			        WHERE entry.entry_date = "'.$rpt_date.'" 
			          AND items.item_type = "D"
			          AND entry.item_id = items.item_id
			          AND entry.reg_num = "'.$reg_curs[$i]['reg_num'].'" ');
        		
        		if($item_type_D_sum_query)
        		{
        			$item_type_D_array['item_type_D'] = ($item_type_D_sum_query[0]->item_type_D)/100;
        		}
        		else
        		{
        			$item_type_D_array['item_type_D'] = 0;
        		}
        		$total_over_short['total_over_short'] = $item_type_D_array['item_type_D'] - $item_type_C_array['item_type_C']; 
        		$item_type_D_array_final[] = $item_type_D_array;
        		$total_over_short_final[] = $total_over_short;
        	}
        	$rpt_automate_curs = DB::select('SELECT items.item_desc, 
	           items.item_id,
	           items.item_type,
	           SUM(entry.item_amt)
		      FROM items ,entry
		      WHERE items.item_type IN ("C","D") 
		        AND items.item_id=entry.item_id 
		        AND entry.entry_date = "'.$rpt_date.'"
		      GROUP BY items.item_desc,items.item_id,items.item_type
		      ORDER BY items.item_type,items.item_id');
        	$rpt_automate_curs = json_decode(json_encode($rpt_automate_curs), true);
        	$start_reg = 1;
  			$cur_reg = 1;
        	for($j=0;$j<count($rpt_automate_curs);$j++)
	        {
	        	for($i=0;$i<count($reg_curs);$i++)
        		{
        			$reg_array['item_id'] = $rpt_automate_curs[$j]['item_id']; 
	        		$reg_array['item_desc'] = $rpt_automate_curs[$j]['item_desc']; 
	        		$reg_array['item_type'] = $rpt_automate_curs[$j]['item_type']; 
	        		$item_amt_query = DB::select('SELECT entry.item_amt
		          as item_amt
		          FROM entry
		          WHERE entry.item_id = "'.$rpt_automate_curs[$j]['item_id'].'" AND 
		                entry.entry_date = "'.$rpt_date.'" AND 
		                entry.reg_num = "'.$reg_curs[$i]['reg_num'].'"');
	        		if($item_amt_query)
	        		{
	        			$reg_array['item_amt'] = ($item_amt_query[0]->item_amt)/100;
	        		}
	        		else
	        		{
	        			$reg_array['item_amt'] = 0;
	        		}
	        		$reg_array['reg_num'] = $reg_curs[$i]['reg_num'];
	        		$reg_array_final[] = $reg_array;
        		}
        		$reg_array_final1[] = $reg_array_final;
        		unset($reg_array_final);
	        }	
	      	 

	      	$rpt_grandtot_curs = DB::select('SELECT SUM(entry.item_amt) as item_amt, 
				            items.item_desc, 
				            items.item_type
				       FROM entry, items
				      WHERE entry.entry_date = "'.$rpt_date.'"
				        AND items.item_id = entry.item_id
				        AND items.item_type IN ("C","D")
				            GROUP BY items.item_desc,items.item_type
				            ORDER BY items.item_type,items.item_desc');
        	$rpt_grandtot_curs = json_decode(json_encode($rpt_grandtot_curs), true); 

        	$grand_total_total_credit_D = DB::select('SELECT SUM(entry.item_amt)
			         as  item_type_D
			         FROM entry,items
			        WHERE entry.entry_date = "'.$rpt_date.'" 
			          AND items.item_type = "D"
			          AND entry.item_id = items.item_id');
        	if($grand_total_total_credit_D)
    		{
    			$grand_total_total_credit_D = ($grand_total_total_credit_D[0]->item_type_D)/100;
    		}
    		else
    		{
    			$grand_total_total_credit_D = 0;
    		}


    		$grand_total_total_credit_C = DB::select('SELECT SUM(entry.item_amt)
			         as  item_type_C
			         FROM entry,items
			        WHERE entry.entry_date = "'.$rpt_date.'" 
			          AND items.item_type = "C"
			          AND entry.item_id = items.item_id');
        	if($grand_total_total_credit_C)
    		{
    			$grand_total_total_credit_C = ($grand_total_total_credit_C[0]->item_type_C)/100;
    		}
    		else
    		{
    			$grand_total_total_credit_C = 0;
    		}
    		$calc_godo = $this->calc_ovshrt($rpt_date);
        	//echo '<pre>';print_r($calc_godo);exit;
        	$go_ovshrt = $calc_godo[1];
        	$do_ovshrt = $calc_godo[2];
	    	return View::make('mktmgr.regcheckoutprintdata',compact('reg_curs','reg_array_final1','item_type_C_array_final','item_type_D_array_final','total_over_short_final','rpt_grandtot_curs','grand_total_total_credit_C','grand_total_total_credit_D','go_ovshrt','do_ovshrt','str_num','rpt_date'));
	    }
	}
	// public function calc_godo($rpt_date)
	// {

	// }
}