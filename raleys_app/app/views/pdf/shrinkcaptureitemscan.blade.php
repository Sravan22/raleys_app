<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}
.center{
    text-align: center;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>


<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
        <h1 style="color:#ce0002;">Shrink Capture Item Scan Report</h1>
        <table>
        <tr>
             <th>Shrink Capture No</th>
            <th>Shrink Date</th>
            <th>Employee Id</th>
            <th>Total Line Items</th>
            <th>Total Value</th>
          </tr>
         <tr>
           <td>{{ $shrink_number }}</td>
           <td>{{ $shrink_date }}</td>
           <td>{{ $employee_id }}</td>
           <td>{{ $tot_line_items }}</td>
           <td>${{ $tot_value }}</td>
         </tr> 
          </table>  

          <br>
          <br>  
    <table>
        <tr>
             <th>Scan No</th>
            <th>Description</th>
            <th>Item Cost</th>
            <th>Qty</th>
            <th>Ext Cost</th>
        </tr>

         @for ($i = 0; $i < count($shrinkcaptureitemscan); $i++)
         <tr>
            <?php 
              $params = array(
                        'shrink_number' =>  $shrink_number,
                        'seq_number' =>  $shrinkcaptureitemscan[$i]->seq_number,
                        'status' =>  $status
                        ); 
              $queryString = http_build_query($params);
            ?>
            <td>{{  $shrinkcaptureitemscan[$i]->seq_number }}</td>
            {{-- <td>{{ $shrinkcaptureitemscan[$i]->item_desc }}</td> --}}
            <td>  {{  $shrinkcaptureitemscan[$i]->item_desc }}</td>
            <td>${{ $shrinkcaptureitemscan[$i]->upc_cost }}</td>
            @if($shrinkcaptureitemscan[$i]->rw_rtl_amt!=0)
            <?php $quantity = $shrinkcaptureitemscan[$i]->rw_rtl_amt/$shrinkcaptureitemscan[$i]->rtl_amt; ?>
            <td>{{ floor($quantity) }}</td>
            <?php $ext_value = ($shrinkcaptureitemscan[$i]->upc_cost) * ($shrinkcaptureitemscan[$i]->rw_rtl_amt/$shrinkcaptureitemscan[$i]->rtl_amt); ?>
            <td>${{ number_format($ext_value, 2, '.', '');  }}</td>
            @else
            <td>{{ $shrinkcaptureitemscan[$i]->quantity }}</td>
            <?php $ext_value = ($shrinkcaptureitemscan[$i]->upc_cost) * ($shrinkcaptureitemscan[$i]->quantity); ?>
            <td>${{ number_format($ext_value, 2, '.', '');  }}</td>
            @endif
         </tr>
         @endfor 





            </table>
</div>  

</body>
</html>