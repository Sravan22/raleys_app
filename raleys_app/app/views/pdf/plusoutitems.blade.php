<!DOCTYPE html>
<html>
   <head>
      <style>
         table {
         font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
         font-size: 9px;
         color: #333;
         border-collapse: collapse;
         width: 100%;
         }
         .center {
         text-align: center;
         }
         td, th {
         border: 1px solid #dddddd;
         text-align: left;
         padding: 5px;
         }
         .center{
         text-align: center;
         }
         tr:nth-child(even) {
         background-color: #dddddd;
         }
      </style>
   </head>
   <body>
      
      <div class="row">
      <div class="col-md-6">
         <a class="custom-input-width pull-left" href="{{ ('/') }}">
         <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
         </a>
      </div>
      <h1 style="color:#ce0002;">Planned and Recent Plus Out Report</h1>
         <table>
            <tr>
               <th>SKU</th>
               <th>Description</th>
               <th>Case Pack</th>
               <th>Coin Size</th>
               <th>Qty</th>
            @if(count($soorditm_rec)>0)
         @for ($i = 0; $i < count($soorditm_rec); $i++)   
         <tr>
            <td>{{ $soorditm_rec[$i]->sku_number }}</td>
            <?php 
                $soorditm_rec_query =  DB::select('SELECT description, case_pack, cont_size   FROM soitem WHERE sku_number = "'.$soorditm_rec[$i]->sku_number.'" ');
           $soorditm_rec_query_array = json_decode(json_encode($soorditm_rec_query), true);
             if(!empty($soorditm_rec_query_array))
              {
                  echo '<td>'.$soorditm_rec_query_array[0]['description'].'</td>';
                  echo '<td>'.$soorditm_rec_query_array[0]['case_pack'].'</td>';
                  echo '<td>'.$soorditm_rec_query_array[0]['cont_size'].'</td>';
              }
              else
              {
                  echo '<td>UNKNOWN ITEM</td>';
                  echo '<td></td>';
                  echo '<td></td>'; 
              }
            ?>
            
            <td>{{ $soorditm_rec[$i]->quantity }}</td>
         </tr>
        
         @endfor
          @endif 
         </table>
      </div>
   </body>
</html>