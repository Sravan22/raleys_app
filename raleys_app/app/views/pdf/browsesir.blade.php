<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 8px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="#">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
    <table>
         <tr>
            <th>UPC No</th>
            <th>Case Pack</th>
            <th>Vendor Qty</th>
            <th>Vendor Cost</th>
            <th>Vendor Allowance</th>
            <th>Vendor CRV</th>
            <th>Store Qty</th>
            <th>Store Cost</th>
            <th>Store Allowance</th>
            <th>Store CRV</th>
            <th>Free Qty</th>
            <th>Delivery Unit</th>
            <th>Note</th>
            <th>Active</th>
        </tr>
        @foreach ($results as $rs)
          <tr>
          <td>{{ $rs['upc_number'] }}</td>
            <td>{{ $rs['case_pack'] }}</td>
            <td>{{ $rs['vendor_qty'] }}</td>
            <td>{{ $rs['vendor_cost'] }}</td>
            <td>{{ $rs['vendor_allow'] }}</td>
            <td>{{ $rs['vendor_crv'] }}</td>
            <td>{{ $rs['store_qty'] }}</td>
            <td>{{ $rs['store_cost'] }}</td>
            <td>{{ $rs['store_allow'] }}</td>
            <td>{{ $rs['store_crv'] }}</td>
            <td>{{ $rs['free_qty'] }}</td>
            <td>{{ $rs['delvry_unt_ovrd'] }}</td>
            <td>{{ $rs['spec_note_cd'] }}</td>
            <td>{{ $rs['active_sw'] }}</td>
        @endforeach
    </table>
</div>  

</body>
</html>