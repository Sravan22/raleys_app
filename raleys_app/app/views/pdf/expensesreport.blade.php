<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">Store Expenses</h1>
    <table>
        <tr>
            <th>Invoice Date</th>
            <th>Invoice Number</th>
            <th>Vendor Name</th>
            <th>Type</th>
            <th>Status</th>
        </tr>
        @foreach ($receivings as $rcv)
          <tr>
            <td>{{ date("m/d/Y",strtotime(trim($rcv->invoice_date))); }}</td>
            <td>{{ $rcv->id }}</td>
            <td>{{ $rcv->name }}</td>
            <td>{{ $rcv->type_code }}</td>
            <td>{{ $rcv->status_code }}</td>
          </tr>
        @endforeach
    </table>
</div>  

</body>
</html>