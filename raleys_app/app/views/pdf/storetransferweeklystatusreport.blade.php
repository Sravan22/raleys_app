<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
        <h1 style="color: red; font-size: 14px;">Store Transfers Accounting Recap Report</h1>
      <p style="text-align: center;">Date Range <b>{{ $fromdate }}</b> thru<b>{{ $todate }}</b></div>
      </div>
    @if ($reportquery_array)
    <table>
        <thead>
           <tr>
            <th>Transfer Date</th>
            <th>Transfer Number</th>
            <th>Xfr Type</th>
            <th>From Store/Dept</th>
            <th>From Account</th>
            <th>To Store/Dept</th>
            <th>To Account </th>
            <th>Transfer Amount</th>
            <th>Status</th>
          </tr>
        </thead>
       @foreach ($reportquery_array as $store_result)
          <tr>
            <td>{{ $store_result['create_date'] }}</td>
            <td>{{ $store_result['transfer_number'] }}</td>
            <td>{{ $store_result['transfer_type'] }}</td>
            <td>{{ $store_result['from_store_no']  }} / {{ $store_result['from_dept_no']  }}</td>
            <td>{{ $store_result['from_account'] }}</td>
            <td>{{ $store_result['to_store_no']  }} / {{ $store_result['to_dept_no']  }}</td>
            <td>{{ $store_result['to_account'] }}</td>
            <td>{{ $store_result['tot_value'] }}</td>
            <td>{{ $store_result['status'] }}</td>
          </tr>
       @endforeach
    </table>        
     @else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Store Transfers meet Query criteria.
            </div>
        @endif
</div>  

</body>
</html>