<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">Department Report</h1>
    <table class="table table-striped">
        
      <tr>
       
        <th>Inv Date</th>
        <th>Inv Number</th>
        <th>Type</th>
        <th>Inv Area</th>
        <th>Sect</th>
        <th>Emp ID</th>
        <th>Tot Lns</th>
        <th>Value at Cost</th>
        <th>Value at RT1</th>
        <th>CRV at RT1</th>
        <th>Stat</th>
      </tr>

    
         @foreach($work_data->sodinv_hdrs_rec as $key => $value)
      <tr>
        
       
        <td >{{date('m/d/Y',strtotime($value['create_date']))}}</td>

        <td>{{$value['inventory_number']}}</td>
        <td>{{$value['inventory_type']}}</td>
        <td>{{$value['inventory_area']}}</td>
        <td>{{$value['inventory_section']}}</td>
        <td>{{$value['employee_id']}}</td>
        <td>{{$value['total_scans']}}</td>
        <td>${{$value['total_value_cst']}}</td>
        <td>${{$value['total_value_rtl']}}</td>
        <td>${{$value['total_crv_rtl']}}</td>
        <td>{{$value['invhdr_status']}}</td>
      </tr>
       @endforeach
      
    
  </table>
  </div>  

</body>
</html>