<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">Weekly Report</h1>
    <table>
        <tr>
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th>Name</th>
            <th>Type</th>
            <th>Total Cost</th>
        </tr>
        @foreach ($results as $rs)
          <tr>
            <td>{{ $rs->id }}</td>
            <td>{{ $rs->invoice_date }}</td>
            <td>{{ $rs->name }}</td>
            <td>{{ $rs->type_code }}</td>
            <td>{{ $rs->tot_vend_cost }}</td>
          </tr>
        @endforeach
    </table>
</div>  

</body>
</html>