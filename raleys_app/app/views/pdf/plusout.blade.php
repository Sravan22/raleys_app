<!DOCTYPE html>
<html>
   <head>
      <style>
         table {
         font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
         font-size: 9px;
         color: #333;
         border-collapse: collapse;
         width: 100%;
         }
         .center {
         text-align: center;
         }
         td, th {
         border: 1px solid #dddddd;
         text-align: left;
         padding: 5px;
         }
         .center{
         text-align: center;
         }
         tr:nth-child(even) {
         background-color: #dddddd;
         }
      </style>
   </head>
   <body>
      
      <div class="row">
      <div class="col-md-6">
         <a class="custom-input-width pull-left" href="{{ ('/') }}">
         <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
         </a>
      </div>
      <h1 style="color:#ce0002;">Planned and Recent Plus Out Report</h1>
         <table>
            <tr>
               <th>Ship Date</th>
               <th>Order Type</th>
               <th>Total Lines</th>
               <th>Total Units</th>
            </tr>
            @for ($i = 0; $i < count($sotrnhdr_rec); $i++)   
            <tr>
               <?php 
               $order_date =  DB::select('SELECT order_date  FROM soordhdr   WHERE seq_number = "'.$sotrnhdr_rec[$i]->seq_number.'" ');
               $order_date_array = json_decode(json_encode($order_date), true);
               if(!empty($order_date_array))
               {  
               $params = array(
               'seq_number' =>  $sotrnhdr_rec[$i]->seq_number
               ); 
               $queryString = http_build_query($params);
               $order_date = date("m/d/Y", strtotime($order_date_array[0]['order_date']));
                ?>   
               <td> {{$order_date}}</td>
               <?php
               }
               $description =  DB::select('SELECT description FROM sotrncfg  WHERE type_code    = "'.$sotrnhdr_rec[$i]->type_code.'"  AND subtype_code = "'.$sotrnhdr_rec[$i]->subtype_code.'" ');
               $description_array = json_decode(json_encode($description), true);
               if(!empty($description_array))
               {
               echo '
               <td>'.$description_array[0]['description'].'</td>
               ';
               }
               $tot_units_tot_lines =  DB::select('SELECT count(*) as tot_lines, sum(quantity) as tot_units  FROM soorditm  WHERE seq_number = "'.$sotrnhdr_rec[$i]->seq_number.'"');
               $tot_units_tot_lines_array = json_decode(json_encode($tot_units_tot_lines), true);
               if(!empty($tot_units_tot_lines_array))
               {
               echo '<td>'.$tot_units_tot_lines_array[0]['tot_lines'].'</td>';
               echo '
               <td>'.$tot_units_tot_lines_array[0]['tot_units'].'</td>';
               }
               ?>
            </tr>
            @endfor
         </table>
      </div>
   </body>
</html>