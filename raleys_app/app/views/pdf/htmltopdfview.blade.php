
<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
      <h1 style="color:#ce0002;">cHg-regs Report</h1>
    <table>
        <tr>
            <th>Register Number</th>
            <th>Register Type</th>
            
          </tr>
        @foreach ($searchresult as $result)
        <tr>
            <td>{{ $result->reg_num }}</td>
            <td>{{ $result->reg_type }}</td>
            
        </tr>
        @endforeach
    </table>
</div>  

</body>
</html>