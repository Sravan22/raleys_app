<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
            <a class="custom-input-width pull-left" href="{{ ('/') }}">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
                </a>
        </div>
        <h1 style="color:#ce0002;">Shrink Capture Report</h1>
    <table>
        <tr>
            <th>Dept No</th>
            <th>Department Description</th>
            <th>Shrink Captured?</th>
            <th>Shrink Amount</th>
          </tr>
        
        
             
      <?php 
              $params = array(
                        'dept_no' =>  1,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>1</td>
            @if($dept_01_tot_value[0]->dept_01_tot_value > 0)
            <td>Grocery</td>
            <td>Y</td>
            <td>${{ $dept_01_tot_value[0]->dept_01_tot_value; }}</td>
            @else
            <td>Grocery</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  2,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>2</td>
            @if($dept_02_tot_value[0]->dept_02_tot_value > 0)
            <td>Liquor</u></td>
            <td>Y</td>
            <td>{{ $dept_02_tot_value[0]->dept_02_tot_value; }}</td>
            @else
            <td>Liquor</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
          <?php 
              $params = array(
                        'dept_no' =>  3,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>3</td>
            @if($dept_03_tot_value[0]->dept_03_tot_value > 0)
            <td>Meat</td>
            <td>Y</td>
            <td>${{ $dept_03_tot_value[0]->dept_03_tot_value; }}</td>
            @else
            <td>Meat</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  4,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>4</td>
            @if($dept_04_tot_value[0]->dept_04_tot_value > 0)
            <td>Deli</u></td>
            <td>Y</td>
            <td>${{ $dept_04_tot_value[0]->dept_04_tot_value; }}</td>
            @else
            <td>Deli</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  5,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>5</td>
            @if($dept_05_tot_value[0]->dept_05_tot_value > 0)
            <td>Fresh Service Deli</td>
            <td>Y</td>
            <td>${{ $dept_05_tot_value[0]->dept_05_tot_value; }}</td>
            @else
            <td>Fresh Service Deli</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  6,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>6</td>
            @if($dept_06_tot_value[0]->dept_06_tot_value > 0)
            <td>Bakery</td>
            <td>Y</td>
            <td>${{ $dept_06_tot_value[0]->dept_06_tot_value; }}</td>
            @else
            <td>Bakery</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  7,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>7</td>
            @if($dept_07_tot_value[0]->dept_07_tot_value > 0)
            <td>Produce</td>
            <td>Y</td>
            <td>${{ $dept_07_tot_value[0]->dept_07_tot_value; }}</td>
            @else
            <td>Produce</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  8,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>8</td>
            @if($dept_08_tot_value[0]->dept_08_tot_value > 0)
            <td>Natural Foods</td>
            <td>Y</td>
            <td>${{ $dept_08_tot_value[0]->dept_08_tot_value; }}</td>
            @else
            <td>Natural Foods</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  9,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>9</td>
            @if($dept_09_tot_value[0]->dept_09_tot_value > 0)
            <td>General Merchandise</td>
            <td>Y</td>
            <td>${{ $dept_09_tot_value[0]->dept_09_tot_value; }}</td>
            @else
            <td>General Merchandise</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  10,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>10</td>
            @if($dept_10_tot_value[0]->dept_10_tot_value > 0)
            <td>Hot Wok</td>
            <td>Y</td>
            <td>${{ $dept_10_tot_value[0]->dept_10_tot_value; }}</td>
            @else
            <td>Hot Wok</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  11,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>11</td>
            @if($dept_11_tot_value[0]->dept_11_tot_value > 0)
            <td>Floral</td>
            <td>Y</td>
            <td>${{ $dept_11_tot_value[0]->dept_11_tot_value; }}</td>
            @else
            <td>Floral</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  12,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>12</td>
            @if($dept_12_tot_value[0]->dept_12_tot_value > 0)
            <td>Coffee Shop</td>
            <td>Y</td>
            <td>${{ $dept_12_tot_value[0]->dept_12_tot_value; }}</td>
            @else
            <td>Coffee Shop</td>
            <td>N</td>
            <td>********</td>
            @endif
         </tr>
         <?php 
              $params = array(
                        'dept_no' =>  35,
                        'shrink_date' =>  $shrink_date
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>35</td>
            @if($dept_35_tot_value[0]->dept_35_tot_value > 0)
            <td>Seafood</td>
            <td>Y</td>
            <td>${{ $dept_35_tot_value[0]->dept_35_tot_value; }}</td>
            @else
            <td>Seafood</td>
            <td>N</td>
            <td>********</td>
            @endif
          </tr>
        
    </table>
</div>  

</body>
</html>