<?php

?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">FM Notes Report</h1>
    <table>
        <tr>
            <th> Userid</th>
            <th>Username</th>
            <th> Update Date and Time </th>
            <th>Item Code</th>
            <th>Item Desc</th>
            <th>Item Dept</th>
            <th>Master Field No</th>
            <th>Original Value</th>
            <th>New Value</th>
            <th>Description</th>
        </tr>
        @for ($i = 0; $i < count($fmreport_query_array); $i++)
        <tr>  

            <td>{{ $fmreport_query_array[$i]['userid'] }}</td>
            <?php $sluser_rec = DB::select('SELECT * FROM sluser WHERE userid = "'.$fmreport_query_array[$i]['userid'].'" ');
             $sluser_rec_array = json_decode(json_encode($sluser_rec), true);
                echo '<td>'.$sluser_rec_array[0]['name'].'</td>';
             ?>
            <td>{{ date('m/d/Y',strtotime($fmreport_query_array[$i]['update_date'])) }} {{ $fmreport_query_array[$i]['update_time'] }}</td>
            <td>{{ $fmreport_query_array[$i]['item_code'] }}</td>
            <td>{{ $fmreport_query_array[$i]['item_desc'] }}</td>
            <td>{{ $fmreport_query_array[$i]['item_dept'] }}</td>
            <td>{{ $fmreport_query_array[$i]['mstr_fld_no'] }}</td>
            <td>{{ $fmreport_query_array[$i]['orig_value'] }}</td>
            <td>{{ $fmreport_query_array[$i]['new_value'] }}</td>
            <?php 
            $slmstfld_rec = DB::select('SELECT description FROM slmstfld WHERE field_no = "'.$fmreport_query_array[$i]['mstr_fld_no'].'" ');
             $slmstfld_rec_array = json_decode(json_encode($slmstfld_rec), true);
             if(!empty($slmstfld_rec_array))
             {
                echo '<td>'.$slmstfld_rec_array[0]['description'].'</td>';
             }
             else
             {
                echo '<td></td>';
             }
                
             ?>
          </tr>
       
        @endfor 
    </table>
</div>  

</body>
</html>
