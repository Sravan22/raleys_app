<?php

?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 9px;
    color: #333;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 5px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<div class="row">
    <div class="col-md-6">
                    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
        </div>
      <h1 style="color:#ce0002;">Store Transfer Hard Copy Report</h1>
    <table>
        <tr>
            <th>Transfer Date</th>
            <th>Transfer Number</th>
            <th>Emp ID</th>
            <th>Type</th>
            <th>From Store/Dept</th>
            <th>To Store/Dept</th>
            <th>Total Lns</th>
            <th>Total Value</th>
            <th>Status</th>
        </tr>
        @for ($i = 0; $i < count($storehc_report_array); $i++)
        <tr>  

            <td>{{ date("m/d/Y", strtotime($storehc_report_array[$i]['create_date'])) }}</td>
            <td>{{ $storehc_report_array[$i]['seq_number'] }} - {{ $storehc_report_array[$i]['transfer_number'] }}</td>
            <td>{{ $storehc_report_array[$i]['employee_id'] }}</td>
            <td>{{ $storehc_report_array[$i]['transfer_type'] }}</td>
            <td>{{ $storehc_report_array[$i]['from_store_no'] }} / {{ $storehc_report_array[$i]['from_dept_no'] }}</td>
            <td>{{ $storehc_report_array[$i]['to_store_no'] }} / {{ $storehc_report_array[$i]['to_dept_no'] }}</td>
            <td>{{ $storehc_report_array[$i]['tot_line_items'] }}</td>
            <td>{{ $storehc_report_array[$i]['tot_value'] }}</td>
            <td>{{ $storehc_report_array[$i]['status'] }}</td>
            
          </tr>
       
        @endfor 
    </table>
</div>  

</body>
</html>
