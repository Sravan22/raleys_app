@extends('layout.dashboard')
@section('content')
@section('section')

<div class="containerhome homepageicons"> 
   <!--  <div class="row"> -->
            <a href="{{ URL::route('mktmgr-bookkeeper')}}" accesskey="1">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                <div class="numtop"> Alt+1 </div>
                 <div> BooksPro </div> 
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-nonperishableinvprepview')}}" accesskey="2">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
              <div class="numtop"> Alt+2 </div>
                 <div> Non Perishable Inventory Prep </div> 
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-receivingsqueryadvance-query')}}" accesskey="3">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               <div class="numtop"> Alt+3 </div>
                 <div> Receivings </div> 
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-shrinkcapturereview')}}" accesskey="4">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                <div class="numtop"> Alt+4 </div>
                 <div> Shrink Capture Review </div>
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-storeorderview')}}" accesskey="5">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               <div class="numtop"> Alt+5 </div>
                 <div> Store Orders </div> 
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-storetransfersview') }}" accesskey="6">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                <div class="numtop"> Alt+6 </div>
                 <div> Store Transfers </div>
            </div>
            </div>
            </a>
      <!--   </div> -->
</div>

@stop
