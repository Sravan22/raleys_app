@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm05.slcadm05fm_menu')
</header>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Fm Notes Detail</div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('slcadm05-post-fm-query')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>
            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label col-sm-5">User Id</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="userid" placeholder="User Id" name="userid" autofocus="" tabindex="2" />
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control" id="update_date" placeholder="Date" name="update_date" tabindex="3" />
                     @if($errors->has('update_date'))
                     {{ $errors->first('update_date')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5"> Time</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="update_time" placeholder=" Time" name="update_time" tabindex="4" />
                     @if($errors->has('update_time'))
                     {{ $errors->first('update_time')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">UPC / PLU / SKU</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="item_code" placeholder="UPC / PLU / SKU" name="item_code" tabindex="5" />
                     @if($errors->has('item_code'))
                     {{ $errors->first('item_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">FM Note</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="new_value" placeholder="FM Note" name="new_value" tabindex="6" />
                     @if($errors->has('new_value'))
                     {{ $errors->first('new_value')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 47%">
                        <input type="submit" name="login-submit" id="submit" tabindex="7" value="Search" class="btn">
                        {{-- <input type="reset" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn"> --}}
                        <input type="button" value="Cancel" id="cancel" class="btn" onClick="document.location.href='{{URL::to('slcadm05/return-home')}}'" tabindex="8" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="9"></div>

            </form>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>


<script type="text/javascript">
   $('#focusguard-2').on('focus', function() {
  $('#userid').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#cancel').focus();
});


</script>
@stop