@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
   @include('slcadm05.slcadm05menu')
</header>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
<div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">Summary Report for..</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('slcadm05-post-summary-report')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>

               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">User ID</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="userid" {{ (Input::old('userid'))?' 
                              value="'.Input::old('userid').'"':''}} placeholder="User ID" name="userid" autofocus="" tabindex="2" />
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                     {{ $message }}
                     </div> 
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control" id="update_date" placeholder="" {{ (Input::old('update_date'))?' 
                              value="'.Input::old('update_date').'"':''}} name="update_date" tabindex="3" />
                     @if($errors->has('update_date'))
                     {{ $errors->first('update_date')}}
                     @endif
                  </div> 
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn" />
                        <input type='reset' class="btn" value="Reset" tabindex="5" />
                        <input type="reset" id="cancel" value="Cancel" class="btn" onclick="document.location.href='{{URL::to('slcadm05/return-home')}}'" tabindex="6" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="7"></div>
            </form>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript">
   $('#focusguard-2').on('focus', function() {
  $('#userid').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#cancel').focus();
});

</script>
@stop
