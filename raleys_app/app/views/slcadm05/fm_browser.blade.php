<?php 
    //echo '<pre>';print_r($report_data);exit;
?>
@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
         @include('slcadm05.slcadm05fm_menu')
    </header>
<?php 
    $params = array(
              'download' =>  'pdf',
              'userid' => $report_data['userid'],
              'update_date' => $report_data['update_date'],
              'update_time' => $report_data['update_time'],
              'item_code' => $report_data['item_code'],
              'new_value' => $report_data['new_value']
              ); 
              $queryString = http_build_query($params);
 ?>
<div class="container">
          <div class="row">
            <div class="col-md-6"><h3>Browse</h3></div>
            <div class="col-md-6">
                <span class="pull-right">
                    <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
                    <a href="{{ route('pdf-slcadm05-fm_report',$queryString) }}" target="_blank">
                    <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
                </span>
            </div>
          </div>
        </div>

<div class="container">
<!-- <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>
        Print</span></a> -->
      @if($data) 
    <table class="table table-striped" id="example">
        <thead>
          <tr>
            <th>User Id</th>
            <th> Date/Time</th>
            <th>UPC / PLU / SKU </th>
            <th>FM Note</th>
          </tr>
        </thead>
        <tbody>
        @foreach($data as $result)
        <tr>  
            <td>{{ $result->userid }}</td>
            <td>{{ date("d/m/Y", strtotime($result->update_date)) }}  {{ $result->update_time }}</td>
            <td>{{ $result->item_code }}</td>
           
            
            <td>{{ $result->new_value }}</td>
          </tr>
          
       
        @endforeach
        </tbody>
    </table>          
    @else
    <div class="alert alert-danger">
              <strong>Alert!</strong> No Result meet Query criteria.
    </div>
    @endif
   
</div>


@stop
