@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Combo Field <FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{ URL::route('slcadm00-combo-browser')}}" class="form-horizontal" method="post" role="form" style="display: block;">

           <div class="focusguard" id="focusguard-1" tabindex="1"></div>
            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">Master Field Number</label>
                  <div class="col-sm-6">
                     <select name="mstr_field_no" id="mstr_field_no" class="form-control" autofocus="" tabindex="2">
                            <option value="">Select</option>
                          @foreach($data->mstfld_cur as $mstrow)
                          <option value="{{$mstrow->field_no}}">{{$mstrow->description}}</option>
                          @endforeach
                      </select>
                    
                     @if($errors->has('mstr_field_no'))
                     {{ $errors->first('mstr_field_no')}}
                     @endif
                  </div>
               </div>

               {{-- <div class="form-group">
               <div class="col-md-offset-1 col-sm-9 justify ">
                <b>When the above Master Field is selected to be Modified,  
               also modify the value of the following Master Field.     
               See Help (F1) for more details</b>
                </div>
                </div> --}}
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Order of Appearance</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="seq_no" placeholder="Order of Appearance" name="seq_no" tabindex="3">
                     @if($errors->has('seq_no'))
                     {{ $errors->first('seq_no')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">When the Value is</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="when_value" placeholder="When the Value is" name="when_value" tabindex="4">
                     @if($errors->has('when_value'))
                     {{ $errors->first('when_value')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Update Master Field</label>
                  <div class="col-sm-6">
                    
                     
                       <select name="other_fld_1" id="other_fld_1" class="form-control" tabindex="5">
                            <option value="">Select</option>
                          @foreach($data->mstfld_cur as $mstrow)
                          <option value="{{$mstrow->field_no}}">{{$mstrow->description}}</option>
                          @endforeach
                      </select>
                     
                     @if($errors->has('other_fld_1'))
                     {{ $errors->first('other_fld_1')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Which may be Keyed with Field</label>
                  <div class="col-sm-6">
                     
                     <select name="other_fld_2" id="other_fld_2" class="form-control" tabindex="6">
                            <option value="">Select</option>
                          @foreach($data->mstfld_cur as $mstrow)
                          <option value="{{$mstrow->field_no}}">{{$mstrow->description}}</option>
                          @endforeach
                      </select>
                     
                     @if($errors->has('other_fld_2'))
                     {{ $errors->first('other_fld_2')}}
                     @endif
                  </div>
               </div>
               
         <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="last_updated_by" placeholder="Last Updated By" name="last_updated_by" value="" tabindex="7">
                     @if($errors->has('last_updated_by'))
                     {{ $errors->first('last_updated_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update" tabindex="8">
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="9" value="Search" class="btn">
                        <input type="reset" name="accept-submit" id="submit" tabindex="10" value="Reset" class="btn">
                          <input type="reset" id="cancel" tabindex=11 value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('/home')}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="12"></div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>

@section('jssection')
 @parent
 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
    $( "#last_update" ).datepicker({
        changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat:"mm/dd/yy"
    });
  $('#focusguard-2').on('focus', function() {
  $('#mstr_field_no').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#cancel').focus();
});

  } );
  </script>
   
      @endsection
@stop