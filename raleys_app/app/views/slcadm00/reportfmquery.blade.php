@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00reportfmmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Fm Notes Detail<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('slcadm00-report-fm-browse')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">User ID</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="userid" placeholder="User ID" name="userid" tabindex="2" autofocus=""/>
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                  </div>
               </div>

   
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">From Date</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="from_date" placeholder="Date" name="from_date" tabindex="3"/>
                     @if($errors->has('from_date'))
                     {{ $errors->first('from_date')}}
                     @endif
                  </div>
               </div>
                
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">To Date</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="to_date" placeholder="Date" name="to_date" tabindex="4" />
                     @if($errors->has('to_date'))
                     {{ $errors->first('to_date')}}
                     @endif
                  </div>
               </div>
               
              
              
               
              
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="5" value="Search" class="btn">
                        <input type="reset" name="accept-submit" id="submit" tabindex="6" value="Reset" class="btn">
                          <input type="reset" id="cancel" tabindex="7" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('/slcadm00/report')}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="8"></div>

            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>

@section('jssection')
 @parent
 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
    $( "#from_date,#to_date" ).datepicker({
        changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat:"mm/dd/yy"
    });
     $('#focusguard-2').on('focus', function() {
  $('#userid').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#cancel').focus();
});


  } );
  </script>

   
      @endsection
@stop