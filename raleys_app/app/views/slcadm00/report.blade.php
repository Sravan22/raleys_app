@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00menu')
    
    </header>
    <style type="text/css">
      table {
    border-collapse: collapse;
    width: 100%;
}
th, td {
   border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
    </style>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
       <style type="text/css">
                tr a {
                    text-decoration: underline;
                }
            </style>
<table align="center"  class="table table-striped " style="width: 700px; ">
    <thead>
    <tr>
        <th class="text-center">Report ID</th>
        <th class="text-center">Description</th>
    </tr>
    </thead>
    <tbody>
      <tr>
       <td class="text-center"> <a href="{{URL::route('slcadm00-report-mod-query')}}">SLAUDIT</a> </td>
        <td class="text-center">Modification Audit Report</td>
        </tr>
        <tr>
        <td class="text-center"><a href="{{URL::route('slcadm00-report-scan-query')}}">SLSCNDTL</a></td>
        <td class="text-center">Detail Audit Report</td>
        </tr>
        <tr>
        <td class="text-center"><a href="{{URL::route('slcadm00-report-fm-query')}}">SLFMNDTL </td>
        <td class="text-center">FM Note Report</td>
    
      </tr>
      
    </tbody>
  </table>

 </form>
</div>
@stop
