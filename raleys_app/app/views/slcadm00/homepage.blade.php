@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
        @include('slcadm00.slcadm00menu')
    </header>
    <style type="text/css" media="screen">
    h5 {
	font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
	font-size: 20px;
	font-style: normal;
	font-variant: normal;
	font-weight: 500;
	line-height: 26.4px;
}
    	
    </style>
<br>
<br>
<br>
<br>
<br>

<div class="container">
   <div class="row">
      <div class="col-md-4 col-md-offset-4 text-center">
         <b><h5>S H E L F &nbsp&nbsp   L A B E L &nbsp&nbsp   I N F O</h5></b>
         <b><h5>C O N F I R M A T I O N </h5></b>
       </div>
   </div>
   <br>

   <div class="row">
      <div class="col-md-12 text-center">
        <b><h4> SLIC SYSTEM MAINTENANCE </h4> </b>
      </div>
   </div>
   <br>
   <br>
   <div class="row">
      <div class="col-md-2 col-md-offset-5 text-center">
       <b>  <?php echo date('m/d/Y'); ?> </b>
      </div>
   </div>
  
</div>
@stop

