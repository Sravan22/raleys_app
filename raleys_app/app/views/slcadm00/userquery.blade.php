@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
    @include('slcadm00.slcadm00menu')
</header>
<div class="col-md-12">
    <br>
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <div class="alert alert-{{ $msg }}" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('alert-' . $msg) }}                               
    </div>
    @endif
    @endforeach
</div>
<div class="container">
    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >User Maintenance <FIELDSET></FIELDSET></div>
            </div>
            <div class="panel-body" >
                <form action="{{ URL::route('slcadm00-user-browser')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                <div class="focusguard" id="focusguard-1" tabindex="1"></div>

                    <div class="form-group" style="padding-top:20px">
                        <label for="inputPassword" class="control-label col-sm-5">User Id</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="userid" placeholder="User Id" name="userid" value="" tabindex="2" autofocus="" />
                            @if($errors->has('userid'))
                            {{ $errors->first('userid')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-5">Password</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="passwd" placeholder="Password" name="passwd" value="" tabindex="3"/>
                            @if($errors->has('passwd'))
                            {{ $errors->first('passwd')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="" tabindex="4" onkeydown="upperCaseF(this)"/>
                            @if($errors->has('name'))
                            {{ $errors->first('name')}}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Pos Type</label>
                        <div class="col-sm-6">
                            <select name="pos_type" id="pos_type" class="form-control" tabindex="5">
                                <option value="">Select</option>
                                @foreach($data->post_types as $posrow)
                                <option value="{{$posrow->id}}">{{$posrow->description}}</option>
                                @endforeach
                            </select>

                            @if($errors->has('pos_type'))
                            {{ $errors->first('pos_type')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Batch Maint</label>
                        <div class="col-sm-6">

                            <select class="form-control" id="batch_cd" placeholder="" name="batch_cd" tabindex="6">
                                <option value="">Select</option>
                                <option value="Y">Y</option>
                                <option value="N">N</option>

                            </select>


                            @if($errors->has('batch_cd'))
                            {{ $errors->first('batch_cd')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Add via FM</label>
                        <div class="col-sm-6">

                            <select class="form-control" id="add_cd" placeholder="" name="add_cd"  tabindex="7">
                                <option value="">Select</option>
                                <option value="Y">Y</option>
                                <option value="N">N</option>

                            </select>


                            @if($errors->has('add_cd'))
                            {{ $errors->first('add_cd')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Delete via Fm</label>
                        <div class="col-sm-6">

                            <select class="form-control" id="delete_cd" placeholder="" name="delete_cd" tabindex="8">
                                <option value="">Select</option>
                                <option value="Y">Y</option>
                                <option value="N">N</option>

                            </select>



                            @if($errors->has('delete_cd'))
                            {{ $errors->first('delete_cd')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Modify Via FM</label>
                        <div class="col-sm-6">

                            <select class="form-control" id="update_cd" placeholder="" name="update_cd" tabindex="9">
                                <option value="">Select</option>
                                <option value="A">A - All</option>
                                <option value="N">N - None</option>
                                <option value="S">S - Specific</option>
                              <!--   <option value="C">C - Clearance Price</option> -->
                                <!-- <option value="R">R</option> -->

                            </select>


                            @if($errors->has('update_cd'))
                            {{ $errors->first('update_cd')}}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-5">Last Updated By</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="last_updated_by" placeholder="Last Modifyd By" name="last_updated_by" tabindex="10" />
                            @if($errors->has('last_updated_by'))
                            {{ $errors->first('last_updated_by')}}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-5">Last Updated</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update" tabindex="11" />
                            @if($errors->has('last_update'))
                            {{ $errors->first('last_update')}}
                            @endif
                        </div>
                    </div>

                    <!--<div class="form-group">
                       <label for="inputPassword" class="control-label col-sm-5">FM Display</label>
                       <div class="col-sm-6">
                          <input type="text" class="form-control" id="" placeholder="FM Display" name="">
                          @if($errors->has(''))
                          {{ $errors->first('')}}
                          @endif
                       </div>
                    </div>
     
                    <div class="form-group">
                       <label for="inputPassword" class="control-label col-sm-5">FM Add</label>
                       <div class="col-sm-6">
                          <input type="text" class="form-control" id="" placeholder="FM Add" name="">
                          @if($errors->has(''))
                          {{ $errors->first('')}}
                          @endif
                       </div>
                    </div>
     
                    <div class="form-group">
                       <label for="inputPassword" class="control-label col-sm-5">FM Modify</label>
                       <div class="col-sm-6">
                          <input type="text" class="form-control" id="" placeholder=" FM Modify" name="">
                          @if($errors->has(''))
                          {{ $errors->first('')}}
                          @endif
                       </div>
                    </div>
                    
                    
                    <div class="form-group">
                       <label for="" class="control-label col-sm-5">FM Top</label>
                       <div class="col-sm-6">
                          <input type="Date" class="form-control" id="" placeholder="FM Top" name="">
                          @if($errors->has(''))
                          {{ $errors->first('')}}
                          @endif
                       </div>
                    </div>-->

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12" style="margin-left: 40%">
                                <input type="submit" name="accept-submit" id="submit" tabindex="12" value="Search" class="btn">
                                <input type="reset" name="accept-submit" id="submit" tabindex="13" value="Reset" class="btn">
                          <input type="reset" id="reset" tabindex="14" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('/home')}}'">
                                {{ Form::token()}}
                            </div>
                        </div>
                    </div>
                    <div class="focusguard" id="focusguard-2" tabindex="15"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .form-horizontal .control-label {
        text-align: right; 
        /* padding-left: 60px; */
    }
</style>

@section('jssection')
@parent

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
$(function () {
  $("#last_update").datepicker({
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat: "mm/dd/yy"
  });
   $('#focusguard-2').on('focus', function() {
  $('#userid').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#reset').focus();
});

});
</script>

@endsection
@stop