@extends('layout.dashboard')
@section('page_heading','Add Field')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00menu')
</header>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">Add Field</div>
         </div>
         <div style="padding-top:30px;" class="panel-body" >
            <form action="" class="form-horizontal" method="post" role="form" style="display: block;" >
               <div class="form-group ">
                  <div class="col-md-offset-1 col-md-4">
                     <label for="inputPassword" class="control-label col-sm-12">User ID</label>
                  </div>
                  
                  <div class="col-sm-7">
                     <input type="text" readonly="" class="form-control" name="" value="">
                </div>
               </div>
               <div class="form-group">
                  <div class="col-md-offset-1 col-md-4">
                     <label for="inputPassword" class="control-label col-sm-12">Sequence No.</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" readonly="" class="form-control" name="" value="">
                     
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-offset-1 col-md-4">
                     <label for="inputPassword" class="control-label col-sm-12">Field No.</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="number" class="form-control" name="" id="">
                     
                  </div>
               </div>
               <div class="form-group" style="padding-top: 10px">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Add" class="btn">
                        <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class=""></div>
@stop
<script type="text/javascript">
   function validateForm()
   {
      //alert('validateForm');   
      //var new_limit = document.getElementById('new_limit');
      var new_limit = document.getElementById("new_limit").value;
     if(new_limit < 1)
      {
         alert('Limit Must Be Greater Than Zero');return false;
      }
      else if(new_limit > 999)
      {
         alert('Limit Cannot Be More than 999');return false;
      }
      // else
      // {
      //      alert('Proper Data');return false;
      // }
   }
   
</script>