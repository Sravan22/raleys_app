@extends('layout.dashboard')
@section('page_heading','Master Field')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00menu')
    
    </header>
  <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
          <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>

<table class="table table-striped">
    <thead>
    <tr>
        <th class="text-center">Field Number</th>
        <th class="text-center">Description</th>
        <th class="text-center">Field Type Code</th>
                </tr>
    </thead>
    <tbody>
       @foreach($data as $row)
      <tr>       
        <td class="text-center"><a href="{{URL::route('slcadm00-master-update',array('field_no'=>$row->field_no))}}">{{$row->field_no}}</a></td>        
        <td class="text-center">{{$row->description}}</td>
         <td class="text-center">{{$row->type_cd}}</td>
      </tr>
      @endforeach
      
    </tbody>
  </table>
     <div class="row">
    <div class="col-sm-6" align="left"> {{$pagination->links()}}</div>
        <div class="col-sm-6" align="left">
           
            <input type="button" value="Back" class="btn" style=" margin: 20px 0;" onclick="window.location.href='{{URL::route('slcadm00-master-query')}}'" />
        </div>
        </div>
 </form>
</div>
@stop
