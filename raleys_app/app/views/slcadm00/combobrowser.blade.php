@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00menu')
    
    </header>
 <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
           <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>

<table class="table table-striped">
    <thead>
    <tr>
        <th class="text-center">Master Field Number</th>
        <th class="text-center">Seq.</th>
        <th class="text-center">When value is ..</th>
        <th class="text-center">Use Master Field Number</th>
        <th class="text-center">This Master Field Number May Proceed</th>
                </tr>
    </thead>
    <tbody>
    
      
       @foreach($data as $row)
      <tr>
      
        <td class="text-center"><a href="{{URL::route('slcadm00-combo-update',array('mstr'=>$row->mstr_field_no,'whv'=>$row->when_value,'otherfld1'=>$row->other_fld_1))}}">{{$row->mstr_field_no}}</a></td>
        <td class="text-center">{{$row->seq_no}}</td>
        <td class="text-center">{{$row->when_value}}</td>
        <td class="text-center">{{$row->other_fld_1}}</td>
        <td class="text-center">{{$row->other_fld_2}}</td> 
      </tr>
      @endforeach
      
    </tbody>
  </table>

<div class="row">
    <div class="col-sm-6" align="left"> {{$pagination->links()}}</div>
        <div class="col-sm-6" align="left">
           
            <input type="button" value="Back" class="btn" style=" margin: 20px 0;" onclick="window.location.href='{{URL::route('slcadm00-combo-query')}}'" />
        </div>
        </div>

 
 </form>
</div>
@stop
