@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('slcadm00.slcadm00menu')
    
    </header>
<div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
      <style type="text/css">
   tr a {
   text-decoration: underline;
   }
   
</style>


<table align="center" class="table table-striped" style="width: 80%">
    <thead>
    <tr>
        <th class="text-center">Active</th>
        <th class="text-center">Id</th>
        <th class="text-center">Description</th>
                </tr>
    </thead>
    <tbody>
        
        @foreach($data as $row)
      <tr>
        <td class="text-center">{{$row->active_switch}}</td>
        <td class="text-center"><a href="{{URL::route('slcadm00-types-update',array('id'=>$row->id))}}">{{$row->id}}</a></td>
        <td class="text-center">{{$row->description}}</td>
      </tr>
      @endforeach
      
      
      
    </tbody>
  </table>
<div class="row">
    
        <div class="col-sm-12" align="center">
           
            <input type="button" value="Back" class="btn"  onclick="window.location.href='{{URL::route('slcadm00-types-query')}}'" />
        </div>
        </div>
 </form>
</div>
@stop
