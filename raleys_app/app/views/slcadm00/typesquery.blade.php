@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
      </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >POS Type</div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('slcadm00-types-browser')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group" style="padding-top: 20px;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

                  <label for="inputPassword" class="control-label col-sm-5">POS Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="id"  placeholder="Pos Type Code" name="id" tabindex="2" autofocus="" maxlength="1" onkeydown="upperCaseF(this)" />
                     @if($errors->has('id'))
                     {{ $errors->first('id')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="description" placeholder="Description" name="description" tabindex="3" onkeydown="upperCaseF(this)"/>
                     @if($errors->has('description'))
                     {{ $errors->first('description')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Active</label>
                  <div class="col-sm-6">
                        <input type="text" class="form-control" id="active_switch" placeholder="Active" name="active_switch" onkeydown="upperCaseF(this)" readonly="" />

                       <!--  <select class="form-control" id="active_switch" placeholder="Active" name="active_switch" readonly>
                         <option value=""></option> -->
                          <!-- <option value="Y">Y</option>
                          <option value="N">N</option> -->
                     <!--  </select> -->
                      
                   
                     @if($errors->has('active_switch'))
                     {{ $errors->first('active_switch')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="last_updated_by" placeholder="Last Updated By" name="last_updated_by" tabindex="4"/>
                     @if($errors->has('last_updated_by'))
                     {{ $errors->first('last_updated_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="last_update" placeholder="mm/dd/yyyy" name="last_update" tabindex="5"/>
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-10" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="6" value="Search" class="btn"/>
                         <input type="reset" id="submit" tabindex="7" value="Reset" class="btn" />
                          <input type="reset" id="" tabindex="8" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('/home')}}'"/>
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="9"></div>

            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@section('jssection')
 @parent
 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
    $( "#last_update" ).datepicker({
        changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat:"mm/dd/yy"
    });

     $('#focusguard-2').on('focus', function() {
  $('#id').focus();
});

$('#focusguard-1').on('focus', function() {
  $('.btn').focus();
});

  } );
  </script>
   
      @endsection
@stop