@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
    @include('slcadm00.slcadm00menu')
</header>
<div class="col-md-12">
    <br>
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <div class="alert alert-{{ $msg }}" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('alert-' . $msg) }}                               
    </div>
    @endif
    @endforeach
</div>
<div class="container">
    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >User Maintenance </div>
            </div>
            <div class="panel-body" >
                <form action="{{URL::route('slcadm00-user-add')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                <div class="focusguard" id="focusguard-1" tabindex="0"></div>
                    <div class="form-group" style="padding-top:20px">
                        <label for="inputPassword" class="control-label col-sm-5">User Id</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="userid" placeholder="User Id" name="userid" value="{{$data->userid}}" autofocus="" tabindex="1">
                            @if($errors->has('userid'))
                            {{ $errors->first('userid')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-5">Password</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="passwd" placeholder="Password" name="passwd" value="{{$data->passwd}}" tabindex="2"/>
                            @if($errors->has('passwd'))
                            {{ $errors->first('passwd')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{$data->name}}" tabindex="3" />
                            @if($errors->has('name'))
                            {{ $errors->first('name')}}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Pos Type</label>
                        <div class="col-sm-6">
                            <select name="pos_type" id="pos_type" class="form-control" tabindex="4">

                                @foreach($data->post_types as $posrow)
                                <option value="{{$posrow->id}}" @if($data->pos_type==$posrow->id)  {{'selected'}} @endif>{{$posrow->description}}</option>
                                @endforeach
                            </select>

                            @if($errors->has('pos_type'))
                            {{ $errors->first('pos_type')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Batch Maint</label>
                        <div class="col-sm-6">

                            <select class="form-control" id="batch_cd" placeholder="" name="batch_cd" tabindex="5">

                                <option value="Y" @if($data->batch_cd=='Y')  {{'selected'}} @endif>Y</option>
                                <option value="N" @if($data->batch_cd=='N' || $data->batch_cd=='')  {{'selected'}} @endif>N</option>

                            </select>


                            @if($errors->has('batch_cd'))
                            {{ $errors->first('batch_cd')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Add via FM</label>
                        <div class="col-sm-6">

                            <select class="form-control" id="add_cd" placeholder="" name="add_cd" tabindex="6">

                                <option value="Y" @if($data->add_cd=='Y')  {{'selected'}} @endif>Y</option>
                                <option value="N" @if($data->add_cd=='N' || $data->add_cd=='')  {{'selected'}} @endif>N</option>

                            </select>


                            @if($errors->has('add_cd'))
                            {{ $errors->first('add_cd')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Delete via Fm</label>
                        <div class="col-sm-6">

                            <select class="form-control" id="delete_cd" placeholder="" name="delete_cd" tabindex="7">

                                <option value="Y" @if($data->delete_cd=='Y')  {{'selected'}} @endif>Y</option>
                                <option value="N" @if($data->delete_cd=='N' || $data->delete_cd=='')  {{'selected'}} @endif>N</option>

                            </select>



                            @if($errors->has('delete_cd'))
                            {{ $errors->first('delete_cd')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Modify Via FM</label>
                        <div class="col-sm-6">

                            <select class="form-control" id="update_cd" placeholder="" name="update_cd" tabindex="8">

                                <option value="A" @if($data->update_cd=='A')  {{'selected'}} @endif>A - All</option>
                                <option value="N" @if($data->update_cd=='N')  {{'selected'}} @endif>N - None</option>
                                <option value="S" @if($data->update_cd=='S')  {{'selected'}} @endif>S - Specific</option>
                                <option value="C" @if($data->update_cd=='C')  {{'selected'}} @endif>C - Clearance Price</option>
                                <option value="R" @if($data->update_cd=='R')  {{'selected'}} @endif>R</option>

                            </select>


                            @if($errors->has('update_cd'))
                            {{ $errors->first('update_cd')}}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-5">Last Updated By</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="last_updated_by" placeholder="Last Updated By" name="last_updated_by" value="{{$data->last_updated_by}}" readonly>
                            @if($errors->has('last_updated_by'))
                            {{ $errors->first('last_updated_by')}}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-5">Last Updated</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update" value="{{date('m/d/Y H:i',strtotime($data->last_update))}}" readonly>
                            @if($errors->has('last_update'))
                            {{ $errors->first('last_update')}}
                            @endif
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12" style="margin-left: 48%">
                                <input type="submit" name="accept-submit" id="submit" tabindex="9" value="Add" class="btn">
                                <input type="button" onclick="window.location.href='{{URL::route('slcadm00-user-query')}}'" name="login-submit" id="cancel" tabindex="10" value="Cancel" class="btn">
                                {{ Form::token()}}
                            </div>
                        </div>
                    </div>
                        <div class="focusguard" id="focusguard-2" tabindex="11"></div>

                </form>



                <table class="table table-striped">
                    <tr>
                        <td>FM Display:</td><td>{{$data->disp_dtl}}</td>
                    </tr>
                    <tr>
                        <td>FM Add:</td><td>{{$data->add_dtl}}</td>
                    </tr>
                    <tr>
                        <td>FM Modify:</td><td>{{$data->upd_dtl}}</td>
                    </tr>
                    <tr>
                        <td>FM Top:</td><td>{{$data->top_dtl}}</td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .form-horizontal .control-label {
        text-align: right; 
        /* padding-left: 60px; */
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript">
    $(function() {
 $('#focusguard-2').on('focus', function() {
  $('#userid').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#cancel').focus();
});

    });
</script>
@stop