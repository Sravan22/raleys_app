<div class="container" >
    <div class="menu_search_line" >
        <ul class="navbar dib_float_left" >
            <div class="dropdown sub">
                <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                    Types<span class="caret"></span>
                </a>
                <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><a href="{{ URL::route('slcadm00-types-query')}}">Search</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-types-browser')}}">Browse</a></li>-->


                    <li><a href="{{ URL::route('slcadm00-types-add')}}">Add</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-types-update')}}">Update</a></li>-->

                </ul>
            </div>  
            <div class="dropdown sub">
                <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                    Master <span class="caret"></span>
                </a>
                <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><a href="{{ URL::route('slcadm00-master-query')}}">Search</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-master-browser')}}">Browse</a></li>-->


                    <li><a href="{{ URL::route('slcadm00-master-add')}}">Add</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-master-update')}}">Update</a></li>-->

                </ul>
            </div>  
            <div class="dropdown sub">
                <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                    Pos<span class="caret"></span>
                </a>
                <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><a href="{{ URL::route('slcadm00-pos-query')}}">Search</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-pos-browser')}}">Browse</a></li>-->


                    <li><a href="{{ URL::route('slcadm00-pos-add')}}">Add</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-pos-update')}}">Update</a></li>-->

                </ul>
            </div>  
            <div class="dropdown sub">
                <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                    Combo<span class="caret"></span>
                </a>
                <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><a href="{{ URL::route('slcadm00-combo-query')}}">Search</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-combo-browser')}}">Browse</a></li>-->
                    <li><a href="{{ URL::route('slcadm00-combo-add')}}">Add</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-combo-update')}}">Update</a></li>-->

                </ul>
            </div> 

            <div class="dropdown sub">
                <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                    Users <span class="caret"></span>
                </a>
                <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><a href="{{ URL::route('slcadm00-user-query')}}">Search</a></li>
                    <!--          <li><a href="{{ URL::route('slcadm00-user-browser')}}">Browse</a></li>-->
                      <li><a href="{{ URL::route('slcadm00-user-add')}}">Add</a></li>
                     <?php /* <li><a href="{{ URL::route('slcadm00-user-update')}}">Update</a></li>
                      <li><a href="{{ URL::route('slcadm00-user-fm-display')}}">Fm_display</a></li>
                      <li><a href="{{ URL::route('slcadm00-user-fm-add')}}">Fm_add</a></li>
                      <li><a href="{{ URL::route('slcadm00-user-fm-modify')}}">Fm_modify</a></li>
                      <li><a href="{{ URL::route('slcadm00-user-fm-top')}}">Fm_top</a></li> */ ?> 
                </ul>
            </div> 
            <div class="dropdown sub">
                <a id="dLabel" href="{{ URL::route('slcadm00-report')}}" role="button" 
                   class="menu_choice" data-target="#">
                    Report
                </a>
            </div>
			 <div class="dropdown sub">
                <a id="dLabel" href="{{ URL::route('user-home')}}" role="button" 
                   class="menu_choice" data-target="#">
                                     Exit            
                </a>
            </div>
