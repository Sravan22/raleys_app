@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('slcadm00.slcadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Master Field</div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('slcadm00-master-browser')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label col-sm-5">Field Number</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control keyfirst" id="field_no" placeholder="Field Number" name="field_no" value="" autofocus="" tabindex="2" />
                     @if($errors->has('field_no'))
                     {{ $errors->first('field_no')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Description</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="description" placeholder="Description" name="description" value="" tabindex="3" onkeydown="upperCaseF(this)" />
                     @if($errors->has('description'))
                     {{ $errors->first('description')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Field Type Code</label>
                  <div class="col-sm-6">
                      
                      
                      <select class="form-control" id="type_cd" placeholder="" name="type_cd" tabindex="4">
                          <option value="">Select Field Type Code</option>
                          <option value="N">N = Normal</option>
                           <option value="C">C = Combo Field</option>
                           <option value="F">F = FM Note</option>
                      </select>
                      
                      
                      
                     
                     @if($errors->has('type_cd'))
                     {{ $errors->first('type_cd')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="last_updated_by" placeholder="Last Updated By" name="last_updated_by" value="" tabindex="5"/>
                     @if($errors->has('last_updated_by'))
                     {{ $errors->first('last_updated_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update" value="" tabindex="6"/>
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                        <input type="submit" name="accept-submit" id="submit" tabindex="7" value="Search" class="btn"/>
                       <input type="reset" name="accept-submit" id="submit" tabindex="8" value="Reset" class="btn keylast"/>
                          <input type="reset" id="reset" tabindex="9" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('/home')}}'"/>
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="10"></div>
            </form>
         </div>
      </div>
   </div>
</div>

@section('jssection')
 @parent
 
 
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
    $( "#last_update" ).datepicker({
        changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat:"mm/dd/yy"
    });

    /*Allow number only in field Number*/
    $('[id^=field_no]').keypress(validateNumber);

  });
  
  </script>
   
      @endsection
@stop