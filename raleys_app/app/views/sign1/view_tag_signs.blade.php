<?php 
   //echo '<pre>';print_r($stock_array);exit;
 ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
  <div class="col-md-6 text-center" style="margin-left:290px;">
         <b><h4>TAGS & SIGNS QUEUED FOR PRINT<br><br>
         User Id:{{$user_id}} &nbsp;&nbsp; &nbsp; &nbsp;Date:{{date("m-d-Y");}}</h4></b>
      </div> 
</header><br>
<div class="container">
   <div class="row">
   @if(!empty($login_cur))
      
     <div class="panel-body col-md-6 col-md-offset-3 text-center" style="padding-top:30px" >
   <div class="table-responsive">
      <table class="table table-bordered table-colored">
         <thead>
            <tr>
               <th class="text-center">Stock</th>
               <th class="text-center">
                  Sign Type
               </th>
               <th class="text-center">Copies Requested</th>
               <th class="text-center">Sheet Count</th>
               
            </tr>
         </thead>
         <tbody>
            <?php $i=0;  ?>
            @foreach($login_cur as $array)
          <tr>
              <?php  $x=$array->sum;
                     $y=$array->sign_count;
                     $sheet_count=$x/$y;
                     $mod_value=fmod($x, $y);
               ?>
               <td><u><a href="#" id="{{ $user_id;}}^{{ $array->sign_id;}}^{{ $prnt_yet;}}^{{ $array->batch_no;}}^{{ $start_date;}}^{{ $end_date;}}^{{$i}}" onclick="print_submit(this.id)">{{ $array->stock_code;}}</a></u></td>
               <td>{{ $array->sign_name;}}</td>
               <td>{{ $array->sum;}}</td>
               
                 <?php if($mod_value > 0){?>
                <td>{{round($sheet_count+1)}} </td> 
                <?php  }
                else {?> 
                <td>{{round($sheet_count)}} </td> 
                <?php } ?>


                 

               
               <!-- <td>{{ $array->sum/$array->sign_count;}}</td> -->
               
            </tr>
            <?php $i++; ?>
              
                    
            @endforeach
            
           
         </tbody>
      </table>
      <div class="col-sm-12" align="center">
           <input type="button" name="login-submit" id="submit" tabindex="4" onClick="document.location.href='{{URL::to('/home')}}'" value="Cancel" class="btn">
                                               
                                 </div>
   </div>
   </div>
   @else
    <div class="panel-body col-md-6 col-md-offset-3 text-center" style="padding-top:30px" >
    <div class="flash-message">
        <p class="alert alert-warning">Their is no data for this date. <a href="{{URL::to('/home')}}" style="color:blue;">Go Back</a> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        
    </div>
    </div>
    @endif


   </div>
   </div>
   <script>

   function print_submit(id){
    if(confirm("Insert Stock\n FACE UP; RIGHT SIDE FIRST \nwhen ready, press ENTER print")){
       alert(id);
       var myVal = id;
    var myVal = myVal.split('^');
    //alert(myVal);
    var user_id = myVal[0];
    var sign_id = myVal[1];

    var prnt_yet = myVal[2];
    var batch_no = myVal[3];
    var start_date = myVal[4];
    var i = myVal[6];
    //var sign_data = val;
    var end_date = myVal[5];
    //var seq_no = myVal[6];
    $.ajax({
 
         url: "prnt_sh_file",
          type: "GET", 
         data: { user_id: user_id,sign_id:sign_id, prnt_yet:prnt_yet, batch_no:batch_no,start_date:start_date,end_date:end_date},
         success: function(data) {
              alert(data);return false;
            if(data == 'printed'){
              alert('Print Done');
            }    
            else
            {
              alert('Printing Aborted');
            } 
         }
       });
    }
    else{

    }
   }
   </script>
   @endsection
@stop