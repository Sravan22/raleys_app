@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('sign1.sign1reportmodmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Modification Audit<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('sign1-report-mod-browse')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">User ID</label>
                  <div class="col-sm-6">
                    {{$data->userid}} {{$data->user_name}}
                  </div>
               </div>

   
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Date</label>
                  <div class="col-sm-6">
                     {{date('m/d/Y',strtotime($data->update_date))}} 
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Time</label>
                  <div class="col-sm-6">
                     {{$data->update_time}}
                  </div>
               </div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Action Code</label>
                  <div class="col-sm-6">
                    {{$data->action_cd}} {{$data->action_desc}}
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">Maintenance Type</label>
                  <div class="col-sm-6">
                     {{$data->maint_type_cd}} {{$data->maint_desc}}
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-5"> UPC / PLU / SKU</label>
                  <div class="col-sm-6">
                     {{$data->item_code}}
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Field Number</label>
                  <div class="col-sm-6">
                    
                     {{$data->mstr_fld_no}}
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Original Value</label>
                  <div class="col-sm-6">
                     {{$data->orig_value}}
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">New Value</label>
                  <div class="col-sm-6">
                     {{$data->new_value}}
                  </div>
               </div>
               

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        
                       
                         <input type="button" onclick="window.location.href='{{URL::route('sign1-report-mod-query')}}'" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop