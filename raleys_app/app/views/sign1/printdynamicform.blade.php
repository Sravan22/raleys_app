<?php
   //echo '<pre>';print_r($sign_line_array);exit;
?>
@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-7 col-md-offset-2 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Sign Stock: {{ $stock_code }} -- {{ $sign_name }}</div>
         </div>
         <div class="panel-body" >
             
            
            <form action="{{URL::route('dynamic-form-post')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               @if(!empty($sign_line_array))
               @for($i=0;$i<count($sign_line_array);$i++)
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-5">{{trim($sign_line_array[$i]['element_prompt'] ) }}</label>
                  <div class="col-sm-6">

                  <input type="hidden" name="stock_code" value="{{ $stock_code }}">
                             <input type="hidden" name="sign_id" value="{{ $sign_id }}">
                   <input type="hidden" name="special_proc_cd[]" value="{{ $sign_line_array[$i]['special_proc_cd'] }}">

                     <input type="text" class="form-control class_field{{ $i }} field_name_{{ $sign_line_array[$i]['special_proc_cd'] }}"   name="sign_line[]" <?php if($sign_line_array[$i]['element_type_cd'] == "S"){ echo 'style="width: 98%;text-transform: uppercase"'; } ?>      style="width: 98%"  onblur="myFunction(this.id,this.value);" id="{{ $sign_line_array[$i]['required_sw'] }}^{{ $sign_line_array[$i]['element_type_cd'] }}^{{ $sign_line_array[$i]['special_proc_cd'] }}^{{ $sign_line_array[$i]['max_length'] }}^{{ $i }}">
                      <div class="error_msg_empty error_msg<?php echo $i; ?>"></div>    
                  </div>
               </div>
        
         @endfor
         @endif
         <div class="form-group" style="padding-top: 20px;">
         <div class="row">
         <div class="col-sm-12" align="center">
         <input type="submit" name="login-submit" id="submit" tabindex="<?php   ?>" value="Print" class="btn">
         <input type="button" name="reset-submit" id="submit" tabindex="<?php   ?>" value="Reset" class="btn reset">
         <input type="button" onclick="history.go(-1);" name="cancel-submit" id="submit" tabindex="<?php   ?>" value="Cancel" class="btn">
         {{ Form::token()}}
         </div>
         </div>
         </div>
         </form>
          </div>
      </div>
   </div>
</div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
  $(".reset").on('click',function(){
      //alert('hiiii');
      $(this).closest('form').find("input[type=text], textarea").val("");
      //$('.focus_out').focusout()
      $('.error_msg_empty').hide();
      //location.reload();
  });
  //$('.error_msg_empty').show();
});
function myFunction(id,val) {
  $('.error_msg_empty').show();
    console.log("val is"+id);
   var myVal = id;
    var myVal = myVal.split('^');
    //alert(myVal);
    var required_sw = myVal[0];
    var element_type_cd = myVal[1];

    var special_proc_cd = myVal[2];
    //alert(special_proc_cd);
    var max_length = myVal[3];
     var sign_data = val;
    var i = myVal[4];
    $('.error_msg'+i).text("");
    
    if(special_proc_cd == "RP") {
      var save_reg_price=0;
       var save_for_qty=$(".field_name_FQ").val();
      var save_sale_price=$(".field_name_SP").val();
      var save_reg_price=$(".field_name_RP").val();
      if(sign_data > 0){
        save_reg_price=sign_data;
        if(save_sale_price > 0){

        }
        else{
          if(save_reg_price > 0){
            $('.error_msg'+i).text("Sale Price required prior to Regular Price's entry"); 
        $('.class_field'+i).val('');
        $('.class_field'+i).focus();
        return false;
          }
        }
    }
  }
    

    if(special_proc_cd == "WP"){
      if(sign_data > 0){
      var save_was_price=0;
      console.log("inside wp");
      var save_for_qty=$(".field_name_FQ").val();
      var save_sale_price=$(".field_name_SP").val();
      save_was_price=$(".field_name_WP").val();
       //console.log("save_was_price"+save_was_price);
       if(save_sale_price > 0){
       if(save_for_qty > 1){

       }else{
        save_for_qty=1;
       }
          var ws_unit_price=save_sale_price/save_for_qty;
       console.log(ws_unit_price);
       if(save_was_price > ws_unit_price){
       }
       else{
         $('.error_msg'+i).text("WAS Price must be larger than Sale Price"); 
        $('.class_field'+i).val('');
        $('.class_field'+i).focus();
        return false;
       }
     }else{
       if(save_was_price > 0){
          $('.error_msg'+i).text("Sale Price required prior to WAS Price's entry"); 
          $('.class_field'+i).val('');
          $('.class_field'+i).focus();
          return false;
        }
     }

    }

  }
  //  if(special_proc_cd == "SA")
  //    {
  //      // Special processing for Save Amount
  //      if($('.field_name_RP')[0].val()){
  //      var save_for_qty=$(".field_name_FQ").val();
  //      var save_sale_price=$(".field_name_SP").val();
  //      var save_reg_price=$(".field_name_RP").val();
       
  //      if(save_for_qty > 0){

  //      }else{
  //       save_for_qty=1;
  //      }
  //      var ws_save_amt = ( save_for_qty * save_reg_price) - save_sale_price;
  //      console.log(ws_save_amt);
  //      if(ws_save_amt < 0){
  //       var error_value="Error! Invalid Everyday Price save_reg_price and Sale Price save_sale_price - creates a negative Save Amount";
  //       $('.error_msg'+i).text("Invalid Everyday Price,and Sale Price- creates a negative Save Amount"); 
  //       $('.class_field'+i).val('');
  //       $('.class_field'+i).focus();
  //       return false;
  //      }
  //      else{

  //      $('.field_name_SA').val(ws_save_amt.toFixed(2));
  //    }
  //   }
  //   else
  //   {
  //     var save_was_price=0;
  //     console.log("inside wp");
  //     var save_for_qty=$(".field_name_FQ").val();
  //     var save_sale_price=$(".field_name_SP").val();
  //     save_was_price=$(".field_name_WP").val();
  //      if(save_for_qty > 1){

  //      }else{
  //       save_for_qty=1;
  //      }
       
     
  //    var save_amount=(save_was_price*save_for_qty)-save_sale_price;
  //    $('.field_name_SA').val(save_amount.toFixed(2));
  //   }
  // }


  if(special_proc_cd == "SA")
     {
       // Special processing for Save Amount
       var save_reg_price=$(".field_name_RP").val();
       //alert("outside"+save_reg_price);
       if(save_reg_price === undefined){
        var save_was_price=0;
      console.log("inside wp");
      var save_for_qty=$(".field_name_FQ").val();
      var save_sale_price=$(".field_name_SP").val();
      save_was_price=$(".field_name_WP").val();
       if(save_for_qty > 1){

       }else{
        save_for_qty=1;
       }
       
     
     var save_amount=(save_was_price*save_for_qty)-save_sale_price;
     //alert("save amount"+save_amount);
     $('.field_name_SA').val(save_amount.toFixed(2));


        
    }
    else
    {
       var save_for_qty=$(".field_name_FQ").val();
       var save_sale_price=$(".field_name_SP").val();
       var save_reg_price=$(".field_name_RP").val();
       //alert("inside regular price");
       if(save_for_qty > 0){

       }else{
        save_for_qty=1;
       }
       var ws_save_amt = ( save_for_qty * save_reg_price) - save_sale_price;
       console.log(ws_save_amt);
       if(ws_save_amt < 0){
        var error_value="Error! Invalid Everyday Price save_reg_price and Sale Price save_sale_price - creates a negative Save Amount";
        $('.error_msg'+i).text("Invalid Everyday Price,and Sale Price- creates a negative Save Amount"); 
        $('.class_field'+i).val('');
        $('.class_field'+i).focus();
        return false;
       }
       else{

       $('.field_name_SA').val(ws_save_amt.toFixed(2));
     }
    }
  }
  
    //  if(special_proc_cd == "S3"){
    //  var save_for_qty=$(".field_name_FQ").val();
    //  var save_sale_price=$(".field_name_SP").val();
    //  var save_reg_price=$(".field_name_RP").val();
    //  if(save_for_qty > 0){

    //    }else{
    //     save_for_qty=1;
    //    }
    //    var ws_save_amt1 = ( save_for_qty * save_reg_price) - save_sale_price;
    //    console.log(ws_save_amt1);
    //    if(ws_save_amt1 < 0){
        
    //     $('.error_msg'+i).text("Invalid Everyday Price and Sale Price"); 
    //     $('.class_field'+i).val('');
    //     $('.class_field'+i).focus();
    //    return false;
    //    }
    //   else{

    //    $('.field_name_SA').val(ws_save_amt1);
    //  }

    // }
    if(special_proc_cd == "RP")
     {
      //alert("1 st one");
      var save_reg_price=0;
      var save_for_qty=$(".field_name_S1").val();
      
      if($('.field_name_RP')[0].val()){
        save_reg_price=$(".field_name_RP").val();
       
        if(save_for_qty > 0){

       }else{
        if(save_reg_price > 0){
          //alert("inside rp err msg");
          $('.error_msg'+i).text("Sale Price required prior to Regular Price's entry"); 
          return false;
          
        }
       }

     }
   }

    if(special_proc_cd == "30"){
      var one_qty_price=$(".field_name_S1").val();
      var save_reg_price=$(".field_name_RP").val();
      var six_qty_price=(one_qty_price * 70)/100;
      var save_amt=((one_qty_price-six_qty_price)*6)+((save_reg_price-one_qty_price)*6);
      if(save_amt > 0){
      $('.field_name_30').val(six_qty_price.toFixed(2));
      $('.field_name_S3').val(save_amt.toFixed(2));
    }
    else{
      $('.error_msg'+i).text("Invalid Everyday Price("+ save_reg_price +") and Sale Price("+ one_qty_price+ ") - creates a negative Save Amount"); 
      return false;
    }
    }
    if(special_proc_cd == "10"){
      var one_qty_price=$(".field_name_S1").val();
      var save_reg_price=$(".field_name_RP").val();
      var six_qty_price=(one_qty_price * 90)/100;
      var save_amt=((one_qty_price-six_qty_price)*6)+((save_reg_price-one_qty_price)*6);
      $('.field_name_10').val(six_qty_price.toFixed(2));
      $('.field_name_S3').val(save_amt.toFixed(2));
    }
    if(special_proc_cd == "25"){
      var save_reg_price=$(".field_name_SP").val();
      var clearence_price=(save_reg_price * 75)/100;
      var save_amt=save_reg_price - clearence_price;
      $('.field_name_25').val(clearence_price.toFixed(2));
      $('.field_name_S3').val(save_amt.toFixed(2));
    }
    if(special_proc_cd == "50"){
      var save_reg_price=$(".field_name_SP").val();
      var clearence_price=(save_reg_price * 50)/100;
      var save_amt=save_reg_price - clearence_price;
      $('.field_name_50').val(clearence_price.toFixed(2));
      $('.field_name_S3').val(save_amt.toFixed(2));
    }
    if(special_proc_cd == "75"){
      var save_reg_price=$(".field_name_SP").val();
      var clearence_price=(save_reg_price * 25)/100;
      var save_amt=save_reg_price - clearence_price;
      $('.field_name_75').val(clearence_price.toFixed(2));
      $('.field_name_S3').val(save_amt.toFixed(2));
    }
    
 
    var max_length = myVal[3];
     var sign_data = val;
    var i = myVal[4];

     $.ajax({
 
         url: "edit_sign_data_for_AtoG/"+i,
          type: "POST", 
         data: { required_sw: required_sw,max_length:max_length, element_type_cd:element_type_cd, special_proc_cd:special_proc_cd,sign_data: sign_data},
         success: function(data) {
          console.log("data is"+data);
            if(data){
              $('.error_msg'+i).text(data);
              $('.class_field'+i).val('');
             $('.class_field'+i).focus();
              return false;   
            }    
            else
            {
               $('.error_msg'+i).text('');return false;   
            } 
         }
       });
  }
  </script>
@stop