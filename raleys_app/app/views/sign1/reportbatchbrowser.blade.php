@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('sign1.sign1reportbatchmenu')
    
    </header>
 <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
  <div class="menu_search_line">    
</div>
   
     
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
        <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>
<table class="table table-striped">
    <thead>
    <tr>
        <th>User Id</th>
        <th>Batch Id</th>
        <th>Status</th>
        <th> Batch Type </th>
        <th> Created</th>
       
    </tr>
    </thead>
    <tbody>
        
        @foreach($data as $row)
      <tr>
          <td><a href="{{URL::route('sign1-report-batch-view',array_merge($paginate_array,array('userid'=>$row->userid,'seq_no'=>$row->seq_no)))}}"> {{$row->userid}}</a></td>        
        
        <td>{{$row->batch_id}}</td>
        <td>{{$row->stat_desc}}</td>
        <td>{{$row->batch_type}}</td>        
        <td>{{date('m/d/Y',strtotime($row->created_date))}}</td>
      
      </tr>
      
      @endforeach
      
    </tbody>
  </table>
{{$pagination->links()}}

 <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        
                       
                         <input type="button" onclick="window.location.href='{{URL::route('sign1-report-batch-query')}}'" name="login-submit" id="submit" tabindex="4" value="Back" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>

 </form>
</div>
@stop
