<?php 
   //echo '<pre>';print_r($ad_begin_date_array);exit;
   ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>{{-- Select Ad Begin Date --}} </h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="row">
         <div class="col-md-8" style="margin-left:182px;">
            <h4> SELECT AD BEGIN DATE  <br>
               TO MAKE AD SIGNS FOR
            </h4>
            <br>  
         </div>
      </div>
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >{{-- SAFE COUNT --}}</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('sign-stockcode-N')}}"  id="booksafedate" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return validateForm()">
               <div class="row">
                  <div class="form-group">
                     <label style="padding-left:80px;padding-top:3px;" for="inputPassword" class="control-label col-sm-5">Ad Begin Date</label>
                     <div class="col-sm-7">
                        <select name="ad_begin_date" id="ad_begin_date" class="form-control">
                           <option value="">Ad Begin Date</option>
                           @for($i=0;$i<count($ad_begin_date_array);$i++)
                           <option value="{{ $ad_begin_date_array[$i]['date_requested'] }}">{{   date('m/d/Y',strtotime($ad_begin_date_array[$i]['date_requested'])); }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label style="padding-left:80px;padding-top:3px;" for="inputPassword" class="control-label col-sm-5">Select Department</label>
                     <div class="col-sm-7">
                        <select name="gl_dept_no" id="gl_dept_no" class="form-control">
                           <option value="">Departments</option>
                           @for($i=0;$i<count($gl_dept_array);$i++)
                           <option value="{{ $gl_dept_array[$i]['gl_dept_no'] }}^{{ $gl_dept_array[$i]['description'] }}">{{ $gl_dept_array[$i]['description'] }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('/home')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               </br>
            </form>
         </div>
      </div>
   </div>
</div>
@stop
<script type="text/javascript">
   function validateForm() {
    var ad_begin_date = document.getElementById("ad_begin_date").value;
    if (ad_begin_date == "") {
        alert("Ad Begin Date is required");
        document.getElementById("ad_begin_date").focus();
        return false;
    }
    var gl_dept_no = document.getElementById("gl_dept_no").value;
    if (gl_dept_no == "") {
        alert("Department is required");
        document.getElementById("gl_dept_no").focus();
        return false;
    }
}
</script>