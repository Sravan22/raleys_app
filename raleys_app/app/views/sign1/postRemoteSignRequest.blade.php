
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>REMOTE SIGN REQUESTS</h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div class="row">
      <div class="col-md-8" style="margin-left:182px;">
         <b>User Id:<?php echo $ws_userid;?></b> <br>
         <b>Date:<?php 
         $date = new DateTime($date_requested);
         echo $date->format('m/d/Y');
         ?></b>
      </div>
      <div id="loginbox" class="mainbox col-sm-12">
         <div style="padding-top:30px" class="panel-body" >
            <div class="table-responsive ">
               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th class="text-center">Stk</th>
                        <th class="text-center">Sign Type</th>
                        <th class="text-center">UPC</th>
                        <th class="text-center">Item Description</th>
                        <th class="text-center">Printed</th>
                     
                     </tr>
                  </thead>
                  <tbody>
                     @if($sign_cur)
                     @foreach($sign_cur as $row)
                     <tr>

                     <?php 
                     $params = array(
                        'seq_no'=> $row->seq_no,
                        'sign_id'=>$row->sign_id,
                        'stock_code'=>$row->stock_code
                        );
                     $queryString = http_build_query($params);
                     ?>
                        <td class="text-center"><u>
                        {{ HTML::link(URL::route('sign1-requestdisplay',$queryString), $row->stock_code) }}</u></td>
                        
                        <?php $sign_id = DB::select("SELECT sign_name FROM sssign WHERE sign_id = '{$row->sign_id}'");
                        $sign_id = json_decode(json_encode($sign_id), true);
                        ?>

                        <td class="text-center">{{ $sign_id[0]['sign_name'] }}</td>
                        <td class="text-center">{{ $row->upc_number }}</td>
                        <td class="text-center">{{ $row->description }}</td>
                        <td class="text-center">{{ $row->printed_sw }}</td>
                       
                     </tr>
                     @endforeach
                     @else
                     <tr>
                     <td colspan="6">
                        <div class="alert alert-danger" role="alert">
                           <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
                           THERE ARE NO AD ITEMS TO DISPLAY FOR THIS DEPARTMENT 
                        </div>
                        </td>
                     </tr>
                     @endif   
                  </tbody>
               </table>
               {{-- {{ $pagination->links() }} --}}
            </div>
         </div>
      </div>
   </div>
</div>
@stop