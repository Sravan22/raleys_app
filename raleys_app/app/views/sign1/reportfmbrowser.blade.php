@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('sign1.sign1reportfmmenu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    
</div>
   
     
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
        <style type="text/css">
   tr a {
   text-decoration: underline;
   }
</style>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Date / Time</th>
        <th> UPC / PLU / SKU</th>
        <th> Description</th>
        <th> Action </th>
        <th> Fld No</th>
        <th> FM Note</th>
    </tr>
    </thead>
    <tbody>
        
        @foreach($data as $row)
      <tr>
          <td><a href="{{URL::route('sign1-report-fm-view',array('userid'=>$row->userid,'seq_no'=>$row->seq_no))}}">{{date('m/d/Y',strtotime($row->update_date))}} {{$row->update_time}}</a></td>        
        
        <td>{{$row->item_code}}</td>
        <td>{{$row->item_desc}}</td>
        <td>{{$row->action_desc}}</td>
        <td>{{$row->mstr_fld_no}}</td>
        <td>{{$row->new_value}}</td>
      
      </tr>
      
      @endforeach
      
    </tbody>
  </table>
{{$pagination->links()}}

 <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        
                       
                         <input type="button" onclick="window.location.href='{{URL::route('sign1-report-fm-query')}}'" name="login-submit" id="submit" tabindex="4" value="Back" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>

 </form>
</div>
@stop
