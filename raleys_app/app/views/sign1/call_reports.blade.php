<?php 
   //echo '<pre>';print_r($stock_array);exit;
 ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>Slic System Reports</h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div class="row">
       
      
      <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
       
       <style type="text/css">
                tr a {
                    text-decoration: underline;
                }
            </style>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Report ID</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
      <tr>
       <td> <a href="{{URL::route('sign1-report-mod-query')}}">1</a> </td>
        <td>Modification Audit Report</td>
        </tr>
       
        <tr>
        <td><a href="{{URL::route('sign1-report-fm-query')}}">2 </td>
        <td>FM Notes Report</td>
    
      </tr>
       <tr>
        <td><a href="{{URL::route('sign1-report-batch-query')}}">3</a></td>
        <td>Batch Admin Menu</td>
        </tr>
      
    </tbody>
  </table>
      </form>
    
       
   </div>
 
    
    <div class="row" style="padding-top:10px;">

   
        <div class="col-sm-12" align="center">
           
              <input type="button" value="Back" class="btn"  onclick="window.location.href='{{URL::route('user-home')}}'" />
            
          
        </div>
     
</div>

</div>

@stop