<?php 
  //echo '<pre>';print_r($results);exit;
?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
<style type="text/css">
  .style{
    float:right;margin-right:-120px;
  }
</style>
@section('section')
<header class="row">
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>AD SIGNS for {{ $department_name }}</h3>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div class="row">
      <div class="col-md-8" style="margin-left:182px;">
         <strong>Sign Stock: {{ $stock_code }} - {{ $stock_name }}</strong> <br>
         <strong>Name: {{ $sign_name }}</strong><br>
        {{--  <strong>ID:<span class="sign_id"></span></strong> --}}
         <span class="sign_id hidden" data-param2=""></span>
         {{-- <input type="button" data-toggle="modal" data-target="#myModal" style="float:right;margin-right:-119px;" value="Stock" class="btn btn-primary"> --}}
         <?php 
                      $params = array(
                                'ad_begin_date' =>  $ad_begin_date,
                                'gl_dept_no' =>  $gl_dept_no
                                ); 
                      $queryString = http_build_query($params);
                    ?>
         <u>{{ HTML::link(URL::route('sstypsgn-dynamic-from',$queryString), 'Stock',array('class' => 'btn btn-primary style')) }}</u>
         {{-- <a href="#" class="btn btn-primary" >Stock</a> --}}
         <a href="#" class="btn btn-primary" style="float:right;margin-right:-190px;">Print</a>
         
      </div>
      <div id="loginbox" class="mainbox col-sm-12">
         <div style="padding-top:30px" class="panel-body" >
            <div class="table-responsive ">
               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th class="text-center">UPC</th>
                        <th class="text-center">Item Description</th>
                        <th class="text-center">Size</th>
                        <th class="text-center">Sale Price</th>
                        <th class="text-center">Regular Price</th>
                        <th class="text-center">Prt</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if($results)
                     @foreach($results as $row)
                     <tr>
                     <?php 
                      $params = array(
                                'seq_no' =>  $row->seq_no,
                                'sign_id' =>  $sign_id,
                                'stock_code' =>  $stock_code
                                ); 
                      $queryString = http_build_query($params);
                    ?>
                     <td class="text-center"><u>{{ HTML::link(URL::route('sign1-requestdisplay',$queryString), $row->upc_number) }}</u></td>
                      {{--  <td class="text-center"><u> <a href="javascript:void(0)" data-href="{{URL::route('sign1-requestdisplay',$queryString)}}" class="ancUrl updateprint" data-param1="{{ $row->seq_no }}">{{ $row->upc_number }}</a><u></td> --}}
                        {{-- <td class="text-center">{{ $row->upc_number }}</td> --}}
                        <td class="text-left" style="padding-left:100px;">{{ $row->description }}</td>
                        <td class="text-center">{{ $row->size }}</td>
                        <td class="text-center">{{ $row->sale_price }}</td>
                        <td class="text-center">{{ $row->regular_price }}</td>
                        <td class="text-center">{{ $row->printed_sw }}</td>
                     </tr>
                     @endforeach
                     @else
                     <tr>
                        <td colspan="6">
                           <div class="alert alert-danger" role="alert">
                              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
                              THERE ARE NO AD ITEMS TO DISPLAY FOR THIS DEPARTMENT 
                           </div>
                        </td>
                     </tr>
                     @endif   
                  </tbody>
               </table>
               {{-- {{ $pagination->links() }} --}}
                {{-- <input type="button" style="margin-left:45%;" onclick="history.go(-1);" name="cancel-submit" id="submit" tabindex="" value="Cancel" class="btn"> --}}
            </div>
         </div>
          
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    //location.reload();

   $("#stock_option").on('change',function(){
       
         $('#sign_option').find('option').remove().end().append('<option value="">Select</option>');
        //var aurl = 'sign_option';
         //alert(url);
        $.ajax({
          url: "sign_option",
           type: "POST", 
          data: { stock_option: $(this).val()},
          dataType:'json',
          // beforeSend: function () {
          //    // $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please wait till the Balance is transfered...</div>");
          // },
          success: function(data) {
            //alert(data);
            $.each(data, function(i, data) {
            $('#sign_option').append("<option value='" + data.sign_id +"^"+ data.sign_name + "'>" + data.sign_name + "</option>");
          });
          }
        });

        
   });

     $('.ancUrl').click(function(){
      //alert($(this).attr('data-param1'));return false;
      window.location.href=$(this).attr('data-href');
})

});
  $(document).one('ready',function(){
   if( window.localStorage )
  {
    if( !localStorage.getItem( 'firstLoad' ) )
    {
      localStorage[ 'firstLoad' ] = true;
      location.reload(true);
    }  
    else
      localStorage.removeItem( 'firstLoad' );
  }
   
});
</script>
 

@stop