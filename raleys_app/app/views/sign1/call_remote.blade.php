<?php 
   //echo '<pre>';print_r($stock_array);exit;
 ?>
@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
<header class="row">

<div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="invalertdanger alert-{{ $msg }}" role="alert">
      {{ Session::get('alert-' . $msg) }}  

   </div>
      
      @endif
    @endforeach
    </div>
  
   {{-- @include('sign1.menu') --}}
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h3>{{-- Remotely Requested Signs --}}</h3>
         </b>
      </div>
   </div>
</header>
<?php $previewsdate =  date('m/d/Y', strtotime('0 day')); ?>
<div class="container">
   
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
         <div class="row">
      <div class="col-md-8" style="margin-left:182px;">
        <h4> Date Requested : {{ $previewsdate }}</h4><br>
          
      </div>
   </div>
    
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Process Signs Requested by..</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('sign1-remotly-requested')}}"  id="s_remote" class="form-horizontal" method="post"  role="form" style="display: block;" onsubmit="return validateForm()">

            <input type="hidden" value="{{ $previewsdate }}" name ="Date_requested">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

            <div class="row">
               <div class="form-group">
                  <label style="padding-left:80px;padding-top:0px;" for="inputPassword" class="control-label col-sm-5">User ID</label>
                  <div class="col-sm-7">
                     <input type="text" name="ws_userid" id="ws_userid" autocomplete="off" isimportant="true" class="form-control" value="" autofocus="" tabindex="2" placeholder="Enter User Id" required=""> 
                     @if($errors->has('ws_userid'))
                     {{ $errors->first('ws_userid')}}
                     @endif       
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label style="padding-left:80px;padding-top:0px;" for="inputPassword" class="control-label col-sm-5">Password</label>
                  <div class="col-sm-7">
                     <input type="password" name="ws_passwd" id="ws_passwd" autocomplete="off" isimportant="true" class="form-control" value="" tabindex="3" placeholder="Enter User Password" required="">
                     @if($errors->has('ws_passwd'))
                     {{ $errors->first('ws_passwd')}}
                     @endif       
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label style="padding-left:80px;padding-top:0px;" for="inputPassword" class="control-label col-sm-5">Printed Yet</label>
                  <div class="col-sm-7">
                     <input type="text" name="ws_printed_sw" id="ws_printed_sw" 
                        maxlength="1" 
                     autocomplete="off" isimportant="true" class="form-control" value="N" tabindex="4" style="text-transform:uppercase" required="">
                     @if($errors->has('ws_printed_sw'))
                     {{ $errors->first('ws_printed_sw')}}
                     @endif       
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" style="margin-left: 50%">
                     <input type="submit" id="submit" tabindex="5" value="Submit" class="btn">
                     <input type="button" value="Cancel" id="cancel" class="btn" onClick="document.location.href='{{URL::to('/home')}}'" tabindex="6" />
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <div class="focusguard" id="focusguard-2" tabindex="7"></div>
            </form>
         </div>
      </div>
   </div>
 

</div>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>

<script>
  $('#s_remote').submit(function() {
    if ($.trim($("#ws_userid").val()) === "" || $.trim($("#ws_passwd").val()) === "") {
        alert('you did not fill out one of the fields');
        return false;
    }
});
jQuery(document).ready(function($) {
      
      $(".invalertdanger").fadeTo(2000, 500).slideUp(1500, function(){
      $(".invalertdanger").slideUp(500);
      });


      $('#focusguard-2').on('focus', function() {
      $('#ws_userid').focus();
      });

      $('#focusguard-1').on('focus', function() {
      $('#cancel').focus();

      });

      $('#ws_printed_sw').keyup(function() {
      var val = this.value.toUpperCase();

      if(val != "Y" && val != "N") {
      this.value = '';
      alert( "'Y' or 'N' is required. \n Please try again.");
      }
});
  
});
 
</script>
@stop