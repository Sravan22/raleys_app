@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('sign1.sign1reportbatchmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Batch Detail Screen<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="#" class="form-horizontal" method="post" role="form" style="display: block;">
                
                <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">UPC / Item Code</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="item_cd" placeholder="UPC / Item Code" name="item_cd" value="{{$data->item_cd}}" readonly>
                     @if($errors->has('item_cd'))
                     {{ $errors->first('item_cd')}}
                     @endif
                  </div>
               </div>
                
                
                 <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Field No</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="field_no" placeholder="Field No" name="field_no" value="{{$data->field_no}}" readonly>
                     @if($errors->has('field_no'))
                     {{ $errors->first('field_no')}}
                     @endif
                  </div>
               </div>


<div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Action</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="change_cd" placeholder="Action" name="change_cd" value="{{$data->change_cd}}" readonly>
                     @if($errors->has('change_cd'))
                     {{ $errors->first('change_cd')}}
                     @endif
                  </div>
               </div>      
                
                
                <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Active</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="active_sw" placeholder="Active" name="active_sw" value="{{$data->active_sw}}" readonly>
                     @if($errors->has('active_sw'))
                     {{ $errors->first('active_sw')}}
                     @endif
                  </div>
               </div>
                
                
                <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">Original Value</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="org_value" placeholder="Original Value" name="org_value" value="{{$data->org_value}}" readonly>
                     @if($errors->has('org_value'))
                     {{ $errors->first('org_value')}}
                     @endif
                  </div>
               </div>
                
                
                <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">New Value</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="new_value" placeholder="New Value" name="new_value" value="{{$data->new_value}}" readonly>
                     @if($errors->has('new_value'))
                     {{ $errors->first('new_value')}}
                     @endif
                  </div>
               </div>
                
                
                
            
                
                
                  <div class="form-group" style="padding-top: 20px;">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
              <input type="text" class="form-control" id="last_update_by" placeholder="Last Updated By" name="last_update_by" value="{{$data->last_update_by}}" readonly >
                     @if($errors->has('last_update_by'))
                     {{ $errors->first('last_update_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update" value="{{date('m/d/Y',strtotime($data->last_update))}}" readonly >
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>
                
             
               
              
              
               
              
               <div class="form-group" style="padding-top: 20px;">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 40%">
                       
                       <input type="button" name="summary-submit" id="submit" tabindex="4" value="Cancel" class="btn"  onclick="document.location.href='{{URL::route('sign1-report-batch-summary',array('userid'=>$data->usrid,'seq_no'=>$data->hdrseqn))}}'">
                       
                       
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop