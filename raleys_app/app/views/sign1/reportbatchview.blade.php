@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('sign1.sign1reportbatchmenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Batch Maintenance<FIELDSET></FIELDSET></div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('sign1-report-batch-update',array('userid'=>$data->usrid,'seq_no'=>$data->sqno))}}" class="form-horizontal" method="post" role="form" style="display: block;">
                 <input type="hidden" class="form-control" id="seq_no" name="seq_no" value="{{$data->sqno}}" >
                  <input type="hidden" class="form-control" id="status_cd_orig" name="status_cd_orig" value="{{$data->status_cd}}" >
                <div class="form-group" style="padding-top: 20px;">
                  <label for="inputPassword" class="control-label  col-sm-5">Store No</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="store_no" placeholder="Store No" name="store_no" value="{{$data->store_no}}" readonly>
                     @if($errors->has('store_no'))
                     {{ $errors->first('store_no')}}
                     @endif
                  </div>
               </div>
                
                <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Format</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="pos_type" placeholder="" name="pos_type" value="{{$data->str_format}}" readonly>
                     @if($errors->has('pos_type'))
                     {{ $errors->first('pos_type')}}
                     @endif
                  </div>
               </div>
                
            <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5" value="{{$data->str_format}}">User ID</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="userid" placeholder="User ID" name="userid" value="{{$data->userid}}" readonly>
                     @if($errors->has('userid'))
                     {{ $errors->first('userid')}}
                     @endif
                  </div>
               </div>
                
                
                <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Batch ID</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="batch_id" placeholder="Batch ID" name="batch_id" value="{{$data->batch_id}}"  >
                     @if($errors->has('batch_id'))
                     {{ $errors->first('batch_id')}}
                     @endif
                  </div>
               </div>

<div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Status</label>
                  <div class="col-sm-6">
                    <select class="form-control" id="status_cd" name="status_cd">
                        
                        <option value="P" <?php if($data->status_cd=='P') { ?>selected <?php } ?>>PENDING</option>
                        <option value="A" <?php if($data->status_cd=='A') { ?>selected <?php } ?>>APPLY</option>
                        <option value="D" <?php if($data->status_cd=='D') { ?>selected <?php } ?>>DELETE</option>
                        <option value="H" <?php if($data->status_cd=='H') { ?>selected <?php } ?>>HOLD</option>
                        <option value="R" <?php if($data->status_cd=='R') { ?>selected <?php } ?>>REVERSED</option>
                        <option value="B" <?php if($data->status_cd=='B') { ?>selected <?php } ?>>BACKOUT</option>
                        <option value="C" <?php if($data->status_cd=='C') { ?>selected <?php } ?>>COMPLETE</option>
                        <option value="E" <?php if($data->status_cd=='E') { ?>selected <?php } ?>>ERROR</option>
                    </select>
                     @if($errors->has('status_cd'))
                     {{ $errors->first('status_cd')}}
                     @endif
                  </div>
               </div>
                
                 <div class="form-group">
                  <label for="inputPassword" class="control-label  col-sm-5">Batch Type</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="batch_type" placeholder="Batch Type" name="batch_type"  value="{{$data->batch_type}}" readonly >
                     @if($errors->has('batch_type'))
                     {{ $errors->first('batch_type')}}
                     @endif
                  </div>
               </div>
   
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Create Date</label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control" id="created_date" placeholder="Date" name="created_date" value="{{date('m/d/Y H:i:s',strtotime($data->created_date))}}" readonly >
                     @if($errors->has('created_date'))
                     {{ $errors->first('created_date')}}
                     @endif
                  </div>
               </div>
                
                
                  <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated By</label>
                  <div class="col-sm-6">
              <input type="text" class="form-control" id="last_update_by" placeholder="Last Updated By" name="last_update_by" value="{{$data->last_update_by}}" readonly >
                     @if($errors->has('last_update_by'))
                     {{ $errors->first('last_update_by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-5">Last Updated</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="last_update" placeholder="Last Updated" name="last_update" value="{{date('m/d/Y',strtotime($data->last_update))}}" readonly >
                     @if($errors->has('last_update'))
                     {{ $errors->first('last_update')}}
                     @endif
                  </div>
               </div>
                
             
               
              
              
               
              
               <div class="form-group" style="padding-top: 20px;">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 8%">
                        <input type="submit" name="update-submit" id="submit" tabindex="4" value="Update" class="btn">
                       <input type="submit" name="change-submit" id="submit" tabindex="4" value="Changes Batch ID" class="btn">
                       
                       <input type="button" name="summary-submit" id="submit" tabindex="4" value="Batch Summary Screen" class="btn"  onclick="document.location.href='{{URL::route('sign1-report-batch-summary',array('userid'=>$data->usrid,'seq_no'=>$data->sqno))}}'">
                       
                       
                          <input type="button" id="" tabindex="4" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::route('sign1-report-batch-browse')}}'">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop