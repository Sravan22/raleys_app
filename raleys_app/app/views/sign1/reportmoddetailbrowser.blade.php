
@extends('layout.dashboard')
@section('page_heading','Maintenance Summary')
@section('content')
@section('section')

<header class="row">
    @include('sign1.sign1reportmodmenu')

</header>
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
 <div class="container">
   <div class="row">
      <div class="col-md-6" ></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('sign1-pdf-detail-reports',['download'=>'pdf'])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
<div class="container">
    <div class="menu_search_line">    
    </div>
    <div class="container" style="padding-top:10px;">
        <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
            <style type="text/css">
                tr a {
                    text-decoration: underline;
                }
            </style>
            
            <h3 class="text-center">Detail</h3>
            <table class="table table-striped">
                <thead>
                <th>Update Date</th>
                <th>Upadate Time</th>
                <th>User</th>
                <th>UPC / PLU / SKU</th>
                <th>Description</th>
                <th>Action</th>
                <th>Field Number</th>
                <th>Old</th>
                <th>New</th>
                <th>Maint Type</th>
                </thead>
                
                <tbody>
                    
                    @foreach($data->results as $row)
                    <tr>
                        <td>{{$row->update_date}}</td>
                        <td>{{$row->update_time}}</td>
                        <td>{{$row->userid}} {{ $row->user_name}}</td>
                        <td>{{$row->item_code}}</td>
                        <td>{{$row->item_desc}}</td>
                        <td>{{$row->action_desc}}</td>
                        
                        <td>{{$row->mstr_fld_no}} {{$row->mstr_fld_desc}}</td>
                        <td>@if($row->action_cd=='A' OR $row->action_cd=='F') {{$row->orig_value}} @endif</td>
                        <td>{{$row->new_value}}</td>
                        <td>@if($row->maint_type_cd == "B") {{'BATCH'}} @endif</td>
                        
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            
            
            <h3 class="text-center">Summary</h3>
            <table class="table table-striped">

                <tbody>
                    
                    <tr>
                        <td>Total Items Scanned: </td>
                        <td> {{$data->tot_scanned}}</td>
                    </tr>
                    <tr>
                        <td>Total Aisle Scanned:</td>
                        <td> {{$data->tot_aisle_scanned}}</td>
                    </tr>
                    <tr>
                        <td>Items Not Found: </td>
                        <td> {{$data->tot_notfound}} </td>

                    </tr>
                    <tr>
                        <td> Items Added:</td>
                        <td> {{$data->tot_add}}</td>

                    </tr>
                    <tr>
                        <td>  Items Deleted:</td>
                        <td> {{$data->tot_del}}</td>

                    </tr>
                    <tr>
                        <td>  Price Changes:</td>
                        <td>{{$data->tot_updprice}}</td>



                    </tr>
                    <tr>
                        <td>     Updated Items: </td>
                        <td>{{$data->tot_upd}}</td>

                    </tr>
                    <tr>
                        <td>   Total FM Notes: </td>
                        <td> {{$data->tot_fm_notes}}</td>

                    </tr>
                    <tr>
                        <td>  Total Signin's:</td>
                        <td> {{$data->tot_signin}}</td>

                    </tr>
                    <tr>
                        <td>  Total Signout's: </td>
                        <td> {{$data->tot_signout}}</td>

                    </tr>
                    <tr>
                        <td>  Total Unknown:</td>
                        <td> {{$data->tot_unknown}}</td>
                    </tr>

                </tbody>
            </table>
            <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
    <input type="button" class="btn" name="" onclick="history.go(-1);" value="Back">        
 
   {{ Form::token()}}
        </div>
        </div>
  </div>
        </form>
    </div>
    @stop
