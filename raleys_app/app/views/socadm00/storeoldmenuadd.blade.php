@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transfer Menu Manager</div>
         </div>
         <br>
         <div class="panel-body" >
            <form action="{{URL::route('socadm00-post-storeoldmenuadd')}}" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return validateForm()">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" maxlength="2" style="text-transform: uppercase" class="form-control" id="type_code" placeholder="Type Code" name="type_code">
                     @if($errors->has('type_code'))
                     {{ $errors->first('type_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Sub-Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" maxlength="2" style="text-transform: uppercase" class="form-control" id="subtype_code" placeholder="Sub-Type Code" name="subtype_code">
                     @if($errors->has('subtype_code'))
                     {{ $errors->first('subtype_code')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Menu Item Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="description" placeholder="Menu Item Description" name="description">
                     @if($errors->has('description'))
                     {{ $errors->first('description')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Position Number on Menu</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="menu_position" placeholder="Position Number on Menu" name="menu_position" onKeyPress="return StopNonNumeric(this,event)">
                     @if($errors->has('menu_position'))
                     {{ $errors->first('menu_position')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Goes to  Program</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="goto_pgm_id" placeholder="Goes to  Program" name="goto_pgm_id">
                     @if($errors->has('goto_pgm_id'))
                     {{ $errors->first('goto_pgm_id')}}
                     @endif
                  </div>
               </div>
               
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Active Switch</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="active_switch" style="text-transform: uppercase" placeholder="Active Switch" name="active_switch" {{ (Input::old('active_switch'))?' 
                              value="'.Input::old('active_switch').'"':''}} value="Y">
                     @if($errors->has('active_switch'))
                     {{ $errors->first('active_switch')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated:</label>
                  <div class="col-sm-6">
                     <input type="date" readonly="" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Add" class="btn">
                        <input type="reset" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
    ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   text-transform: initial;
   }

</style>
@stop


<script type="text/javascript">
   function validateForm()
   {
      var str = document.getElementById("active_switch").value;
      var active_switch = str.toUpperCase();
      if(!(active_switch == 'Y' || active_switch == 'N'))
      {
         alert('Active Switch must be  Y  or  N');
         $('#active_switch').val('');$('#active_switch').focus();return false;
      }
   }
   function StopNonNumeric(el, evt)
   {
       //var r=e.which?e.which:event.keyCode;
       //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
       var charCode = (evt.which) ? evt.which : event.keyCode;
       var number = el.value.split('.');
       if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
           return false;
       }
       //just one dot (thanks ddlab)
       if(number.length>1 && charCode == 46){
            return false;
       }
       //get the carat position
       var dotPos = el.value.indexOf(".");
       if( dotPos>-1 && (number[1].length > 3)){
           return false;
       }
       return true;
   }

</script>
