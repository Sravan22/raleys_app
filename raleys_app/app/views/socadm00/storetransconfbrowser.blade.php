<?php 
  //echo '<pre>';print_r($confbrowser_array);exit;
?>
<?php 
  //echo $type_code;exit;
?>
@extends('layout.dashboard')
@section('page_heading',"Store Transmission Configuration Manager")
@section('content')
@section('section')

<header class="row">
       @include('socadm00.socadm00menu')    
    </header>
<div class="container">

<div class="container" style="padding-top:10px;">
<?php 
    $params = array(
              'download' =>  'pdf',
              'type_code' => $report_data['type_code'],
              'subtype_code' => $report_data['subtype_code'],
              'description' => $report_data['description'],
              'partition_code' => $report_data['partition_code'],
              'dept_number' => $report_data['dept_number'],
              'max_item_length' => $report_data['max_item_length'],
              'max_qty' => $report_data['max_qty'],
              'neg_qty_flag' => $report_data['neg_qty_flag'],
              'zero_qty_flag' => $report_data['zero_qty_flag'],
              'return_flag' => $report_data['return_flag'],
              'history_flag' => $report_data['history_flag'],
              'vendor_acct_no' => $report_data['vendor_acct_no'],
              'active_switch'  => $report_data['active_switch']
              ); 
              $queryString = http_build_query($params);
            ?>
  <div class="col-md-12 text-right">
            <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-storetransconf-query',$queryString) }}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
        </div>
   @if ($data)
<table class="table table-striped">

    <thead>
    <tr>
        <th>Active Switch</th>
        <th>Type Code</th>
        <th>SubType Code</th>
        <th>Description</th>
        <th>Partition Code</th>
        <th>Dep Number</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($data as $confbrowser_array)  
      <tr>
      <?php 
              $params = array(
                        'entry_number' =>  $confbrowser_array->entry_number
                        ); 
              $queryString = http_build_query($params);
            ?>
        
        <td>{{ $confbrowser_array->active_switch }}</td>
        {{-- <td>{{ $confbrowser_array[$i]['type_code'] }}</td> --}}
        <td><u>{{ HTML::link(URL::route('socadm00-config-update',$queryString), $confbrowser_array->type_code) }}</u></td>
        <td>{{ $confbrowser_array->subtype_code }}</td>
        <td>{{ $confbrowser_array->description }}</td>
        <td>{{ $confbrowser_array->partition_code }}</td>
        <td>{{ $confbrowser_array->dept_number }}</td>
        
      </tr>
        @endforeach
    </tbody>
  </table>
  {{$pagination->links()}}
@else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Store Transfers meet Query criteria.
            </div>
        @endif

</div>
@stop


<style type="text/css">
  

@media print {
  a[href]:after {
    content: "" !important; 
    
  }
}
</style>