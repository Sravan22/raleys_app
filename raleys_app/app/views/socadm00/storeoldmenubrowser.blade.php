<?php
  //echo '<pre>';print_r($somenu_query_result_array);exit;
?>
@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('socadm00.socadm00menu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
  @if ($data)
<table class="table table-striped">
    <thead>
    <tr>
        <th>Active Switch</th>
        <th>Type Code</th>
        <th>Sub-Type Code</th>
        <th>Menu Item Description</th>
        <th>Goto Pgm ID</th>
        
        </tr>
    </thead>
    <tbody>
    @foreach ($data as $somenu_query_result_array)
      <tr>
      <?php 
              $params = array(
                        'entry_number' =>  $somenu_query_result_array->entry_number
                        ); 
              $queryString = http_build_query($params);
            ?>
        <td>{{ $somenu_query_result_array->active_switch }}</td>
         <td><u>{{ HTML::link(URL::route('socadm00-storeoldmenu-update',$queryString), $somenu_query_result_array->type_code) }}</u></td>
        {{-- <td>{{ $somenu_query_result_array[$i]['type_code'] }}</td> --}}
        <td>{{ $somenu_query_result_array->subtype_code }}</td>
        <td>{{ $somenu_query_result_array->description }}</td>
        <td>{{ $somenu_query_result_array->goto_pgm_id }}</td>
       
        
      </tr>
      @endforeach
    </tbody>
  </table>
  {{$pagination->links()}}
@else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Store Transfers meet Query criteria.
            </div>
        @endif
 </form>
</div>
@stop
