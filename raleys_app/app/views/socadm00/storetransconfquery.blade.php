@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transmissions Configuration Manager</div>
         </div>
         <br>
         <div class="panel-body" >
            <form action="{{URL::route('socadm00-post-storetransconfquery')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" style="text-transform: uppercase" class="form-control" id="type_code" placeholder="Type Code" name="type_code" minlength="1" maxlength="3">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Sub-Type Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" style="text-transform: uppercase" class="form-control" id="subtype_code" placeholder="Sub-Type Code" name="subtype_code">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Banner Description</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="description" placeholder="Banner Description" name="description">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Partition Code</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" style="text-transform: uppercase" class="form-control" id="partition_code" placeholder="Partition Code" name="partition_code">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Department Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="dept_number" placeholder="Department Number" name="dept_number">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Maximum Item Length</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="max_item_length" placeholder="Maximum Item Length" name="max_item_length">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Maximum Quantity</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="max_qty" placeholder="Maximum Quantity" name="max_qty">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Special Process Flag</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" style="text-transform: uppercase" class="form-control" id="neg_qty_flag" placeholder="Special Process Flag" name="neg_qty_flag">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Zero Qty Flag</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" style="text-transform: uppercase" class="form-control" id="zero_qty_flag" placeholder="Zero Qty Flag" name="zero_qty_flag">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Return Flag</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" style="text-transform: uppercase" class="form-control" id="return_flag" placeholder="Return Flag" name="return_flag">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">History FLag</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" style="text-transform: uppercase" class="form-control" id="history_flag" placeholder="History FLag" name="history_flag">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Vendor Account Number</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" id="vendor_acct_no" placeholder="Vendor Account Number" name="vendor_acct_no">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Active Switch</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" style="text-transform: uppercase" class="form-control" id="active_switch" placeholder="Active Switch" name="active_switch">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated:</label>
                  <div class="col-sm-6">
                     <input type="date" readonly="" class="form-control" id="" placeholder="" name="">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('socadm00/storetransmission')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
     ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   te
</style>
@stop