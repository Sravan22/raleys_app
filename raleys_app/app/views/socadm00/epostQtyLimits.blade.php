@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">Order Quantity Warning Limits</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('socadm00-postqtylimits_update')}}" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return validateForm()">
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Description</label>
                  </div>
                  <input type="hidden" name="store_number" value="{{{ Session::get('storeid') }}}">
                  <div class="col-sm-7">
                     <input type="text" readonly="" class="form-control" name="" value="{{ $department_name_array[0]['name'] }}">
                     
                  </div>
               </div>
               <input type="hidden" name="entry_number" value="{{ $current_limit_array[0]['entry_number'] }}">
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Current Limit</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" readonly="" class="form-control" name="" value="{{ $current_limit_array[0]['max_qty'] }}">
                     
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">New Limit</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="number" class="form-control" name="new_limit" id="new_limit">
                     
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class=""></div>
@stop
<script type="text/javascript">
   function validateForm()
   {
      //alert('validateForm');   
      //var new_limit = document.getElementById('new_limit');
      var new_limit = document.getElementById("new_limit").value;
     if(new_limit < 1)
      {
         alert('Limit Must Be Greater Than Zero');return false;
      }
      else if(new_limit > 999)
      {
         alert('Limit Cannot Be More than 999');return false;
      }
      // else
      // {
      //      alert('Proper Data');return false;
      // }
   }
   
</script>