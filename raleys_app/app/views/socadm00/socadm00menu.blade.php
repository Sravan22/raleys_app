<div class="container" >
  <div class="menu_search_line" >
    <ul class="navbar dib_float_left" >
     <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                  Menus <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{ URL::route('socadm00-storetransmission-query')}}">Query</a></li>
          {{-- <li><a href="{{ URL::route('socadm00-storetransmission-browser')}}">Browse</a></li> --}}
         
         
          <li><a href="{{ URL::route('socadm00-storetransmission-add')}}">Add</a></li>
         {{--  <li><a href="{{ URL::route('socadm00-storetransmission-updatemsg')}}">Update</a></li> --}}
         
            </ul>
        </div>  
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                  Configurations <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{ URL::route('socadm00-storetransconf-query')}}">Query</a></li>
          {{-- <li><a href="{{ URL::route('socadm00-storetransconf-browsermsg')}}">Browse</a></li> --}}
         
         
          <li><a href="{{ URL::route('socadm00-storetransconf-add')}}">Add</a></li>
          {{-- <li><a href="{{ URL::route('socadm00-storetransconf-update')}}">Update</a></li> --}}
         
            </ul>
        </div>  
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                  Functions <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{ URL::route('socadm00-storetransfun-query')}}">Query</a></li>
          {{-- <li><a href="{{ URL::route('socadm00-storetransfun-browsermsg')}}">Browse</a></li> --}}
         
         
          <li><a href="{{ URL::route('socadm00-storetransfun-add')}}">Add</a></li>
          {{-- <li><a href="{{ URL::route('socadm00-storetransfun-updatemsg')}}">Update</a></li> --}}
         
            </ul>
        </div>  
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                  Dex_commids<span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{ URL::route('socadm00-storeDex-commids-query')}}">Query</a></li>
          {{-- <li><a href="{{ URL::route('socadm00-storeDex-commids-browsermsg')}}">Browse</a></li>
          <li><a href="{{ URL::route('socadm00-storeDex-commids-updatemsg')}}">Update</a></li> --}}
         
            </ul>
        </div> 

         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                  Schedules <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{ URL::route('socadm00-schedule-query')}}">Query</a></li>
          {{-- <li><a href="{{ URL::route('socadm00-schedule-browsermsg')}}">Browse</a></li> --}}
         
         
          <li><a href="{{ URL::route('socadm00-schedule-add')}}">Add</a></li>
          {{-- <li><a href="{{ URL::route('socadm00-schedule-updatemsg')}}">Update</a></li> --}}
         
            </ul>
        </div>  


        <div class="dropdown sub">
            <a id="dLabel" href="{{ URL::route('socadm00-Qtylimits')}}" role="button" 
               class="menu_choice" data-target="#">
              Qty_limits
            </a>
         </div>

 

{{-- 
         <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                  Old_menus<span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="{{ URL::route('socadm00-storeold-menu-query')}}">Query</a></li>
          <li><a href="{{ URL::route('socadm00-storeold-menu-add')}}">Add</a></li>
         
         
            </ul>
        </div>  --}} 

        <div class="dropdown sub">
            <a id="dLabel" href="{{ URL::route('user-home')}}" role="button" 
               class="menu_choice" data-target="#">
                Exit
            </a>
        </div>