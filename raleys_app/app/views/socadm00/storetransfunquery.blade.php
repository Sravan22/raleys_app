@extends('layout.dashboard')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
   <div class="col-md-12">
   
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Transmissions Menu Manager</div>
         </div>
         <br>
         <div class="panel-body" >
            <form action="{{URL::route('socadm00-post-storetransfunquery')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Function Code</label>
                  <div class="col-sm-6">
                     <input type="text" minlength="2" maxlength="2" style="text-transform: uppercase" class="form-control" id="function_code" placeholder="Function Code" name="function_code">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Description</label>
                  <div class="col-sm-6">
                     <input type="text" maxlength="40" class="form-control" id="description" placeholder="Description" name="description">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Active Switch</label>
                  <div class="col-sm-6">
                     <input type="text" maxlength="1" class="form-control" style="text-transform: uppercase" id="active_switch" placeholder="Active Switch" name="active_switch">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated By</label>
                  <div class="col-sm-6">
                     <input type="text" readonly="" class="form-control" id="last_updated_by" placeholder="" name="last_updated_by">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="" class="control-label col-sm-6">Last Updated:</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control" id="last_update" readonly="" placeholder="" name="last_update">
                     @if($errors->has(''))
                     {{ $errors->first('')}}
                     @endif
                  </div>
               </div>

               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('socadm00/storetransmission')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
   ::-webkit-input-placeholder {
   text-transform: initial;
}

:-moz-placeholder { 
   text-transform: initial;
}

::-moz-placeholder {  
   text-transform: initial;
}

:-ms-input-placeholder { 
   text-transform: initial;
}

</style>
@stop