@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')

<header class="row">
       @include('socadm00.socadm00menu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    
</div>
<div class="container" style="padding-top:10px;">

  @if ($data)
<table class="table table-striped">
    <thead>
    <tr>
        <th> GL </th>
        <th> Order Type </th>
        <th> Vender Number </th>
        <th> Description </th>
        <th> Sun </th>
        <th> Mon </th>
        <th> Tue </th>
        <th> Wed </th>
        <th> Thr </th>
        <th> Fri </th>
        <th> Sat </th>
                
        </tr>
    </thead>
    <tbody>
    @foreach ($data as $sovndschd_result_array)
      <tr>
        <?php 
              $params = array(
                        'id' =>  $sovndschd_result_array->id 
                        ); 
              $queryString = http_build_query($params);
            ?>
        <td>{{ $sovndschd_result_array->gl_dept_number }}</td>
        <td>{{ $sovndschd_result_array->type_code }}</td>
         <td><u>{{ HTML::link(URL::route('socadm00-schedulebrowse-update',$queryString), $sovndschd_result_array->vendor_number) }}</u></td>
       {{--  <td>{{ $sovndschd_result_array[$i]['vendor_number'] }}</td> --}}
        <td>{{ $sovndschd_result_array->description }}</td>
        <td>{{ $sovndschd_result_array->sun_sw }}</td>
        <td>{{ $sovndschd_result_array->mon_sw }}</td>
        <td>{{ $sovndschd_result_array->tue_sw }}</td>
        <td>{{ $sovndschd_result_array->wed_sw }}</td>
        <td>{{ $sovndschd_result_array->thr_sw }}</td>
        <td>{{ $sovndschd_result_array->fri_sw }}</td>
        <td>{{ $sovndschd_result_array->sat_sw }}</td>
      </tr>
      @endforeach
      
    </tbody>
  </table>
  {{$pagination->links()}}
@else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Store Schedules meet Query criteria.
            </div>
        @endif

</div>
@stop
  