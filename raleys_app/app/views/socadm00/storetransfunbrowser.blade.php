@extends('layout.dashboard')
@section('page_heading')
@section('content')
@section('section')
<header class="row">
   @include('socadm00.socadm00menu')
</header>
<div class="container">
<div class="menu_search_line">    </div>
<div class="container" style="padding-top:10px;">
   @if ($data)
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Function Code</th>
            <th>Description</th>
            <th>Active Switch</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($data as $sofnctns_array)  
         <tr>
           <?php 
              $params = array(
                        'entry_number' =>  $sofnctns_array->entry_number
                        ); 
              $queryString = http_build_query($params);
            ?>
            <td><u>{{ HTML::link(URL::route('socadm00-storetransfunc-update',$queryString), $sofnctns_array->function_code) }}</u></td>
            {{-- <td>{{ $sofnctns_array[$i]['function_code'] }}</td> --}}
            <td>{{ $sofnctns_array->description }}</td>
            <td>{{ $sofnctns_array->active_switch }}</td>
         </tr>
         @endforeach
      </tbody>
   </table>
    {{$pagination->links()}}
   @else
   <div class="alert alert-danger">
      <strong>Alert!</strong> No Menu Options meet Query criteria.
   </div>
   @endif
</div>
@stop