@extends('layout.dashboard')

@section('page_heading','Receivings')
@section('content')
@section('section')

<header class="row">
        @include('menu.recivermenu')
    </header>
<div class="container">
  <!-- <p id="menulist">
      <a href="{{ URL::route('menu-receivings')}}" >Receivings</a> >> 
      <a href="{{ URL::route('menu-receivings-query')}}" >Query</a>
  </p> -->
   <form class="form-horizontal" role="form" method="get" action="#">
<!--  <div class="col-sm-6"> -->
<div class="row">
    <div class="col-lg-6">
       
<div class="form-group">
  <label for="inputEmail3" class="col-sm-4 control-label">Store</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="store" placeholder="Store ID" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Department</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="department" placeholder="Department" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Invoice Number</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="invoice_number" placeholder="Invoice Number" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Invoice Date</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="invoice_date" placeholder="Invoice Date" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Invoice Type</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="invoice_type" placeholder="Invoice Type" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Status</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="status" placeholder="Status" />
  </div>
</div>
 
<!-- <div class="form-group">
  <div class="col-sm-offset-2 col-sm-8">
    <button type="submit" class="btn btn-default">Sign in</button>
  </div>
</div> 
</form>-->
   <!--  </div> -->
</div>


<div class="col-lg-6">
       
<div class="form-group">
  <label for="inputEmail3" class="col-sm-4 control-label">Vendor</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="vendor" placeholder="MONDELEZ GLOBAL LLC" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Number</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="number" placeholder="Number" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Transporter</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="transporter" placeholder="Transporter" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Number</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="number_2" placeholder="Number" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Cost Discrepancies</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="cost_discrepancies" placeholder="Cost Discrepancies" />
  </div>
</div>
<div class="form-group">
  <label for="#" class="col-sm-4 control-label">Method Received</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="method_received" placeholder="Method Received" />
  </div>
</div>

</div>
 
 <div class="col-lg-12">
   <div class="form-group">
  <label for="#" class="col-sm-4 control-label"></label>
  <label for="#" class="col-sm-4 control-label">Vendor</label>
  <label for="#" class="col-sm-4 control-label">Store</label>
  </div>
  <div class="form-group">
  <label for="#" class="col-sm-2 control-label">Qty</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="qty_vendor" placeholder="0.00" />
  </div>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="qty_store" placeholder="0.00" />
  </div>
  </div>

  <div class="form-group">
  <label for="#" class="col-sm-2 control-label">Store Inventory</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="store_inventory_vendor" placeholder="$0.00" />
  </div>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="store_inventory_store" placeholder="$0.00" />
  </div>
  </div>

  <div class="form-group">
  <label for="#" class="col-sm-2 control-label"></label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="store_inventory_vendor1" placeholder="+" />
  </div>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="store_inventory_store1" placeholder="$0.00 (CRV) " />
  </div>
  </div> 

  <div class="form-group">
  <label for="#" class="col-sm-2 control-label"></label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="store_inventory_vendor2" placeholder="+" />
  </div>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="store_inventory_store2" placeholder="$0.00 (Allowances) " />
  </div>
  </div> 

  <div class="form-group">
  <label for="#" class="col-sm-2 control-label">Selling Supplies</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="selling_supplies_vendor" placeholder="$0.00 +" />
  </div>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="selling_supplies_store" placeholder="$0.00" />
  </div>
  </div>

  <div class="form-group">
  <label for="#" class="col-sm-2 control-label">Store Supplies</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="store_supplies_vendor" placeholder="$0.00 +" />
  </div>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="store_supplies_store" placeholder="$0.00" />
  </div>
  </div>

  <div class="form-group">
  <label for="#" class="col-sm-2 control-label">Net</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="net_vendor" placeholder="$0.00" />
  </div>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="net_store" placeholder="$0.00" />
  </div>
  </div>


  <div class="form-group">
  <label for="#" class="col-sm-2 control-label">Created Date</label>
  <div class="col-sm-2">
    <input type="text" class="form-control" id="created_date" placeholder="Created Date" />
  </div>
  <label for="#" class="col-sm-2 control-label">Updated Date</label>
  <div class="col-sm-2">
    <input type="text" class="form-control" id="updated_date" placeholder="Updated Date" />
  </div>
  </div>

  <div class="form-group">
  <label for="#" class="col-sm-2 control-label">By</label>
  <div class="col-sm-2">
    <input type="text" class="form-control" id="by" placeholder="DSDMAIN" />
  </div>

  </div>
  
 </div>
 <div class="col-lg-12">
 <div class="form-group">
  <div class="col-sm-offset-8 col-sm-8">
    <button type="submit" class="btn btn-default">Submit</button>
  </div>
</div> 
</div>
</div>
</form>
@stop
