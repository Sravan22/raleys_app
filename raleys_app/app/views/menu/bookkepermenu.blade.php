<div class="container">

<div class="navbar navbar-inverse top_menu_background_color top_menu_min_height">    
             <div class="container">
            <div class="navbar-header"> 
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_collapse2" id="nav_btn">
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>   
                             <span class="icon-bar"></span>   
              </button>    
            </div>  
            <div class="navbar-collapse collapse" id="navbar_collapse2" style="height: 1px;">  
              <ul class="navbar dib_float_left">
    <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">Reg<span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Begin-Loans</a>
                <ul class="dropdown-menu">
                   <li><a href="{{URL::route('menu-bookkeeperregister')}}">Register</a></li>
                   <li><a href="{{URL::route('menu-bookkeeperregistertotal')}}">Totals</a></li>
                   <li><a href="{{URL::route('menu-bookkeeperregistercmt')}}">Comments</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Loans-to-Reg</a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::route('menu-bookloanrregister')}}">Register</a></li>
                    <li><a href="{{URL::route('menu-bookloanrregtotal')}}">Totals</a></li>
                    <li><a href="{{URL::route('menu-bookloanrregcmt')}}">Comments</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Reg-Check-Out</a>
                <ul class="dropdown-menu">
                  <li><a href="#">Item</a></li>
                  <li><a href="#">Register</a></li>
                  <li><a href="#">Total</a></li>
                  <li><a href="#">Print</a></li>
                  <li><a href="#">Comment</a></li>
                </ul>
              </li>
                <li><a href="#">Comment</a></li>
            </ul>
        </div>
        
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Safe <span class="caret mainheadlink"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Total-Safe-Count</a>
                <ul class="dropdown-menu">
                   <li><a href="#">Detail</a></li>
                   <li><a href="#">Print</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Safe-Report</a>
                <ul class="dropdown-menu">
                 <li><a href="#">Safe-Report</a></li>
                 <li><a href="#">Print</a></li>
                 <li><a href="#">Comments</a></li>
                </ul>
               </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Reg-Check-Out</a>
                <ul class="dropdown-menu">
                    <li><a href="#">Item</a></li>
                    <li><a href="#">Register</a></li>
                    <li><a href="#">Total</a></li>
                    <li><a href="#">Print</a></li>
                    <li><a href="#">Comment</a></li>
                </ul>
              </li>
                 <li><a href="#">Comment</a></li>
            </ul>
        </div>
         
        
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Wkly-recap <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="#">Sales</a></li>
              <li><a href="#">Recap</a></li>
              <li><a href="#">View</a></li>
              <li><a href="#">Print</a></li>
              <li><a href="#">Unlock</a></li>
              <li><a href="#">Comments</a></li>
        </ul>
        </div>
     <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Inv <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Postage</a>
                <ul class="dropdown-menu">
                  <li><a href="#">Detail</a></li>
                  <li><a href="#">Print</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Lottery</a>
                <ul class="dropdown-menu">
                 <li><a href="#">Add</a></li>
                 <li><a href="#">Totals</a></li>
                 <li><a href="#">Print</a></li>
                 <li><a href="#">View</a></li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Commuter</a>
              <ul class="dropdown-menu">
                <li><a href="#">Add</a></li>
                <li><a href="#">Print</a></li>
              </ul>
              </li>
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Mthly-Commuter</a>
                <ul class="dropdown-menu">
                 <li><a href="#">Add</a></li>
                 <li><a href="#">Print</a></li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">pHone</a>
                <ul class="dropdown-menu">
                  <li><a href="#">Add</a></li>
                  <li><a href="#">Print</a></li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">epS</a>
                <ul class="dropdown-menu">
                   <li><a href="#">Add</a></li>
                   <li><a href="#">Print</a></li>
                </ul>
              </li>
                <li><a href="#">Comment</a></li>
            </ul>
        </div>
    

    
       <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Fuel <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             <li><a href="#">N/A for store 305</a></li>
            </ul>
        </div>
        
     <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Mgmt <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             <li><a href="#">Daily</a></li>
          <li><a href="#">Weekly</a></li>
          <li><a href="#">Comments</a></li>
            </ul>
        </div>
    
    <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                cHg-regs <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="#">Query</a></li>
          <li><a href="#">Browse</a></li>
          <li><a href="#">Next</a></li>
          <li><a href="#">Previous</a></li>
          <li><a href="#">Add</a></li>
          <li><a href="#">Update</a></li>
          <li><a href="#">Delete</a></li>
            </ul>
        </div>
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Utility <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
              <li><a href="#">Utility Payment Count</a></li>
            </ul>
        </div>
        
        <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Comments <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="#">Comment List</a></li>
            </ul>
        </div>

    

 <div class="dropdown sub">
            <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
                Others <span class="caret"></span>
            </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
               <li><a href="#">N/A Supervisor only</a></li>
            </ul>
        </div>

    </ul>            
            </div>                         
          </div>   
     </div>
</div>            