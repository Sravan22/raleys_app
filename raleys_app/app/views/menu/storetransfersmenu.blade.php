<div class="container">
  <div class="menu_search_line">
    <ul class="navbar dib_float_left">
    
    <li class="menu_choice active">
        <a id="seShowEnrollmentModal" href="{{ URL::route('forms-storetransfersquery')}}">Query</a>
    </li>

    <li class="menu_choice">
        <a id="seShowEnrollmentModal" href="{{ URL::route('forms-storetransfersbrowse')}}">Browse</a>
    </li>
    
    <li class="menu_choice">
        <a id="seShowEnrollmentModal" href="{{ URL::route('forms-storetransfersupdate')}}">Update</a>
    </li>

    <li class="menu_choice">
        <a id="seShowEnrollmentModal" href="{{ URL::route('forms-storetransfersitems')}}">Items</a>
    </li>

    <li class="menu_choice">
        <a id="seShowEnrollmentModal" href="#">Print</a>
    </li>

    <li class="dropdown menu_choice">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reports<span class="caret"></span></a>
        <ul id="menu4" class="dropdown-menu">
          <li><a href="{{ URL::route('menu-receivings-sequery')}}">3 Reports</a></li>
        </ul>
    </li>

    <li class="menu_choice">
        <a id="seShowEnrollmentModal" href="{{ URL::route('forms-storetransfersoperators')}}">Operators</a>
    </li>

    <li class="menu_choice">
        <a id="seShowEnrollmentModal" href="#">Exit</a>
    </li>
    </ul>
</div>
</div>  