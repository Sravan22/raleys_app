<div class="container">
  <div class="menu_search_line">
    <ul class="navbar dib_float_left">
    <li id="main-home-link" class="menu_choice home_menu_choice active">
        Printer Selection Menu
    </li>
    <li class="menu_choice active">
        <a id="seShowEnrollmentModal" href="{{ URL::route('menu-receivings-query')}}">Query</a>
    </li>
    <li class="menu_choice"><a id="seShowEnrollmentModal" href="{{ URL::route('menu-receivings-browse')}}">Browse</a></li>
    <li class="menu_choice"><a id="seShowEnrollmentModal" href="{{ URL::route('menu-receivings-update')}}">Update</a></li> 
    <li class="menu_choice"><a id="seShowEnrollmentModal" href="{{ URL::route('menu-receivings-items')}}">Items</a></li>
    <li class="menu_choice"><a id="seShowEnrollmentModal" href="{{ URL::route('menu-receivings-receivingsofflinereceiving')}}">Offline Receiving</a></li>

      
    <li class="dropdown menu_choice">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Store Expenses<span class="caret"></span></a>
        <ul id="menu4" class="dropdown-menu">
          <li><a href="{{ URL::route('menu-receivings-sequery')}}">Query</a></li>
          <li><a href="{{ URL::route('menu-receivings-sebrowse')}}">Browse</a></li>
          <li><a href="{{ URL::route('menu-receivings-receivingsseupdate')}}">Update</a></li> 
          <li><a href="{{ URL::route('menu-receivings-receivingslogexpense')}}">Log Expense</a></li>
          <li><a href="%">Reports</a></li>
          <li><a href="%">Exit</a></li>
        </ul>
    </li>


    <li class="dropdown menu_choice">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reports<span class="caret"></span></a>
        <ul id="menu4" class="dropdown-menu">
          <li><a href="{{ URL::route('menu-report-weeklyreport')}}">Weekly Receiving Report</a></li>
          <li><a href="#">Offline Report</a></li>
          <li><a href="#">Store Invoice</a></li>
          <li><a href="%">Cost Discrepancy Report</a></li>
          <li><a href="%">Transporter Listing</a></li>
        </ul>
    </li>


    <li class="dropdown menu_choice">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Dex Log<span class="caret"></span></a>
        <ul id="menu4" class="dropdown-menu">
          <li><a href="{{ URL::route('menu-DEX-invoicedate')}}">Invoice Date</a></li>
          <li><a href="#">Vendor</a></li>
          <li><a href="#">Update</a></li>
          <li><a href="%">Browse</a></li>
          <li><a href="%">Exit</a></li>
        </ul>
    </li>
    <li class="menu_choice"><a id="seShowEnrollmentModal" href="#">Exit</a></li>
    </ul>
</div>
</div>  