<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title>Raley's</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
    <title>Raley's</title>
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/custom.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/ralyesmenu.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/subsubmenu.css") }}" />
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/select2.min.css") }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset("assets/stylesheets/right_label.css") }}"/>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset("assets/scripts/generalfunctions.js") }}"></script>


	
</head>
<body>
	@yield('body')
	



<script src="{{ asset("assets/scripts/frontend.js") }}" type="text/javascript"></script> 

<script src="{{ asset("assets/scripts/jquery.hotkeys-0.7.9.min.js") }}" type="text/javascript"></script>
<script src="{{ asset("assets/scripts/select2.full.min.js") }}" type="text/javascript"></script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>  
	@yield('jssection')
    @include('layout.shortcutkeys')
        
</body>
</html>