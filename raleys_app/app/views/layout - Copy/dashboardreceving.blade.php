<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8"/>
  <title>Raley's Receivings</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta content="" name="description"/>
  <meta content="" name="author"/>
  <title>Raley's</title>
  <link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
  <link rel="stylesheet" href="{{ asset("assets/stylesheets/custom.css") }}" />
  <link rel="stylesheet" href="{{ asset("assets/stylesheets/ralyesmenu.css") }}" />
  <link rel="stylesheet" href="{{ asset("assets/stylesheets/subsubmenu.css") }}" />
  <link rel="stylesheet" href="{{ asset("assets/stylesheets/select2.min.css") }}" />
</head>
<body>
  @yield('body')
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  
  <script type="text/javascript" src="{{ asset("assets/scripts/generalfunctions.js") }}"></script>
  <script src="{{ asset("assets/scripts/frontend.js") }}" type="text/javascript"></script>
  <script src="{{ asset("assets/scripts/jquery.hotkeys-0.7.9.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("assets/scripts/select2.full.min.js") }}" type="text/javascript"></script>
  @yield('jssection')
  @include('layout.shortcutkeys')
</body>
</html>
@section('body')
<div class="container-fluid">
<div class="row">
<div class="col-md-6">
  <a class="custom-input-width pull-left" href="{{ URL::route('user-home') }}">
    <img src="{{ asset("assets/images/raleys_logo.jpg") }}" width="100">
  </a>
</div>
<div class="col-md-6 text-right">
 @if(Auth::check())
<div class="dropdown sub" style="border-color:#fff;">
  {{ Auth::user()->username }}
  <a id="dLabel" role="button" data-toggle="dropdown" class="menu_choice" data-target="#">
    <span class="glyphicon glyphicon-cog"></span> 
  </a>
  <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
    <li><a href="#">Profile</a></li>
    <li><a href="#">Change Password</a></li>
    <li><a href="{{ URL::route('account-sign-out')}}">Sign Out</a></li>
  </ul>
</div>
</div>
</div>
</div>
<div class="row">  
  @yield('section')
</div>
@stop