@extends('layout.dashboard')
@extends('layout.main')
@section('content')
@section('section')
<div class="container">
   <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#ce0002; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >LOGIN</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('account-sign-post')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputEmail" style="text-align: right;" class="control-label col-sm-4">User Name</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" name="username"{{ (Input::old('username'))?' value="'.Input::old('username').'"':''}} placeholder="User Name">
                     @if($errors->has('username'))
                     {{ $errors->first('username')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" style="text-align: right;" class="control-label col-sm-4">Password</label>
                  <div class="col-sm-8">
                     <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="userpassword">
                     @if($errors->has('userpassword'))
                     {{ $errors->first('userpassword')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="login-submit" tabindex="4" 
                           value="Sign In" class="btn col-md-offset-2">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-12">
                      <div class="col-md-3"></div>
                      <div class="col-md-4" style="margin-left:48px;width:148px;"><a href="{{ URL::route('account-forgotpassword')}}" tabindex="5" class="forgot-password">Forgot Password?</a></div>
                      <div class="col-md-4"><a href="{{ URL::route('account-create')}}" tabindex="5" class="forgot-password">Register Account</a></div>
                        
                     </div>

                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop