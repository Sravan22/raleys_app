@extends('layout.dashboard')
@extends('layout.main')

@section('content')
@section('section')

<div class="container">
    	  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#ce0002; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >FORGOT PASSWORD</div>
                       </div> 

                 <div style="padding-top:30px" class="panel-body" >

						
								<form action="{{URL::route('account-forgotpassword-post')}}" class="form-horizontal" method="post" role="form" style="display: block;">
								<div class="form-group">
            <label for="inputEmail" class="control-label col-sm-2">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="email"{{ (Input::old('email'))?' value="'.Input::old('email').'"':''}} placeholder="Email">
                 @if($errors->has('email'))
            {{ $errors->first('email')}}
            @endif
            </div>
        </div>
        
									
									<div class="form-group">
										<div class="row">
											 <div class="col-sm-12" align="center">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" value="Sign In" class="btn">
                                                {{ Form::token()}}
											</div>
										</div>
									</div>
        
		<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
												
						 <a href="{{ URL::route('account-sign-in')}}" tabindex="5" class="forgot-password">Sign In</a>							
                          &nbsp; &nbsp;
                         <a href="{{ URL::route('account-create')}}" tabindex="5" class="forgot-password">Register Account</a> 
                         
												</div>
											</div>
										</div>
									</div>
							
        
								</form>
								
							
					</div>
				</div>
			</div>
		</div>
        
        
        
        
    
    
		<!----<form  action="{{URL::route('account-sign-post')}}" method="post">
				<div class="field">
					Email : <input type="text" name="email"{{ (Input::old('email'))?' value="'.Input::old('email').'"':''}}>
					@if($errors->has('email'))
						{{ $errors->first('email')}}
						@endif
				</div>
				<div class="field">
					Password : <input type="password" name="userpassword">
					@if($errors->has('password'))
						{{ $errors->first('password')}}
						@endif
				</div>
				<a href="{{ URL::route('account-forgotpassword')}}">Forgot Password</a>
				<input type="submit" value="Sign In">
				{{ Form::token()}}
		</form>--->


        
@stop		
		