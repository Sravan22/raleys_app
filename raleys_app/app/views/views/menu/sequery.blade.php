@extends('layout.dashboard')

@section('page_heading','Receivings')
@section('content')
@section('section')

<header class="row">
        @include('menu.recivermenu')
    </header>
<div class="container">
  <!-- <p id="menulist">
      <a href="{{ URL::route('menu-receivings')}}" >Receivings</a> >> Store Expenses >>
      <a href="{{ URL::route('menu-receivings-sequery')}}" >Query</a>
  </p> -->
  <div class="container">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >Store Expenses </div>
                       </div> 

                 <div style="padding-top:30px" class="panel-body" >

            
                <form action="{{URL::route('account-sign-post')}}" class="form-horizontal" method="post" role="form" style="display: block;">

                <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Store</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="store" placeholder="Store" name="store">
                 @if($errors->has('store'))
            {{ $errors->first('store')}}
            @endif
            </div>
        </div> 

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Vendor</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="vendor" placeholder="Vendor" name="Vendor">
                 @if($errors->has('vendor'))
            {{ $errors->first('vendor')}}
            @endif
            </div>
        </div>   

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Vendor Number</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="vendornum" placeholder="Vendor Number" name="vendornum">
                 @if($errors->has('vendornum'))
            {{ $errors->first('vendornum')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Invoice Date</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="invdate" placeholder="Invoice Date" name="invdate">
                 @if($errors->has('invdate'))
            {{ $errors->first('invdate')}}
            @endif
            </div>
        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Invoice Number</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="invnum" placeholder="Invoice Number" name="invnum">
                 @if($errors->has('invnum'))
            {{ $errors->first('invnum')}}
            @endif
            </div>
        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Expense Type</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="expensetype" placeholder="Expense Type" name="expensetype">
                 @if($errors->has('expensetype'))
            {{ $errors->first('expensetype')}}
            @endif
            </div>
        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Amount</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="amount" placeholder="Amount" name="amount">
                 @if($errors->has('amount'))
            {{ $errors->first('amount')}}
            @endif
            </div>
        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Status</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="status" placeholder="Status" name="status">
                 @if($errors->has('status'))
            {{ $errors->first('status')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Created</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="created" placeholder="Created" name="created">
                 @if($errors->has('created'))
            {{ $errors->first('created')}}
            @endif
            </div>
        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">Last Updated</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="lastupdated" placeholder="Last Updated" name="lastupdated">
                 @if($errors->has('lastupdated'))
            {{ $errors->first('lastupdated')}}
            @endif
            </div>
        </div>
        

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-8">By</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="by" placeholder="by" name="by">
                 @if($errors->has('by'))
            {{ $errors->first('by')}}
            @endif
            </div>
        </div>
        
                  
                  <div class="form-group">
                    <div class="row">
                       <div class="col-sm-12" align="center">
        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                <input type="submit" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn">
                                                {{ Form::token()}}
                      </div>
                    </div>
                  </div>
                  
                </form>
                
              
          </div>
        </div>
      </div>
    </div>
</div>
@stop
