@extends('layout.dashboard')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.recivermenu')
    </header>

<div class="container">
<!-- <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>
        Print</span></a> -->
        <div class="col-md-12 text-right">
            <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-weeklyreport',['download'=>'pdf' , 'storeno'=>'storeno', 'department'=>'department', 'fromdate'=>'fromdate', 'todate'=>'todate']) }}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
        </div>

 <table class="table table-striped">
        <thead>
          <tr>
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th>Name</th>
            <th>Type</th>
            <th>Total Cost</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($results as $rs)
          <tr>
            <td>{{ $rs->id }}</td>
            <td>{{ $rs->invoice_date }}</td>
            <td>{{ $rs->name }}</td>
            <td>{{ $rs->type_code }}</td>
            <td>{{ $rs->tot_vend_cost }}</td>
          </tr>
        @endforeach
        </tbody>
    </table>          
    <?php //echo $results->links(); ?>
</div>
@stop
