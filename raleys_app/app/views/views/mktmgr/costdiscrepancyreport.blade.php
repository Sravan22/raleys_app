@extends('layout.dashboard')

@section('page_heading','Receivings')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.recivermenu')
    </header>
<div class="container">
  <!-- <p id="menulist">
      <a href="{{ URL::route('mktmgr-receivings')}}" >Receivings</a> >> Store Expenses >>
      <a href="{{ URL::route('mktmgr-receivings-sequery')}}" >Query</a>
  </p> -->
  <div class="container">
        <div id="loginbox" style="margin-top:5px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >Cost Discrepancy Report</div>
                       </div> 

                 <div style="padding-top:30px" class="panel-body" >

            
                <form action="{{URL::route('mktmgr-post-report-costdiscrepancyreport')}}" class="form-horizontal" method="post" role="form" style="display: block;">

                <div class="form-group">
                    <label for="inputPassword" class="col-sm-4 control-label">Please key date of report</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="keydate" placeholder="" name="keydate" />
                         @if($errors->has('keydate'))
                    {{ $errors->first('keydate')}}
                    @endif
                    </div>
                </div> 

                  <div class="form-group">
                    <div class="row">
                       <div class="col-sm-12" align="center">
        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                <input type="submit" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn">
                                                {{ Form::token()}}
                      </div>
                    </div>
                  </div>
                  
                </form>
                
              
          </div>
        </div>
      </div>
    </div>
</div>
@stop
