@extends('layout.dashboard')

@section('page_heading','Receivings >> Printer Selection Menu')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.recivermenu')
    </header>

<div class="container">
<!-- <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>
        Print</span></a> -->
        <div class="col-md-12 text-right">
            <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
        </div>
    <table class="table table-striped" id="example">
        <thead>
          <tr>
            <th>Department Number</th>
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th>Invoice Type</th>
            <th>Status</th>
            <th>Vendor Number</th>
            <th>Transporter Number</th>
            <th>Cost Discrepancies</th>
            <th>Method Received</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($receivings as $vnd)
          <tr>
            <td>{{ $vnd->dept_number }}</td>
            <td>{{ $vnd->id }}</td>
            <td>{{ $vnd->invoice_date }}</td>
            <td>{{ $vnd->type_code }}</td>
            <td>{{ $vnd->status_code }}</td>
            <td>{{ $vnd->vendor_number }}</td>
            <td>{{ $vnd->trns_number }}</td>
            <td>{{ $vnd->cost_discrep_sw }}</td>
            <td>{{ $vnd->type_code }}</td>
          </tr>
        @endforeach
        </tbody>
    </table>          
    <?php echo $receivings->links(); ?>
</div>

@stop
