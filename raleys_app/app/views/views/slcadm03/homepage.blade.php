@extends('layout.dashboard')
@section('content')
@section('section')

<div class="containerhome homepageicons"> 
  <!--   <div class="row"> -->
            <a href="{{ URL::route('slcadm03-displaybatch')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               Display Batches 
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcadm03-batchtags')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               Print Batch Tags/Signs
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcadm03-batchpos')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Apply Batch to POS File
            </div>
            </div>
            </a>

            <a href="#">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                 Delete Batch
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcadm03-reprinttags')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Reprint Tags/Signs
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcadm03-summarypage')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Summary Pages
            </div>
            </div>
            </a>

             <a href="{{ URL::route('slcadm03-restorepage')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Restore Batch
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcadm03-displayreportcode')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Display Report Codes
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcadm03-itemlookup')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Item Lookup
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcadm03-emailhelpdesk')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                E-mail (HelpDesk News)
            </div>
            </div>
            </a>

      <!--   </div> -->
</div>
@stop
