<div class="container">



	<div class="container-fluid">
		
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		    <span class="sr-only">Toggle navigation</span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		  </button>
		  
		</div>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
      <!-- .dropdown -->  
		    <li class="dropdown">
			   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Menus <span class="caret"></span></a>
			  <ul class="dropdown-menu" role="menu">				          
			    <li><a href="#">Shrink Capture</a></li>
          <li><a href="#">Query</a></li>
          <li><a href="#">Browse</a></li>
          <li><a href="#">Add</a></li>
          <li><a href="#">Update</a></li>
			  </ul>
			</li> <!-- .dropdown -->		
      <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Configurations <span class="caret"></span></a>				
          <ul class="dropdown-menu" role="menu">
              <li><a href="#">Query</a></li>
              <li><a href="#">Browse</a></li>
              <li><a href="#">Add</a></li>
              <li><a href="#">Update</a></li>
              <li><a href="#">Report</a></li>
          </ul>
      </li>   

      <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Functions <span class="caret"></span></a>        
          <ul class="dropdown-menu" role="menu">
              <li><a href="#">Query</a></li>
              <li><a href="#">Browse</a></li>
              <li><a href="#">Add</a></li>
              <li><a href="#">Update</a></li>
          </ul>
      </li>  


      <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dex_commids <span class="caret"></span></a>        
          <ul class="dropdown-menu" role="menu">
              <li><a href="#">Query</a></li>
              <li><a href="#">Browse</a></li>
              <li><a href="#">Update</a></li>
          </ul>
      </li> 

      <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Schedules <span class="caret"></span></a>        
          <ul class="dropdown-menu" role="menu">
              <li><a href="#">Query</a></li>
              <li><a href="#">Browse</a></li>
              <li><a href="#">Add</a></li>
              <li><a href="#">Update</a></li>
          </ul>
      </li> 

      <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Qty_limits<span class="caret"></span></a>        
          <ul class="dropdown-menu" role="menu">
              <li><a href="#">Grocery</a></li>
              <li><a href="#">Liquor</a></li>
              <li><a href="#">Meat</a></li>
              <li><a href="#">Deli</a></li>
              <li><a href="#">FSD</a></li>
              <li><a href="#">Bakery</a></li>
              <li><a href="#">Produce</a></li>
              <li><a href="#">Nat Fds</a></li>
              <li><a href="#">GM</a></li>
              <li><a href="#">Hot Wok</a></li>
              <li><a href="#">Floral</a></li>
              <li><a href="#">CoffShp</a></li>
              <li><a href="#">Seafood</a></li>
              <li><a href="#">RX</a></li>
          </ul>
      </li>  

      <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Old_menus<span class="caret"></span></a>        
          <ul class="dropdown-menu" role="menu">
              <li><a href="#">Query</a></li>
              <li><a href="#">Browse</a></li>
              <li><a href="#">Add</a></li>
              <li><a href="#">Update</a></li>
              <li><a href="#">Report</a></li>
          </ul>
      </li>  
       </ul> <!-- .nav .navbar-nav -->		 
		</div><!-- /.navbar-collapse -->
		    
	</div><!-- /.container-fluid -->

 
</div>  

<style>
.dropdown-submenu {
  position: relative;
}
.dropdown-submenu > .dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
}
.dropdown-submenu:hover > .dropdown-menu {
  display: block;
}
.dropdown-submenu:hover > a:after {
  border-left-color: #fff;
}
.dropdown-submenu.pull-left {
  float: none;
}
.dropdown-submenu.pull-left > .dropdown-menu {
  left: -100%;
  margin-left: 10px;
}

</style>