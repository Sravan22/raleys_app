@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')

<div class="container homepageicons"> 
    <div class="row">
             <a href="{{ URL::route('slcscn01-slcslic')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               SLIC
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcscn01-slcaislescan')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               Aisle Scan
            </div>
            </div>
            </a>

             <a href="{{ URL::route('slcscn01-slcqueryitem')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
             <div class="caption">
               Query Items
            </div>
            </div>
            </a>

            <a href="{{ URL::route('slcscn01-slctagupdtqry')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               Aisle/Tag Updt/Qry
            </div>
            </div>
            </a>

             <a href="{{ URL::route('slcscn01-slcapplybatch')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Apply Batch
            </div>
            </div>
            </a>

        </div>
</div>
@stop
