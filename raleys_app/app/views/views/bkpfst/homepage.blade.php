@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')

<div class="containerhome homepageicons"> 
    <!-- <div class="row"> -->
             <a href="{{ URL::route('bkpfst-bookkeeper1st')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Bookkeeper-1st
            </div>
            </div>
            </a>

            <a href="{{ URL::route('bkpfst-bkpfstfsaisle1')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style"><div class="caption">
               Flash Sales - Aisle 1
            </div>
            </div>
            </a>

            <a href="{{ URL::route('bkpfst-bkpfstfsmainstore')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Flash Sales - Main Store
            </div>
            </div>
            </a>

            <a href="{{ URL::route('bkpfst-bkpfstlogintoaisle1')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                 Log Into Aisle 1
            </div>
            </div>
            </a>

            <a href="{{ URL::route('bkpfst-bkpfstreceivings')}}">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Receivings
            </div>
            </div>
            </a>

        <!-- </div> -->
</div>
@stop
