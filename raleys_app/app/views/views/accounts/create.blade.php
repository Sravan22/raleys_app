@extends('layout.dashboard')
@extends('layout.main')

@section('content')
@section('section')

  <div class="container">
    	  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-9 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#ce0002; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >Register Account</div>
                       </div> 

                 <div style="padding-top:30px" class="panel-body" >

						
								<form action="{{URL::route('account-create-post')}}" class="form-horizontal" method="post" role="form" style="display: block;">

					<div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">First Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="firstname"{{ (Input::old('firstname'))?' 
			                       value="'.Input::old('firstname').'"':''}} placeholder="First Name">
			                 @if($errors->has('firstname'))
			            {{ $errors->first('firstname')}}
			            @endif       
			            </div>
			        </div>				

					<div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Last Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="lastname"{{ (Input::old('lastname'))?' 
			                       value="'.Input::old('lastname').'"':''}} placeholder="Last Name">
			                  @if($errors->has('lastname'))
			            {{ $errors->first('lastname')}}
			            @endif 
			            </div>
			        </div>	

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">User Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="username"{{ (Input::old('username'))?' 
			                       value="'.Input::old('username').'"':''}} placeholder="User Name">
			                 @if($errors->has('username'))
			            {{ $errors->first('username')}}
			            @endif
			            </div>
			        </div>

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">User Email</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="email"{{ (Input::old('email'))?' 
			                       value="'.Input::old('email').'"':''}} placeholder="User Email">
			                 @if($errors->has('email'))
			            {{ $errors->first('email')}}
			            @endif
			            </div>
			        </div>

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Password</label>
			            <div class="col-sm-9">
			                <input type="password" class="form-control" name="passwrod"{{ (Input::old('passwrod'))?' 
			                       value="'.Input::old('passwrod').'"':''}} placeholder="Passwrod">
			                 @if($errors->has('passwrod'))
			            {{ $errors->first('passwrod')}}
			            @endif
			            </div>
			        </div>

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Retype-Password</label>
			            <div class="col-sm-9">
			                <input type="password" class="form-control" name="retypepassword"{{ (Input::old('retypepassword'))?' 
			                       value="'.Input::old('retypepassword').'"':''}} placeholder="Retype Passwrod">
			                 @if($errors->has('retypepassword'))
			            {{ $errors->first('retypepassword')}}
			            @endif
			            </div>
			        </div>

				<div class="form-group">
					<div class="row">
					<div class="col-sm-12" align="center">
					<input type="submit" name="login-submit" id="login-submit" tabindex="4" value="Register" class="btn">
                         {{ Form::token()}}
					</div>
					</div>
				</div>

					<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
												
						 <a href="{{ URL::route('account-sign-in')}}" tabindex="5" class="forgot-password">Sign In</a>							
                          &nbsp; &nbsp;
                         <a href="{{ URL::route('account-forgotpassword')}}" tabindex="5" class="forgot-password">Forgot Password?</a> 
                         
												</div>
											</div>
										</div>
									</div>

			</form>
					</div>
				</div>
			</div>
		</div>
@stop
