@extends('layout.dashboard')
@section('content')
@section('section')

<div class="containerhome homepageicons"> 
   <!--  <div class="row"> -->
            <a href="{{ URL::route('mktmgr-bookkeeper')}}" accesskey="1">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Bookkeeper Market - 1
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-nonperishableinvprepview')}}" accesskey="2">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
               Non Perishable Inventory Prep  - 2
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-receivings-query')}}" accesskey="3">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Receivings - 3
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-shrinkcapturereview')}}" accesskey="4">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Shrink Capture Review - 4
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-storeorderview')}}" accesskey="5">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Store Orders - 5
            </div>
            </div>
            </a>

            <a href="{{ URL::route('mktmgr-storetransfersview')}}" accesskey="6">
            <div class="col-lg-3 col-md-4 col-xs-6 thumb box-style">
            <div class="caption">
                Store Transfers - 6
            </div>
            </div>
            </a>
      <!--   </div> -->
</div>
@stop
