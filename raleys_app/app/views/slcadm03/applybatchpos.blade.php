@extends('layout.dashboard')
@section('page_heading',$data['page_heading'])
@section('content')
@section('section')

<header class="row">
       @include('slcadm03.slcadm03menu')
    
    </header>
<div class="container">
  <div class="menu_search_line">    

</div>
<div class="container" style="padding-top:10px;">
  <form action="#" class="form-horizontal" method="post" role="form" style="display: block;"> 
<table class="table table-striped">
    <thead>
    <tr>
        <th>{{ $data['tabcol1'] }}</th>
        <th>{{ $data['tabcol2'] }}</th>
        <th>{{ $data['tabcol3'] }}</th>
        <th>{{ $data['tabcol4'] }}</th>
        <th>{{ $data['tabcol5'] }}</th>
        <th>{{ $data['tabcol6'] }}</th>
        <th>{{ $data['tabcol7'] }}</th>
        <th>{{ $data['tabcol8'] }}</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>6279</td>
        <td>Floral Price Changes</td>
        <td>12/15</td>
        <td>12/21</td>
        <td>7</td>
        <td>---</td>
        <td>---</td>
        <td>---</td>
      </tr>
      
    </tbody>
  </table>

  <!--  <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
            <input type="submit" name="addinfo" id="addinfo" tabindex="4" value="Query" class="btn">
              {{ Form::token()}}
        </div>
        </div>
  </div> -->
 </form>
</div>
@stop
