<?php 
   //echo '<pre>';print_r($data->dept_detail_list);exit;
?>
@extends('layout.dashboard')
@section('page_heading','Display Batches')
@section('content')
@section('section')
<header class="row">
   {{--  @include('slcadm03.slcadm03menu') --}}
</header>
<div class="container">
   <div class="dropdown sub">
      <a  href="{{ URL::route('slcadm03-returnhome')}}" >
      Exit
      </a>
   </div>
</div>
<?php
   $cnt=0;
   $tchng_desc='';
   if(!empty($data->dept_detail_list)){
           
             foreach($data->dept_detail_list as $dept=>$row){
                 if($cnt==0){
                 $tchng_desc=$row['tchng_desc'];
                 break;
                 }
   }}
     ?>
<div class="container" style="padding-top:10px;">
   <table class="table table-striped">
      <tr>
         <td class="col-md-4">Batch:{{$data->batch_no}}</td>
         <td class="col-md-4 text-center">Dept:{{$cur_dept}}</td>
         <td class="col-md-4 text-right">O TFWW A:{{$tchng_desc}}</td>
      </tr>
   </table>
   <table class="table table-striped">
      <thead>
         <tr>
            <th>GTIN</th>
            <th>Pack/Size</th>
            <th>Description</th>
            <th>Reg.Rtl</th>
            <th>PromoRtl</th>
            <th>Tg</th>
            <th>Sn</th>
            <th>V</th>
            <th>XSTI</th>
            <th>G</th>
            {{-- <th>Scale</th>
            <th>Wic</th> 
            <th>Age</th>--}}
         </tr>
      </thead>
      <tbody>
         @if(!empty($data->dept_detail_list))
         @foreach($data->dept_detail_list as $dept=>$row)
         <tr>
            <td>{{$row['gtin']}}</td>
            
            <td>{{substr($row['pack_size'], 0, 10)}}</td>
            <td>{{$row['desc_18']}}</td>
            <td>{{$row['reg_retail']}}</td>
            <td>{{$row['promo_retail']}}</td>
            @if($row['ttag_count'] == 0)
            <td></td>
            @else
            <td>{{$row['ttag_count']}}</td>
            @endif
            @if($row['tsign_count'] == 0)
            <td></td>
            @else
            <td>{{$row['tsign_count']}}</td>
            @endif
            <td>{{$row['dept_override']}}</td>
             
            <td>{{$row['tax_flag']}}{{$row['fs_flag']}}{{$row['scale_flag']}}{{$row['wic_flag']}}</td>

            <td>{{$row['age_flag']}}</td>
            {{-- <td>{{$row['scale_flag']}}</td>
            <td>{{$row['wic_flag']}}</td> 
            <td>{{$row['age_flag']}}</td>--}}
         </tr>
         @endforeach
         @endif
      </tbody>
   </table>
</div>
<div class="container" style="padding-top:10px;">
   <div class="col-sm-12" align="center">
      <input type="button" value="Back" class="btn"  onclick="window.location.href='{{URL::route('slcadm03-batch-display-detail',array('type'=>$batch_function,'batch'=>$data->batch_no,'disp_type'=>'D'))}}'" />
   </div>
</div>
@stop