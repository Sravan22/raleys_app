@extends('layout.dashboard')
@section('page_heading','Welcome')
@section('content')
@section('section')
 
 
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div class="mainbox" style="padding-top:8%;">
      <div class="panel panel-info" style="width: 480px;">
         <div class="panel_heading">
            <div class="panel-title">SELECT STORE</div>
         </div>
         <div class="panel_body">
            <form action="{{URL::route('slcadm03-store-option')}}" class="form-horizontal" method="post" role="form" class="col-sm-12">
               <div class="form-group" style="padding-bottom: 5px;">
                  <label for="store_no" class="control-label col-sm-4">Select Store</label>
                  <div class="col-sm-7">
                     <select id="store_no" name="store_no" class="form-control" value="store_no">
                         <option value="305">Store 305</option>
                         <option value="355">Fuel Store 355</option>
                     </select>
                  </div>
               </div>
                
               <div class="form-group" style="padding-bottom: 5px;">
                 <div class="row">
                    <div class="col-md-4"></div>
                    {{-- <div class="col-md-2"></div> --}}
                    <div class="col-md-4">
                       <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                       <input type="button" value="Cancel" class="btn" onClick="document.location.href='#'" />
                       {{ Form::token()}}
                    </div>
                 </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
 
@stop