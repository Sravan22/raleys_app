@extends('layout.dashboard')
@section('page_heading','Display Batches')
@section('content')
@section('section')
<header class="row">
   @include('slcadm03.slcadm03menu')
</header>
<div class="container">
   <div class="dropdown sub">
      <a  href="{{ URL::route('slcadm03-returnhome')}}" >
      Exit
      </a>
   </div>
   {{-- <a href="{{ URL::route('slcadm03-view-batch-details')}}" class="btn btn-primary" style="float:right;">View Batch Details</a> --}}
</div>
<div class="container" style="padding-top:10px;">
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Batch</th>
            <th>Display Batches  Description</th>
            <th>Batch Load</th>
            <th>Eff Date</th>
            <th>Item Count</th>
            <th>Tags Print</th>
            <th>Regst Apply</th>
            <th>Rpt Cd</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td> 6279</td>
            <td>Floral Price Changes</td>
            <td>12/15</td>
            <td>12/21</td>
            <td>7</td>
            <td>---</td>
            <td></td>
            <td></td>
         </tr>
      </tbody>
   </table>
</div>
<div class="container">
   <table class="table table-striped">
      <thead>
         <tr>
            <th>Dept</th>
            <th>Ad On</th>
            <th>Ad Off</th>
            <th>OnTPR</th>
            <th>OffTPR</th>
            <th>Price</th>
            <th>Item</th>
            <th>Disco</th>
            <th>New</th>
            <th>Total</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td><u>{{ HTML::link(URL::route('slcadm03-batch-department-details'), 80 ) }}</u></td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>31</td>
            <td>23</td>
            <td>0</td>
            <td>15</td>
            <td>0</td>
            <td>69</td>
         </tr>
         <tr>
            <td>81</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
         </tr>
         <tr>
            <td>82</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
         </tr>
         <tr>
            <td>Total</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>33</td>
            <td>25</td>
            <td>0</td>
            <td>15</td>
            <td>0</td>
            <td>73</td>
         </tr>
      </tbody>
   </table>
</div>
@stop

<style type="text/css">
  .verticalLine {
    border-right-style:  dotted;
}
</style>