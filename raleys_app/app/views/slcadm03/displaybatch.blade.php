@extends('layout.dashboard')
@section('page_heading','Display Batches')
@section('content')
@section('section')

<header class="row">
       @include('slcadm03.slcadm03menu')
    
    </header>
<div class="container">
   
<div class="dropdown sub">
            <a  href="{{ URL::route('slcadm03-returnhome')}}" >
                Exit
            </a>
        </div>
</div>
<div class="container" style="padding-top:10px;">
<table class="table table-striped">
    <thead>
    <tr>
        <th>Batch</th>
        <th>Display Batches  Description</th>
        <th>Batch Load</th>
        <th>Eff Date</th>
        <th>Item Count</th>
        <th>Tags Print</th>
        <th>Regst Apply</th>
        <th>Rpt Cd</th>
       
      </tr>
    </thead>
    <tbody>
      <tr>
      <td><u>{{ HTML::link(URL::route('slcadm03-batch-sign-tag-counts'), 6279 ) }}</u></td>
        {{-- <td>6279</td> --}}
        <td>Floral Price Changes</td>
        <td>12/15</td>
        <td>12/21</td>
        <td>7</td>
        <td>---</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>6321</td>
        <td>Liquor TPR Changes</td>
        <td>12/15</td>
        <td>12/21</td>
        <td>1</td>
        <td>---</td>
        <td></td>
        <td></td>
      </tr>
      
    </tbody>
  </table>
</div>
@stop
