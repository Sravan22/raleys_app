@extends('layout.admindashboard')


@section('content')
@section('section')

  <div class="container">
   <ul class="navbar dib_float_left">
    <!-- <li id="main-home-link" class="menu_choice home_menu_choice active">
        Printer Selection Menu
    </li> -->
     <li class="menu_choice"><a id="seShowEnrollmentModal" style="color:#F00;" href="{{ URL::route('admin-admregistration')}}">Admin</a></li>
  <li class="menu_choice"><a id="seShowEnrollmentModal"  href="{{ URL::route('admin-userregistration')}}">User</a></li>
   


    </ul>
    	  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-9 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#000; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" style="color:#fff;" >Admin Register</div>
                       </div> 

                 <div style="padding-top:30px" class="panel-body" >

						
								<form action="{{URL::route('admin-create-post')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                                <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> 
                         <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Store Number</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="storenumber"{{ (Input::old('storenumber'))?' 
			                       value="'.Input::old('storenumber').'"':''}} placeholder="storenumber">
			                 @if($errors->has('storenumber'))
			            {{ $errors->first('storenumber')}}
			            @endif       
			            </div>
			        </div>				

					<div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">First Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="firstname"{{ (Input::old('firstname'))?' 
			                       value="'.Input::old('firstname').'"':''}} placeholder="First Name">
			                 @if($errors->has('firstname'))
			            {{ $errors->first('firstname')}}
			            @endif       
			            </div>
			        </div>				

					<div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Last Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="lastname"{{ (Input::old('lastname'))?' 
			                       value="'.Input::old('lastname').'"':''}} placeholder="Last Name">
			                  @if($errors->has('lastname'))
			            {{ $errors->first('lastname')}}
			            @endif 
			            </div>
			        </div>	

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">User Name</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="username"{{ (Input::old('username'))?' 
			                       value="'.Input::old('username').'"':''}} placeholder="User Name">
			                 @if($errors->has('username'))
			            {{ $errors->first('username')}}
			            @endif
			            </div>
			        </div>

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">User Email</label>
			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="email"{{ (Input::old('email'))?' 
			                       value="'.Input::old('email').'"':''}} placeholder="User Email">
			                 @if($errors->has('email'))
			            {{ $errors->first('email')}}
			            @endif
			            </div>
			        </div>

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Password</label>
			            <div class="col-sm-9">
			                <input type="password" class="form-control" name="passwrod"{{ (Input::old('passwrod'))?' 
			                       value="'.Input::old('passwrod').'"':''}} placeholder="Passwrod">
			                 @if($errors->has('passwrod'))
			            {{ $errors->first('passwrod')}}
			            @endif
			            </div>
			        </div>

			        <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">Retype-Password</label>
			            <div class="col-sm-9">
			                <input type="password" class="form-control" name="retypepassword"{{ (Input::old('retypepassword'))?' 
			                       value="'.Input::old('retypepassword').'"':''}} placeholder="Retype Passwrod">
			                 @if($errors->has('retypepassword'))
			            {{ $errors->first('retypepassword')}}
			            @endif
			            </div>
			        </div>
                  <div class="form-group">
			            <label for="inputEmail" class="control-label col-sm-3">User Type</label>
			            <div class="col-sm-9">
			                <input type="checkbox"  name="superadmin" value="1" > Superadmin
                            <input type="checkbox"  name="admin" value="1" > Admin 
                            <input type="checkbox"  name="subadmin" value="1" > Subadmin
                            
                            
			                 @if($errors->has('access_rights'))
			            {{ $errors->first('access_rights')}}
			            @endif
			            </div>
			        </div> 

				<div class="form-group">
					<div class="row">
					<div class="col-sm-12" align="center">
					<input type="submit" name="login-submit" id="login-submit" tabindex="4" value="Register" class="btn">
                         {{ Form::token()}}
					</div>
					</div>
				</div>

					<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
												
						<!-- <a href="{{ URL::route('admin')}}" tabindex="5" class="forgot-password">Sign In</a>							
                          &nbsp; &nbsp;
                         <a href="{{ URL::route('admin-forgotpassword')}}" tabindex="5" class="forgot-password">Forgot Password?</a> --->
                         
												</div>
											</div>
										</div>
									</div>

			</form>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
  <div class="panel-body">
        <table class="table table-bordered">
      <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
             @if(Auth::user()->username == 'superadmin') <th>Action</th> @else  @endif
        </tr>
      </thead>
      <tbody id="userlistall">
       @foreach ($superadminlist as $sir)
        <tr class="" id="user{{ $sir->id }}">
            <td>{{ $sir->firstname}}</td>
            <td>{{ $sir->email }}</td> 
            @if(Auth::user()->username == 'superadmin') 
           
            	<!-- <button type="button" class="btn btn-default btn-sm" > -->
                              <td>
                              <a href="{{ route('admin-edituser',['id'=>$sir->id]) }}" class="glyphicon glyphicon-edit">Update</a>
                              
                              

			   <!--  </button> -->
             &nbsp;&nbsp; 
            <!-- <button type="button" class="btn btn-default btn-sm"> -->
            <a href="{{ route('admin-deleteuser',['id'=>$sir->id]) }}" post_data='{{$sir->id}}' class="glyphicon glyphicon-trash">Delete</a>
             
            <!-- </button> -->
            </td>  @else @endif
        </tr>
        @endforeach
      </tbody>
    </table>  
      </div>
    <?php //echo $superadminlist->links(); ?>
</div>	
<style type="text/css">
   .table-bordered > thead > tr > th
  {
    background-color: #000;
    text-align: center;
    color: #ffffff;
  }
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">  
$(document).ready(function(){
$(".glyphicon-trash").click(function (e) { 
	e.preventDefault();
	var value = $(".glyphicon-trash").attr("post_data");
	
	var userid = value.substr(value.indexOf("=") + 1);
	//alert(value);
   if(userid != '')
   {
   	 $.post("deleteuser",{userid: userid}, function(result){
	 	//alert(result);
   	 	 
     	//userlistall
     	//$("#userlistall").html(result);
		//$('#'+userid).remove();
		//alert(userid);
		$('#user'+userid).remove();
       });
   }
});
});
</script>
@stop
