@extends('layout.admindashboard')
@extends('layout.main')

@section('content')
@section('section')
    
    <div class="container">
    	  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#000; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title"  style="color:#fff;">LOGIN</div>
                       </div> 

                 <div style="padding-top:30px" class="panel-body" >

						
								<form action="{{URL::route('admin-sign-post')}}" class="form-horizontal" method="post" role="form" style="display: block;">
								<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
  <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-3">User Name</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="username"{{ (Input::old('username'))?' value="'.Input::old('username').'"':''}} placeholder="User Name" autofocus>
                 @if($errors->has('username'))
            {{ $errors->first('username')}}
            @endif
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-3">Password</label>
            <div class="col-sm-9">
                <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="userpassword">
                 @if($errors->has('password'))
            {{ $errors->first('password')}}
            @endif
            </div>
        </div>
								<div class="form-group">
										<div class="row">
											 <div class="col-sm-12" align="center">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" 
                               value="Sign In" class="btn">
                                                {{ Form::token()}}
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="{{ URL::route('admin-forgotpassword')}}" tabindex="5" class="forgot-password">Forgot Password?</a>
                          &nbsp; &nbsp;
                         <!--- <a href="{{ URL::route('admin-create')}}" tabindex="5" class="forgot-password">Register Account</a>-->
												</div>
											</div>
										</div>
									</div>

								</form>
								
							
					</div>
				</div>
			</div>
		</div>
   <!--  <form  action="{{URL::route('account-sign-post')}}" method="post">
        <div class="field">
          Email : <input type="text" name="email"{{ (Input::old('email'))?' value="'.Input::old('email').'"':''}}>
          @if($errors->has('email'))
            {{ $errors->first('email')}}
            @endif
        </div>
        <div class="field">
          Password : <input type="password" name="userpassword">
          @if($errors->has('password'))
            {{ $errors->first('password')}}
            @endif
        </div>
        <a href="{{ URL::route('account-forgotpassword')}}">Forgot Password</a>
        <input type="submit" value="Sign In">
        {{ Form::token()}}
    </form> -->


        
@stop   