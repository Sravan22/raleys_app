@extends('layout.dashboard')

@section('page_heading','Receivings')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.recivermenu')
    </header>
<style> 
  @media print {
  a[href]:after {
    content: none !important;
  }
}
</style>
@if($cdreports)
 <div class="container">
<div class="row">
<div class="col-md-6">
<h3>Cost Discrepancy Report</h3></div>
<div class="col-md-6">
<span class="pull-right">
<a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
<a href="{{ route('pdf-costdiscrepancyreport',['keydate'=>$keydate, 'strno'=>$strno ])}}" target="_blank">
<i class="fa fa-file-pdf-o fa-fw iconsize"></i>
</a>
</span>
</div>
</div>
</div>
@endif
  <div class="container">
    <table class="table table-striped">
        <thead>
          <tr>
            <th>Invoice No</th>
            <th>Invoice Date</th>
            <th>Store Number</th>
            <th>Method</th>
            <th>Type</th>
            <th>Department</th>
            <th>Delivery Unit</th>
            <th>Vendor Number</th>
            <th>Cost Discrepancy</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
        @if($cdreports)
        @foreach ($cdreports as $cdr)
          <tr>
            <td>{{ $cdr->id }}</td>
            <td>{{ date("m/d/Y", strtotime($cdr->invoice_date)) }}</td>
            <td>{{ $cdr->store_number }}</td>
            <td>{{ $cdr->method_rcvd }}</td>
            <td>{{ $cdr->type_code }}</td>
            <td>{{ $cdr->dept_number }}</td>
            <td>{{ $cdr->delivery_unit }}</td>
            <td>{{ $cdr->vendor_number }}</td>
            <td>{{ $cdr->cost_discrep_sw }}</td>
            <td>{{ $cdr->status_code }}</td>
          </tr>
       @endforeach
      @else
      <tr><td colspan="10" align="center">No records found!</td></tr>  
    @endif
        </tbody>
    </table>          
    <?php //echo $cdreports->links(); ?>
</div>
@stop
