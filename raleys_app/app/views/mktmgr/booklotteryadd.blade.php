@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >LOTTERY INVENTORY</div>
         </div>
         <div style="padding-top:15px" class="panel-body" >
            <form action="{{URL::route('mktmgr-booklotteryadddata')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-7">Please enter the Game number</label>
                  <div class="col-sm-5">
                     <input type="text" class="form-control" maxlength="5" id="game_no" placeholder="" name="game_no">
                     @if($errors->has('game_no'))
                     {{ $errors->first('game_no')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-7">Please enter the Date of the Information</label>
                  <div class="col-sm-5">
                     <input type="date" class="form-control" id="" placeholder="" name="dateofinfo" value="{{$data->dateofinfo}}">
                     @if($errors->has('dateofinfo'))
                     {{ $errors->first('dateofinfo')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="reset"  tabindex="4" value="Reset" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@section('jssection')
@parent
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $( function() {
     $( "#dateofinfo" ).datepicker({
         changeMonth: true,
       changeYear: true,
       showButtonPanel: true,
       dateFormat:"mm/dd/yy"
     });
   } );
</script>
@endsection
@stop