@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >LOTTERY INVENTORY - {{date('m/d/Y',strtotime($data->lottery_date))}}</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-booklotteryformdata')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <input type="hidden" class="form-control" id="lottery_date" placeholder="" name="lottery_date" value="{{date('Y-m-d',strtotime($data->lottery_date))}}" >
               <div class="focusguard" id="focusguard-1" tabindex="0"></div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-3">Game number</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="game_no" placeholder="" name="game_no" value="{{$data->game_no}}" readonly="" >
                     @if($errors->has('game_no'))
                     {{ $errors->first('game_no')}}
                     @endif
                  </div>
                  <label for="inputPassword" class="control-label col-sm-3">Ticket Value</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control sel multiply tck" autofocus="" tabindex="1" id="ticket_value" placeholder="" name="ticket_value" value="{{$data->ticket_value}}" maxlength="5" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>/>
                      <span class="control-label"></span>
                     @if($errors->has('ticket_value'))
                     {{ $errors->first('ticket_value')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 15px;">
                  <label for="inputPassword" class="control-label col-sm-9">Beginning Inventory</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="begin_inv" placeholder="" name="begin_inv" value="{{$data->begin_inv}}" readonly="">
                     @if($errors->has('begin_inv'))
                     {{ $errors->first('begin_inv')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" >
                  <label for="inputPassword" class="control-label col-sm-9">Deliveries</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control sel multiply flot" id="deliveries" placeholder="" name="deliveries" value="{{$data->deliveries}}" tabindex="2" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>/>
                     @if($errors->has('deliveries'))
                     {{ $errors->first('deliveries')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" >
                  <label for="inputPassword" class="control-label col-sm-9">Returns</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control sel flot" tabindex="3" id="returns" placeholder="" name="returns" value="{{$data->returns}}" <?php if($data->file_locked =='Y'){echo 'disabled';} ?> />
                     @if($errors->has('returns'))
                     {{ $errors->first('returns')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" >
                  <label for="inputPassword" class="control-label col-sm-9">Ending Inventory</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control multiply sel flot" tabindex="4" id="end_inv" placeholder="" name="end_inv" value="{{$data->end_inv}}" <?php if($data->file_locked =='Y'){echo 'disabled';} ?> />
                     @if($errors->has('end_inv'))
                     {{ $errors->first('end_inv')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 20px;">
                  <label for="inputPassword" class="control-label col-sm-9">Calculated Sales</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="calc_sales" placeholder="" name="calc_sales" value="{{$data->calc_sales}}" readonly="">
                     @if($errors->has('calc_sales'))
                     {{ $errors->first('calc_sales')}}
                     @endif
                  </div>
               </div>
               <div class="form-group" style="margin-top: 20px;">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="accept-submit" id="submit" tabindex="5" value="Accept" class="btn"
                        <?php if($data->file_locked =='Y'){echo 'disabled';} ?>/>  
                        <input type="button" onclick="history.go(-1)" id="reset"  tabindex="6" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="7"></div>
               {{-- files locked  --}}
               <br>
               @if($data->file_locked =="Y")
               <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @endif
               <div class="form-group" style="margin-top: 20px;">
                  <label class="control-label col-sm-12">   Note: Beginning Inventory from Ending Inventory on {{($data->bi_di_date)}}</label>
               </div>
               
            </form>
         </div>
      </div>
   </div>
</div>
@section('jssection')
@parent
<script>
   function isFloat(num){
       
      if(num.toString().indexOf(".")==-1){
          return false;
      }
      return true;
   }
         function convertnulltozero(val){
       
        if(isNaN(val)==true || val==NaN || val==null || val===undefined || val=='NaN' || typeof(val)=='undefined' || val==''){
           
              val=0;
              
          } else {
              val=val.toFixed(2);
          }
          
        return val;
    }
     jQuery(document).ready(function($) {

        /*Multiply by 100*/
        $(".multiply").focus(function(event) {
        var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(Math.round(selfieldval*100));
        this.select();
        });

        $('#ticket_value').select();
        $('#focusguard-2').on('focus', function() {
        $('#ticket_value').focus();
        });

        $('#focusguard-1').on('focus', function() {
        $('#reset').focus();
        });

        $('#ticket_value').on('blur',function(){
            var num = $(this).val(); 
           console.log(num)
           if(num < 100) {
           $(this).parent().addClass('has-error');
           $(this).next('.control-label').text('Invalid number')
           $('#submit').addClass('disabled');
           //$('#ticket_value').val("");
           
           //$('#ticket_value').select();

           }
           /*else if(num>6000) {
           $(this).parent().addClass('has-error');  
           $(this).next('.control-label').text('Less than 6000')
           $('#submit').addClass('disabled');
           }*/
           else {
           $(this).parent().addClass('has-success');
            $(this).parent().removeClass('has-error');
            
             $(this).next('.control-label').text('');
              $('#submit').removeClass('disabled');
           }
      })
       
                  $("input.form-control").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
           // Allow: Ctrl/cmd+A
          (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+C
          (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+X
          (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
   });
   
    /*  $('.tck').blur(function() {
   if(parseInt($(this).val()) < 100) {
     $(this).val('');
   
     alert('invalid number ');
   
     return false;
   }
   else
   {
   selfield = this.id;
   selfieldval=$("#"+selfield).val(); 
    $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
   }
   });
       
     
       
        $(".sel").on("click", function () {
    $(this).select();
    });
          $(".flot").blur(function(){  
     var sum = 0; 
      selfield = this.id;
       
      selfieldval=$("#"+selfield).val(); 
      $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
   });*/
   
   
   
         $( "input.form-control" ).on("blur", function() {
           if(isFloat($(this).val())==true){
                 
             } else {
              var nw=parseFloat($(this).val())/100;
   
               $(this).val(convertnulltozero(nw));
               calculateSalesDiff();
              }
          });
            
        $( "input.form-control" ).on( "change keyup", function() {              
              calculateSalesDiff();
        });
        
        
        
        function calculateSalesDiff(){
            
              var ticket_value=parseFloat($("#ticket_value").val());
              var begin_inv=parseFloat($("#begin_inv").val());
              var deliveries=parseFloat($("#deliveries").val());
              var returns=parseFloat($("#returns").val());
              var end_inv=parseFloat($("#end_inv").val());
   
            
              
   var calc_sales = ((begin_inv + deliveries - returns - end_inv)); 
   
   $("#calc_sales").val(convertnulltozero(calc_sales))
       //$("#calc_sales").val((calc_sales));
       
   
        }
        
        
        
      });
</script>
@endsection
@stop
