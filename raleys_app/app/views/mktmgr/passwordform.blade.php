@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<div class="container">



<div class="flash-message" id="recapmsg">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
<div id="pwdform" style="margin-top:5px; margin-left: 21%;" class="mainbox col-md-7 col-md-offset-2.5 col-sm-6 col-sm-offset-2">       
        <!--   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-2">   -->                  
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >RECAP REPORT</div>
                       </div> 

                 <div class="panel-body" >
   <form action="{{URL::route('mktmgr-unlockpwd')}}" class="form-horizontal" method="post" role="form" style="display: block;">
    
    <div class="form-group" style="padding: 15px 15px;">
            <label for="inputPassword" class="control-label col-sm-7" style="text-align: right;">
                Please enter the password to unlock the files
            </label>
            <div class="col-sm-5">
            <input type="hidden" name="dateofinfo" id="dateofinfo" value="{{ $dateofinfo }}"> 
                <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                   @if($errors->has('password'))
            {{ $errors->first('password')}}
            @endif
                             </div>
        </div>
    
      
                                    
                                    <div class="form-group">
                                        <div class="row">
                                             <div class="col-sm-12" align="center">
                <input type="submit" name="submit" id="submit" tabindex="4" value="Submit" class="btn">
                <input type="reset" id="" tabindex="4" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('mktmgr/bookwklyunlock')}}'">
                                                {{ Form::token()}}
                                            </div>
                                        </div>
                                    </div>
                                    
                                </form>
                                
                            
                    </div>
                </div>
            </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>        
 <script type="text/javascript">
            $(function(){
                            var dtToday = new Date();
                            
                            var month = dtToday.getMonth() + 1;
                            var day = dtToday.getDate();
                            var year = dtToday.getFullYear();
                            if(month < 10)
                                month = '0' + month.toString();
                            if(day < 10)
                                day = '0' + day.toString();
                            
                            var maxDate = year + '-' + month + '-' + day;
                            $('#dateofinfo').attr('max', maxDate);
                        });
        </script>
@stop