<?php
   //echo '<pre>';print_r($get_ov);exit;
    //echo $dayIsLocked.' '.$file_locked;exit; 
    //echo '<pre>';print_r($init_rec);exit;
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<style type="text/css">
   #coinbox{
   display: none;
   }
   #currencybox{
   display: none;
   }
   #miscbox{
   display: none;
   }
   .menu_search_line{
   /* display: none;*/
   height: 0px;
   }
   .misc_desc{
   display: initial !important;
   } 
   ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   text-transform: initial;
   }
</style>
<div class="container">
<div id="mgmtmaindivbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">RECAP REPORT &nbsp;&nbsp;&nbsp;&nbsp; {{ date('m/d/Y',strtotime($recap_date)) }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <form {{--  action="{{URL::route('mktmgr-postsafecountdetail')}}"  --}} id="" class="form-horizontal" method="post" role="form" style="display: block;">
         <input type="hidden" name="recap_date" id="recap_date"  value=" " /> 
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Dept Sales</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="2" value="{{ number_format((float)$init_rec['dept_sales'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('dept_sales','mgmtmaindivbox')" class="btn  ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Net Sales</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$init_rec['net_safe'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('net_safe','mgmtmaindivbox')" class="btn  ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Deposits</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$init_rec['deposits'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('depositsbox','mgmtmaindivbox')" class="btn  ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Delivery & Transfers</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$init_rec['del_tranfs'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('del_tranfs','mgmtmaindivbox')" class="btn pot_total ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Store Transactions</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$init_rec['store_tx'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('store_tx','mgmtmaindivbox')" class="btn pot_total ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Paid Out Types</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$init_rec['po_types'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('po_types','mgmtmaindivbox')" class="btn pot_total ft" style="width:125px;text-align: right;">
            </div>
         </div>
         {{-- 
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">&nbsp;</label>
            <div style="padding-right: 0px;" class="col-sm-3 text-center">
               ------------------------
            </div>
         </div>
         --}}
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Total</label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="total">{{ number_format((float)$init_rec['summary'], 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <br>
         <div class="row" style="font-weight:bold;">
            <div class="col-md-6 text-center">
               <div class="row">
                  <div class="col-md-6">DRUG (O)S</div>
                  <div class="col-md-6 text-left">{{ number_format((float)$get_ov['drgov'], 2, '.', ''); }}</div>
               </div>
            </div>
            <div class="col-md-6 text-center">
               <div class="row">
                  <div class="col-md-6">MARKET (O)S</div>
                  <div class="col-md-6 text-left">{{ number_format((float)$get_ov['grov'], 2, '.', ''); }}</div>
               </div>
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="row">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-8 text-center">
               <input type="button" name="safecountsubmit" id="safecountsubmit" value="Submit" class="btn" onClick="document.location.href='{{URL::to('mktmgr/mgmtweeklydate')}}'">
               <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/mgmtweeklydate')}}'" />
               {{--  <input type="button" name=""  href="javascript:;" value="MGMT Approval" class="btn "> --}}
               <a class="btn btn-info mgmtapproval" href="javascript:;">MGMT Approval</a>
            </div>
            <div class="col-sm-2">&nbsp;</div>
         </div>
         <div class="row">
            <br>
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center mgmtapprovalmsg" style="color:red;">
            </div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         {{-- @if($dayIsLocked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Data is locked for {{ date("m/d/Y", strtotime($safe_date)) }}. Any changes won't be saved</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif --}}
         </form>
      </div>
   </div>
</div>
{{--Dept Sales Box Starts --}}  
<div id="dept_sales" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <form method="post" id="potbox-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">DEPARTMENT SALES   </div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div  class="panel-body scrollbar" id="style-1">
               <?php
                  $deptsales_total = 0;
                  for($i=0;$i<count($disp_deptsales);$i++)
                  {
                   $deptsales_total = $deptsales_total + $disp_deptsales[$i]['dept_sales'];
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-2" style="font-size:12px;">
                        <label for="inputPassword">(&nbsp;<?php echo $disp_deptsales[$i]['item_id'];
                           ?>&nbsp;)</label>
                     </div>
                     <div class="col-sm-6" style="font-size:12px;">
                        <b><?php echo $disp_deptsales[$i]['item_desc']; 
                           ?></b>
                     </div>
                     <div class="col-sm-4 text-right">
                        <strong><span>{{ number_format((float)$disp_deptsales[$i]['dept_sales'], 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
               <?php   }
                  ?>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row" style="margin-left:31px;margin-top:-2px;">
               <div class="col-sm-6">
                  <div class="row">
                     <div class="col-sm-6">
                        <b>Total to account For</b>
                     </div>
                     <div class="col-sm-6" style="margin-left:177px;font-size:16px;text-align:right;margin-top: -18px;">
                        <strong><span class="po_types_total">{{ number_format((float)$deptsales_total, 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php /*if($file_locked == "Y"){ echo 'disabled'; }*/ ?> name="login-submit" id="potbox_submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('dept_sales','mgmtmaindivbox')"/>
               </div>
            </div>
         </div>
         <br>
         {{-- if($file_locked == "Y") --}}
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         {{--  @endif --}}
         <br>  
      </div>
   </form>
</div>
{{-- Dept Sales Ends --}} 
{{--Net Safe Box Starts --}}  
<div id="net_safe" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <form method="post" id="potbox-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">DEPARTMENT SALES   </div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div  class="panel-body scrollbar" id="style-1">
               <?php
                  $netsafe_total = 0;
                  for($i=0;$i<count($disp_netsafe);$i++)
                  {
                   $netsafe_total = $netsafe_total + $disp_netsafe[$i]['dept_sales'];
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-2" style="font-size:12px;">
                        <label for="inputPassword">(&nbsp;<?php echo $disp_netsafe[$i]['item_id'];
                           ?>&nbsp;)</label>
                     </div>
                     <div class="col-sm-6" style="font-size:12px;">
                        <b><?php echo $disp_netsafe[$i]['item_desc']; 
                           ?></b>
                     </div>
                     <div class="col-sm-4 text-right">
                        <strong><span>{{ number_format((float)$disp_netsafe[$i]['dept_sales'], 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
               <?php   }
                  ?>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row" style="margin-left:31px;margin-top:-2px;">
               <div class="col-sm-6">
                  <div class="row">
                     <div class="col-sm-6">
                        <b>Total to account For</b>
                     </div>
                     <div class="col-sm-6" style="margin-left:177px;font-size:16px;text-align:right;margin-top: -18px;">
                        <strong><span class="po_types_total">{{ number_format((float)$netsafe_total, 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php /*if($file_locked == "Y"){ echo 'disabled'; }*/ ?> name="login-submit" id="potbox_submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('net_safe','mgmtmaindivbox')"/>
               </div>
            </div>
         </div>
         <br>
         {{-- if($file_locked == "Y") --}}
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         {{--  @endif --}}
         <br>  
      </div>
   </form>
</div>
{{-- Net Safe Ends --}} 
{{-- Deposits Box Starts--}}
<div id="depositsbox" style="display: none" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">DEPOSIT DETAIL  &nbsp;&nbsp;&nbsp;&nbsp; {{ date('m/d/Y',strtotime($recap_date)) }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <form {{--  action="{{URL::route('mktmgr-postsafecountdetail')}}"  --}} id="" class="form-horizontal" method="post" role="form" style="display: block;">
         <input type="hidden" name="recap_date" id="recap_date"  value=" " /> 
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Currency Deposits</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="2" value="{{ number_format((float)$init_maindep['currency'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('curr_dep_box','depositsbox')" class="btn  ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Check Deposits</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$init_maindep['checks'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('checks_dep_box','depositsbox')" class="btn  ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6"> M/C - Visa Deposits</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$init_maindep['express_cred'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('visa_deposits_dep_box','depositsbox')" class="btn  ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Express Debit</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$init_maindep['express_deb'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('express_debit_dep_box','depositsbox')" class="btn pot_total ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Discover Deposits</label>
            <div class="col-sm-3">
               &nbsp;
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">American Express</label>
            <div class="col-sm-3">
               &nbsp;
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">EBT Welfare</label>
            <div class="col-sm-3">
               &nbsp;
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Future Item 2</label>
            <div class="col-sm-3">
               &nbsp;
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">EBT WIC</label>
            <div class="col-sm-3">
               &nbsp;
            </div>
         </div>
         <hr/>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Total</label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="total">{{ number_format((float)$init_maindep['total'], 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <hr/>
         <div class="row">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-8 text-center">
               <input type="button" name="safecountsubmit" id="safecountsubmit" value="Submit" class="btn">
               <input type="button" value="Cancel" class="btn" onclick="cancel_click('depositsbox','mgmtmaindivbox')" />
            </div>
            <div class="col-sm-2">&nbsp;</div>
         </div>
         {{-- @if($dayIsLocked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Data is locked for {{ date("m/d/Y", strtotime($safe_date)) }}. Any changes won't be saved</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif --}}
         </form>
      </div>
   </div>
</div>
{{-- Deposits Box Ends --}}
{{-- Delivery Box Box Starts --}}  
<div id="delivery_box" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">
            <div class="row">
               <div class="col-md-6">DELIVERIES  </div>
               <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
            </div>
         </div>
      </div>
      <div class="table-responsive ">
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="text-center">Day</th>
                  <th class="text-center">Date</th>
                  <th class="text-center">ID</th>
                  <th class="text-center">Description</th>
                  <th class="text-center">Amount</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  //$disp_currency;
                   $del_total = 0;
                       for($i=0;$i<count($disp_del);$i++)
                       {
                        $del_total = $del_total + $disp_del[$i]['item_amt'];
                        $weekday = date('l', strtotime($disp_del[$i]['wk_end_date']));
                          $date = date("m/d/Y", strtotime($disp_del[$i]['wk_end_date']));
                          $get_desc_del = DB::select('SELECT safeitems.item_desc as item_desc FROM safeitems
                             WHERE safeitems.item_id = "'.$disp_del[$i]['item_id'].'"'); 
                  ?>
               <tr>
                  <td class="text-center">{{ $weekday }}</td>
                  <td class="text-center">{{ $date }}</td>
                  <td class="text-center">{{ $disp_del[$i]['item_id'] }}</td>
                  <td class="text-center">{{ $get_desc_del[0]->item_desc }}</td>
                  <td class="text-center">{{ number_format((float)$disp_del[$i]['item_amt'], 2, '.', ''); }}</td>
               </tr>
               <?php 
                  }
                  ?>
            </tbody>
         </table>
      </div>
      <br>
      <div class="row">
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4" style="text-align: right;font-size:15px;margin-left:-43px">
            <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ number_format((float)$del_total, 2, '.', ''); }}</strong></span>
         </div>
      </div>
      <br>
      <div class="form-group">
         <div class="row">
            <div class="col-sm-12" align="center">
               <input type="button" value="Cancel" class="btn" onclick="cancel_click('delivery_box','del_tranfs')">
               {{ Form::token()}}
            </div>
         </div>
      </div>
      <br>
   </div>
</div>
{{-- Delivery Box Ends --}}
{{-- Transfer Box Box Starts --}}  
<div id="transfers_box" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">
            <div class="row">
               <div class="col-md-6">TRANSFERS  </div>
               <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
            </div>
         </div>
      </div>
      <div class="table-responsive ">
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="text-center">Day</th>
                  <th class="text-center">Date</th>
                  <th class="text-center">ID</th>
                  <th class="text-center">Description</th>
                  <th class="text-center">Amount</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  //$disp_currency;
                   $tranfs_total = 0;
                       for($i=0;$i<count($disp_tranfs);$i++)
                       {
                        $tranfs_total = $tranfs_total + $disp_tranfs[$i]['item_amt'];
                        $weekday = date('l', strtotime($disp_tranfs[$i]['wk_end_date']));
                          $date = date("m/d/Y", strtotime($disp_tranfs[$i]['wk_end_date']));
                          $get_desc_tranfs = DB::select('SELECT safeitems.item_desc as item_desc FROM safeitems
                             WHERE safeitems.item_id = "'.$disp_tranfs[$i]['item_id'].'"');
                  ?>
               <tr>
                  <td class="text-center">{{ $weekday }}</td>
                  <td class="text-center">{{ $date }}</td>
                  <td class="text-center">{{ $disp_tranfs[$i]['item_id'] }}</td>
                  <td class="text-center">{{ $get_desc_tranfs[0]->item_desc }}</td>
                  <td class="text-center">{{ number_format((float)$disp_tranfs[$i]['item_amt'], 2, '.', ''); }}</td>
               </tr>
               <?php 
                  }
                  ?>
            </tbody>
         </table>
      </div>
      <br>
      <div class="row">
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4" style="text-align: right;font-size:15px;margin-left:-43px">
            <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ number_format((float)$tranfs_total, 2, '.', ''); }}</strong></span>
         </div>
      </div>
      <br>
      <div class="form-group">
         <div class="row">
            <div class="col-sm-12" align="center">
               <input type="button" value="Cancel" class="btn" onclick="cancel_click('transfers_box','del_tranfs')">
               {{ Form::token()}}
            </div>
         </div>
      </div>
      <br>
   </div>
</div>
{{-- Transfer Box Ends --}}
{{-- Delivery and Transfers Box Starts--}}
<div id="del_tranfs" style="display: none" class="mainbox col-md-4 col-md-offset-4">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">DELIVERY AND TRANSFERS &nbsp;&nbsp;&nbsp;&nbsp; {{ date('m/d/Y',strtotime($recap_date)) }}</div>
      </div>
      <div class="panel-body">
         <form id="" class="form-horizontal" method="post" role="form" style="display: block;">
            <br>
            <div class="form-group">
               <div class="col-sm-1">&nbsp;</div>
               <label for="inputPassword" class="control-label col-sm-6">Transfers</label>
               <div class="col-sm-3">
                  <input type="button" tabindex="3" value="{{ number_format((float)$tranfs_total, 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('transfers_box','del_tranfs')" class="btn transferstotal" style="width:125px;text-align:right;margin-left: -42px;">
               </div>
            </div>
            <div class="form-group">
               <div class="col-sm-1">&nbsp;</div>
               <label for="inputPassword" class="control-label col-sm-6"> Deliveries  </label>
               <div class="col-sm-3">
                  <input type="button" tabindex="2" value="{{ number_format((float)$del_total, 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('delivery_box','del_tranfs')" class="btn deliveriestotal" style="width:125px;text-align:right;margin-left: -42px;">
               </div>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;">
            <div class="form-group">
               <div class="col-sm-1">&nbsp;</div>
               <label for="inputPassword" class="control-label col-sm-6" style="margin-top:-9px;">Total</label>
               <div class="col-sm-3 text-right" style="margin-left:10px;">
                  <strong><span class="delivery_transfer_total">{{ number_format((float)($del_total + $tranfs_total), 2, '.', ''); }}</span></strong>
               </div>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;">
            <div class="row">
               <div class="col-sm-3">&nbsp;</div>
               <div class="col-sm-6">
                  <input type="button" name="safecountsubmit" id="safecountsubmit" value="Submit" class="btn" onclick="cancel_click('del_tranfs','mgmtmaindivbox')">
                  <input type="button" value="Cancel" class="btn" onclick="cancel_click('del_tranfs','mgmtmaindivbox')">
               </div>
               <div class="col-sm-3">&nbsp;</div>
            </div>
         </form>
      </div>
   </div>
</div>
{{-- Delivery and Transfers Ends --}}
{{--Store Transactions Box Starts --}}  
<div id="store_tx" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <form method="post" id="potbox-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">STORE TRANSACTIONS</div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div  class="panel-body scrollbar" id="style-1">
               <?php
                  $storetx_total = 0;
                  for($i=0;$i<count($disp_storetx);$i++)
                  {
                   $storetx_total = $storetx_total + $disp_storetx[$i]['item_amt'];
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-2" style="font-size:12px;">
                        <label for="inputPassword">(&nbsp;<?php echo $disp_storetx[$i]['item_id'];
                           ?>&nbsp;)</label>
                     </div>
                     <div class="col-sm-6" style="font-size:12px;">
                        <b><?php echo $disp_storetx[$i]['item_desc']; 
                           ?></b>
                     </div>
                     <div class="col-sm-4 text-right">
                        <strong><span>{{ number_format((float)$disp_storetx[$i]['item_amt'], 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
               <?php   }
                  ?>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row" style="margin-left:31px;margin-top:-2px;">
               <div class="col-sm-6">
                  <div class="row">
                     <div class="col-sm-6">
                        <b>Total to account For</b>
                     </div>
                     <div class="col-sm-6" style="margin-left:177px;font-size:16px;text-align:right;margin-top: -18px;">
                        <strong><span class="po_types_total">{{ number_format((float)$storetx_total, 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php /*if($file_locked == "Y"){ echo 'disabled'; }*/ ?> name="login-submit" id="potbox_submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('store_tx','mgmtmaindivbox')"/>
               </div>
            </div>
         </div>
         <br>
         {{-- if($file_locked == "Y") --}}
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         {{--  @endif --}}
         <br>  
      </div>
   </form>
</div>
{{--Store Transactions Ends --}}
{{--Paid Out Types Box Starts --}}  
<div id="po_types" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <form method="post" id="potbox-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">PAID OUT TYPES   </div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div  class="panel-body scrollbar" id="style-1">
               <?php
                  $potypes_total = 0;
                  for($i=0;$i<count($disp_potypes);$i++)
                  {
                   $potypes_total = $potypes_total + $disp_potypes[$i]['item_amt'];
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-2" style="font-size:12px;">
                        <label for="inputPassword">(&nbsp;<?php echo $disp_potypes[$i]['item_id'];
                           ?>&nbsp;)</label>
                     </div>
                     <div class="col-sm-6" style="font-size:12px;">
                        <b><?php echo $disp_potypes[$i]['item_desc']; 
                           ?></b>
                     </div>
                     <div class="col-sm-4 text-right">
                        <strong><span>{{ number_format((float)$disp_potypes[$i]['item_amt'], 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
               <?php   }
                  ?>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row" style="margin-left:31px;margin-top:-2px;">
               <div class="col-sm-6">
                  <div class="row">
                     <div class="col-sm-6">
                        <b>Total to account For</b>
                     </div>
                     <div class="col-sm-6" style="margin-left:177px;font-size:16px;text-align:right;margin-top: -18px;">
                        <strong><span class="po_types_total">{{ number_format((float)$potypes_total, 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php /*if($file_locked == "Y"){ echo 'disabled'; }*/ ?> name="login-submit" id="potbox_submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('po_types','mgmtmaindivbox')"/>
               </div>
            </div>
         </div>
         <br>
         {{-- if($file_locked == "Y") --}}
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         {{--  @endif --}}
         <br>  
      </div>
   </form>
</div>
{{-- Paid Out Types Ends --}} 
{{-- Currency Deposits Box Starts --}}  
<div id="curr_dep_box" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">
            <div class="row">
               <div class="col-md-6">CURRENCY  </div>
               <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
            </div>
         </div>
      </div>
      <div class="table-responsive ">
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="text-center">Day</th>
                  <th class="text-center">Date</th>
                  <th class="text-center">ID</th>
                  <th class="text-center">Description</th>
                  <th class="text-center">Amount</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  //$disp_currency;
                   $currency_total = 0;
                       for($i=0;$i<count($disp_currency);$i++)
                       {
                        $currency_total = $currency_total + $disp_currency[$i]['item_amt'];
                        $weekday = date('l', strtotime($disp_currency[$i]['wk_end_date']));
                          $date = date("m/d/Y", strtotime($disp_currency[$i]['wk_end_date']));
                  ?>
               <tr>
                  <td class="text-center">{{ $weekday }}</td>
                  <td class="text-center">{{ $date }}</td>
                  <td class="text-center">{{ $disp_currency[$i]['item_id'] }}</td>
                  <td class="text-center">{{ $disp_currency[$i]['item_desc'] }}</td>
                  <td class="text-center">{{ number_format((float)$disp_currency[$i]['item_amt'], 2, '.', ''); }}</td>
               </tr>
               <?php 
                  }
                  ?>
            </tbody>
         </table>
      </div>
      <br>
      <div class="row">
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4" style="text-align: right;font-size:15px;margin-left:-43px">
            <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ number_format((float)$currency_total, 2, '.', ''); }}</strong></span>
         </div>
      </div>
      <br>
      <div class="form-group">
         <div class="row">
            <div class="col-sm-12" align="center">
               <input type="button" value="Cancel" class="btn" onclick="cancel_click('curr_dep_box','depositsbox')">
               {{ Form::token()}}
            </div>
         </div>
      </div>
      <br>
   </div>
</div>
{{-- Currency Deposits Ends --}}
{{-- Currency Deposits Box Starts --}}  
<div id="checks_dep_box" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">
            <div class="row">
               <div class="col-md-6">CHECKS  </div>
               <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
            </div>
         </div>
      </div>
      <div class="table-responsive ">
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="text-center">Day</th>
                  <th class="text-center">Date</th>
                  <th class="text-center">ID</th>
                  <th class="text-center">Description</th>
                  <th class="text-center">Amount</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  //$disp_currency;
                   $checks_total = 0;
                       for($i=0;$i<count($disp_checks);$i++)
                       {
                        $checks_total = $checks_total + $disp_checks[$i]['item_amt'];
                        $weekday = date('l', strtotime($disp_checks[$i]['wk_end_date']));
                          $date = date("m/d/Y", strtotime($disp_checks[$i]['wk_end_date']));
                  ?>
               <tr>
                  <td class="text-center">{{ $weekday }}</td>
                  <td class="text-center">{{ $date }}</td>
                  <td class="text-center">{{ $disp_checks[$i]['item_id'] }}</td>
                  <td class="text-center">{{ $disp_checks[$i]['item_desc'] }}</td>
                  <td class="text-center">{{ number_format((float)$disp_checks[$i]['item_amt'], 2, '.', ''); }}</td>
               </tr>
               <?php 
                  }
                  ?>
            </tbody>
         </table>
      </div>
      <br>
      <div class="row">
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4" style="text-align: right;font-size:15px;margin-left:-43px">
            <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ number_format((float)$checks_total, 2, '.', ''); }}</strong></span>
         </div>
      </div>
      <br>
      <div class="form-group">
         <div class="row">
            <div class="col-sm-12" align="center">
               <input type="button" value="Cancel" class="btn" onclick="cancel_click('checks_dep_box','depositsbox')">
               {{ Form::token()}}
            </div>
         </div>
      </div>
      <br>
   </div>
</div>
{{-- Currency Deposits Ends --}}
{{-- Express Debit Box Starts --}}  
<div id="visa_deposits_dep_box" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">
            <div class="row">
               <div class="col-md-6">EXPRESS CREDIT  </div>
               <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
            </div>
         </div>
      </div>
      <div class="table-responsive ">
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="text-center">Day</th>
                  <th class="text-center">Date</th>
                  <th class="text-center">ID</th>
                  <th class="text-center">Description</th>
                  <th class="text-center">Amount</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  //$disp_currency;
                   $express_cred_total = 0;
                       for($i=0;$i<count($disp_express_cred);$i++)
                       {
                        $express_cred_total = $express_cred_total + $disp_express_cred[$i]['item_amt'];
                        $weekday = date('l', strtotime($disp_express_cred[$i]['wk_end_date']));
                          $date = date("m/d/Y", strtotime($disp_express_cred[$i]['wk_end_date']));
                  ?>
               <tr>
                  <td class="text-center">{{ $weekday }}</td>
                  <td class="text-center">{{ $date }}</td>
                  <td class="text-center">{{ $disp_express_cred[$i]['item_id'] }}</td>
                  <td class="text-center">{{ $disp_express_cred[$i]['item_desc'] }}</td>
                  <td class="text-center">{{ number_format((float)$disp_express_cred[$i]['item_amt'], 2, '.', ''); }}</td>
               </tr>
               <?php 
                  }
                  ?>
            </tbody>
         </table>
      </div>
      <br>
      <div class="row">
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4" style="text-align: right;font-size:15px;margin-left:-43px">
            <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ number_format((float)$express_cred_total, 2, '.', ''); }}</strong></span>
         </div>
      </div>
      <br>
      <div class="form-group">
         <div class="row">
            <div class="col-sm-12" align="center">
               <input type="button" value="Cancel" class="btn" onclick="cancel_click('visa_deposits_dep_box','depositsbox')">
            </div>
         </div>
      </div>
      <br>
   </div>
</div>
{{-- Express Debit Ends --}}
{{-- Visa Deposits Box Starts --}}  
<div id="express_debit_dep_box" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">
            <div class="row">
               <div class="col-md-6">EXPRESS DEBIT  </div>
               <div class="col-md-6">{{ date('m/d/Y',strtotime($recap_date)) }}</div>
            </div>
         </div>
      </div>
      <div class="table-responsive ">
         <table class="table table-striped">
            <thead>
               <tr>
                  <th class="text-center">Day</th>
                  <th class="text-center">Date</th>
                  <th class="text-center">ID</th>
                  <th class="text-center">Description</th>
                  <th class="text-center">Amount</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  //$disp_currency;
                   $express_deb_total = 0;
                       for($i=0;$i<count($disp_express_deb);$i++)
                       {
                        $express_deb_total = $express_deb_total + $disp_express_deb[$i]['item_amt'];
                        $weekday = date('l', strtotime($disp_express_deb[$i]['wk_end_date']));
                          $date = date("m/d/Y", strtotime($disp_express_deb[$i]['wk_end_date']));
                  ?>
               <tr>
                  <td class="text-center">{{ $weekday }}</td>
                  <td class="text-center">{{ $date }}</td>
                  <td class="text-center">{{ $disp_express_deb[$i]['item_id'] }}</td>
                  <td class="text-center">{{ $disp_express_deb[$i]['item_desc'] }}</td>
                  <td class="text-center">{{ number_format((float)$disp_express_deb[$i]['item_amt'], 2, '.', ''); }}</td>
               </tr>
               <?php 
                  }
                  ?>
            </tbody>
         </table>
      </div>
      <br>
      <div class="row">
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4">&nbsp;</div>
         <div class="col-sm-4" style="text-align: right;font-size:15px;margin-left:-43px">
            <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ number_format((float)$express_deb_total, 2, '.', ''); }}</strong></span>
         </div>
      </div>
      <br>
      <div class="form-group">
         <div class="row">
            <div class="col-sm-12" align="center">
               <input type="button" value="Cancel" class="btn" onclick="cancel_click('express_debit_dep_box','depositsbox')">
            </div>
         </div>
      </div>
      <br>
   </div>
</div>
{{-- Visa Deposits Ends --}}
@stop
<style type="text/css">
   /* #coinbox{
   display:none;
   }*/
</style>
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ asset("assets/plugins/vdialog/css/vdialog.css") }}" />
<script src="http://code.jquery.com/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/vdialog/js/lib/vdialog.js") }}"></script>
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script> --}}
<script type="text/javascript">
   function hide_show_toggle_safecnt(pop_div,main_div)
   {
     $('#'+pop_div).show();
     $('#'+main_div).hide();
   }
   function cancel_click(pop_div,main_div)
   {
     $('#'+pop_div).hide();
     $('#'+main_div).show();   
   }  
   
   
      $(document).ready(function() {
   
         //$(".mgmtapproval").click(function(){
            $('.mgmtapproval').on('click', function() {
               var recap_date = '{{ $recap_date }}';
               $('.mgmtapprovalmsg').hide();
   
               $.post("checkmgmtweeklyapprovalprint",{recap_date:recap_date} ,function(data1) {
   
                     //alert(data1);return false;
                     if(data1 == 0)
                     {
                        $('.mgmtapprovalmsg').show();
                        var msg = 'Please print the recap reports first.';
                        $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+msg+"</div>");
                     }
                     else
                     {
                         vdialog.confirm('Are You Sure', function(){
              //alert('Confirm');
              $('.mgmtapprovalmsg').show();
              
                  $.post("checkmgmtweeklyapproval",{recap_date:recap_date} ,function(data) {
                     //alert(data);return false;
                     $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data+"</div>");
                     //cancel_click('transfersbox','dandtbox');
         
                  });
            });
                     }
                     
                     //cancel_click('transfersbox','dandtbox');
                     //return false;
                  });
   
   
   
             
            });  
             
        // });
           
   
          
   
           
   
   
          
         
      $("input.form-control").keydown(function (e) {
           // Allow: backspace, delete, tab, escape, enter and .
           if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
               (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
               (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
               (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
               (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
           }
           // Ensure that it is a number and stop the keypress
           if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
               e.preventDefault();
           }
       });
      
      });
      
      
   
</script>