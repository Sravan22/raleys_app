@extends('layout.dashboard')

@section('page_heading','Receivings')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.recivermenu')
    </header>
<style> 
  @media print {
  a[href]:after {
    content: none !important;
  }
}
</style>
@if($storeinv)
 <div class="container">
<div class="row">
<div class="col-md-6">
<h3>Store invoice</h3></div>
<div class="col-md-6">
<span class="pull-right">
<a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
<a href="{{ route('pdf-storeinvoice',['invno'=>$invno])}}" target="_blank">
<i class="fa fa-file-pdf-o fa-fw iconsize"></i>
</a>
</span>
</div>
</div>
</div>
@endif
  <div class="container">
  @if($storeinv)
    <table class="table table-striped">
        <thead>
          <tr>
            <th>UPC No</th>
            <th>Case Pack</th>
            <th>Vendor Qty</th>
            <th>Vendor Cost</th>
            <th>Vendor Allowance</th>
            <th>Vendor CRV</th>
            <th>Store Qty</th>
            <th>Store Cost</th>
            <th>Store Allowance</th>
            <th>Store CRV</th>
            <th>Free Qty</th>
            <th>Delivery Unit</th>
            <th>Note</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($storeinv as $sir)
          <tr>
            <td>{{ $sir['upc_number'] }}</td>
            <td>{{ $sir['case_pack'] }}</td>
            <td>{{ $sir['vendor_qty'] }}</td>
            <td>{{ $sir['vendor_cost'] }}</td>
            <td>{{ $sir['vendor_allow'] }}</td>
            <td>{{ $sir['vendor_crv'] }}</td>
            <td>{{ $sir['store_qty'] }}</td>
            <td>{{ $sir['store_cost'] }}</td>
            <td>{{ $sir['store_allow'] }}</td>
            <td>{{ $sir['store_crv'] }}</td>
            <td>{{ $sir['free_qty'] }}</td>
            <td>{{ $sir['delvry_unt_ovrd'] }}</td>
            <td>{{ $sir['spec_note_cd'] }}</td>
            <td>{{ $sir['active_sw'] }}</td>
          </tr>
       @endforeach
        </tbody>
    </table>
    @else
      <div class="alert alert-danger">
        <strong>Alert!</strong> No Store invoice meet Query criteria.
      </div>
    @endif          
    <?php //echo $cdreports->links(); ?>
</div>
@stop
