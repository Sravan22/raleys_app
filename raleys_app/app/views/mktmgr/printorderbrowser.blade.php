<?php
    //echo $order_date;
    //echo '<pre>';print_r($print_order_result_array);
           //exit; 
?>
@extends('layout.dashboard')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6">
         <h3>Browse</h3>
      </div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-storeorderquery',['download'=>'pdf','dept_number'=>$dept_number,
             'order_date'=>$order_date])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
<div class="container">
@if ($print_order_result_array)
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Dept No</th>
            <th>Order Date</th>
            <th>Type</th>
            <th>Last Updated</th>
         </tr>
        </thead> 
        <tbody>
         @for ($i = 0; $i < count($print_order_result_array); $i++)   
         <tr>
          
            
            <?php 
            $seq_number = DB::select('SELECT * FROM sotrnhdr WHERE seq_number = "'.$print_order_result_array[$i]['seq_number'].'" ' );
            $seq_number_array = json_decode(json_encode($seq_number), true);
            //echo '<pre>'; print_r($seq_number_array);//exit;
            $description = DB::select('select * from sotrncfg where type_code= "'.$seq_number_array[0]['type_code'].'" and subtype_code = "'.$seq_number_array[0]['subtype_code'].'" ' );
            $description_array = json_decode(json_encode($description), true);
          // echo '<pre>'; print_r($description_array);exit;
              $params = array(
                        'order_date' =>  date('m/d/Y',strtotime($print_order_result_array[$i]['order_date'])),
                        'seq_number' =>  $print_order_result_array[$i]['seq_number'],
                        'description' => $description_array[0]['description'],
                        'status_code' => $seq_number_array[0]['status_code']
                        ); 
              //echo '<pre>';print_r($params);exit;
              $queryString = http_build_query($params);
            $order_date = date('m/d/Y',strtotime($print_order_result_array[$i]['order_date']));  ?>
          <td>{{ $print_order_result_array[$i]['dept_number'] }}</td>
          <td><u>{{ HTML::link(URL::route('mktmgr-printorderdetails', $queryString  ) , $order_date) }}</u></td>
            
            <td>{{ $description_array[0]['description'] }}</td>
            <td>{{ $print_order_result_array[$i]['last_update'] }} </td>
         </tr>
        @endfor

        @else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Store Transfers meet Query criteria.
            </div>
        @endif
               
      </tbody>
   </table>

</div>
<div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
    <input type="button" class="btn" name="" onclick="history.go(-1);" value="Back">        
 
   {{ Form::token()}}
        </div>
        </div>
  </div>
@stop