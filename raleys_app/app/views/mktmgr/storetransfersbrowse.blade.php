<?php
    //echo '<pre>';print_r($report_data);exit; 
   ?>
<style type="text/css">
@media  print {
 a[href]:after {
 content: "" !important; 
 }
}
</style>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
 <?php 
   $params = array(
             'download' =>  'pdf',
             'from_store_no' => $report_data['from_store_no'],
             'from_dept_no' => $report_data['from_dept_no'],
             'from_account' => $report_data['from_account'],
             'to_store_no' => $report_data['to_store_no'],
             'to_dept_no' => $report_data['to_dept_no'],
             'to_account' => $report_data['to_account'],
             'create_date' => $report_data['create_date'],
             'transfer_number' => $report_data['transfer_number'],
             'transfer_type' => $report_data['transfer_type'],
             'employee_id' => $report_data['employee_id'],
             'status' => $report_data['status']
             ); 
             $queryString = http_build_query($params);
   ?> 
<header class="row">
   @include('mktmgr.storetransfersmenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div class="row">
      <div class="col-md-6">
         <h3>Browse</h3>
      </div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
         {{-- <a href="{{ route('pdf-store-transfer-hardcopy-report',$queryString) }}" target="_blank"> --}}
         {{-- <i class="fa fa-file-pdf-o fa-fw iconsize"></i></a> --}}
         </span>
      </div>
   </div>
</div>
<div class="container">
   <!-- <a class="dt-button buttons-print" tabindex="0" aria-controls="example" href="#"><span>
      Print</span></a> -->
      @if ($data)
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Transfer Date</th>
            <th>Transfer Number</th>
            <th>Emp ID</th>
            <th>Type</th>
            <th>From Store/Dept</th>
            <th>To Store/Dept</th>
            <th>Total Lns</th>
            <th style="padding-left: 20px;">Total Value</th>
            <th>Status</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($data as $store_result)
         <tr>
            <?php 
            $params = array(
                       'seq_number' =>  $store_result->seq_number,
                      'transfer_number' =>  $store_result->transfer_number,
                       'from_store_no' => $store_result->from_store_no,
                      'from_dept_no' => $store_result->from_dept_no,
                     'to_store_no' => $store_result->to_store_no,
                     'to_dept_no' => $store_result->to_dept_no,
                      'from_account' => $store_result->from_account,
                     'from_account_desc' => $store_result->from_account_desc,
                     'to_account' => $store_result->to_account,
                    'to_account_desc' => $store_result->to_account_desc,
                     'tot_value' => $store_result->tot_value
                      ); 
            $queryString = http_build_query($params);
           ?>
            <td>{{ date("m/d/Y", strtotime($store_result->create_date));  }}</td>
           
            <td><u>{{ HTML::link(URL::route('mktmgr-store-transfer-update',['seq_number' => $store_result->seq_number ] ) , $store_result->from_store_no) }}</u> - <u>{{ HTML::link(URL::route('mktmgr-store-item-no',$queryString ) , $store_result->transfer_number) }}</u></td>
           
            <td>{{ sprintf('%06d', $store_result->employee_id) }}</td>
            <td>{{ $store_result->transfer_type }}</td> 
            <td>{{ $store_result->from_store_no }} / {{ sprintf("%02d", $store_result->from_dept_no); }}</td>
            <td>{{ $store_result->to_store_no }} / {{ sprintf("%02d", $store_result->to_dept_no); }}</td>
            <td>{{ $store_result->tot_line_items }}</td>
            <td style="text-align: right;padding-right: 35px;">${{ $store_result->tot_value }}</td>
            <td>{{ $store_result->status }}</td>
         </tr>
         @endforeach
         {{--  @endforeach --}}
      </tbody>
   </table>
   {{ $pagination->links(); }}
   @else
   <div class="alert alert-danger">
      <strong>Alert!</strong> No Store Transfers meet Query criteria.
   </div>
   @endif

<div class="col-sm-12" align="center">
<input type="button" value="Cancel" class="btn" id="cancel" name="cancel" onclick="goBack()">
<input name="_token" type="hidden" value="4zM6GBMJYXBbeWlMmpncj1SpgOgxmPNc2xu1lcZG">
</div>
</div>

<script>
function goBack() {
    window.history.back();
}
</script>
@stop

