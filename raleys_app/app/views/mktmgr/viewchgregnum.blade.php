

@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
    <style type="text/css">
      .text{
        text-align: right;
      }
    </style>
    <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
  <!-- <p id="menulist">
      <a href="{{ URL::route('mktmgr-receivings')}}" >Receivings</a> >> Store Expenses >>
      <a href="{{ URL::route('mktmgr-receivings-sequery')}}" >Query</a>
  </p> -->
        <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >Chg Reg </div>
                       </div> 
                 <div style="padding-top:5px" class="panel-body" >

				 <form class="form-horizontal" method="post" action="{{URL::route('mktmgr-chgreg-delete')}}" >
                <div class="form-group">
            <label for="inputPassword"  class="control-label col-sm-6">Register Number</label>
            <div class="col-sm-6">
                {{ $chg_data->reg_num }} <input type="hidden" name="reg_num" value="{{ $chg_data->reg_num }}">
            </div>
        </div> 

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-6">Registration Type</label>
            <div class="col-sm-6">
                {{ $chg_data->reg_type }}
            </div>
        </div>  

        
                  
                  <div class="form-group">
                    <div class="row">
                    <!-- <div class="col-sm-3" align="center">
                    &nbsp;&nbsp;
                    </div> -->
                    <!-- <div class="col-sm-3" align="center">
                    &nbsp;&nbsp;
                    </div> -->
                       <div class="col-sm-12" align="center">
                      {{--  {{ HTML::link(URL::route('mktmgr-update-chgreg',['reg_num' => $chg_data->reg_num]), 'Update', array('class'=>'btn')) }} --}}
                       <input type="button" onclick="window.location.href='{{ URL::route('mktmgr-update-chgreg',['reg_num' => $chg_data->reg_num]) }}'" value="Update" class="btn"/>
                       <input type="button" onclick="history.go(-1)" value="Cancel" class="btn"/>
                       <input type="submit" name="chg-delete" id="submit" tabindex="4" value="Delete" class="btn"/>
                      </div>
                      

                      <div class="col-sm-3" >
                       <!-- {{ HTML::link(URL::route('mktmgr-update-chgreg',['reg_num' => $chg_data->reg_num]), 'Delete', array('class'=>'btn')) }} -->
                       <!-- <input type="submit" Onclick="ConfirmDelete()" value="Delete"> -->
                        
                         
                                                {{ Form::token()}}
                      </div>
                    </div>
                  </div>
                
</form>				
              
          </div>
        </div>
      </div>
</div>
@stop
<script type="text/javascript">
  function ConfirmDelete()
{
  var x = confirm("Are you sure you want to delete?");
  if (x)
      //return true;
    // route("admin.stocks.edit", ":id") }}
    alert("Deleted");
  else
    return false;
}

</script>