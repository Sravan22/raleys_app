@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<style type="text/css">
    .makecenter
    {
        margin-left:20px;
    }
    .row.vertical-divider > div[class^="col-"]
    {
      text-align: left;
      padding: 2px;
    }
</style>
<form class="form-horizontal" role="form" method="post" action="{{URL::route('mktmgr-updatebookregcheckoutitem')}}">
  <div class="container" style="">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
        <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >REGISTER CHECK OUT</div>
                       </div> 

                 <div style="padding-top:15px" class="panel-body" >
                  
<div class="row horizantal-divider">
  <div class="col-xs-6">    
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">DATE</label>
     <div class="col-sm-4" style="text-align: left;margin-left: -48px;">
     <input class="form-control col-sm-6" type="text" tabindex="20" name="selecteddate" id="selecteddate" value="{{ $dateofinfo }}" 
            readonly="readonly">
    </div>
  </div>
  </div>
 
 <!-- <div class="col-lg-12"> -->

  

 <div class="col-xs-6">
  <div class="form-horizontal">

        <div class="form-group">
          <label for="#" class="col-sm-2 control-label">CATEGORY</label>
          <div class="col-sm-2" style="padding-left:0px;">
            <input type="text" class="form-control col-sm-2" tabindex="21"  name="selecteditemsid" id="selecteditemsid" value="{{ $itemid }}" readonly="readonly">
          </div>
           <div class="col-sm-8" style="text-align:right;margin-left: -48px;">  
           <input type="text" class="form-control col-sm-4" tabindex="22" name="selecteditemsname" id="selecteditemsname" value="{{ $itemname }}" readonly="readonly">
          </div>
         </div>

    
  </div>
 </div>
 <div class="col-xs-12"><hr></div>

    <div class="col-sm-4">
        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Rg/Chkr</label>
          <input class="form-control" id="rg1" name="rg1" type="text" readonly="readonly" tabindex="23" value="1">
          <input class="form-control" id="rg4" name="rg4" type="text" readonly="readonly" tabindex="24" value="4">
          <input class="form-control" id="rg7" name="rg7" type="text" readonly="readonly" tabindex="25" value="7">
          <input class="form-control" id="rg12" name="rg12" type="text" readonly="readonly" tabindex="26" value="12">
          <input class="form-control" id="rg15" name="rg15" type="text" readonly="readonly" tabindex="27" value="15">
          <input class="form-control" id="rg30" name="rg30" type="text" readonly="readonly" tabindex="28" value="30">
        </div>

        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Amount</label>
          <input class="form-control calall" id="amt1" name="amt1" type="number" tabindex="1" step="0.01">
          <input class="form-control calall" id="amt4" name="amt4" type="number" tabindex="2" step="0.01">
          <input class="form-control calall" id="amt7" name="amt7" type="number" tabindex="3" step="0.01">
          <input class="form-control calall" id="amt12" name="amt12" type="number" tabindex="4" step="0.01">
          <input class="form-control calall" id="amt15" name="amt15" type="number" tabindex="5" step="0.01">
          <input class="form-control calall" id="amt30" name="amt30" type="number" tabindex="6" step="0.01">
        </div>
    </div>
    <div class="col-sm-4">
          <div class="col-xs-6">
          <label for="ex1" class="makecenter">Rg/Chkr</label>
          <input class="form-control" id="rg2" name="rg2" type="text" readonly="readonly" tabindex="29" value="2">
          <input class="form-control" id="rg5" name="rg5" type="text" readonly="readonly" tabindex="30" value="5">
          <input class="form-control" id="rg8" name="rg8" type="text" readonly="readonly" tabindex="31" value="8">
          <input class="form-control" id="rg13" name="rg13" type="text" readonly="readonly" tabindex="32" value="13">
          <input class="form-control" id="rg17" name="rg17" type="text" readonly="readonly" tabindex="33" value="17">
           <label for="ex1" class="makecenter">Grand Total</label>
        </div>

        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Amount</label>
          <input class="form-control calall" id="amt2" name="amt2" type="number" tabindex="7" step="0.01">
          <input class="form-control calall" id="amt5" name="amt5" type="number" tabindex="8" step="0.01">
          <input class="form-control calall" id="amt8" name="amt8" type="number" tabindex="9" step="0.01">
          <input class="form-control calall" id="amt13" name="amt13" type="number" tabindex="10" step="0.01">
          <input class="form-control calall" id="amt17" name="amt17" type="number" tabindex="11" step="0.01">
          <input type="number" tabindex="17" class="form-control" id="gtot" name="gtot" placeholder="Grand Total" readonly="readonly">
        </div>
    </div>
   
    
    <div class="col-sm-4">
        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Rg/Chkr</label>
          <input class="form-control" id="rg3" name="rg3" type="text" readonly="readonly" tabindex="34" value="3">
          <input class="form-control" id="rg6" name="rg6" type="text" readonly="readonly" tabindex="35" value="6">
          <input class="form-control" id="rg9" name="rg9" type="text" readonly="readonly" tabindex="36" value="9">
          <input class="form-control" id="rg14" name="rg14" type="text" readonly="readonly" tabindex="37" value="14">
          <input class="form-control" id="rg29" name="rg29" type="text" readonly="readonly" tabindex="38" value="29">
        </div>

        <div class="col-xs-6">
          <label for="ex1" class="makecenter">Amount</label>
          <input class="form-control calall" id="amt3" name="amt3" type="number" tabindex="12" step="0.01">
          <input class="form-control calall" id="amt6" name="amt6" type="number" tabindex="13" step="0.01">
          <input class="form-control calall" id="amt9" name="amt9" type="number" tabindex="14" step="0.01">
          <input class="form-control calall" id="amt14" name="amt14" type="number" tabindex="15" step="0.01">
          <input class="form-control calall" id="amt29" name="amt29" type="number" tabindex="16" step="0.01">
        </div>
    </div>
  </div>
  
    </div>
  @if($dayIsLocked != 'Y')
  
          <div class="form-group">
                    <div class="row">
                       <div class="col-sm-12 topspace" align="center">
        <input type="submit" name="login-submit" id="submit" tabindex="18" value="Submit" class="btn">
        <input type="reset" name="login-submit" id="cancel" tabindex="19" value="Cancel" class="btn"  onclick="goBack()">
                                                {{ Form::token()}}
                      </div>
                    </div>
                  </div>
  @endif
</div>


 </div>
        </div>  </div>
      </div>
  </form>  
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">

 $(document).ready(function() {
  $(".calall").val("00.00");
  });

$(".calall").blur(function(){ 
  selval=$("#"+this.id).val();
  if(selval == ''){$("#"+selval).val("00.00");}
  else{updatedval=parseFloat($("#"+this.id).val(),10).toFixed(2); $("#"+this.id).val(updatedval);}
  amt1=parseFloat($("#amt1").val(),10).toFixed(2);
  amt4=parseFloat($("#amt4").val(),10).toFixed(2);
  amt7=parseFloat($("#amt7").val(),10).toFixed(2);
  amt12=parseFloat($("#amt12").val(),10).toFixed(2);
  amt15=parseFloat($("#amt15").val(),10).toFixed(2);
  amt30=parseFloat($("#amt30").val(),10).toFixed(2);

  amt2=parseFloat($("#amt2").val(),10).toFixed(2);
  amt5=parseFloat($("#amt5").val(),10).toFixed(2);
  amt8=parseFloat($("#amt8").val(),10).toFixed(2);
  amt13=parseFloat($("#amt13").val(),10).toFixed(2);
  amt17=parseFloat($("#amt17").val(),10).toFixed(2);
  
  amt3=parseFloat($("#amt3").val(),10).toFixed(2);
  amt6=parseFloat($("#amt6").val(),10).toFixed(2);
  amt9=parseFloat($("#amt9").val(),10).toFixed(2);
  amt14=parseFloat($("#amt14").val(),10).toFixed(2);
  amt29=parseFloat($("#amt29").val(),10).toFixed(2);
  
  sumall= parseFloat(amt1)+parseFloat(amt4)+parseFloat(amt7)+parseFloat(amt12)+parseFloat(amt15)+parseFloat(amt30)+parseFloat(amt2)+parseFloat(amt5)+parseFloat(amt8)+parseFloat(amt13)+parseFloat(amt17)+parseFloat(amt3)+parseFloat(amt6)+parseFloat(amt9)+parseFloat(amt14)+parseFloat(amt29); 
  $("#gtot").val(parseFloat(sumall).toFixed(2));
});  
</script>
 <script>
function goBack() {
    window.history.back();
}
</script>  
@stop
