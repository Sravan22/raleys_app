<?php 
   //echo '<pre>',print_r($resultsArray3);exit();
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
   
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
            {{--  <a href="" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a> --}}
         </span>
      </div>
   </div>
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="panel">
         <div class="" style="margin-left: 78px;margin-bottom:-23px;font-weight:bold;">
            <div class="panel-title">{{  date("m/d/Y", strtotime($txDate)) }}</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Daily Gas Report</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
        
            <table class="table table-striped">
               <thead>
                  <th class="text-center">&nbsp;</th>
                  <th class="text-center">Unleaded</th>
                  <th class="text-center">Premium</th>
                  <th class="text-center">Mid-Grade</th>
                  <th class="text-center">Diesel</th>
               </thead>
               <tbody>
                  <tr>
                     <td>Beg Inventory (Gal.)</td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[0]['beg_act_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[1]['beg_act_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[2]['beg_act_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[3]['beg_act_inv'], 2, '.', ','); ?></td>
                  </tr>
                  <tr>
                     <td>Prev Ave Cost/Gal.</td>
                     <td class="text-center"><?php echo $resultsArray3[0]['prv_gallon_cost']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[1]['prv_gallon_cost']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[2]['prv_gallon_cost']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[3]['prv_gallon_cost']; ?></td>
                  </tr>
                  <tr>
                     <td>Deliveries (Gal.)</td>
                     <td class="text-center"><?php if(!empty($resultsArray3[0]['tdy_delivery'])){ echo number_format((float)$resultsArray3[0]['tdy_delivery'], 2, '.', ',');  } else { echo '0.00'; } ?></td>
                     <td class="text-center"><?php if(!empty($resultsArray3[1]['tdy_delivery'])){ echo number_format((float)$resultsArray3[1]['tdy_delivery'], 2, '.', ',');  } else { echo '0.00'; } ?></td>
                     <td class="text-center"><?php if(!empty($resultsArray3[2]['tdy_delivery'])){ echo number_format((float)$resultsArray3[2]['tdy_delivery'], 2, '.', ',');  } else { echo '0.00'; } ?></td>
                     <td class="text-center"><?php if(!empty($resultsArray3[3]['tdy_delivery'])){ echo number_format((float)$resultsArray3[3]['tdy_delivery'], 2, '.', ',');  } else { echo '0.00'; } ?></td>
                  </tr>
                  <tr>
                     <td>Price/Gallon</td>
                     <td class="text-center"><?php echo (float)$resultsArray3[0]['tdy_price']; ?></td>
                     <td class="text-center"><?php echo (float)$resultsArray3[1]['tdy_price']; ?></td>
                     <td class="text-center"><?php echo (float)$resultsArray3[2]['tdy_price']; ?></td>
                     <td class="text-center"><?php echo (float)$resultsArray3[3]['tdy_price']; ?></td>
                  </tr>
                  <tr>
                     <td>New Ave Cost/Gallon</td>
                     <td class="text-center"><?php echo $resultsArray3[0]['tdy_gallon_cost']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[1]['tdy_gallon_cost']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[2]['tdy_gallon_cost']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[3]['tdy_gallon_cost']; ?></td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td>Sales (Gallons)</td>
                     <td class="text-center"><?php echo $resultsArray3[0]['tdy_sales']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[1]['tdy_sales']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[2]['tdy_sales']; ?></td>
                     <td class="text-center"><?php echo $resultsArray3[3]['tdy_sales']; ?></td>
                  </tr>
                  <tr>
                     <td>Cost of Sales</td>
                     <td class="text-center"><?php if(!empty($resultsArray3[0]['cost_of_sales'])) { echo round($resultsArray3[0]['cost_of_sales'],2); } else { echo '0.00'; } ?></td>
                     <td class="text-center"><?php if(!empty($resultsArray3[0]['cost_of_sales'])) { echo round($resultsArray3[1]['cost_of_sales'],2); } else { echo '0.00'; } ?></td>
                     <td class="text-center"><?php if(!empty($resultsArray3[0]['cost_of_sales'])) { echo round($resultsArray3[2]['cost_of_sales'],2); } else { echo '0.00'; } ?></td>
                     <td class="text-center"><?php if(!empty($resultsArray3[0]['cost_of_sales'])) { echo round($resultsArray3[3]['cost_of_sales'],2); } else { echo '0.00'; } ?></td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>

                  <tr>
                     <td>Calc End Inv (Gal.)</td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[0]['calc_ending_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[1]['calc_ending_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[2]['calc_ending_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[3]['calc_ending_inv'], 2, '.', ','); ?></td>
                  </tr>
                  <tr>
                     <td>Actl End Inv (Gal.)</td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[0]['tdy_act_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[1]['tdy_act_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[2]['tdy_act_inv'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[3]['tdy_act_inv'], 2, '.', ','); ?></td>
                  </tr>
                  @if($msg)
                  <tr>
                  
                    <td colspan="5">
                        <div>
                     @if($msg)
                     <div class="alert  alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        {{ $msg  }}                               
                     </div>
                     @endif
                  </div>
                    </td>
                  </tr>
                  @endif
                  <tr>
                     <td>Shrink (Gallons)</td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[0]['shrink'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[1]['shrink'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[2]['shrink'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[3]['shrink'], 2, '.', ','); ?></td>
                  </tr>
                  <tr>
                     <td>Shrink Cost</td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[0]['shrink_cost'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[1]['shrink_cost'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[2]['shrink_cost'], 2, '.', ','); ?></td>
                     <td class="text-center"><?php echo number_format((float)$resultsArray3[3]['shrink_cost'], 2, '.', ','); ?></td>
                  </tr>
                  <tr>
                     <td>Storage Fee</td>
                     <td class="text-center"><?php echo round($resultsArray3[0]['storage_fee'],2); ?></td>
                     <td class="text-center"><?php echo round($resultsArray3[1]['storage_fee'],2); ?></td>
                     <td class="text-center"><?php echo round($resultsArray3[2]['storage_fee'],2); ?></td>
                     <td class="text-center"><?php echo round($resultsArray3[3]['storage_fee'],2); ?></td>
                  </tr>
               </tbody>
            </table>
            
         </div>
      </div>
   </div>
</div>
@stop