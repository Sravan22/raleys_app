<?php
//echo count($reportquery_array)."<br>";
//echo '<pre>';print_r($reportquery_array);exit;  
?>
<style type="text/css">
   @media  print {
     a[href]:after {
     content: "" !important; 
     }
   }
</style>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
 @include('mktmgr.storetransfersmenu')
</header>
<div class="container">
<div class="row">
<div class="col-md-6">
<h3>Store Transfers Accounting Recap Report</h3></div>
<div class="col-md-6">
@if($reportquery_array)
<span class="pull-right">
<a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
<a href="{{ route('pdf-poststoreweeklyreport',['download'=>'pdf' , 'start_datec'=>$start_date, 'end_datec'=>$end_date]) }}" target="_blank">
<i class="fa fa-file-pdf-o fa-fw iconsize"></i>
</a>
</span>
@endif
</div>
</div>
</div>
<br />
<div class="container center">
<table class="table" style="width: 500px; margin-left: 250px;">
<tr>
<td>Date Range</td><td><b>{{ date("m/d/Y",strtotime(trim($start_date))); }}</b></td>
<td>to</td><td><b>{{ date("m/d/Y",strtotime(trim($end_date)));  }}</b></td>
</tr>
</table>
</div>

<div class="container">
@if($reportquery_array)

<table class="table table-striped" id="example">
<thead>
<tr>
  <th>Transfer Date</th>
  <th>Transfer Number</th>
  <th>Xfr Type</th>
  <th>From Store/Dept</th>
  <th>From Account</th>
  <th>To Store/Dept</th>
  <th>To Account</th>
  <th>Transfer Amount</th>
</tr>
</thead>
<tbody>
@foreach ($reportquery_array as $store_result)
<tr>
  <td>{{ date("m-d-Y",strtotime(trim($store_result['create_date']))); }}</td>
  <td>{{ $store_result['from_store_no']  }}-{{ $store_result['transfer_number'] }}</td>
  <td>{{ $store_result['transfer_type'] }}</td>
  <td>{{ $store_result['from_store_no']  }} / {{ $store_result['from_dept_no']  }}</td>
  <td>{{ $store_result['from_account'] }}</td>
  <td>{{ $store_result['to_store_no']  }} / {{ $store_result['to_dept_no']  }}</td>
  <td>{{ $store_result['to_account'] }}</td>
  <td>{{ $store_result['tot_value'] }}</td>
</tr>
 @endforeach
</tbody>
</table>        
@else
  <div class="alert alert-danger">
    <strong>Alert!</strong> No Store Transfers meet Query criteria.
  </div>
@endif
</div>

<div class="col-sm-12" align="center">
<input type="button" value="Cancel" class="btn" id="cancel" name="cancel" onclick="goBack()" autofocus="">
<input name="_token" type="hidden" value="4zM6GBMJYXBbeWlMmpncj1SpgOgxmPNc2xu1lcZG">
</div>

</div>
<script>
function goBack() {
    window.history.back();
}
</script>
@stop
