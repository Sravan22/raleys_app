<?php 
   //echo '<pre>',print_r($results);exit();
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Enter Invoice Info</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{ URL::route('bookspro-post-deliveryadd_update') }}" id="booksafedate" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputEmail" class="control-label col-sm-6">Invoice Number</label>
                  <div class="col-sm-6">
                     <input type="text" name="invoice_no" maxlength="14"  class="form-control target_invoice_no" placeholder="Invoice Number" onKeyPress="return StopNonNumeric(this,event)">
                     @if($errors->has('invoice_no'))
                     {{ $errors->first('invoice_no')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Delivery Date</label>
                  <div class="col-sm-6">
                     <input type="date" class="form-control target_invoice_date" id="invoice_date" placeholder="" name="invoice_date" value="{{ $previewsdate }}"> 
                     @if($errors->has('invoice_date'))
                     {{ $errors->first('invoice_date')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="button" data-toggle="modal" data-target="#myModal"  value="Invoice List" class="btn">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title text-center"><strong> Invoices List </strong></h4>
                  </div>
                  <div class="modal-body">
                     {{--  
                     <p>Some text in the modal.</p>
                     --}}
                     <div class="panel-body">
                        <div class="table-responsive">
                           <table class="table table-striped">
                              <thead>
                                 <tr>
                                    <th class="text-center">Dlvry Date</th>
                                    <th class="text-center">Invc Number</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @foreach($invoice_no_date as $row)
                                 <tr>
                                    <td class="text-center">{{ date("m/d/Y", strtotime($row->invoice_date)) }}</td>
                                    <td class="text-center invoice_date hidden">{{ $row->invoice_date }}</td>
                                    <td class="text-center invoice_no"><span style="cursor: pointer;"><u>{{ $row->invoice_no }}</span></u></td>
                                 </tr>
                                 @endforeach
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
   .modal-body {
   max-height: calc(100vh - 210px);
   overflow-y: auto;
   }
</style>
<script type="text/javascript">
   function StopNonNumeric(el, evt)
          {
              //var r=e.which?e.which:event.keyCode;
              //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
              var charCode = (evt.which) ? evt.which : event.keyCode;
              var number = el.value.split('.');
              if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                  return false;
              }
              //just one dot (thanks ddlab)
              if(number.length>1 && charCode == 46){
                   return false;
              }
              //get the carat position
              var dotPos = el.value.indexOf(".");
              if( dotPos>-1 && (number[1].length > 3)){
                  return false;
              }
              return true;
          }
   
      $(document).ready(function(){
         //alert('Hii');
         $('.invoice_no').on('click', function(e) {
            //alert($(this).parent().find(".invoice_date").text());return false;
            $('.target_invoice_no').val($(this).text());
            $('.target_invoice_date').val($(this).parent().find(".invoice_date").text());
            //alert($(this).parent().find(".invoice_date").text());
            $('#myModal').modal('hide');
         });
      });
</script>
@stop