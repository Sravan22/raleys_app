<?php 
   //echo $file_locked;exit;
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<style type="text/css">
   #coinbox{
   display: none;
   }
   #currencybox{
   display: none;
   }
   #miscbox{
   display: none;
   }
   .menu_search_line{
   /* display: none;*/
   height: 0px;
   }
   .misc_desc{
   display: initial !important;
   } 
   ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   text-transform: initial;
   }
</style>
<div class="container">
   <div id="safecountbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >SAFE COUNT</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form  action="{{URL::route('bookspro-safecountpostdata')}}"  id="" class="form-horizontal" method="post" role="form" style="display: block;">
               <input type="hidden" name="safe_date" id="safe_date"  value="{{ $safe_date }}" /> 
               <div class="form-group">
                  <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Coin Total</label>
                  <div class="col-sm-4">
                     <input  type="button" tabindex="1" autofocus="" value="{{ number_format((float)$coin_tot, 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('coinbox','safecountbox')" class="btn safecountcointotal" id="cointot"  name="cointot"  style="width:125px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Currency Total</label>
                  <div class="col-sm-4">
                     <input  type="button" tabindex="2" value="{{ number_format((float)$curr_tot, 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('currencybox','safecountbox')" class="btn safecountcurrencytotal" style="width:125px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Other Total</label>
                  <div class="col-sm-4">
                     <input  type="button" tabindex="3" value="{{ number_format((float)$other_tot, 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('miscbox','safecountbox')" class="btn safecountothertotal" style="width:125px;text-align: right;">
                  </div>
               </div>
               <hr style="border:1px solid #000;" />
               <div class="row">
                  <div class="col-sm-4">&nbsp;</div>
                  <div class="col-sm-4">
                  <input type="hidden" name="file_locked" value="{{ $file_locked }}">
                  <input type="hidden" class="grand_tot" name="grand_tot" value="{{ number_format((float)$grand_tot, 2, '.', ''); }}">
                     <b>Grand Total  :</b><strong><span class="grand_total">{{ number_format((float)$grand_tot, 2, '.', ''); }}</span></strong>
                  </div>
                  <div class="col-sm-4">&nbsp;</div>
               </div>
               <br>
               <div class="row">
                  <div class="col-sm-4">&nbsp;</div>
                  <div class="col-sm-4">
                     <input type="submit" name="safecountsubmit" id="safecountsubmit" value="Submit" class="btn">
                     <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/safecount')}}'" />
                  </div>
                  <div class="col-sm-4">&nbsp;</div>
               </div>
               <br>
            </form>
         </div>
      </div>
   </div>
   <div id="coinbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="display: none;">
      <form method="post" id="coin-form">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >COIN </div>
            </div>
            <?php
               //echo '<pre>';print_r($safe_coin_array);
               ?>
            <div style="padding-top:30px" class="panel-body" >
               <div class="row">
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
                  </div>
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-3">BOXED</label>
                  </div>
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-3">ROLLED</label>
                  </div>
                  <div class="col-sm-3" style="padding-left: 35px;">
                     <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
                  </div>
               </div>
               <br>
               <div class="focusguardsafecoin" id="focusguard-safecoin" tabindex="15"></div>
               <div class="row">
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-3">Pennies</label>
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft txt text-right" id="pennies_box"  name="pennies_box" tabindex="1" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> value="{{ number_format((float)$safe_coin_array[0]['pennies_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft txt text-right" id="pennies_roll"  name="pennies_roll" tabindex="2" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> value="{{ number_format((float)$safe_coin_array[0]['pennies_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3" style="padding-right: 45px;text-align: right;">
                     <strong style="margin-left:20px;"><span class="pennies_total">{{ number_format((float)$safe_coin_array[0]['pennies_box']/100 + $safe_coin_array[0]['pennies_roll']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-3">Nickles</label>
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft coin txt1 text-right" id="nickle_box" tabindex="3" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="nickles_box" value="{{ number_format((float)$safe_coin_array[0]['nickles_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft coin txt1 text-right" id="nickle_roll" tabindex="4" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="nickles_roll" value="{{ number_format((float)$safe_coin_array[0]['nickles_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3" style="padding-right: 45px;text-align: right;">
                     <strong style="margin-left:20px;"><span class="nickles_total">{{ number_format((float)$safe_coin_array[0]['nickles_box']/100 + $safe_coin_array[0]['nickles_roll']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-3">Dimes</label>
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft coin txt2 text-right" id="dimes_box" tabindex="5" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="dimes_box" value="{{ number_format((float)$safe_coin_array[0]['dimes_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft coin txt2 text-right" id="dimes_roll" tabindex="6" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="dimes_roll" value="{{ number_format((float)$safe_coin_array[0]['dimes_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3" style="padding-right: 45px;text-align: right;">
                     <strong style="margin-left:20px;"><span class="dimes_total">{{ number_format((float)$safe_coin_array[0]['dimes_box']/100 + $safe_coin_array[0]['dimes_roll']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-3">Quarters</label>
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft coin txt3 text-right" id="quarter_box" tabindex="7" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="quarters_box" value="{{ number_format((float)$safe_coin_array[0]['quarters_box']/100, 2, '.', '');  }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft coin txt3 text-right" id="quarter_roll" tabindex="8" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="quarters_roll" value="{{ number_format((float)$safe_coin_array[0]['quarters_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3" style="padding-right: 45px;text-align: right;">
                     <strong style="margin-left:20px;"><span class="quarters_total">{{ number_format((float)$safe_coin_array[0]['quarters_box']/100 + $safe_coin_array[0]['quarters_roll']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-3" style="padding-left:24px;">
                     <label for="inputPassword" class="control-label">Misc</label>&nbsp;&nbsp;<input type="text" tabindex="9" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> style="width:60px;text-transform: uppercase" class="form-control misc_desc text-right" placeholder="0" name="misc_desc" value="{{ $safe_coin_array[0]['misc_desc'] }}">
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft coin txt4 text-right" id="misc_box" tabindex="10" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc_box" value="{{ number_format((float)$safe_coin_array[0]['misc_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3">
                     <input  type="text" placeholder="0.00"  class="form-control ft coin txt4 text-right" id="misc_roll" tabindex="11" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc_roll" value="{{ number_format((float)$safe_coin_array[0]['misc_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-3" style="padding-right: 45px;text-align: right;">
                     <strong style="margin-left:20px;"><span class="misc_total">{{ number_format((float)$safe_coin_array[0]['misc_box']/100 + $safe_coin_array[0]['misc_roll']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
               <hr style="border:1px solid #000;" />
               <div class="row">
                  <div class="col-sm-6">&nbsp;<span class="coinmessage"></span></div>
                  <div class="col-sm-6">
                     <div class="row">
                        <div class="col-sm-6">
                           <b>Grand Total......</b>
                        </div>
                        <div class="col-sm-6" style="padding-left:35px;font-size:16px;">
                           <strong><span class="coin_total">{{ number_format((float)$coin_tot, 2, '.', ''); }}</span></strong>
                        </div>
                     </div>
                  </div>
               </div>
               <br>
               <div class="row">
                  <div class="col-sm-4">&nbsp;</div>
                  <div class="col-sm-4">
                     <input type="button" <?php if(($dayIsLocked == "Y" || $file_locked == "Y")) echo 'disabled'; ?> name="" id="safecoinsubmit" id="submit" value="Submit" class="btn" tabindex="12">
                     <input type="button" name="" id="safecoinreset" tabindex="13" value="Cancel" class="btn" onclick="cancel_click('coinbox','safecountbox')">
                  </div>
                  <div class="col-sm-4">&nbsp;</div>
               </div>
               <br>
               @if(($dayIsLocked == "Y" && $file_locked == "Y") || ($dayIsLocked == "N" && $file_locked == "Y"))
               <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @elseif($dayIsLocked == "Y")
                   <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Data is locked for {{ date("m/d/Y", strtotime($safe_date)) }}. Any changes won't be saved</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @endif
               <div class="focusguardsafecoin" id="focusguard-safecoinreset" tabindex="14"></div>
               <br>
               <!--   </form> -->
            </div>
         </div>
      </form>
   </div>
   <div id="currencybox" style="margin-left:87px;" class="mainbox col-md-10 col-md-offset-3 col-sm-8 col-sm-offset-2" style="display: none;">
      <form method="post" id="currency-form">
         <div class="panel panel-info">
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title">CURRENCY</div>
            </div>
            <div style="padding-top:30px" class="panel-body">
               <?php
                  //echo '<pre>';print_r($safe_curr_array);
                  ?>
               <div class="col-sm-3" style="padding-left: 48px;">
                  <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
               </div>
               <div class="col-sm-2">
                  <label for="inputPassword" class="control-label col-sm-3">LEFT</label>
               </div>
               <div class="col-sm-2">
                  <label for="inputPassword" class="control-label col-sm-12" style="padding-left: 22px;">RIGHT</label>
               </div>
               <div class="col-sm-2">
                  <label for="inputPassword" class="control-label col-sm-12">CASH HELD</label>
               </div>
               <div class="col-sm-2" style="padding-left:40px;">
                  <label for="inputPassword" class="control-label col-sm-12" style="padding-left:28px;">TOTAL</label>
               </div>
            </div>
            <div class="focusguardsafecurr" id="focusguard-safecurr" tabindex="24"></div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-3" style="padding-left: 68px;">
                     <label for="inputPassword" class="control-label col-sm-12">Ones</label>
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyone ft txt5 text-right" id="ones_left" tabindex="1" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="ones_box" value="{{ number_format((float)$safe_curr_array[0]['ones_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyone ft txt5 text-right" id="ones_right" tabindex="2" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="ones_roll" value="{{ number_format((float)$safe_curr_array[0]['ones_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyone ft txt5 text-right" id="ones_cashheld" tabindex="3" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="ones_held" value="{{ number_format((float)$safe_curr_array[0]['ones_held']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2" style="text-align:right;padding-right:55px;">
                     <strong style="margin-left:20px;"><span class="ones_total">{{ number_format((float)$safe_curr_array[0]['ones_box']/100 + $safe_curr_array[0]['ones_roll']/100 + $safe_curr_array[0]['ones_held']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-3" style="padding-left: 68px;">
                     <label for="inputPassword" class="control-label col-sm-12">Fives</label>
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyfive ft txt6 text-right" id="fives_left" tabindex="4" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="fives_box" value="{{ number_format((float)$safe_curr_array[0]['fives_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyfive ft txt6 text-right" id="fives_right" tabindex="5" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="fives_roll" value="{{ number_format((float)$safe_curr_array[0]['fives_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyfive ft txt6 text-right" id="fives_cashheld" tabindex="6" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="fives_held" value="{{ number_format((float)$safe_curr_array[0]['fives_held']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2" style="text-align:right;padding-right:55px;">
                     <strong style="margin-left:20px;"><span class="fives_total">{{ number_format((float)$safe_curr_array[0]['fives_box']/100 + $safe_curr_array[0]['fives_roll']/100 + $safe_curr_array[0]['fives_held']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-3" style="padding-left: 68px;">
                     <label for="inputPassword" class="control-label col-sm-12">Tens</label>
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyten ft txt7 text-right" id="tens_left" tabindex="7" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="tens_box" value="{{ number_format((float)$safe_curr_array[0]['tens_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyten ft txt7 text-right" id="tens_right" tabindex="8" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="tens_roll" value="{{ number_format((float)$safe_curr_array[0]['tens_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencyten ft txt7 text-right" id="tens_cashheld" tabindex="9" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="tens_held" value="{{ number_format((float)$safe_curr_array[0]['tens_held']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2" style="text-align:right;padding-right:55px;">
                     <strong style="margin-left:20px;"><span class="tens_total">{{ number_format((float)$safe_curr_array[0]['tens_box']/100 + $safe_curr_array[0]['tens_roll']/100 + $safe_curr_array[0]['tens_held']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-3" style="padding-left: 68px;">
                     <label for="inputPassword" class="control-label col-sm-12">Twenties</label>
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencytwenty ft txt8 text-right" id="twenties_left" tabindex="10" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="twenties_box" value="{{ number_format((float)$safe_curr_array[0]['twenties_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencytwenty ft txt8 text-right" id="twenties_right" tabindex="11" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="twenties_roll" value="{{ number_format((float)$safe_curr_array[0]['twenties_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencytwenty ft txt8 text-right" id="twenties_cashheld" tabindex="12" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="twenties_held" value="{{ number_format((float)$safe_curr_array[0]['twenties_held']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2" style="text-align:right;padding-right:55px;">
                     <strong style="margin-left:20px;"><span class="twenties_total">{{ number_format((float)$safe_curr_array[0]['twenties_box']/100 + $safe_curr_array[0]['twenties_roll']/100 + $safe_curr_array[0]['twenties_held']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-3" style="padding-left: 84px;">
                     <label for="inputPassword" class="control-label">Misc 1 </label>&nbsp;&nbsp;<input type="text" style="width:70px" class="form-control misc_desc text-right" placeholder="0" tabindex="13" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc1_desc" value="{{ $safe_curr_array[0]['misc1_desc'] }}">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencymiscone ft txt9 text-right" id="misc1_left" tabindex="14" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc1_box" value="{{ number_format((float)$safe_curr_array[0]['misc1_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencymiscone ft txt9 text-right" id="misc1_right" tabindex="15" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc1_roll" value="{{ number_format((float)$safe_curr_array[0]['misc1_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencymiscone ft txt9 text-right" id="misc1_cashheld" tabindex="16" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc1_held" value="{{ number_format((float)$safe_curr_array[0]['misc1_held']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2" style="text-align:right;padding-right:55px;">
                     <strong style="margin-left:20px;"><span class="misc1_total">{{ number_format((float)$safe_curr_array[0]['misc1_box']/100 + $safe_curr_array[0]['misc1_roll']/100 + $safe_curr_array[0]['misc1_held']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-3 " style="padding-left: 84px;">
                     <label for="inputPassword" class="control-label">Misc 2 </label>&nbsp;&nbsp;<input type="text" style="width:70px" class="form-control misc_desc text-right" placeholder="0" tabindex="17" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc2_desc" value="{{ $safe_curr_array[0]['misc2_desc'] }}">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencymisctwo ft txt10 text-right" id="misc2_left" tabindex="18" name="misc2_box" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> value="{{ number_format((float)$safe_curr_array[0]['misc2_box']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencymisctwo ft txt10 text-right" id="misc2_right" tabindex="19" name="misc2_roll" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> value="{{ number_format((float)$safe_curr_array[0]['misc2_roll']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2">
                     <input  type="text" placeholder="0.00"  class="form-control currencymisctwo ft txt10 text-right" id="misc2_cashheld" tabindex="20" name="misc2_held" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> value="{{ number_format((float)$safe_curr_array[0]['misc2_held']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
                  <div class="col-sm-2" style="text-align:right;padding-right:55px;">
                     <strong style="margin-left:20px;"><span class="misc2_total">{{ number_format((float)$safe_curr_array[0]['misc2_box']/100 + $safe_curr_array[0]['misc2_roll']/100 + $safe_curr_array[0]['misc2_held']/100, 2, '.', ''); }}</span></strong>
                  </div>
               </div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               {{-- <label for="inputPassword" class="control-label col-sm-3" style="text-align: right;">Grand Total</label> --}}
               <div class="col-sm-4" style="margin-left:80px;font-size: 16px;">
                  <b>Grand Total  :</b><strong><span class="currency_total">{{ number_format((float)$curr_tot, 2, '.', ''); }}</span></strong>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="margin-left:80px;">
                  <input type="button" <?php if(($dayIsLocked == "Y" || $file_locked == "Y")) echo 'disabled'; ?> name="" id="safecurrencysubmit" tabindex="21" value="Submit" class="btn" onclick="cancel_click('currencybox','safecountbox')">
                  <input type="reset" name="" value="Cancel" id="safecurrreset" tabindex="22" class="btn" onclick="cancel_click('currencybox','safecountbox')">
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            @if(($dayIsLocked == "Y" && $file_locked == "Y") || ($dayIsLocked == "N" && $file_locked == "Y"))
               <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @elseif($dayIsLocked == "Y")
                   <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Data is locked for {{ date("m/d/Y", strtotime($safe_date)) }}. Any changes won't be saved</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @endif
            <div class="focusguardsafecoin" id="focusguard-safecurrreset" tabindex="23"></div>
            <br>
            <!--   </form> -->
         </div>
      </form>
   </div>
   <div id="miscbox" style="margin-left: 100px;" class="mainbox miscbox col-md-10" style="display:none">
      <form method="post" id="other-form">
         <div class="panel panel-info safe_total">
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >MISC</div>
            </div>
            <?php
               //echo '<pre>';print_r($safe_other_array);
            ?>
            <div style="padding-top:30px" class="panel-body" >
               <div class="row vertical-divider" style="margin-top: 10px">
                  <div class="col-xs-6" style="position:relative;padding-left:55px;">
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <b>DESCRIPTION</b>
                        </div>
                        <div class="focusguard" id="focusguard-1" tabindex="33"></div>
                        <div class="col-sm-6">
                           <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
                        </div>
                     </div>
                     <br><br>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Food stamps</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="foodstamps" tabindex="1" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> autofocus="" name="food_stamps" value="{{ number_format((float)$safe_other_array[0]['food_stamps']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Total Merch checks</label>
                        </div>
                        <div class="col-sm-6" style="text-align:right;padding-right:72px;">
                           <strong><span class="merch_total" style="margin-left:-55px;">{{ number_format((float)($safe_other_array[0]['merch_fives']+$safe_other_array[0]['merch_tens']+$safe_other_array[0]['merch_twenties']+$safe_other_array[0]['merch_misc'])/100, 2, '.', ''); }}</span></strong>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Total Script checks</label>
                        </div>
                        <div class="col-sm-6" style="text-align:right;padding-right:72px;">
                           <strong><span class="script_total" style="margin-left:-55px;">{{ number_format((float)($safe_other_array[0]['script1']+$safe_other_array[0]['script2']+$safe_other_array[0]['script3']+$safe_other_array[0]['script5']+$safe_other_array[0]['script6']+$safe_other_array[0]['script7']+$safe_other_array[0]['script8']+$safe_other_array[0]['script4']+$safe_other_array[0]['script9']+$safe_other_array[0]['script10'])/100, 2, '.', ''); }}</span></strong>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Beginning Cash Loans</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="begin_loans" tabindex="16" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?>  name="begin_loans" value="{{ number_format((float)$safe_other_array[0]['begin_loans']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Misc 1</label>&nbsp;&nbsp;<input type="text" style="width:70px;" placeholder="0"  class="form-control misc_desc text-right" tabindex="17" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc1_desc" value="{{ $safe_other_array[0]['misc1_desc']; }}">
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="misc1_amt" tabindex="18" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc1_amt" onKeyPress="return StopNonNumeric(this,event)" value="{{ number_format((float)$safe_other_array[0]['misc1_amt']/100, 2, '.', ''); }}">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Misc 2</label>&nbsp;&nbsp;<input type="text" style="width:70px;" placeholder="0"  class="form-control misc_desc text-right" tabindex="19" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc2_desc" value="{{ $safe_other_array[0]['misc2_desc']; }}">
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="misc2_amt" tabindex="20" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc2_amt" value="{{ number_format((float)$safe_other_array[0]['misc2_amt']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <br />
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">In-Store Charges</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="instr_chrg" tabindex="21" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?>  name="instr_chrg" value="{{ number_format((float)$safe_other_array[0]['instr_chrg']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Payroll Advances</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="pay_adv" tabindex="22" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="pay_adv" value="{{ number_format((float)$safe_other_array[0]['pay_adv']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Night Crew Till</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="night_crew" tabindex="23" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="night_crew" value="{{ number_format((float)$safe_other_array[0]['night_crew']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">EPS Debit To Carryover</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="debit" tabindex="24" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="debit" value="{{ number_format((float)$safe_other_array[0]['debit']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">EPS Credit To Carryover</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="credit" tabindex="25" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="credit" value="{{ number_format((float)$safe_other_array[0]['credit']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">EPS EBT To Carryover</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="ebt" tabindex="26" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="ebt" value="{{ number_format((float)$safe_other_array[0]['ebt']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">EPS GC To Carryover</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="giftcard" tabindex="27" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="giftcard" value="{{ number_format((float)$safe_other_array[0]['giftcard']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Misc 3</label>&nbsp;&nbsp;<input type="text" style="width:70px;" placeholder="0" class="form-control misc_desc text-right" tabindex="28" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc3_desc" value="{{ $safe_other_array[0]['misc3_desc']; }}">
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control gtotal ft text-right" id="misc3_amt" tabindex="29" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="misc3_amt" value="{{ number_format((float)$safe_other_array[0]['misc3_amt']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6" style="position:relative;padding-left:55px;">
                     <div class="form-group">
                        <div class="col-xs-8">
                           <b>Merchandise Checks</b>
                        </div>
                     </div>
                     <br/><br/>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Fives</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control merch gtotal ft text-right" id="merch_fives" tabindex="2" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?>  name="merch_fives" value="{{ number_format((float)$safe_other_array[0]['merch_fives']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Tens</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control merch gtotal ft text-right" id="merch_tens" tabindex="3" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?>  name="merch_tens" value="{{ number_format((float)$safe_other_array[0]['merch_tens']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Twenties</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control merch gtotal ft text-right" id="merch_twenties" tabindex="4" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?>  name="merch_twenties" value="{{ number_format((float)$safe_other_array[0]['merch_twenties']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Misc</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control merch gtotal ft text-right" id="merch_misc" tabindex="5" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?>  name="merch_misc" value="{{ number_format((float)$safe_other_array[0]['merch_misc']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-8">
                           <b>Scripts Checks</b>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script1</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script1" tabindex="6" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?>  name="script1" value="{{ number_format((float)$safe_other_array[0]['script1']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script2</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script2" tabindex="7" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="script2" value="{{ number_format((float)$safe_other_array[0]['script2']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script3</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script3" tabindex="8" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?>  name="script3" value="{{ number_format((float)$safe_other_array[0]['script3']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script4</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script4" tabindex="9" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="script4" value="{{ number_format((float)$safe_other_array[0]['script4']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script5</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script5" tabindex="10" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="script5" value="{{ number_format((float)$safe_other_array[0]['script5']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script6</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script6" tabindex="11" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="script6" value="{{ number_format((float)$safe_other_array[0]['script6']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script7</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script7" tabindex="12" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="script7" value="{{ number_format((float)$safe_other_array[0]['script7']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script8</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script8" tabindex="13" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="script8" value="{{ number_format((float)$safe_other_array[0]['script8']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script9</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script9" tabindex="14" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="script9" value="{{ number_format((float)$safe_other_array[0]['script9']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6 text-left">
                           <label for="inputPassword" class="control-label">Script10</label>
                        </div>
                        <div class="col-sm-6">
                           <input  type="text" placeholder="0.00"  class="form-control script gtotal ft text-right" id="script10" tabindex="15" <?php if($dayIsLocked == "Y"){ echo 'disabled'; } ?> name="script10" value="{{ number_format((float)$safe_other_array[0]['script10']/100, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                        </div>
                     </div>
                  </div>
               </div>
               <hr style="border:1px solid #000;" />
               <div class="row">
                  {{-- <div class="col-sm-4">&nbsp;</div> --}}
                  {{-- <label for="inputPassword" class="control-label col-sm-3" style="text-align: right;">Grand Total</label> --}}
                  <div class="col-sm-6" style="margin-left:80px;">
                  <div class="col-xs-6"><b>Grand Total  :</b></div>
                  <div class="col-xs-6"><strong><span class="gtotal_total">{{ number_format((float)$other_tot, 2, '.', ''); }}</span></strong></div>
                     
                  </div>
                  <div class="col-sm-6">&nbsp;</div>
               </div>
               <div class="row">
                  <div class="col-sm-4">&nbsp;</div>
                  <div class="col-sm-4" style="margin-left:80px;">
                     <input type="button" <?php if(($dayIsLocked == "Y" || $file_locked == "Y")) echo 'disabled'; ?> name="" id="safeothersubmit" tabindex="30" id="submit" value="Submit" class="btn" onclick="cancel_click('miscbox','safecountbox')">
                     <input type="button" name="" tabindex="31" id="otherreset" value="Cancel" class="btn" onclick="cancel_click('miscbox','safecountbox')">
                  </div>
                  <div class="col-sm-4">&nbsp;</div>
               </div>
               @if(($dayIsLocked == "Y" && $file_locked == "Y") || ($dayIsLocked == "N" && $file_locked == "Y"))
               <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @elseif($dayIsLocked == "Y")
                   <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Data is locked for {{ date("m/d/Y", strtotime($safe_date)) }}. Any changes won't be saved</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @endif
               <br>
               <div class="focusguard" id="focusguard-2" tabindex="32"></div>
            </div>
         </div>
      </form>
   </div>
</div>
@stop
<style type="text/css">
   /* #coinbox{
   display:none;
   }*/
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript">
   function hide_show_toggle_safecnt(pop_div,main_div)
   {
      $('#'+pop_div).show();
      $('#'+main_div).hide();
   }
   function cancel_click(pop_div,main_div)
   {
      $('#'+pop_div).hide();
      $('#'+main_div).show();   
   }  
    $(document).ready(function() {
   
   
      $( ".safecountcointotal" ).click(function() {
        $( "#pennies_box" ).focus();
      });
   
      $( ".safecountcurrencytotal" ).click(function() {
        $( "#ones_left" ).focus();
      });
   
      $( ".safecountothertotal" ).click(function() {
        $( "#foodstamps" ).focus();
        $( "#foodstamps" ).select();
      });
   
      $('#focusguard-safecoin').on('focus', function() {
         $('#safecoinreset').focus();
       });
   
       $('#focusguard-safecoinreset').on('focus', function() {
         $('#pennies_box').focus();
       });
   
       $('#focusguard-2').on('focus', function() {
         $('#foodstamps').focus();
       });
   
       $('#focusguard-1').on('focus', function() {
         $('#reset').focus();
       });

       $('#focusguard-safecurrreset').on('focus', function() {
         $('#ones_left').focus();
       });
   
       $('#focusguard-safecurr').on('focus', function() {
         $('#safecurrreset').focus();
       });
   
      $(".selectthis").click(function(){  
         this.select();
      });
      $(".txt,.txt1,.txt2,.txt3,.txt4,.txt5,.txt6,.txt7,.txt8,.txt9,.txt10,.ft,.merch").focus(function(event) {
      var sum = 0; 
      selfield = this.id;
      //this.select();
      // $("#safecoinsubmit").removeAttr('disabled');
      selfieldval=$("#"+selfield).val(); 
      $("#"+selfield).val(Math.round(selfieldval*100));
      this.select();
      });
   
   $(".txt,.txt1,.txt2,.txt3,.txt4,.txt5,.txt6,.txt7,.txt8,.txt9,.txt10,.ft,.merch,.script,.gtotal").blur(function(){  
       var sum = 0; 
        selfield = this.id;
         //$("#safecoinsubmit").removeAttr('disabled');
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
     });
   
   
   $(".txt,.txt1,.txt2,.txt3,.txt4,.txt5,.txt6,.txt7,.txt8,.txt9,.txt10,.merch,.script,.gtotal,.ft").on("blur", function() {
    calculateSum();
    calculatetotal();
    });
   
   
    function calculateSum() {
   var sum = sum1 = sum2 = sum3 = sum4 = sum5 = sum6 = sum7 = sum8 = sum9 = sum10 = merchsum = script = gtotal = g_total = 0;
    
   
   //first row 
    $(".txt").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum += parseFloat(this.value);
   
    }    
    });
   $(".pennies_total").text(sum.toFixed(2));
   
   //second row 
    $(".txt1").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum1 += parseFloat(this.value);
    }    
    });
   $(".nickles_total").text(sum1.toFixed(2));
    
   
    //third row 
    $(".txt2").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum2 += parseFloat(this.value);
    }    
    });
   $(".dimes_total").text(sum2.toFixed(2));
   
   
   //Forth row 
    $(".txt3").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum3 += parseFloat(this.value);
    }    
    });
   $(".quarters_total").text(sum3.toFixed(2));
   
   
   //Fifth row 
    $(".txt4").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum4 += parseFloat(this.value);
    }    
    });
   $(".misc_total").text(sum4.toFixed(2));
   
   
   
   //currency ones row 
    $(".txt5").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum5 += parseFloat(this.value);
    }    
    });
   $(".ones_total").text(sum5.toFixed(2));
   
   //currency fives row 
    $(".txt6").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum6 += parseFloat(this.value);
    }    
    });
   $(".fives_total").text(sum6.toFixed(2));
   
   //currency tens row 
    $(".txt7").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum7 += parseFloat(this.value);
    }    
    });
   $(".tens_total").text(sum7.toFixed(2));
   
   //currency twenties row 
    $(".txt8").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum8 += parseFloat(this.value);
    }    
    });
   $(".twenties_total").text(sum8.toFixed(2));
   
   //currency misc1 row 
    $(".txt9").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum9 += parseFloat(this.value);
    }    
    });
   $(".misc1_total").text(sum9.toFixed(2));
   
   
   //currency misc2 row 
    $(".txt10").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum10 += parseFloat(this.value);
    }    
    });
   $(".misc2_total").text(sum10.toFixed(2));
   

   //Merch Sum
      $(".merch").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   merchsum += parseFloat(this.value);
    }    
    });
   $(".merch_total").text(merchsum.toFixed(2));


   //Script Sum
   $(".script").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   script += parseFloat(this.value);
    }    
    });
   $(".script_total").text(script.toFixed(2));

    //Others Final Sum
   $(".gtotal").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   gtotal += parseFloat(this.value);
    }    
    });
      $(".gtotal_total").text(gtotal.toFixed(2));
      $(".safecountothertotal").val(gtotal.toFixed(2));
   
   $(".ft").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   g_total += parseFloat(this.value);
    }    
    });
      $(".grand_total").text(g_total.toFixed(2));
      $(".grand_tot").val(g_total.toFixed(2));   

    } 
   
   
   function calculatetotal()
   {
   var totalsum = 0;
   var totalsum1 = 0;
   
   //coin total
   $(".txt,.txt1,.txt2,.txt3,.txt4").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   totalsum += parseFloat(this.value);
   
    }    
    });
   $(".coin_total").text(totalsum.toFixed(2));
   $(".safecountcointotal").val(totalsum.toFixed(2));
   //currency total
   $(".txt5,.txt6,.txt7,.txt8,.txt9,.txt10").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   totalsum1 += parseFloat(this.value);
   
    }    
    });
   $(".currency_total").text(totalsum1.toFixed(2));
   $(".safecountcurrencytotal").val(totalsum1.toFixed(2));
   
   }
      $("#safecoinsubmit").click(function(e){
            
            var datastring = $("#coin-form").serializeArray();
            var safetype = '{{ $safetype }}';
            //alert(safetype);//return false;
            var safe_date = '{{ $safe_date }}';
            //alert(safe_date);
            $.post("safecoinsubmit",{data:datastring,type:safetype,safe_date:safe_date} ,function(data) {
               //alert(data);return false;

            //   $( ".coinmessage" ).html( data );
            //   setTimeout(function(){
            //    $( ".coinmessage" ).hide();
            //    cancel_click('coinbox','safecountbox');
            // }, 1200);
              cancel_click('coinbox','safecountbox');
              // $('#safecountbox').show();
              // $('#coinbox').hide();
            });

          //$( ".safecountcurrencytotal" ).focus();  
      });
      $("#safecurrencysubmit").click(function(){
         //alert('currency-form');
            var datastring = $("#currency-form").serializeArray();
            var currencytype = '{{ $currencytype }}';
             var safe_date = '{{ $safe_date }}';
             //alert(safe_date);return false;
            $.post("safecurrencysubmit",{data:datastring,type:currencytype,safe_date:safe_date} ,function(data) {
               cancel_click('currencybox','safecountbox');
            });
          $( ".safecountothertotal" ).focus();  
      });
      $("#safeothersubmit").click(function(){
            var datastring = $("#other-form").serializeArray();
            var othertype = '{{ $othertype }}';
             var safe_date = '{{ $safe_date }}';
            $.post("safeothersubmit",{data:datastring,type:othertype,safe_date:safe_date} ,function(data) {
               //alert(data);return false;
               cancel_click('miscbox','safecountbox');
            });
          $( "#safecountsubmit" ).focus();  
      });
   });
   function StopNonNumeric1(el, evt)
       {
           var charCode = (evt.which) ? evt.which : event.keyCode;
           var number = el.value.split('.');
           if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
               return false;
           }
           //just one dot (thanks ddlab)
           if(number.length>1 && charCode == 46){
                return false;
           }
           //get the carat position
           var dotPos = el.value.indexOf(".");
           if( dotPos>-1 && (number[1].length > 3)){
               return false;
           }
           return true;
       }
   /*  Other Total Jquery SumUp Code Start  */
   $(document).ready(function() {
   
      
   $("input.form-control").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

   });
   
   
   /*  Other Total Jquery SumUp Code End  */
</script>