@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>

<div class="container">
    <table class="table table-striped">
    <thead>
      <tr>
        <th>Date</th>
        <th>Comments</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($comments as $cmt)
      <tr>
        <td>{{ HTML::link(URL::route('mktmgr-bookcommentadd',['dateofinfo' => $cmt->date_stamp]), $cmt->date_stamp ) }}</td>
        <td>{{ HTML::link(URL::route('mktmgr-bookcommentadd',['dateofinfo' => $cmt->date_stamp]), $cmt->comment_line1.$cmt->comment_line2.$cmt->comment_line3.$cmt->comment_line4.$cmt->comment_line5.$cmt->comment_line6.$cmt->comment_line7.$cmt->comment_line8.$cmt->comment_line9) }}</td>
      </tr>
    @endforeach
    </tbody>
  </table>          
 {{ $comments->links() }}
</div>
@stop
