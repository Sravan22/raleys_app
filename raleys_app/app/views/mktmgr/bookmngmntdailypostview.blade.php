@extends('layout.dashboardbookkeepermarket')
@section('page_heading','Daily')
@section('content')
@section('section')
<header class="row">
    @include('mktmgr.bookkepermenu')
</header>
<div class="container">
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div> <!-- end .flash-message -->

<div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
<div class="panel panel-info" >
<div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
    <div class="panel-title" >Safe Report - {{  date('m/d/Y', strtotime($date_stamp)) }}</div>
</div>
<div style="padding-top:10px" class="panel-body">
<form action="{{URL::route('mktmgr-post-bookmngmntdailymgrok')}}" class="form-horizontal" method="post" role="form" style="display: block;">
<input type="hidden" name="safe_date" value="{{ $date_stamp }}">
<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-7">(BS) Beginning Safe</label>
    <div class="col-sm-5">
        <?php echo number_format($begin_safe/100,2); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-7" style="height: 20px;"></div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-5">(DT) Deliveries & Transfers</label>
    <div class="col-sm-7">
        <input type="button" onclick="showdetails('DELIVERIES')" name="DELIVERIES" tabindex="4" value="<?php echo number_format($del_tranfs/100,2); ?>" class="btn" style="width:225px;">
        @if($errors->has(''))
        {{ $errors->first('')}}
        @endif
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-5">(DP) Deposits</label>
    <div class="col-sm-7">
        <!--<input type="text" class="form-control" id="" placeholder="" name="" value="{{ $deposits }}">-->
        <input type="button" onclick="showdetails('DEPOSITS')" name="DEPOSITS" tabindex="4" value="<?php echo number_format($deposits/100,2); ?>" class="btn" style="width:225px;">
        @if($errors->has(''))
        {{ $errors->first('')}}
        @endif
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-5">(ST) Store Transactions</label>
    <div class="col-sm-7">
        <!--<input type="text" class="form-control" id="" placeholder="" name="" value="{{ $store_tx }}">-->
        <input type="button" onclick="showdetails('STORE TRANSACTIONS')" name="STORE TRANSACTIONS" tabindex="4" value="<?php echo number_format($store_tx/100,2); ?>" class="btn" style="width:225px;">
        @if($errors->has(''))
        {{ $errors->first('')}}
        @endif
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-5">(PO) Paid Out Types</label>
    <div class="col-sm-7">
        <input type="button" onclick="showdetails('PAID OUT TYPES')" name="PAID OUT TYPES" tabindex="4" value="<?php echo number_format($po_types/100,2); ?>" class="btn" style="width:225px;">
        <!--<input type="text" class="form-control" id="" placeholder="" name="" value="{{ $po_types }}">-->
        @if($errors->has(''))
        {{ $errors->first('')}}
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-sm-7" style="height: 20px;"></div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-7">(TS) Total To Account For</label>
    <div class="col-sm-5">
        <?php echo number_format($tot_acct/100,2); ?>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-7">(CT) Safe Count Total</label>
    <div class="col-sm-5">
        <?php echo number_format($safe_cnt/100,2); ?>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-7">(SA) Safe Over &lt;Short&gt;</label>
    <div class="col-sm-5">
        <?php echo number_format($safe_os/100,2); ?>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-7">(GO) Grocery Over &lt;Short&gt;</label>
    <div class="col-sm-5">
        <?php echo number_format($groc_os/100,2); ?>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-7">(DO) Drug Over &lt;Short&gt;</label>
    <div class="col-sm-5">
        <?php echo number_format($drug_os/100,2); ?>
    </div>
</div>


<div class="form-group" style="margin-top: 20px;">
     <label for="inputPassword" class="control-label col-sm-7">STORE TOTAL OVER &lt;SHORT&gt;</label>
    <div class="col-sm-5">
        <?php echo number_format($total/100,2); ?>
    </div>
</div>

<div class="form-group" style="margin-top: 20px;">
    <div class="row">
        <div class="col-sm-12" style="padding-left: 38%">
            <!--<input type="button" onclick="window.location.href='{{URL::route('mktmgr-post-bookmngmntdailyshowdetails')}}'" name="showdetails-submit" id="submit" tabindex="4" value="Show Detail" class="btn">-->
            <input type="submit" name="mgmtapproval-submit" id="submit" tabindex="4" value="MGMT Approval" class="btn">
            <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Accept" class="btn">
            <input type="button" name="cancel" value="Cancel" class="btn" onclick="window.location.href='{{URL::route('mktmgr-bookmngmntdaily')}}'">
            {{ Form::token()}}
        </div>
    </div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
@stop

@section('jssection')
@parent
<script>
parentHtml = $("#loginbox").html();
function showdetails(t) {
    $.ajax({
        url:'{{URL::route('mktmgr-post-bookmngmntdailyshowdetails')}}',
        type:'GET',
        data: "t="+t+"&date_stamp={{$date_stamp}}",
        success:function(data){
            $("#loginbox").html(data);
        }
    });
}

function clickCancel() {
    $("#loginbox").html(parentHtml);
    //htmlStore = parentHtml;
}

$(document).ready(function(){

$("#calgetval").blur(function(e){ 
  selfield = this.id;
  selfieldval=$("#"+selfield).val(); 
  alert(selfieldval);
});

});

</script>
<script>
function goBack() {
    window.history.back();
}
</script>  
@endsection
@stop
@endsection
@stop
