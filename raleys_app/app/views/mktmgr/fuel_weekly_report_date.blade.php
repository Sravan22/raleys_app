<?php 
   //echo 'Fuel Weekly Date';exit;
   //echo '<pre>',print_r($results);exit();
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php $d=strtotime("last Saturday"); $lastsaturday = date("Y-m-d", $d); ?>
{{-- <div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div> --}}
<div>
   @if(Session::has('message'))
   <div class="alert  alert-danger" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('message')  }}                               
   </div>
   @endif
</div>
<div class="container">
<div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
<div class="panel panel-info" >
<div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
   <div class="panel-title" ><span>Store : 355 </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter Date</div>
</div>
<div style="padding-top:30px" class="panel-body" >
   <form action="{{ URL::route('bookspro-post-fuel_weekly_report_date') }}" id="booksafedate" class="form-horizontal" method="post" role="form" style="display: block;">
      <div class="form-group">
         <label for="inputPassword" class="control-label col-sm-6">Enter Date</label>
         <div class="col-sm-6">
            <input type="date" class="form-control" id="wkEndDate" placeholder="" name="wkEndDate" value="{{ $lastsaturday }}"> 
            @if($errors->has('wkEndDate'))
            {{ $errors->first('wkEndDate')}}
            @endif
         </div>
      </div>
      <div class="form-group">
         <div class="row">
            <div class="col-sm-12" align="center">
               <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
               <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
               {{ Form::token()}}
            </div>
         </div>
      </div>
   </form>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }
</style>
@stop