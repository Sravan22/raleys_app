<?php
  //echo '<pre>';print_r($departments);exit;
?>
@extends('layout.dashboard')
@section('page_heading','Store Orders')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" /> 
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel_heading">
            <div class="panel-title">Select Department</div>
         </div>
         <div  class="panel_body" >
            <form action="{{URL::route('mktmgr-post-edit-warning-limits')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12" >Department</label>
                  </div>
                  <input type="hidden" name="store_number" value="{{{ Session::get('storeid') }}}">
                  <div class="col-sm-7 padding_bottom">
                     <select id="dept_name_number" name="dept_name_number" class="form-control keyfirst" value="dept_name_number" autofocus="" tabindex="2">
                        <option value="">Select Department</option>
                        @for ($i = 0; $i < count($departments); $i++)
                        <option value="{{ $departments[$i]['type_code'] }}">{{ $departments[$i]['name'] }}</option>
                        @endfor
                     </select>
                     @if($errors->has('dept_name_number'))
                     {{ $errors->first('dept_name_number')}}
                     @endif
                  </div>
               </div>
               <div class="form-group padding_bottom">
                  <div class="row">
                     <div class="col-md-4"></div>
                    <div class="col-md-2"></div>
                     <div class="col-md-4">
                        <input type="submit" name="login-submit" id="submit" tabindex="3" value="Submit" class="btn">
                        <input type="reset" name="" id="submit" tabindex="4" value="Reset" class="btn keylast" >
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="5"></div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop