@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-5 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >LOTTERY INVENTORY - {{date('m/d/Y',strtotime($data->lottery_date))}}</div>
         </div>
         <div style="padding-top:15px" class="panel-body" >
            <form action="{{URL::route('mktmgr-booklotterytotalformdata')}}" class="form-horizontal " method="post" role="form" style="display: block;">
               <input type="hidden" class="form-control" id="lottery_date" placeholder="" name="lottery_date" value="{{date('Y-m-d',strtotime($data->lottery_date))}}" >
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-md-offset-1 col-sm-7 ">Calculated Sales</label>
                  <div class="col-sm-3">
                     <input type="text" class="form-control" id="calc_sales" placeholder="" name="calc_sales" value="{{$data->calc_sales}}" readonly="">
                     @if($errors->has('calc_sales'))
                     {{ $errors->first('calc_sales')}}
                     @endif
                  </div>
               </div>
               <div class="focusguard" id="focusguard-1" tabindex="0"></div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label  col-md-offset-1 col-sm-7">S/L Sales From Unity Report</label>
                  <div class="col-sm-3">
                      <input type="text" class="form-control sel flot" id="sales_unity" tabindex="1" placeholder="" name="sales_unity" value="{{$data->sales_unity}}" 
                      <?php if($data->file_locked =='Y'){echo 'disabled';} ?>/>
                     @if($errors->has('sales_unity'))
                     {{ $errors->first('sales_unity')}}
                     @endif
                  </div>
               </div>
               <hr style="border:1px solid #000;">
               <div class="form-group" style="">
                  <label for="inputPassword" class="control-label col-md-offset-1 col-sm-7">Sales Difference</label>
                  <div class="col-sm-3">
                      <input type="text" class="form-control" id="sales_diff" readonly="" placeholder="" name="sales_diff" value="{{$data->sales_diff}}">
                     @if($errors->has('sales_diff'))
                     {{ $errors->first('sales_diff')}}
                     @endif
                  </div>
               </div>
               {{-- files locked  --}}
               <br>
               @if($data->file_locked =="Y")
               <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @endif
               <div class="form-group" style="margin-top: 20px;">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="accept-submit" id="submit" tabindex="2" value="Submit" class="btn"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>/>
                        <input type="submit" name="login-submit" id="cancel" tabindex="3" value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
                <div class="focusguard" id="focusguard-2" tabindex="4"></div>


            </form>
         </div>
      </div>
   </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script>
 $(function() {

      /*select sales unity column*/
      $('#sales_unity').select();
      
      /*tab functionality*/
      $('#focusguard-2').on('focus', function() {
      $('#sales_unity').focus();
      });

      $('#focusguard-1').on('focus', function() {
      $('#cancel').focus();
      });

      /*focus input field and multiply by 100*/    
      $("#sales_unity").focus(function(event) {
      var sum = 0; 
      selfield = this.id;
      selfieldval=$("#"+selfield).val(); 
      $("#"+selfield).val(Math.round(selfieldval*100));
      this.select();
      });

      /*divide by 100*/
      $('#sales_unity').blur(function(event) {
      selfield = this.id;
      selfieldval=$("#"+selfield).val();
      $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));

      calculateSalesDiff();
      });

      /*numeric functionality*/

      $("input.form-control").keydown(function (e) {
      
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


 });
          /*calculate sales Difference*/
        function calculateSalesDiff(){

        var sales_unity = $("#sales_unity").val();
        var calc_sales  = $("#calc_sales").val();      
        var sales_diff  = (calc_sales - sales_unity) ; 
        $("#sales_diff").val(convertnulltozero(sales_diff));


        }
        /*values*/
        function convertnulltozero(val){
         
          if(isNaN(val)==true || val==NaN || val==null || val===undefined || val=='NaN' || typeof(val)=='undefined' || val==''){
             
                val=0;
                
            } else {
                val=val.toFixed(2);
            }
            
          return val;
      }

/*
     function isFloat(num){
         
        if(num.toString().indexOf(".")==-1){
            return false;
        }
        return true;
    }
           function convertnulltozero(val){
         
          if(isNaN(val)==true || val==NaN || val==null || val===undefined || val=='NaN' || typeof(val)=='undefined' || val==''){
             
                val=0;
                
            } else {
                val=val.toFixed(2);
            }
            
          return val;
      }*/



  // jQuery(document).ready(function($) {

 /*$("input.form-control").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $(".sel").on("click", function () {
      $(this).select();
      });
    $(".flot").blur(function(){  
       var sum = 0; 
        selfield = this.id;
         
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
     });
*/
     
      /* $( "input.form-control" ).on( "blur", function() {
         var nw=parseFloat($(this).val());
          $(this).val(nw.toFixed(2));
        });*/
          
      /*$( "input#sales_unity" ).on( "blur", function() {              
           if(isFloat($(this).val())==true){
                   
               } else {
                var nw=parseFloat($(this).val())/100;

                 $(this).val(convertnulltozero(nw));
                 
                }
                 calculateSalesDiff();
               
      });
      */
      /*function calculateSalesDiff(){
          
           var sales_unity= $("#sales_unity").val();
            var calc_sales=$("#calc_sales").val();      
            var sales_diff = (calc_sales - sales_unity) ; 
           $("#sales_diff").val(convertnulltozero(sales_diff));
     
   
      }*/
      
      
      
    //});
</script>
@endsection
@stop
