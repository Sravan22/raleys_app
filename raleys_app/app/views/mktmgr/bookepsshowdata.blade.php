@extends('layout.dashboardbookkeepermarket')
@section('page_heading','Commuter')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style type="text/css">
  .leftpadding{
    padding-left: 100px;
    font-weight: bold;
  }
</style>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-12">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >EPS ERROR FORM - {{date('m/d/Y',strtotime($data->dateofinfo))}}</div>
         </div>
         <div style="padding-top:10px" class="panel-body" >
            <form action="{{URL::route('mktmgr-bookepspost')}}" class="form-horizontal" method="post" role="form" style="display: block;">
            <div class="focusguard" id="focusguard-1" tabindex="0"></div>

               <input type="hidden"  id="eps_date" placeholder="" name="eps_date" value="{{date('Y-m-d',strtotime($data->eps_date))}}" >
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-6">&nbsp;</label>
                        <label for="inputPassword" class="control-label col-sm-3" style="padding-left: 33px;">DEBIT</label>
                        <label for="inputPassword" class="control-label col-sm-3" style="padding-left: 43px;">EBT</label>
                     </div>
                     <div class="form-group">
                        {{-- <label for="inputPassword" class="control-label col-sm-6 center">Prev Pend.</label> --}}
                        <span class="col-sm-6 leftpadding">Prev Pend.</span>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="prev_pend_deb" placeholder="" name="prev_pend_deb" value="{{$data->prev_pend_deb}}" tabindex="1"  autofocus=""
                            <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('prev_pend_deb'))
                           {{ $errors->first('prev_pend_deb')}}
                           @endif
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="prev_pend_ebt" placeholder="" name="prev_pend_ebt" value="{{$data->prev_pend_ebt}}" tabindex="6" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        {{-- <label for="inputPassword" class="control-label col-sm-6">Store Total</label> --}}
                         <span class="col-sm-6 leftpadding">Store Total</span>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="net_sales_deb" placeholder="" name="net_sales_deb" value="{{$data->net_sales_deb}}" tabindex="2"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('net_sales_deb'))
                           {{ $errors->first('net_sales_deb')}}
                           @endif
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="net_sales_ebt" placeholder="" name="net_sales_ebt" value="{{$data->net_sales_ebt}}" tabindex="7" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        {{-- <label for="inputPassword" class="control-label col-sm-6">Pending...</label> --}}
                         <span class="col-sm-6 leftpadding">Pending...</span>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="curr_pend_deb" placeholder="" name="curr_pend_deb" value="{{$data->curr_pend_deb}}" tabindex="3" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('curr_pend_deb'))
                           {{ $errors->first('curr_pend_deb')}}
                           @endif
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="curr_pend_ebt" placeholder="" name="curr_pend_ebt" value="{{$data->curr_pend_ebt}}" tabindex="8" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        {{-- <label for="inputPassword" class="control-label col-sm-6">EFT Reject.</label> --}}
                         <span class="col-sm-6 leftpadding">EFT Reject.</span>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="offline_rej_deb" placeholder="" name="offline_rej_deb" value="{{$data->offline_rej_deb}}" tabindex="4" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('offline_rej_deb'))
                           {{ $errors->first('offline_rej_deb')}}
                           @endif
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="offline_rej_ebt" placeholder="" name="offline_rej_ebt" value="{{$data->offline_rej_ebt}}" tabindex="9" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 20px;">
                        {{-- <label for="inputPassword" class="control-label col-sm-6">Calc Host.</label> --}}
                         <span class="col-sm-6 leftpadding">Calc Host.</span>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="tot_debits" placeholder="" name="tot_debits" value="{{$data->tot_debits}}" readonly="">
                        </div>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" id="tot_ebt" placeholder="" name="tot_ebt" value="{{$data->tot_ebt}}" readonly="">
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 20px;">
                        {{-- <label for="inputPassword" class="control-label col-sm-6">DDS - HOST.</label> --}}
                         <span class="col-sm-6 leftpadding">DDS - Host</span>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="dep_sum_deb" placeholder="" name="dep_sum_deb" value="{{$data->dep_sum_deb}}" tabindex="5" <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('dep_sum_deb'))
                           {{ $errors->first('dep_sum_deb')}}
                           @endif
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control thisselect" id="dep_sum_ebt" placeholder="" name="dep_sum_ebt" value="{{$data->dep_sum_ebt}}" tabindex="10"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        {{-- <label for="inputPassword" class="control-label col-sm-6">Calc Totals</label> --}}
                         <span class="col-sm-6 leftpadding">Calc Totals</span>
                        <div class="col-sm-3">
                           <input type="text" class="form-control thisselect" id="tot_debits2" placeholder="" name="tot_debits2" value="{{$data->tot_debits}}" readonly="">
                        </div>
                        <div class="col-sm-3">
                           <input type="text" class="form-control thisselect" id="tot_ebt2" placeholder="" name="tot_ebt2" value="{{$data->tot_ebt}}" readonly="">
                        </div>
                     </div>
                     <div class="form-group">
                        {{-- <label for="inputPassword" class="control-label col-sm-6">EPS Errors.</label> --}}
                         <span class="col-sm-6 leftpadding">EPS Errors.</span>
                        <div class="col-sm-3">
                           <input type="text" class="form-control thisselect" id="debit_diff" placeholder="" name="debit_diff" value="{{$data->debit_diff}}" readonly="">
                           @if($errors->has('debit_diff'))
                           {{ $errors->first('debit_diff')}}
                           @endif
                        </div>
                        <div class="col-sm-3">
                           <input type="text" class="form-control thisselect" id="ebt_diff" placeholder="" name="ebt_diff" value="{{$data->ebt_diff}}" readonly="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-6" style="padding-left: 30px;">CREDIT</label>
                        <label for="inputPassword" class="control-label col-sm-6" style="padding-left: 20px;">GIFT CARD</label>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="prev_pend_cr" placeholder="" name="prev_pend_cr" value="{{$data->prev_pend_cr}}" tabindex="11"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('prev_pend_cr'))
                           {{ $errors->first('prev_pend_cr')}}
                           @endif
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="prev_pend_gc" placeholder="" name="prev_pend_gc" value="{{$data->prev_pend_gc}}" tabindex="16"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="net_sales_cr" placeholder="" name="net_sales_cr" value="{{$data->net_sales_cr}}" tabindex="12"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('net_sales_cr'))
                           {{ $errors->first('net_sales_cr')}}
                           @endif
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="net_sales_gc" placeholder="" name="net_sales_gc" value="{{$data->net_sales_gc}}" tabindex="17"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="curr_pend_cr" placeholder="" name="curr_pend_cr" value="{{$data->curr_pend_cr}}" tabindex="13"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('curr_pend_cr'))
                           {{ $errors->first('curr_pend_cr')}}
                           @endif
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="curr_pend_gc" placeholder="" name="curr_pend_gc" value="{{$data->curr_pend_gc}}" tabindex="18"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="offline_rej_cr" placeholder="" name="offline_rej_cr" value="{{$data->offline_rej_cr}}" tabindex="14"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('offline_rej_cr'))
                           {{ $errors->first('offline_rej_cr')}}
                           @endif
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="offline_rej_gc" placeholder="" name="offline_rej_gc" value="{{$data->offline_rej_gc}}" tabindex="19"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 20px;">
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="tot_credits" placeholder="" name="tot_credits" value="{{$data->tot_credits}}" readonly="">
                        </div>
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="tot_gc" placeholder="" name="tot_gc" value="{{$data->tot_gc}}" readonly="">
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 20px;">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="dep_sum_cr" placeholder="" name="dep_sum_cr" value="{{$data->dep_sum_cr}}" tabindex="15"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                           @if($errors->has('dep_sum_cr'))
                           {{ $errors->first('dep_sum_cr')}}
                           @endif
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="dep_sum_gc" placeholder="" name="dep_sum_gc" value="{{$data->dep_sum_gc}}" tabindex="20"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="tot_credits2" placeholder="" name="tot_credits2" value="{{$data->tot_credits}}" readonly="">
                        </div>
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="tot_gc2" placeholder="" name="tot_gc2" value="{{$data->tot_gc}}" readonly="">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="credit_diff" placeholder="" name="credit_diff" value="{{$data->credit_diff}}" readonly="">
                           @if($errors->has('credit_diff'))
                           {{ $errors->first('credit_diff')}}
                           @endif
                        </div>
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="gc_diff" placeholder="" name="gc_diff" value="{{$data->gc_diff}}" readonly="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-6" style="padding-left: 20px;">ST. CREDIT</label>
                        <label for="inputPassword" class="control-label col-sm-6" style="padding-left: 20px;">E-CHECK</label>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="prev_pend_sc" placeholder="" name="prev_pend_sc" value="{{$data->prev_pend_sc}}" tabindex="21"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="prev_pend_ec" placeholder="" name="prev_pend_ec" value="{{$data->prev_pend_ec}}"  tabindex="26"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="net_sales_sc" placeholder="" name="net_sales_sc" value="{{$data->net_sales_sc}}" tabindex="22"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="net_sales_ec" placeholder="" name="net_sales_ec" value="{{$data->net_sales_ec}}" tabindex="27"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="curr_pend_sc" placeholder="" name="curr_pend_sc" value="{{$data->curr_pend_sc}}" tabindex="23"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="curr_pend_ec" placeholder="" name="curr_pend_ec" value="{{$data->curr_pend_ec}}" tabindex="28"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="offline_rej_sc" placeholder="" name="offline_rej_sc" value="{{$data->offline_rej_sc}}" tabindex="24"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="offline_rej_ec" placeholder="" name="offline_rej_ec" value="{{$data->offline_rej_ec}}" tabindex="29"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 20px;">
                        <div class="col-sm-6">
                            <input type="text" class="form-control thisselect" id="tot_sc" placeholder="" name="tot_sc" value="{{$data->tot_sc}}" readonly="" >
                        </div>
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="tot_ec" placeholder="" name="tot_ec" value="{{$data->tot_ec}}" readonly="">
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 20px;">
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="dep_sum_sc" placeholder="" name="dep_sum_sc" value="{{$data->dep_sum_sc}}" tabindex="25"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="dep_sum_ec" placeholder="" name="dep_sum_ec" value="{{$data->dep_sum_ec}}" tabindex="30"<?php if($data->file_locked =='Y'){echo 'disabled';} ?>>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="tot_sc2" placeholder="" name="tot_sc2" value="{{$data->tot_sc}}" readonly="">
                        </div>
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="tot_ec2" placeholder="" name="tot_ec2" value="{{$data->tot_ec}}" readonly="">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="sc_diff" placeholder="" name="sc_diff" value="{{$data->sc_diff}}" readonly="">
                        </div>
                        <div class="col-sm-6">
                           <input type="text" class="form-control thisselect" id="ec_diff" placeholder="" name="ec_diff" value="{{$data->ec_diff}}" readonly="">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group" style="margin-top: 20px;">
                  {{-- <label for="inputPassword" class="control-label col-sm-6">Tot EPS Err (Debit + EBT + Credit + GC + St.Cred + EChk)...</label> --}}
                   <span class="col-sm-6 leftpadding">Tot EPS Err (Debit + EBT + Credit + GC + St.Cred + EChk)...</span>
                  <div class="col-sm-3">
                     <input type="text" readonly=""  class="form-control" id="tot_deb_cred" placeholder="" name="tot_deb_cred" value="{{$data->tot_deb_cred}}">
                  </div>
               </div>
               <div class="form-group" style="margin-top: 20px;">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                         <input type="submit" name="accept-submit" id="submit" tabindex="31"  value="Accept" class="btn"
                         <?php if($data->file_locked =='Y'){echo 'disabled';} ?>>  
                         <input type="submit" name="login-submit" id="cancel" tabindex="32"  value="Cancel" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="33"></div>
              {{-- files locked  --}}
               <br>
               @if($data->file_locked =="Y")
               <div class="row">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @endif
            </form>
         </div>
      </div>
   </div>
</div>
</div>
@section('jssection')
@parent
   
<script>
 function isFloat(num){
         
        if(num.toString().indexOf(".")==-1){
            return false;
        }
        return true;
    }
   jQuery(document).ready(function($) {
     $('#prev_pend_deb').select();
     
      $('#focusguard-2').on('focus', function() {
      $('#prev_pend_deb').focus();
      });

      $('#focusguard-1').on('focus', function() {
      $('#cancel').focus();
      });

     
     $("input.form-control").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
       $( "input.form-control" ).on( "blur", function() {
        
        if(isFloat($(this).val())==true){
                   
               } else {
                var nw=parseFloat($(this).val())/100;

                 $(this).val(convertnulltozero(nw));
                }
                
        
        });
          
      $( "input.form-control" ).on( "change keyup", function() {              
            calculateSalesDiff();
      });
      
      function convertnulltozero(val){
         
          if(isNaN(val)==true || val==NaN || val==null || val===undefined || val=='NaN' || typeof(val)=='undefined' || val==''){
             
                val=0;
                
            } else {
                val=val.toFixed(2);
            }
            
          return val;
      }
      
      function calculateSalesDiff(){
          
            var prev_pend_deb=parseFloat($("#prev_pend_deb").val());
            var net_sales_deb=parseFloat($("#net_sales_deb").val());
            var curr_pend_deb=parseFloat($("#curr_pend_deb").val());
            var offline_rej_deb=parseFloat($("#offline_rej_deb").val());
            var dep_sum_deb=parseFloat($("#dep_sum_deb").val());
   
            var prev_pend_ebt=parseFloat($("#prev_pend_ebt").val());
            var net_sales_ebt=parseFloat($("#net_sales_ebt").val());
            var curr_pend_ebt=parseFloat($("#curr_pend_ebt").val());
            var offline_rej_ebt=parseFloat($("#offline_rej_ebt").val());
            var dep_sum_ebt=parseFloat($("#dep_sum_ebt").val());
   
            var prev_pend_cr=parseFloat($("#prev_pend_cr").val());
            var net_sales_cr=parseFloat($("#net_sales_cr").val());
            var curr_pend_cr=parseFloat($("#curr_pend_cr").val());
            var offline_rej_cr=parseFloat($("#offline_rej_cr").val());
            var dep_sum_cr=parseFloat($("#dep_sum_cr").val());
   
            var prev_pend_gc=parseFloat($("#prev_pend_gc").val());
            var net_sales_gc=parseFloat($("#net_sales_gc").val());
            var curr_pend_gc=parseFloat($("#curr_pend_gc").val());
            var offline_rej_gc=parseFloat($("#offline_rej_gc").val());
            var dep_sum_gc=parseFloat($("#dep_sum_gc").val());
   
            var prev_pend_sc=parseFloat($("#prev_pend_sc").val());
            var net_sales_sc=parseFloat($("#net_sales_sc").val());
            var curr_pend_sc=parseFloat($("#curr_pend_sc").val());
            var offline_rej_sc=parseFloat($("#offline_rej_sc").val());
            var dep_sum_sc=parseFloat($("#dep_sum_sc").val());
   
            var prev_pend_ec=parseFloat($("#prev_pend_ec").val());
            var net_sales_ec=parseFloat($("#net_sales_ec").val());
            var curr_pend_ec=parseFloat($("#curr_pend_ec").val());
            var offline_rej_ec=parseFloat($("#offline_rej_ec").val());
            var dep_sum_ec=parseFloat($("#dep_sum_ec").val());
            
            
            //--- debit
   
    var tot_debits = ((prev_pend_deb + net_sales_deb - curr_pend_deb - offline_rej_deb) ); 
      //var tot_debits = tot_debits.toFixed(2);
     var debit_diff = ((dep_sum_deb - (tot_debits)));
   
     
     $("#tot_debits,#tot_debits2").val(convertnulltozero(tot_debits));
     $("#debit_diff").val(convertnulltozero(debit_diff));
     
   //--- ebt
     var tot_ebt = ((prev_pend_ebt + net_sales_ebt - curr_pend_ebt - offline_rej_ebt) ); 
   
      var ebt_diff = ((dep_sum_ebt - (tot_ebt))); 
      
       $("#tot_ebt,#tot_ebt2").val(convertnulltozero(tot_ebt));
     $("#ebt_diff").val(convertnulltozero(ebt_diff));
   
   //--- credit
      var tot_credits =  ((prev_pend_cr + net_sales_cr - curr_pend_cr - offline_rej_cr) );
   
      var credit_diff =  ((dep_sum_cr - (tot_credits)) ); 
      
      $("#tot_credits,#tot_credits2").val(convertnulltozero(tot_credits));
     $("#credit_diff").val(convertnulltozero(credit_diff));
   
   //--- gc
      var tot_gc =((prev_pend_gc + net_sales_gc -  curr_pend_gc - offline_rej_gc) );
   
      var gc_diff = ((dep_sum_gc - (tot_gc)));
      
      $("#tot_gc,#tot_gc2").val(convertnulltozero(tot_gc));
     $("#gc_diff").val(convertnulltozero(gc_diff));
   
   //--- sc (store credit)
      var tot_sc =((prev_pend_sc + net_sales_sc - curr_pend_sc - offline_rej_sc) );
   
      var sc_diff = ((dep_sum_sc - (tot_sc)));
      
      $("#tot_sc,#tot_sc2").val(convertnulltozero(tot_sc));
     $("#sc_diff").val(convertnulltozero(sc_diff));
   
   //--- ec (E-Check)
      var tot_ec = ((prev_pend_ec + net_sales_ec - curr_pend_ec - offline_rej_ec) );
   
  
      var ec_diff = ((dep_sum_ec - (tot_ec)));
    
      $("#tot_ec,#tot_ec2").val(convertnulltozero(tot_ec));
     $("#ec_diff").val(convertnulltozero(ec_diff));
   
   
            //--- tot all
             var tot_deb_cred = (debit_diff + ebt_diff + gc_diff + sc_diff + ec_diff + credit_diff);
   
             $("#tot_deb_cred").val(convertnulltozero(tot_deb_cred));
      }
      
      
      
    });

</script>


@endsection
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>    
<script type="text/javascript">
 //alert('Hii');
   $(document).ready(function() {
      //alert('Hii');
    $(".thisselect").on("click", function () {
      $(this).select();
   });
 });
</script>