@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <?php $d=strtotime("last Saturday"); $lastsaturday = date("Y-m-d", $d);?>
   <div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >POSTAGE STAMP INVENTORY</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-bookpostagepost')}}" id="booksafedate" class="form-horizontal" method="post" role="form" style="display: block;">
              
                  <div class="form-group">
                     <!-- <div class="col-sm-6"> -->
                     <label style="" for="inputPassword" class="control-label col-sm-7">Please enter the Date of the Information</label>
                     <!-- </div> -->
                     <div class="col-sm-5">
                        <!-- <input type="date" class="form-control" id="info_date" name="info_date"{{ (Input::old('info_date'))?'
                           value="'.Input::old('info_date').'"':''}} placeholder="MM-DD-YY" /> -->
                        <input type="date" class="form-control" id=""  name="dateofinfo" value="{{$lastsaturday}}">
                        @if($errors->has('dateofinfo'))
                        {{ $errors->first('dateofinfo')}}
                        @endif       
                     </div>
                  </div>
              
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
@section('jssection')
@parent
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $( function() {
     $( "#dateofinfo" ).datepicker({
         changeMonth: true,
       changeYear: true,
       showButtonPanel: true,
       dateFormat:"mm/dd/yy"
     });
   } );
</script>
@endsection
@stop