     
@extends('layout.dashboard')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<div class="container">
   <div class="row">
      <div class="col-md-6">
         <h3></h3>
      </div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
        {{-- <a href="{{ route('pdf-report-plusout',['download'=>'pdf'])}}" target="_blank"> --}}
           <!--   <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
           </a> -->
         </span>
      </div>
   </div>
</div>
<div class="container">
<h4> Sales Quantities for DSD Items for {{$results[0]['name']}}</h4>
{{-- @if ($print_order_result_array) --}}
   <table class="table table-striped" id="example">
      <thead>
        <tr>
          <th colspan="5" style="padding-left: 53%">------------------------------Previous Ad----------------------------</th>
        </tr>
         <tr>
		    <th>Description</th>
        <th>Ad Retail</th>
        <th>Retail</th>
        <th>Sold</th>
        <th>Cases</th>
      </tr>
    </thead>
    <tbody>
    <tr id="tabdata">
        <td colspan="5" style="text-align:center;">No Records found!</td>
    </tr>
 </tbody>
   </table>

</div>
@stop
        
