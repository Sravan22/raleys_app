<?php 
   //echo count($reg_array_final1[0]);exit;
   //echo '<pre>',print_r($reg_array_final1);exit();
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
         {{--  <a href="" target="_blank">
         <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
         </a> --}}
         </span>
      </div>
   </div>
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="panel">
         <div class="" style="margin-left: 78px;margin-bottom:-23px;font-weight:bold;">
            <div class="panel-title">{{-- {{  date("m/d/Y", strtotime($txDate)) }} --}}</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Raley's / Bel Air</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">SAFE COUNT</div>
         </div>
         <div class="col-md-4"></div>
         <div class="col-md-4" style=" text-align:center; font-weight:bold;">
            <div class="panel-title"> Store Number : {{ $str_num }} &nbsp;&nbsp;&nbsp; DATE : {{ date('m/d/Y',strtotime($rpt_date)) }}</div>
         </div>
         <div class="col-md-4"></div>

         {{-- @if($reg_array_final1) --}}
         <div style="padding-top:30px" class="panel-body">
            <table class="table table-bordered table-colored">
               <thead>
                  <tr>
                     <th colspan="5" class="text-center">COIN</th>
                  </tr> 
                  <tr>
                     <th>Description</th>
                     <th>Boxed</th>
                     <th>Rolled</th>
                     <th></th>
                     <th class="text-right">Total</th>
                  </tr>  
               </thead>
               <tbody>
                   <tr>
                     <td><strong>Pennies</strong></td>
                     <td>{{ number_format((float)$safe_coin_array[0]['pennies_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_coin_array[0]['pennies_roll']/100, 2, '.', ''); }}</td>
                     <td></td>
                     <td class="text-right">{{ number_format((float)$safe_coin_array[0]['pennies_box']/100 + $safe_coin_array[0]['pennies_roll']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Nickles</strong></td>
                     <td>{{ number_format((float)$safe_coin_array[0]['nickles_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_coin_array[0]['nickles_roll']/100, 2, '.', ''); }}</td>
                     <td></td>
                     <td class="text-right">{{ number_format((float)$safe_coin_array[0]['nickles_box']/100 + $safe_coin_array[0]['nickles_roll']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Dimes</strong></td>
                     <td>{{ number_format((float)$safe_coin_array[0]['dimes_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_coin_array[0]['dimes_roll']/100, 2, '.', ''); }}</td>
                     <td></td>
                     <td class="text-right">{{ number_format((float)$safe_coin_array[0]['dimes_box']/100 + $safe_coin_array[0]['dimes_roll']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Quarters</strong></td>
                     <td>{{ number_format((float)$safe_coin_array[0]['quarters_box']/100, 2, '.', '');  }}</td>
                     <td>{{ number_format((float)$safe_coin_array[0]['quarters_roll']/100, 2, '.', ''); }}</td>
                     <td></td>
                     <td class="text-right">{{ number_format((float)$safe_coin_array[0]['quarters_box']/100 + $safe_coin_array[0]['quarters_roll']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Misc {{ $safe_coin_array[0]['misc_desc'] }}</strong> </td>
                     <td>{{ number_format((float)$safe_coin_array[0]['misc_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_coin_array[0]['misc_roll']/100, 2, '.', ''); }}</td>
                     <td></td>
                     <td class="text-right">{{ number_format((float)$safe_coin_array[0]['misc_box']/100 + $safe_coin_array[0]['misc_roll']/100, 2, '.', ''); }}</td> 
                   </tr>
               </tbody>
            </table>
         </div>
          
        {{--  @endif    --}}


        <div style="padding-top:30px" class="panel-body">
            <table class="table table-bordered table-colored">
               <thead>
                  <tr>
                     <th colspan="5" class="text-center">CURRENCY</th>
                  </tr> 
                  <tr>
                     <th>Description</th>
                     <th>Left</th>
                     <th>Right</th>
                     <th>Cash Held</th>
                     <th class="text-right">Total</th>
                  </tr>  
               </thead>
               <tbody>
                   <tr>
                     <td><strong>Ones</strong></td>
                     <td>{{ number_format((float)$safe_curr_array[0]['ones_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['ones_roll']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['ones_held']/100, 2, '.', ''); }}</td>
                     <td class="text-right">{{ number_format((float)$safe_curr_array[0]['ones_box']/100 + $safe_curr_array[0]['ones_roll']/100 + $safe_curr_array[0]['ones_held']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Fives</strong></td>
                     <td>{{ number_format((float)$safe_curr_array[0]['fives_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['fives_roll']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['fives_held']/100, 2, '.', ''); }}</td>
                     <td class="text-right">{{ number_format((float)$safe_curr_array[0]['fives_box']/100 + $safe_curr_array[0]['fives_roll']/100 + $safe_curr_array[0]['fives_held']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Tens</strong></td>
                     <td>{{ number_format((float)$safe_curr_array[0]['tens_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['tens_roll']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['tens_held']/100, 2, '.', ''); }}</td>
                     <td class="text-right">{{ number_format((float)$safe_curr_array[0]['tens_box']/100 + $safe_curr_array[0]['tens_roll']/100 + $safe_curr_array[0]['tens_held']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Twenties</strong></td>
                     <td>{{ number_format((float)$safe_curr_array[0]['twenties_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['twenties_roll']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['twenties_held']/100, 2, '.', ''); }}</td>
                     <td class="text-right">{{ number_format((float)$safe_curr_array[0]['twenties_box']/100 + $safe_curr_array[0]['twenties_roll']/100 + $safe_curr_array[0]['twenties_held']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Misc  {{ $safe_curr_array[0]['misc1_desc'] }}</strong> </td>
                     <td>{{ number_format((float)$safe_curr_array[0]['misc1_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['misc1_roll']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['misc1_held']/100, 2, '.', ''); }}</td>
                     <td class="text-right">{{ number_format((float)$safe_curr_array[0]['misc1_box']/100 + $safe_curr_array[0]['misc1_roll']/100 + $safe_curr_array[0]['misc1_held']/100, 2, '.', ''); }}</td> 
                   </tr>
                   <tr>
                     <td><strong>Misc  {{ $safe_curr_array[0]['misc2_desc'] }}</strong> </td>
                     <td>{{ number_format((float)$safe_curr_array[0]['misc2_box']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['misc2_roll']/100, 2, '.', ''); }}</td>
                     <td>{{ number_format((float)$safe_curr_array[0]['misc2_held']/100, 2, '.', ''); }}</td>
                     <td class="text-right">{{ number_format((float)$safe_curr_array[0]['misc2_box']/100 + $safe_curr_array[0]['misc2_roll']/100 + $safe_curr_array[0]['misc2_held']/100, 2, '.', ''); }}</td> 
                   </tr>
               </tbody>
            </table>
         </div>


         <div style="padding-top:30px" class="panel-body">
            <table class="table table-bordered table-colored">
               <thead>
                  <tr>
                     <th colspan="2" class="text-center">Merchandise Checks</th>
                  </tr> 
                  <tr>
                     <th>Description</th>
                     <th class="text-right">Total</th>
                  </tr>
                   
               </thead>
               <tbody>
                   <tr>
                     <th>Fives</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['merch_fives']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Tens</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['merch_tens']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Twenties</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['merch_twenties']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Misc</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['merch_misc']/100, 2, '.', ''); }}</td>
                 </tr> 
               </tbody>
             </table>
         </div>

         <div style="padding-top:30px" class="panel-body">
            <table class="table table-bordered table-colored">
               <thead>
                  <tr>
                     <th colspan="2" class="text-center">Script Checks</th>
                  </tr> 
                  <tr>
                     <th>Description</th>
                     <th class="text-right">Total</th>
                  </tr>
                   
               </thead>
               <tbody>
                   <tr>
                     <th>Script 1</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script1']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Script 2</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script2']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Script 3</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script3']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Script 4</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script4']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Script 5</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script5']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Script 6</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script6']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Script 6</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script6']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Script 7</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script7']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Script 8</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script8']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Script 9</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script9']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Script 10</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['script10']/100, 2, '.', ''); }}</td>
                 </tr>
               </tbody>
             </table>
         </div>
         <br>
         <br>
         <div style="padding-top:30px" class="panel-body">
            <table class="table table-bordered table-colored">
               <thead>
                  <tr>
                     <th colspan="2" class="text-center">MISC</th>
                  </tr> 
                  <tr>
                     <th>Description</th>
                     <th class="text-right">Total</th>
                  </tr>
                   
               </thead>
               <tbody>
                   <tr>
                     <th>Food Stamps</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['food_stamps']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Merchandise Checks</th>
                     <td class="text-right">{{ number_format((float)($safe_other_array[0]['merch_fives']+$safe_other_array[0]['merch_tens']+$safe_other_array[0]['merch_twenties']+$safe_other_array[0]['merch_misc'])/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Script Checks</th>
                     <td class="text-right">{{ number_format((float)($safe_other_array[0]['script1']+$safe_other_array[0]['script2']+$safe_other_array[0]['script3']+$safe_other_array[0]['script5']+$safe_other_array[0]['script6']+$safe_other_array[0]['script7']+$safe_other_array[0]['script8']+$safe_other_array[0]['script4']+$safe_other_array[0]['script9']+$safe_other_array[0]['script10'])/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Beginning Cash Loans</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['begin_loans']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Misc {{ $safe_other_array[0]['misc1_desc'] }}</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['misc1_amt']/100, 2, '.', ''); }}</td>
                 </tr> 
                 <tr>
                     <th>Misc {{ $safe_other_array[0]['misc2_desc'] }}</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['misc2_amt']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>In-Store Charges</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['instr_chrg']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Payroll Advances</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['pay_adv']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Night Crew Till</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['night_crew']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>E.P.S. Debit to Carryover</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['debit']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>E.P.S. Credit to Carryover</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['credit']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>EPS EBT To Carryover</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['ebt']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                     <th>Misc {{ $safe_other_array[0]['misc3_desc'] }}</th>
                     <td class="text-right">{{ number_format((float)$safe_other_array[0]['misc3_amt']/100, 2, '.', ''); }}</td>
                 </tr>
                 <tr>
                    <th><h4>TOTAL SAFE COUNT</h4></th>
                    <td class="text-right"><strong><span class="grand_total">{{ number_format((float)$grand_tot, 2, '.', ''); }}</span></strong></td>
                 </tr>
               </tbody>
             </table>
         </div>
      </div>
   </div>
</div>
@stop