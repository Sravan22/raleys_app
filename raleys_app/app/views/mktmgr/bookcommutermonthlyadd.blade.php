@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>

<div class="container">
            <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    	  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-8 col-md-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >MTHLY COMMUTER INVENTORY</div>
                       </div> 

                 <div style="padding-top:15px" class="panel-body" >

						
								<form action="{{URL::route('mktmgr-bookcommutermonthlyshow')}}" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;">
			
             <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-6">Please enter the inventory type</label>
            <div class="col-sm-6">
                <select class="form-control" id="inv_type" name="inv_type">
                    <option value="">Select</option>
                    @foreach($data->invtypeitemlist as $key => $value)
                     <option value="{{ $value['id'] }}">{{ $value['id'] }} : {{ $value['value'] }} </option>   
                    @endforeach
                </select>
                 
                 @if($errors->has('inv_type'))
            {{ $errors->first('inv_type')}}
            @endif
            </div>
        </div>
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-6">Please enter the description</label>
            <div class="col-sm-6">
               <select class="form-control" id="desc" name="desc">
                    
                    <option value="">Select</option>
                </select>
                
                
                 @if($errors->has('desc'))
            {{ $errors->first('desc')}}
            @endif
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-6">Please enter the month</label>
            <div class="col-sm-6">
                <select class="form-control" id="comm_month" name="comm_month">
                    <option value="">Select</option>
                    @for($m=1;$m<=12;$m++){
                     <option value="{{ $m }}">{{ date('F', mktime(0, 0, 0, $m, 10)); }} </option>   
                    @endfor
                </select>
                 
                 @if($errors->has('comm_month'))
            {{ $errors->first('comm_month')}}
            @endif
            </div>
        </div>					
        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-6">Please enter the date of the information</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="dateofinfo" placeholder="" name="dateofinfo" value="{{$data->dateofinfo}}">
                 @if($errors->has('dateofinfo'))
            {{ $errors->first('dateofinfo')}}
            @endif
            </div>
        </div>
									
									<div class="form-group">
										<div class="row">
											 <div class="col-sm-12" align="center">
				<input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                <input type="reset" value="Reset" class="btn">
                <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                                                {{ Form::token()}}
											</div>
										</div>
									</div>
									
								</form>
								
							
					</div>
				</div>
			</div>
		</div>


<meta name="csrf-token" content="{{ csrf_token() }}" />



@section('jssection')
 @parent
 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
    $( "#dateofinfo" ).datepicker({
        changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat:"mm/dd/yy"
    });
  } );
  </script>
    <script type="text/javascript">
  var aurl = "{{URL::route('mktmgr-bookcommuterrecapitemslistmonthly')}}";
  
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
    
    $.ajaxSetup({
        //header:$('meta[name="csrf-token"]').attr('content')
         headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
    })
    
   
    
    

        
        $("#inv_type").on('change',function(){
            
           $('#desc').find('option').remove().end().append('<option value="">Select</option>');
    
                $.ajax({
                  url: aurl, 
                  type: "GET",
                  data:{_token: CSRF_TOKEN,inv_type:$(this).val()},
                  dataType: "json",                  
                  success: function( data ) {
                      
                      $.each(data, function (i, item) {
                        $('#desc').append($('<option>', { 
                            value: item.id,
                            text : item.id + " : "+item.value 
                        }));
                    });
                      
                      
                  }
                });
                
                
              });
  
    });
  
  </script>
      @endsection
@stop
