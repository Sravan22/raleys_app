<?php
   //echo '<pre>';print_r($sodsdcwk_rec_array);exit;
?>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storetransfersmenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel_heading" >
            <div class="panel-title" >Weekly Transfers Accounting Recap Report</div>
         </div>
         <div class="panel_body" >
            {{-- <form action="{{URL::route('post-store-weekly-acc-report')}}" class="form-horizontal" method="post" role="form" style="display: block;"> --}}
            <form action="{{URL::route('post-store-weekly-acc-report')}}" class="form-horizontal" method="post" role="form" >
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <div class="col-md-5">
                     <label for="cur_wk_bgn_date" class="control-label col-sm-12">From Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control keyfirst" id="" placeholder="" name="cur_wk_bgn_date" 
                            value="{{ $sodsdcwk_rec_array[0]['cur_wk_bgn_date'] }}" tabindex="2" autofocus="">
                  </div>
               </div>
               <div class="form-group padding_bottom">
                  <div class="col-md-5">
                     <label for="cur_wk_end_date" class="control-label col-sm-12">To Date</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="date" class="form-control" id="" placeholder="" name="cur_wk_end_date" 
                            value="{{ $sodsdcwk_rec_array[0]['cur_wk_end_date'] }}" tabindex="3">
                  </div>
               </div>
              <!--  <div class="form-group">
                  <div class="col-md-5">
                     <label for="inputPassword" class="control-label col-sm-12">Copies</label>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="" placeholder="Number of Copies" name="copies" value="1">
                  </div>
               </div>
                -->
               <div class="form-group padding_bottom">
                  <div class="row">
                     <div class="col-md-4"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="reset" tabindex="5"  class="btn keylast" value="Reset">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
                <div class="focusguard" id="focusguard-2" tabindex="6"></div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop