<?php 
  //echo '<pre>',print_r($results);exit();
?>

@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="panel">
      <div class="" style="margin-left: 140px;margin-bottom:-23px;font-weight:bold;">
            <div class="panel-title">Store : 355</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title"> Actual Ending Inventory List  </div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <div class="table-responsive ">
               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th class="text-center">Sales Date</th>
                        <th class="text-center">Grade</th>
                        <th class="text-center">Actual Ending Inventory(Gallons)</th>
                     </tr>
                  </thead>
                  <tbody>
                  @foreach($results as $row)
                  <tr>   
                        <td class="text-center">{{ date("m/d/Y", strtotime($row->tx_date)) }}</td>
                        <td class="text-center">{{ $row->descr }}</td>
                        <td class="text-center">{{ number_format((float)$row->act_end_inv, 2, '.', ''); }}</td>
                  </tr>
                  @endforeach
                  
                  </tbody>
               </table>
               {{ $pagination->links() }}
            </div>
         </div>
      </div>
   </div>
</div>
@stop