<?php
  //echo $dayIsLocked;exit;
  //echo '<pre>';print_r($begin_loan_data);exit;
  // $total = $begin_loan_data[0]->food_stamps + $begin_loan_data[0]->ones
  //           + $begin_loan_data[0]->fives + $begin_loan_data[0]->tens
  //           + $begin_loan_data[0]->twenties + $begin_loan_data[0]->rolled_coins
  //           + $begin_loan_data[0]->quarters + $begin_loan_data[0]->nickles
  //           + $begin_loan_data[0]->dimes + $begin_loan_data[0]->pennies
  //           + $begin_loan_data[0]->misc; 
            //echo $total;
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style type="text/css">
  .form-horizontal .control-label {
    text-align: right !important;
    /* padding-left: 60px; */
}
</style>
<div class="container">
   <div class="flash-message">
   @if($dayIsLocked == 'Y')
   <p class="alert alert-info">Data is locked for {{ date("m/d/Y", strtotime($loandate)) }}. Any changes won't be saved <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
   @endif
</div>
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >BEGIN LOAN DETAIL &nbsp;&nbsp;&nbsp; Register {{ $reg_number }} &nbsp;&nbsp;&nbsp;{{ date('m/d/Y',strtotime($loandate)) }}</div>
         </div>
         <form action="@if($dayIsLocked != 'Y') {{URL::route('mktmgr-postbeginloanreg-update')}} @endif" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;">
            <div class="form-group">
               <label for="inputEmail" class="control-label col-sm-4">Food Stamps</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" autofocus="" id="food_stamps" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->food_stamps, 2, '.', ''); }}" name="food_stamps" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('food_stamps'))
                  {{ $errors->first('food_stamps')}}
                  @endif
               </div>
            </div>
            <input type="hidden" name="reg_num" value="{{ $reg_number }}">
            <input type="hidden" name="entry_date" value="{{ $loandate }}">
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Ones</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="ones"  name="ones" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->ones, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('ones'))
                  {{ $errors->first('ones')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Fives</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="fives"  name="fives" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->fives, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('fives'))
                  {{ $errors->first('fives')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Tens</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="tens"  name="tens" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->tens, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('tens'))
                  {{ $errors->first('tens')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Twenties</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="twenties"  name="twenties" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->twenties, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('twenties'))
                  {{ $errors->first('twenties')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Rolled Coins</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="rolled_coins"  name="rolled_coins" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->rolled_coins, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('rolled_coins'))
                  {{ $errors->first('rolled_coins')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Quarters</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="quarters"  name="quarters" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->quarters, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('quarters'))
                  {{ $errors->first('quarters')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Dimes</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="dimes"  name="dimes" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->dimes, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('dimes'))
                  {{ $errors->first('dimes')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Nickles</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="nickles"  name="nickles" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->nickles, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('nickles'))
                  {{ $errors->first('nickles')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Pennies</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="pennies"  name="pennies" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->pennies, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('pennies'))
                  {{ $errors->first('pennies')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-4">Misc</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control alltotal" maxlength="9" id="misc"  name="misc" placeholder="0.00" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="{{ number_format((float)$begin_loan_data[0]->misc, 2, '.', ''); }}" onKeyPress="return StopNonNumeric(this,event)">
                  @if($errors->has('misc'))
                  {{ $errors->first('misc')}}
                  @endif
               </div>
            </div>
            <hr style="margin-right: 105px;">
            <div class="form-group">
               <label for="inputPassword" class="col-sm-4 text-right">Total</label>
               <div class="col-sm-8">
               <b><span class="totalvalue">{{ number_format((float)$bl_total/100, 2, '.', ''); }}</span></b>
                  <input type="hidden" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> class="form-control totalvalue" id="bl_total"  name="bl_total" readonly="readonly" value="{{ $bl_total }}">
                  {{-- <input type="hidden" name="querymake" id="querymake" value="" /> --}}
                  @if($errors->has('bl_total'))
                  {{ $errors->first('bl_total')}}
                  @endif
               </div>
            </div>
            <hr style="margin-right: 105px;">
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="row">
               @if($file_locked == 'Y')
               <div class="col-sm-12" align="center">
                  <input type="button" style="margin-right:90px;" value="Go Back" class="btn" id="cancel" name="cancel" onclick="goBack()">
                  {{ Form::token()}}
               </div>
               @else
               <div class="col-sm-12" align="center">
                  <input type="submit" style="margin-left:-51px;" name="login-submit" id="submit" tabindex="4" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?>  value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeperregister')}}'" />
                  {{ Form::token()}}
               </div>
               @endif                
            </div>
            </div>
          <br>
          @if($file_locked == "Y")
          <div class="row" style="margin-right: 96px;">
                  <div class="col-md-1">&nbsp;</div>
                  <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
                  <div class="col-md-1">&nbsp;</div>
               </div>
               @endif
         </form>
      </div>
   </div>
</div>


<style type="text/css">
  @media (min-width: 992px){
.col-md-offset-1 {
    margin-left: 18.333333% !important;
}
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>     
<script>
 $(document).ready(function() {

  $(".form-control").keydown(function (e) {
   // Allow: backspace, delete, tab, escape, enter 
   
   if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl/cmd+A
       (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
       (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
       (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
       (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
   }

   // Ensure that it is a number and stop the keypress
   if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
       e.preventDefault();
   }

 
   });

    $(".alltotal").on("click", function () {
      $(this).select();
   });
 });

 /*$(".alltotal").one('click',function(){  
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval*100));
        $(".totalvalue").text(sum.toFixed(2));
       return true;
    });

 $(".alltotal").one('blur',function(){  
        var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
        $('.alltotal').each(function() { 
         sum += parseFloat($(this).val()); 
        });
        $(".totalvalue").val(sum.toFixed(2));
       $(".totalvalue").text(sum.toFixed(2));
       return true;
    }); */ 
    $(".alltotal").focus(function(event) {
      var sum = 0; 
      selfield = this.id;
      //this.select();
      // $("#safecoinsubmit").removeAttr('disabled');
      selfieldval=$("#"+selfield).val(); 
      $("#"+selfield).val(parseFloat(selfieldval*100));
      this.select();
      });
 $(".alltotal").blur(function(){  
       var sum = 0; 
        selfield = this.id;
       
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
        $('.alltotal').each(function() { 
         sum += parseFloat($(this).val()); 
        });
        $(".totalvalue").val(sum.toFixed(2));
       $(".totalvalue").text(sum.toFixed(2));
      
       return true;
     });

   function goBack() {
       window.history.back();
   }
   /*
   function StopNonNumeric(el, evt)
       {
           var charCode = (evt.which) ? evt.which : event.keyCode;
           var number = el.value.split('.');
           if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
               return false;
           }
           //just one dot (thanks ddlab)
           if(number.length>1 && charCode == 46){
                return false;
           }
           //get the carat position
           var dotPos = el.value.indexOf(".");
           if( dotPos>-1 && (number[1].length > 3)){
               return false;
           }
           return true;
       }
       */
setTimeout(function() { $(".flash-message").hide(); }, 2500);
</script>  
@stop
