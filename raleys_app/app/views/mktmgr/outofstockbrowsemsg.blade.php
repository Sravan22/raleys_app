@extends('layout.dashboard')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>

<div class="container" style="">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> 
       <p class="alert alert-info">A Query must be performed prior to using this option : <a href="{{ URL::route('mktmgr-outofstockquery')}}">Query</a>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></p>
  </div>
  

@stop

