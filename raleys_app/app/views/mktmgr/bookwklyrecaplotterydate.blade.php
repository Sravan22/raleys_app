
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
 <!--   <div class="flash-message" id="recapmsg">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </p>
    @endif
    @endforeach
 </div> -->
   <!-- end .flash-message -->
   <div id="loginbox" style="margin-top:5px;" class="mainbox col-md-7 col-md-offset-3 col-sm-6 col-sm-offset-2">
      <!--   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-2">   -->                  
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Open Lottery Inventory</div>
         </div>
         <div class="panel-body" >
           
            <form action="{{URL::route('mktmgr-wklyrecap-lotteryinv')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <table class="table table-striped">
                  <thead>
                     <tr>
                     <th></th>
                        <th>Game</th>
                        <th>Date</th>
                        <th>End Inv</th>
                        <th>Value</th>
                        <th></th>
                        <th>Inv In Hand</th>
                     </tr>
                  </thead>
                  <tbody>
                    <input type="hidden" name="recapDate" value="<?php echo $calcdate; ?>">
                     <?php
                     for($i = 0; $i < count($openInvCurs); $i++ )
                     {
                     
                    ?>
                    <tr>
                    <td></td>
                  <input type="hidden" name="gameno<?php echo $i;?>" value="<?php echo $openInvCurs[$i]['gameno'];?>"/>
                  <input type="hidden" name="endinv<?php echo $i;?>" value="<?php echo $openInvCurs[$i]['endinv'];?>"/>
                  <input type="hidden" name="ticketvalue<?php echo $i;?>" value="<?php echo $openInvCurs[$i]['ticketvalue'];?>"/>
                    
                    {{-- lottery date --}}

                   <?php $lotterydate = date("m-d-Y", strtotime($openInvCurs[$i]['lotterydate']));?>
                      <td><?php echo $openInvCurs[$i]['gameno'];?></td>
                      <td><?php echo $lotterydate;?></td>

                      <td>{{ number_format((float)$openInvCurs[$i]['endinv'], 2, '.', ''); }}
                      </td>
                      <td>{{ number_format((float)$openInvCurs[$i]['ticketvalue'], 2, '.', ''); }}
                      </td>
                      <td></td>
                      <td>
                      <div>
                       <input type="text" class="form-control multiply"  id="inhand<?php echo $i;?>" name="inhand<?php echo $i;?>" value="<?php echo $openInvCurs[$i]['inhand'];?>">
                       <span class="control-label"></span>
                      </div>
                       </td>
                       <input type="hidden"  name="count"  value="<?php echo $i;?>">
                    </tr>
                      <?php
                      
                     }     
                     ?>
                    

                     
                  </tbody>
               </table>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <span class="control-label"></span>
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>        
<script type="text/javascript">
 
   $(document).ready(function(){

       
        $(".multiply").focus(function(event) {
        var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        
        $("#"+selfield).val(Math.round(selfieldval*100));
        this.select();

             /*checking is number is less than 100*/
            $("#"+selfield).on('keyup',function(){
              var num = $(this).val(); 
              
               console.log(num)

                  if(num < 100) {
                     $(this).parent().addClass('has-error');
                     $(this).next('.control-label').text('Invalid number')
                     $('#submit').addClass('disabled');
                     }
                  else {
                     $(this).parent().addClass('has-success');
                      $(this).parent().removeClass('has-error');
                      
                       $(this).next('.control-label').text('');
                        $('#submit').removeClass('disabled');
                     }
            })
      });
        $('.multiply').blur(function(event) {
         selfield = this.id;
         selfieldval=$("#"+selfield).val();
         $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
      });

          $("input.form-control").keydown(function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
          // Allow: Ctrl/cmd+A
          (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
          // Allow: Ctrl/cmd+C
          (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
          // Allow: Ctrl/cmd+X
          (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
          // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
          // let it happen, don't do anything
          return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
          }
          });

       
       

      /* calcdate =
       $("#continue").click(function(){
           $.post("runprocessweekly", {calcdate: calcdate}, function(result){ 
            alert('hiii');return false;
       });*/
     //$("#placevendorlist").html($makedropdown);
   });
      /* $("#cancelrecap").click(function(){
   msg='<p class="alert alert-info">Recap calculation aborted at u<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></p>';
            $("#recapmsg").html('');
            $("#recapmsg").html(msg);
       });*/
   });
</script>
@stop
