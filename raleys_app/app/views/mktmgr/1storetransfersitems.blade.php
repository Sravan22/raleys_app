<?php
   // echo count($StoreItemArray)."<br>";
   // echo $StoreItemArray[0]['item_desc'];
   // echo '<pre>';print_r($item_upper_part_details)."<br>";

   // echo  $item_upper_part_details['transfer_number'];
   // exit;
   // exit;
   ?>
  
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storetransfersmenu')
</header>
<div class="container">
  
      <div class="row" style="margin-left:150px;">
       <div class="col-md-2">&nbsp;</div>
       <div class="col-md-3 text-right"><b>Store Transfer Number</b></div>
       <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
       <div class="col-md-4 text-left">{{ $item_upper_part_details['from_store_no'] }} - {{ $item_upper_part_details['transfer_number']  }}</div>
       <div class="col-md-2">&nbsp;</div>
    </div>
    <div class="row" style="margin-left:150px;">
       <div class="col-md-2">&nbsp;</div>
       <div class="col-md-3 text-right"><b>From Store/Dept</b></div>
       <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
       <div class="col-md-4 text-left">{{ $item_upper_part_details['from_store_no'] }} / {{ $item_upper_part_details['from_dept_no']  }}</div>
       <div class="col-md-2">&nbsp;</div>
    </div>
    <div class="row" style="margin-left:150px;">
       <div class="col-md-2">&nbsp;</div>
       <div class="col-md-3 text-right"><b>To Store/Dept</b></div>
       <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
       <div class="col-md-4 text-left">{{ $item_upper_part_details['to_store_no'] }} / {{ $item_upper_part_details['to_dept_no']  }}</div>
       <div class="col-md-2">&nbsp;</div>
    </div>
    <div class="row" style="margin-left:150px;">
       <div class="col-md-2">&nbsp;</div>
       <div class="col-md-3 text-right"><b>From Account</b></div>
       <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
       <div class="col-md-4 text-left">{{ $item_upper_part_details['from_account'] }}  {{  $item_upper_part_details['from_account_desc']  }}</div>
       <div class="col-md-2">&nbsp;</div>
    </div>
    <div class="row" style="margin-left:150px;">
       <div class="col-md-2">&nbsp;</div>
       <div class="col-md-3 text-right"><b>To Account</b></div>
       <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
       <div class="col-md-4 text-left">{{ $item_upper_part_details['to_account']  }}  {{ $item_upper_part_details['to_account_desc'] }} </div>
       <div class="col-md-2">&nbsp;</div> 
    </div>
    <div class="row" style="margin-left:150px;">
       <div class="col-md-2">&nbsp;</div>
       <div class="col-md-3 text-right"><b>TOTAL AMOUNT</b></div>
       <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
       <div class="col-md-4 text-left">${{ $item_upper_part_details['tot_value'] }}</div>
       <div class="col-md-2">&nbsp;</div>
    </div>



<br><br><br>
<div class="row">
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>UPC Number</th>
            <th>SKU Number</th>
            <th>Description</th>
            <th>Qty</th>
            <th>Units</th>
            <th>Cost</th>
            <th>Ext Cost</th>
         </tr>
      </thead>
      <tbody>
         @for ($i = 0; $i < count($StoreItemArray); $i++)
         <tr>
            <td>{{ $StoreItemArray[$i]['upc_number'] }}</td>
            <td>{{ $StoreItemArray[$i]['item_number'] }}</td>
            <td>{{ $StoreItemArray[$i]['item_desc'] }}</td>
            <td>{{ $StoreItemArray[$i]['quantity'] }}</td>
            

            @if($StoreItemArray[$i]['qty_units']=="C")
            <td>CS</td>
            <td>${{ number_format($StoreItemArray[$i]['upc_cost'] * $StoreItemArray[$i]['case_pack'],2) }}</td>
            <td>${{ number_format(($StoreItemArray[$i]['upc_cost'] * $StoreItemArray[$i]['case_pack']) * $StoreItemArray[$i]['quantity'] ,2)}}</td>
            @endif


            @if($StoreItemArray[$i]['qty_units']=="E")
            <td>EA</td>
                @if($StoreItemArray[$i]['discount_rate']==0)
                    <td>${{ number_format($StoreItemArray[$i]['upc_cost'],2) }}</td>
                @else
                <?php $e_cost =  $StoreItemArray[$i]['rtl_amt'] * (100 - $StoreItemArray[$i]['discount_rate']) / 100; ?>
                 <td>${{ number_format($e_cost,2) }}</td>
                @endif    
                <td>${{ number_format($StoreItemArray[$i]['upc_cost'] * $StoreItemArray[$i]['quantity'],2) }}</td>
            @endif

            
             @if($StoreItemArray[$i]['qty_units']=="R")
            <td>RT</td>
                @if($StoreItemArray[$i]['discount_rate']==0)
                <?php $r_cost =  ($StoreItemArray[$i]['rw_rtl_amt'] *  $StoreItemArray[$i]['rtl_amt'])/ $StoreItemArray[$i]['upc_cost']; ?>
                    <td> ${{ number_format($r_cost,2) }}</td>
                @else
                <?php $r_cost =  ($StoreItemArray[$i]['rw_rtl_amt'] *  $StoreItemArray[$i]['rtl_amt']) * ($StoreItemArray[$i]['rtl_amt']) * (100 - $StoreItemArray[$i]['discount_rate']) / 100; ?>
                 <td>${{ number_format($r_cost,2) }}</td>
                @endif    
                <td>${{ number_format($StoreItemArray[$i]['upc_cost'] * $StoreItemArray[$i]['upc_cost'],2) }}</td>
            @endif

           



            @if($StoreItemArray[$i]['qty_units']=="M")
            <td>MS</td>
            <td>${{ $StoreItemArray[$i]['upc_cost'] }}</td>
            <td>${{ number_format($StoreItemArray[$i]['upc_cost']  *  $StoreItemArray[$i]['quantity'],2) }} </td>   
            @endif            


         </tr>
         @endfor
      </tbody>
   </table>
</div>
@stop




