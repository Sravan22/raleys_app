<?php
  //echo '<pre>';print_r($adjsales);exit;
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<form action="{{URL::route('mktmgr-postbookwklysdetailssave')}}" id="coin-form" class="form-horizontal" 
   method="post" role="form" style="display: block;" onsubmit="return checkamounts()">
   <div class="container">
      <div class="flash-message">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <!-- end .flash-message -->
      <!------------------- Department Sales popup---------------------------->      
      <div id="deptsalestot"  class="mainbox miscbox col-md-12" style="display: none;">
         <div class="panel panel-info safe_total" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Department Sales : {{ $infodate }}</div>
            </div>
            <div class="row">
               <div  class="panel-body" style="margin-top: 10px;">
                  <div class="col-xs-6"  >
                     <div class="form-horizontal">
                        <div class="form-group">
                           <div class="col-xs-4">
                              <label for="inputPassword" style="margin-left: 14px;" class="control-label col-sm-12">DEPARTMENT</label>
                           </div>
                           <div class="col-sm-4">
                              <label for="inputPassword" style="text-align: right;" class="control-label col-sm-12">SALES</label>
                           </div>
                           <div class="col-sm-4">
                              <label for="inputPassword" style="text-align: right" class="control-label col-sm-10">CUST CT</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6" >
                     <div class="form-group">
                        <div class="col-xs-4">
                           <label for="inputPassword" class="control-label col-sm-12">DEPARTMENT</label>
                        </div>
                        <div class="col-sm-4">
                           <label for="inputPassword" style="text-align: right;" class="control-label col-sm-12">SALES</label>
                        </div>
                        <div class="col-sm-4">
                           <label for="inputPassword" style="text-align: right;" class="control-label col-sm-10">CUST CT</label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div  class="panel-body" style="margin-top: 10px;">
             
                  <?php $gtot=0; //echo '<pre>'; print_r($depttot); echo '</pre>'; exit;  ?>
                  @foreach($depttot as $key => $value)
                  <?php 
                     $gtot=$gtot+$value->gettot;
                     ?>
                  <div class="col-xs-6" >
                     <div class="form-group">
                        <div class="col-sm-6">
                           <label for="inputPassword" class="control-label col-sm-12">{{ $value->item_desc }}</label>
                        </div>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" readonly="readonly" value="{{ number_format((float)$value->gettot, 2, '.', '');  }}">
                        </div>
                        <div class="col-sm-3">
                           <input type="text" class="form-control" readonly="readonly" value="{{ $value->custcnt }}">
                        </div>
                     </div>
                  </div>
                  @endforeach  
               
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <?php $gtot= number_format((float)$gtot, 2, '.', ''); ?>
               <label for="inputPassword" style="text-align: right;" class="control-label col-sm-6">Total Sales</label>
               <div class="col-sm-3">
                  <input  class="form-control grandtotal"  readonly="readonly" placeholder="0.00" id="grandtot"  
                     name="misc_grand_total" value="{{ $gtot }}">
                  @if($errors->has('otherpricetot'))
                  {{ $errors->first('otherpricetot')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <!-- <input type="button" name="" id="misc_submit" tabindex="4" value="Submit" class="btn"> -->
                     <input type="button" id="deptallval" tabindex="4" value="Submit" class="btn">
                     <input type="reset" id="" tabindex="4" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('mktmgr/bookwklysales')}}'">       <!-- onclick="pushtot('deptsalestot','mainboxpopup','grandtot')"> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------------------ End of Department Sales popup ---------------------->
      <!------------------- Sales Adjustment popup ---------------------------->      
      <div id="adjustmentpopup" style="margin-left: 10px;display: none;" class="mainbox miscbox col-md-12">
         <div class="panel panel-info safe_total">
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Department Sales Adjustments : {{ $infodate }}</div>
            </div>
            <div  class="panel-body" style="margin-top: 10px;">
                                 <div class="col-xs-6" >
                     <div class="form-group">
                        <div class="col-xs-4">
                           <label for="inputPassword" class="control-label col-sm-12">DEPARTMENT</label>
                        </div>
                        <div class="col-sm-4">
                           <label for="inputPassword" class="control-label col-sm-12" style="text-align: right;">SALES</label>
                        </div>
                        <div class="col-sm-4">
                           <label for="inputPassword" style="text-align: right;" class="control-label col-sm-10">CUST CT</label>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6" >
                     <div class="form-group">
                        <div class="col-xs-4">
                           <label for="inputPassword" class="control-label col-sm-12">DEPARTMENT</label>
                        </div>
                        <div class="col-sm-4">
                           <label for="inputPassword" style="text-align: right;" class="control-label col-sm-12">SALES</label>
                        </div>
                        <div class="col-sm-4">
                           <label for="inputPassword" style="text-align: right;" class="control-label col-sm-10">CUST CT</label>
                        </div>
                     </div>
                  </div>
             
            </div>
            <div  class="panel-body" style="margin-top: 10px;">
               
                  <?php $gtotadj = 0; $i = 1; ?> 
                  @foreach($adjsales as $key => $value)
                  <?php 
                     $salesid='salesid'.$i; $custcnt = 'custcntid'.$i; $itemidcnt = 'itemidcntid'.$i; 
                     ?>
                  <div class="col-xs-6" >
                     <div class="form-group">
                        <div class="col-sm-6">
                           <label for="inputPassword" class="control-label col-sm-12">{{ $value->item_desc }}</label>
                           <input type="hidden" id="{{ $itemidcnt }}" name="{{ $itemidcnt }}" value="{{ $value->item_id }}">
                        </div>
                        <div class="col-sm-3">
                           <input type="number" class="form-control caltot" id="{{ $salesid }}" name="{{ $salesid }}" 
                               placeholder="0.00" value="0.00">
                        </div>
                        <div class="col-sm-3">
                           <input type="number" placeholder="0" class="form-control" id="{{ $custcnt }}" name="{{ $custcnt }}">
                        </div>
                     </div>
                  </div>
                  <?php $i++; ?>
                  @endforeach  
       
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <?php $gtotadj = number_format((float)$gtotadj, 2, '.', ''); ?>
               <label for="inputPassword" style="text-align: right;" class="control-label col-sm-6">Total Sales</label>
               <div class="col-sm-3">
                  <input type="hidden" id="totrows" name="totrows" value="{{ $i }}">
                  <input  class="form-control grandtotal"  readonly="readonly" placeholder="0.00" id="gtotadj"  
                     step="0.01" name="adj_grand_total" value="{{ $gtotadj }}">
                  @if($errors->has('otherpricetot'))
                  {{ $errors->first('otherpricetot')}}
                  @endif
               </div>
            </div>
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <!-- <input type="button" name="" id="misc_submit" tabindex="4" value="Submit" class="btn"> -->
                     <input type="button" id="adjallval" tabindex="4" value="Submit" class="btn">
                     <input type="reset" id="" tabindex="4" value="Cancel" class="btn" 
                        onclick="document.location.href='{{URL::to('mktmgr/bookwklysales')}}'">
                     <!-- onclick="pushtot('adjustmentpopup','mainboxpopup','gtotadj')"> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------------------End Sales Adjustment popup---------------------->
      <!-- start of main div -->
      <div id="mainboxpopup" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Department Sales &nbsp;&nbsp;&nbsp;&nbsp;{{ $infodate }}</div>
            </div>
            <div class="panel-body" >
               <br>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-offset-1 col-sm-4">Customer Cnt</label>
                  <div class="col-sm-6">
                     <input  type="number" placeholder="Count" class="form-control cc" id="customercnt"  name="customercnt" 
                        value="{{ $cust_cnt }}">
                     @if($errors->has('customercnt'))
                     {{ $errors->first('customercnt')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-offset-1 col-sm-4">Competitor Coupon</label>
                  <div class="col-sm-6">
                     <input type="number" placeholder="0.00" class="form-control cc" id="comptitorcoupon" 
                        name="comptitorcoupon" value="{{ $comp_coupon }}">
                     @if($errors->has('comptitorcoupon'))
                     {{ $errors->first('comptitorcoupon')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-offset-1 col-sm-4">Total Sales</label>
                  <div class="col-sm-6">
                    <?php 
                   $totsale_query = DB::select('SELECT SUM(recapmain.dept_sales) as saletot FROM recapmain,recapitems WHERE recapitems.item_type IN ("M","J") AND recapmain.dept_id NOT IN ("CC") AND recapitems.item_id = recapmain.dept_id AND recapmain.wk_end_date = "'.$dateofinfo.'" ');
                   //echo '<pre>';print_r($totsale_query);exit;
                    $totsale=($totsale_query[0]->saletot)/100;


               $adjusttotal_query = DB::select('SELECT SUM(recapmain.dept_sales) as adjtot FROM recapmain,recapitems WHERE recapitems.item_type = "A"
                      AND recapitems.item_id = recapmain.dept_id
                      AND recapmain.wk_end_date = "'.$dateofinfo.'" ');
               $adjusttotal=($adjusttotal_query[0]->adjtot)/100;
                    //echo $totsale;exit;
                    // for($i = 0; $i < count($depttot); $i++)
                    //  {
                    //     $totsale=$totsale+$depttot[$i]->gettot;
                    //  }
                     ?>
                     <input type="button"  onclick="hide_show_toggle_safecnt('deptsalestot','mainboxpopup')" style="width:190px;" readonly="readonly" placeholder="0.00"  id="totsale" name="totsale" class="btn" value="<?php if($totsale) { echo $totsale; } else { echo '0.00'; } ?>"> 
                     @if($errors->has('totsale'))
                     {{ $errors->first('totsale')}}
                     @endif
                  </div>
                  {{-- 
                  <div class="col-sm-3">
                     <input type="button" class="btn" id="showdetail_coin" placeholder="" value="show detail" 
                        onclick="hide_show_toggle_safecnt('deptsalestot','mainboxpopup')">
                  </div>
                  --}}
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-offset-1 col-sm-4">Adjustments</label>
                  <div class="col-sm-6">
                     {{--                      <input  class="form-control grandtotal" type="text" placeholder="0.00" readonly="readonly" 
                        id="adjustments" name="adjustments"> --}}
                     <input type="button"  onclick="hide_show_toggle_safecnt('adjustmentpopup','mainboxpopup')"  placeholder="0.00"  class="btn" readonly="readonly"  value="<?php if($adjusttotal) { echo $adjusttotal; } else { echo '0.00'; } ?>"
                        tabindex="4" style="width:190px;"
                        id="adjustments" name="adjustments" >
                     @if($errors->has('adjustments'))
                     {{ $errors->first('adjustments')}}
                     @endif
                  </div>
                  {{-- 
                  <div class="col-sm-3">
                     <input type="button" class="btn" id="showdetail_coin" placeholder="" value="show detail" 
                        onclick="hide_show_toggle_safecnt('adjustmentpopup','mainboxpopup')">
                  </div>
                  --}}
               </div>
               <hr style="border:1px solid #000;" />
               <div class="form-group">
                  <!-- <div class="col-sm-3">&nbsp;</div> -->
                  <label for="inputPassword" class="control-label col-sm-offset-1 col-sm-4">TOTAL ADJ SALES</label>
                  <?php $totaladjsales = $totsale + $adjusttotal; ?>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" placeholder="0.00"  value="<?php
                        if($totaladjsales)
                        {
                           echo $totaladjsales;
                        }
                        else
                        {
                           echo '0.00';
                        }
                     ?>" 
                        id="GrandTot"  name="GrandTot">
                     @if($errors->has('otherpricetot'))
                     {{ $errors->first('otherpricetot')}}
                     @endif
                  </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center" style="margin-left:-31px;">
                        <input type="submit" name="safe-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" onclick="history.go(-1);" name="" id="submit" tabindex="4" value="Cancel" class="btn">
                     </div>
                  </div>
               </div>
               <!-- </form> -->
            </div>
         </div>
      </div>
      <!-- end of main div -->
   </div>
</form>
<!-- </div> -->
@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 

<script type="text/javascript">
   $(document).ready(function() {
       $(".caltot").on("click", function () {
      $(this).select();
   });
   
   
     $(".cc").each(function() {
          $(this).keyup(function() {
          calculateSum();
         });
     });
   
   
   function calculateSum() {
     var sum = 0;
     $(".cc").each(function() {
         if (!isNaN(this.value) && this.value.length != 0) {
             sum += parseFloat(this.value);
          }
     });
     $(".cctotal").val(sum.toFixed(2));
   }
   
    $("#adjustmentpopup").hide();
    $("#deptsalestot").hide();
   
    $("#adjallval").click(function(){
         $("#adjustmentpopup").hide();
         $("#mainboxpopup").show();
       var tot = $("#gtotadj").val(); 
         $("#adjustments").val(tot);
         $("#btnAddProfile").prop('value', tot);
         finaltot();

    });
   
    $("#deptallval").click(function(){
       $("#deptsalestot").hide();
       $("#mainboxpopup").show();
    });
   
  /* $(".caltot").one('click',function(){  
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval*100));
        $("#gtotadj").val(sum.toFixed(2));
       $("#adjustments").text(sum.toFixed(2));
       return true;
    });
*/
/* $(".caltot").one('blur',function(){  
        var sum = 0; 
        selfield = this.id;
        //alert(selfield);return false;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
        $('.caltot').each(function() { 
         sum += parseFloat($(this).val()); 
        });
        $("#gtotadj").val(sum.toFixed(2));
       $("#adjustments").text(sum.toFixed(2));
      finaltot();
       return true;
    });*/
      $(".caltot").blur(function(){  
       var sum = 0; 
        selfield = this.id;
         
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
        $('.caltot').each(function() { 
         sum += parseFloat($(this).val()); 
        });
        $("#gtotadj").val(sum.toFixed(2));
       $("#adjustments").text(sum.toFixed(2));
      finaltot();
       return true;
     });
     



   
  /*  $(".caltot").blur(function(){  
         var sum = 0; 
         selfield = this.id;
         selfieldval=$("#"+selfield).val(); 
         $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
        $('.caltot').each(function() { 
         sum += parseFloat($(this).val()); 
         });
        $("#gtotadj").val(parseFloat(sum).toFixed(2));
        $("#adjustments").val(parseFloat(sum).toFixed(2));
   
        finaltot();
        return true;
     });
   */
   
    
   });
   
   function finaltot()
   {
   totsale=$("#totsale").val();
   adjustments=$("#adjustments").val();
   gt = (parseFloat(totsale)+parseFloat(adjustments)).toFixed(2);
   $("#GrandTot").val(gt);
   }  
   
   function hide_show_toggle_safecnt(hiddiv, maindiv)
   {
    $("#"+hiddiv).show();
    $("#"+maindiv).hide(); 
   }
</script>
