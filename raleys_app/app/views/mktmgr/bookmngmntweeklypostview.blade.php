@extends('layout.dashboardbookkeepermarket')
@section('page_heading','Weekly')
@section('content')
@section('section')

<header class="row">
    @include('mktmgr.bookkepermenu')
</header>
<div class="container">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >Recap Report - {{date('m/d/Y',strtotime($date_stamp))}}</div>
            </div> 

            <div style="padding-top:10px" class="panel-body" >


                <form action="{{URL::route('mktmgr-post-bookmngmntweeklymgrok')}}" class="form-horizontal" method="post" role="form" style="display: block;">
                <input type="hidden" name="recap_date" value="{{ $date_stamp }}">
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Dept Sales</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('DEPARTMENT SALES')" name="DEPARTMENT SALES" tabindex="4" value="<?php echo number_format($wk_dept_sales/100,2); ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                  <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Net Safe</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('NET SAFE')" name="NET SAFE" tabindex="4" value="<?php echo number_format($wk_net_safe/100,2); ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Deposits</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('DEPOSIT DETAIL')" name="DEPOSIT DETAIL" tabindex="4" value="<?php echo number_format($wk_deposits/100,2); ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Delivery & Transfers</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('DELIVERY AND TRANSFERS')" name="DELIVERY AND TRANSFERS" tabindex="4" value="<?php echo number_format($wk_del_tranfs/100,2); ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Store Transactions</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('STORE TRANSACTIONS')" name="STORE TRANSACTIONS" tabindex="4" value="<?php echo number_format($wk_store_tx/100,2); ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Paid Out Types</label>
                        <div class="col-sm-7">
                            <input type="button" onclick="showdetails('PAID OUT TYPES')" name="PAID OUT TYPES" tabindex="4" value="<?php echo number_format($wk_po_types/100,2); ?>" class="btn" style="width:225px;">
                            @if($errors->has(''))
                            {{ $errors->first('')}}
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-5">Total</label>
                        <div class="col-sm-7">
                            <?php echo number_format($wk_summary/100,2); ?>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 20px; ">
                         <label for="inputPassword" class="control-label col-sm-3" style="text-align: right;">DRUG (O)S</label>
                        <div class="col-sm-2" style="padding-top: 7px; ">
                            <?php echo number_format($wk_drgov,2); ?>
                        </div>
                         
                         <label for="inputPassword" class="control-label col-sm-3">MARKET (O)S</label>
                        <div class="col-sm-2 " style="padding-top: 7px;">
                            <?php echo number_format($wk_grov,2); ?>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <input type="submit" name="mgmtapproval-submit" id="submit" tabindex="4" value="MGMT Approval" class="btn">
                                <input type="submit" name="accept-submit" id="submit" tabindex="4" value="Accept" class="btn">
                                <input type="button" name="cancel" value="Cancel" class="btn" onclick="window.location.href='{{URL::route('mktmgr-bookmngmntweekly')}}'">
                                {{ Form::token()}}
                            </div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('jssection')
@parent
<script>
parentHtml = $("#loginbox").html();
function showdetails(t) {
    if(t == 'CURRENCY DEPOSITS' || t == 'CHECK DEPOSITS' || t == 'VISA DEPOSITS' || t == 'EXPRESS DEBIT' || t == 'DELIVERIES' || t == 'TRANSFERS') {
        htmlStore = $("#loginbox").html();
    }else {
        htmlStore = parentHtml;
    }
    $.ajax({
        url:'{{URL::route('mktmgr-post-bookmngmntweeklyshowdetails')}}',
        type:'GET',
        data: "t="+t+"&date_stamp={{$date_stamp}}",
        success:function(data){
            $("#loginbox").html(data);
        }
    });
}

function clickCancel() {
    $("#loginbox").html(htmlStore);
    htmlStore = parentHtml;
}
</script>
@endsection
@stop
@endsection
@stop
