@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>
<div class="container">
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Expenses </div>
         </div>
         <div style="padding-top:5px" class="panel-body">
         <br>
            <div class="row">
               <div class="col-md-5 text-right"><b>Store</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left">305</div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Vendor</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span>{{ $rcv_data->name }}</span> </div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Vendor Number</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span>{{ $rcv_data->vendor_number }}</span></div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Invoice Date</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span>{{ date("m/d/Y", strtotime($rcv_data->invoice_date)) }}</span></div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Invoice Number</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span>{{ $rcv_data->id }}</span></div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Expense Type</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><?php if($rcv_data->type_code == 'G'){ echo 'General Repair'; } elseif($rcv_data->type_code == 'S'){ echo 'Supply'; } else { echo 'Service'; }?></div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Amount</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span>{{ number_format($rcv_data->tot_vend_cost,2); }}</span></div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Status</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span><?php if($rcv_data->status_code == 'O'){ echo 'Open'; }   else { echo 'Void'; }?></span></div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Created At</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span>{{date("m/d/Y H:i:s", strtotime($rcv_data->create_datetime)); }}</span></div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Last Updated </b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span>{{date("m/d/Y H:i:s", strtotime($rcv_data->last_update)); }}</span></div>
            </div>
            <div class="row">
               <div class="col-md-5 text-right"><b>Last Updated By</b></div>
               <div class="col-md-1">:</div>
               <div class="col-md-6 text-left"><span>{{ $rcv_data->last_updated_by }}</span></div>
            </div>
            <br>
            <div class="row">
               <div class="col-sm-12">
                  <input type="button" tabindex="8" style="margin-left: 52%" id="btn" class="btn" value="Cancel" onclick="history.go(-1);">
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
</div>
@stop