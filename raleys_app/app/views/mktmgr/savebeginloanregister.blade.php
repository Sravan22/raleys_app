@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<div class="container">
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
    	  <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >BEGINNING LOANS REGISTER</div>
                       </div> 
     <form action="{{URL::route('mktmgr-postbeginloanreg')}}" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;">
	       <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-4">Register Number</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="reg_number"  name="reg_number" value="{{{ $data['reg_number'] }}}" readonly="readonly">
            </div>
        </div>	

        <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-4">Date</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="reg_date"  name="reg_date" value="{{{ $data['reg_date'] }}}" readonly="readonly">
            </div>
        </div>  						    

                                <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-4">Food Stamps</label>
        <div class="col-sm-8">
            <input type="floatnumber" class="form-control alltotal" id="food_stamps" placeholder="Food Stamps" value="{{ $beginloadtabdata['food_stamps']}}" name="food_stamps">
                 @if($errors->has('food_stamps'))
            {{ $errors->first('food_stamps')}}
            @endif
            </div>
        </div>

        

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Ones</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="ones"  name="ones" value="{{ $beginloadtabdata['ones']}}" >
                 @if($errors->has('ones'))
            {{ $errors->first('ones')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Fives</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="fives"  name="fives" value="{{ $beginloadtabdata['fives']}}">
                 @if($errors->has('fives'))
            {{ $errors->first('fives')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Tens</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="tens"  name="tens" value="{{ $beginloadtabdata['tens']}}">
                 @if($errors->has('tens'))
            {{ $errors->first('tens')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Twenties</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="twenties"  name="twenties" value="{{ $beginloadtabdata['twenties']}}">
                 @if($errors->has('twenties'))
            {{ $errors->first('twenties')}}
            @endif
            </div>
        </div>

         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Rolled Coins</label>
            <div class="col-sm-8">
        <input type="floatNumber" class="form-control alltotal" id="rolled_coins"  name="rolled_coins" value="{{ $beginloadtabdata['rolled_coins']}}">
                 @if($errors->has('rolled_coins'))
            {{ $errors->first('rolled_coins')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Quarters</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="quarters"  name="quarters" value="{{ $beginloadtabdata['quarters']}}">
                 @if($errors->has('quarters'))
            {{ $errors->first('quarters')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Dimes</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="dimes"  name="dimes" value="{{ $beginloadtabdata['dimes']}}">
                 @if($errors->has('dimes'))
            {{ $errors->first('dimes')}}
            @endif
            </div>
        </div>


        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Nickles</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="nickles"  name="nickles" value="{{ $beginloadtabdata['nickles']}}">
                 @if($errors->has('nickles'))
            {{ $errors->first('nickles')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Pennies</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="pennies"  name="pennies" value="{{ $beginloadtabdata['pennies']}}">
                 @if($errors->has('pennies'))
            {{ $errors->first('pennies')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Misc</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control alltotal" id="misc"  name="misc" value="{{ $beginloadtabdata['misc']}}">
                 @if($errors->has('misc'))
            {{ $errors->first('misc')}}
            @endif
            </div>
        </div>

        <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-4">Total</label>
            <div class="col-sm-8">
                <input type="floatNumber" class="form-control" id="bl_total"  name="bl_total" readonly="readonly" value="{{ $beginloadtabdata['bl_total']}}">
                <input type="hidden" name="querymake" id="querymake" value="{{ $querymake }}" />
                 @if($errors->has('bl_total'))
            {{ $errors->first('bl_total')}}
            @endif
            </div>
        </div>
        <div class="form-group">
            <div class="row">
            <div class="col-sm-12" align="center">
             
              <input type="button" style="width: 140px;margin-left: 75px;" value="Cancel" class="btn" id="cancel" name="cancel" onclick="goBack()">
              {{ Form::token()}}
            </div>
            </div>
        </div>
       @if($approvalsOk == 'N' )
        <div class="form-group">
			<div class="row">
			<div class="col-sm-12" align="center">
			  <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
              <input type="button" value="Cancel" class="btn" id="cancel" name="cancel" onclick="goBack()">
              {{ Form::token()}}
			</div>
			</div>
		</div>
       @endif 
       </form>
		</div>
		</div>
		</div>
		</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>     
<script type="text/javascript">
    $("#misc").blur(function(){
        tot=calnet();
        $("#total").val(tot);
    });

    
    $(".alltotal").blur(function(){  var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
       $('.alltotal').each(function() { 
        sum += parseFloat($(this).val()); 
        });
       $("#bl_total").val(sum.toFixed(2));
       return true;
    });
     
     function calnet() {
     
     food_stamps=$("#food_stamps").val();   
     misc=$("#misc").val();
     pennies=$("#pennies").val();
     nickles=$("#nickles").val();
     dimes=$("#dimes").val();
     quarters=$("#quarters").val();
     twenties=$("#twenties").val();
     tens=$("#tens").val();
     fives=$("#fives").val();
     ones=$("#ones").val();

      if(food_stamps == ''){food_stamps = 0;}  
     if(misc == ''){misc  = 0; }
     if(pennies == ''){pennies  = 0; }
     if(nickles == ''){nickles  = 0; }
     if(dimes == ''){dimes  = 0; }
     if(quarters == ''){quarters  = 0; }
     if(twenties == ''){twenties  = 0; }
     if(tens == ''){tens  = 0; }
     if(fives == ''){fives  = 0; }
     if(ones == ''){ones  = 0; }
     

     return tot=parseInt(food_stamps)+parseInt(misc)+parseInt(pennies)+parseInt(nickles)+parseInt(dimes)+parseInt(quarters)+parseInt(twenties)
         +parseInt(tens)+parseInt(fives)+parseInt(ones);

     //$("#total").val(tot);
    }
 </script>
 <script>
function goBack() {
    window.history.back();
}
</script>  
@stop