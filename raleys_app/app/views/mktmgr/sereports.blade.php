@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>
<div class="container">
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Expenses Report</div>
         </div>
         <div class="panel-body" style="margin-top:10px">
            <form action="{{URL::route('mktmgr-post-receivings-sereports')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">From Date</label>
                  <div class="col-sm-8">
                     <input type="date" class="form-control" id="fromdate" autofocus="" placeholder="MM-DD-YY" name="fromdate" tabindex="2" />
                     @if($errors->has('fromdate'))
                     {{ $errors->first('fromdate')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">To Date</label>
                  <div class="col-sm-8">
                     <input type="date" class="form-control" id="todate" placeholder="MM-DD-YY" name="todate" tabindex="3"/>
                     @if($errors->has('todate'))
                     {{ $errors->first('todate')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 45%">
                        <input type="submit"  tabindex="4" value="Search" class="btn">
                        <input type="reset" name="cancel-btn" id="btn" tabindex="5" value="Reset" class="btn">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="6"></div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
   $('#cancel-btn').click(function() {
     location.reload();
   });
   $(function() {
     $('#focusguard-2').on('focus', function() {
  $('#fromdate').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#btn').focus();
});

   });
</script>
@stop
