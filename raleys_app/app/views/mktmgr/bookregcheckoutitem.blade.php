@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<link rel="stylesheet" href="{{ asset("assets/stylesheets/mystyle.css") }}" />
<?php $previewsdate =  date('Y-m-d', strtotime('-1 day')); ?>
<div class="container">
   <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <div class="mainbox">
      <div class="panel panel-info">
         <div class="panel_heading">
            <div class="panel-title">REGISTER CHECK OUT</div>
         </div>
         {{-- <form action="{{URL::route('mktmgr-savebookregcheckoutitem')}}" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;"> --}}
         <form action="{{URL::route('mktmgr-registercheckoutitems')}}" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;">
            <div class="form-group">
               <label for="itemid_name" class="control-label col-sm-5">Category code</label>
               <div class="col-sm-6">
                  <select class="form-control" id="itemid_name" name="itemid_name">
                     <option value="">Choose Category code</option>
                     @foreach ($itemslist as $key => $value) 
                     <option value="{{ $value->item_id }}^{{$value->item_desc }}">{{ $value->item_id }} : {{ $value->item_desc }}</option>
                     @endforeach
                  </select>
                  @if($errors->has('itemid_name'))
                  {{ $errors->first('itemid_name')}}
                  @endif
               </div>
            </div>
            <div class="form-group padding_bottom">
               <label for="dateofinfo" class="control-label col-sm-5"> Date of the Information</label>
               <div class="col-sm-6">
                  <input type="date" class="form-control" id="dateofinfo" placeholder="" name="dateofinfo" value="{{ $previewsdate }}">
                  @if($errors->has('dateofinfo'))
                  {{ $errors->first('dateofinfo')}}
                  @endif
               </div>
            </div>
            {{-- <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                     <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                     {{ Form::token()}}
                  </div>
               </div>
            </div> --}}
            <div class="form-group padding_bottom">
                 <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                       <input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                       <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" />
                       {{ Form::token()}}
                    </div>
                 </div>
               </div>
         </form>
      </div>
   </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>        
<script type="text/javascript">
   $(function(){
                   var dtToday = new Date();
                   
                   var month = dtToday.getMonth() + 1;
                   var day = dtToday.getDate();
                   var year = dtToday.getFullYear();
                   if(month < 10)
                       month = '0' + month.toString();
                   if(day < 10)
                       day = '0' + day.toString();
                   
                   var maxDate = year + '-' + month + '-' + day;
                   $('#dateofinfo').attr('max', maxDate);
               });
</script>
@stop