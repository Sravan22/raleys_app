@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<style>

.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    cursor: pointer;
    background-color: #eee;
    opacity: 1;
}
</style>
<form action="{{URL::route('mktmgr-postiuregcheckoutreg')}}" class="form-horizontal" method="post" role="form" style="display: block;">
<!-- start of department sale div -->
    <div class="container allhideboxs" id="departmentsales">
    <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
     <div style="" class="mainbox col-md-12">
      <div class="panel panel-info safe_total">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >Detail By Register Screen &nbsp;&nbsp;&nbsp; Register No: {{ $reg_num }} 
                 &nbsp;&nbsp;&nbsp; Date : {{ $convertedate }} </div>
          <input type="hidden" name="seldate" id="seldate" value="{{ $dateofinfo }}">
          <input type="hidden" name="selregnum" id="selregnum" value="{{ $reg_num }}">
      </div>

     <div  class="panel-body col-md-offset-1" style="padding-top: 20px;" class="mainbox ">
        
         <?php $gtot='00.00'; $i=1;?>
     @foreach($resultArray as $row)
        <?php 
              $dbval = '00.00'; $itemfield='itemamt'.$i; $itemcode='itemcode'.$i;
              if($row['item_amt'] != '')
              {
                 $gtot=$gtot+$row['item_amt'];
                 $dbval=number_format($row['item_amt'], 2, '.', '');
              }
              $i=$i+1;
        ?>
            <div class="col-xs-6" >
               <div class="form-group">
                  <div class="col-sm-6">
                     <label for="inputPassword" class="control-label">({{$row['itemid']}}){{$row['itemdesc'] }}</label>
                  </div>
                  <div class="col-sm-4">
                     <input type="hidden" class="form-control" name="{{ $itemcode }}"  id="{{ $itemcode }}" value="{{ $row['itemid'] }}">
                     <input type="number" class="form-control alltotal" name="{{ $itemfield }}"  id="{{ $itemfield }}" value="{{ $dbval }}" step="0.01">
                  </div>
               </div>
            </div>
     
     @endforeach  
     </div>
       
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;padding-bottom: 10px;">Grand Total</label>
            <div class="col-sm-3">
               <input type="number"  class="form-control grandtotal"  readonly="readonly" id="gtot"  
                       name="gtot" value="{{ number_format($gtot, 2, '.', '') }}" step="0.01">
                <input type="hidden" name="numrows" id="numrows" value="{{ $i }}">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
      @if($dayIsLocked == 'N')
         <div class="form-group">
          <div class="row">
             <div class="col-sm-12" align="center">
               <input type="submit" name="dept-sales" id="dept-sales" tabindex="4" value="Submit" class="btn showmain">
                        <!-- <input type="button" name="" id="submit" tabindex="4" value="Cancel" class="btn"> -->
             </div>
           </div>
         </div>
      @endif   
    </div> 
          </div>
     </div>
      <!-- end of department sale div -->
 </form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>      
 <script type="text/javascript">
    
    $(".alltotal").blur(function(){ 
      var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
       $('.alltotal').each(function() { 
        sum += parseFloat($(this).val()); 
        });
       sum = parseFloat(sum).toFixed(2);
       $("#gtot").val(sum);
       return true;
    });

 </script>
@stop