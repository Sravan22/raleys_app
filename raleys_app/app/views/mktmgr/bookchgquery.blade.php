@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
   <div class="col-md-12">
   <br>
 @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
      
      @endif
    @endforeach
    </div>
<div class="container">
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >CHG Query</div>
         </div>
         <div class="panel-body" >
            <form action="{{URL::route('mktmgr-post-bookchglist')}}" class="form-horizontal" method="post" role="form" style="display: block;"
            onsubmit="return validateForm()">
            <div class="focusguard" id="focusguard-1" tabindex="1"></div>

               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Register Number</label>
                  <div class="col-sm-6">
                     <input type="number" class="form-control" id="regnumber" placeholder="" name="regnumber" min="1" max="999" tabindex="2" autofocus="" />
                     @if($errors->has('regnumber'))
                     {{ $errors->first('regnumber')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6">Registration Type</label>
                  <div class="col-sm-6">
                     <input type="text" class="form-control" maxlength="1" id="regtype" placeholder="" name="regtype" style="text-transform: uppercase" tabindex="3" autocomplete="off" >
                     @if($errors->has('regtype'))
                     {{ $errors->first('regtype')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 53%">
                        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Search" class="btn">
                        <input type="button" value="Cancel" tabindex="5" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookkeeper')}}'" id="cancel" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="6"></di
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .form-horizontal .control-label {
   text-align: right; 
   /* padding-left: 60px; */
   }

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript">

   /*function validateForm()
   {
      var regtype = document.getElementById("regtype").value;
      regtype = regtype.toUpperCase();
      if(regtype != 'G')
      {
         $('#regtype').val('');
         $('#regtype').focus();
         alert('Registration Type must be  G');
         
          return false;
      }
      $('#regtype').val(regtype);
      return true;       
      
   }*/
   $(function() {
 $('#focusguard-2').on('focus', function() {
  $('#regnumber').focus();
});

$('#focusguard-1').on('focus', function() {
  $('#cancel').focus();
});
  
   });
   
</script>
@stop