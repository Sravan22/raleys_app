<?php
   //echo $msg;exit;
   //echo $dayIsLocked;exit;
   //echo "<pre>";print_r($loanitems_array);exit;
   //echo "<pre>";print_r($loanitems_BL_LR_GT_array);exit;
   //  $exclude = array('item_id'=>'GT','item_id'=>'LR','item_id'=>'BL');
   //  //echo "<pre>";print_r($exclude);
   // for($i=0;$i<count($loanitems_array);$i++)
   //          {
   //           if (in_array($loanitems_array[$i]['item_id'], $exclude))
   //           {
   //            echo "<pre>";print_r($loanitems_array[$i]['item_id']);
   //           }
   //          } exit;
   //echo "<pre>";print_r($lr_tot);
   //echo "<pre>";print_r($bl_tot);exit;
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="flash-message">
      @if($dayIsLocked == 'Y')
      <p class="alert alert-info">Data is locked for {{ date("m/d/Y", strtotime($loan_date)) }}. Any changes won't be saved <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >LOAN REPORT &nbsp;&nbsp;&nbsp;&nbsp;  Register {{ $reg_num }}  &nbsp;&nbsp;&nbsp;&nbsp; {{ date("m/d/Y", strtotime($loan_date)) }}</div>
         </div>
         <form action="@if($dayIsLocked != 'Y') {{URL::route('mktmgr-updateloantoregister')}} @endif" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;">
            <?php //$i=1; $gtot =0;
               ?>
            <div class="form-group">
               <input type="hidden" name="reg_num" value="{{ $reg_num }}">
               <input type="hidden" name="entry_date" value="{{ $loan_date }}">
               <div class="row">
                  <div class="col-md-6">
                     <label for="inputEmail" class="control-label">Beginning Loans.................</label>
                  </div>
                  <div class="col-md-6" style="padding-left:81px;">
                     <span><b>{{ number_format((float)$bl_tot, 2, '.', ''); }}</b></span>
                  </div>
               </div>
            </div>
            <?php 
               $lr_tot = 0;
               $exclude = array('BL','LR','GT');
               //echo '<pre>';print_r($exclude);exit;
               //$loanitems_BL_LR_GT_array[1]['item_id']
               //$exclude = array($bl,$lr);
               for($i=0;$i<count($loanitems_array);$i++)
               {
                  $lr_tot=$lr_tot+$loanitems_array[$i]['item_amt'];
                  if (in_array($loanitems_array[$i]['item_id'], $exclude)) continue;
                  //echo '<pre>';print_r($loanitems_array[$i]['item_id']);//exit;
                  ?>
            <div class="form-group">
               <label for="inputEmail" class="control-label col-sm-6">(<?php echo $loanitems_array[$i]['item_id']; ?>) <?php echo $loanitems_array[$i]['item_desc']; ?></label>
               <input type="hidden" name="item_id[]" value="<?php echo $loanitems_array[$i]['item_id']; ?>">
               <input type="hidden" name="item_desc[]" value="<?php echo $loanitems_array[$i]['item_desc']; ?>">
               <div class="col-sm-6">
                  <input type="text" class="form-control alltotal <?php if($loanitems_array[$i]['item_id']=='B1') { echo 'selectthis'; } ?>" id="<?php echo $loanitems_array[$i]['item_id']; ?>"  name="item_amt[]" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="<?php echo number_format((float)$loanitems_array[$i]['item_amt'], 2, '.', ''); ?>"  onKeyPress="return StopNonNumeric(this,event)">
               </div>
            </div>
            <?php
               }
               ?>
            <div class="form-group">
               <div class="row">
                  <div class="col-md-6">
                     <label for="inputEmail" class="control-label"> Loans To Registers Totals............</label>
                  </div>
                  <div class="col-md-6" style="margin-top:7px;padding-left:81px;">
                     <span class="totalvalue" style="font-weight:bold;"><b>{{ number_format((float)($lr_tot + $bl_tot), 2, '.', ''); }}</b></span>
                  </div>
               </div>
               <hr>
            </div>
            <div class="form-group">
               <div class="row">
                  @if($file_locked == 'Y')
                  <div class="col-sm-12 topspace" align="center">
                     <input type="button" value="Go Back" class="btn" id="cancel" name="cancel" onclick="goBack()">
                     {{ Form::token()}}
                  </div>
                  @else
                  <div class="col-sm-12" align="center">
                     <input type="submit" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?>  name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                     <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/loantoregregister')}}'" />
                     {{ Form::token()}}
                  </div>
                  @endif                
               </div>
            </div>
             <br>
            @if($file_locked == "Y")
            <div class="row">
               <div class="col-md-1">&nbsp;</div>
               <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
               <div class="col-md-1">&nbsp;</div>
            </div>
            @endif
            <br>
      </div>
   </div>
</div>
</div>     
</form>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<script>
   $(document).ready(function() {
      $(".alltotal").on("click", function () {
        $(this).select();
     });
      $(".selectthis").select();
   });
   
   /*$(".alltotal").one('click',function(){  
          selfield = this.id;
          selfieldval=$("#"+selfield).val(); 
          $("#"+selfield).val(parseFloat(selfieldval*100));
          $(".totalvalue").text(sum.toFixed(2));
         return true;
      });
   
   $(".alltotal").one('blur',function(){  
          var sum = 0; 
          selfield = this.id;
          selfieldval=$("#"+selfield).val(); 
          $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
          $('.alltotal').each(function() { 
           sum += parseFloat($(this).val()); 
          });
          $(".totalvalue").val(sum.toFixed(2));
         $(".totalvalue").text(sum.toFixed(2));
         return true;
      });  
   */
   
   $(".alltotal").focus(function(event) {
        var sum = 0; 
        selfield = this.id;
        //this.select();
        // $("#safecoinsubmit").removeAttr('disabled');
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval*100));
        this.select();
        });
   $(".alltotal").blur(function(){  
         var sum = 0; 
          selfield = this.id;
         
          selfieldval=$("#"+selfield).val(); 
          $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
          $('.alltotal').each(function() { 
           sum += parseFloat($(this).val()); 
          });
          $(".totalvalue").val(sum.toFixed(2));
         $(".totalvalue").text(sum.toFixed(2));
        
         return true;
       });
   $("input.form-control").keydown(function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
               // Allow: Ctrl/cmd+A
              (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
               // Allow: Ctrl/cmd+C
              (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
               // Allow: Ctrl/cmd+X
              (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
               // Allow: home, end, left, right
              (e.keyCode >= 35 && e.keyCode <= 39)) {
                   // let it happen, don't do anything
                   return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
      });
   
       /*
   $(".alltotal").focus(function(event) {
        var sum = 0; 
        selfield = this.id;
        //this.select();
        // $("#safecoinsubmit").removeAttr('disabled');
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval*100));
        this.select();
        });
      $(".alltotal").blur(function(){  
         var sum = 0; 
          selfield = this.id;
         
          selfieldval=$("#"+selfield).val(); 
          $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
          $('.alltotal').each(function() { 
           sum += parseFloat($(this).val()); 
          });
          $(".totalvalue").val(sum.toFixed(2));
         $(".totalvalue").text(sum.toFixed(2));
        
         return true;
       });
   */
     function goBack() {
         window.history.back();
     }
       
   
</script>  
@stop