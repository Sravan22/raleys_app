<?php 
   //echo '<pre>';print_r($resultArray);exit;
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
 <div class="flash-message">
      @if($dayIsLocked == 'Y')
      <p class="alert alert-info">Data is locked for {{ date("m/d/Y", strtotime($reg_date)) }}. Any changes won't be saved <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
   </div>
<form action="@if($dayIsLocked != 'Y') {{URL::route('mktmgr-updateregcheckoutregister')}} @endif" class="form-horizontal" method="post" role="form" style="display: block;padding-top: 15px;">
   <div id="deptsalestot" style="margin-left: 100px;" class="mainbox miscbox col-md-10">
   <div class="panel panel-info safe_total">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">
            <div class="row">
               <div class="col-md-4"> Work By Register Screen</div>
               <div class="col-md-4">Register {{ $reg_num }} </div>
               <div class="col-md-4">{{ date("m/d/Y", strtotime($reg_date)) }}</div>
            </div>
            <input type="hidden" name="reg_num" value="{{ $reg_num }}">
            <input type="hidden" name="entry_date" value="{{ $reg_date }}">
         </div>
      </div>
      <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
         <div  class="panel-body scrollbar" id="style-1" style="margin-top: 10px;height:297px;">
            <?php //$gtot=0; 
               for($i=0;$i<count($resultArray);$i++) {?>
            <?php 
               if(!($resultArray[$i]['item_id'] == 'LR' || $resultArray[$i]['item_id'] == 'BL'))
                { ?>
            <input type="hidden" name="item_id[]" value="{{ $resultArray[$i]['item_id'] }}">
            <input type="hidden" name="item_desc[]" value="{{ $resultArray[$i]['item_desc'] }}">
            <?php }
               ?>
            <div class="col-xs-6" >
               <div class="form-group">
                  <div class="col-sm-2">
                     <label for="inputPassword">(<?php echo $resultArray[$i]['item_id']; ?>)</label>
                  </div>
                  <div class="col-sm-7">
                     <b><?php echo $resultArray[$i]['item_desc']; ?></b>
                  </div>
                  <div class="col-sm-3 alltotal">
                     <input type="text" class="form-control checktotal <?php echo $resultArray[$i]['type']; ?>" autocomplete="off" typeAtt="<?php echo $resultArray[$i]['type']; ?>" <?php if($resultArray[$i]['item_id'] == 'LR' || $resultArray[$i]['item_id'] == 'BL') { echo 'disabled'; } ?> style="width:100%;" id="<?php echo $resultArray[$i]['item_id']; ?>" name="item_amt[]" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="<?php echo number_format((float)$resultArray[$i]['item_amt'], 2, '.', ''); ?>" onKeyPress="return StopNonNumeric(this,event)">
                  </div>
               </div>
            </div>
            <?php } ?>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-2">Total Credits :</label>
            <div class="col-sm-2">
               <strong><span class="totalcredit"><b>{{ number_format((float)$redo_totals[0]['tot_cred'], 2, '.', ''); }}</b></span></strong>
            </div>
            <label for="inputPassword" class="control-label col-sm-2">Total Debits :</label>
            <div class="col-sm-2">
               <strong><span class="totaldebit"><b>{{ number_format((float)$redo_totals[0]['tot_deb'], 2, '.', ''); }}</b></span></strong>
            </div>
            <label for="inputPassword" class="control-label col-sm-2">Total O/S :</label>
            <div class="col-sm-2">
               <strong><span class="tot_os"><b>{{ number_format((float)$redo_totals[0]['tot_os'], 2, '.', ''); }}</b></span></strong>
            </div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="row">
         @if($file_locked == 'Y')
         <div class="col-sm-12 topspace" align="center">
            <input type="button" value="Go Back" class="btn" id="cancel" name="cancel" onclick="goBack()">
            {{ Form::token()}}
         </div>
         @else
         <div class="col-sm-12" align="center">
            <input type="submit" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
            <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/loantoregregister')}}'" />
            {{ Form::token()}}
         </div>
         @endif                
      </div>
   </div>
</form>
<style type="text/css">
   .scrollbar
   {
   /* background: #F5F5F5;*/
   overflow-y: scroll;
   }
   #wrapper
   {
   text-align: center;
   /* width: 500px;*/
   margin: auto;
   }
   /*
   *  STYLE 1
   */
   #style-1::-webkit-scrollbar-track
   {
   -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
   border-radius: 10px;
   background-color: #F5F5F5;
   }
   #style-1::-webkit-scrollbar
   {
   width: 12px;
   background-color: #F5F5F5;
   }
   #style-1::-webkit-scrollbar-thumb
   {
   border-radius: 10px;
   -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
   background-color: #555;
   }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<script>
   $(document).ready(function() {
      $(".alltotal .checktotal").on("click", function () {
        $(this).select();
     });
   });
   $(".alltotal .checktotal").focus(function(event) {
      var sum = 0; 
      selfield = this.id;
      //this.select();
      // $("#safecoinsubmit").removeAttr('disabled');
      selfieldval=$("#"+selfield).val(); 
      $("#"+selfield).val(parseFloat(selfieldval*100));
      this.select();
      });
   
   $(".alltotal .checktotal").on('blur',function(e){  
        e.stopPropagation();
          var c_sum = 0; 
          selfield = $(this).attr('typeAtt');
          selfid = this.id;
          selfidval=$("#"+selfid).val(); 
          $("#"+selfid).val(parseFloat(selfidval/100).toFixed(2));
          if(selfield == 'C')
          {
            $('.alltotal  .C').each(function() { 
             c_sum += parseFloat($(this).val()); 
            });
            $(".totalcredit").text(c_sum.toFixed(2));
          }
          d_sum = 0;
          if(selfield == 'D')
          {
            $('.alltotal  .D').each(function() { 
             d_sum += parseFloat($(this).val()); 
            });
           $(".totaldebit").text(d_sum.toFixed(2));
          }//tot_os
         $('.checktotal').each(function() { 
            $(".tot_os").text(($(".totaldebit").text() - $(".totalcredit").text()).toFixed(2));
          });
        return true;
      });  
   
       
   
   
     function goBack() {
         window.history.back();
     }
   
      function StopNonNumeric(el, evt)
       {
           //var r=e.which?e.which:event.keyCode;
           //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
           var charCode = (evt.which) ? evt.which : event.keyCode;
           var number = el.value.split('.');
           if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
               return false;
           }
           //just one dot (thanks ddlab)
           if(number.length>1 && charCode == 46){
                return false;
           }
           //get the carat position
           var dotPos = el.value.indexOf(".");
           if( dotPos>-1 && (number[1].length > 3)){
               return false;
           }
           return true;
       }
   setTimeout(function() { $(".flash-message").hide(); }, 2500);
</script>
@stop