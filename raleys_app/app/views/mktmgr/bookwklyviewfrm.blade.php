<?php 
//echo $del_tranfs_sum;exit;

    //echo '<pre>';print_r($futureitem2);exit;
 
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style>
   .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
   cursor: pointer;
   background-color: #eee;
   opacity: 1;
   }
</style>
<form action="{{URL::route('mktmgr-postbookwklyviewdata')}}" class="form-horizontal" method="post" role="form" style="display: block;">
   <!-- start of department sale div -->
   <div class="container allhideboxs " id="departmentsales" style="display: none;">
      <div style="" class=" mainbox col-md-12 ">
         <div class="panel panel-info safe_total">
            <div class="panel-heading " style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Department Sales : {{ $dateofinfo }}</div>
            </div>
            <div  class="panel-body" style="padding-top: 15px;" class="mainbox col-md-12">
               <?php $gtot='0.00'; ?>
               @foreach($deptsaleslist as $row)
               <?php 
                  $dbval = '0.00';
                  if($row['dept_sales'] != '')
                  {
                    $gtot=$gtot+$row['dept_sales'];
                    $dbval=number_format($row['dept_sales'], 2, '.', '');
                  }
                  ?>
                  <div class="col-md-6">
                     <div class="col-md-2"><strong><span>({{$row['item_id']}})</span></strong></div>
                     <div class="col-md-6"><strong><span>{{$row['item_desc'] }}</span></strong></div>
                     <div class="col-md-4" style="text-align:right;padding-right:69px;"><strong><span>{{ $dbval }}</span></strong></div>
                  </div>
               {{--<div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-6 col-sm-offset-1">
                        <label for="inputPassword" class="control-label ">({{$row['item_id']}}){{$row['item_desc'] }}</label>
                     </div>
                     <div class="col-sm-4">
                         <input type="number" class="form-control" readonly="readonly" value="{{ $dbval }}">
                        <strong><span>{{ $dbval }}</span></strong>
                     </div>
                  </div>
               </div> --}}
               @endforeach  
            </div>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" style="text-align: right;" class="control-label col-sm-6">Grand Total</label>
               <div class="col-sm-3">
                  <input type="number"  class="form-control grandtotal"  readonly="readonly" id="totdeptsales"  
                     name="totdeptsales" value="{{ $gtot }}">
                  
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total to Account For:&nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $gtot }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center" style="">
                     <input type="submit" name="dept-sales" id="dept-sales" tabindex="4" value="Submit" class="btn showmain">
                     <input type="button" name="" id="submit" tabindex="4" value="Cancel" class="btn showmain">
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of department sale div -->
   <!---end
      <!-- start of netsafe div -->
   <div class="container allhideboxs" id="netsafediv" style="display: none;">
      <div style="" class="mainbox col-md-12 ">
         <div class="panel panel-info safe_total">
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >Net Safe Activity : {{ $dateofinfo }}</div>
            </div>
            <div  class="panel-body" style="padding-top: 10px;" class="mainbox col-md-12 ">
               <?php $netsafeamt='0.00'; $netsafetot = '0.00'; ?>
               @foreach($netsafeactive as $row)
               <?php 
                  if($row['item_amt'] != '')
                  {
                     $netsafetot=$netsafetot+$row['item_amt'];
                     $netsafeamt=number_format($row['item_amt'], 2, '.', '');
                  }
                  ?>
               {{-- <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-6 col-sm-offset-1">
                        <label for="inputPassword" class="control-label">({{$row['item_id']}}){{$row['item_desc'] }}</label>
                     </div>
                     <div class="col-sm-4">
                        <input type="number" class="form-control" readonly="readonly" value="{{ $row['item_amt'] }}">
                     </div>
                  </div>
               </div> --}}
               <div class="col-md-6">
                     <div class="col-md-2"><strong><span>({{$row['item_id']}})</span></strong></div>
                     <div class="col-md-6"><strong><span>{{$row['item_desc'] }}</span></strong></div>
                     <div class="col-md-4" style="text-align:right;padding-right:69px;"><strong><span><?php if(!empty($row['item_amt'])){ echo number_format((float)$row['item_amt'], 2, '.', ''); } else { echo '0.00'; } ?></span></strong></div>
                  </div>
               @endforeach  
            </div>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" style="text-align: right;" class="control-label col-sm-6">Total to account for</label>
               <div class="col-sm-3">
                  <input type="number"  class="form-control grandtotal"  readonly="readonly" id="netsafetot" name="netsafetot" value="{{ $netsafetot }}">
                  @if($errors->has('otherpricetot'))
                  {{ $errors->first('otherpricetot')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total to Account For:&nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $netsafetot }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
             <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center" style="">
                     <input type="submit" name="net-sales" id="net-sales" tabindex="4" value="Submit" class="btn showmain">
                     <input type="button" name="" id="submit" tabindex="4" value="Cancel" class="btn showmain">
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of netsafe div -->   
   <!-- start of deposit details div -->
   <!-- start of Currency deposit div-->
   <div class="container allhideboxs" id="currencydepositebox" style="display: none;">
      <div class="mainbox col-md-8 col-md-offset-2 col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">CURRENCY</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $currentytotfrombox='0.00'?>
                  @if($currentytot)
                  @foreach ($currentytot as $row)
                  <?php
                     $currentytotfrombox = $currentytotfrombox+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     if($row['item_amt'] != 0)
                     {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php } $currentytotfrombox=number_format((float)$currentytotfrombox, 2, '.', ''); ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$currentytotfrombox}}" 
                     id="currentytotfrombox"  name="currentytotfrombox">
                  @if($errors->has('currentytotfrombox'))
                  {{ $errors->first('currentytotfrombox')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $currentytotfrombox }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="depositdetails-sales" id="depositdetails-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of Currency deposit div-->
   <!-- start of check deposit div-->
   <div class="container allhideboxs" id="checkdepositebox" style="display: none;">
      <div class="mainbox col-md-8 col-md-offset-2  col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">CHECK</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $disp_checks_total='0.00';?>
                  @if($disp_checks)
                  @foreach ($disp_checks as $row)
                  <?php
                     $disp_checks_total = $disp_checks_total+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     
                     
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     //echo $seldate.'<br>';
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     // if($row['item_amt'] != 0)
                     // {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php
                   //}
                    $disp_checks_total=number_format((float)$disp_checks_total, 2, '.', ''); ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$disp_checks_total}}" 
                     id="currentytotfrombox"  name="currentytotfrombox">
                  
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $disp_checks_total }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="depositdetails-sales" id="depositdetails-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of check deposit div-->
   <!-- start of visa deposit div-->
   <div class="container allhideboxs" id="visadepositebox" style="display: none;">
      <div class="mainbox col-md-8 col-md-offset-2  col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">EXPRESS CREDIT</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $visadeposttotfrombox='0.00'?>
                  @if($visadepoist)
                  @foreach ($visadepoist as $row)
                  <?php
                     $visadeposttotfrombox = $visadeposttotfrombox+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     if($row['item_amt'] != 0)
                     {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php } $visadeposttotfrombox=number_format((float)$visadeposttotfrombox, 2, '.', ''); ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$visadeposttotfrombox}}" 
                     id="visadeposttotfrombox"  name="visadeposttotfrombox">
                  @if($errors->has('visadeposttotfrombox'))
                  {{ $errors->first('visadeposttotfrombox')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $visadeposttotfrombox }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="visadepost-sales" id="visadepost-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of visa deposit div-->
   <!-- start of express debit div-->
   <div class="container allhideboxs" id="expressdebitbox" style="display: none;">
      <div class="mainbox col-md-8  col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">EXPRESS DEBIT</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $exptotfrombox='0.00'?>
                  @if($expressdebit)
                  @foreach ($expressdebit as $row)
                  <?php
                     $exptotfrombox = $exptotfrombox+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     if($row['item_amt'] != 0)
                     {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php } $exptotfrombox=number_format((float)$exptotfrombox, 2, '.', ''); ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$exptotfrombox}}" 
                     id="exptotfrombox"  name="exptotfrombox">
                  @if($errors->has('exptotfrombox'))
                  {{ $errors->first('exptotfrombox')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $exptotfrombox }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="expressdebit-sales" id="expressdebit-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of express debit div-->
   <!-- start of express debit div-->
   <div class="container allhideboxs" id="discoverdepositsbox" style="display: none;">
      <div class="mainbox col-md-8 col-md-offset-2 col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">DISCOVER</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $discoverdepositstotbox='0.00'?>
                  @if($disoverdeposit)
                  @foreach ($disoverdeposit as $row)
                  <?php
                     $discoverdepositstotbox = $discoverdepositstotbox+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     if($row['item_amt'] != 0)
                     {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php } $discoverdepositstotbox=number_format((float)$discoverdepositstotbox, 2, '.', ''); ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$discoverdepositstotbox}}" 
                     id="discoverdepositstotbox"  name="discoverdepositstotbox">
                  @if($errors->has('discoverdepositstotbox'))
                  {{ $errors->first('discoverdepositstotbox')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $discoverdepositstotbox }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="discoverdeposits-sales" id="discoverdeposits-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of discover deposit div-->
   <!-- start of americanexpress div -->
   <div class="container allhideboxs" id="americanexpressbox" style="display: none;">
      <div class="mainbox col-md-8 col-md-offset-2 col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">AMERICAN EXPRESS</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $americanexpresstotbox='0.00'?>
                  @if($americanexpress)
                  @foreach ($americanexpress as $row)
                  <?php
                     $americanexpresstotbox = $americanexpresstotbox+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     if($row['item_amt'] != 0)
                     {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php } $americanexpresstotbox=number_format((float)$americanexpresstotbox, 2, '.', ''); ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$americanexpresstotbox}}" 
                     id="americanexpresstotbox"  name="americanexpresstotbox">
                  @if($errors->has('americanexpresstotbox'))
                  {{ $errors->first('americanexpresstotbox')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $americanexpresstotbox }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="discoverdeposits-sales" id="americanexpress-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>
               <br>
            </div>
         </div>
      </div>
   </div>
   <!-- end of americanexpress div -->
   <!-- start of futureitem1 div -->
   <div class="container allhideboxs" id="futureitem1box" style="display: none;">
      <div class="mainbox col-md-8 col-md-offset-2  col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">FUTURE ITEM 1</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $futureitem1totbox='0.00'?>
                  @if($futureitem1)
                  @foreach ($futureitem1 as $row)
                  <?php
                     $futureitem1totbox = $futureitem1totbox+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     if($row['item_amt'] != 0)
                     {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php } $futureitem1totbox=number_format((float)$futureitem1totbox, 2, '.', ''); ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$futureitem1totbox}}" 
                     id="americanexpresstotbox"  name="futureitem1totbox">
                  @if($errors->has('futureitem1totbox'))
                  {{ $errors->first('futureitem1totbox')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $futureitem1totbox }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="futureitem1-sales" id="futureitem1-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of futureitem1 div -->
   <!-- start of futureitem2 div -->
   <div class="container allhideboxs" id="futureitem2box" style="display: none;">
      <div class="mainbox col-md-8 col-md-offset-2 col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">FUTURE ITEM 2</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $futureitem2totbox='0.00';
                     //echo '<pre>';print_r($futureitem2);//exit;
                  ?>
                  @if($futureitem2)
                  @foreach ($futureitem2 as $row)
                  <?php
                     $futureitem2totbox = $futureitem2totbox+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     // if($row['item_amt'] != 0)
                     // {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php
                  // } 
                   $futureitem2totbox=number_format((float)$futureitem2totbox, 2, '.', '');
                  ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$futureitem2totbox}}" 
                     id="americanexpresstotbox"  name="futureitem1totbox">
                  @if($errors->has('futureitem1totbox'))
                  {{ $errors->first('futureitem1totbox')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $futureitem2totbox }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="futureitem1-sales" id="futureitem1-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>

            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of futureitem2 div -->
   <!-- start of ebtwic div -->
   <div class="container allhideboxs" id="ebtwicbox" style="display: none;">
      <div class="mainbox col-md-8 col-md-offset-2  col-sm-offset-2">
         <div class="panel panel-info safe_total">
            <table class="table table-striped" id="example">
               <thead>
                  <tr>
                     <th>&nbsp;</th>
                     <th colspan="2" class="text-center">EBT WIC</th>
                     <th colspan="2" class="text-center">{{ $dateofinfo }}</th>
                  </tr>
                  <tr>
                     <th>Day</th>
                     <th>Date</th>
                     <th>ID</th>
                     <th>Description</th>
                     <th>Amount</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $futureitem3totbox='0.00'?>
                  @if($futureitem3)
                  @foreach ($futureitem3 as $row)
                  <?php
                     $futureitem1totbox = $futureitem3totbox+$row['item_amt'];
                     $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                     $weekday = date('l', strtotime($row['wk_end_date']));
                     $seldate = date("m/d/Y", strtotime($row['wk_end_date']));
                     $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                     // if($row['item_amt'] != 0)
                     // {    
                     ?>
                  <tr>
                     <td>{{ $weekday }}</td>
                     <td>{{ $seldate }}</td>
                     <td>{{ $itemid }}</td>
                     <td>{{ $itemdesc }}</td>
                     <td>{{ $cdamt }}</td>
                  </tr>
                  <?php
                  // } 
                   $futureitem3totbox=number_format((float)$futureitem3totbox, 2, '.', ''); ?> 
                  @endforeach
                  @else
                  <tr>
                     <td  colspan="5" style="text-align:center;">No Records</td>
                  </tr>
                  @endif
               </tbody>
            </table>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Total</label>
               <div class="col-sm-3">
                  <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{$futureitem3totbox}}" 
                     id="americanexpresstotbox"  name="futureitem1totbox">
                  @if($errors->has('futureitem1totbox'))
                  {{ $errors->first('futureitem1totbox')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $futureitem3totbox }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center">
                     <input type="submit" name="futureitem1-sales" id="futureitem1-sales" tabindex="4" value="Submit" class="btn showsubmain">
                     <input type="button" value="Cancel" class="btn showsubmain">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of ebtwic   div -->
   <div class="container allhideboxs" id="depositdetails" style="display: none;">
      <div class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-2">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >DEPOSIT DETAIL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $dateofinfo }}</div>
            </div>
            <div class="panel-body" style="padding-top: 15px;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Currency Deposits</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="currencydeposite" value="{{$currentytotfrombox}}" name="currencydeposite"> --}}
                     <input type="button" id="currencydeposite" value="{{$currentytotfrombox}}" class="btn viewdepositbox" name="currencydeposite" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Check Deposits</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="checkdeposite" value="{{$disp_checks_total}}" name="checkdeposite"> --}}
                     <input type="button" id="checkdeposite" value="{{$disp_checks_total}}" class="btn viewdepositbox" name="checkdeposite" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">M/C - Visa Deposits</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="mcvisadeposits" value="{{$visadeposttotfrombox}}" 
                        name="mcvisadeposits"> --}}
                    <input type="button" id="mcvisadeposits" value="{{$visadeposttotfrombox}}" class="btn viewdepositbox" name="mcvisadeposits" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Express Debit</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="expressdebit" value="{{$exptotfrombox}}" 
                        name="expressdebit"> --}}
                     <input type="button" id="expressdebit" value="{{$exptotfrombox}}" class="btn viewdepositbox" name="expressdebit" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Discover Deposits</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="discoverdeposits" value="{{$discoverdepositstotbox}}" 
                        name="discoverdeposits"> --}}
                     <input type="button" id="discoverdeposits" value="{{$discoverdepositstotbox}}" class="btn viewdepositbox" name="discoverdeposits" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">American Express</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="americanexpress" value="{{ $americanexpresstotbox }}" 
                        name="americanexpress"> --}}
                     <input type="button" id="americanexpress" value="{{$americanexpresstotbox}}" class="btn viewdepositbox" name="americanexpress" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">EBT Welfare</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="ebteelfare" value="{{ $futureitem1totbox }}" name="ebteelfare"> --}}
                     <input type="button" id="ebteelfare" value="{{$futureitem1totbox}}" class="btn viewdepositbox" name="ebteelfare" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Future Item 2</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="futureitem2" value="{{ $futureitem2totbox }}" name="futureitem2"> --}}
                     <input type="button" id="futureitem2" value="{{$futureitem2totbox}}" class="btn viewdepositbox" name="futureitem2" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">EBT WIC</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewdepositbox" id="ebtwic" value="{{ $futureitem3totbox }}" name="ebtwic"> --}}
                     <input type="button" id="ebtwic" value="{{$futureitem3totbox}}" class="btn viewdepositbox" name="ebtwic" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <hr style="border:1px solid #000;" />
               <?php 
                  $subdivtot=$currentytotfrombox + $disp_checks_total+  $visadeposttotfrombox + $exptotfrombox + $discoverdepositstotbox + 
                             $americanexpresstotbox + $futureitem1totbox + $futureitem2totbox +$futureitem3totbox;
                            $subdivtot= number_format((float)$subdivtot, 2, '.', '');
                  ?>
               {{-- <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Total</label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{ $subdivtot }}" id="total"  name="total">
                     @if($errors->has('total'))
                     {{ $errors->first('total')}}
                     @endif
                  </div>
               </div> --}}
               <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $subdivtot }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="depositdetails-sales" id="depositdetails-sales" tabindex="4" value="Submit" class="btn showmain">
                        <input type="button" value="Cancel" class="btn showmain" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <br>
            </div>
         </div>
      </div>
   </div>
   <!-- end of deposit details div -->
   <!-- start of transfer table -->
   <div class="container allhideboxs" id="transfertable" style="display: none;">
      <div class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-2">
         <table class="table table-striped" id="example">
            <thead>
               <tr>
                  <th>Day</th>
                  <th>Date</th>
                  <th>ID</th>
                  <th>Description</th>
                  <th>Amount</th>
               </tr>
            </thead>
            <tbody>
               <?php $dtrnstab1='0.00'?>
               @if($delivertransfertable1)
               @foreach ($delivertransfertable1 as $row)
               <?php
                  $dtrnstab1 = $dtrnstab1+$row['item_amt'];
                  $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                  $weekday = date('l', strtotime($row['wk_end_date']));
                  $seldate = date("m-d-Y", strtotime($row['wk_end_date']));
                  $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                  if($row['item_amt'] != 0)
                  {    
                  ?>
               <tr>
                  <td>{{ $weekday }}</td>
                  <td>{{ $seldate }}</td>
                  <td>{{ $itemid }}</td>
                  <td>{{ $itemdesc }}</td>
                  <td>{{ $cdamt }}</td>
               </tr>
               <?php } $dtrnstab1=number_format((float)$dtrnstab1, 2, '.', ''); ?> 
               @endforeach
               @else
               <tr>
                  <td  colspan="5" style="text-align:center;">No Records</td>
               </tr>
               @endif
            </tbody>
         </table>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Total</label>
            <div class="col-sm-6">
               <input  type="text" class="form-control ftotal"  readonly="readonly" value="{{$dtrnstab1}}" id="dtrnstab1"  name="dtrnstab1">
               @if($errors->has('dtrnstab1'))
               {{ $errors->first('dtrnstab1')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="submit" name="dtrnstab1-sales" id="dtrnstab1-sales" tabindex="4" value="Submit" class="btn showdelivertransfer">
                  <input type="button"  value="Cancel" class="btn showdelivertransfer">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- end of transfer table -->
   <!-- start of deliver table -->
   <div class="container allhideboxs" id="delivertable" style="display: none;">
      <div class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-2">
         <table class="table table-striped" id="example">
            <thead>
               <tr>
                  <th>Day</th>
                  <th>Date</th>
                  <th>ID</th>
                  <th>Description</th>
                  <th>Amount</th>
               </tr>
            </thead>
            <tbody>
               <?php $dtrnstab2='0.00'?>
               @if($delivertransfertable2)
               @foreach ($delivertransfertable2 as $row)
               <?php
                  $dtrnstab2 = $dtrnstab2+$row['item_amt'];
                  $cdamt=number_format((float)$row['item_amt'], 2, '.', '');
                  $weekday = date('l', strtotime($row['wk_end_date']));
                  $seldate = date("m-d-Y", strtotime($row['wk_end_date']));
                  $itemid=$row['item_id']; $itemdesc=$row['item_desc'];
                  if($row['item_amt'] != 0)
                  {    
                  ?>
               <tr>
                  <td>{{ $weekday }}</td>
                  <td>{{ $seldate }}</td>
                  <td>{{ $itemid }}</td>
                  <td>{{ $itemdesc }}</td>
                  <td>{{ $cdamt }}</td>
               </tr>
               <?php } $dtrnstab2=number_format((float)$dtrnstab2, 2, '.', ''); ?> 
               @endforeach
               @else
               <tr>
                  <td  colspan="5" style="text-align:center;">No Records</td>
               </tr>
               @endif
            </tbody>
         </table>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Total</label>
            <div class="col-sm-6">
               <input  type="text" class="form-control ftotal"  readonly="readonly" value="{{$dtrnstab2}}" id="dtrnstab2"  name="dtrnstab2">
               @if($errors->has('dtrnstab2'))
               {{ $errors->first('dtrnstab2')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="submit" name="dtrnstab2-sales" id="dtrnstab2-sales" tabindex="4" value="Submit" class="btn showdelivertransfer">
                  <input type="button"  value="Cancel" class="btn showdelivertransfer">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--end of deliver
      <!-- start of transfer --> 
   <div class="container allhideboxs" id="deliveriestransferbox" style="display: none;">
      <div class="mainbox col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-2" style="margin-left:290px;">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >DELIVERIES &  TRANSFER DETAIL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $dateofinfo }}</div>
            </div>
            <div class="panel-body" >
               <div class="form-group" style="padding-top:15px;margin-left:15px;">
                  <?php $delivertransfer1=number_format((float)($delivertransfer1/100), 2, '.', '');?>
                  <label for="inputPassword" class="control-label col-sm-4" style="text-align: right;">Transfers</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control" id="transfersdeliver" value="{{ $delivertransfer1 }}" name="transfersdeliver"> --}}
                     <input type="button" id="transfersdeliver" value="{{ $delivertransfer1 }}" class="btn" name="transfersdeliver" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group" style="margin-left:15px;">
                  <?php $delivertransfer2 =number_format((float)($delivertransfer2/100), 2, '.', '');?>
                  <label for="inputPassword" class="control-label col-sm-4" style="text-align: right;">Deliveries</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control" id="deliveriestransfer" value="{{ $delivertransfer2 }}" name="deliveriestransfer"> --}}
                     <input type="button" id="deliveriestransfer" value="{{ $delivertransfer2 }}" class="btn" name="deliveriestransfer" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <hr style="border:1px solid #000;" />
               <?php $delivertransfertot=number_format((float)($delivertransfertot/100), 2, '.', '');?>
               {{-- <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4" style="text-align: right;">Total</label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{ $delivertransfertot }}" id="dttot"  name="dttot">
                    
                  </div>
               </div> --}}
               <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total  &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $delivertransfertot }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="depositdetails-sales" id="depositdetails-sales" tabindex="4" value="Submit" class="btn showmain">
                        <input type="button"  value="Cancel" class="btn showmain">
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <br>
            </div>
         </div>
      </div>
   </div>
   <!-- end of transfer -->
   <!-- start of STORE TRANSACTIONS div --> 
   <div class="container allhideboxs" id="storetransctionsbox" style="display: none;">
      <div style="" class="mainbox col-md-10 col-md-offset-1 ">
         <div class="panel panel-info safe_total">
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >STORE TRANSACTIONS : {{ $dateofinfo }}</div>
            </div>
            <div  class="panel-body" class="mainbox col-md-10 col-md-offset-3 col-sm-8 col-sm-offset-2" style="padding-top: 15px;">
               <?php $storetransctionsboxtot='0.00'; $storetransctionsboxtot = '0.00'; ?>
               @if($storetrans)
               @foreach($storetrans as $row)
               <?php 
                  if($row['item_amt'] != '')
                  {
                     $storetransctionsboxtot=$storetransctionsboxtot+$row['item_amt'];
                     $netsafeamt=number_format($row['item_amt'], 2, '.', '');
                  }
                  ?>
               {{-- <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-8">
                        <label for="inputPassword" class="control-label">({{$row['item_id']}}){{$row['item_desc'] }}</label>
                     </div>
                     <div class="col-sm-4">
                        <input type="number" class="form-control" readonly="readonly" value="{{ $row['item_amt'] }}">
                     </div>
                  </div>
               </div> --}}
               <div class="col-md-6">
                     <div class="col-md-2"><strong><span>({{$row['item_id']}})</span></strong></div>
                     <div class="col-md-6"><strong><span>{{$row['item_desc'] }}</span></strong></div>
                     <div class="col-md-4" style="text-align:right;"><strong><span>{{  number_format((float)$row['item_amt'], 2, '.', '') }}</span></strong></div>
                  </div>
               @endforeach  
               @else
               <table class="table table-striped" id="example">
                  <thead>
                     <tr>
                        <td  colspan="5" style="text-align:center;">No Records</td>
                     </tr>
                  </thead>
               </table>
               @endif
            </div>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" class="control-label col-sm-5" style="text-align: right;">Grand Total</label>
               <div class="col-sm-3">
                  <input type="number"  class="form-control grandtotal"  readonly="readonly" id="storetransctionsboxtot" name="storetransctionsboxtot" 
                     value="{{ $storetransctionsboxtot }}">
                  @if($errors->has('storetransctionsboxtot'))
                  {{ $errors->first('storetransctionsboxtot')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total to Account For:&nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $storetransctionsboxtot }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center" style="">
                     <input type="submit" name="storetransctions-sales" id="storetransctions-sales" tabindex="4" value="Submit" class="btn showmain">
                     <input type="button"  value="Cancel" class="btn showmain">
                     <!-- <input type="button" name="" id="submit" tabindex="4" value="Cancel" class="btn"> -->
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of STORE TRANSACTIONS div --> 
   <!-- start of PAY OUT TYPES div --> 
   <div class="container allhideboxs" id="payouttypebox" style="display: none;">
      <div style="" class="mainbox col-md-10 col-md-offset-1 ">
         <div class="panel panel-info safe_total">
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >STORE TRANSACTIONS: {{ $dateofinfo }}</div>
            </div>
            <div  class="panel-body" class="mainbox col-md-10 col-md-offset-3 col-sm-8 col-sm-offset-2" style="padding-top: 15px;">
               <?php $payouttypeboxtot='0.00'; $payouttypeboxtot = '0.00'; ?>
               @if($paidouttypes)
               @foreach($paidouttypes as $row)
               <?php 
                  if($row['item_amt'] != '')
                  {
                     $payouttypeboxtot=$payouttypeboxtot+$row['item_amt'];
                     $netsafeamt=number_format($row['item_amt'], 2, '.', '');
                  }
                  $payouttypeboxtot=number_format((float)$payouttypeboxtot, 2, '.', ''); 
                  ?> 
               {{-- <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-7">
                        <label for="inputPassword" class="control-label">({{$row['item_id']}}){{$row['item_desc'] }}</label>
                     </div>
                     <div class="col-sm-5">
                        <input type="number" class="form-control" readonly="readonly" value="{{ $row['item_amt'] }}">
                     </div>
                  </div>
               </div> --}}
               <div class="col-md-6">
                     <div class="col-md-2"><strong><span>({{$row['item_id']}})</span></strong></div>
                     <div class="col-md-6"><strong><span>{{$row['item_desc'] }}</span></strong></div>
                     <div class="col-md-4" style="text-align:right;"><strong><span>{{ number_format((float)$row['item_amt'], 2, '.', ''); }}</span></strong></div>
                  </div>
               @endforeach
               @else
               <table class="table table-striped" id="example">
                  <thead>
                     <tr>
                        <td  colspan="5" style="text-align:center;">No Records</td>
                     </tr>
                  </thead>
               </table>
               @endif  
            </div>
            <hr style="border:1px solid #000;" />
            {{-- <div class="form-group">
               <label for="inputPassword" style="text-align: right;" class="control-label col-sm-5">Grand Total</label>
               <div class="col-sm-3">
                  <input type="number"  class="form-control grandtotal"  readonly="readonly" id="payouttypeboxtot" name="payouttypeboxtot" 
                     value="{{ $payouttypeboxtot }}">
                  @if($errors->has('payouttypeboxtot'))
                  {{ $errors->first('payouttypeboxtot')}}
                  @endif
               </div>
            </div> --}}
            <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4" style="text-align: center;font-size: 15px">
                  <b>Total to Account For:&nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{ $payouttypeboxtot }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12" align="center" style="">
                     <input type="submit" name="storetransctions-sales" id="storetransctions-sales" tabindex="4" value="Submit" class="btn showmain">
                     <input type="button"  value="Cancel" class="btn showmain">
                     <!-- <input type="button" name="" id="submit" tabindex="4" value="Cancel" class="btn"> -->
                  </div>
               </div>
            </div>
            <br>
         </div>
      </div>
   </div>
   <!-- end of PAY OUT TYPES div --> 
   <!-- start of main div -->
   <div class="container">
      <div class="flash-message" id="recapmsg">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         </p>
         @endif
         @endforeach
      </div>
      <!-- end .flash-message -->
      <?php
      if(!$deposits_sum)
      {
         $deposits_sum = 0;
      }
      if(!$dept_sales_sum)
      {
         $dept_sales_sum = 0;
      }
      if(!$net_safe_sum)
      {
         $net_safe_sum = 0;
      }
      if(!$del_tranfs_sum)
      {
         $del_tranfs_sum = 0;
      }
      if(!$store_tx_sum)
      {
         $store_tx_sum = 0;
      }
      if(!$po_types_sum)
      {
         $po_types_sum = 0;
      }
      //echo $deposits_sum;exit;
      ?>
      <div id="mainbox" class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-2">
         <!--   <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-2">   -->                  
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >RECAP REPORT &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{ $dateofinfo }}</div>
            </div>
            <div class="panel-body" style="padding-top: 15px;" >
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Dept Sales</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewbox" id="departmentsale" value="{{ $gtot }}" name="departmentsale"> --}}
                     <input  type="button" id="departmentsale" value="{{ number_format((float)($dept_sales_sum/100), 2, '.', ''); }}"  class="btn viewbox" name="departmentsale" tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Net Safe</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewbox" id="netsafe" value="{{ $netsafetot }}" name="netsafe"> --}}
                     <input  type="button" id="netsafe" value="{{ number_format((float)($net_safe_sum/100), 2, '.', ''); }}"  class="btn viewbox" name="netsafe"  tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Deposits</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewbox" id="deposits" value="{{ $subdivtot }}" name="deposits"> --}}
                     <input  type="button" id="deposits" value="{{ number_format((float)($deposits_sum/100), 2, '.', ''); }}"  class="btn viewbox" name="deposits"  tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Delivery and Transfers</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewbox" id="deliverytransfersmain" value="{{ $delivertransfertot }}" 
                        name="deliverytransfersmain"> --}}
                     <input  type="button" id="deliverytransfersmain" value="{{ number_format((float)($del_tranfs_sum/100), 2, '.', ''); }}"  class="btn viewbox" name="deliverytransfersmain"  tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Store Transactions</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewbox" id="storetransactions" value="{{ $storetransctionsboxtot }}" 
                        name="storetransactions"> --}}
                     <input  type="button" id="storetransactions" value="{{ number_format((float)($store_tx_sum/100), 2, '.', ''); }}"  class="btn viewbox" name="storetransactions"  tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Paid Out Types</label>
                  <div class="col-sm-6">
                     {{-- <input type="text" readonly="readonly" class="form-control viewbox" id="paidouttypes" value="{{ $payouttypeboxtot }}" name="paidouttypes"> --}}
                     <input  type="button" id="paidouttypes" value="{{ number_format((float)($po_types_sum/100), 2, '.', ''); }}"  class="btn viewbox" name="paidouttypes"  tabindex="4" style="width:190px;text-align: right;">
                  </div>
               </div>
               <hr style="border:1px solid #000;" />
               <?php  
                  //echo $gtot.'<br>';
                  //echo $subdivtot.'<br>';
                  //echo $storetransctionsboxtot.'<br>';
                  //echo $delivertransfertot.'<br>';
                  //echo $payouttypeboxtot.'<br>';
                  //echo $netsafetot.'<br>';
                  $maindivtot = ($deposits_sum/100) + ($dept_sales_sum/100) + ($net_safe_sum/100) + ($del_tranfs_sum/100) + ($store_tx_sum/100) + ($po_types_sum/100);  
                  //echo $maindivtot;exit;
                   $maindivtot = number_format((float)$maindivtot, 2, '.', ''); 
                   ?>
               {{-- <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-6" style="text-align: right;">Total</label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" value="{{ $maindivtot }}" id="total"  name="total">
                   <strong><span>{{ $maindivtot }}</span></strong>
                  </div>
               </div> --}}
               <div class="row">
               <div class="col-sm-4">&nbsp;</div>
               <div class="col-sm-4">
                  <b>Total &nbsp;&nbsp;:&nbsp;&nbsp; </b><span><strong>{{  $maindivtot }}</strong></span>
               </div>
               <div class="col-sm-4">&nbsp;</div>
            </div>
            <hr style="border:1px solid #000;" />
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="button" value="Submit" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookwklyview')}}'" />
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/bookwklyview')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <br>
            </div>
         </div>
      </div>
   </div>
   <!-- end of deliverytransfers div -->
   <!-- start of deliverytransfers div -->
   <div>
   </div>
   <!-- end of deliverytransfers div -->
   <!-- start of storetransactions div -->
   <div>
   </div>
   <!-- end of storetransactions div -->
   <!-- start of paidouttypes div -->
   <div>
   </div>
   <!-- end of paidouttypes div -->
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>        
<script type="text/javascript">
   $(".allhideboxs").hide();
   //$("#departmentsales").hide();$("#netsafediv").hide(); $("#depositdetails").hide(); $("#deliveriestransferbox").hide();
   $(document).ready(function(){
   
    $(".viewbox").click(function(){
      //$("#mainbox").hide(); $("#departmentsales").hide(); $("#netsafediv").hide(); $("#depositdetails").hide();
      $(".allhideboxs").hide();
      selid=this.id;
      switch(selid){
       case 'departmentsale':
           $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#departmentsales").show();
           break;
       case 'netsafe':
           $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#netsafediv").show();
           break;
       case 'deposits':
           $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#depositdetails").show();
           break;
       case 'deliverytransfersmain':
          $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#deliveriestransferbox").show();
           break;
       case 'storetransactions':
          $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#storetransctionsbox").show();
           break;
       case 'paidouttypes':
          $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#payouttypebox").show();
           break;    
       default :
           break;
    }
    return false;
   });
   
    $(".viewdepositbox").click(function(){
     $(".allhideboxs").hide();
      selid=this.id;
      switch(selid){
       case 'currencydeposite':
           $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#currencydepositebox").show();
           break;
       case 'checkdeposite':
           $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#checkdepositebox").show();
           break;    
       case 'mcvisadeposits':
           $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#visadepositebox").show();
           break;
       case 'expressdebit':
           $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#expressdebitbox").show();
           break;
       case 'discoverdeposits':
           $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#discoverdepositsbox").show();
           break;    
       case 'americanexpress':
          $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#americanexpressbox").show();
           break;    
       case 'ebteelfare':
          $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#futureitem1box").show();
           break;
        case 'futureitem2':
          $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#futureitem2box").show();
           break;   ebtwic   
         case 'ebtwic':
          $(".allhideboxs").hide(); $("#mainbox").hide();
           $("#ebtwicbox").show();
           break;          
       default :
           break;
    }
    return false;
    });
   
    $("#transfersdeliver").click(function(){
       $(".allhideboxs").hide(); $("#mainbox").hide(); 
        $("#transfertable").show();
    });
    
    $("#deliveriestransfer").click(function() {
      $(".allhideboxs").hide(); $("#mainbox").hide(); 
        $("#delivertable").show();
    });
   
   
    $(".showmain").click(function(){
       $(".allhideboxs").hide();
       $("#mainbox").show(); return false;
    });
   
    $(".showsubmain").click(function(){
      $(".allhideboxs").hide(); $("#mainbox").hide();
      $("#depositdetails").show(); return false;
    });
   
    $(".showdelivertransfer").click(function(){
     $(".allhideboxs").hide(); $("#mainbox").hide(); 
     $("#deliveriestransferbox").show(); return false;
    });
   
   });
</script>
@stop
<style type="text/css">
   .panel-info {
   /* border-color: #bce8f1; */
   border-color: #000;
   }
   .panel {
   margin-bottom: 20px;
   background-color: #fff;
   border: 1px solid transparent;
   border-radius: 4px;
   -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
   box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
   }
</style>