<?php
  //echo '<pre>';print_r($iteminnerdetails);exit;
?>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Shrink Capture Review')
@section('content')
@section('section')
<header class="row">
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h4>Shrink Capture 40470 Item Scan {{ $iteminnerdetails[0]->seq_number }} Detail </h4>
         </b>
      </div>
   </div>
</header>

<br>
<br>
<div class="container">
<form method="post" action="{{URL::route('mktmgr--post-update-shrink-itemscan')}}" id="quantityform" onsubmit="return myfunction()">
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Shrink Capture No</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->hdr_seq_number }}</div>
      <input type="hidden" name="hdr_seq_number" id="hdr_seq_number" value="{{ $iteminnerdetails[0]->hdr_seq_number }}">
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Scan No</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->seq_number }}</div>
      <input type="hidden" name="seq_number" id="seq_number" value="{{ $iteminnerdetails[0]->seq_number }}">
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>UPC Number</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->upc_number }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Warehouse</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->whse_number }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Item Number</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->item_number }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Item in GL Dept</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->gl_dept }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Cost</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->upc_cost }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Retail</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->rtl_amt }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Scale Id</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->scale_id }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Case Pack</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->case_pack }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Container Size</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->cont_size }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Item Description</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left">{{ $iteminnerdetails[0]->item_desc }}</div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b>Quantity</b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">:</div>
      <div class="col-md-4 text-left"><input type="text" id="quantity" name="quantity" value="{{ $iteminnerdetails[0]->quantity }}"></div>
      
      <div class="col-md-2">&nbsp;</div>
   </div>
   <br>
   <div class="row" style="margin-left:150px;">
      <div class="col-md-2">&nbsp;</div>
      <div class="col-md-3 text-right"><b><input type="submit" class="btn" value="Update"></b></div>
      <div class="col-md-1" style="padding:0px;width:0px !important;">&nbsp;</div>
      <div class="col-md-4 text-left"><input type="button" class="btn" onclick="history.go(-1);" value="Cancel"></div>
      <div class="col-md-2">&nbsp;</div>
   </div>
   </form>
   <br>
</div>


@stop

<script type="text/javascript">
   function myfunction()
   {
      //alert('Submitted');
      var quantity = $('#quantity').val();
      //alert(quantity);return false;
      if($('#quantity').val() == 0)
      {
         alert('Shrink Quantity has NOT been updated - Quantity cannot be Zero');
         $('#quantity').focus();return false;
      }
      if($('#quantity').val() == <?php echo $iteminnerdetails[0]->quantity; ?>)
      {
         alert('Shrink Quantity has NOT been updated - Old & New Qtys Are the Same');
         $('#quantity').focus();return false;
      }
      
   }
</script>