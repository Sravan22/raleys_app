<?php 
   //echo '<pre>',print_r($deliveries_array);exit();
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
            {{--  <a href="" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a> --}}
         </span>
      </div>
   </div>
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="panel">
         <div class="" style="margin-left: 78px;margin-bottom:-23px;font-weight:bold;">
            <div class="panel-title">Weekend Date : {{  date("m/d/Y", strtotime($wkEndDate)) }}</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Gas Weekly Report</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
        
            <table class="table table-striped">
               <thead>
                  <th class="text-center">&nbsp;</th>
                  <th class="text-center">Unleaded</th>
                  <th class="text-center">Premium</th>
                  <th class="text-center">Mid-Grade</th>
                  <th class="text-center">Diesel</th>
               </thead>
               <tbody>
                  <tr>
                     <td>Deliveries (Gal.)</td>
                     <?php for($i=0;$i<count($deliveries_array);$i++) { ?>
                     <td class="text-center"><?php echo number_format((float)$deliveries_array[$i], 2, '.', ','); ?></td>
                     <?php } ?>
                  </tr>
                 <tr>
                     <td>Sales (Gallons)</td>
                     <?php for($i=0;$i<count($sales_array);$i++) { ?>
                     <td class="text-center"><?php echo number_format((float)$sales_array[$i], 2, '.', ','); ?></td>
                     <?php } ?>
                  </tr>
                  <tr>
                     <td>Cost of Sales</td>
                     <?php for($i=0;$i<count($cost_of_sales_array);$i++) { ?>
                     <td class="text-center"><?php echo number_format((float)$cost_of_sales_array[$i], 2, '.', ','); ?></td>
                     <?php } ?>
                  </tr>
                 <tr>
                     <td>Shrink (Gallons)</td>
                     <?php for($i=0;$i<count($shrink_array);$i++) { ?>
                     <td class="text-center"><?php echo number_format((float)$shrink_array[$i], 2, '.', ','); ?></td>
                     <?php } ?>
                  </tr>
                  <tr>
                     <td>Shrink Cost</td>
                     <?php for($i=0;$i<count($shrink_cost_array);$i++) { ?>
                     <td class="text-center"><?php echo number_format((float)$shrink_cost_array[$i], 2, '.', ','); ?></td>
                     <?php } ?>
                  </tr>
                  <tr>
                     <td>Storage Fee</td>
                     <?php for($i=0;$i<count($storage_fee_array);$i++) { ?>
                     <td class="text-center"><?php echo number_format((float)$storage_fee_array[$i], 2, '.', ','); ?></td>
                     <?php } ?>
                  </tr>
               </tbody>
            </table>
            
         </div>
      </div>
   </div>
</div>
@stop