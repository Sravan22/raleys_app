@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>

<div class="container">
<div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

 <!------------------- Department Sales popup---------------------------->      
<div id="deptsalestot" style="margin-left: 100px;" class="mainbox miscbox col-md-10">
   <div class="panel panel-info safe_total">
  <!--  <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >Work By Register Screen.</div>
      </div> -->
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
      <div class="panel-title left" >Work By Register Screen</div>
         <div class="panel-title right" >Grand Total For : {{ $convert_date }}</div>
      </div>
      <div  class="panel-body" style="margin-top: 10px;">
          {{-- <div class="row vertical-divider">
         </div> --}}
         <div  class="panel-body" style="margin-top: 10px;">
         {{-- <div class="row vertical-divider"> --}}
         <?php $gtot=0; //echo '<pre>'; print_r($alldata); echo '</pre>'; exit;
          ?>
        @foreach($alldata as $key => $value)
     	<div class="col-xs-6" >
               	<div class="form-group">
                  <div class="col-sm-2">
                     <label for="inputPassword">({{ $value->item_id }})</label>
                  </div>
                  <div class="col-sm-6">
                   <label for="inputPassword">{{ trim($value->item_desc) }}</label>
                  </div>
                  <div class="col-sm-4">
                     <input type="text" class="form-control" readonly="readonly" value="{{ $value->gsubtot }}">
                  </div>
               </div>
        </div>
     @endforeach  
     {{-- </div> --}}
     </div>  
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-2">Total Credit</label>
            <div class="col-sm-2">
               <input  class="form-control grandtotal"  readonly="readonly" placeholder="0.00" id="grandtot"  
                       name="misc_grand_total" value="{{ $totarray['tot_cred']/100 }}">
            </div>

            <label for="inputPassword" class="control-label col-sm-2">Total Debits</label>
            <div class="col-sm-2">
               <input  class="form-control grandtotal"  readonly="readonly" placeholder="0.00" id="grandtot"  
                       name="misc_grand_total" value="{{ $totarray['tot_deb']/100 }}">
            </div>

            <label for="inputPassword" class="control-label col-sm-2">Total O/S</label>
            <div class="col-sm-2">
               <input  class="form-control grandtotal"  readonly="readonly" placeholder="0.00" id="grandtot"  
                       name="misc_grand_total" value="{{ $totarray['tot_os'] }}">
            </div>
         </div>
         </form>
      </div>
   </div>

<!------------------ End of Department Sales popup ---------------------->
@stop