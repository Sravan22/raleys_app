<?php
  //echo '<pre>';print_r($receivings);exit;
?>
@extends('layout.dashboard')
@section('page_heading','Receivings >> Printer Selection Menu')
@section('content')
@section('section')

<header class="row">
        @include('mktmgr.recivermenu')
    </header>
<style>
 
  @media print {
  a[href]:after {
    content: none !important;
  }
}
</style> 
<div class="container">
<div class="row">
<div class="col-md-6">
<h3>Expenses Report</h3></div>
<div class="col-md-6">
@if(count($receivings) > 0)
<span class="pull-right">
<a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
<a href="{{ route('pdf-expensesreport',['download'=>'pdf', 'frmdt' => $frmdt, 'todt' => $todt]) }}" target="_blank">
<i class="fa fa-file-pdf-o fa-fw iconsize"></i>
</a>
</span>
@endif
</div>
</div>
</div>

<div class="container">
 <table class="table table-striped">
    <thead>
      <tr>
        <th>Invoice Date</th>
        <th>Invoice Number</th>
        <th>Vendor Name</th>
        <th>Type</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    @if(count($receivings) > 0)
    @foreach ($receivings as $rcv)
      <tr>
        <td>{{ date("m/d/Y", strtotime($rcv->invoice_date)) }}</td>
        <td>{{ $rcv->id }}</td>
        <td>{{ $rcv->name }}</td>
        <td>{{ $rcv->type_code }}</td>
        {{-- <td>{{ $rcv->status_code }}</td> --}}
            @if($rcv->status_code == 'A') 
            <td>Accepted</td>
            @elseif ($rcv->status_code == 'H')
            <td>On Hold</td>
            @elseif ($rcv->status_code == 'I')
            <td>Incomplete</td>
            @elseif ($rcv->status_code == 'O')
            <td>Open</td>
            @elseif ($rcv->status_code == 'R')
            <td>Reviewed</td>
            @elseif ($rcv->status_code == 'U')
            <td>Uploaded</td>
            @else
            <td>Voided</td>
            @endif
      </tr>
    @endforeach
    @else
    <tr><td colspan="5" align="center">No records found!</td></tr>
    @endif
    </tbody>
  </table>          
 
</div>
@stop
