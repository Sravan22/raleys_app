<?php
   //echo '<pre>';print_r($shrinkcapturedepart);exit;
 ?>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h4>Shrink Captures for Department {{ $dept_no }} </h4>
         </b>
      </div>
   </div>
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-shrinkcapturedept',['download'=>'pdf','shrink_date'=>$shrink_date,'dept_no'=>$dept_no])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>
 
<div class="container">
<form action="#" class="form-horizontal" method="post" role="form" style="display: block;">
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Shrink Date</th>
            <th>Shrink Number</th>
            <th>Employee Id</th>
            <th>Total Line Items</th>
            <th>Total Value</th>
            <th>Status</th>
         </tr>
      </thead>
      <tbody>
         @for ($i = 0; $i < count($shrinkcapturedepart); $i++)
         <?php 
              $params = array(
                        'shrink_date' =>  date('m/d/Y',strtotime($shrinkcapturedepart[$i]->shrink_date)),
                        'shrink_number' =>  $shrinkcapturedepart[$i]->shrink_number,
                        'employee_id' =>  $shrinkcapturedepart[$i]->employee_id,
                        'tot_line_items' =>  $shrinkcapturedepart[$i]->tot_line_items,
                        'tot_value' =>  $shrinkcapturedepart[$i]->tot_value,
                        'seq_number' =>  $shrinkcapturedepart[$i]->seq_number,
                        'status' =>  $shrinkcapturedepart[$i]->status
                        ); 
              $queryString = http_build_query($params);
            ?>
         <tr>
            <td>{{ date('m/d/Y',strtotime($shrinkcapturedepart[$i]->shrink_date)) }}</td>
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-capture-itemscan',$queryString), $shrinkcapturedepart[$i]->shrink_number) }}</u></td>
           {{--  <td>{{ $shrinkcapturedepart[$i]->shrink_number }}</td> --}}
            <td>{{ $shrinkcapturedepart[$i]->employee_id }}</td>
            <td>{{ $shrinkcapturedepart[$i]->tot_line_items }}</td>
            <td>${{ $shrinkcapturedepart[$i]->tot_value }}</td>
            @if($shrinkcapturedepart[$i]->status == "O")
            <td>Open</td>
            @elseif($shrinkcapturedepart[$i]->status == "V")
            <td>Void</td>
            @elseif($shrinkcapturedepart[$i]->status == "D")
            <td>Deleted</td>
            @elseif($shrinkcapturedepart[$i]->status == "C")
            <td>Closed</td>
            @elseif($shrinkcapturedepart[$i]->status == "S")
            <td>Sent</td>
            @else
            <td>Unknown</td>
            @endif
            {{-- <td>{{ $shrinkcapturedepart[$i]->status }}</td> --}}
         </tr>
         
          @endfor
      </tbody>
   </table>
   <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
    <input type="button" class="btn" name="" onclick="history.go(-1);" value="Back">        
 
   {{ Form::token()}}
        </div>
        </div>
  </div>

  </form>
  </div>
   {{-- @else
   <div class="alert alert-danger">
      <strong>Alert!</strong> No Store Transfers meet Query criteria.
   </div>
   @endif --}}

@stop