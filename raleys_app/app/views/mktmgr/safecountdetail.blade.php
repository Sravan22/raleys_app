@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div id="safecountbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >SAFE COUNT</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-postsafecountdetail')}}" id="coin-form" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return checkamounts()">
               <input type="hidden" name="safe_date" id="dateinfo"  value="{{ $infodate }}" /> 
               <div class="form-group">
               <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Coin Total</label>
                  <div class="col-sm-4">
                     <input  type="button" value="0.00"  class="form-control" id="cointot"  name="cointot" onclick="hide_show_toggle_safecnt('coinbox','safecountbox')">
                     @if($errors->has('cointot'))
                     {{ $errors->first('cointot')}}
                     @endif
                  </div>
               
               </div>
               <div class="form-group">
               <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Currency Total</label>
                  <div class="col-sm-4">
                     
                     <input type="button" value="0.00"  class="form-control" id="currency_GrandTot1"  name="currencytot" onclick="hide_show_toggle_safecnt('currencybox','safecountbox')">
                     @if($errors->has('currencytot'))
                     {{ $errors->first('currencytot')}}
                     @endif
                  </div>
                 
               </div>
               <div class="form-group">
               <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Other Total</label>
                 
                  <div class="col-sm-4">
                     <input  class="form-control grandtotal" type="button" placeholder="0.00"  id="otherpricetot"  name="otherpricetot" value="0.00" onclick="hide_show_toggle_safecnt('miscbox','safecountbox')">
                     @if($errors->has('otherpricetot'))
                     {{ $errors->first('otherpricetot')}}
                     @endif
                  </div>
                 
               </div>
               <hr style="border:1px solid #000;" />
               <div class="form-group">
               <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-3">Grand Total</label>
                  <div class="col-sm-5">
                     <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" style="width: 180px;" placeholder="0.00" id="GrandTot"  name="GrandTot">
                     @if($errors->has('otherpricetot'))
                     {{ $errors->first('otherpricetot')}}
                     @endif
                  </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center" style="margin-left:61px;">
                        <input type="submit" name="safe-submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <!-- <input type="button" name="" id="submit" tabindex="4" value="Cancel" class="btn"> -->
                      <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/booksafecnt')}}'" />
                     </div>
                  </div>
               </div>
               <!-- </form> -->
         </div>
      </div>
   </div>
   <!------------------- Coinbox popup---------------------------->      
   <div id="coinbox" style="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >COIN</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">BOXED</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">ROLLED</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Pennies</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt" id="pennies_box"  name="pennies_box">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt" id="pennies_roll"  name="pennies_roll" >
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly=""  class="form-control " id="pennies_tot"  name="pennies_tot">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Nickles</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt1" id="nickle_box"  name="nickles_box">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt1" id="nickle_roll"  name="nickles_roll">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control " id="nickle_tot"  name="nickle_tot">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Dimes</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt2" id="dimes_box"  name="dimes_box">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt2" id="dimes_roll"  name="dimes_roll">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control" id="dimes_tot"  name="dimes_tot">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Quarters</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt3" id="quarter_box"  name="quarters_box">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt3" id="quarter_roll"  name="quarters_roll" >
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control" id="quarter_tot"  name="quarter_tot">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label">Misc</label>&nbsp;&nbsp;<input type="text" style="width:60px" class="form-control misc_desc" placeholder="0" name="misc_desc">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt4" id="misc_box"  name="misc_box" onblur="sum_of_qty_price(this.value,'misc')">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin txt4" id="misc_roll"  name="misc_roll" onblur="sum_of_qty_price(this.value,'misc')">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control" id="misc_tot"  name="misc_tot">
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <div class="col-sm-2">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-3" style="text-align: right;">Grand Total</label>
            <div class="col-sm-6">
               <input  class="form-control"  readonly="readonly" placeholder="0.00" id="Coin_GrandTot"  name="Coin_GrandTot">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" style="margin-left: 109px;" name="" id="coin_submit" tabindex="4" value="Submit" class="btn">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('coinbox','safecountbox')">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
         <!--   </form> -->
      </div>
   </div>
</div>
   <!------------------End of Coinbox popup---------------------->
   <!------------------- currency popup---------------------------->      
   <div id="currencybox" style="margin-left:87px;" class="mainbox col-md-10 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">CURRENCY</div>
      </div>
      <div style="padding-top:30px" class="panel-body">
         <div class="col-sm-3" style="padding-left: 48px;">
            <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
         </div>
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-3">LEFT</label>
         </div>
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-12" style="padding-left: 22px;">RIGHT</label>
         </div>
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-12">CASH HELD</label>
         </div>
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-12" style="padding-left:28px;">TOTAL</label>
         </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-3" style="padding-left: 68px;">
            <label for="inputPassword" class="control-label col-sm-12">Ones</label>
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyone ft txt5" id="ones_left"  name="ones_box"  onblur="sum_of_qty_price_currency(this.value,'ones')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyone ft txt5" id="ones_right"  name="ones_roll"  onblur="sum_of_qty_price_currency(this.value,'ones')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyone ft txt5" id="ones_cashheld"  name="ones_held" onblur="sum_of_qty_price_currency(this.value,'ones')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly="" class="form-control currencyonetotal" id="ones_tot"  name="ones_tot">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-3" style="padding-left: 68px;">
            <label for="inputPassword" class="control-label col-sm-12">Fives</label>
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyfive ft txt6" id="fives_left"  name="fives_box"  onblur="sum_of_qty_price_currency(this.value,'fives')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyfive ft txt6" id="fives_right"  name="fives_roll" onblur="sum_of_qty_price_currency(this.value,'fives')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyfive ft txt6" id="fives_cashheld"  name="fives_held" onblur="sum_of_qty_price_currency(this.value,'fives')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly=""  class="form-control currencyfivetotal" id="fives_tot"  name="fives_tot">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-3" style="padding-left: 68px;">
            <label for="inputPassword" class="control-label col-sm-12">Tens</label>
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyten ft txt7" id="tens_left"  name="tens_box" onblur="sum_of_qty_price_currency(this.value,'tens')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyten ft txt7" id="tens_right"  name="tens_roll" onblur="sum_of_qty_price_currency(this.value,'tens')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyten ft txt7" id="tens_cashheld"  name="tens_held" onblur="sum_of_qty_price_currency(this.value,'tens')" >
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly=""  class="form-control currencytentotal" id="tens_tot"  name="tens_tot">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-3" style="padding-left: 68px;">
            <label for="inputPassword" class="control-label col-sm-12">Twenties</label>
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencytwenty ft txt8" id="twenties_left"  name="twenties_box" onblur="sum_of_qty_price_currency(this.value,'twenties')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencytwenty ft txt8" id="twenties_right"  name="twenties_roll" onblur="sum_of_qty_price_currency(this.value,'twenties')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencytwenty ft txt8" id="twenties_cashheld"  name="twenties_held" onblur="sum_of_qty_price_currency(this.value,'twenties')">
         </div> 
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly="" class="form-control currencytwentytotal" id="twenties_tot"  name="twenties_tot">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-3" style="padding-left: 84px;">
            <label for="inputPassword" class="control-label">Misc 1 </label>&nbsp;&nbsp;<input type="number" style="width:70px" class="form-control misc1_desc" placeholder="0" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" name="misc1">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymiscone ft txt9" id="misc1_left"  name="misc1_box" onblur="sum_of_qty_price_currency(this.value,'misc1')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymiscone ft txt9" id="misc1_right"  name="misc1_roll" onblur="sum_of_qty_price_currency(this.value,'misc1')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymiscone ft txt9" id="misc1_cashheld"  name="misc1_held" onblur="sum_of_qty_price_currency(this.value,'misc1')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly="" class="form-control currencymisconetotal" id="misc1_tot"  name="misc1_tot">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-3 " style="padding-left: 84px;">
            <label for="inputPassword" class="control-label">Misc 2 </label>&nbsp;&nbsp;<input type="number" style="width:70px" class="form-control misc2_desc" placeholder="0" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" name="misc2">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymisctwo ft txt10" id="misc2_left"  name="misc2_box" onblur="sum_of_qty_price_currency(this.value,'misc2')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymisctwo ft txt10" id="misc2_right"  name="misc2_roll" onblur="sum_of_qty_price_currency(this.value,'misc2')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymisctwo ft txt10" id="misc2_cashheld"  name="misc2_held" onblur="sum_of_qty_price_currency(this.value,'misc2')">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly="" class="form-control currencymisctwototal" id="misc2_tot"  name="misc2_tot">
         </div>
      </div>
      </div>
      <hr style="border:1px solid #000;" />
      <div class="form-group">
      <div class="col-sm-2">&nbsp;</div>
         <label for="inputPassword" class="control-label col-sm-3" style="text-align: right;">Grand Total</label>
         <div class="col-sm-4">
            <input  class="form-control currencygrandtotal"  readonly="readonly" placeholder="0.00" id="currency_GrandTot"  name="currency_GrandTot">
            @if($errors->has('currency_GrandTot'))
            {{ $errors->first('currency_GrandTot')}}
            @endif
         </div>
      </div>
      <div class="form-group">
         <div class="row" style="margin-left: 71px;">
            <div class="col-sm-12" align="center">
               <input type="button" name="" id="total_submit" tabindex="4" value="Submit" class="btn">
               <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('currencybox','safecountbox')">
            </div>
         </div>
      </div>
      <!--   </form> -->
   </div>
</div>
</div>
<!------------------End of currency popup---------------------->
<!------------------- Misc popup---------------------------->      
<div id="miscbox" style="margin-left: 100px;" class="mainbox miscbox col-md-10">
   <div class="panel panel-info safe_total">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >MISC</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <div class="row vertical-divider" style="margin-top: 10px">
            <div class="col-xs-6" >
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                       <b>DESCRIPTION</b>
                  </div>
                  <div class="col-sm-4">
                     <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
                  </div>
               </div>
               <br><br>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Food stamps</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id="foodstamps"  name="food_stamps">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Total Merch checks</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="text" placeholder="0.00" readonly=""  class="form-control totalmerch" id=""  name="totalmerchchecks">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Total Script checks</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00" readonly="" class="form-control totalscript" id=""  name="totalscriptchecks">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Beginning Cash Loans</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="begin_loans">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Misc 1</label>&nbsp;&nbsp;<input type="text" style="width:70px;" placeholder="0" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control safeothermisc1" name="misc1">
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="misc1_amt">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Misc 2</label>&nbsp;&nbsp;<input type="text" style="width:70px;" placeholder="0" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control safeothermisc1" name="misc2">
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="misc2_amt">
                  </div>
               </div>
               <br />
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">In-Store Charges</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="instr_chrg">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Payroll Advances</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="pay_adv">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Night Crew Till</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="night_crew">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">EPS Debit To Carryover</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="debit">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">EPS Credit To Carryover</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="credit">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">EPS EBT To Carryover</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="ebt">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">EPS GC To Carryover</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="giftcard">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Misc 3</label>&nbsp;&nbsp;<input type="text" style="width:70px;" placeholder="0" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" class="form-control safeothermisc1" name="misc3_desc">
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft" id=""  name="misc3_amt">
                  </div>
               </div>
            </div>
            <div class="col-xs-6">
               <div class="form-group">
                  <div class="col-xs-8">
                      <b>Merchandise Checks</b>
               </div>
               </div>
               <br/><br/>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Fives</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control merch gtotal ft" id=""  name="merch_fives">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Tens</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control merch gtotal ft" id=""  name="merch_tens">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Twenties</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control merch gtotal ft" id=""  name="merch_twenties">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Misc</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control merch gtotal ft" id=""  name="merch_misc">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-8 ">
                      <b>Scripts Checks</b>
               </div>
               </div>
               
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script1</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script1">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script2</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script2">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script3</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script3">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script4</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script4">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script5</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script5">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script6</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script6">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script7</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script7">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script8</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script8">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script9</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script9">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script10</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft" id=""  name="script10">
                  </div>
               </div>
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
         
            <label for="inputPassword" class="control-label col-sm-2">Grand Total</label>
            <div class="col-sm-3">
               <input  class="form-control grandtotal"  readonly="readonly" placeholder="0.00" id="GrandTot"  name="misc_grand_total">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" style="padding-left: 20%">
                  <input type="button" name="" id="misc_submit" tabindex="4" value="Submit" class="btn">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('miscbox','safecountbox')">
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
<!------------------End of Misc popup---------------------->
</div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script>
   $(document).ready(function() {
   $(".txt,.txt1,.txt2,.txt3,.txt4,.txt5,.txt6,.txt7,.txt8,.txt9,.txt10").on("click", function () {
      $(this).select();
      });



   $('#coinbox').hide();
   $('#currencybox').hide();
   $('#miscbox').hide();
  
   $(".txt,.txt1,.txt2,.txt3,.txt4,.txt5,.txt6,.txt7,.txt8,.txt9,.txt10").blur(function(){  
       var sum = 0; 
        selfield = this.id;
         
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
     });

   $(".txt,.txt1,.txt2,.txt3,.txt4,.txt5,.txt6,.txt7,.txt8,.txt9,.txt10").on("blur", function() {
    calculateSum();
    calculatetotal();
    });


    function calculateSum() {
var sum = sum1 = sum2 = sum3 = sum4 = sum5 = sum6 = sum7 = sum8 = sum9 = sum10 = 0;
    

   //first row 
    $(".txt").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum += parseFloat(this.value);

    }    
    });
   $("input#pennies_tot").val(sum.toFixed(2));

   //second row 
    $(".txt1").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum1 += parseFloat(this.value);
    }    
    });
   $("input#nickle_tot").val(sum1.toFixed(2));
    

    //third row 
    $(".txt2").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum2 += parseFloat(this.value);
    }    
    });
   $("input#dimes_tot").val(sum2.toFixed(2));


   //Forth row 
    $(".txt3").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum3 += parseFloat(this.value);
    }    
    });
   $("input#quarter_tot").val(sum3.toFixed(2));


   //Fifth row 
    $(".txt4").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum4 += parseFloat(this.value);
    }    
    });
   $("input#misc_tot").val(sum4.toFixed(2));



   //currency ones row 
    $(".txt5").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum5 += parseFloat(this.value);
    }    
    });
   $("input#ones_tot").val(sum5.toFixed(2));

   //currency fives row 
    $(".txt6").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum6 += parseFloat(this.value);
    }    
    });
   $("input#fives_tot").val(sum6.toFixed(2));

   //currency tens row 
    $(".txt7").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum7 += parseFloat(this.value);
    }    
    });
   $("input#tens_tot").val(sum7.toFixed(2));

   //currency twenties row 
    $(".txt8").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum8 += parseFloat(this.value);
    }    
    });
   $("input#twenties_tot").val(sum8.toFixed(2));

   //currency misc1 row 
    $(".txt9").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum9 += parseFloat(this.value);
    }    
    });
   $("input#misc1_tot").val(sum9.toFixed(2));


   //currency misc2 row 
    $(".txt10").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   sum10 += parseFloat(this.value);
    }    
    });
   $("input#misc2_tot").val(sum10.toFixed(2));



    } 


function calculatetotal()
{
   var totalsum = 0;
   var totalsum1 = 0;

   //coin total
   $(".txt,.txt1,.txt2,.txt3,.txt4").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   totalsum += parseFloat(this.value);

    }    
    });
   $("input#Coin_GrandTot").val(totalsum.toFixed(2));

   //currency total
   $(".txt5,.txt6,.txt7,.txt8,.txt9,.txt10").each(function() {
   if (!isNaN(this.value) && this.value.length != 0) {
   totalsum1 += parseFloat(this.value);

    }    
    });
   $("input#currency_GrandTot").val(totalsum1.toFixed(2));

}
    


  
  
    });


  




   function sum_of_qty_price(value,field_str)
   {
    //alert(value);
    //alert($('#'+field_str+'_box').val());
   var box=parseFloat($('#'+field_str+'_box').val());
   //alert(box);
   var boxval=box.toFixed(2);
   $('#'+field_str+'_box').val(boxval);
   
   var roll=parseFloat($('#'+field_str+'_roll').val());
   var rollval=roll.toFixed(2);
   $('#'+field_str+'_roll').val(rollval);
    var tot = parseFloat(boxval) + parseFloat(rollval); 
   //alert(tot);
   
   $('#'+field_str+'_tot').val(tot.toFixed(2)); 
   sum_of_all_tot();
   }
   function sum_of_all_tot()
   {
      //alert($('#pennies_tot').val());return false;
    peniestot=parseFloat($('#pennies_tot').val());
    nickletot=parseFloat($('#nickle_tot').val());
    dimestot=parseFloat($('#dimes_tot').val());
    quartertot=parseFloat($('#quarter_tot').val());
    misctot=parseFloat($('#misc_tot').val());
    if(!isNaN(peniestot))
    {
      var total=peniestot;
      //alert(total);
    }
    
    if(!isNaN(nickletot))
    {
      var total=peniestot+nickletot;
      //alert(total);
      
    }
    if(!isNaN(dimestot))
    {
      var total=peniestot+nickletot+dimestot;
      //alert(total);
    }
    if(!isNaN(quartertot))
    {
      var total=peniestot+nickletot+quartertot+dimestot;
      //alert(total);
    }
    if(!isNaN(misctot))
    {
      var total=peniestot+nickletot+quartertot+misctot+dimestot;
      //alert(total);
    }
   
   
   var coingrandtotal = $('#Coin_GrandTot').val(total.toFixed(2));
    
   }
   function sum_of_qty_price_currency(value,field_str)
   {
    
   var box=parseFloat($('#'+field_str+'_left').val());
   
   var boxval=box.toFixed(2);
   $('#'+field_str+'_left').val(boxval);
   
   var roll=parseFloat($('#'+field_str+'_right').val());
   var rollval=roll.toFixed(2);
   $('#'+field_str+'_right').val(rollval);
   
   var cash=parseFloat($('#'+field_str+'_cashheld').val());
   var cashval=cash.toFixed(2);
   $('#'+field_str+'_cashheld').val(cashval);
    var tot = parseFloat(boxval) + parseFloat(rollval) + parseFloat(cashval); 
   
   
   $('#'+field_str+'_tot').val(tot.toFixed(2)); 
   sum_of_all_tot_currency();
   }
   function sum_of_all_tot_currency()
   {
    onestot=parseFloat($('#ones_tot').val());
    fivestot=parseFloat($('#fives_tot').val());
    tenstot=parseFloat($('#tens_tot').val());
    twentiestot=parseFloat($('#twenties_tot').val());
    misc1tot=parseFloat($('#misc1_tot').val());
    misc2tot=parseFloat($('#misc2_tot').val());
    if(!isNaN(onestot))
    {
      var total=onestot;
    }
    
    if(!isNaN(fivestot))
    {
      var total=onestot+fivestot;
      
    }
    if(!isNaN(tenstot))
    {
      var total=onestot+fivestot+tenstot;
    }
    if(!isNaN(twentiestot))
    {
      var total=onestot+fivestot+tenstot+twentiestot;
    }
    if(!isNaN(misc1tot))
    {
      var total=onestot+fivestot+tenstot+twentiestot+misc1tot;
    }
    if(!isNaN(misc2tot))
    {
      var total=onestot+fivestot+tenstot+twentiestot+misc1tot+misc2tot;
    }
   
   
   $('#currency_GrandTot').val(total.toFixed(2));
   $('#currency_GrandTot1').val(total.toFixed(2));
  
    //$(".greatgranttotal").val(parseInt(total)+parseInt(cointot)+parseInt(sum1));
   }
   function hide_show_toggle_safecnt(pop_div,main_div)
   {
    //alert('hiii');
    $('#'+pop_div).show();
    $('#'+main_div).hide();
   }
   function cancel_click(pop_div,main_div)
   {
    $('#'+pop_div).hide();
    $('#'+main_div).show();
   }
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Sravan Kumar Sriramula Code js Code -->
<script type="text/javascript">  
   $(document).ready(function(){

      $("#coin_submit").click(function (e) { 
         //alert($('.ft').val());
         // if($(".ft").val() == '')
         // {
         //    $(".ft").css("border-color", "red");
         //    //alert("Please fill atleast one form details");
         //    return false;
         // }
         //return false;
      var datastring = $("#coin-form").serialize();
      //alert(datastring);
       $.post("coindetails",{data:datastring} ,function(data) {
                      // alert(data);
                           //$('#stage1').html(data);
       });
       cancel_click('coinbox','safecountbox');
        cointot=$('#Coin_GrandTot').val();
        //alert(cointot);return false;
       $('#cointot').val(cointot);
       //alert($('#cointot').val(cointot));return false;
         return false;
      });

      $("#total_submit").click(function (e) { 
      var datastring = $("#coin-form").serialize();
      //alert(datastring);
       $.post("coindetails",{data:datastring} ,function(data) {
                      // alert(data);
                           //$('#stage1').html(data);
       });
       cancel_click('currencybox','safecountbox');
        //cointot=$('#Coin_GrandTot').val();
       //$('#cointot').val(cointot);
         return false;
      });

      $("#misc_submit").click(function (e) { 
      var datastring = $("#coin-form").serialize();
      //alert(datastring);
       $.post("coindetails",{data:datastring} ,function(data) {
                      // alert(data);
                           //$('#stage1').html(data);
       });
       cancel_click('miscbox','safecountbox');
        //cointot=$('#Coin_GrandTot').val();
       //$('#cointot').val(cointot);
         return false;
      });
   
   });

</script>
<script type="text/javascript">
   $(document).ready(function() {
    $(".merch").each(function() {
         $(this).keyup(function() {
         calculateSum();
        });
    });
});

function calculateSum() {
    var sum = 0;
    $(".merch").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".totalmerch").val(sum.toFixed(2));
 }


  $(document).ready(function() {
    $(".script").each(function() {
         $(this).keyup(function() {
          calculateSum1 ();
        });
    });
});

function calculateSum1() {
    var sum = 0;
    $(".script").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".totalscript").val(sum.toFixed(2));
 }

$(document).ready(function() {
    $(".gtotal").each(function() {
         $(this).keyup(function() {

         calculateSum2();
        });
    });
});

function calculateSum2() {
    var sum1 = 0;
    $(".gtotal").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum1 += parseFloat(this.value);
         }
    });
   $(".grandtotal").val(sum1.toFixed(2));
 }
 ///$(".greatgranttotal").val(parseInt(total)+parseInt(cointot)+parseInt(sum.toFixed(2)));
  //$(".greatgranttotal").val(parseInt(total)+parseInt(cointot)+parseInt(miscfinaltotal));
 $(document).ready(function() {
    $(".currencyone").each(function() {
         $(this).keyup(function() {
         calculateSum3();
        });
    });
});

function calculateSum3() {
    var sum = 0;
    $(".currencyone").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencyonetotal").val(sum.toFixed(2));
 }

 $(document).ready(function() {
    $(".currencyfive").each(function() {
         $(this).keyup(function() {
         calculateSumFive();
        });
    });
});

function calculateSumFive() {
    var sum = 0;
    $(".currencyfive").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencyfivetotal").val(sum.toFixed(2));
 }
 
  $(document).ready(function() {
    $(".currencyten").each(function() {
         $(this).keyup(function() {
         calculateSumTen();
        });
    });
});

function calculateSumTen() {
    var sum = 0;
    $(".currencyten").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencytentotal").val(sum.toFixed(2));
 }

 $(document).ready(function() {
    $(".currencytwenty").each(function() {
         $(this).keyup(function() {
         calculateSumTwenty();
        });
    });
});

function calculateSumTwenty() {
    var sum = 0;
    $(".currencytwenty").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencytwentytotal").val(sum.toFixed(2));
 }

 $(document).ready(function() {
    $(".currencymisctwo").each(function() {
         $(this).keyup(function() {
         calculateSumMiscTwo();
        });
    });
});

function calculateSumMiscTwo() {
    var sum = 0;
    $(".currencymisctwo").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencymisctwototal").val(sum.toFixed(2));
 }
 $(document).ready(function() {
    $(".currencymiscone").each(function() {
         $(this).keyup(function() {
         calculateSumMiscOne();
        });
    });
});

function calculateSumMiscOne() {
    var sum = 0;
    $(".currencymiscone").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencymisconetotal").val(sum.toFixed(2));
 }

  $(document).ready(function() {
    $(".ft").each(function() {
         $(this).keyup(function() {
         calculateFinalTotal();
        });
    });
});

function calculateFinalTotal() {
    var sum = 0;
    $(".ft").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".ftotal").val(sum.toFixed(2));
 }

 function checkamounts()
 {
   //alert("Form Submitted");
   //return false;
   //alert($("#cointot").val());
   if($("#cointot").val() == '' || $("#currency_GrandTot1").val() == '' || $("#otherpricetot").val() == '')
   {
     // alert("Please fill atleast one form details");
      $("#errorDiv").html('Please fill Coin Currency and Other Totals');
      //$("#errorDiv").html('Please fill Coin Currency and Other Totals');
      $( "#errorDiv" ).fadeOut( 10000 );

      return false;
   }
   else
   {
      $("#errorDiv").html('');
   }
   
 }
 $(function() {
            $('.gtotal').blur(function() {
                var amt = parseFloat(this.value);
                $(this).val(amt.toFixed(2));
            });


        });
 //var finaltotal = coingrandtotal+miscgrandtotal+currencygrandtotal;
 //$(".greatgranttotal").val(finaltotal);

</script>
<style type="text/css">
   .row.vertical-divider {
   overflow: hidden;
   }
   .row.vertical-divider > div[class^="col-"] {
   text-align: center;
   padding-bottom: 100px;
   margin-bottom: -100px;
   border-left: 1px solid #000;
   border-right: 0px solid #000;
   }
   .row.vertical-divider div[class^="col-"]:first-child {
   border-left: none;
   }
   .row.vertical-divider div[class^="col-"]:last-child {
   border-right: none;
   }
   .misc_desc{
      display: initial !important;
   } 
   .misc1_desc{
      display: initial !important;
   }
   .misc2_desc{
      display: initial !important;
   }
   .safeothermisc1{
      display: initial !important;
   }
   .safeothermisc2{
      display: initial !important;
   }
   @media (min-width: 992px){
   .miscbox.safe_total{
       margin-left: 91px !important;
   }
}
</style>