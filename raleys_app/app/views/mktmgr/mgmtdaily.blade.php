<?php
   //echo $dayIsLocked.' '.$file_locked;exit; 
   //echo '<pre>';print_r($disp_storetx_array);exit;
?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<style type="text/css">
   #coinbox{
   display: none;
   }
   #currencybox{
   display: none;
   }
   #miscbox{
   display: none;
   }
   .menu_search_line{
   /* display: none;*/
   height: 0px;
   }
   .misc_desc{
   display: initial !important;
   } 
   ::-webkit-input-placeholder {
   text-transform: initial;
   }
   :-moz-placeholder { 
   text-transform: initial;
   }
   ::-moz-placeholder {  
   text-transform: initial;
   }
   :-ms-input-placeholder { 
   text-transform: initial;
   }
</style>
<?php 
   $nextDate = date('m/d/Y',strtotime($safe_date . "+1 days"));
?>
<div class="container">
<div id="safereportbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">SAFE REPORT &nbsp;&nbsp;&nbsp;&nbsp; {{ date('m/d/Y',strtotime($safe_date)) }}</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <form {{--  action="{{URL::route('mktmgr-postsafecountdetail')}}"  --}} id="" class="form-horizontal" method="post" role="form" style="display: block;">
         <input type="hidden" name="safe_date" id="safe_date"  value=" " /> 
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(BS) Beginning Safe</label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="begin_safe">{{ number_format((float)$resultArray['begin_safe'], 2, '.', ''); }}</span></strong>
               <input type="hidden" name="" class="ft" value="{{ number_format((float)$resultArray['begin_safe'], 2, '.', ''); }}">
            </div>
         </div>
         <br>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(DT) Deliveries & Transfers</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="2" value="{{ number_format((float)$resultArray['del_tranfs'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('dandtbox','safereportbox')" class="btn del_tranfs ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(DP) Deposits</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$resultArray['deposits'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('depositsbox','safereportbox')" class="btn depositstotal ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(ST) Store Transactions</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$resultArray['store_tx'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('stbox','safereportbox')" class="btn st_total ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(PO) Paid Out Types</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$resultArray['po_types'], 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('potbox','safereportbox')" class="btn pot_total ft" style="width:125px;text-align: right;">
            </div>
         </div>
         <br>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(TS) Total To Account For</label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="tot_acct">{{ number_format((float)$resultArray['tot_acct'], 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(CT) Safe Count Total</label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="safe_cnt">{{ number_format((float)$resultArray['safe_cnt'], 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(SA) Safe Over<<span>Short</span>></label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="safe_os">{{ number_format((float)$resultArray['safe_os'], 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(GO) Grocery Over<<span>Short</span>></label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="groc_os">{{ number_format((float)$resultArray['groc_os'], 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">(DO) Drug Over<<span>Short</span>></label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="drug_os">{{ number_format((float)$resultArray['drug_os'], 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <br>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">STORE TOTAL OVER<<span>Short</span>></label>
            <div class="col-sm-3" style="text-align:right;  margin-top:6px;   padding-bottom: 8px;">
               <strong><span class="total">{{ number_format((float)$resultArray['total'], 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="row">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-8 text-center">
               <input type="button" name="safecountsubmit" id="safecountsubmit" value="Submit" class="btn" onClick="document.location.href='{{URL::to('mktmgr/mgmtdailydate')}}'">
               <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/mgmtdailydate')}}'" />
               <input type="button" name="" id="mgmtapproval" value="MGMT Approval" class="btn btn-info mgmtapproval">
            </div>
            <div class="col-sm-2">&nbsp;</div>
         </div>
         
      <div class="row"><br>
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center mgmtapprovalmsg" style="color:red;">
                
            </div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         {{-- @if($dayIsLocked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Data is locked for {{ date("m/d/Y", strtotime($safe_date)) }}. Any changes won't be saved</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif --}}
         </form>
      </div>
   </div>
</div>
{{-- Delivery Box Starts --}}  
<div id="deliverybox" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <form method="post" id="deliveries-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">DELIVERIES</div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($safe_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div  class="panel-body scrollbar" id="style-1">
               <?php
                  $delivery_total = 0;
                  for($i=0;$i<count($disp_del_array);$i++)
                  {
                   $delivery_total = $delivery_total + $disp_del_array[$i]['item_amt'];
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-2">
                        <label for="inputPassword">(&nbsp;<?php echo $disp_del_array[$i]['item_id']; ?>&nbsp;)</label>
                     </div>
                     <div class="col-sm-6"> 
                        <b><?php echo $disp_del_array[$i]['item_desc']; ?></b>
                     </div>
                     @if($file_locked == "Y")
                     <div class="col-sm-4 text-right">
                        <strong><span>{{ number_format((float)$disp_del_array[$i]['item_amt'], 2, '.', ''); }}</span></strong>
                     </div>
                     @else
                     <div class="col-sm-4">
                        <input type="text" class="form-control text-right deliveries" style="width: 100%"   name="<?php echo $disp_del_array[$i]['item_id']; ?>" id="<?php echo $disp_del_array[$i]['item_id']; ?>" value="<?php echo number_format((float)$disp_del_array[$i]['item_amt'], 2, '.', ''); ?>">
                     </div>
                     @endif
                  </div>
               </div>
               <?php  }
                  ?>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row" style="margin-left:31px;margin-top:-2px;">
               <div class="col-sm-6">
                  <div class="row">
                     <div class="col-sm-6">
                        <b>Total to account For</b>
                     </div>
                     <div class="col-sm-6" style="margin-left:177px;font-size:16px;text-align:right;margin-top: -18px;">
                        <strong><span class="deliveries_total">{{ number_format((float)$delivery_total, 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php if($file_locked == "Y"){ echo 'disabled'; } ?> name="login-submit" id="deliveries-submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('deliverybox','dandtbox')"/>
               </div>
            </div>
         </div>
         <br>
         @if($file_locked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif
         </br>
      </div>
   </form>
</div>
{{-- Delivery Box Ends --}} 
{{-- Transfers Box Starts --}}  
<div id="transfersbox" style="margin-left: 180px;display: none" class="mainbox miscbox col-md-8">
   <form method="post" id="transfers-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">TRANSFERS   </div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($safe_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-2" style="padding-left: 26px;">
                     <label for="inputPassword" class="control-label col-sm-12">ID</label>
                  </div>
                  <div class="col-sm-4" style="padding-left: 45px;">
                     <label for="inputPassword" class="control-label col-sm-12">Description</label>
                  </div>
                  <div class="col-sm-3 text-center">
                     <label for="inputPassword" class="control-label col-sm-12">Trf Amount</label>
                  </div>
                  {{-- <div class="col-sm-3 text-center">
                     <label for="inputPassword" class="control-label col-sm-12">ID Number</label>
                  </div> --}}
               </div>
            </div>
            <?php
               $transfers_total = 0;
               for($i=0;$i<count($disp_trf_array);$i++)
               {
                $transfers_total = $transfers_total + $disp_trf_array[$i]['item_amt'];
               ?>
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-2">
                     <label for="inputPassword" class="control-label col-sm-12">(&nbsp;<?php echo $disp_trf_array[$i]['item_id']; ?>&nbsp;)</label>
                  </div>
                  <div class="col-sm-4">
                     <label for="inputPassword" class="control-label col-sm-12"><?php echo $disp_trf_array[$i]['item_desc']; ?></label>
                  </div>
                  @if($file_locked == "Y")
                  <div class="col-sm-3 text-right" style="margin-left: -53px;">
                     <strong><span>{{ number_format((float)$disp_trf_array[$i]['item_amt'], 2, '.', ''); }}</span></strong>
                  </div>
                  <!-- <div class="col-sm-3 text-right">
                     <strong><span><?php if($disp_trf_array[$i]['source_id']) { echo $disp_trf_array[$i]['source_id']; } else { echo '0'; }  ?></span></strong>
                  </div> -->
                  @else
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-12"><input type="text" class="form-control text-right transfers" style="width: 100%" name="<?php echo $disp_trf_array[$i]['item_id']; ?>" id="<?php echo $disp_trf_array[$i]['item_id']; ?>" value="<?php echo number_format((float)$disp_trf_array[$i]['item_amt'], 2, '.', ''); ?>"></label>
                  </div>
                  <!-- <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-12"><input type="text" class="form-control text-right" style="width: 100%" name="<?php echo $disp_trf_array[$i]['item_id']; ?>" id="<?php echo $disp_trf_array[$i]['source_id']; ?>" value="<?php echo  $disp_trf_array[$i]['source_id']; ?>"></label>
                  </div> -->  
                  @endif
               </div>
            </div>
            <?php  }
               ?>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row">
               <div class="form-group">
                  <div class="col-sm-2" style="padding-left: 26px;">
                     <label for="inputPassword" class="control-label col-sm-12">&nbsp;</label>
                  </div>
                  <div class="col-sm-4 text-right" style="margin-left: -53px;">
                     <label for="inputPassword" class="control-label col-sm-12">Total</label>
                  </div>
                  <div class="col-sm-3 text-right" <?php if($file_locked == "Y") { echo 'style="margin-left:16px"'; }  else { echo 'style="margin-left:40px"'; } ?>>
                     <label for="inputPassword" class="control-label col-sm-12"><span class="transfers_total">{{ number_format((float)$transfers_total, 2, '.', ''); }}</span></label>
                  </div>
                  <div class="col-sm-3">
                     <label for="inputPassword" class="control-label col-sm-12">&nbsp;</label>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php if($file_locked == "Y"){ echo 'disabled'; } ?> name="login-submit" id="transfers-submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('transfersbox','dandtbox')"/>
               </div>
            </div>
         </div>
         <br>
         @if($file_locked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif
         <br>
      </div>
   </form>
</div>
{{-- Transfers Box Ends --}}
{{-- Delivery & Transfers Box Starts  --}}  
<div id="dandtbox" style="display: none;" class="mainbox col-md-4 col-md-offset-4">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">DELIVERY AND TRANSFERS &nbsp;&nbsp;&nbsp;&nbsp; {{ date('m/d/Y',strtotime($safe_date)) }}</div>
      </div>
      <div class="panel-body">
         <form {{--  action="{{URL::route('mktmgr-postsafecountdetail')}}"  --}} id="" class="form-horizontal" method="post" role="form" style="display: block;">
         <input type="hidden" name="safe_date" id="safe_date"  value=" " /> 
         <br>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6"> Deliveries  </label>
            <div class="col-sm-3">
               <input  type="button" tabindex="2" value="{{ number_format((float)$delivery_total, 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('deliverybox','dandtbox')" class="btn deliveriestotal" style="width:125px;text-align:right;margin-left: -42px;">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6">Transfers</label>
            <div class="col-sm-3">
               <input  type="button" tabindex="3" value="{{ number_format((float)$transfers_total, 2, '.', ''); }}" onclick="hide_show_toggle_safecnt('transfersbox','dandtbox')" class="btn transferstotal" style="width:125px;text-align:right;margin-left: -42px;">
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="col-sm-1">&nbsp;</div>
            <label for="inputPassword" class="control-label col-sm-6" style="margin-top:-9px;">Total</label>
            <div class="col-sm-3 text-right" style="margin-left:10px;">
               <strong><span class="delivery_transfer_total">{{ number_format((float)($delivery_total + $transfers_total), 2, '.', ''); }}</span></strong>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="row">
            <div class="col-sm-3">&nbsp;</div>
            <div class="col-sm-6">
               <input type="button" <?php if($file_locked == "Y"){ echo 'disabled'; } ?> name="safecountsubmit" id="safecountsubmit" value="Submit" class="btn" onclick="cancel_click('dandtbox','safereportbox')">
               <input type="button" value="Cancel" class="btn" onclick="cancel_click('dandtbox','safereportbox')" />
            </div>
            <div class="col-sm-3">&nbsp;</div>
         </div>
         <br>
         {{-- @if($dayIsLocked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Data is locked for {{ date("m/d/Y", strtotime($safe_date)) }}. Any changes won't be saved</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif --}}
         </form>
      </div>
   </div>
</div>
{{-- Delivery & Transfers Box Ends --}} 
{{-- Deposits Box Starts --}}  
<div id="depositsbox" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <form method="post" id="depositsbox-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">DEPOSITS   </div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($safe_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div  class="panel-body scrollbar" id="style-1">
               <?php
                  $deposits_total = 0;
                  for($i=0;$i<count($disp_deposits_array);$i++)
                  {
                   $deposits_total = $deposits_total + $disp_deposits_array[$i]['item_amt'];
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-2">
                        <label for="inputPassword">(&nbsp;<?php echo $disp_deposits_array[$i]['item_id']; ?>&nbsp;)</label>
                     </div>
                     <div class="col-sm-6"> 
                        <b><?php echo $disp_deposits_array[$i]['item_desc']; ?></b>
                     </div>
                     @if($file_locked == "Y")
                     <div class="col-sm-4 text-right">
                        <strong><span>{{ number_format((float)$disp_deposits_array[$i]['item_amt'], 2, '.', ''); }}</span></strong>
                     </div>
                     @else
                     <div class="col-sm-4">
                        <input type="text" class="form-control text-right deposits" style="width: 100%" id="<?php echo $disp_deposits_array[$i]['item_id']; ?>" name="<?php echo $disp_deposits_array[$i]['item_id']; ?>" value="<?php echo number_format((float)$disp_deposits_array[$i]['item_amt'], 2, '.', ''); ?>">
                     </div>
                     @endif
                  </div>
               </div>
               <?php  }
                  ?>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row" style="margin-left:31px;margin-top:-2px;">
               <div class="col-sm-6">
                  <div class="row">
                     <div class="col-sm-6">
                        <b>Total to account For</b>
                     </div>
                     <div class="col-sm-6" style="padding-left:86px;font-size:16px;">
                        <strong><span class="deposits_total">{{ number_format((float)$deposits_total, 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php if($file_locked == "Y"){ echo 'disabled'; } ?> name="login-submit" id="depositsbox-submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('depositsbox','safereportbox')"/>
               </div>
            </div>
         </div>
         <br>
         @if($file_locked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif
         <br>
      </div>
   </form>
</div>
{{-- Deposits Box Ends --}} 
{{-- Store Transactions Box Starts --}}  
<div id="stbox" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <form method="post" id="stbox-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">STORE TRANSACTIONS   </div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($safe_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div  class="panel-body scrollbar" id="style-1">
               <?php
                  $storetx_total = 0;
                  for($i=0;$i<count($disp_storetx_array);$i++)
                  {
                   $storetx_total = $storetx_total + $disp_storetx_array[$i]['item_amt'];
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-2" style="font-size:12px;">
                        <label for="inputPassword">(&nbsp;<?php echo $disp_storetx_array[$i]['item_id']; ?>&nbsp;)</label>
                     </div>
                     <div class="col-sm-6" style="font-size:12px;">
                        <b><?php echo $disp_storetx_array[$i]['item_desc']; ?></b>
                     </div>
                     @if($file_locked == "Y")
                     <div class="col-sm-4 text-right">
                        <strong><span>{{ number_format((float)$disp_storetx_array[$i]['item_amt'], 2, '.', ''); }}</span></strong>
                     </div>
                     @else
                     <div class="col-sm-4">
                        <input type="text" class="form-control text-right store_tx" style="width: 100%" name="<?php echo $disp_storetx_array[$i]['item_id']; ?>" id="<?php echo $disp_storetx_array[$i]['item_id']; ?>" value="<?php echo number_format((float)$disp_storetx_array[$i]['item_amt'], 2, '.', ''); ?>">
                     </div>
                     @endif
                  </div>
               </div>
               <?php  }
                  ?>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row" style="margin-left:31px;margin-top:-2px;">
               <div class="col-sm-6">
                  <div class="row">
                     <div class="col-sm-6">
                        <b>Total to account For</b>
                     </div>
                     <div class="col-sm-6" style="margin-left:177px;font-size:16px;text-align:right;margin-top: -18px;">
                        <strong><span class="store_tx_total">{{ number_format((float)$storetx_total, 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php if($file_locked == "Y"){ echo 'disabled'; } ?> name="login-submit" id="stbox_submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('stbox','safereportbox')"/>
               </div>
            </div>
         </div>
         <br>
         @if($file_locked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif
         <br>
      </div>
   </form>
</div>
{{-- Store Transactions Ends --}} 
{{--Paid Out Types Box Starts --}}  
<div id="potbox" style="margin-left: 100px;display: none" class="mainbox miscbox col-md-10">
   <form method="post" id="potbox-form">
      <div class="panel panel-info">
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title">
               <div class="row">
                  <div class="col-md-6">PAID OUT TYPES   </div>
                  <div class="col-md-6">{{ date('m/d/Y',strtotime($safe_date)) }}</div>
               </div>
            </div>
         </div>
         <div  class="panel-body" id="wrapper" style="margin-top: 10px;">
            <div  class="panel-body scrollbar" id="style-1">
               <?php
                  $potypes_total = 0;
                  for($i=0;$i<count($disp_potypes_array);$i++)
                  {
                   $potypes_total = $potypes_total + $disp_potypes_array[$i]['item_amt'];
                  ?>
               <div class="col-xs-6" >
                  <div class="form-group">
                     <div class="col-sm-2" style="font-size:12px;">
                        <label for="inputPassword">(&nbsp;<?php echo $disp_potypes_array[$i]['item_id']; ?>&nbsp;)</label>
                     </div>
                     <div class="col-sm-6" style="font-size:12px;">
                        <b><?php echo $disp_potypes_array[$i]['item_desc']; ?></b>
                     </div>
                     @if($file_locked == "Y")
                     <div class="col-sm-4 text-right">
                        <strong><span>{{ number_format((float)$disp_potypes_array[$i]['item_amt'], 2, '.', ''); }}</span></strong>
                     </div>
                     @else
                     <div class="col-sm-4">
                        <input type="text" class="form-control text-right po_types" style="width: 100%" name="<?php echo $disp_potypes_array[$i]['item_id']; ?>" id="<?php echo $disp_potypes_array[$i]['item_id']; ?>" value="<?php echo number_format((float)$disp_potypes_array[$i]['item_amt'], 2, '.', ''); ?>">
                     </div>
                     @endif
                  </div>
               </div>
               <?php  }
                  ?>
            </div>
            <hr style="margin-top:7px;margin-bottom:7px;" />
            <div class="row" style="margin-left:31px;margin-top:-2px;">
               <div class="col-sm-6">
                  <div class="row">
                     <div class="col-sm-6">
                        <b>Total to account For</b>
                     </div>
                     <div class="col-sm-6" style="margin-left:177px;font-size:16px;text-align:right;margin-top: -18px;">
                        <strong><span class="po_types_total">{{ number_format((float)$potypes_total, 2, '.', ''); }}</span></strong>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <hr style="margin-top:7px;margin-bottom:7px;" />
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" <?php if($file_locked == "Y"){ echo 'disabled'; } ?> name="login-submit" id="potbox_submit" tabindex="4" value="Submit" class="btn">
                  <input type="button" value="Cancel" class="btn"  onclick="cancel_click('potbox','safereportbox')"/>
               </div>
            </div>
         </div>
         <br>
         @if($file_locked == "Y")
         <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
            <div class="col-md-1">&nbsp;</div>
         </div>
         @endif
         <br>  
      </div>
   </form>
</div>
{{-- Paid Out Types Ends --}} 
@stop
<style type="text/css">
   /* #coinbox{
   display:none;
   }*/
</style>
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css"
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<link rel="stylesheet" href="{{ asset("assets/plugins/vdialog/css/vdialog.css") }}" />
<script src="http://code.jquery.com/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="{{ asset("assets/plugins/vdialog/js/lib/vdialog1.js") }}"></script>
<script type="text/javascript">
   function hide_show_toggle_safecnt(pop_div,main_div)
   {
     $('#'+pop_div).show();
     $('#'+main_div).hide();
   }
   function cancel_click(pop_div,main_div)
   {
     $('#'+pop_div).hide();
     $('#'+main_div).show();   
   }  
   $(document).ready(function(){
       
      $(".deposits,.deliveries,.transfers,.store_tx,.po_types,.ft").focus(function(event) {
          var sum = 0; 
         selfield = this.id;
          selfieldval=$("#"+selfield).val(); 
         $("#"+selfield).val(Math.round(selfieldval*100));
         this.select();


          


      });
   
   $(".deposits,.deliveries,.transfers,.store_tx,.po_types,.ft").blur(function(){  
       var sum = 0; 
        selfield = this.id;
         selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
   
       
     });
      
    
   
      $(".deposits,.deliveries,.transfers,.store_tx,.po_types,.ft").on("blur", function(event) {
         event.preventDefault();
       calculateSum();
       calculatetotal();
       });
      function calculateSum() {
      var deposits_sum = deliveries_sum = transfers_sum = store_tx_sum = po_types_sum = 0;
      
      $(".deposits").each(function() {
         if (!isNaN(this.value) && this.value.length != 0) {
         deposits_sum += parseFloat(this.value);
         
          }    
       });
      $(".deposits_total").text(deposits_sum.toFixed(2));
      $(".depositstotal").val(deposits_sum.toFixed(2));
   
       $(".deliveries").each(function() {
         if (!isNaN(this.value) && this.value.length != 0) {
         deliveries_sum += parseFloat(this.value);
         
          }    
       });
      $(".deliveries_total").text(deliveries_sum.toFixed(2));
      $(".deliveriestotal").val(deliveries_sum.toFixed(2));
   
      $(".transfers").each(function() {
         if (!isNaN(this.value) && this.value.length != 0) {
         transfers_sum += parseFloat(this.value);
         
          }    
       });
      $(".transfers_total").text(transfers_sum.toFixed(2));
      $(".transferstotal").val(transfers_sum.toFixed(2));
      
      $(".delivery_transfer_total").text((parseFloat($(".deliveries_total").text()) + parseFloat($(".transfers_total").text())).toFixed(2));
      $(".del_tranfs").val($(".delivery_transfer_total").text());

     
      
      //$(".delivery_transfer_total").text((parseFloat($(".deliveriestotal").val()) + parseFloat($(".transferstotal")).val()).toFixed(2));
   
      $(".store_tx").each(function() {
         if (!isNaN(this.value) && this.value.length != 0) {
         store_tx_sum += parseFloat(this.value);
         
          }    
       });
      $(".store_tx_total").text(store_tx_sum.toFixed(2));
      $(".st_total").val(store_tx_sum.toFixed(2));
   
   
       $(".po_types").each(function() {
         if (!isNaN(this.value) && this.value.length != 0) {
         po_types_sum += parseFloat(this.value);
         
          }    
       });
      $(".po_types_total").text(po_types_sum.toFixed(2));
      $(".pot_total").val(po_types_sum.toFixed(2));
      
       var sum = 0;
      $('.ft').each(function() {
        sum += +$(this).val()||0;
      });
      $(".tot_acct").text(sum.toFixed(2));
      $(".safe_os").text(($(".safe_cnt").text() - $(".tot_acct").text()).toFixed(2));
      //$(".total").text(($(".safe_os").text() + $(".groc_os").text()).toFixed(2));
      $(".total").text((parseFloat($(".safe_os").text()) + parseFloat($(".groc_os").text())).toFixed(2));
   
      }
   
   
      
   });
   
      $(document).ready(function() {
   
         $(".mgmtapproval").click(function(){  
            $('.mgmtapprovalmsg').hide();
            var safe_date = '{{ $safe_date }}';
            var nextDate = '{{ $nextDate }}';
            $.post("checkmgmtapproval",{safe_date:safe_date} ,function(data) {
               //alert(data);return false;
               $('.mgmtapprovalmsg').show();
               $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data[1]+"</div>");
               setTimeout(function() { 
                  $('.mgmtapprovalmsg').hide();
                  //alert(data[0]);return false;
                 if(data[0] === 1)
                  {
                     $('.mgmtapprovalmsg').show();
                     vdialog.confirm('Is the store closed on '+nextDate+'', function(){
                        // $.post("checkholiday",{nextDate:nextDate} ,function(data) {
                        //    $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data+"</div>");
                        // });

                        $.ajax({
                         url: "checkholiday",
                          type: "POST", 
                         data: { nextDate: nextDate},
                         //dataType:'json',
                         beforeSend: function () {
                             $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please wait till the Balance is transfered...</div>");
                         },
                         success: function(data) {
                           $('.mgmtapprovalmsg').html("<div class='alert alert-danger alert-message' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data+"</div>");
                           
                         }
                       });
                     });

                  } 
               }, 1800);
            });
         });
         $("#transfers-submit").click(function(){
            var datastring = $("#transfers-form").serializeArray();
            var safe_date = '{{ $safe_date }}';
            $.post("transfersboxsubmit",{data:datastring,safe_date:safe_date} ,function(data) {
               //alert(data);return false;
               cancel_click('transfersbox','dandtbox');
            });
          }); 
   
         $("#deliveries-submit").click(function(){
            var datastring = $("#deliveries-form").serializeArray();
            var safe_date = '{{ $safe_date }}';
            $.post("deliveriesboxsubmit",{data:datastring,safe_date:safe_date} ,function(data) {
               //alert(data);return false;
               cancel_click('deliverybox','dandtbox');
            });
          }); 
   
          $("#depositsbox-submit").click(function(){
            var datastring = $("#depositsbox-form").serializeArray();
            var safe_date = '{{ $safe_date }}';
            $.post("depositsboxsubmit",{data:datastring,safe_date:safe_date} ,function(data) {
               //alert(data);return false;
               cancel_click('depositsbox','safereportbox');
            });
          });  
   
   
         $("#stbox_submit").click(function(){
            //alert('Hii');return false;
            var datastring = $("#stbox-form").serializeArray();
            //alert(datastring);return false;
            var safe_date = '{{ $safe_date }}';
            //alert(safe_date);return false;
            $.post("stboxsubmit",{data:datastring,safe_date:safe_date} ,function(data) {
              //alert(data);return false;
               cancel_click('stbox','safereportbox');
            });
          // $( ".safecountothertotal" ).focus();  
         });
   
         $("#potbox_submit").click(function(){
            var datastring = $("#potbox-form").serializeArray();
            var safe_date = '{{ $safe_date }}';
            $.post("potboxsubmit",{data:datastring,safe_date:safe_date} ,function(data) {
               //alert(data);return false;
               cancel_click('potbox','safereportbox');
            });
          });  
         
      $("input.form-control").keydown(function (e) {
           // Allow: backspace, delete, tab, escape, enter and .
           if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
               (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
               (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
               (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
               (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
           }
           // Ensure that it is a number and stop the keypress
           if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
               e.preventDefault();
           }
       });
      
      });
      
      
   
</script>