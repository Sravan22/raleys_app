@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php 
   //$previewsdate =  date('Y-m-d', strtotime('-1 day'));
?>
<div class="col-md-12">
   <br>
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
   @if(Session::has('alert-' . $msg))
   <div class="alert alert-{{ $msg }}" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      {{ Session::get('alert-' . $msg) }}                               
   </div>
   @endif
   @endforeach
</div>
<div class="container">
   <div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >UTILITY PAYMENT COUNT</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-post-utility-pay-count')}}" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return validateForm()">
               <div class="form-group">
                  <div class="col-md-5">
                     <span class="pull-right"><label for="inputPassword" class="control-label col-sm-12">Date</label></span>
                  </div>
                  <div class="col-sm-7" style="padding-left:27px;">
                  <input type="hidden" name="entry_date" value="{{ $entry_date }}">
                     {{date('m/d/Y',strtotime($entry_date))}}
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-5">
                     <span class="pull-right"><label for="inputPassword" class="control-label col-sm-12">Utility Payment Cnt</label></span>
                  </div>
                  <div class="col-sm-7">
                     <input type="text" class="form-control" id="util_cnt" minlength="1" maxlength="5" style="width:106px;" placeholder="COUNT" name="util_cnt" onKeyPress="return StopNonNumeric(this,event)">
                     @if($errors->has('util_cnt'))
                     {{ $errors->first('util_cnt') }}
                     @endif 
                  </div>
               </div>
               
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center">
                        <input type="submit" name="submit" id="submit" tabindex="4" value="Submit" class="btn">
                        <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{ URL::route('mktmgr-bookutilitypymntcnt')}}'" />
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop

<script type="text/javascript">
   function StopNonNumeric(el, evt)
{
    //var r=e.which?e.which:event.keyCode;
    //return (r>31)&&(r!=46)&&(48>r||r>57)?!1:void 0
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot (thanks ddlab)
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var dotPos = el.value.indexOf(".");
    if( dotPos>-1 && (number[1].length > 3)){
        return false;
    }
    return true;
}

   function validateForm()
   {
      var util_cnt = document.getElementById("util_cnt").value.length;
      //alert(util_cnt);return false;
      if(util_cnt==0)
      {
         alert('Utility Payment Count is Required');
         $('#util_cnt').focus();return false;
      }
   }
   
</script>
