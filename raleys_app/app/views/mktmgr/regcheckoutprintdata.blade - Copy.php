<?php 
   //echo count($reg_array_final1);exit;
   //echo '<pre>',print_r($reg_array_final1);exit();
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
         {{--  <a href="" target="_blank">
         <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
         </a> --}}
         </span>
      </div>
   </div>
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="panel">
         <div class="" style="margin-left: 78px;margin-bottom:-23px;font-weight:bold;">
            <div class="panel-title">{{-- {{  date("m/d/Y", strtotime($txDate)) }} --}}</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Raley's / Bel Air</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Register Check Out Sheet</div>
         </div>
         <div class="col-md-4"></div>
         <div class="col-md-4" style=" text-align:center; font-weight:bold;">
            <div class="panel-title"> Store Number : {{ $str_num }} &nbsp;&nbsp;&nbsp; DATE : {{ date('m/d/Y',strtotime($rpt_date)) }}</div>
         </div>
         <div class="col-md-4"></div>

         @if($reg_array_final1)
            <div style="padding-top:30px" class="panel-body">
               <div class="col-md-3">
                  <table class="table table-striped">
                     <tbody>
                        <tr>
                           <td>Reg No</td>
                        </tr>
                        <?php
                           for($j=0;$j<count($reg_array_final1[0]);$j++)
                           { ?>
                        <tr>
                           <td><?php echo $reg_array_final1[0][$j]['item_desc']; ?> 
                           </td>
                        </tr>
                        <?php  }
                           ?>
                     </tbody>
                  </table>
               </div>
               <?php
                  for($i=0;$i<count($reg_array_final1);$i++)
                  { ?>
               <div class="col-md-1" style="width:72px;margin-left: 0px;padding-left:0px;padding-right:0px;margin-right:0px;">
                  <table class="table table-bordered table-colored">
                     <tbody>
                        <tr>
                           <td>{{ $reg_array_final1[$i][0]['reg_num'] }}</td>
                        </tr>
                        <?php
                           for($j=0;$j<count($reg_array_final1[$i]);$j++)
                           { ?>
                        <tr>
                           <td><?php echo $reg_array_final1[$i][$j]['item_amt']; ?> 
                           </td>
                        </tr>
                        <?php 
                           }
                             ?>
                     </tbody>
                  </table>
               </div>
               <?php
                  if($i>9)
                       {
                         break;
                       }
                    }  
                    ?>
            </div>

            @if($i<count($reg_array_final1))
            <div style="padding-top:30px" class="panel-body">
               <div class="col-md-3">
                  <table class="table table-striped">
                     <tbody>
                        <tr>
                           <td>Reg No</td>
                        </tr>
                        <?php
                           for($j=0;$j<count($reg_array_final1[0]);$j++)
                           { ?>
                        <tr>
                           <td><?php echo $reg_array_final1[0][$j]['item_desc']; ?> 
                           </td>
                        </tr>
                        <?php  }
                           ?>
                     </tbody>
                  </table>
               </div>
               <?php
                  for($i=11;$i<count($reg_array_final1);$i++)
                  { ?>
               <div class="col-md-1" style="width:72px;margin-left: 0px;padding-left:0px;padding-right:0px;margin-right:0px;">
                  <table class="table table-bordered table-colored">
                     <tbody>
                        <tr>
                           <td>{{ $reg_array_final1[$i][0]['reg_num'] }}</td>
                        </tr>
                        <?php
                           for($j=0;$j<count($reg_array_final1[$i]);$j++)
                           { ?>
                        <tr>
                           <td><?php echo $reg_array_final1[$i][$j]['item_amt']; ?> 
                           </td>
                        </tr>
                        <?php 
                           }
                             ?>
                     </tbody>
                  </table>
               </div>
               <?php
                  if($i>20)
                  {
                    break;
                  }
                  }  
                  ?>
            </div>


            <div style="padding-top:30px" class="panel-body">
               <div class="col-md-3">
                  <table class="table table-striped">
                     <tbody>
                        <tr>
                           <td>Reg No</td>
                        </tr>
                        <?php
                           for($j=0;$j<count($reg_array_final1[0]);$j++)
                           { ?>
                        <tr>
                           <td><?php echo $reg_array_final1[0][$j]['item_desc']; ?> 
                           </td>
                        </tr>
                        <?php  }
                           ?>
                     </tbody>
                  </table>
               </div>
               <?php
                  for($i=22;$i<count($reg_array_final1);$i++)
                  { ?>
               <div class="col-md-1" style="width:72px;margin-left: 0px;padding-left:0px;padding-right:0px;margin-right:0px;">
                  <table class="table table-bordered table-colored">
                     <tbody>
                        <tr>
                           <td>{{ $reg_array_final1[$i][0]['reg_num'] }}</td>
                        </tr>
                        <?php
                           for($j=0;$j<count($reg_array_final1[$i]);$j++)
                           { ?>
                        <tr>
                           <td><?php echo $reg_array_final1[$i][$j]['item_amt']; ?> 
                           </td>
                        </tr>
                        <?php 
                           }
                             ?>
                     </tbody>
                  </table>
               </div>
               <?php
                  if($i>30)
                  {
                    break;
                  }
                  }  
                  ?>
            </div>
            @endif
         @endif   
      </div>
   </div>
</div>
@stop