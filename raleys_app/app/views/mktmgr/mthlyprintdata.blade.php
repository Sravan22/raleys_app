
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
   p.big {
    line-height: 250%;
   }
   .dottedUnderline { border-bottom: 1px dotted; }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<?php $wk_date = date("m/d/Y", strtotime($end_date));?>
<div class="container">

   <div id="loginbox" class="mainbox col-sm-12">
      
         <div class="" style=" text-align:center;">
            <div class=" col-md-12" >
             <b style="font-size: 22px">Raley's /  Bel Air </b>
               <span class="pull-right">
               <a href="#" onclick="printPage()" id="romovepdf"><i class="fa fa-print fa-fw iconsize "></i> </a>
               </span></div>
               <div style="margin-right: 30px;font-weight:bold;">
               Monthly Commuter Inventory
               </div>
               <div class="col-md-1">
               &nbsp;
                  
               </div>
             <div class="col-md-5" style="text-align: right;font-weight:bold;">
               Store Number.. <?php echo Session::get('storeid');?>
               </div>
               <div class="col-md-6" style="text-align: left;font-weight:bold;">
               Week-Ending Date..<?php echo $wk_date;?>
             </div>
              
                
            </div>
            
         </div>
         </div>
         <br>
        @if(!empty($results))
         <div class="table-responsive col-md-12">
               <table class="table table-bordered table-colored">
                  <thead>
                     <tr>
                        <th class="text-center" >Inventory Type</th>
                        <th class="text-center" >DESCRIPTION</th>
                        <th class="text-center" >PRICE</th>
                        <th class="text-center" >BEGINNING INVENTORY</th>
                        <th class="text-center" >DELIVERIES</th>
                        <th class="text-center">TRANSFER NUMBER</th>
                        <th class="text-center" >RETURNS</th> 
                        <th class="text-center" >ENDING INVENTORY</th> 
                        <th class="text-center" >SALES</th>   
                     </tr>
                  </thead>
                  <tbody>
                   @foreach($results as $result)
                  <tr>
                  <?php 
                  $final = count($results);

                 static $i = 1;
                 
                  
                  ?>
                     <td  class="text-center" ><?php if($i != $final){
                      echo $result->inv_type;
                      
                     }
                     else
                     {
                        echo '';
                     }
                     ?>
                     </td>
                        <td  class="text-center" ><?php 
                        if($result->price)
                           {
                              echo $result->desc;
                           }
                        
                        else{
                              if($result->inv_type)
                                 { 
                                  if($i != $final)
                                    {
                                    ?> 
                                       <b>Group</b> 
                           <?php    }
                                   else
                                    {
                                    ?>
                                      <b> Grand</b>     
                           <?php    }  
                                 }
                             }   
                             ?>
                        </td>

                     <td  class="text-center" ><?php if($result->price){
                        echo number_format((float)$result->price, 2, '.', '');
                        } else {?>
                                 <b> Total </b> 
                           <?php } ?>
                    </td>

                     <td  class="text-center" >{{number_format((float)$result->begin_inv, 2, '.', '');}}</td>

                     <td  class="text-center" >{{number_format((float)$result->deliveries, 2, '.', '');}}</td>
                     <td class="text-center">
                     <?php if($result->price)
                     {
                     echo $result->trf_no_1;
                     }
                     else
                     {
                        echo '';
                     }
                     ?>
                        
                     </td>

                     <td  class="text-center" >{{number_format((float)$result->returns, 2, '.', '');}}</td>

                     <td  class="text-center" > {{number_format((float)$result->end_inv, 2, '.', '');}}</td>

                     <td>{{number_format((float)$result->sales, 2, '.', '');}}</td>
                     
                     <?php $i++;?>

                  </tr>
                    @endforeach  
                    
                  </tbody>
               </table>
            </div>
            @else
             <div class="invalertdanger" role="alert" ">
  <strong>Warning! </strong>Their is No data for {{$wk_date}} date. <a href="{{ ('bookcommutermonthlyprnt')}}" class="alert-link" style="color: blue;">Go Back </a>for submit another date.
</div>
@endif
         </div>
        
         

@stop         