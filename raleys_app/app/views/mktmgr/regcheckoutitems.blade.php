<?php
   //echo 'resultArray';
   //echo '<pre>';print_r($resultArray);exit;
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style type="text/css">
   .makecenter
   {
   margin-left:20px;
   }
   .row.vertical-divider > div[class^="col-"]
   {
   text-align: left;
   padding: 2px;
   }
</style>
<div class="container" style="">
   <div class="flash-message">
      @if($dayIsLocked == 'Y')
      <p class="alert alert-info">Data is locked for {{ date("m/d/Y", strtotime($dateofinfo)) }}. Any changes won't be saved <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
   </div>
   <div id="loginbox" style="margin-left:10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >REGISTER CHECK OUT</div>
         </div>
         <form action="@if($dayIsLocked != 'Y') {{URL::route('mktmgr-updateregistercheckout')}} @endif" class="form-horizontal" role="form" method="post">
            <div style="padding-top:15px" class="panel-body" >
               <div class="row horizantal-divider">
                  <div class="col-xs-6">
                     <div class="col-md-4 text-right"><b>Date :</b></div>
                     <div class="col-md-8"><b>{{ date('m/d/Y', strtotime($dateofinfo)) }}</b></div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <div class="col-xs-6">
                     <div class="col-md-4 text-right"><b>CATEGORY :</b></div>
                     <div class="col-md-8"><b>{{ $item_name }}</b></div>
                  </div>
                  <div class="col-xs-12">
                     <hr>
                  </div>
                  <div class="row">
                     <div class="col-xs-12" style="margin-left: 15px;margin-top:10px;">
                        <div class="col-sm-2" style="padding-left:50px;"><strong>Rg/Chkr</strong></div>
                        <div class="col-sm-2" style="padding-left:35px;"><strong>Amount</strong></div>
                        <div class="col-sm-2"><strong>Rg/Chkr</strong></div>
                        <div class="col-sm-2"><strong>Amount</strong></div>
                        <div class="col-sm-2"><strong>Rg/Chkr</strong></div>
                        <div class="col-sm-2"><strong>Amount</strong></div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12" style="margin-left: 15px">
                        <div class="col-sm-2" style="padding-left:50px;"><strong>----------</strong></div>
                        <div class="col-sm-2" style="padding-left:35px;"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                        <div class="col-sm-2"><strong>----------</strong></div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12" style="">
                        <div class="row" style="margin-top:25px;">
                           <input type="hidden" name="item_id" value="{{ $item_id }}">
                           <input type="hidden" name="entry_date" value="{{ $dateofinfo }}">
                           <?php $total = 0; ?>
                           @foreach($resultArray as $row)
                           <?php $total=$total+$row['item_amt'];  ?>

                           <input type="hidden" name="reg_num[]" value="{{ $row['reg_num'] }}">
                           <div class="col-md-2 col-xs-6" style="padding-left:97px;"><strong>{{ $row['reg_num'] }}</strong></div>
                           <div class="col-md-2 col-xs-6"  style="margin-right:-30px;padding-left:34px;">
                              
                           <input type="text" class="form-control alltotal" id="<?php echo $row['reg_num']; ?>" name="item_amt[]" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> value="<?php echo number_format((float)$row['item_amt'], 2, '.', ''); ?>" onKeyPress="return StopNonNumeric(this,event)">
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <hr>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="col-md-6 text-right"><b>Total :</b></div>
                        <div class="col-md-6"><strong><span class="totalvalue"><b>{{  number_format((float)$total, 2, '.', '');  }}</b></span></strong></div>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <hr>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        @if($file_locked == 'Y')
                        <div class="col-sm-12 topspace" align="center">
                           <input type="button" value="Go Back" class="btn" id="cancel" name="cancel" onclick="goBack()">
                           {{ Form::token()}}
                        </div>
                        @else
                        <div class="col-sm-12" align="center">
                           <input type="submit" <?php if($dayIsLocked == 'Y'){ echo 'disabled'; } ?> name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                           <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/regcheckoutitem')}}'" />
                           {{ Form::token()}}
                        </div>
                        @endif                
                     </div>
                  </div>
                  @if($file_locked == "Y")
            <div class="row">
               <div class="col-md-1">&nbsp;</div>
               <div class="col-md-10 text-center" style="color:red;">Files are Locked</div>
               <div class="col-md-1">&nbsp;</div>
            </div>
            @endif
                  <br>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<script>
 $(document).ready(function() {
    $(".alltotal").on("click", function () {
      $(this).select();
   });
 });

 /*$(".alltotal").one('click',function(){  
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval*100));
        $(".totalvalue").text(sum.toFixed(2));
       return true;
    });

 $(".alltotal").one('blur',function(){  
        var sum = 0; 
        selfield = this.id;
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
        $('.alltotal').each(function() { 
         sum += parseFloat($(this).val()); 
        });
        $(".totalvalue").val(sum.toFixed(2));
       $(".totalvalue").text(sum.toFixed(2));
       return true;
    });  

*/
$(".alltotal").focus(function(event) {
      var sum = 0; 
      selfield = this.id;
      //this.select();
      // $("#safecoinsubmit").removeAttr('disabled');
      selfieldval=$("#"+selfield).val(); 
      $("#"+selfield).val(parseFloat(selfieldval*100));
      this.select();
      });

 $(".alltotal").blur(function(){  
       var sum = 0; 
        selfield = this.id;
       
        selfieldval=$("#"+selfield).val(); 
        $("#"+selfield).val(parseFloat(selfieldval/100).toFixed(2));
        $('.alltotal').each(function() { 
         sum += parseFloat($(this).val()); 
        });
        $(".totalvalue").val(sum.toFixed(2));
       $(".totalvalue").text(sum.toFixed(2));
      
       return true;
     });


   function goBack() {
       window.history.back();
   }
   function StopNonNumeric(el, evt)
       {
           var charCode = (evt.which) ? evt.which : event.keyCode;
           var number = el.value.split('.');
           if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
               return false;
           }
           //just one dot (thanks ddlab)
           if(number.length>1 && charCode == 46){
                return false;
           }
           //get the carat position
           var dotPos = el.value.indexOf(".");
           if( dotPos>-1 && (number[1].length > 3)){
               return false;
           }
           return true;
       }
       setTimeout(function() { $(".flash-message").hide(); }, 2500);
</script> 
@stop

