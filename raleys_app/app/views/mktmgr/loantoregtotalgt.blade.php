@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<div class="container">
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
    	  <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >GRAND TOTALS</div>
                       </div> 
     <form action="" class="form-horizontal col-md-offset-1" method="post" role="form" style="display: block;padding-top: 15px;">


    <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Register Date</label>
        <div class="col-sm-6">
           <input type="text" class="form-control" id="dateofinfo" name="dateofinfo" readonly="readonly" value="{{ $dateofinfo }}">
            </div>
    </div> 
    <?php $i=1; $gtot =0; ?>
     @foreach($alldata as $row)
        <?php 
              $dbval = '00.00'; 
              $gtot=$gtot+$row['item_amt'];
              $dbval=number_format($row['item_amt'], 2, '.', '');
              $amtid = 'amt'.$i; $itemdescid = 'itemdesc'.$i; $i=$i+1;
        ?>
        <div class="form-group">
            <input type="hidden" id="{{ $itemdescid }}" name="{{ $itemdescid }}" value="{{ $row['allitems'] }}">
            <label for="inputEmail" class="control-label col-sm-6">({{ $row['allitems'] }}) {{ $row['item_desc'] }}</label>
            <?php
            if($row['allitems']== 'GT' or $row['allitems']== 'LR' )
            {?>
            <div class="col-sm-6">
          <input type="number" class="form-control caltosum" id="{{ $amtid }}" name="{{ $amtid }}" value="{{ $dbval }}" readonly="readonly">
            </div>
           </div>
          <?php }
          else
          {?>
        <div class="col-sm-6">
        <input type="number" class="form-control caltosum" id="{{ $amtid }}" name="{{ $amtid }}" value="{{ $dbval }}" readonly="readonly">
            </div>
           </div>

        <?php } ?>
     @endforeach  
     <?php $gtot=number_format($gtot, 2, '.', ''); ?>

     <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Loans To Registers Total</label>
        <div class="col-sm-6" style="padding-left:85px;">
           <b>{{ $gtot }}</b>
            
            </div>
    </div> 

   	</form>
 

    </div>
				</div>
			</div>
		</div>     
     <div class="form-group">
                    <div class="row">
                       <div class="col-sm-12 topspace" align="center">
        <input type="submit" name="login-submit" id="submit" tabindex="4" value="Accept" class="btn"   onclick="goBack()">
       <!-- <input type="button" name="login-submit" id="cancel" tabindex="4" value="Cancel" class="btn"  onclick="goBack()"> -->
                                                {{ Form::token()}}
                      </div>
                    </div>
                  </div>
<script>
function goBack() {
    window.history.back();
}
</script>  
@stop