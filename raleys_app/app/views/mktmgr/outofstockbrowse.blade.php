<?php
  // echo '<pre>';print_r($out_of_stock_result);exit; 
?>
@extends('layout.dashboard')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storeordersmenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6">
         <h3>Browse</h3>
      </div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
         <!-- <i class="fa fa-file-pdf-o fa-fw iconsize"></i> -->
         </span>
      </div>
   </div>
</div>
<div class="container">
@if ($out_of_stock_result)
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Date Scanned</th>
            <th>Scan Type</th>
            <th>GL Dept</th>
            <th>SKU Number</th>
            <th>Description</th>
         </tr>
        </thead> 
        <tbody>
         @for ($i = 0; $i < count($out_of_stock_result); $i++)   
         <tr>
            <td>{{ $out_of_stock_result[0]->create_date }}</td>
            <td>{{ $out_of_stock_result[0]->audit_type }}</td>
            <td>{{ $out_of_stock_result[0]->gl_dept_number }}</td>
            <td>{{ $out_of_stock_result[0]->sku_number }}</td>
            <td>{{ $out_of_stock_result[0]->description }}</td>
         </tr>
        @endfor

        @else
            <div class="alert alert-danger">
              <strong>Alert!</strong> No Store Transfers meet Query criteria.
            </div>
        @endif
               
      </tbody>
   </table>
</div>
@stop