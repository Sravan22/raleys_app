@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
        @include('mktmgr.bookkepermenu')
    </header>
<div class="container">
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
    	  <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                        <div class="panel-title" >LOAN REPORT</div>
                       </div> 
     <form action="{{URL::route('mktmgr-savebookloanrregister')}}" class="form-horizontal" method="post" role="form" style="display: block;">

    <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Register Nummber</label>
        <div class="col-sm-6">
            <input type="number" class="form-control" id="reg_num" name="reg_num" 
                   value={{ $reg_num }} readonly="readonly" step="0.01">
            </div>
    </div>

    <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Register Date</label>
        <div class="col-sm-6">
           <input type="date" class="form-control" id="dateofinfo" name="dateofinfo" 
                   value={{ $dateofinfo }} readonly="readonly">
            </div>
    </div> 
    <?php $i=1 ?>
     @foreach($item_id as $key => $value)
        @if($value->item_id != 'GT')
        <?php $val = "val".$i; $idval = "id".$i; ?>
	    <div class="form-group">
            <input type="hidden" id="{{ $idval }}" name="{{ $idval }}" value="{{ $value->item_id }}">
            <label for="inputEmail" class="control-label col-sm-6">({{ $value->item_id }}) {{ $value->item_desc}}</label>
        <div class="col-sm-6">
            <input type="number" class="form-control setnumtot" id="{{ $val }}" name="{{ $val }}" step="0.01">
        </div>
        </div>
        <?php $i=$i+1; ?>
        @endif	
     @endforeach  

     <div class="form-group">
            <label for="inputEmail" class="control-label col-sm-6">Loans To Registers Total</label>
        <div class="col-sm-6">
            <input type="number" class="form-control" id="gt" name="gt" readonly="readonly" step="0.01">
            <input type="hidden" class="form-control" id="tot" name="tot" value="{{ count($item_id)}}">
            </div>
    </div> 

    @if($dayIsLocked != 'Y')
    <div class="form-group">
	  <div class="row">
											 <div class="col-sm-12" align="center">
				<input type="submit" name="login-submit" id="submit" tabindex="4" value="Submit" class="btn">
                <!-- <input type="submit" name="login-submit" id="submit" tabindex="4" value="Cancel" class="btn"> -->
        {{ Form::token()}}
											</div>
										</div>
									</div>
		@endif							
								</form>
								
							
					</div>
				</div>
			</div>
		</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script type="text/javascript">
 $(".setnumtot").blur(function(){ var sum = 0;
    amt=$("#"+this.id).val();
    $("#"+this.id).val(parseFloat(amt).toFixed(2));
    allsum=sumall();
    $("#gt").val(parseFloat(allsum,10).toFixed(2));
 });

 function sumall()
 {
    sumallval = 0;
    tot=$("#tot").val();
    for(i=1; i< tot; i++)
    {   
        selval=$("#val"+i).val();
        if(selval != '') { sumallval = sumallval+parseInt($("#val"+i).val()); }
    }
    return sumallval;
       /* return tot=parseInt(misc)+parseInt(pennies)+parseInt(nickles)+parseInt(dimes)+parseInt(quarters)+parseInt(twenties)
         +parseInt(tens)+parseInt(fives)+parseInt(ones);*/
}
 </script>       
@stop