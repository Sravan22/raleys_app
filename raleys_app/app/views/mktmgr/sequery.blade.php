@extends('layout.dashboard')
@section('page_heading','Receivings')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.recivermenu')
</header>
<div class="container">
   <div class="flash-message " style="padding-bottom: 20px;">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
   </div>
   <!-- end .flash-message -->
   <div id="loginbox" style="margin-top:-20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >Store Expenses </div>
         </div>
         <div style="padding-top:10px" class="panel-body" >
            <form action="{{URL::route('mktmgr-search-receivings-sequery')}}" class="form-horizontal" method="post" role="form" style="display: block;">
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Store</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="store" placeholder="Store" readonly ="" name="store" value="305">
                     @if($errors->has('store'))
                     {{ $errors->first('store')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Vendor</label>
                  <div class="col-sm-8">
                     <select id="vendor" name="vendor" class="form-control" value="vendor">
                        <option value="">Select</option>
                        @for ($i = 0; $i < count($vendor_details); $i++)
                        <option value="{{ $vendor_details[$i]['vendor_number'] }}">{{ $vendor_details[$i]['name'] }}</option>
                        @endfor
                     </select>
                     @if($errors->has('vendor'))
                     {{ $errors->first('vendor')}}
                     @endif
                  </div>
               </div>
               <div class="focusguard" id="focusguard-1" tabindex="1"></div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Vendor Number</label>
                  <div class="col-sm-8">
                     <input type="text"  class="form-control" name="vendor"{{ (Input::old('vendornum'))?' 
                     value="'.Input::old('vendornum').'"':''}} id="vendornum" placeholder="Vendor Number" autofocus=""  tabindex="2" readonly="" />
                     @if($errors->has('vendornum'))
                     {{ $errors->first('vendornum')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Invoice Date</label>
                  <div class="col-sm-8">
                     <input type="date" class="form-control" id="invdate" placeholder="MM-DD-YY" name="invdate" tabindex="3" />
                     @if($errors->has('invdate'))
                     {{ $errors->first('invdate')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Invoice Number</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="invnum" placeholder="Invoice Number" name="invnum" tabindex="4" />
                     @if($errors->has('invnum'))
                     {{ $errors->first('invnum')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Expense Type</label>
                  <div class="col-sm-8">
                     <select id="expensetype" name="expensetype" class="form-control" value="expensetype" tabindex="5">
                        <option value="">Select</option>
                        <option value="G">G-General Repair</option>
                        <option value="S">S-Supply</option>
                        <option value="V">V-Service</option>
                     </select>
                     @if($errors->has('expensetype'))
                     {{ $errors->first('expensetype')}}
                     @endif
                  </div>
                  {{--             
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="expensetype" placeholder="Expense Type" name="expensetype">
                     @if($errors->has('expensetype'))
                     {{ $errors->first('expensetype')}}
                     @endif
                  </div>
                  --}}
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Amount</label>
                  <div class="col-sm-8">
                     <input type="text" disabled="" class="form-control" id="amount" placeholder="Amount" name="amount">
                     @if($errors->has('amount'))
                     {{ $errors->first('amount')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Status</label>
                  <div class="col-sm-8">
                     <select class="form-control" id="status" name="status" placeholder="Status" tabindex="6" >
                        <option value="">Select Status</option>
                        {{-- 
                        <option value="A">A - Accepted</option>
                        --}}
                        {{-- 
                        <option value="B">B</option>
                        --}}
                        {{-- 
                        <option value="I">I - Incompleted</option>
                        --}}
                        <option value="O">O - Open</option>
                        {{-- 
                        <option value="P">P</option>
                        --}}
                        {{--  
                        <option value="R">R - Reviewed</option>
                        <option value="U">U - Unknown</option>
                        --}}
                        <option value="V">V - Voided</option>
                     </select>
                  </div>
                  @if($errors->has('status'))
                  {{ $errors->first('status')}}
                  @endif
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Created</label>
                  <div class="col-sm-8">
                     <input type="text" disabled="" class="form-control" id="created" placeholder="Created" name="created">
                     @if($errors->has('created'))
                     {{ $errors->first('created')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">Last Updated</label>
                  <div class="col-sm-8">
                     <input type="text" disabled="" class="form-control" id="lastupdated" placeholder="Last Updated" name="lastupdated">
                     @if($errors->has('lastupdated'))
                     {{ $errors->first('lastupdated')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <label for="inputPassword" class="control-label col-sm-4">By</label>
                  <div class="col-sm-8">
                     <input type="text" disabled="" class="form-control" id="by" placeholder="by" name="by">
                     @if($errors->has('by'))
                     {{ $errors->first('by')}}
                     @endif
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" style="margin-left: 265px;">
                        <input type="submit" tabindex="7" value="Search" class="btn"/>
                        <input type="Reset" id ="reset" tabindex="8" value="Reset" class="btn" />
                        <!--   <input type="button" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/returnhome')}}'" /> -->
                        {{ Form::token()}}
                     </div>
                  </div>
               </div>
               <div class="focusguard" id="focusguard-2" tabindex="9"></div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
@stop
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){ 


        $('#focusguard-2').on('focus', function() {
        $('#vendor').focus();
        });

        $('#focusguard-1').on('focus', function() {
        $('#reset').focus();
        });
   
      $('#invnum').blur(function() {
         invnum = $("#invnum").val();
         vendornum = $("#vendornum").val();
         if (invnum != '') {
             $.post("check_inv_number", {
                 invnum: invnum,
                 vendornum: vendornum,
             }, function(result) { 
                  $("#msg").html(result);
             });
         }
       });
      $(document).keypress(function (e) {
   
            if (e.which == 13 && e.target.tagName != 'TEXTAREA') {
              
              var txt = $(e.target);
             var allOther= $("input[type=text]:not([disabled])");
              var index = jQuery.inArray(txt[0], allOther);
             var next= $(allOther[index+1]);
              if(next) next.focus();
              debugger;
              //Need to set focus to next active text field here.
                return false;
            }
        });
   
   $('#vendor').on('change', function(e) {
   vendor = $("#vendor").val();
   $("#vendornum").val(vendor);
   });
   
   $("#amount").blur(function(){
       
         amount=parseFloat($("#amount").val(),10).toFixed(2);
       $("#amount").val(amount);
        $("#amount").val(amount);
        calnet(); return true;
     });
   });
   
   $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
   
            $(function(){
                            var dtToday = new Date();
                            
                            var month = dtToday.getMonth() + 1;
                            var day = dtToday.getDate();
                            var year = dtToday.getFullYear();
                            if(month < 10)
                                month = '0' + month.toString();
                            if(day < 10)
                                day = '0' + day.toString();
                            
                            var maxDate = year + '-' + month + '-' + day;
                            $('#invdate').attr('max', maxDate);
                        });
   
   function calnet() {
        amount=$("#amount").val(); 
       
       }
   
       
</script>