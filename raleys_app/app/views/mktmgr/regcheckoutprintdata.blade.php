<?php 
   //echo count($reg_array_final1);exit;
   //echo '<pre>',print_r($reg_array_final1);exit();
   ?>
@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
         {{--  <a href="" target="_blank">
         <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
         </a> --}}
         </span>
      </div>
   </div>
</div>
<div class="container">
   <div id="loginbox" class="mainbox col-sm-12">
      <div class="panel">
         <div class="" style="margin-left: 78px;margin-bottom:-23px;font-weight:bold;">
            <div class="panel-title">{{-- {{  date("m/d/Y", strtotime($txDate)) }} --}}</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Raley's / Bel Air</div>
         </div>
         <div class="" style=" text-align:center; font-weight:bold;">
            <div class="panel-title">Register Check Out Sheet</div>
         </div>
         <div class="col-md-4"></div>
         <div class="col-md-4" style=" text-align:center; font-weight:bold;">
            <div class="panel-title"> Store Number : {{ $str_num }} &nbsp;&nbsp;&nbsp; DATE : {{ date('m/d/Y',strtotime($rpt_date)) }}</div>
         </div>
         <div class="col-md-4"></div>
         @if($reg_array_final1)
            @if($reg_array_final1[0])
         <div style="padding-top:30px" class="panel-body">
            <table class="table table-bordered table-colored" style="width: 0%">
               <thead>
                  <th>Registration No</th>
                  <?php for($i=0;$i<count($reg_curs);$i++) { ?>
                  <th class="text-right"><?php echo $reg_curs[$i]['reg_num']; ?></th>
                  <?php if($i>9)
                     {
                        break;
                     }
                     }
                     
                     ?>
               </thead>
               <tbody>
                  <?php  for($i=0;$i<count($reg_array_final1);$i++)  { ?>
                  @if($reg_array_final1[$i][0]['item_type'] == "C")  
                  <tr>
                     <td><?php echo $reg_array_final1[$i][0]['item_desc']; ?></td>
                     <?php for($j=0;$j<count($reg_array_final1[$i]);$j++)  {  ?>
                     <td class="text-right">{{ number_format((float)$reg_array_final1[$i][$j]['item_amt'], 2, '.', ''); }}</td>
                     <?php if($j>9)
                        {
                           break;
                        } } ?>
                  </tr>
                  @endif
                  <?php } ?>
                  <tr>
                     <td><strong>Total Credit</strong></td>
                     <?php for($i=0;$i<count($item_type_C_array_final);$i++) { ?>
                     <td class="text-right"><strong><?php echo $item_type_C_array_final[$i]['item_type_C']; ?></strong></td>
                     <?php if($i>9)
                        {
                           break;
                        } } ?>
                  </tr>
                  <?php for($i=0;$i<count($reg_array_final1);$i++) { ?>
                  @if($reg_array_final1[$i][0]['item_type'] == "D")  
                  <tr>
                     <td><?php echo $reg_array_final1[$i][0]['item_desc']; ?></td>
                     <?php for($j=0;$j<count($reg_array_final1[$i]);$j++)  { ?>
                     <td class="text-right">{{ number_format((float)$reg_array_final1[$i][$j]['item_amt'], 2, '.', ''); }}</td>
                     <?php if($j>9)
                        {
                           break;
                        } }  ?>
                  </tr>
                  @endif
                  <?php } ?>
                  <tr>
                     <td><strong>Total Debit</strong></td>
                     <?php for($i=0;$i<count($item_type_D_array_final);$i++) { ?>
                     <td class="text-right"><strong><?php echo $item_type_D_array_final[$i]['item_type_D']; ?></strong></td>
                     <?php if($i>9)
                        {
                           break;
                        } } ?>
                  </tr>
                  <tr>
                     <td><strong>Total Over <span><</span>Short<span>></span></strong></td>
                     <?php for($i=0;$i<count($total_over_short_final);$i++) { ?>
                     <td class="text-right"><strong>{{ number_format((float)$total_over_short_final[$i]['total_over_short'], 2, '.', ''); }}</strong></td>
                     <?php if($i>9)
                        {
                           break;
                        } } ?>
                  </tr>
               </tbody>
            </table>
            @if($i<count($reg_array_final1[0]))
            <table class="table table-bordered table-colored" style="width: 0%">
               <thead>
                  <th>Registration No</th>
                  <?php for($i=11;$i<count($reg_curs);$i++) { ?>
                  <th class="text-right"><?php echo $reg_curs[$i]['reg_num']; ?></th>
                  <?php if($i>22)
                     {
                        break;
                     }
                     }
                     
                     ?>
               </thead>
               <tbody>
                  <?php  for($i=11;$i<count($reg_array_final1);$i++)  { ?>
                  @if($reg_array_final1[$i][0]['item_type'] == "C")  
                  <tr>
                     <td><?php echo $reg_array_final1[$i][0]['item_desc']; ?></td>
                     <?php for($j=11;$j<count($reg_array_final1[$i]);$j++)  {  ?>
                     <td class="text-right">{{ number_format((float)$reg_array_final1[$i][$j]['item_amt'], 2, '.', ''); }}</td>
                     <?php if($j>22)
                        {
                           break;
                        } } ?>
                  </tr>
                  @endif
                  <?php } ?>
                  <tr>
                     <td><strong>Total Credit</strong></td>
                     <?php for($i=11;$i<count($item_type_C_array_final);$i++) { ?>
                     <td><strong><?php echo $item_type_C_array_final[$i]['item_type_C']; ?></strong></td>
                     <?php if($i>22)
                        {
                           break;
                        } } ?>
                  </tr>
                  <?php for($i=11;$i<count($reg_array_final1);$i++) { ?>
                  @if($reg_array_final1[$i][0]['item_type'] == "D")  
                  <tr>
                     <td><?php echo $reg_array_final1[$i][0]['item_desc']; ?></td>
                     <?php for($j=11;$j<count($reg_array_final1[$i]);$j++)  { ?>
                     <td class="text-right">{{ number_format((float)$reg_array_final1[$i][$j]['item_amt'], 2, '.', ''); }}</td>
                     <?php if($j>22)
                        {
                           break;
                        } }  ?>
                  </tr>
                  @endif
                  <?php } ?>
                  <tr>
                     <td><strong>Total Debit</strong></td>
                     <?php for($i=11;$i<count($item_type_D_array_final);$i++) { ?>
                     <td><strong><?php echo $item_type_D_array_final[$i]['item_type_D']; ?></strong></td>
                     <?php if($i>22)
                        {
                           break;
                        } } ?>
                  </tr>
                  <tr>
                     <td><strong>Total Over <span><</span>Short<span>></span></strong></td>
                     <?php for($i=11;$i<count($total_over_short_final);$i++) { ?>
                     <td class="text-right"><strong>{{ number_format((float)$total_over_short_final[$i]['total_over_short'], 2, '.', ''); }}</strong></td>
                     <?php if($i>22)
                        {
                           break;
                        } } ?>
                  </tr>
               </tbody>
            </table>
            @endif
         </div>
            @endif 
         @endif   
         <?php 
            //echo '<pre>';print_r($rpt_grandtot_curs); //exit;
            ?>
         @if($rpt_grandtot_curs)
         <div style="padding-top:30px" class="panel-body">
            <table class="table table-bordered table-colored" style="width: 0%">
               <thead>
                  <th class="text-left">Item Description</th>
                  <th class="text-right">Grand Total</th>
               </thead>
               <tbody>
                  <?php  for($i=0;$i<count($rpt_grandtot_curs);$i++)  { ?>
                  @if($rpt_grandtot_curs[$i]['item_type'] == "C")  
                  <tr>
                     <td><?php echo $rpt_grandtot_curs[$i]['item_desc']; ?></td>
                     <td class="text-right">{{ number_format((float)$rpt_grandtot_curs[$i]['item_amt'], 2, '.', ''); }}</td>
                  </tr>
                  @endif
                  <?php } ?>
                  <tr>
                     <td><strong>Total Credit</strong></td>
                     <td class="text-right"><strong>{{ number_format((float)$grand_total_total_credit_C, 2, '.', ''); }}</strong></td>
                  </tr>
                  <?php  for($i=0;$i<count($rpt_grandtot_curs);$i++)  { ?>
                  @if($rpt_grandtot_curs[$i]['item_type'] == "D")  
                  <tr>
                     <td><?php echo $rpt_grandtot_curs[$i]['item_desc']; ?></td>
                     <td class="text-right">{{ number_format((float)$rpt_grandtot_curs[$i]['item_amt'], 2, '.', ''); }}</td>
                  </tr>
                  @endif
                  <?php } ?>
                  <tr>
                     <td><strong>Total Debit</strong></td>
                     <td class="text-right"><strong>{{ number_format((float)$grand_total_total_credit_D, 2, '.', ''); }}</strong></td>
                  </tr>
                  <tr>
                     <td><strong>Total Over <span><</span>Short<span>></span></strong></td>
                     <td class="text-right"><strong>{{ number_format((float)($grand_total_total_credit_D-$grand_total_total_credit_C), 2, '.', ''); }}</strong></td>
                  </tr>
                  <tr>
                     <td><strong>Grocery Over <span><</span>Short<span>></span></strong></td>
                     <td class="text-right"><strong>{{ number_format((float)($go_ovshrt), 2, '.', ''); }}</strong></td>
                  </tr>
                  <tr>
                     <td><strong>Drug Over <span><</span>Short<span>></span></strong></td>
                     <td class="text-right"><strong>{{ number_format((float)($do_ovshrt), 2, '.', ''); }}</strong></td>
                  </tr>
               </tbody>
            </table>
         </div>
         @endif
      </div>
   </div>
</div>
@stop