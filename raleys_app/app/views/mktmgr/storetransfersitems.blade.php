<?php
   // echo count($StoreItemArray)."<br>";
   // echo $StoreItemArray[0]['item_desc'];
   // echo '<pre>';print_r($item_upper_part_details)."<br>";

   // echo  $item_upper_part_details['transfer_number'];
   // exit;
   // exit;
   ?>
  
@extends('layout.dashboardstoretransfer')
@section('page_heading','Store Transfers')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.storetransfersmenu')
</header>
<div class="container">
  
      
<table align="center" class="table " id="example" style="width: 300px; margin-left: 39%; margin-top: 2%; ">
   <tr>
      <th>Store Transfer Number</th>
      <td>{{ $item_upper_part_details['from_store_no'] }} - {{ $item_upper_part_details['transfer_number']  }} </td>
   </tr>
   <tr>
      <th>From Store/Dept</th>
      <td>{{ $item_upper_part_details['from_store_no'] }} / {{ sprintf("%02d", $item_upper_part_details['from_dept_no']); }}</td>
   </tr>
   <tr>
      <th>To Store/Dept</th>
      <td>{{ $item_upper_part_details['to_store_no'] }} / {{ sprintf("%02d", $item_upper_part_details['to_dept_no']); }}</td>
   </tr>
   <tr>
      <th>From Account</th>
      <td>{{ $item_upper_part_details['from_account'] }}  {{  $item_upper_part_details['from_account_desc']  }}</td>
   </tr>
   <tr>
      <th>To Account</th>
      <td>{{ $item_upper_part_details['to_account']  }}  {{ $item_upper_part_details['to_account_desc'] }}</td>
   </tr>
   <tr>
      <th>TOTAL AMOUNT</th>
      <td>${{ $item_upper_part_details['tot_value'] }}</td>
   </tr>
</table>

<div class="row">
   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>UPC Number</th>
            <th>SKU Number</th>
            <th>Description</th>
            <th>Qty</th>
            <th>Units</th>
            <th>Cost</th>
            <th>Ext Cost</th>
         </tr>
      </thead>
      <tbody>
         @for ($i = 0; $i < count($StoreItemArray); $i++)
         <tr>
            <td>{{ $StoreItemArray[$i]['upc_number'] }}</td>
            <td>{{ $StoreItemArray[$i]['item_number'] }}</td>
            <td>{{ $StoreItemArray[$i]['item_desc'] }}</td>
            <td>{{ $StoreItemArray[$i]['quantity'] }}</td>
            

            @if($StoreItemArray[$i]['qty_units']=="C")
            <td>CS</td>
            <td>${{ number_format($StoreItemArray[$i]['upc_cost'] * $StoreItemArray[$i]['case_pack'],2) }}</td>
            <td>${{ number_format(($StoreItemArray[$i]['upc_cost'] * $StoreItemArray[$i]['case_pack']) * $StoreItemArray[$i]['quantity'] ,2)}}</td>
            @endif


            
           
            @if($StoreItemArray[$i]['qty_units']=="E")
            <td>EA</td>
                @if($StoreItemArray[$i]['discount_rate']==0)
                    <?php $e_cost = $StoreItemArray[$i]['upc_cost']; ?>
                    <td>${{ number_format($StoreItemArray[$i]['upc_cost'],2) }}</td>
                @else
                <?php $e_cost =  $StoreItemArray[$i]['rtl_amt'] * (100 - $StoreItemArray[$i]['discount_rate']) / 100; ?>
                 <td>${{ number_format($e_cost,2) }}</td>
                @endif    
                <td>${{ number_format($e_cost * $StoreItemArray[$i]['quantity'],2) }}</td>
            @endif

           
             @if($StoreItemArray[$i]['qty_units']=="R")
            <td>RT</td>
                @if($StoreItemArray[$i]['discount_rate']==0)
                <?php $r_cost =  ($StoreItemArray[$i]['rw_rtl_amt'] /  $StoreItemArray[$i]['rtl_amt'])* $StoreItemArray[$i]['upc_cost']; ?>
                    <td> ${{ number_format($r_cost,2) }}</td>
                @else
                <?php $r_cost =  ($StoreItemArray[$i]['rw_rtl_amt'] /  $StoreItemArray[$i]['rtl_amt']) * ($StoreItemArray[$i]['rtl_amt']) * (100 - $StoreItemArray[$i]['discount_rate']) / 100; ?>
                 <td>${{ number_format($r_cost,2) }}</td>
                @endif    
                <td>${{ number_format($r_cost * $StoreItemArray[$i]['quantity'],2) }}</td>
            @endif

           



            @if($StoreItemArray[$i]['qty_units']=="M")
            <td>MS</td>
            <td>${{ $StoreItemArray[$i]['upc_cost'] }}</td>
            <td>${{ number_format($StoreItemArray[$i]['upc_cost']  *  $StoreItemArray[$i]['quantity'],2) }} </td>   
            @endif            


         </tr>
         @endfor
      </tbody>
   </table>
   <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
    <input type="button" class="btn" name="" onclick="history.go(-1);" value="Back" autofocus="">        
 
   {{ Form::token()}}
        </div>
        </div>
  </div>
</div>
@stop




