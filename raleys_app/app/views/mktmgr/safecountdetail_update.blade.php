
<p style="display:none;"> Safe Date is : {{ $currency_data[0]->safe_date; }}</p>
<?php
   $coin_total = $coin_data[0]->pennies_box + $coin_data[0]->pennies_roll + $coin_data[0]->nickles_box + $coin_data[0]->nickles_roll + $coin_data[0]->dimes_box  + $coin_data[0]->dimes_roll + $coin_data[0]->quarters_box + $coin_data[0]->quarters_roll + $coin_data[0]->misc_box + $coin_data[0]->misc_roll;
   

   $currency_total = $currency_data[0]->ones_box + $currency_data[0]->ones_roll + $currency_data[0]->ones_held + $currency_data[0]->fives_box + $currency_data[0]->fives_roll + $currency_data[0]->fives_held + $currency_data[0]->tens_box + $currency_data[0]->tens_roll + $currency_data[0]->tens_held + $currency_data[0]->twenties_box
   + $currency_data[0]->twenties_roll + $currency_data[0]->twenties_held + $currency_data[0]->misc1_box + $currency_data[0]->misc1_roll + $currency_data[0]->misc1_held + $currency_data[0]->misc2_box + $currency_data[0]->misc2_roll + $currency_data[0]->misc2_held;

   //echo $currency_total;exit;
   $other_total = $other_data[0]->food_stamps + $other_data[0]->merch_fives + $other_data[0]->merch_tens + $other_data[0]->merch_twenties + $other_data[0]->merch_misc + $other_data[0]->script1 + $other_data[0]->script2 + $other_data[0]->script3 + $other_data[0]->script4 + $other_data[0]->script5 + $other_data[0]->script6 + $other_data[0]->script7 
   + $other_data[0]->script8 + $other_data[0]->script9 + $other_data[0]->script10 + $other_data[0]->begin_loans + $other_data[0]->misc1_amt + $other_data[0]->misc2_amt + $other_data[0]->instr_chrg + $other_data[0]->pay_adv + $other_data[0]->night_crew + $other_data[0]->debit + $other_data[0]->credit + $other_data[0]->ebt + $other_data[0]->giftcard + $other_data[0]->misc3_amt;
   //echo $other_total;exit;
?>


@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<div class="container">
   <div id="safecountbox"  class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-info" >
         <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
            <div class="panel-title" >SAFE COUNT</div>
         </div>
         <div style="padding-top:30px" class="panel-body" >
            <form action="{{URL::route('mktmgr-postsafecountdetailupdate')}}" id="coin-form" class="form-horizontal" method="post" role="form" style="display: block;" onsubmit="return checkamounts()">
               <input type="hidden" name="safe_date" id="dateinfo"  value="{{ $infodate }}" /> 
               <div class="form-group">
               
               <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Coin Total</label>
                  <div class="col-sm-4">
                     <input  type="button"  onclick="hide_show_toggle_safecnt('coinbox','safecountbox')"  class="form-control" id="cointot"  name="cointot" value="{{ number_format($coin_total,2); }}">
                     @if($errors->has('cointot'))
                     {{ $errors->first('cointot')}}
                     @endif
                  </div>
                
               </div>
               <div class="form-group">
               <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Currency Total</label>
                  
               
                  <div class="col-sm-4">
                     <input type="button" class="form-control" id="currency_GrandTot1" onclick="hide_show_toggle_safecnt('currencybox','safecountbox')"  name="currencytot" value="{{ number_format($currency_total,2); }}">
                     @if($errors->has('currencytot'))
                     {{ $errors->first('currencytot')}}
                     @endif
                  </div>
                
               </div>
               <div class="form-group">
               <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-4">Other Total</label>
                  
                  
                  <div class="col-sm-4">
                     <input  class="form-control grandtotal" type="button" onclick="hide_show_toggle_safecnt('miscbox','safecountbox')" id="otherpricetot"  name="otherpricetot" value="{{ number_format($other_total,2); }}">
                     @if($errors->has('otherpricetot'))
                     {{ $errors->first('otherpricetot')}}
                     @endif
                  </div>
                
               </div>
               <hr style="border:1px solid #000;" />
               <div class="form-group">
               <!-- <div class="col-sm-3">&nbsp;</div> -->
               <div class="col-sm-2">&nbsp;</div>
                  <label for="inputPassword" class="control-label col-sm-3">Grand Total</label>
                  <div class="col-sm-6">
                     <input  type="text" class="form-control ftotal greatgranttotal"  readonly="readonly" style="width: 110px;" placeholder="0.00" id="GrandTot"  name="GrandTot" value="{{ number_format($coin_total + $currency_total + $other_total,2); }}">
                     @if($errors->has('otherpricetot'))
                     {{ $errors->first('otherpricetot')}}
                     @endif
                  </div>
                  <div class="col-sm-3" id="errorDiv" style="padding-left:0px;color:red"></div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-sm-12" align="center" style="margin-left:-14px;">
                        <input type="submit" name="safe-submit" id="submit" tabindex="4" value="Update" class="btn">
                        <input type="button" name="" id="submit" tabindex="4" value="Cancel" class="btn" onClick="document.location.href='{{URL::to('mktmgr/booksafecnt')}}'">
                     </div>
                  </div>
               </div>
               <!-- </form> -->
         </div>
      </div>
   </div>
   <!------------------- Coinbox popup---------------------------->      
   <div id="coinbox"  class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info" >
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >COIN</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">BOXED</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">ROLLED</label>
            </div>
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Pennies</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" step="0.01" placeholder="0.00"  class="form-control ft coin width_hundred" id="pennies_box"  name="pennies_box" onblur="sum_of_qty_price(this.value,'pennies')" value="{{  number_format((float)$coin_data[0]->pennies_box, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" step="0.01" placeholder="0.00"  class="form-control ft coin width_hundred" id="pennies_roll"  name="pennies_roll" onblur="sum_of_qty_price(this.value,'pennies')" value="{{ number_format((float)$coin_data[0]->pennies_roll, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly=""  class="form-control width_hundred" id="pennies_tot"  name="pennies_tot" value="{{ number_format((float)$coin_data[0]->pennies_box + $coin_data[0]->pennies_roll, 2, '.', ''); }}">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Nickles</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin width_hundred" id="nickle_box"  name="nickles_box" onblur="sum_of_qty_price(this.value,'nickle')" value="{{  number_format((float)$coin_data[0]->nickles_box, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin width_hundred" id="nickle_roll"  name="nickles_roll" onblur="sum_of_qty_price(this.value,'nickle')" value="{{  number_format((float)$coin_data[0]->nickles_roll, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control width_hundred" id="nickle_tot"  name="nickle_tot" value="{{ number_format((float)$coin_data[0]->nickles_box + $coin_data[0]->nickles_roll, 2, '.', ''); }}">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Dimes</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin width_hundred" id="dimes_box"  name="dimes_box" onblur="sum_of_qty_price(this.value,'dimes')" value="{{  number_format((float)$coin_data[0]->dimes_box, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin width_hundred" id="dimes_roll"  name="dimes_roll" onblur="sum_of_qty_price(this.value,'dimes')" value="{{  number_format((float)$coin_data[0]->dimes_roll, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control width_hundred" id="dimes_tot"  name="dimes_tot" value="{{ number_format((float)$coin_data[0]->dimes_box + $coin_data[0]->dimes_roll, 2, '.', ''); }}">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label col-sm-3">Quarters</label>
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin width_hundred" id="quarter_box"  name="quarters_box" onblur="sum_of_qty_price(this.value,'quarter')" value="{{  number_format((float)$coin_data[0]->quarters_box, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin width_hundred" id="quarter_roll"  name="quarters_roll" onblur="sum_of_qty_price(this.value,'quarter')" value="{{  number_format((float)$coin_data[0]->quarters_roll, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control width_hundred" id="quarter_tot"  name="quarter_tot" value="{{ number_format((float)$coin_data[0]->quarters_box + $coin_data[0]->quarters_roll, 2, '.', ''); }}">
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-3">
               <label for="inputPassword" class="control-label">Misc</label>&nbsp;&nbsp;<input type="text" style="width:60px" class="form-control misc_desc" placeholder="0.00" name="misc_desc" value="{{  $coin_data[0]->misc_desc }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin width_hundred" id="misc_box"  name="misc_box" onblur="sum_of_qty_price(this.value,'misc')" value="{{  number_format((float)$coin_data[0]->misc_box, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00"  class="form-control ft coin width_hundred" id="misc_roll"  name="misc_roll" onblur="sum_of_qty_price(this.value,'misc')" value="{{  number_format((float)$coin_data[0]->misc_roll, 2, '.', ''); }}">
            </div>
            <div class="col-sm-3">
               <input  type="number" placeholder="0.00" readonly="" class="form-control width_hundred" id="misc_tot"  name="misc_tot" value="{{ number_format((float)$coin_data[0]->misc_box + $coin_data[0]->misc_roll, 2, '.', ''); }}">
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-3">Grand Total</label>
            <div class="col-sm-6">
               <input  class="form-control"  readonly="readonly" placeholder="0.00" id="Coin_GrandTot"  name="Coin_GrandTot" value="{{ number_format((float)$coin_total, 2, '.', ''); }}">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" name="" id="coin_submit" tabindex="4" value="Submit" class="btn">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('coinbox','safecountbox')">
                  {{ Form::token()}}
               </div>
            </div>
         </div>
         <!--   </form> -->
      </div>
   </div>
</div>
   <!------------------End of Coinbox popup---------------------->
   <!------------------- currency popup---------------------------->      
   <div id="currencybox" style="margin-left: 87px;" class="mainbox col-md-10 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title">CURRENCY</div>
      </div>
      <div style="padding-top:30px" class="panel-body">
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-3">DESCRIPTION</label>
         </div>
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-3">LEFT</label>
         </div>
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-3">RIGHT</label>
         </div>
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-3">CASH HELD</label>
         </div>
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
         </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-12">Ones</label>
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyone ft width_onetwenty" id="ones_left"  name="ones_box"  onblur="sum_of_qty_price_currency(this.value,'ones_left')" value="{{ number_format((float)$currency_data[0]->ones_box, 2, '.', '');  }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyone ft width_onetwenty" id="ones_right"  name="ones_roll"  onblur="sum_of_qty_price_currency(this.value,'ones_right')" value="{{ number_format((float)$currency_data[0]->ones_roll, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyone ft width_onetwenty" id="ones_cashheld"  name="ones_held" onblur="sum_of_qty_price_currency(this.value,'ones_cashheld')" value="{{ number_format((float)$currency_data[0]->ones_held, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly="" class="form-control currencyonetotal width_onetwenty" id="ones_tot"  name="ones_tot" value="{{ number_format((float)$currency_data[0]->ones_box + $currency_data[0]->ones_roll + $currency_data[0]->ones_held, 2, '.', ''); }}">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-12">Fives</label>
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyfive ft width_onetwenty" id="nextinputfield"  name="fives_box"  onblur="sum_of_qty_price_currency(this.value,'fives_left')" value="{{ number_format((float)$currency_data[0]->fives_box, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyfive ft width_onetwenty" id="fives_right"  name="fives_roll" onblur="sum_of_qty_price_currency(this.value,'fives_right')" value="{{ number_format((float)$currency_data[0]->fives_roll, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyfive ft width_onetwenty" id="fives_cashheld"  name="fives_held" onblur="sum_of_qty_price_currency(this.value,'fives_cashheld')" value="{{ number_format((float)$currency_data[0]->fives_held, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly=""  class="form-control currencyfivetotal width_onetwenty" id="fives_tot"  name="fives_tot" value="{{ number_format((float)$currency_data[0]->fives_box + $currency_data[0]->fives_roll + $currency_data[0]->fives_held, 2, '.', ''); }}">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-12">Tens</label>
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyten ft width_onetwenty" id="tens_left"  name="tens_box" onblur="sum_of_qty_price_currency(this.value,'tens_left')" value="{{ number_format((float)$currency_data[0]->tens_box, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyten ft width_onetwenty" id="tens_right"  name="tens_roll" onblur="sum_of_qty_price_currency(this.value,'tens_right')" value="{{ number_format((float)$currency_data[0]->tens_roll, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencyten ft width_onetwenty" id="tens_cashheld"  name="tens_held" onblur="sum_of_qty_price_currency(this.value,'tens_cashheld')" value="{{ number_format((float)$currency_data[0]->tens_held, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly=""  class="form-control currencytentotal width_onetwenty" id="tens_tot"  name="tens_tot" value="{{ number_format((float)$currency_data[0]->tens_box + $currency_data[0]->tens_roll + $currency_data[0]->tens_held, 2, '.', ''); }}">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label col-sm-12">Twenties</label>
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencytwenty ft width_onetwenty" id="twenties_left"  name="twenties_box" onblur="sum_of_qty_price_currency(this.value,'twenties_left')" value="{{ number_format((float)$currency_data[0]->twenties_box, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencytwenty ft width_onetwenty" id="twenties_right"  name="twenties_roll" onblur="sum_of_qty_price_currency(this.value,'twenties_right')" value="{{ number_format((float)$currency_data[0]->twenties_roll, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencytwenty ft width_onetwenty" id="twenties_cashheld"  name="twenties_held" onblur="sum_of_qty_price_currency(this.value,'twenties_cashheld')" value="{{ number_format((float)$currency_data[0]->twenties_held, 2, '.', ''); }}">
         </div> 
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly="" class="form-control currencytwentytotal width_onetwenty" id="twenties_tot"  name="twenties_tot" value="{{ number_format((float)$currency_data[0]->twenties_box + $currency_data[0]->twenties_roll + $currency_data[0]->twenties_held, 2, '.', ''); }}">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-2">
            <label for="inputPassword" class="control-label">Misc 1 0</label>&nbsp;&nbsp;<input type="text" style="width:60px" class="form-control misc1_desc" placeholder="0.00" name="misc1_desc" value="{{ $currency_data[0]->misc1_desc }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymiscone ft width_onetwenty" id="misc1_left"  name="misc1_box" onblur="sum_of_qty_price_currency(this.value,'misc1_left')" value="{{ number_format((float)$currency_data[0]->misc1_box, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymiscone ft width_onetwenty" id="misc1_right"  name="misc1_roll" onblur="sum_of_qty_price_currency(this.value,'misc1_right')" value="{{ number_format((float)$currency_data[0]->misc1_roll, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymiscone ft width_onetwenty" id="misc1_cashheld"  name="misc1_held" onblur="sum_of_qty_price_currency(this.value,'misc1_cashheld')" value="{{ number_format((float)$currency_data[0]->misc1_held, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly="" class="form-control currencymisconetotal width_onetwenty" id="misc1_tot"  name="misc1_tot" value="{{ number_format((float)$currency_data[0]->misc1_box + $currency_data[0]->misc1_roll + $currency_data[0]->misc1_held, 2, '.', ''); }}">
         </div>
      </div>
      </div>
      <div class="row">
      <div class="form-group">
         <div class="col-sm-2 ">
            <label for="inputPassword" class="control-label">Misc 2 0</label>&nbsp;&nbsp;<input type="text" style="width:60px" class="form-control misc2_desc" placeholder="0.00" name="misc2_desc" value="{{ $currency_data[0]->misc2_desc }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymisctwo ft width_onetwenty" id="misc2_left"  name="misc2_box" onblur="sum_of_qty_price_currency(this.value,'misc2_left')" value="{{ number_format((float)$currency_data[0]->misc2_box, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymisctwo ft width_onetwenty" id="misc2_right"  name="misc2_roll" onblur="sum_of_qty_price_currency(this.value,'misc2_right')" value="{{ number_format((float)$currency_data[0]->misc2_roll, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00"  class="form-control currencymisctwo ft width_onetwenty" id="misc2_cashheld"  name="misc2_held" onblur="sum_of_qty_price_currency(this.value,'misc2_cashheld')" value="{{ number_format((float)$currency_data[0]->misc2_held, 2, '.', ''); }}">
         </div>
         <div class="col-sm-2">
            <input  type="number" placeholder="0.00" readonly="" class="form-control currencymisctwototal width_onetwenty" id="misc2_tot"  name="misc2_tot" value="{{ number_format((float)$currency_data[0]->misc2_box + $currency_data[0]->misc2_roll + $currency_data[0]->misc2_held, 2, '.', ''); }}">
         </div>
      </div>
      </div>
      <hr style="border:1px solid #000;" />
      <div class="form-group">
         <label for="inputPassword" class="control-label col-sm-3">Grand Total</label>
         <div class="col-sm-6">
            <input  class="form-control currencygrandtotal"  readonly="readonly" placeholder="0.00" id="currency_GrandTot"  name="currency_GrandTot" value="{{ number_format((float)$currency_total, 2, '.', '');  }}">
            @if($errors->has('currency_GrandTot'))
            {{ $errors->first('currency_GrandTot')}}
            @endif
         </div>
      </div>
      <div class="form-group">
         <div class="row">
            <div class="col-sm-12" align="center">
               <input type="button" name="" id="total_submit" tabindex="4" value="Submit" class="btn">
               <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('currencybox','safecountbox')">
            </div>
         </div>
      </div>
      <!--   </form> -->
   </div>
</div>
</div>
<!------------------End of currency popup---------------------->
<!------------------- Misc popup---------------------------->      
<div id="miscbox" style="margin-left: 87px;" class="mainbox miscbox col-md-10 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info safe_total">
      <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
         <div class="panel-title" >MISC</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >
         <div class="row vertical-divider" style="margin-top: 10px">
            <div class="col-xs-6" >
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                       <b>DESCRIPTION</b>
                  </div>
                  <div class="col-sm-4">
                     <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
                  </div>
               </div>
               <br><br>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Food stamps</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id="foodstamps"  name="food_stamps" value="{{ number_format((float)$other_data[0]->food_stamps, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Total Merch checks</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="text" placeholder="0.00" readonly=""  class="form-control totalmerch width_hundred" id=""  name="totalmerchchecks" value="{{ number_format((float)$other_data[0]->merch_fives + $other_data[0]->merch_tens + $other_data[0]->merch_twenties + $other_data[0]->merch_misc, 2, '.', '');  }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Total Script checks</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00" readonly="" class="form-control totalscript width_hundred" id=""  name="totalscriptchecks" value="{{ number_format((float)$other_data[0]->script1 + $other_data[0]->script2 + $other_data[0]->script3 + $other_data[0]->script4 + $other_data[0]->script5 + $other_data[0]->script6 + $other_data[0]->script7 + $other_data[0]->script8 + $other_data[0]->script9 + $other_data[0]->script10,2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Beginning Cash Loans</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="begin_loans" value="{{ number_format((float)$other_data[0]->begin_loans, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Misc 1 0</label>&nbsp;&nbsp;<input type="text" style="width:60px;" placeholder="0.00" class="form-control safeothermisc1" name="misc1_desc" value="{{ $other_data[0]->misc1_desc }}">
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="misc1_amt" value="{{ number_format((float)$other_data[0]->misc1_amt, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Misc 2 0</label>&nbsp;&nbsp;<input type="text" style="width:60px;" placeholder="0.00" class="form-control safeothermisc1" name="misc2_desc" value="{{ $other_data[0]->misc2_desc }}">
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="misc2_amt" value="{{ number_format((float)$other_data[0]->misc2_amt, 2, '.', ''); }}">
                  </div>
               </div>
               <br />
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">In-Store Charges</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="instr_chrg" value="{{ number_format((float)$other_data[0]->instr_chrg, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Payroll Advances</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="pay_adv" value="{{ number_format((float)$other_data[0]->pay_adv, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Night Crew Till</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="night_crew" value="{{ number_format((float)$other_data[0]->night_crew, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">EPS Debit To Carryover</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="debit" value="{{ number_format((float)$other_data[0]->debit, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">EPS Credit To Carryover</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="credit" value="{{ number_format((float)$other_data[0]->credit, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">EPS EBT To Carryover</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="ebt" value="{{ number_format((float)$other_data[0]->ebt, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">EPS GC To Carryover</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="giftcard" value="{{ number_format((float)$other_data[0]->giftcard, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Misc 3 0</label>&nbsp;&nbsp;<input type="text" style="width:60px;" placeholder="0.00" class="form-control safeothermisc1" name="misc3_desc" value="{{ $other_data[0]->misc3_desc }}">
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control gtotal ft width_hundred" id=""  name="misc3_amt" value="{{ number_format((float)$other_data[0]->misc3_amt, 2, '.', ''); }}">
                  </div>
               </div>
            </div>
            <div class="col-xs-6">
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                       <b>DESCRIPTION</b>
                  </div>
                  <div class="col-sm-4">
                     <label for="inputPassword" class="control-label col-sm-3">TOTAL</label>
                  </div>
               </div>
               <br><br>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Fives</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control merch gtotal ft width_hundred" id=""  name="merch_fives" value="{{ number_format((float)$other_data[0]->merch_fives, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Tens</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control merch gtotal ft width_hundred" id=""  name="merch_tens" value="{{ number_format((float)$other_data[0]->merch_tens, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Twenties</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control merch gtotal ft width_hundred" id=""  name="merch_twenties" value="{{ number_format((float)$other_data[0]->merch_twenties, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Misc</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control merch gtotal ft width_hundred" id=""  name="merch_misc" value="{{ number_format((float)$other_data[0]->merch_misc, 2, '.', ''); }}">
                  </div>
               </div>
               <br />
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script1</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script1" value="{{ number_format((float)$other_data[0]->script1, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script2</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script2" value="{{ number_format((float)$other_data[0]->script2, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script3</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script3" value="{{ number_format((float)$other_data[0]->script3, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script4</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script4" value="{{ number_format((float)$other_data[0]->script4, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script5</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script5" value="{{ number_format((float)$other_data[0]->script5, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script6</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script6" value="{{ number_format((float)$other_data[0]->script6, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script7</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script7" value="{{ number_format((float)$other_data[0]->script7, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script8</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script8" value="{{ number_format((float)$other_data[0]->script8, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script9</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script9" value="{{ number_format((float)$other_data[0]->script9, 2, '.', ''); }}">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-xs-5 text-left">
                     <label for="inputPassword" class="control-label">Script10</label>
                  </div>
                  <div class="col-sm-4">
                     <input  type="number" placeholder="0.00"  class="form-control script gtotal ft width_hundred" id=""  name="script10" value="{{ number_format((float)$other_data[0]->script10, 2, '.', ''); }}">
                  </div>
               </div>
            </div>
         </div>
         <hr style="border:1px solid #000;" />
         <div class="form-group">
            <label for="inputPassword" class="control-label col-sm-3">Grand Total</label>
            <div class="col-sm-6">
               <input  class="form-control grandtotal"  readonly="readonly" placeholder="0.00" id="GrandTot"  name="misc_grand_total" value="{{ number_format((float)$other_total, 2, '.', ''); }}">
               @if($errors->has('otherpricetot'))
               {{ $errors->first('otherpricetot')}}
               @endif
            </div>
         </div>
         <div class="form-group">
            <div class="row">
               <div class="col-sm-12" align="center">
                  <input type="button" name="" id="misc_submit" tabindex="4" value="Submit" class="btn">
                  <input type="reset" name="" id="submit" tabindex="4" value="Cancel" class="btn" onclick="cancel_click('miscbox','safecountbox')">
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
<!------------------End of Misc popup---------------------->
</div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script>
   $(document).ready(function() {
   $('#coinbox').hide();
   $('#currencybox').hide();
   $('#miscbox').hide();
   });
   
   function sum_of_qty_price(value,field_str)
   {
    //alert(value);
    //alert($('#'+field_str+'_box').val());
   var box=parseFloat($('#'+field_str+'_box').val());
   //alert(box);
   var boxval=box.toFixed(2);
   $('#'+field_str+'_box').val(boxval);
   
   var roll=parseFloat($('#'+field_str+'_roll').val());
   var rollval=roll.toFixed(2);
   $('#'+field_str+'_roll').val(rollval);
    var tot = parseFloat(boxval) + parseFloat(rollval); 
   //alert(tot);
   
   $('#'+field_str+'_tot').val(tot.toFixed(2)); 
   sum_of_all_tot();
   }
   function sum_of_all_tot()
   {
      //alert($('#pennies_tot').val());return false;
    peniestot=parseFloat($('#pennies_tot').val());
    nickletot=parseFloat($('#nickle_tot').val());
    dimestot=parseFloat($('#dimes_tot').val());
    quartertot=parseFloat($('#quarter_tot').val());
    misctot=parseFloat($('#misc_tot').val());
    if(!isNaN(peniestot))
    {
      var total=peniestot;
      //alert(total);
    }
    
    if(!isNaN(nickletot))
    {
      var total=peniestot+nickletot;
      //alert(total);
      
    }
    if(!isNaN(dimestot))
    {
      var total=peniestot+nickletot+dimestot;
      //alert(total);
    }
    if(!isNaN(quartertot))
    {
      var total=peniestot+nickletot+quartertot+dimestot;
      //alert(total);
    }
    if(!isNaN(misctot))
    {
      var total=peniestot+nickletot+quartertot+misctot+dimestot;
      //alert(total);
    }
   
   
   var coingrandtotal = $('#Coin_GrandTot').val(total.toFixed(2));
    
   }
   function sum_of_qty_price_currency(value,field_str)
   {
    
   var box=parseFloat($('#'+field_str+'_left').val());
   
   var boxval=box.toFixed(2);
   $('#'+field_str+'_left').val(boxval);
   
   var roll=parseFloat($('#'+field_str+'_right').val());
   var rollval=roll.toFixed(2);
   $('#'+field_str+'_right').val(rollval);
   
   var cash=parseFloat($('#'+field_str+'_cashheld').val());
   var cashval=cash.toFixed(2);
   $('#'+field_str+'_cashheld').val(cashval);
    var tot = parseFloat(boxval) + parseFloat(rollval) + parseFloat(cashval); 
   
   
   $('#'+field_str+'_tot').val(tot.toFixed(2)); 
   sum_of_all_tot_currency();
   }
   function sum_of_all_tot_currency()
   {
    onestot=parseFloat($('#ones_tot').val());
    fivestot=parseFloat($('#fives_tot').val());
    tenstot=parseFloat($('#tens_tot').val());
    twentiestot=parseFloat($('#twenties_tot').val());
    misc1tot=parseFloat($('#misc1_tot').val());
    misc2tot=parseFloat($('#misc2_tot').val());
    if(!isNaN(onestot))
    {
      var total=onestot;
    }
    
    if(!isNaN(fivestot))
    {
      var total=onestot+fivestot;
      
    }
    if(!isNaN(tenstot))
    {
      var total=onestot+fivestot+tenstot;
    }
    if(!isNaN(twentiestot))
    {
      var total=onestot+fivestot+tenstot+twentiestot;
    }
    if(!isNaN(misc1tot))
    {
      var total=onestot+fivestot+tenstot+twentiestot+misc1tot;
    }
    if(!isNaN(misc2tot))
    {
      var total=onestot+fivestot+tenstot+twentiestot+misc1tot+misc2tot;
    }
   
   
   $('#currency_GrandTot').val(total.toFixed(2));
   $('#currency_GrandTot1').val(total.toFixed(2));
  
    //$(".greatgranttotal").val(parseInt(total)+parseInt(cointot)+parseInt(sum1));
   }
   function hide_show_toggle_safecnt(pop_div,main_div)
   {
    //alert('hiii');
    $('#'+pop_div).show();
    $('#'+main_div).hide();
   }
   function cancel_click(pop_div,main_div)
   {
    $('#'+pop_div).hide();
    $('#'+main_div).show();
   }
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Sravan Kumar Sriramula Code js Code -->
<script type="text/javascript">  
   $(document).ready(function(){

      $("#coin_submit").click(function (e) { 
         //alert($('.ft').val());
         // if($(".ft").val() == '')
         // {
         //    $(".ft").css("border-color", "red");
         //    //alert("Please fill atleast one form details");
         //    return false;
         // }
         //return false;
      var datastring = $("#coin-form").serialize();
      //alert(datastring);
       $.post("coindetails",{data:datastring} ,function(data) {
                      // alert(data);
                           //$('#stage1').html(data);
       });
       cancel_click('coinbox','safecountbox');
        cointot=$('#Coin_GrandTot').val();
       $('#cointot').val(cointot);
         return false;
      });

      $("#total_submit").click(function (e) { 
      var datastring = $("#coin-form").serialize();
      //alert(datastring);
       $.post("coindetails",{data:datastring} ,function(data) {
                      // alert(data);
                           //$('#stage1').html(data);
       });
       cancel_click('currencybox','safecountbox');
        //cointot=$('#Coin_GrandTot').val();
       //$('#cointot').val(cointot);
         return false;
      });

      $("#misc_submit").click(function (e) { 
      var datastring = $("#coin-form").serialize();
      //alert(datastring);
       $.post("coindetails",{data:datastring} ,function(data) {
                      // alert(data);
                           //$('#stage1').html(data);
       });
       cancel_click('miscbox','safecountbox');
        //cointot=$('#Coin_GrandTot').val();
       //$('#cointot').val(cointot);
         return false;
      });
   
   });

</script>
<script type="text/javascript">
   $(document).ready(function() {
    $(".merch").each(function() {
         $(this).keyup(function() {
         calculateSum();
        });
    });
});

function calculateSum() {
    var sum = 0;
    $(".merch").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".totalmerch").val(sum.toFixed(2));
 }


  $(document).ready(function() {
    $(".script").each(function() {
         $(this).keyup(function() {
         calculateSum1 ();
        });
    });
});

function calculateSum1() {
    var sum = 0;
    $(".script").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".totalscript").val(sum.toFixed(2));
 }

$(document).ready(function() {
    $(".gtotal").each(function() {
         $(this).keyup(function() {
         calculateSum2();
        });
    });
});

function calculateSum2() {
    var sum1 = 0;
    $(".gtotal").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum1 += parseFloat(this.value);
         }
    });
   $(".grandtotal").val(sum1.toFixed(2));
 }
 ///$(".greatgranttotal").val(parseInt(total)+parseInt(cointot)+parseInt(sum.toFixed(2)));
  //$(".greatgranttotal").val(parseInt(total)+parseInt(cointot)+parseInt(miscfinaltotal));
 $(document).ready(function() {
    $(".currencyone").each(function() {
         $(this).keyup(function() {
         calculateSum3();
        });
    });
});

function calculateSum3() {
    var sum = 0;
    $(".currencyone").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencyonetotal").val(sum.toFixed(2));
 }

 $(document).ready(function() {
    $(".currencyfive").each(function() {
         $(this).keyup(function() {
         calculateSumFive();
        });
    });
});

function calculateSumFive() {
    var sum = 0;
    $(".currencyfive").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencyfivetotal").val(sum.toFixed(2));
 }
 
  $(document).ready(function() {
    $(".currencyten").each(function() {
         $(this).keyup(function() {
         calculateSumTen();
        });
    });
});

function calculateSumTen() {
    var sum = 0;
    $(".currencyten").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencytentotal").val(sum.toFixed(2));
 }

 $(document).ready(function() {
    $(".currencytwenty").each(function() {
         $(this).keyup(function() {
         calculateSumTwenty();
        });
    });
});

function calculateSumTwenty() {
    var sum = 0;
    $(".currencytwenty").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencytwentytotal").val(sum.toFixed(2));
 }

 $(document).ready(function() {
    $(".currencymisctwo").each(function() {
         $(this).keyup(function() {
         calculateSumMiscTwo();
        });
    });
});

function calculateSumMiscTwo() {
    var sum = 0;
    $(".currencymisctwo").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencymisctwototal").val(sum.toFixed(2));
 }
 $(document).ready(function() {
    $(".currencymiscone").each(function() {
         $(this).keyup(function() {
         calculateSumMiscOne();
        });
    });
});

function calculateSumMiscOne() {
    var sum = 0;
    $(".currencymiscone").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".currencymisconetotal").val(sum.toFixed(2));
 }

  $(document).ready(function() {
    $(".ft").each(function() {
         $(this).keyup(function() {
         calculateFinalTotal();
        });
    });
});

function calculateFinalTotal() {
    var sum = 0;
    $(".ft").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
         }
    });
    $(".ftotal").val(sum.toFixed(2));
 }
$(document).ready(function() {
    $(".coin,.currencyone,.currencyfive,.currencyten,.currencytwenty,.currencymiscone,.currencymisctwo,.gtotal").on("click", function () {
      $(this).select();
   });
 });
</script>
<style type="text/css">
   .row.vertical-divider {
   overflow: hidden;
   }
   .row.vertical-divider > div[class^="col-"] {
   text-align: center;
   padding-bottom: 100px;
   margin-bottom: -100px;
   border-left: 1px solid #000;
   border-right: 0px solid #000;
   }
   .row.vertical-divider div[class^="col-"]:first-child {
   border-left: none;
   }
   .row.vertical-divider div[class^="col-"]:last-child {
   border-right: none;
   }
   .misc_desc{
      display: initial !important;
   } 
   .misc1_desc{
      display: initial !important;
   }
   .misc2_desc{
      display: initial !important;
   }
   .safeothermisc1{
      display: initial !important;
   }
   .safeothermisc2{
      display: initial !important;
   }
   @media (min-width: 992px){
   .miscbox.safe_total{
       margin-left: 91px !important;
   }
}

.width_hundred{
   width: 110px !important;
}
.width_onetwenty{
   width: 120px !important;
}
</style>