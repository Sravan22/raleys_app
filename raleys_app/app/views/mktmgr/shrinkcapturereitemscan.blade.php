<?php
  // echo '<pre>';print_r($shrinkcaptureitemscan);exit;

?>
@extends('layout.dashboardstoretransfer')
@section('page_heading','Shrink Capture Review')
@section('content')
@section('section')
<style>
   @media print 
   {
   a[href]:after { content: none !important; }
   img[src]:after { content: none !important; }
   }
</style>
<header class="row">
   <div class="container">
      <div class="row text-center" style="margin-top: -40px">
         <b>
            <h4>Shrink Capture {{ $seq_number }}: Item Scan Detail </h4>
         </b>
      </div>
   </div>
</header>
{{-- 
<div class="container">
   <div class="row text-center">
      <b>
         <h4>Shrink Capture 40466: Item Scan Detail </h4>
      </b>
   </div>
</div>
--}}
<br>
<br>
<div class="container">
   <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6">
         <span class="pull-right">
         <a href="#" onclick="printPage()"><i class="fa fa-print fa-fw iconsize"></i> </a>
             <a href="{{ route('pdf-report-shrinkcaptureitemscan',['download'=>'pdf','shrink_number'=>$shrink_number,'shrink_date'=>$shrink_date,'employee_id'=>$employee_id,'tot_line_items'=>$tot_line_items,'tot_value'=>$tot_value,'seq_number'=>$seq_number,'status'=>$status])}}" target="_blank">
             <i class="fa fa-file-pdf-o fa-fw iconsize"></i>
             </a>
         </span>
      </div>
   </div>
</div>

<div class="container">


   <table align="center" class="table " id="example" style="width: 300px; ">
   <tr>
      <th>Shrink Capture No :</th>
      <td>{{ $shrink_number }} </td>
   </tr>
   <tr>
      <th>Shrink Date :</th>
      <td>{{ $shrink_date }} </td>
   </tr>
   <tr>
      <th>Employee Id :</th>
      <td>{{ $employee_id }}</td>
   </tr>
   <tr>
      <th>Total Line Items</th>
      <td>{{ $tot_line_items }}</td>
   </tr>
   <tr>
      <th>Total Value</th>
      <td>${{ $tot_value }} </td>
   </tr>
</table>

   <table class="table table-striped" id="example">
      <thead>
         <tr>
            <th>Scan No</th>
            <th>Description</th>
            <th>Item Cost</th>
            <th>Qty</th>
            <th>Ext Cost</th>
         </tr>
      </thead>
      <tbody>
         @for ($i = 0; $i < count($shrinkcaptureitemscan); $i++)
         <tr>
            <?php 
              $params = array(
                        'shrink_number' =>  $shrink_number,
                        'seq_number' =>  $shrinkcaptureitemscan[$i]->seq_number,
                        'status' =>  $status
                        ); 
              $queryString = http_build_query($params);
            ?>
            <td>{{  $shrinkcaptureitemscan[$i]->seq_number }}</td>
            {{-- <td>{{ $shrinkcaptureitemscan[$i]->item_desc }}</td> --}}
            <td><u>{{ HTML::link(URL::route('mktmgr-shrink-itemscan-update',$queryString), $shrinkcaptureitemscan[$i]->item_desc) }}</u></td>
            <td>${{ $shrinkcaptureitemscan[$i]->upc_cost }}</td>
            @if($shrinkcaptureitemscan[$i]->rw_rtl_amt!=0)
            <?php $quantity = $shrinkcaptureitemscan[$i]->rw_rtl_amt/$shrinkcaptureitemscan[$i]->rtl_amt; ?>
            <td>{{ floor($quantity) }}</td>
            <?php $ext_value = ($shrinkcaptureitemscan[$i]->upc_cost) * ($shrinkcaptureitemscan[$i]->rw_rtl_amt/$shrinkcaptureitemscan[$i]->rtl_amt); ?>
            <td>${{ number_format($ext_value, 2, '.', '');  }}</td>
            @else
            <td>{{ $shrinkcaptureitemscan[$i]->quantity }}</td>
            <?php $ext_value = ($shrinkcaptureitemscan[$i]->upc_cost) * ($shrinkcaptureitemscan[$i]->quantity); ?>
            <td>${{ number_format($ext_value, 2, '.', '');  }}</td>
            @endif
         </tr>
         @endfor 
         
      </tbody>
   </table>
   <div class="form-group">
        <div class="row">
        <div class="col-sm-12" align="center">
           
    <input type="button" class="btn" name="" onclick="history.go(-1);" value="Back">        
 
   {{ Form::token()}}
        </div>
        </div>
  </div>
   {{-- @else
   <div class="alert alert-danger">
      <strong>Alert!</strong> No Store Transfers meet Query criteria.
   </div>
   @endif --}}
</div>
@stop