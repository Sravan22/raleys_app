@extends('layout.dashboardbookkeepermarket')
@section('content')
@section('section')
<header class="row">
   @include('mktmgr.bookkepermenu')
</header>
<style type="text/css">
   .makecenter
   {
   margin-left:20px;
   }
   .row.vertical-divider > div[class^="col-"]
   {
   text-align: left;
   padding: 2px;
   }
</style>
<form class="form-horizontal" role="form" method="post" action="{{URL::route('mktmgr-updatebookregcheckoutitemupdateall')}}">
   <div class="container" style="">
      <div class="flash-message">
         @foreach (['danger', 'warning', 'success', 'info'] as $msg)
         @if(Session::has('alert-' . $msg))
         <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
         @endif
         @endforeach
      </div>
      <div id="loginbox" style="margin-top:-20px; margin-left: 10px;" class="mainbox col-xs-12 col-sm-12 col-sm-offset-1">
         <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
               <div class="panel-title" >REGISTER CHECK OUT</div>
            </div>
            <div style="padding-top:15px" class="panel-body" >
               <div class="row horizantal-divider">
                  <div class="col-xs-6">
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">DATE</label>
                        <div class="col-sm-4" style="text-align:left;margin-left:-48px;margin-top:7px;">
                           <div class="col-sm-6"><b>{{date("m/d/Y", strtotime($dateofinfo)); }}</b></div>
                           <input class="form-control col-sm-6" type="hidden" name="selecteddate" id="selecteddate" value="{{date("m/d/Y", strtotime($dateofinfo)); }}" tabindex="33" readonly="readonly">
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-lg-12"> -->
                  <?php $gtot=0;?>
                  @foreach($resultArray as $row)
                  <?php 
                     $gtot=$gtot+$row['item_amt'];
                     ?>
                  @endforeach  
                  <div class="col-xs-6">
                     <div class="form-horizontal">
                        <div class="form-group">
                           <label for="#" class="col-sm-2 control-label">CATEGORY</label>
                           <div class="col-sm-2" style="padding-left:0px;margin-top:7px;">
                           <div class="col-sm-2"><b>{{ $itemid }}</b></div>
                              <input type="hidden" class="form-control col-sm-2"  name="selecteditemsid" id="selecteditemsid" value="{{ $itemid }}"
                                 tabindex="34"  readonly="readonly">
                           </div>
                           <div class="col-sm-8" style="text-align:right;margin-left:-48px;margin-top:7px;">  
                              <div class="col-sm-7 text-right"><b>{{ $itemname }}</b></div>
                              <input type="hidden" class="form-control col-sm-4" name="selecteditemsname" id="selecteditemsname" tabindex="35" value="{{ $itemname }}" readonly="readonly">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <hr>
                  </div>
                  <div class="col-sm-4">
                     <div class="col-xs-6">
                        <label for="ex1" class="makecenter">Rg/Chkr</label>
                        <input class="form-control" id="rg1" name="rg1" type="hidden" readonly="readonly" tabindex="17" value="1">
                        <input class="form-control" id="rg4" name="rg4" type="hidden" readonly="readonly" tabindex="18" value="4">
                        <input class="form-control" id="rg7" name="rg7" type="hidden" readonly="readonly" tabindex="19" value="7">
                        <input class="form-control" id="rg12" name="rg12" type="hidden" readonly="readonly" tabindex="20" value="12">
                        <input class="form-control" id="rg15" name="rg15" type="hidden" readonly="readonly" tabindex="21" value="15">
                        <input class="form-control" id="rg30" name="rg30" type="hidden" readonly="readonly" tabindex="22" value="30">
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>1</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>4</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>7</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>12</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>15</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>30</b></span>
                     </div>
                     <div class="col-xs-6">
                        <label for="ex1" class="makecenter">Amount</label>
                        <input class="form-control calamt" id="amt1" name="amt1" type="number" step="0.01" tabindex="1"
                           value="{{ number_format($resultArray[0]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt4" name="amt4" type="number" step="0.01" tabindex="2" 
                           value="{{ number_format($resultArray[3]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt7" name="amt7" type="number" step="0.01" tabindex="3" 
                           value="{{ number_format($resultArray[6]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt12" name="amt12" type="number" step="0.01" tabindex="4" 
                           value="{{ number_format($resultArray[9]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt15" name="amt15" type="number" step="0.01" tabindex="5" 
                           value="{{ number_format($resultArray[12]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt30" name="amt30" type="number" step="0.01" tabindex="6" 
                           value="{{ number_format($resultArray[15]['item_amt'], 2, '.', '') }}">
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="col-xs-6">
                        <label for="ex1" class="makecenter">Rg/Chkr</label>
                        <input class="form-control" id="rg2" name="rg2" type="hidden" readonly="readonly" tabindex="23" value="2">
                        <input class="form-control" id="rg5" name="rg5" type="hidden" readonly="readonly" tabindex="24" value="5">
                        <input class="form-control" id="rg8" name="rg8" type="hidden" readonly="readonly" tabindex="25" value="8">
                        <input class="form-control" id="rg13" name="rg13" type="hidden" readonly="readonly" tabindex="26" value="13">
                        <input class="form-control" id="rg17" name="rg17" type="hidden" readonly="readonly" tabindex="27" value="17">
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>2</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>5</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>8</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>13</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>17</b></span>
                     </div>
                     <div class="col-xs-6">
                        <label for="ex1" class="makecenter">Amount</label>
                        <input class="form-control calamt" id="amt2" name="amt2" type="number" step="0.01" tabindex="7" 
                           value="{{ number_format($resultArray[1]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt5" name="amt5" type="number" step="0.01" tabindex="8" 
                           value="{{ number_format($resultArray[4]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt8" name="amt8" type="number" step="0.01" tabindex="9" 
                           value="{{ number_format($resultArray[7]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt13" name="amt13" type="number" step="0.01" tabindex="10" 
                           value="{{ number_format($resultArray[10]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt17" name="amt17" type="number" step="0.01" tabindex="11" 
                           value="{{ number_format($resultArray[13]['item_amt'], 2, '.', '') }}">
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="col-xs-6">
                        <label for="ex1" class="makecenter">Rg/Chkr</label>
                        <input class="form-control" id="rg3" name="rg3" type="hidden" readonly="readonly" tabindex="28" value="3">
                        <input class="form-control" id="rg6" name="rg6" type="hidden" readonly="readonly" tabindex="29" value="6">
                        <input class="form-control" id="rg9" name="rg9" type="hidden" readonly="readonly" tabindex="30" value="9">
                        <input class="form-control" id="rg14" name="rg14" type="hidden" readonly="readonly" tabindex="31" value="14">
                        <input class="form-control" id="rg29" name="rg29" type="hidden" readonly="readonly" tabindex="32" value="29">
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>3</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>6</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>9</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>14</b></span>
                        <br>
                        <span style="line-height: 33px;margin-left:37px;"><b>29</b></span>
                     </div>
                     <div class="col-xs-6">
                        <label for="ex1" class="makecenter">Amount</label>
                        <input class="form-control calamt" id="amt3" name="amt3" type="number" step="0.01" tabindex="12" 
                           value="{{ number_format($resultArray[2]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt6" name="amt6" type="number" step="0.01" tabindex="13" 
                           value="{{ number_format($resultArray[5]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt9" name="amt9" type="number" step="0.01" tabindex="14" 
                           value="{{ number_format($resultArray[8]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt14" name="amt14" type="number" step="0.01" tabindex="15" 
                           value="{{ number_format($resultArray[11]['item_amt'], 2, '.', '') }}">
                        <input class="form-control calamt" id="amt29" name="amt29" type="number" step="0.01" tabindex="16" 
                           value="{{ number_format($resultArray[14]['item_amt'], 2, '.', '') }}">
                     </div>
                  </div>
               </div>
            </div>
            @if($dayIsLocked != 'Y')
            <div class="form-group">
               <div class="row">
                  <div class="col-sm-12 topspace" align="center">
                     <input type="submit" name="login-submit" id="submit" value="Update" class="btn">
                     <input type="reset" name="login-submit" id="cancel" value="Cancel" class="btn"  onclick="goBack()">
                     {{ Form::token()}}
                  </div>
               </div>
            </div>
            @endif
         </div>
         <div class="row">
         <div class="col-md-3">
         </div>
         <div class="col-md-3 text-right">
          <label for="ex1" class="makecenter">Grand Total</label>
         </div>
         <div class="col-md-3">
         <b>{{ number_format($gtot, 2, '.', '') }}</b>
         <input type="hidden" step="0.01" class="form-control" id="gtot" name="gtot" placeholder="Grand Total" readonly="readonly"
            value="{{ number_format($gtot, 2, '.', '') }}">
            </div>
            <div class="col-md-3">
         </div>
            </div>
      </div>
   </div>
   </div>
   </div>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
     $(".calamt").blur(function(){  var sum = 0; 
           selfield = this.id;
           selfieldval=$("#"+selfield).val(); 
           $("#"+selfield).val(parseFloat(selfieldval).toFixed(2));
          $('.calamt').each(function() { 
           sum += parseFloat($(this).val()); 
           });
          $("#gtot").val(parseFloat(sum).toFixed(2));
          return true;
       });
    
    $('.calamt').each(function() { 
           sum += parseFloat($(this).val()); 
           });
          $("#gtot").val(parseFloat(sum).toFixed(2));
          return true;
   });       
   
    /*$(document).ready(function() {
     $(".calall").val("00.00");
     });
   
   $(".calall").blur(function(){ 
     selval=$("#"+this.id).val();
     if(selval == ''){$("#"+selval).val("00.00");}
     else{updatedval=parseFloat($("#"+this.id).val(),10).toFixed(2); $("#"+this.id).val(updatedval);}
     amt1=parseFloat($("#amt1").val(),10).toFixed(2);
     amt4=parseFloat($("#amt4").val(),10).toFixed(2);
     amt7=parseFloat($("#amt7").val(),10).toFixed(2);
     amt12=parseFloat($("#amt12").val(),10).toFixed(2);
     amt15=parseFloat($("#amt15").val(),10).toFixed(2);
     amt30=parseFloat($("#amt30").val(),10).toFixed(2);
   
     amt2=parseFloat($("#amt2").val(),10).toFixed(2);
     amt5=parseFloat($("#amt5").val(),10).toFixed(2);
     amt8=parseFloat($("#amt8").val(),10).toFixed(2);
     amt13=parseFloat($("#amt13").val(),10).toFixed(2);
     amt17=parseFloat($("#amt17").val(),10).toFixed(2);
     
     amt3=parseFloat($("#amt3").val(),10).toFixed(2);
     amt6=parseFloat($("#amt6").val(),10).toFixed(2);
     amt9=parseFloat($("#amt9").val(),10).toFixed(2);
     amt14=parseFloat($("#amt14").val(),10).toFixed(2);
     amt29=parseFloat($("#amt29").val(),10).toFixed(2);
     
     sumall= parseFloat(amt1)+parseFloat(amt4)+parseFloat(amt7)+parseFloat(amt12)+parseFloat(amt15)+parseFloat(amt30)+parseFloat(amt2)+parseFloat(amt5)+parseFloat(amt8)+parseFloat(amt13)+parseFloat(amt17)+parseFloat(amt3)+parseFloat(amt6)+parseFloat(amt9)+parseFloat(amt14)+parseFloat(amt29); 
     $("#gtot").val(parseFloat(sumall).toFixed(2));
   });  */
</script>
<script>
   function goBack() {
       window.history.back();
   }
</script>  
@stop