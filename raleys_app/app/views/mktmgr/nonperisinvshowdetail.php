    <div id="mainbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
        <div class="panel panel-info" >
            <div class="panel-heading" style="background-color:#CCC; color:#FFF; text-align:center; font-weight:bold;">
                <div class="panel-title" >Non Perishable Inventory Prep <?php echo $data->sodinv_dtl['inventory_number']; ?></div>
            </div> 
            
            <div style="padding-top:30px" class="panel-body" >

<form action="" class="form-horizontal" method="post" role="form" style="display: block;">
                
                <div class="form-group clear">
                        <label for="inputPassword" class="control-label col-sm-5">Inventory Date:</label>                        
                        <div class="control-label col-sm-7">
                           <?php echo $data->sodinv_dtl['create_date']; ?>
                        </div>
                    </div>


                    <div class="form-group clear">
                        <label for="inputPassword" class="control-label col-sm-5">Inventory Number:</label>                        
                        <div class="control-label col-sm-7">
                            <?php echo $data->sodinv_dtl['inventory_number']; ?>
                        </div>
                    </div>

                    <div class="form-group clear">
                        <label for="inputPassword" class="control-label col-sm-5">Inventory Type:</label>                        
                        <div class="control-label col-sm-7">
                            <?php echo $data->sodinv_dtl['inventory_type']; ?>
                        </div>
                    </div>

    <div class="form-group clear" style="margin-top: 20px;">
                        <label for="inputPassword" class="control-label col-sm-5">Inventory Area:</label>                        
                        <div class="control-label col-sm-7">
                            <?php echo $data->sodinv_dtl['inventory_area']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Inventory Section:</label>                        
                        <div class="control-label col-sm-7">
                            <?php echo $data->sodinv_dtl['inventory_section']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Employee ID:</label>                        
                        <div class="control-label col-sm-7">
                           $<?php echo $data->sodinv_dtl['employee_id']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Total Line Items:</label>                        
                        <div class="control-label col-sm-7">
                            $<?php echo $data->sodinv_dtl['total_scans']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Total Value At Cost:</label>                        
                        <div class="control-label col-sm-7">
                            $<?php echo $data->sodinv_dtl['total_value_cst']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Total Value At Retail:</label>                        
                        <div class="control-label col-sm-7">
                           $<?php echo $data->sodinv_dtl['total_value_rtl']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Total CRV Value At Retail:</label>                        
                        <div class="control-label col-sm-7">
                            $<?php echo $data->sodinv_dtl['total_crv_rtl']; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-sm-5">Current Status:</label>                        
                        <div class="control-label col-sm-7 hdrstatus">
                            <?php echo $data->sodinv_dtl['hdr_status']; ?>
                        </div>
                    </div>

                  

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12" align="center">
                            
                            <input type="hidden" id="invhdr_status" value="<?php echo $data->sodinv_dtl['invhdr_status']; ?>" />
                            <input type="hidden" id="hdr_status" value="<?php echo $data->sodinv_dtl['hdr_status']; ?>" />
                            
                            <input type="button" name="approve-submit" id="approve-submit" onclick="clickApprove('<?php echo $data->sodinv_dtl['inventory_number']; ?>')" tabindex="4" value="Approve" class="btn">                          
                            <input type="button" name="void-submit" id="void-submit" onclick="clickVoid('<?php echo $data->sodinv_dtl['inventory_number']; ?>')" tabindex="4" value="Void" class="btn">                          
                            <input type="button" name="cancel-submit" id="cancel-submit" onclick="clickCancel()" tabindex="4" value="Back" class="btn">                          
                        </div>
                    </div>
                </div>
    
    
    <div class="form-group">
        <div class="row">
            <div class="col-sm-12 errormsg" align="center">
            </div>
        </div>
    </div>

</form>

            </div>
        </div>
    </div>