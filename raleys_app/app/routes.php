<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/* start InvController*/
Route::get('/mktmgr/bookpostageprnt', array(
		'as' => 'mktmgr-bookpostageprnt',
		'uses' => 'InvController@getbookpostagePrint'
	));
Route::post('/mktmgr/post-postageprint', array(
		'as' => 'bookspro-post-postageprint',
		'uses' => 'InvController@postPostagePrint'
	));
Route::get('/mktmgr/bookphonecardprnt', array(
		'as' => 'mktmgr-bookphonecardprnt',
		'uses' => 'InvController@getbookphonecardPrnt'
	));
Route::post('/mktmgr/post-phoneprint', array(
		'as' => 'bookspro-post-phoneprint',
		'uses' => 'InvController@postphoneprint'
	));
Route::get('/mktmgr/bookcommuterprnt', array(
		'as' => 'mktmgr-bookcommuterprnt',
		'uses' => 'InvController@getbookcommuterPrnt'
	));
Route::post('/mktmgr/post-commuterprint', array(
		'as' => 'bookspro-post-commuterprint',
		'uses' => 'InvController@postcommuterprint'
	));
Route::get('/mktmgr/bookcommutermonthlyprnt', array(
		'as' => 'mktmgr-bookcommutermonthlyprnt',
		'uses' => 'InvController@getbookcommutermonthlyPrnt'
	));
Route::post('/mktmgr/post-mthlyprint', array(
		'as' => 'bookspro-post-mthlyprint',
		'uses' => 'InvController@postmthlyprint'
	));
/*end InvController*/

Route::any('/', array(	
	'as' => '/',
	'uses' => 'HomeController@home'
));

Route::get('/', array(	
	'as' => 'home',
	'uses' => 'HomeController@home'

	/*'as'=> 'account-sign-in',
	'uses'=> 'AccountController@getSignIn'*/
));



/*
/ unauthenticated group
*/

Route::group(array('before' => 'guest'), function(){
		/*
		/ CSRF protection group
		*/	
	Route::group(array('before'=>'csrf'), function(){


				Route::post('/account/create', array(
						'as'=> 'account-create-post',
						'uses'=> 'AccountController@postCreate'
					));
	});

		/*
		/ Sign In account (GET)
		*/

		Route::get('/account/sign-in', array(
			'as'=> 'account-sign-in',
			'uses'=> 'AccountController@getSignIn'
		));

		/*
		/ Sign In account (POST)
		*/

		Route::post('/account/sign-post', array(
			'as'=> 'account-sign-post',
			'uses'=> 'AccountController@postSignIn'
		));

		/*
		/ Create account (GET)
		*/
	
	Route::get('/account/create', array(
			'as'=> 'account-create',
			'uses'=> 'AccountController@getCreate'
		));

	Route::get('/account/activate/{code}', array(
				'as'   => 'account-activate',
				'uses' =>'AccountController@getActivate'
		));

	Route::get('/account/forgotpassword', array(
			'as'=> 'account-forgotpassword',
			'uses'=> 'AccountController@getNewPassword'
		));	

	Route::post('/account/account-forgotpassword-post', array(
			'as'=> 'account-forgotpassword-post',
			'uses'=> 'AccountController@postNewPassword'
		));
        
        Route::get('/admin', array(
			'as'=> 'admin',
			'uses'=> 'AdminController@admin'
		));
        Route::post('/admin/sign-post', array(
			'as'=> 'admin-sign-post',
			'uses'=> 'AdminController@AdminpostSignIn'
		));
});

/*
/ Authenticated group
*/
Route::group(array('before' => 'auth'), function() {

    /*
     | Sign Out (GET)
     | --
     */
    Route::get('/account/sign-out', array(
        'as' => 'account-sign-out',
        'uses' => 'AccountController@getSignOut'
    ));

Route::get('/home', array(
		'as' => 'user-home',
		'uses' => 'UserHomeController@userHome'
	));

// start of Bookkeeper menu list
Route::get('/mktmgr/bookkeeper', array(
		'as' => 'mktmgr-bookkeeper',
		'uses' => 'UserHomeController@bookkeeper'
	));
// Old Reg Register Route

// Route::get('/mktmgr/bookkeeperregister', array(
// 		'as' => 'mktmgr-bookkeeperregister',
// 		'uses' => 'UserHomeController@getbookkeeperRegister'
// 	));

// New Routes

Route::get('/mktmgr/bookkeeperregister', array(
		'as' => 'mktmgr-bookkeeperregister',
		'uses' => 'BookProRegController@getBookProRegister'
	));
Route::post('/mktmgr/bookprobeginloanreg', array(
		'as' => 'mktmgr-reg-beginloan',
		'uses' => 'BookProRegController@postBookProRegister'
	));
Route::post('/mktmgr/bookprobeginloanreg-update', array(
		'as' => 'mktmgr-postbeginloanreg-update',
		'uses' => 'BookProRegController@postBookProRegisterUpdate'
	));
Route::post('/mktmgr/bookprobeginloanreg-add', array(
		'as' => 'mktmgr-postbeginloanreg-add',
		'uses' => 'BookProRegController@postBookProRegisterAdd'
	));
Route::post('/mktmgr/regbeginloan-total', array(
		'as' => 'mktmgr-regbeginloan-total',
		'uses' => 'BookProRegController@RegBeginLoanTotal'
	));
Route::get('/mktmgr/regcheckoutitem', array(
		'as' => 'mktmgr-regcheckoutitem',
		'uses' => 'BookProRegController@RegCheckOutItem'
	));
Route::post('/mktmgr/updateregistercheckout', array(
		'as' => 'mktmgr-updateregistercheckout',
		'uses' => 'BookProRegController@updateRegCheckOutItem'
	));
Route::post('/mktmgr/registercheckoutitems', array(
		'as' => 'mktmgr-registercheckoutitems',
		'uses' => 'BookProRegController@RegisterCheckOutItems'
	));
Route::get('/mktmgr/regcheckouttotl', array(
		'as' => 'mktmgr-regcheckouttotl',
		'uses' => 'BookProRegController@getRegCheckOutTotal'
	));
Route::post('/mktmgr-regchkouttotpost', array(
				'as'   => 'mktmgr-regchkouttotpost',
				'uses' => 'BookProRegController@postRegCheckOutTotal'
		));
Route::get('/mktmgr/regcheckoutprint', array(
		'as' => 'mktmgr-regcheckoutprint',
		'uses' => 'BookProRegController@getRegCheckOutPrint'
	));
Route::post('/mktmgr/postregcheckoutprint', array(
		'as' => 'mktmgr-postregcheckoutprint',
		'uses' => 'BookProRegController@postRegCheckOutPrint'
	));
Route::get('/mktmgr/loantoregtotal', array(
		'as' => 'mktmgr-loantoregtotal',
		'uses' => 'BookProRegController@getLoantoRegTotal'
	));
Route::post('/mktmgr/postloantoregtotal', array(
		'as' => 'mktmgr-postloantoregtotal',
		'uses' => 'BookProRegController@postLoantoRegTotal'
	));
Route::get('/mktmgr/loantoregregister', array(
		'as' => 'mktmgr-loantoregregister',
		'uses' => 'BookProRegController@getLoantoRegRegister'
	));
Route::post('/mktmgr/postloantoregregister', array(
		'as' => 'mktmgr-postloantoregregister',
		'uses' => 'BookProRegController@postLoantoRegRegister'
	));
Route::post('/mktmgr/updateloantoregister', array(
		'as' => 'mktmgr-updateloantoregister',
		'uses' => 'BookProRegController@updateLoantoRegRegister'
	));
Route::post('/mktmgr/regcheckoutregister', array(
		'as' => 'mktmgr-regcheckoutregister',
		'uses' => 'BookProRegController@regCheckOutRegister'
	));
Route::post('/mktmgr/updateregcheckoutregister', array(
		'as' => 'mktmgr-updateregcheckoutregister',
		'uses' => 'BookProRegController@updateRegCheckOutRegister'
	));
Route::get('/mktmgr/bookkeeperregistertotal', array(
		'as' => 'mktmgr-bookkeeperregistertotal',
		'uses' => 'UserHomeController@getbookkeeperRegistertotal'
	));
Route::get('/mktmgr/bookkeeperregistercmt', array(
		'as' => 'mktmgr-bookkeeperregistercmt',
		'uses' => 'UserHomeController@getbookkeeperRegistercmt'
	));
Route::get('/mktmgr/bookloanrregister', array(
		'as' => 'mktmgr-bookloanrregister',
		'uses' => 'UserHomeController@getbookloanRegister'
	));
Route::post('/mktmgr/postbookloanrregisterreg', array(
		'as'   => 'mktmgr-postbookloanrregisterreg',
		'uses' => 'BeginloanController@postBookLoanrRegisterReg'
	));

Route::post('/mktmgr/savebookloanreg', array(
		'as'   => 'mktmgr-savebookloanreg',
		'uses' => 'BeginloanController@saveBookLoanReg'
	));

Route::get('/mktmgr/bookloanrregisterreg', array(
		'as' => 'mktmgr-bookloanrregisterreg',
		'uses' => 'BeginloanController@getBookLoanRegisterReg'
	));

	Route::get('/mktmgr/bookloanrregistervalidation', array(
				'as'   => 'mktmgr-bookloanrregistervalidation',
				'uses' =>  'UserHomeController@getbookloanRegister'
		));

    Route::post('/mktmgr/postbookloanrregister', array(
				'as'   => 'mktmgr-postbookloanrregister',
				'uses' =>  'BeginloanController@postBookLoanRRegister'
		));

    Route::post('/mktmgr/savebookloanrregister', array(
    			 'as'  => 'mktmgr-savebookloanrregister',
    			 'uses'=> 'UserHomeController@postSaveBookLoanRRegister'
    	));

	Route::get('/mktmgr/bookloanrregtotal', array(
		'as' => 'mktmgr-bookloanrregtotal',
		'uses' => 'BeginloanController@getbookloanregTotal'
	));

	Route::post('/mktmgr/postbookloanrregtotal', array(
		  'as'   => 'mktmgr-postbookloanrregtotal',
		  'uses' => 'BeginloanController@postBookLoanRRegTotal'		
		));
	
	Route::get('/mktmgr/bookloanrregcmt', array(
		'as' => 'mktmgr-bookloanrregcmt',
		'uses' => 'UserHomeController@getbookloanregCmt'
	));

Route::get('/mktmgr/bookregcheckoutitem', array(
		'as' => 'mktmgr-bookregcheckoutitem',
		'uses' => 'UserHomeController@getbookregcheckoutItem'
	));

Route::post('/mktmgr/savebookregcheckoutitem', array(
			'as'  => 'mktmgr-savebookregcheckoutitem',
			'uses' => 'UserHomeController@postSaveBookRegCheckoutItem'
	));

Route::post('/mktmgr/updatebookregcheckoutitem', array(
			'as'   => 'mktmgr-updatebookregcheckoutitem',
			'uses' => 'UserHomeController@postUpdateBookRegCheckOutItem'
	));

Route::post('/mktmgr/updatebookregcheckoutitemupdateall', array(
			'as'   => 'mktmgr-updatebookregcheckoutitemupdateall',
			'uses' => 'UserHomeController@postbookregcheckoutitemupdateall' 
	));

	Route::get('/mktmgr/bookregcheckoutreg', array(
		'as' => 'mktmgr-bookregcheckoutreg',
		'uses' => 'UserHomeController@getbookregcheckoutReg'
	));

	Route::post('/mktmgr/bookregcheckoutrefsave', array(
				 'as'   => 'mktmgr-bookregcheckoutrefsave',
				 'uses' => 'BeginloanController@postBookRegCheckoutRefSave'
		));

	Route::post('/mktmgr/postiuregcheckoutreg', array(
				 'as'   => 'mktmgr-postiuregcheckoutreg',
				 'uses' => 'BeginloanController@postIURegCheckOutReg'
		));	
	
	Route::get('/mktmgr/bookregcheckouttotl', array(
		'as' => 'mktmgr-bookregcheckouttotl',
		'uses' => 'UserHomeController@getbookregcheckoutTotl'
	));

	Route::post('/mktmgr-bookregchkouttotpost', array(
				'as'   => 'mktmgr-bookregchkouttotpost',
				'uses' => 'UserHomeController@postBookregChkOutTot'
		));

	Route::get('/mktmgr/bookregcheckoutprnt', array(
		'as' => 'mktmgr-bookregcheckoutprnt',
		'uses' => 'UserHomeController@getbookregcheckoutPrnt'
	));
	Route::get('/mktmgr/bookregcheckoutcmt', array(
		'as' => 'mktmgr-bookregcheckoutcmt',
		'uses' => 'UserHomeController@getbookregcheckoutCmt'
	));
	/*Route::get('/mktmgr/bookregcheckouttotlviewall', array(
			'as'  => 'mktmgr-bookregcheckouttotlviewall',
			'uses'=> 'UserHomeController@getViewDataRegChkOut'
		));*/
	// Route::get('/bla/blaaa', function(){(
	// 	// 'as' => 'mktmgr-bookregcheckoutcmt',
	// 	// 'uses' => 'UserHomeController@getbookregcheckoutCmt'
	// 	echo "Dummy text";
	// });
	
	Route::get('/mktmgr/bookregcmt', array(
		'as' => 'mktmgr-bookregcmt',
		'uses' => 'UserHomeController@getbookregCmt'
	));
	Route::get('/mktmgr/booksafecnt', array(
		'as' => 'mktmgr-booksafecnt',
		'uses' => 'UserHomeController@getbooksafeCnt'
	));
	Route::get('/mktmgr/booksafeprnt', array(
		'as' => 'mktmgr-booksafeprnt',
		'uses' => 'UserHomeController@getbooksafePrnt'
	));
	Route::get('/mktmgr/booksafereport', array(
		'as' => 'mktmgr-booksafereport',
		'uses' => 'UserHomeController@getbooksafeReport'
	));
	Route::get('/mktmgr/booksafereportprnt', array(
		'as' => 'mktmgr-booksafereportprnt',
		'uses' => 'UserHomeController@getbooksafereportPrnt'
	));
	Route::get('/mktmgr/booksafereportcmt', array(
		'as' => 'mktmgr-booksafereportcmt',
		'uses' => 'UserHomeController@getbooksafereportCmt'
	));
	Route::get('/mktmgr/booksafecmt', array(
		'as' => 'mktmgr-booksafecmt',
		'uses' => 'UserHomeController@getbooksafeCmt'
	));
	Route::get('/mktmgr/bookwklycmt', array(
		'as' => 'mktmgr-bookwklycmt',
		'uses' => 'UserHomeController@getbookwklyCmt'
	));
	Route::get('/mktmgr/bookwklyrecap', array(
		'as' => 'mktmgr-bookwklyrecap',
		'uses' => 'UserHomeController@getbookwklyRecap'
	));
	Route::get('/mktmgr/wklyrecap', array(
		'as' => 'mktmgr-wklyrecap',
		'uses' => 'WeeklyRecapController@getWklyRecap'
	));
	Route::get('/mktmgr/bookwklyprnt', array(
		'as' => 'mktmgr-bookwklyprnt',
		'uses' => 'UserHomeController@getbookwklyPrnt'
	));
	Route::get('/mktmgr/bookwklysales', array(
		'as' => 'mktmgr-bookwklysales',
		'uses' => 'UserHomeController@getbookwklySales'
	));
	Route::post('/mktmgr/salesdata', array(
	'as'   => 'bookspro-salesdata',
	'uses' => 'WeeklyRecapController@PostSalesData'		
	));

	Route::get('/mktmgr/wklysalesdate', array(
		'as' => 'bookspro_weeklysalesdate',
		'uses' => 'WeeklyRecapController@getwklySalesDate'
	));
	Route::get('/mktmgr/weeklysalesprint', array(
		'as' => 'bookspro-weeklysalesprint',
		'uses' => 'WeeklyRecapController@getwklySalesPrint'
	));
	Route::post('/mktmgr/post-wklysalesprint', array(
		'as' => 'bookspro-post-wklysalesprint',
		'uses' => 'WeeklyRecapController@postwklySalesPrint'
	));
	Route::post('mktmgr/wklysalesview', array(
			'as'   => 'bookspro-wklysalesview',
			'uses' => 'WeeklyRecapController@wklySalesView'
		));


	Route::post('/mktmgr/adjsalessubmit', array(
		'as'   => 'bookspro-adjsalessubmit',
		'uses' => 'WeeklyRecapController@AdjSalesSubmit'		
	));

	Route::post('mktmgr/postbookwklysalesview', array(
			'as'   => 'mktmgr-postbookwklysalesview',
			'uses' => 'UserHomeController@PostBookwklysalesView'
		));
	Route::post('/mktmgr/postbookwklysdetailssave', array(
			'as'   => 'mktmgr-postbookwklysdetailssave',
			'uses' => 'UserHomeController@PostBookwklysalesView'	
		));
	Route::post('/mktmgr/postbookwklysdetailssaveadj', array(
			'as'   => 'mktmgr-postbookwklysdetailssaveadj',
			'uses' => 'UserHomeController@postBookwklysdetailssaveAdj'
		));
	Route::post('/mktmgr/bookwklysales', array(
			'as'	=>'mktmgr-bookwklysales',
			'uses'	=> 'UserHomeController@getbookwklySales'
		));
	Route::post('mktmgr/postbookwklyrecap', array(
			'as'   => 'mktmgr-postbookwklyrecap',
		'uses' => 'UserHomeController@PostBookwklyRecap'
	));
	Route::post('mktmgr/postwklyrecap', array(
			'as'   => 'mktmgr-postwklyrecap',
			'uses' => 'WeeklyRecapController@PostWklyRecap'
	));
	Route::get('mktmgr/checklotteryinv', array(
			'as'   => 'mktmgr-checklottery',
			'uses' => 'WeeklyRecapController@checklottery'
	));
	Route::post('/mktmgr/runprocessweekly', array(
			'as'   => 'runprocessweekly',
			'uses' => 'WeeklyRecapController@runProcessWeekly'
	));


	Route::get('mktmgr/calclottery', array(
			'as'   => 'mktmgr-calclottery',
			'uses' => 'UserHomeController@calcLottery'
	));
	Route::post('mktmgr/recaplotteryinv', array(
			'as'   => 'mktmgr-wklyrecap-lotteryinv',
			'uses' => 'WeeklyRecapController@postSaveWklyRecap'
	));
	Route::post('mktmgr/saverewklyrecap', array(
			'as'   => 'mktmgr-saverewklyrecap',
			'uses' => 'UserHomeController@postSavereWklyRecap'
	));
	Route::get('/mktmgr/bookwklyview', array(
		'as' => 'mktmgr-bookwklyview',
		'uses' => 'UserHomeController@getbookwklyView'
	));
	Route::post('/mktmgr/postbookwklyview', array(
		'as'   => 'mktmgr-postbookwklyview',
		'uses' => 'UserHomeController@postBookwklyView'
	));
	Route::post('/mktmgr/postbookwklyviewdata', array(
		'as'   => 'mktmgr-postbookwklyviewdata',
		'uses' => 'UserHomeController@postBookWklyViewData'
		));
	Route::post('/mktmgr/unlocakwekly', array(
		'as'	=> 'mktmgr-unlocakwekly',
		'uses'  => 'UserHomeController@postUnlocakWekly'
	));
	Route::get('/mktmgr/bookwklyunlock', array(
		'as' => 'mktmgr-bookwklyunlock',
		'uses' => 'UserHomeController@getbookwklyUnlock'
	));
	Route::get('/mktmgr/passwordform', array(
		'as' => 'mktmgr-passwordform',
		'uses' => 'UserHomeController@getPasswordForm'
	));
	Route::post('/mktmgr/unlockpwd', array(
		'as'   =>'mktmgr-unlockpwd',
		'uses' =>'UserHomeController@postUnlockPwd'
	));
	
	Route::get('/mktmgr/bookpostageadd', array(
		'as' => 'mktmgr-bookpostageadd',
		'uses' => 'UserHomeController@getbookpostageAdd'
	));
    Route::post('/mktmgr/bookpostagepost', array(
		'as' => 'mktmgr-bookpostagepost',
		'uses' => 'UserHomeController@getbookpostagePost'
	));
        
        Route::post('/mktmgr/bookpostagepostdata', array(
		'as' => 'mktmgr-bookpostagepostdata',
		'uses' => 'UserHomeController@postbookpostageData'
	));
	Route::get('/mktmgr/booklotteryadd', array(
		'as' => 'mktmgr-booklotteryadd',
		'uses' => 'UserHomeController@getbooklotteryAdd'
	));
    Route::post('/mktmgr/booklotteryadddata', array(
		'as' => 'mktmgr-booklotteryadddata',
		'uses' => 'UserHomeController@postbooklotteryAddData'
	));
    Route::post('/mktmgr/booklotteryformdata', array(
		'as' => 'mktmgr-booklotteryformdata',
		'uses' => 'UserHomeController@postbooklotteryFormData'
	)); 
	Route::get('/mktmgr/booklotterytotl', array(
		'as' => 'mktmgr-booklotterytotl',
		'uses' => 'UserHomeController@getbooklotteryTotl'
	));
        Route::post('/mktmgr/booklotterytotaldata', array(
		'as' => 'mktmgr-booklotterytotaldata',
		'uses' => 'UserHomeController@postbooklotteryTotalData'
	));
        Route::post('/mktmgr/booklotterytotalformdata', array(
		'as' => 'mktmgr-booklotterytotalformdata',
		'uses' => 'UserHomeController@postbooklotteryTotalFormData'
	));
        
        
	Route::get('/mktmgr/booklotteryprnt', array(
		'as' => 'mktmgr-booklotteryprnt',
		'uses' => 'UserHomeController@getbooklotteryPrnt'
	));
	Route::get('/mktmgr/booklotteryview', array(
		'as' => 'mktmgr-booklotteryview',
		'uses' => 'UserHomeController@getbooklotteryView'
	));

	Route::any('/pdf/lottery-inventory', array(
			'as' => 'pdf-lottery-inventory',
			'uses' => 'PDFGenerator@getLotteryInventory'
	));
        
        Route::post('/mktmgr/booklotteryviewdata', array(
		'as' => 'mktmgr-booklotteryviewdata',
		'uses' => 'UserHomeController@postbooklotteryViewData'
	));
        
	Route::get('/mktmgr/bookcommuteradd', array(
		'as' => 'mktmgr-bookcommuteradd',
		'uses' => 'UserHomeController@getbookcommuterAdd'
	));
        
        Route::post('/mktmgr/bookcommutershow', array(
		'as' => 'mktmgr-bookcommutershow',
		'uses' => 'UserHomeController@getbookcommuterShowdata'
	));
        Route::post('/mktmgr/bookcommuterpost', array(
		'as' => 'mktmgr-bookcommuterpost',
		'uses' => 'UserHomeController@getbookcommuterPostdata'
	));
        
        
        Route::get('/mktmgr/bookcommuterrecapitemslist', array(
		'as' => 'mktmgr-bookcommuterrecapitemslist',
		'uses' => 'UserHomeController@getrecapitemsdesc'
	));
        
        
        
	
	

        Route::get('/mktmgr/bookcommutermonthlyadd', array(
                'as' => 'mktmgr-bookcommutermonthlyadd',
                'uses' => 'UserHomeController@getbookcommutermonthlyAdd'
            ));
        Route::get('/mktmgr/bookcommuterrecapitemslistmonthly', array(
        'as' => 'mktmgr-bookcommuterrecapitemslistmonthly',
        'uses' => 'UserHomeController@getrecapitemsdescmonthly'
    ));
        Route::post('/mktmgr/bookcommutermonthlyshow', array(
        'as' => 'mktmgr-bookcommutermonthlyshow',
        'uses' => 'UserHomeController@getbookcommutermonthlyShowdata'
    ));
        Route::post('/mktmgr/bookcommutermonthlypost', array(
        'as' => 'mktmgr-bookcommutermonthlypost',
        'uses' => 'UserHomeController@getbookcommutermonthlyPostdata'
    ));
	
	Route::get('/mktmgr/bookphonecardadd', array(
		'as' => 'mktmgr-bookphonecardadd',
		'uses' => 'UserHomeController@getbookphonecardAdd'
	));
        Route::post('/mktmgr/bookphonecardshow', array(
		'as' => 'mktmgr-bookphonecardshow',
		'uses' => 'UserHomeController@getbookphonecardShowdata'
	));
        Route::post('/mktmgr/bookphonecardpost', array(
		'as' => 'mktmgr-bookphonecardpost',
		'uses' => 'UserHomeController@getbookphonecardPostdata'
	));
        
        
	/*Route::get('/mktmgr/bookphonecardprnt', array(
		'as' => 'mktmgr-bookphonecardprnt',
		'uses' => 'UserHomeController@getbookphonecardPrnt'
	));*/
	Route::get('/mktmgr/bookepsadd', array(
		'as' => 'mktmgr-bookepsadd',
		'uses' => 'UserHomeController@getbookepsAdd'
	));
	Route::get('/mktmgr/bookepsprint', array(
		'as' => 'mktmgr-bookepsprint',
		'uses' => 'EpsPrntController@getepsprnt'
	));
	Route::post('/mktmgr/bookepsdatesubmit', array(
		'as' => 'mktmgr-bookepsdatesubmit',
		'uses' => 'EpsPrntController@getepsprntdatesubmit'
	));
        Route::post('/mktmgr/bookepsshow', array(
		'as' => 'mktmgr-bookepsshow',
		'uses' => 'UserHomeController@getbookepsShowdata'
	));
        Route::post('/mktmgr/bookepspost', array(
		'as' => 'mktmgr-bookepspost',
		'uses' => 'UserHomeController@getbookepsPostdata'
	));
        
        
        
	Route::get('/mktmgr/bookepsprnt', array(
		'as' => 'mktmgr-bookepsprnt',
		'uses' => 'UserHomeController@getbookepsPrnt'
	));
	Route::get('/mktmgr/bookmngmntdaily', array(
		'as' => 'mktmgr-bookmngmntdaily',
		'uses' => 'UserHomeController@getbookmngmntDaily'
	));
	Route::any('/mktmgr/post-bookmngmntdaily', array(
		'as' => 'mktmgr-post-bookmngmntdaily',
		'uses' => 'UserHomeController@postBookmngmntDaily'
	));
	Route::get('/mktmgr/post-bookmngmntdailyshowdetails', array(
		'as' => 'mktmgr-post-bookmngmntdailyshowdetails',
		'uses' => 'UserHomeController@postBookmngmntDailyShowDetails'
	));
	Route::post('/mktmgr/post-bookmngmntdailydetailaccept', array(
		'as' => 'mktmgr-post-bookmngmntdailydetailaccept',
		'uses' => 'UserHomeController@postBookmngmntDailyDetailsAcc'
	));
	Route::post('/mktmgr/post-bookmngmntdailymgrok', array(
		'as' => 'mktmgr-post-bookmngmntdailymgrok',
		'uses' => 'UserHomeController@postBookmngmntdailymgrok'
	));
	Route::get('/mktmgr/bookmngmntweekly', array(
		'as' => 'mktmgr-bookmngmntweekly',
		'uses' => 'UserHomeController@getbookmngmntWeekly'
	));
	Route::any('/mktmgr/post-bookmngmntweekly', array(
		'as' => 'mktmgr-post-bookmngmntweekly',
		'uses' => 'UserHomeController@postBookmngmntWeekly'
	));
	Route::get('/mktmgr/post-bookmngmntweeklyshowdetails', array(
		'as' => 'mktmgr-post-bookmngmntweeklyshowdetails',
		'uses' => 'UserHomeController@postBookmngmntWeeklyShowDetails'
	));
	/*Route::get('/mktmgr/post-bookmngmntweeklydepositshowdetails', array(
		'as' => 'mktmgr-post-bookmngmntweeklydepositshowdetails',
		'uses' => 'UserHomeController@postBookmngmntWeeklyDepositShowDetails'
	));*/
	Route::post('/mktmgr/post-bookmngmntweeklymgrok', array(
		'as' => 'mktmgr-post-bookmngmntweeklymgrok',
		'uses' => 'UserHomeController@wk_approve'
	));

	/*Route::get('/mktmgr/post-bookmngmntweeklydepositshowdetails', array(
		'as' => 'mktmgr-post-bookmngmntweeklydepositshowdetails',
		'uses' => 'UserHomeController@postBookmngmntWeeklyDepositShowDetails'
	));*/

	Route::get('/mktmgr/bookmngmntcmt', array(
		'as' => 'mktmgr-bookmngmntcmt',
		'uses' => 'UserHomeController@getbookmngmntCmt'
	));
	Route::get('/mktmgr/bookutilitypymntcnt', array(
		'as' => 'mktmgr-bookutilitypymntcnt',
		'uses' => 'UtilityPaymentCount@getbookutilitypymntCnt'
	));
	Route::any('/mktmgr/post-utility-pay', array(
		'as' => 'mktmgr-post-utility-pay',
		'uses' => 'UtilityPaymentCount@postbookutilitypymntCnt'
	));
	Route::any('/mktmgr/post-utility-pay-count', array(
		'as' => 'mktmgr-post-utility-pay-count',
		'uses' => 'UtilityPaymentCount@postbookutilitypymntCntData'
	));
	Route::any('/mktmgr/post-utility-pay-count-update', array(
		'as' => 'mktmgr-post-utility-pay-count-update',
		'uses' => 'UtilityPaymentCount@postbookutilitypymntCntDataUpdate'
	));
	Route::any('/mktmgr/viewcounts', array(
		'as' => 'mktmgr-viewcounts',
		'uses' => 'UtilityPaymentCount@viewCounts'
	));
	Route::get('/mktmgr/bookcommentlist', array(
		'as' => 'mktmgr-bookcommentlist',
		'uses' => 'UserHomeController@getbookcommentList'
	));
	Route::get('/mktmgr/commentlist', array(
		'as' => 'mktmgr-commentlist',
		'uses' => 'UserHomeController@getcommentList'
	));
	Route::post('/mktmgr/commentdate', array(
		'as' => 'mktmgr-post-commentdate',
		'uses' => 'UserHomeController@postcommentDate'
	));
	Route::post('/mktmgr/post-addcommentdata', array(
		'as' => 'mktmgr-post-addcommentdata',
		'uses' => 'UserHomeController@postAddcommentData'
	));
	Route::post('/mktmgr/post-updatecommentdata', array(
		'as' => 'mktmgr-post-updatecommentdata',
		'uses' => 'UserHomeController@postUpdatecommentData'
	));
	Route::get('/mktmgr/commentadd', array(
		'as' => 'mktmgr-commentadd',
		'uses' => 'UserHomeController@commentAdd'
	));

	Route::post('/mktmgr/safecountdetail', array(
		'as'   => 'mktmgr-safecountdetail',
		'uses' => 'BookkeeperController@getSafeCountDetail'		
	));
	Route::post('/mktmgr/safereportsdetail', array(
		'as'=> 'mktmgr-safereportsdetail',
			'uses'=> 'BookkeeperController@getSafeReportDetail'	
	));
	Route::post('/mktmgr/postsafereportdetail', array(
		'as'   => 'mktmgr-postsafereportdetail',
		'uses' => 'BookkeeperController@postSafeReportDetail'		
	));
	Route::post('/mktmgr/postsafereportdetailupdate', array(
		'as'   => 'mktmgr-postsafereportdetailupdate',
		'uses' => 'BookkeeperController@postSafeReportDetailUpdate'		
	));

	Route::get('/mktmgr/safereportsdetail1', function(){
		// 'as'=> 'mktmgr-safereportsdetail1',
		// 	'uses'=> 'BookkeeperController@getSafeReportDetail1'
		return View::make('mktmgr.safereportsdetail');
	});
	Route::post('/mktmgr/coindetails', array(
		'as'=> 'mktmgr-coindetails',
			'uses'=> 'BookkeeperController@postInsertCoindetails'	
	));
	
	Route::get('/mktmgr/bookcommentadd', array(
		'as' => 'mktmgr-bookcommentadd',
		'uses' => 'UserHomeController@bookcommentAdd'
	));

	Route::post('/mktmgr/post-bookcomment', array(
		'as' => 'mktmgr-post-bookcomment',
		'uses' => 'UserHomeController@postBookcomment'
	));

	Route::get('/mktmgr/returnhome', array(
		'as' => 'mktmgr-returnhome',
		'uses' => 'UserHomeController@getBackHome'
	));

	Route::post('/mktmgr/beginloanreg', array(
			'as'   => 'mktmgr-beginloanreg',
			'uses' => 'BeginloanController@beginLoanRegister'
		));

	Route::post('/mktmgr/postbeginloanreg', array(
			'as'   => 'mktmgr-postbeginloanreg',
			'uses' => 'BeginloanController@postBeginLoanRegister'
		));

	Route::get('/mktmgr/savebeginloanregister', array(
			'as'   => 'mktmgr-savebeginloanregister',
			'uses' =>'UserHomeController@getSaveBeginLoanregister'
		));

	Route::post('/mktmgr/postdategetdata', array(
				'as'   =>'mktmgr-postdategetdata',
				'uses' => 'BeginloanController@postDateGetData'
		));
	/*Route::post('/mktmgr/runprocessweekly', array(
			'as'   => 'runprocessweekly',
			'uses' => 'UserHomeController@runProcessWeekly'
	));
*/
	// Sravan Kumar Safe Routes

	Route::post('/mktmgr/safecountdetail', array(
		'as'   => 'mktmgr-safecountdetail',
		'uses' => 'BookkeeperController@getSafeCountDetail'		
	));
	Route::post('/mktmgr/postsafecountdetail', array(
		'as'   => 'mktmgr-postsafecountdetail',
		'uses' => 'BookkeeperController@postSafeCountDetail'		
	));
	Route::post('/mktmgr/postsafecountdetailupdate', array(
		'as'   => 'mktmgr-postsafecountdetailupdate',
		'uses' => 'BookkeeperController@postSafeCountDetailUpdate'		
	));
	
	// Sravan Kumar Safe Routes Ends
// end of Bookkeeper menu list
	Route::get('/mktmgr/bookchgquery', array(
               'as' => 'mktmgr-bookchgquery',
               'uses' => 'UserHomeController@getbookchgquery'
       ));
	 Route::get('/mktmgr/bookchgadd', array(
               'as' => 'mktmgr-bookchgadd',
               'uses' => 'UserHomeController@getbookchgadd'
       ));
       Route::get('/mktmgr/bookchgupdate/{$reg_num}', array(
               'as' => 'mktmgr-bookchgupdate',
               'uses' => 'UserHomeController@getbookchgupdate'
       ));
       Route::get('/mktmgr/bookchgdelete', array(
               'as' => 'mktmgr-bookchgdelete',
               'uses' => 'UserHomeController@getbookchgdelete'
       ));
       Route::post('/mktmgr/bookchglist', array(
                       'as'   => 'mktmgr-post-bookchglist',
                       'uses' => 'BookkeeperController@postbookchglist' 
       ));
       Route::post('/mktmgr/post-bookkeeper-chgadd', array(
                       'as'   => 'mktmgr-post-bookkeeper-chgadd',
                       'uses' => 'BookkeeperController@postbookkeeperchgadd' 
       ));
       Route::get('/mktmgr/view-chg-reg/{reg_num}', array(
                       'as'   => 'mktmgr-view-chg-reg',
                       'uses' => 'BookkeeperController@viewchgregnum' 
       ));
       Route::get('/mktmgr/update-chg-reg', array(
                       'as'   => 'mktmgr-update-chgreg',
                       'uses' => 'BookkeeperController@updatechgregnum' 
       ));
       Route::post('/mktmgr/post-chg-reg-update', array(
                       'as'   => 'mktmgr-post-chg-reg-update',
                       'uses' => 'BookkeeperController@postupdatechgreg' 
       ));
         Route::post('/mktmgr/chgreg-delete', array(
                       'as'   => 'mktmgr-chgreg-delete',
                       'uses' => 'BookkeeperController@chgregdel' 
       ));
       
	// Start of receivings menu list

Route::get('/mktmgr/receivings', array(
			'as'   => 'mktmgr-receivings',
			'uses' => 'ReceivingsMenuController@receivings'
	));

Route::get('/mktmgr/receivings-query', array(
			'as'   => 'mktmgr-receivings-query',
			'uses' => 'ReceivingsMenuController@receivingsQuery'
	));

Route::get('/mktmgr/receivingsqueryadvance-query', array(
			'as'   => 'mktmgr-receivingsqueryadvance-query',
			'uses' => 'ReceivingsMenuController@receivingsQueryAdvance'
	));
Route::post('/mktmgr/totalvendors', array(
			'as'   => 'totalvendors',
			'uses' => 'ReceivingsMenuController@totalVendors'
	));

Route::post('/mktmgr/post-updatequeryresult', array(
			'as'   => 'mktmgr-post-updatequeryresult',
			'uses' => 'ReceivingsMenuController@receivingsQueryUpdate'
	));

Route::get('/mktmgr/receivings-browse', array(
			'as'   => 'mktmgr-receivings-browse',
			'uses' => 'ReceivingsMenuController@receivingsBrowse'
	));

Route::get('/mktmgr/receivings-update', array(
			'as'   => 'mktmgr-receivings-update',
			'uses' => 'ReceivingsMenuController@receivingsUpdate'
	));

Route::get('/mktmgr/receivings-items', array(
			'as'   => 'mktmgr-receivings-items',
			'uses' => 'ReceivingsMenuController@receivingsItems'
	));

Route::get('/mktmgr/receivings-itemsmsg', array(
			'as'   => 'mktmgr-receivings-itemsmsg',
			'uses' => 'ReceivingsMenuController@receivingsItemsMsg'
	));

Route::get('/mktmgr/receivings-receivingsofflinereceiving', array(
			'as'   => 'mktmgr-receivings-receivingsofflinereceiving',
			'uses' => 'ReceivingsMenuController@receivingsOfflinereceiving'
	));
Route::post('/mktmgr/crv_switch', array(
			'as'   => 'crv_switch',
			'uses' => 'ReceivingsMenuController@crvSwitch'
	));

Route::get('/mktmgr/receivings-sequery', array(
			'as'   => 'mktmgr-receivings-sequery',
			'uses' => 'ReceivingsMenuController@receivingsSeQuery' 
	));

Route::get('/mktmgr/view-receivings-sequery', array(
			'as'   => 'mktmgr-view-receivings-sequery',
			'uses' => 'ReceivingsMenuController@viewReceivingsSeQuery' 
	));

Route::get('/mktmgr/update-receivings-sequery', array(
			'as'   => 'mktmgr-update-receivings-sequery',
			'uses' => 'ReceivingsMenuController@updateReceivingsSeQuery' 
	));
Route::get('/mktmgr/mktmgr-view-storeexpenses', array(
			'as'   => 'mktmgr-view-storeexpenses',
			'uses' => 'ReceivingsMenuController@viewBrowseReceivingsSeQuery' 
	));
Route::post('/mktmgr/search-receivings-sequery', array(
			'as'   => 'mktmgr-search-receivings-sequery',
			'uses' => 'ReceivingsMenuController@searchReceivingsSeQuery' 
	)); 

Route::post('/mktmgr/post-receivings-sequery', array(
			'as'   => 'mktmgr-post-receivings-sequery',
			'uses' => 'ReceivingsMenuController@postReceivingsSeQuery' 
	));

Route::get('/mktmgr/receivings-sebrowse', array(
			'as'   => 'mktmgr-receivings-sebrowse',
			'uses' => 'ReceivingsMenuController@receivingsSeBrowse' 
	));

/*
Route::get('/mktmgr/receivings-receivingsseupdate', array(
			'as'   => 'mktmgr-receivings-receivingsseupdate',
			'uses' => 'ReceivingsMenuController@receivingsSeUpdate' 
	));
*/
Route::get('/mktmgr/receivings-sebrowse', array(
			'as'   => 'mktmgr-receivings-sebrowse',
			'uses' => 'ReceivingsMenuController@receivingsSeBrowse' 
	));

Route::get('/mktmgr/receivings-sereports', array(
			'as'   => 'mktmgr-receivings-sereports',
			'uses' => 'ReceivingsMenuController@receivingsSeReports' 
	));

Route::post('/mktmgr/post-receivings-sereports', array(
			'as'   => 'mktmgr-post-receivings-sereports',
			'uses' => 'ReceivingsMenuController@postReceivingsSeReports' 
	));

Route::get('/mktmgr/receivings-receivingsseupdate', array(
			'as'   => 'mktmgr-receivings-receivingsseupdate',
			'uses' => 'ReceivingsMenuController@receivingsSeUpdate' 
	));
Route::post('/mktmgr/get_vendor_number', array(
			'as'   => 'get_vendor_number',
			'uses' => 'ReceivingsMenuController@getVendorNumber' 
	));

/*
Route::get('/mktmgr/receivings-receivingsseupdate', array(
			'as'   => 'mktmgr-receivings-receivingsseupdate',
			'uses' => 'ReceivingsMenuController@receivingsSeUpdate' 
	));
*/
Route::post('/mktmgr/vendorlist', array(
			'as'   => 'vendorlist',
			'uses' => 'ReceivingsMenuController@vendorListByDeptID'
	));
Route::post('/mktmgr/transporter_list', array(
			'as'   => 'transporter_list',
			'uses' => 'ReceivingsMenuController@transporterListByVenderNo'
	));

Route::post('/mktmgr/crv_status', array(
			'as'   => 'crv_status',
			'uses' => 'ReceivingsMenuController@crvStatusByVenderNo'
	));

Route::get('/mktmgr/vendorlist', 'ReceivingsMenuController@autocomplete');

Route::post('mktmgr/vendor-databydate', array(
			'as'	=> 'mktmgr-vendor-databydate',
			'uses'  => 'ReceivingsMenuController@postVendorDataByDate' 
	));

 // end of receivings menu list

// Start of shrinkcapturereview menu list
Route::any('/pdf/report-shrinkcapturedata', array(
			'as' => 'pdf-report-shrinkcapturedata',
			'uses' => 'PDFGenerator@getPdfShrinkCaptureData'
	));
Route::any('/pdf/report-shrinkcaptureitemscan', array(
			'as' => 'pdf-report-shrinkcaptureitemscan',
			'uses' => 'PDFGenerator@getPdfShrinkCaptureItemScan'
	));

Route::any('/pdf/report-shrinkcapturedept', array(
			'as' => 'pdf-report-shrinkcapturedept',
			'uses' => 'PDFGenerator@getPdfShrinkCaptureDept'
	));

Route::get('/mktmgr/shrinkcapturereview', array(
			'as'   => 'mktmgr-shrinkcapturereview',
			'uses' => 'ShrinkCaptureReview@shrinkCaptureReview' 
	));
Route::post('/mktmgr/post-shrink-review', array(
			'as'   => 'mktmgr-post-shrink-review',
			'uses' => 'ShrinkCaptureReview@postShrinkCaptureReview' 
	));
Route::get('/mktmgr/shrink-capture-department', array(
			'as'   => 'mktmgr-shrink-capture-department',
			'uses' => 'ShrinkCaptureReview@ShrinkCaptureDepartment' 
	));
Route::get('/mktmgr/shrink-capture-itemscan', array(
			'as'   => 'mktmgr-shrink-capture-itemscan',
			'uses' => 'ShrinkCaptureReview@ShrinkCaptureItemScan' 
	));
Route::get('/mktmgr/shrink-itemscan-update', array(
			'as'   => 'mktmgr-shrink-itemscan-update',
			'uses' => 'ShrinkCaptureReview@ShrinkCaptureItemScanUpdate' 
	));
Route::post('/mktmgr/update-shrink-itemscan', array(
			'as'   => 'mktmgr-update-shrink-itemscan',
			'uses' => 'ShrinkCaptureReview@UpdateShrinkItemSan' 
	));
Route::post('/mktmgr/post-update-shrink-itemscan', array(
			'as'   => 'mktmgr--post-update-shrink-itemscan',
			'uses' => 'ShrinkCaptureReview@PostUpdateShrinkItemSan' 
	));


Route::post('/mktmgr/shrinkcapturereviewdate', array(
			'as'   => 'mktmgr-shrinkcapturereviewdate',
			'uses' => 'ReceivingsMenuController@postshrinkCaptureReviewDate' 
	));

Route::get('/mktmgr/receivings-receivingslogexpense', array(
			'as'   => 'mktmgr-receivings-receivingslogexpense',
			'uses' => 'ReceivingsMenuController@getReceivingsLogExpense' 
	));
Route::post('/mktmgr/check_inv_number', array(
			'as'   => 'check_inv_number',
			'uses' => 'ReceivingsMenuController@checkInvoiceNumber' 
	));

Route::post('/mktmgr/post-receivings-receivingslogexpense', array(
			'as'   => 'mktmgr-post-receivings-receivingslogexpense',
			'uses' => 'ReceivingsMenuController@postReceivingsLogExpense' 
	));

Route::any('/pdf/expensesreport', array(
			'as' => 'pdf-expensesreport',
			'uses' => 'PDFGenerator@getPdfExpensesReport'
	));

Route::get('/mktmgr/report-weeklyreport', array(
			'as'   => 'mktmgr-report-weeklyreport',
			'uses' => 'ReceivingsMenuController@getWeeklyReport' 
	));
Route::post('/mktmgr/post-report-weeklyreport', array(
			'as'   => 'mktmgr-post-report-weeklyreport',
			'uses' => 'ReceivingsMenuController@postWeeklyReport' 
	));

Route::any('/pdf/report-weeklyreport', array(
			'as' => 'pdf-report-weeklyreport',
			'uses' => 'PDFGenerator@getPdfReportWeekly'
	));

Route::any('/pdf/dexloginvoicereport', array(
			'as' => 'pdf-dexloginvoicereport',
			'uses' => 'PDFGenerator@getDexLogInvoiceReport'
	));

Route::get('/mktmgr/report-browse-weeklyreport', array(
			'as'   => 'mktmgr-browse-report-weeklyreport',
			'uses' => 'ReceivingsMenuController@browseWeeklyReport' 
	));

Route::get('/mktmgr/report-storeinvoice', array(
			'as'   => 'mktmgr-report-storeinvoice',
			'uses' => 'ReceivingsMenuController@getStoreInvoice' 
	));
Route::post('/mktmgr/report-post-storeinvoice', array(
			'as'   => 'mktmgr-post-report-storeinvoice',
			'uses' => 'ReceivingsMenuController@postStoreInvoice' 
	));

Route::get('/mktmgr/report-costdiscrepancyreport', array(
			'as'   => 'mktmgr-report-costdiscrepancyreport',
			'uses' => 'ReceivingsMenuController@getCostDiscrepancyReport' 
	));
Route::post('/mktmgr/report-post-costdiscrepancyreport', array(
			'as'   => 'mktmgr-post-report-costdiscrepancyreport',
			'uses' => 'ReceivingsMenuController@postCostDiscrepancyReport' 
	));
Route::get('/mktmgr/report-offlinereport', array(
			'as'   => 'mktmgr-report-offlinereport',
			'uses' => 'ReceivingsMenuController@getOfflineReport' 
	));

Route::post('/mktmgr/report-post-offlinereport', array(
			'as'   => 'mktmgr-post-report-offlinereport',
			'uses' => 'ReceivingsMenuController@postOfflineReport' 
	));

Route::get('/mktmgr/report-browse-offlinereport', array(
			'as'   => 'mktmgr-browse-report-offlinereport',
			'uses' => 'ReceivingsMenuController@browseOfflineReport' 
	));

Route::get('/mktmgr/report-transportlisting', array(
			'as'   => 'mktmgr-report-transportlisting',
			'uses' => 'ReceivingsMenuController@getTransportlistingReport' 
	));

Route::get('/pdf/storeinvoice', array(
			'as'  => 'pdf-storeinvoice',
			'uses' => 'PDFGenerator@getStoreInvoice'
	));

Route::get('/pdf/browsesir', array(
			'as'  => 'pdf-transporterlist',
			'uses' => 'PDFGenerator@getTransporterList'
	));

Route::get('/pdf/costdiscrepancyreport', array(
			'as' =>'pdf-costdiscrepancyreport',
			'uses'  => 'PDFGenerator@getCostDiscrepancyReport'
	));

Route::post('/mktmgr/report-costdiscrepancysearchreport', array(
			'as'   => 'mktmgr-report-costdiscrepancysearchreport',
			'uses' => 'ReceivingsMenuController@postCostDiscrepancySearchReport' 
	));

Route::get('/mktmgr/DEX-invoicedate', array(
			'as'   => 'mktmgr-DEX-invoicedate',
			'uses' => 'ReceivingsMenuController@getDexInvoiceDate' 
	));

Route::post('/mktmgr/Offline-Receiving', array(
			'as'   => 'mktmgr-Offline-Receiving',
			'uses' => 'ReceivingsMenuController@postOfflineReceiving' 
	)); 

Route::post('/mktmgr/DEX-post-invoicedate', array(
			'as'   => 'mktmgr-post-DEX-invoicedate',
			'uses' => 'ReceivingsMenuController@postDexInvoiceDate' 
	));

Route::get('/mktmgr/browse-vendor', array(
			'as'   => 'mktmgr-browse-vendor',
			'uses' => 'ReceivingsMenuController@browseVendor' 
	));

Route::get('/pdf/htmltopdfview',array(
	        'as'=>'pdf-htmltopdfview',
	        'uses'=>'PDFGenerator@htmltopdfview'
	));

Route::any('/pdf/queryresultadvance', array(
			'as' =>'pdf-queryresultadvance',
			'uses' => 'PDFGenerator@queryresultadvance'
	));

Route::any('/pdf/offlinereport', array(
			'as' =>'pdf-offlinereport',
			'uses' => 'PDFGenerator@offlinereport'
	));

Route::any('/mktmgr/Query-Receiving', array(
			'as'   => 'mktmgr-Query-Receiving',
			'uses' => 'ReceivingsMenuController@postQueryReceiving' 
	)); 
Route::any('/mktmgr/post-queryresult', array(
			'as'   => 'mktmgr-post-queryresult',
			'uses' => 'ReceivingsMenuController@postQueryReceiving' 
		   ));
Route::any('/mktmgr/post-queryresultadvance', array(
			'as'   => 'mktmgr-post-queryresultadvance',
			'uses' => 'ReceivingsMenuController@postQueryReceivingAdvance' 
		   ));
Route::get('/mktmgr/get-itemsbyid', array(
			'as'   => 'mktmgr-get-itemsbyid',
			'uses' => 'ReceivingsMenuController@getItemsById' 
		   ));
Route::any('mktmgr/view-seletedrow', array(
			'as'   => 'mktmgr-view-seletedrow',
			'uses' => 'ReceivingsMenuController@getAllQueryInformation'
	));


// End of shrinkcapturereview menu list


// Actsup Route Starts - Sravan


Route::post('/mktmgr/unlocktable', array(
			'as'   => 'actsup-unlocktable',
			'uses' => 'ActSupController@unlockTableforDate'
	));

// Actsup Route Starts

// Start of Fuel Routes - Sravan

Route::get('/mktmgr/deliveryadd_update', array(
			'as'   => 'bookspro-deliveryadd_update',
			'uses' => 'BooksProFuelController@deliveryAddUpdate'
	));
Route::post('/mktmgr/post-deliveryadd_update', array(
			'as'   => 'bookspro-post-deliveryadd_update',
			'uses' => 'BooksProFuelController@postdeliveryAddUpdate'
	));
Route::post('/mktmgr/post-data-deliveryadd_update', array(
			'as'   => 'bookspro-post-data-deliveryadd_update',
			'uses' => 'BooksProFuelController@postDatadeliveryAddUpdate'
	));
Route::post('/mktmgr/post-data-salesadd_update', array(
			'as'   => 'bookspro-post-data-salesadd_update',
			'uses' => 'BooksProFuelController@postDataSalesAddUpdate'
	));
Route::get('/mktmgr/deliverylist', array(
			'as'   => 'bookspro-deliverylist',
			'uses' => 'BooksProFuelController@getdeliveryList'
	));
Route::get('/mktmgr/saleslist', array(
			'as'   => 'bookspro-saleslist',
			'uses' => 'BooksProFuelController@getSalesList'
	));
Route::get('/mktmgr/salesdate', array(
			'as'   => 'bookspro-salesdate',
			'uses' => 'BooksProFuelController@getSalesDate'
	));
Route::post('/mktmgr/post-salesdate', array(
			'as'   => 'bookspro-post-salesdate',
			'uses' => 'BooksProFuelController@postSalesDate'
	));
Route::get('/mktmgr/endinginvlist', array(
			'as'   => 'bookspro-endinginvlist',
			'uses' => 'BooksProFuelController@getEndingInvList'
	));
Route::get('/mktmgr/endinvdate', array(
			'as'   => 'bookspro-endinvdate',
			'uses' => 'BooksProFuelController@getEndInvDate'
	));
Route::post('/mktmgr/post-endinvdate', array(
			'as'   => 'bookspro-post-endinvdate',
			'uses' => 'BooksProFuelController@postEndInvDate'
	));
Route::post('/mktmgr/post-data-endinvadd_update', array(
			'as'   => 'bookspro-post-data-endinvadd_update',
			'uses' => 'BooksProFuelController@postDataEndInvAddUpdate'
	));
Route::get('/mktmgr/dailyreportdate', array(
			'as'   => 'bookspro-dailyreportdate',
			'uses' => 'BooksProFuelController@getDailyReportDate'
	));
Route::post('/mktmgr/post-dailyreportdate', array(
			'as'   => 'bookspro-post-dailyreportdate',
			'uses' => 'BooksProFuelController@postDailyReportDate'
	));
Route::get('/mktmgr/fuel_weekly_report_date', array(
			'as'   => 'bookspro-fuel_weekly_report_date',
			'uses' => 'BooksProFuelController@getFuelReportWeeklyDate'
	));
Route::post('/mktmgr/post-fuel_weekly_report_date', array(
			'as'   => 'bookspro-post-fuel_weekly_report_date',
			'uses' => 'BooksProFuelController@postFuelReportWeeklyDate'
	));

// End of Fuel Routes - Sravan

// Start of BooksPro Safe Routes - Sravan
Route::get('/mktmgr/safecount', array(
			'as'   => 'bookspro-safecount',
			'uses' => 'BookSafeController@getSafeCount'
	));
Route::post('/mktmgr/postsafecount', array(
			'as'   => 'bookspro-postsafecount',
			'uses' => 'BookSafeController@postSafeCount'
	));
Route::get('/mktmgr/safecountprintdate', array(
			'as'   => 'bookspro-safecountprintdate',
			'uses' => 'BookSafeController@getSafeCountPrintDate'
	));
Route::post('/mktmgr/postsafecountprint', array(
			'as'   => 'bookspro-postsafecountprint',
			'uses' => 'BookSafeController@postSafeCountPrint'
	));
Route::get('/mktmgr/safecoin', array(
		'as'   => 'bookspro-safecoin',
		'uses' => 'BookSafeController@getSafeCoin'		
	));
Route::post('/mktmgr/safecoinsubmit', array(
		'as'   => 'bookspro-safecoinsubmit',
		'uses' => 'BookSafeController@SafeCoinSubmit'		
	));
Route::post('/mktmgr/safecurrencysubmit', array(
		'as'   => 'bookspro-safecurrencysubmit',
		'uses' => 'BookSafeController@SafeCurrencySubmit'		
	));
Route::post('/mktmgr/safeothersubmit', array(
		'as'   => 'bookspro-safeothersubmit',
		'uses' => 'BookSafeController@SafeOtherSubmit'		
	));

Route::post('/mktmgr/safecountpostdata', array(
		'as'   => 'bookspro-safecountpostdata',
		'uses' => 'BookSafeController@postSafeCountPostData'		
	));
Route::post('/mktmgr/stboxsubmit', array(
		'as'   => 'bookspro-stboxsubmit',
		'uses' => 'BookSafeController@stBoxSubmit'		
	));
Route::post('/mktmgr/potboxsubmit', array(
		'as'   => 'bookspro-potboxsubmit',
		'uses' => 'BookSafeController@potBoxSubmit'		
	));
Route::post('/mktmgr/depositsboxsubmit', array(
		'as'   => 'bookspro-depositsboxsubmit',
		'uses' => 'BookSafeController@depositsBoxSubmit'		
	));
Route::post('/mktmgr/deliveriesboxsubmit', array(
		'as'   => 'bookspro-deliveriesboxsubmit',
		'uses' => 'BookSafeController@deliveriesBoxSubmit'		
	));
Route::post('/mktmgr/transfersboxsubmit', array(
		'as'   => 'bookspro-transfersboxsubmit',
		'uses' => 'BookSafeController@transfersBoxSubmit'		
	));

Route::get('/mktmgr/safereportdate', array(
			'as'   => 'bookspro-safereportdate',
			'uses' => 'BookSafeController@getSafeReportDate'
	));
Route::post('/mktmgr/postsafereport', array(
			'as'   => 'bookspro-postsafereport',
			'uses' => 'BookSafeController@postSafeReport'
	));
Route::get('/mktmgr/safereportprintdate', array(
			'as'   => 'bookspro-safereportprintdate',
			'uses' => 'BookSafeController@getSafeReportPrintDate'
	));
Route::post('/mktmgr/postsafereportprintdata', array(
			'as'   => 'bookspro-postsafereportprintdata',
			'uses' => 'BookSafeController@postSafeReportPrintData'
	));
// End of BooksPro Safe Routes - Sravan

// Starts BooksPro Mgmt Routes
Route::get('/mktmgr/mgmtdailydate', array(
			'as'   => 'bookspro-mgmtdailydate',
			'uses' => 'BookMgmtController@getMgmtDailyDate'
	));
Route::post('/mktmgr/postmgmtdaily', array(
			'as'   => 'bookspro-post-mgmtdaily',
			'uses' => 'BookMgmtController@postMgmtDailyDate'
	));
Route::post('/mktmgr/checkmgmtapproval', array(
			'as'   => 'checkmgmtapproval',
			'uses' => 'BookMgmtController@checkMgmtApproval'
	));
Route::post('/mktmgr/checkholiday', array(
			'as'   => 'checkholiday',
			'uses' => 'BookMgmtController@skipHoliday'
	));
	Route::get('/mktmgr/mgmtweeklydate', array(
			'as'   => 'bookspro-mgmtweeklydate',
			'uses' => 'BookMgmtController@getMgmtWeeklyDate'
	));
	Route::post('/mktmgr/postmgmtweekly', array(
			'as'   => 'bookspro-post-mgmtweekly',
			'uses' => 'BookMgmtController@postMgmtWeeklyDate'
	));
	Route::post('/mktmgr/checkmgmtweeklyapproval', array(
			'as'   => 'checkmgmtweeklyapproval',
			'uses' => 'BookMgmtController@checkMgmtWeeklyApproval'
	));
	Route::post('/mktmgr/checkmgmtweeklyapprovalprint', array(
			'as'   => 'checkmgmtweeklyapprovalprint',
			'uses' => 'BookMgmtController@checkmgmtweeklyapprovalprint'
	));
// Ends BooksPro Mgmt Routes

// Start of nonperishableinvprepview menu list

Route::any('/pdf/report-nonperishable', array(
			'as' => 'pdf-report-nonperishable',
			'uses' => 'PDFGenerator@getPdfReportNonPar'
	));
Route::any('/pdf/report-nonperishable-department', array(
			'as' => 'pdf-report-nonpar-department',
			'uses' => 'PDFGenerator@getPdfReportNonParDept'
	));
Route::any('/pdf/report-nonperishable-scan', array(
			'as' => 'pdf-report-nonpar-scan',
			'uses' => 'PDFGenerator@getPdfReportNonParScan'
	));

Route::get('/mktmgr/nonperishableinvprepview', array(
			'as'   => 'mktmgr-nonperishableinvprepview',
			'uses' => 'NonPerishableMenuController@nonPerishableInvPrepview'
	));

Route::get('/mktmgr/noninvdeptinfo/{deptno}', array(
			'as'	=>'mktmgr-noninvdeptinfo',
			'uses'  =>'NonPerishableMenuController@getnonPerishableInvDeptInfo'
	));
Route::get('/mktmgr/msg', array(
			'as'	=>'mktmgr-recapmsg',
			'uses'  =>'NonPerishableMenuController@getrecapMsg'
	));

Route::get('/mktmgr/noninvshowdetail/', array(
			'as'	=>'mktmgr-noninvshowdetail',
			'uses'  =>'NonPerishableMenuController@getNonInvShowDetail'
	));
Route::get('/mktmgr/noninvapproveinv/', array(
			'as'	=>'mktmgr-noninvapproveinv',
			'uses'  =>'NonPerishableMenuController@getNonApproveInv'
	));

Route::get('/mktmgr/noninvvoidinv/', array(
			'as'	=>'mktmgr-noninvvoidinv',
			'uses'  =>'NonPerishableMenuController@getNonVoidInv'
	));



Route::get('/mktmgr/nonmoreinfoseldate/', array(
			'as'	=>'mktmgr-nonmoreinfoseldate',
			'uses'  =>'NonPerishableMenuController@getNonMoreInfoSelDate'
	));
Route::get('/mktmgr/noninvitemdetail/', array(
			'as'	=>'mktmgr-noninvitemdetail',
			'uses'  =>'NonPerishableMenuController@getNonInvItemDetail'
	));

// End of nonperishableinvprepview menu list



/// start of store order menu list 
Route::any('/pdf/report-plus-out-items', array(
			'as' => 'pdf-report-plusoutitems',
			'uses' => 'PDFGenerator@getPdfPlusOutItems'
	));
Route::any('/pdf/report-print-order-details', array(
			'as' => 'pdf-report-printorderdetails',
			'uses' => 'PDFGenerator@getPdfPrintOrderDetails'
	));
Route::any('/pdf/report-plusout', array(
			'as' => 'pdf-report-plusout',
			'uses' => 'PDFGenerator@getPdfPlusOut'
	));

Route::any('/pdf/report-store-order-query', array(
			'as' => 'pdf-report-storeorderquery',
			'uses' => 'PDFGenerator@getPdfStoreOrderQuery'
	));
Route::get('/mktmgr/storeorderview', array(
			'as'   => 'mktmgr-storeorderview',
			'uses' => 'StoreOrderViewController@storeOrderview'
	));

Route::get('/mktmgr/storetransfersview', array(
			'as'	=> 'mktmgr-storetransfersview',
			'uses'  => 'StoreTransfersControler@storeTransfersview'
	));

Route::get('/mktmgr/aditemprint', array(
		'as'   => 'mktmgr-aditemprint',
		'uses' => 'StoreOrderViewController@getAdItemPrint'
	));

Route::get('/mktmgr/aditemordering', array(
		'as'   => 'mktmgr-aditemordering',
		'uses' => 'StoreOrderViewController@getAdItemOrdering'
	));
Route::post('/mktmgr/select_order_type', array(
		'as'   => 'select_order_type',
		'uses' => 'StoreOrderViewController@selectOrderType'
	));
Route::post('/mktmgr/post-store-order-weekly', array(
		'as'   => 'post-store-order-weekly',
		'uses' => 'StoreOrderViewController@postStoreOrderWeekly'
	));
Route::post('/mktmgr/dsd-item-ad', array(
		'as'   => 'post-dsd-item-ad',
		'uses' => 'StoreOrderViewController@postdsditemad'
	));
Route::get('/mktmgr/aditemrelease', array(
		'as'   => 'mktmgr-aditemrelease',
		'uses' => 'StoreOrderViewController@getAdItemRelease'
	));

Route::get('/mktmgr/aditemdsd', array(
		'as'   => 'mktmgr-aditemdsd',
		'uses' => 'StoreOrderViewController@getAdItemDsd'
	));

Route::get('/mktmgr/codeditemquery', array(
		'as'   => 'mktmgr-codeditemquery',
		'uses' => 'StoreOrderViewController@getCodedItemQuery'
	));

Route::get('/mktmgr/codeditembrowser', array(
		'as'   => 'mktmgr-codeditembrowser',
		'uses' => 'StoreOrderViewController@getCodedItemBrowse'
	));

Route::get('/mktmgr/editwarninglimits', array(
		'as'   => 'mktmgr-editwarninglimits',
		'uses' => 'StoreOrderViewController@getEditWarningLimits'
	));
Route::post('/mktmgr/posteditwarninglimits', array(
		'as'   => 'mktmgr-post-edit-warning-limits',
		'uses' => 'StoreOrderViewController@postEditWarningLimits'
	));
Route::any('/mktmgr/posteditwarninglimitsdata', array(
		'as'   => 'mktmgr-post-edit-warning-limits-data',
		'uses' => 'StoreOrderViewController@postEditWarningLimitsData'
	));

Route::get('/mktmgr/storeholidayorders', array(
		'as'   => 'mktmgr-storeholidayorders',
		'uses' => 'StoreOrderViewController@getStoreHolidayOrders'		
	));

Route::get('/mktmgr/outofstockquery', array(
		'as'   => 'mktmgr-outofstockquery',
		'uses' => 'StoreOrderViewController@getOutOfStockQuery'		
	));

Route::get('/mktmgr/outofstockbrowse', array(
		'as'   => 'mktmgr-outofstockbrowse',
		'uses' => 'StoreOrderViewController@getOutOfStockBrowse'		
	));
Route::get('/mktmgr/outofstockbrowsemsg', array(
		'as'   => 'mktmgr-outofstockbrowsemsg',
		'uses' => 'StoreOrderViewController@getOutOfStockBrowsemsg'		
	));
Route::post('/mktmgr/postoutofstock', array(
		'as'   => 'mktmgr-post-outofstock',
		'uses' => 'StoreOrderViewController@postOutOfStock'		
	));

Route::get('/mktmgr/plusoutbrowse', array(
		'as'   => 'mktmgr-plusoutbrowse',
		'uses' => 'StoreOrderViewController@getPlusOutBrowse'		
	));
Route::get('/mktmgr/plusoutsitems', array(
		'as'   => 'mktmgr-plusoutsitems',
		'uses' => 'StoreOrderViewController@getPlusOutItems'		
	));

Route::get('/mktmgr/plusoutitem', array(
		'as'   => 'mktmgr-plusoutitem',
		'uses' => 'StoreOrderViewController@getPlusOutItem'		
	));

Route::get('/mktmgr/printorderquery', array(
		'as'   => 'mktmgr-printorderquery',
		'uses' => 'StoreOrderViewController@getPrintOrderQuery'		
	));

Route::get('/mktmgr/printorderbrowser', array(
		'as'   => 'mktmgr-printorderbrowser',
		'uses' => 'StoreOrderViewController@getPrintOrderBrowse'		
	));
Route::get('/mktmgr/printorderbrowsermsg', array(
		'as'   => 'mktmgr-printorderbrowsermsg',
		'uses' => 'StoreOrderViewController@printorderbrowsermsg'		
	));
Route::get('/mktmgr/printorderdetails', array(
		'as'   => 'mktmgr-printorderdetails',
		'uses' => 'StoreOrderViewController@getPrintOrderdetails'		
	));
Route::get('/mktmgr/printorderdetailsmsg', array(
		'as'   => 'mktmgr-printorderdetailsmsg',
		'uses' => 'StoreOrderViewController@printorderbrowsermsg'		
	));
Route::post('/mktmgr/post-order-query', array(
		'as'   => 'mktmgr-post-order-query',
		'uses' => 'StoreOrderViewController@postPrintOrderdetails'		
	));

Route::get('/mktmgr/storetransfersquery', array(
		'as'   => 'mktmgr-storetransfersquery',
		'uses' => 'StoreTransfersControler@getStoreTransfersQuery'		
	));

Route::get('/mktmgr/storetransfersbrowse', array(
		'as'   => 'mktmgr-storetransfersbrowse',
		'uses' => 'StoreTransfersControler@getStoreTransfersBrowse'		
	));

Route::get('/mktmgr/storetransfersupdate', array(
		'as'   => 'mktmgr-storetransfersupdate',
		'uses' => 'StoreTransfersControler@getStoreTransfersUpdate'		
	));

Route::get('/mktmgr/storetransfersitems', array(
		'as'   => 'mktmgr-storetransfersitems',
		'uses' => 'StoreTransfersControler@getStoreTransfersItems'		
	));

Route::get('/mktmgr/storetransfersoperators', array(
		'as'   => 'mktmgr-storetransfersoperators',
		'uses' => 'StoreTransfersControler@getStoreTransfersOperators'		
	));
Route::get('/mktmgr/storetransferweeklyreport', array(
		'as'   => 'mktmgr-storetransferweeklyreport',
		'uses' => 'StoreTransfersControler@getStoreTransfersWeeklyreport'		
	));
Route::post('/mktmgr/poststoreweeklyreport', array(
		'as'   => 'post-store-weekly-acc-report',
		'uses' => 'StoreTransfersControler@postStoreWeeklyRepot'		
	));

Route::any('/pdf/poststoreweeklyreport', array(
			'as' => 'pdf-poststoreweeklyreport',
			'uses' => 'PDFGenerator@getPdfStoreWeeklyRepot'
	));

Route::any('/pdf/poststoreweeklystatusreport', array(
			'as' => 'pdf-poststoreweeklystatusreport',
			'uses' => 'PDFGenerator@getPdfStoreWeeklyStatusRepot'
	));

Route::get('/mktmgr/storetransferhardcopy', array(
		'as'   => 'mktmgr-storetransferhardcopy',
		'uses' => 'StoreTransfersControler@getStoreTransferHardcopy'		
	));

Route::get('/mktmgr/storetransfersweeklystatus', array(
		'as'   => 'mktmgr-storetransfersweeklystatus',
		'uses' => 'StoreTransfersControler@getStoreTransfersWeeklyStatus'		
	));

Route::post('/mktmgr/postweeklytrnsreport', array(
		'as'  => 'post-weekly-trns-report',
		'uses' => 'StoreTransfersControler@postdateWeeklyTrans'
	));
Route::get('/mktmgr/storetransfersoperators', array(
		'as'   => 'mktmgr-storetransfersoperators',
		'uses' => 'StoreTransfersControler@getStoreTransfersOperators'		
	));
Route::any('/mktmgr/post-store-transfer', array(
		'as'   => 'mktmgr-post-store-transfer',
		'uses' => 'StoreTransfersControler@postStoreTransfer'		
	));
Route::get('/mktmgr/storetransfersitemsmsg', array(
		'as'   => 'mktmgr-storetransfersitemsmsg',
		'uses' => 'StoreTransfersControler@StoreTransferItemMsg'		
	));
Route::get('/mktmgr/storetransfersupdatemsg', array(
		'as'   => 'mktmgr-storetransfersupdatemsg',
		'uses' => 'StoreTransfersControler@StoreTransferUpdateMsg'		
	));
Route::any('/mktmgr/post-store-item-view', array(
		'as'   => 'mktmgr-store-item-no',
		'uses' => 'StoreTransfersControler@postStoreItemView'		
	));
// Route::get('/mktmgr/store-transfer-update', array(
// 		'as'   => 'mktmgr-store-transfer-update',
// 		'uses' => 'StoreTransfersControler@StoreTransferUpdate'		
// 	));
Route::get('/mktmgr/store-transfer-update', array(
		'as'   => 'mktmgr-store-transfer-update',
		'uses' => 'StoreTransfersControler@StoreTransferUpdate'		
	));
Route::post('/mktmgr/post-store-transfer-update', array(
		'as'   => 'mktmgr-post-store-transfer-update',
		'uses' => 'StoreTransfersControler@postStoreTransferUpdate'		
	));
Route::any('/pdf/store-transfer-hardcopy-report', array(
			'as' => 'pdf-store-transfer-hardcopy-report',
			'uses' => 'PDFGenerator@StoreTransferHardCopyReport'
	));


Route::get('/sign1/signs', array(
		'as'   => 'sign1-signs',
		'uses' => 'SignsController@getSigns'		
	));



// start of the user socscn01

Route::get('/socscn01/socdeptfunctions', array(
		'as'   => 'socscn01-socdeptfunctions',
		'uses' => 'UserSocscn01Controller@getsocDeptfunctions'		
	));

Route::get('/socscn01/socreports', array(
		'as'   => 'socscn01-socreports',
		'uses' => 'UserSocscn01Controller@getsocReports'		
	));

Route::get('/socscn01/socdsdreceiving', array(
		'as'   => 'socscn01-socdsdreceiving',
		'uses' => 'UserSocscn01Controller@getsocdsdReceiving'		
	));

Route::get('/socscn01/socretailpricechk', array(
		'as'   => 'socscn01-socretailpricechk',
		'uses' => 'UserSocscn01Controller@getsocRetailpricechk'		
	));
Route::get('/socscn01/socqueryitem', array(
		'as'   => 'socscn01-socqueryitem',
		'uses' => 'UserSocscn01Controller@getsocQueryitem'		
	));
Route::get('/socscn01/socstoretransfer', array(
		'as'   => 'socscn01-socstoretransfer',
		'uses' => 'UserSocscn01Controller@getsocStoretransfer'		
	));
Route::get('/socscn01/socpricesaleschk', array(
		'as'   => 'socscn01-socpricesaleschk',
		'uses' => 'UserSocscn01Controller@getsocPricesaleschk'		
	));
Route::get('/socscn01/returnhome', array(
		'as'   => 'socscn01-returnhome',
		'uses' => 'UserSocscn01Controller@getReturnhome'		
	));
// end of the user socscn01

//start of  user slcadm03
Route::get('/slcadm03/returnhome', array(
			'as'	=>'slcadm03-returnhome',
			'uses'  =>'UserSlcadm03Controller@getReturnHome'
	));
Route::post('/slcadm03/store-option', array(
			'as'	=>'slcadm03-store-option',
			'uses'  =>'UserSlcadm03Controller@getStoreNo'
	));

Route::any('/slcadm03/batchdisplay', array(
			'as'	=>'slcadm03-displayallbatch',
			'uses'  =>'UserSlcadm03Controller@getDisplayAllBatch'
	));
Route::any('/slcadm03/updatebatchdetails', array(
			'as'	=>'updatebatchdetails',
			'uses'  =>'UserSlcadm03Controller@updateBatchDetails'
	));
Route::any('/slcadm03/batch-display-detail', array(
			'as'   => 'slcadm03-batch-display-detail',
			'uses' => 'UserSlcadm03Controller@getBatchDisplayDetail'
	));
Route::any('/slcadm03/slcadm03-print-details', array(
			'as'   => 'slcadm03-print-details',
			'uses' => 'UserSlcadm03Controller@printBatchDetail'
	));


Route::get('/slcadm03/emailhelpdesk', array(
			'as'   => 'slcadm03-emailhelpdesk',
			'uses' => 'UserSlcadm03Controller@getEmailhelpdesk'
	));

Route::get('/slcadm03/displaybatch', array(
			'as'   => 'slcadm03-displaybatch',
			'uses' => 'UserSlcadm03Controller@getDisplayBatch'
	));
Route::get('/slcadm03/batch-sign-tag-counts', array(
			'as'   => 'slcadm03-batch-sign-tag-counts',
			'uses' => 'UserSlcadm03Controller@batchSignTagCounts'
	));
Route::get('/slcadm03/view-batch-details', array(
			'as'   => 'slcadm03-view-batch-details',
			'uses' => 'UserSlcadm03Controller@viewBatchDetails'
	));
Route::get('/slcadm03/batch-department-details', array(
			'as'   => 'slcadm03-batch-department-details',
			'uses' => 'UserSlcadm03Controller@batchDepartmentDetails'
	));

Route::get('/slcadm03/batchtags', array(
			'as'   => 'slcadm03-batchtags',
			'uses' => 'UserSlcadm03Controller@getBatchTags'
	));

Route::get('/slcadm03/batchpos', array(
			'as'   => 'slcadm03-batchpos',
			'uses' => 'UserSlcadm03Controller@getBatchPos'
	));
Route::get('/slcadm03/reprinttags', array(
			'as'   => 'slcadm03-reprinttags',
			'uses' => 'UserSlcadm03Controller@getReprintTags'
	));

Route::get('/slcadm03/summarypage', array(
			'as'   => 'slcadm03-summarypage',
			'uses' => 'UserSlcadm03Controller@getSummaryPage'
	));
Route::get('/slcadm03/restorepage', array(
			'as'   => 'slcadm03-restorepage',
			'uses' => 'UserSlcadm03Controller@getRestorePage'
	));
Route::get('/slcadm03/displayreportcode', array(
			'as'   => 'slcadm03-displayreportcode',
			'uses' => 'UserSlcadm03Controller@getDisplayReportCode'
	));
Route::get('/slcadm03/slitem', array(
			'as'   => 'slcadm03-slitem',
			'uses' => 'UserSlcadm03Controller@getslItem'
	));
Route::get('/slcadm03/generatefile', array(
			'as'   => 'slcadm03-generatefile',
			'uses' => 'UserSlcadm03Controller@generateFile'
	));
Route::get('/slcadm03/itemlookup', array(
			'as'   => 'slcadm03-itemlookup',
			'uses' => 'UserSlcadm03Controller@getItemLookup'
	));
Route::get('/slcadm03/slcadm03_prnt_sh_file','UserSlcadm03Controller@slcadm03_prnt_sh_file');


Route::get('/slcadm03/slcadm03_update_tag_print','UserSlcadm03Controller@printUpdateTagprint'
	);
//end of user slcadm03

// Start for slcscno1 user //

Route::get('/slcscn01/slcslic', array(
		'as'   => 'slcscn01-slcslic',
		'uses' => 'UserSlcscn01Controller@getslcSlic'		
	));
Route::get('/slcscn01/slcaislescan', array(
		'as'   => 'slcscn01-slcaislescan',
		'uses' => 'UserSlcscn01Controller@getslcAislescan'		
	));
Route::get('/slcscn01/slcqueryitem', array(
		'as'   => 'slcscn01-slcqueryitem',
		'uses' => 'UserSlcscn01Controller@getslcQueryitem'		
	));

Route::get('/slcscn01/slctagupdtqry', array(
		'as'   => 'slcscn01-slctagupdtqry',
		'uses' => 'UserSlcscn01Controller@getslcTagupdtqry'		
	));
Route::get('/slcscn01/slcapplybatch', array(
		'as'   => 'slcscn01-slcapplybatch',
		'uses' => 'UserSlcscn01Controller@getslcApplybatch'		
	));
// End for slcscno1 user //


//start of user bkpfst //

Route::get('/bkpfst/returnhome', array(
		'as' => 'bkpfst-returnhome',
		'uses' => 'UserBkpfstController@getBackHome'
	));

Route::get('/bkpfst/bookkeeper1st', array(
		'as' => 'bkpfst-bookkeeper1st',
		'uses' => 'UserBkpfstController@getBookkeeper1st'
	));
Route::get('/bkpfst/bkpfstfsaisle1', array(
			'as'   => 'bkpfst-bkpfstfsaisle1',
			'uses' => 'BkpfstController@getFsaisle1'
	));
Route::get('/bkpfst/bkpfstfsmainstore', array(
			'as'   => 'bkpfst-bkpfstfsmainstore',
			'uses' => 'BkpfstController@getFsmainstore'
	));
Route::get('/bkpfst/bkpfstlogintoaisle1', array(
			'as'   => 'bkpfst-bkpfstlogintoaisle1',
			'uses' => 'BkpfstController@getLogintoaisle1'
	));
Route::get('/bkpfst/bkpfstreceivings', array(
			'as'   => 'bkpfst-bkpfstreceivings',
			'uses' => 'BkpfstController@getReceivings'
	));

//end of user bkpfst //
//****************admin start ***********************
//Route::get('/admin', array(
//			'as'=> 'admin',
//			'uses'=> 'AdminController@admin'
//		));
		
	//Admin panel pages 
Route::get('/admin/create', array(
			'as'=> 'admin-create',
			'uses'=> 'AdminController@getCreateadmin'
		));
Route::get('/admin/viewusers', array(
			'as'=> 'admin-viewusers',
			'uses'=> 'AdminController@getviewusers'
		));
Route::post('/admin/deleteuser', array(
		'as'=> 'admin-deleteuser',
			'uses'=> 'AdminController@postDeleteUser'	
	));
Route::get('/admin/forgotpassword', array(
			'as'=> 'admin-forgotpassword',
			'uses'=> 'AdminController@getForgotpassword'
		));	
//Route::post('/admin/sign-post', array(
//			'as'=> 'admin-sign-post',
//			'uses'=> 'AdminController@AdminpostSignIn'
//		));
Route::post('/admin/create-post', array(
			'as'=> 'admin-create-post',
			'uses'=> 'AdminController@UserCreate'
		));
		
		Route::get('/admin/admregistration', array(
			'as'=> 'admin-admregistration',
			'uses'=> 'AdminController@Admincreate'
		));
		Route::get('/admin/userregistration', array(
			'as'=> 'admin-userregistration',
			'uses'=> 'AdminController@Userscreate'
		));
 Route::get('/admin/sign-out', array(
        'as' => 'admin-sign-out',
        'uses' => 'AdminController@getSignOut'
    ));
	Route::get('/admin/edituser', array(
			'as'=> 'admin-edituser',
			'uses'=> 'AdminController@Edituser'
		));
		Route::post('admin/update-register', array(
			'as'=> 'admin-update-register',
			'uses'=> 'AdminController@Updateuser'
		));
		Route::get('admin/home', array(
			'as'=> 'admin-home',
			'uses'=> 'AdminController@Adminhome'
		));

//*********************admin End*********************

//*************** socadm00**************************//

		Route::get('/socadm00/storetransmission', array(
			'as'   => 'socadm00-storetransmission',
			'uses' => 'UserSocadm00Controller@storetransmission'
	));
Route::get('/socadm00/storetransmission-query', array(
			'as'   => 'socadm00-storetransmission-query',
			'uses' => 'UserSocadm00Controller@getStoreTransmissionQuery'
	));
	
Route::get('/socadm00/storetransmission-browser', array(
			'as'   => 'socadm00-storetransmission-browser',
			'uses' => 'UserSocadm00Controller@getStoreTransmissionBrowser'
	));
Route::get('/socadm00/storetransmission-add', array(
			'as'   => 'socadm00-storetransmission-add',
			'uses' => 'UserSocadm00Controller@getStoreTransmissionAdd'
	));
Route::post('/socadm00/ws_func_desc', array(
			'as'   => 'ws_func_desc',
			'uses' => 'UserSocadm00Controller@getFunctionDesc'
	));
Route::post('/socadm00/post-storetransmissionadd', array(
			'as'   => 'socadm00-post-storetransmissionadd',
			'uses' => 'UserSocadm00Controller@postStoreTransmissionAdd'
	));
Route::post('/socadm00/storetransmission-update', array(
			'as'   => 'socadm00-storetransmission-update',
			'uses' => 'UserSocadm00Controller@getStoreTransmissionUpdate'
	));
Route::get('/socadm00/storetransmission-updatemsg', array(
			'as'   => 'socadm00-storetransmission-updatemsg',
			'uses' => 'UserSocadm00Controller@getStoreTransmissionUpdatemsg'
	));

Route::get('/socadm00/storetransconf-query', array(
			'as'   => 'socadm00-storetransconf-query',
			'uses' => 'UserSocadm00Controller@getStoreTransConfQuery'
	));
Route::get('/socadm00/storetransconfmsg', array(
			'as'   => 'socadm00-storetransfun-browsermsg',
			'uses' => 'UserSocadm00Controller@getStoreTransConfQueryMsg'
	));
Route::get('/socadm00/storetransconfmsg', array(
			'as'   => 'socadm00-storetransfun-updatemsg',
			'uses' => 'UserSocadm00Controller@getStoreTransConfQueryMsg'
	));
Route::any('/socadm00/post-storetransconfquery', array(
			'as'   => 'socadm00-post-storetransconfquery',
			'uses' => 'UserSocadm00Controller@postStoreTransConfQuery'
	));
Route::any('/pdf/report-storetransconf-query', array(
			'as' => 'pdf-report-storetransconf-query',
			'uses' => 'PDFGenerator@getPdfstoretransconf'
	));

Route::get('/socadm00/storetransconf-browser', array(
			'as'   => 'socadm00-storetransconf-browser',
		'uses' => 'UserSocadm00Controller@getStoreTransConfBrowser'
	));
Route::get('/socadm00/storetransconf-browsermsg', array(
			'as'   => 'socadm00-storetransconf-browsermsg',
		'uses' => 'UserSocadm00Controller@getStoreTransConfBrowsermsg'
	));
Route::get('/socadm00/config-update', array(
			'as'   => 'socadm00-config-update',
		'uses' => 'UserSocadm00Controller@StoreTransConfUpdate'
	));
Route::post('/socadm00/post-storetransconf-update', array(
			'as'   => 'socadm00-post-storetransconf-update',
		'uses' => 'UserSocadm00Controller@postStoreTransConfUpdate'
	));
Route::get('/socadm00/storetransconf-add', array(
			'as'   => 'socadm00-storetransconf-add',
		'uses' => 'UserSocadm00Controller@getStoreTransConfAdd'
	));
Route::post('/socadm00/post-storetransconf-add', array(
			'as'   => 'socadm00-post-storetransconf-add',
		'uses' => 'UserSocadm00Controller@postStoreTransConfAdd'
	));
Route::get('/socadm00/storetransconf-update', array(
			'as'   => 'socadm00-storetransconf-update',
		'uses' => 'UserSocadm00Controller@getStoreTransConfUpdate'
	));
Route::get('/socadm00/storetransfun-query', array(
			'as'   => 'socadm00-storetransfun-query',
		'uses' => 'UserSocadm00Controller@getStoreTransFunQuery'
	));
Route::any('/socadm00/post-storetransfunquery', array(
			'as'   => 'socadm00-post-storetransfunquery',
		'uses' => 'UserSocadm00Controller@postStoreTransFunQuery'
	));
Route::get('/socadm00/storetransfun-browser', array(
			'as'   => 'socadm00-storetransfun-browser',
		'uses' => 'UserSocadm00Controller@getStoreTransFunBrowser'
	));

Route::get('/socadm00/storetransfun-add', array(
			'as'   => 'socadm00-storetransfun-add',
		'uses' => 'UserSocadm00Controller@getStoreTransFunAdd'
	));
Route::post('/socadm00/storetransfun-add', array(
			'as'   => 'socadm00-post-storetransfunadd',
		'uses' => 'UserSocadm00Controller@postStoreTransFunAdd'
	));
Route::get('/socadm00/storetransfunc-update', array(
			'as'   => 'socadm00-storetransfunc-update',
		'uses' => 'UserSocadm00Controller@StoreTransFunUpdate'
	));
Route::post('/socadm00/post-storetransfunc-update', array(
			'as'   => 'socadm00-post-storetransfuncupdate',
		'uses' => 'UserSocadm00Controller@postStoreTransFunUpdate'
	));
Route::get('/socadm00/storetransfun-update', array(
			'as'   => 'socadm00-storetransfun-update',
		'uses' => 'UserSocadm00Controller@getStoreTransFunUpdate'
	));
Route::get('/socadm00/storeold-menu-query', array(
			'as'   => 'socadm00-storeold-menu-query',
		'uses' => 'UserSocadm00Controller@getStoreOldMenuQuery'
	));
Route::get('/socadm00/storeold-menu-query-msg', array(
			'as'   => 'socadm00-storeold-menu-browsermsg',
		'uses' => 'UserSocadm00Controller@getStoreOldMenuQueryMsg'
	));
Route::get('/socadm00/storeold-menu-query-msg', array(
			'as'   => 'socadm00-storeold-menu-updatemsg',
		'uses' => 'UserSocadm00Controller@getStoreOldMenuQueryMsg'
	));
Route::any('/socadm00/post-storeold-menu-query', array(
			'as'   => 'socadm00-post-storeoldmenuquery',
		'uses' => 'UserSocadm00Controller@postStoreOldMenuQuery'
	));
Route::get('/socadm00/storeold-menu-browser', array(
			'as'   => 'socadm00-storeold-menu-browser',
		'uses' => 'UserSocadm00Controller@getStoreOldMenuBrowser'
	));
Route::get('/socadm00/storeold-menu-add', array(
			'as'   => 'socadm00-storeold-menu-add',
		'uses' => 'UserSocadm00Controller@getStoreOldMenuAdd'
	));
Route::post('/socadm00/post-storeoldmenuupdate', array(
			'as'   => 'socadm00-post-storeoldmenuupdate',
		'uses' => 'UserSocadm00Controller@postStoreOldMenuUpdate'
	));
Route::post('/socadm00/post-storeold-menu-add', array(
			'as'   => 'socadm00-post-storeoldmenuadd',
		'uses' => 'UserSocadm00Controller@postStoreOldMenuAdd'
	));
Route::get('/socadm00/storeold-menu-update', array(
			'as'   => 'socadm00-storeoldmenu-update',
		'uses' => 'UserSocadm00Controller@getStoreOldMenuUpdate'
	));
Route::get('/socadm00/storeold-menu-update', array(
			'as'   => 'socadm00-storeold-menu-update',
		'uses' => 'UserSocadm00Controller@getStoreOldMenuUpdate'
	));

Route::get('/socadm00/qtylimits', array(
			'as'   => 'socadm00-Qtylimits',
		'uses' => 'UserSocadm00Controller@getQtyLimits'
	));
Route::post('/socadm00/postqtylimits', array(
		'as'   => 'socadm00-PostQtylimits',
		'uses' => 'UserSocadm00Controller@postQtyLimits'
	));
Route::post('/socadm00/postqtylimits_update', array(
		'as'   => 'socadm00-postqtylimits_update',
		'uses' => 'UserSocadm00Controller@postQtyLimitsUpdate'
	));


Route::get('/socadm00/storeDex-commids-query', array(
			'as'   => 'socadm00-storeDex-commids-query',
		'uses' => 'UserSocadm00Controller@getStoreDexCommidsQuery'
	));
Route::any('/socadm00/post-storeDex-commids-query', array(
			'as'   => 'socadm00-post-storedexcommidsquery',
		'uses' => 'UserSocadm00Controller@postStoreDexCommidsQuery'
	));
Route::get('/socadm00/storedexcommids-update', array(
			'as'   => 'socadm00-storedexcommids-update',
		'uses' => 'UserSocadm00Controller@StoreDexCommidsUpdate'
	));
Route::post('/socadm00/post-storedexcommidsupdate', array(
			'as'   => 'socadm00-post-storedexcommidsupdate',
		'uses' => 'UserSocadm00Controller@postStoreDexCommidsUpdate'
	));
Route::get('/socadm00/storeDex-commids-browser', array(
			'as'   => 'socadm00-storeDex-commids-browser',
		'uses' => 'UserSocadm00Controller@getStoreDexCommidsBrowser'
	));

Route::get('/socadm00/storeDex-commids-update', array(
			'as'   => 'socadm00-storeDex-commids-update',
		'uses' => 'UserSocadm00Controller@getStoreDexCommidsUpdate'
	));
Route::get('/socadm00/storeDex-commids-query-msg', array(
			'as'   => 'socadm00-storeDex-commids-browsermsg',
		'uses' => 'UserSocadm00Controller@getStoreDexCommidsmsg'
	));
Route::get('/socadm00/storeDex-commids-query-msg', array(
			'as'   => 'socadm00-storeDex-commids-updatemsg',
		'uses' => 'UserSocadm00Controller@getStoreDexCommidsmsg'
	));

Route::get('/socadm00/store-schedule-query', array(
			'as'   => 'socadm00-schedule-query',
		'uses' => 'UserSocadm00Controller@getStoreScheduleQuery'
	));
Route::any('/socadm00/post-schedulequery', array(
			'as'   => 'socadm00-post-schedule-query',
		'uses' => 'UserSocadm00Controller@postStoreScheduleQuery'
	));
Route::get('/socadm00/store-schedule-browser', array(
			'as'   => 'socadm00-schedule-browser',
		'uses' => 'UserSocadm00Controller@getStoreScheduleBrowser'
	));

Route::get('/socadm00/store-schedule-add', array(
			'as'   => 'socadm00-schedule-add',
		'uses' => 'UserSocadm00Controller@getStoreScheduleAdd'
	));
Route::post('/socadm00/post-store-schedule-add', array(
			'as'   => 'socadm00-post-storescheduleadd',
		'uses' => 'UserSocadm00Controller@postStoreScheduleAdd'
	));
Route::get('/socadm00/store-schedule-update', array(
			'as'   => 'socadm00-schedule-update',
		'uses' => 'UserSocadm00Controller@getStoreScheduleUpdate'
	));
Route::get('/socadm00/store-schedulebrowse-update', array(
			'as'   => 'socadm00-schedulebrowse-update',
		'uses' => 'UserSocadm00Controller@StoreScheduleUpdateForm'
	));
Route::post('/socadm00/store-post-schedulebrowse-update', array(
			'as'   => 'socadm00-post-storeschedule-update',
		'uses' => 'UserSocadm00Controller@postStoreScheduleUpdateForm'
	));
Route::get('/socadm00/store-schedulequerymsg', array(
			'as'   => 'socadm00-schedule-browsermsg',
		'uses' => 'UserSocadm00Controller@StoreScheduleQueryMsg'
	));
Route::get('/socadm00/store-schedulequerymsg', array(
			'as'   => 'socadm00-schedule-updatemsg',
		'uses' => 'UserSocadm00Controller@StoreScheduleQueryMsg'
	));
Route::any('/socadm00/post-menuquery', array(
			'as'   => 'socadm00-post-menuquery',
		'uses' => 'UserSocadm00Controller@postmenuquery'
	));
Route::get('/socadm00/menuquery-update', array(
			'as'   => 'socadm00-menuquery-update',
		'uses' => 'UserSocadm00Controller@menuQueryUpdate'
	));
//end for socadmm00 user

//***********************socadm00****************************


//************************slcadm00**************************
Route::get('/slcadm00/types-query', array(
		'as'   => 'slcadm00-types-query',
		'uses' => 'UserSlcadm00Controller@getTypesQuery'		
	));
Route::any('/slcadm00/types-browser', array(
		'as'   => 'slcadm00-types-browser',
		'uses' => 'UserSlcadm00Controller@getTypesbrowser'		
	));
Route::any('/slcadm00/types-add', array(
		'as'   => 'slcadm00-types-add',
		'uses' => 'UserSlcadm00Controller@getTypesAdd'		
	));
Route::any('/slcadm00/types-query-update', array(
		'as'   => 'slcadm00-types-update',
		'uses' => 'UserSlcadm00Controller@getTypesUpdate'		
	));
Route::get('/slcadm00/master-query', array(
		'as'   => 'slcadm00-master-query',
		'uses' => 'UserSlcadm00Controller@getMasterQuery'		
	));
Route::any('/slcadm00/master-browser', array(
		'as'   => 'slcadm00-master-browser',
		'uses' => 'UserSlcadm00Controller@getMasterBrowser'		
	));
Route::any('/slcadm00/master-add', array(
		'as'   => 'slcadm00-master-add',
		'uses' => 'UserSlcadm00Controller@getMasterAdd'		
	));
Route::any('/slcadm00/master-update', array(
		'as'   => 'slcadm00-master-update',
		'uses' => 'UserSlcadm00Controller@getMasterUpdate'		
	));
Route::get('/slcadm00/pos-query', array(
		'as'   => 'slcadm00-pos-query',
		'uses' => 'UserSlcadm00Controller@getPosQuery'		
	));
Route::any('/slcadm00/pos-browser', array(
		'as'   => 'slcadm00-pos-browser',
		'uses' => 'UserSlcadm00Controller@getPosBrowser'		
	));
Route::any('/slcadm00/pos-add', array(
		'as'   => 'slcadm00-pos-add',
		'uses' => 'UserSlcadm00Controller@getPosAdd'		
	));
Route::any('/slcadm00/pos-update', array(
		'as'   => 'slcadm00-pos-update',
		'uses' => 'UserSlcadm00Controller@getPosUpdate'		
	));
Route::get('/slcadm00/combo-query', array(
		'as'   => 'slcadm00-combo-query',
		'uses' => 'UserSlcadm00Controller@getComboQuery'		
	));
Route::any('/slcadm00/combo-browser', array(
		'as'   => 'slcadm00-combo-browser',
		'uses' => 'UserSlcadm00Controller@getComboBrowser'		
	));
Route::any('/slcadm00/combo-add', array(
		'as'   => 'slcadm00-combo-add',
		'uses' => 'UserSlcadm00Controller@getComboAdd'		
	));
Route::any('/slcadm00/combo-update', array(
		'as'   => 'slcadm00-combo-update',
		'uses' => 'UserSlcadm00Controller@getComboUpdate'		
	));
Route::get('/slcadm00/user-query', array(
		'as'   => 'slcadm00-user-query',
		'uses' => 'UserSlcadm00Controller@getUserQuery'		
	));
Route::any('/slcadm00/user-browser', array(
		'as'   => 'slcadm00-user-browser',
		'uses' => 'UserSlcadm00Controller@getUserBrowser'		
	));
Route::any('/slcadm00/user-add', array(
		'as'   => 'slcadm00-user-add',
		'uses' => 'UserSlcadm00Controller@getUserAdd'		
	));
Route::any('/slcadm00/user-update', array(
		'as'   => 'slcadm00-user-update',
		'uses' => 'UserSlcadm00Controller@getUserUpdate'		
	));
Route::get('/slcadm00/user-fm-display', array(
		'as'   => 'slcadm00-user-fm-display',
		'uses' => 'UserSlcadm00Controller@getUserFmDisplay'		
	));
Route::get('/slcadm00/user-fm-add', array(
		'as'   => 'slcadm00-user-fm-add',
		'uses' => 'UserSlcadm00Controller@getUserFmAdd'		
	));
Route::get('/slcadm00/user-fm-modify', array(
		'as'   => 'slcadm00-user-fm-modify',
		'uses' => 'UserSlcadm00Controller@getUserFmModify'		
	));
Route::get('/slcadm00/user-fm-top', array(
		'as'   => 'slcadm00-user-fm-top',
		'uses' => 'UserSlcadm00Controller@getUserFmTop'		
	));
Route::get('/slcadm00/report', array(
		'as'   => 'slcadm00-report',
		'uses' => 'UserSlcadm00Controller@getReport'		
	));

Route::any('/slcadm00/display-via-fm-add', array(
    'as' => 'slcadm00-display-via-fm-add',
    'uses' => 'UserSlcadm00Controller@getDisplayViaFm'
));
Route::any('/slcadm00/display-via-fm-update', array(
    'as' => 'slcadm00-display-via-fm-update',
    'uses' => 'UserSlcadm00Controller@getDisplayViaFmUpdate'
));

Route::get('/slcadm00/required-for-update-fm',array(

		'as' => 'slcadm00-required-for-update-fm',
		'uses'=>'UserSlcadm00Controller@getRequiredForUpdateFm'
	));
Route::get('/slcadm00/display-at-top-add',array(

		'as' => 'slcadm00-display-at-top-add',
		'uses'=>'UserSlcadm00Controller@getDisplayAtTopAdd'
	)); 
Route::get('/slcadm00/display-at-top-update',array(

		'as' => 'slcadm00-display-at-top-update',
		'uses'=>'UserSlcadm00Controller@getDisplayAtTopUpdate'
	));
Route::get('/slcadm00/report-mod-query',array(
	    'as'=>'slcadm00-report-mod-query',
	    'uses'=>'UserSlcadm00Controller@getReportModQuery'

	));
Route::any('/slcadm00/report-mod-browse',array(
	    'as'=>'slcadm00-report-mod-browse',
	    'uses'=>'UserSlcadm00Controller@getRepModBrowser'

	));
Route::any('/slcadm00/report-mod-view',array(
	    'as'=>'slcadm00-report-mod-view',
	    'uses'=>'UserSlcadm00Controller@getRepModView'

	));

Route::any('/slcadm00/report-mod-summary-query',array(
	    'as'=>'slcadm00-report-mod-summary-query',
	    'uses'=>'UserSlcadm00Controller@getReportModSummaryQuery'

	));

Route::any('/slcadm00/report-mod-summary-browse',array(
	    'as'=>'slcadm00-report-mod-summary-browse',
	    'uses'=>'UserSlcadm00Controller@getReportModSummarybrowse'

	));
Route::any('/pdf/slcadm05-detail_reportt', array(
			'as' => 'pdf-slcadm05-detail_report',
			'uses' => 'PDFGenerator@getDetailReport'

	));		
Route::any('/pdf/detail-reports', array(
			'as' => 'slcadm00-pdf-detail-reports',
			'uses' => 'PDFGenerator@getDetailsReports'

	));		

Route::any('/slcadm00/report-mod-detail-browse',array(
	    'as'=>'slcadm00-report-mod-detail-browse',
	    'uses'=>'UserSlcadm00Controller@getReportModDetailbrowse'

	    ));

Route::any('/slcadm00/report-scan-query',array(
	    'as'=>'slcadm00-report-scan-query',
	    'uses'=>'UserSlcadm00Controller@getReportScanQuery'

	));


Route::any('/slcadm00/report-scan-browse',array(
	    'as'=>'slcadm00-report-scan-browse',
	    'uses'=>'UserSlcadm00Controller@getReportScanBrowser'

	));


Route::any('/slcadm00/report-scan-report',array(
	    'as'=>'slcadm00-report-scan-report',
	    'uses'=>'UserSlcadm00Controller@getReportScanReport'

	));
Route::any('/slcadm00/report-scan-report-browse',array(
	    'as'=>'slcadm00-report-scan-report-browse',
	    'uses'=>'UserSlcadm00Controller@getReportScanReportBrowser'

	));



Route::any('/slcadm00/report-fm-query',array(
	    'as'=>'slcadm00-report-fm-query',
	    'uses'=>'UserSlcadm00Controller@getReportFmQuery'

	));
Route::any('/slcadm00/report-fm-browse',array(
	    'as'=>'slcadm00-report-fm-browse',
	    'uses'=>'UserSlcadm00Controller@getReportFmBrowser'

	));

Route::any('/slcadm00/report-fm-view',array(
	    'as'=>'slcadm00-report-fm-view',
	    'uses'=>'UserSlcadm00Controller@getReportFmView'

	));
Route::any('/slcadm00/report-fm-report',array(
	    'as'=>'slcadm00-report-fm-report',
	    'uses'=>'UserSlcadm00Controller@getReportFmReport'

	));
Route::any('/slcadm00/report-fm-report-browse',array(
	    'as'=>'slcadm00-report-fm-report-browse',
	    'uses'=>'UserSlcadm00Controller@getReportFmReportBrowser'

	));


//end of user slcadm00
//***********************slcadm00*************************


//***************slcadm05 User*****************
		Route::post('/slcadm05/modi-report',array(
			'as'=>'slcadm05-modi-report',
			'uses'=>'Slcadm05UserController@getModiReport'
			));

		Route::any('/slcadm05',array(
			'as'=>'slcadm05-post-slic-report',
			'uses'=>'Slcadm05UserController@postSlicReport'
			));
		Route::get('slcadm05/query',array(
			'as'=>'slcadm05-query',
			'uses'=>'Slcadm05UserController@getQuery'
			));

 Route::any('slcadm05/post-modi-query',array(
 			'as'=>'slcadm05-post-modi-query',
 			'uses'=>'Slcadm05UserController@postModiQuery'
 			));
 Route::any('slcadm05/post-fm-query',array(
 			'as'=>'slcadm05-post-fm-query',
 			'uses'=>'Slcadm05UserController@postFMQuery'
 			));
 Route::any('slcadm05/browser-result',array(
 			'as'=>'slcadm05-browser-result',
 			'uses'=>'Slcadm05UserController@getBrowser'	

 		));
 Route::any('slcadm05/modi-audit-view',array(
 			'as'=>'slcadm05-modi-audit-view',
 			'uses'=>'Slcadm05UserController@getModiAuditView'	

 		));

 Route::get('slcadm05/modi-summary',array(
 			'as'=>'slcadm05-modi-summary',
 			'uses'=>'Slcadm05UserController@getModiSummary'
 		));
 Route::post('slcadm05/post-summary-report',array(
 			'as'=>'slcadm05-post-summary-report',
 			'uses'=>'Slcadm05UserController@postModiSummary'
 		));
 Route::get('slcadm05/return-home',array(
 			'as'=>'slcadm05-return-home',
 			'uses'=>'Slcadm05UserController@returnHome'

 	));
 Route::get('slcadm05/fm-query',array(
			'as'=>'slcadm05-fm-query',
			'uses'=>'Slcadm05UserController@getFmQuery'
			));
Route::any('/pdf/slcadm05-summary_report', array(
			'as' => 'pdf-slcadm05-summary_report',
			'uses' => 'PDFGenerator@getSummaryReport'
	));
Route::any('/pdf/slcadm05-detail_reportt', array(
			'as' => 'pdf-slcadm05-detail_report',
			'uses' => 'PDFGenerator@getDetailReport'
	));
Route::any('/pdf/slcadm05-fm_reportt', array(
			'as' => 'pdf-slcadm05-fm_report',
			'uses' => 'PDFGenerator@getFMReport'
	));

//****************sign1*********///

Route::post('sign1/remotlyrequest', array(
			'as'   => 'sign1-remotly-requested',
			'uses' => 'SignsController@postRemotlyRequest'
	));
Route::get('sign1/remotlyrequestdisplay', array(
			'as'	=>'sign1-requestdisplay',
			'uses'  =>'SignsController@getRemotlyRequestInfo'
	));
Route::any('/sign1/sign_option_for_stock_code',array(
			'as'=>'sign_option_for_stock_code',
			'uses'=>'SignsController@signOptionForStockCode'
		));
Route::any('/sign1/dynamic-form-post',array(
			'as'=>'dynamic-form-post',
			'uses'=>'SignsController@save_sign_data'
		));
Route::any('/sign1/edit_sign_data/{i}','SignsController@editSignData');
Route::any('/sign1/edit_sign_data_for_AtoG/{i}','SignsController@editSignDatafromAtoG');
Route::POST('/sign1/sign1-prnt_tags_signs',array(
			'as'=>'sign1-prnt_tags_signs',
			'uses'=>'SignsController@gettagssigns'
		));
Route::POST('/sign1/sign1-dynamic-edit',array(
			'as'=>'sign1-dynamic-edit',
			'uses'=>'SignsController@getDynamicPrint'
		));

Route::any('/sign1/sign-stockcode-N',array(
			'as'=>'sign-stockcode-N',
			'uses'=>'SignsController@postPriceChangeSignsStockN'
		));
Route::any('/sign1/sign_option',array(
			'as'=>'sign_option',
			'uses'=>'SignsController@signOption'
		));
Route::any('/sign1/store-sign-stocks-names',array(
			'as'=>'store-sign-stocks-names',
			'uses'=>'SignsController@storeSignStocksNames'
		));
Route::any('/sign1/sstypsgn-dynamic-from',array(
			'as'=>'sstypsgn-dynamic-from',
			'uses'=>'SignsController@sstypsgnDynamicForm'
		));
Route::any('/sign1/sstypsgn-dynamic-from-2',array(
			'as'=>'sstypsgn-dynamic-from-2',
			'uses'=>'SignsController@sstypsgnDynamicForm2'
		));
Route::get('/sign1/get_sign_id',array(
			'as'=>'get_sign_id',
			'uses'=>'SignsController@getSignId'
		));
 
Route::get('/sign1/sign_stoce_inner_page',array(
			'as'=>'sign_stoce_inner_page',
			'uses'=>'SignsController@signStockInnerPage'
		));
Route::get('/sign1/signdir',array(
			'as'=>'signdir',
			'uses'=>'SignsController@signDir'
		));
Route::get('/sign1/everyday-value',array(
			'as'=>'sign1-everyday-value',
			'uses'=>'SignsController@getEverydayValue'

			));
Route::get('/sign1/daily-department-hour',array(
			'as'=>'sign1-daily-department-hour',
			'uses'=>'SignsController@getDepartmentHour'

			));
Route::get('/sign1/weekday',array(
			'as'=>'sign1-weekday',
			'uses'=>'SignsController@getWeekday'

			));
Route::get('/sign1/weekday-hour',array(
			'as'=>'sign1-weekday-hour',
			'uses'=>'SignsController@getWeekdayHour'

			));
Route::get('/sign1/weekday-sat-hour',array(
			'as'=>'sign1-weekday-sat-hour',
			'uses'=>'SignsController@getWeekdaySatHour'

			));
Route::get('/sign1/floral-sale',array(
			'as'=>'floral-sale',
			'uses'=>'SignsController@getFloralSale'

			));
Route::get('/sign1/price-drop',array(
			'as'=>'floral-price-drop',
			'uses'=>'SignsController@getDropPrice'

			));
Route::get('/sign1/blank',array(
			'as'=>'floral-blank',
			'uses'=>'SignsController@getBlank'

			));
Route::get('/sign1/blank-brand',array(
			'as'=>'floral-blank-brand',
			'uses'=>'SignsController@getBlankBrand'

			));
Route::get('/sign1/bogo',array(
			'as'=>'floral-bogo',
			'uses'=>'SignsController@getBogo'

			));
Route::get('/sign1/sale(AD)',array(
			'as'=>'floral-sale(AD)',
			'uses'=>'SignsController@getSaleAD'

			));
Route::get('/sign1/d-price-drop',array(
			'as'=>'floral-D-price-drop',
			'uses'=>'SignsController@getDPriceDrop'

			));
Route::get('/sign1/d-blank',array(
			'as'=>'floral-D-blank',
			'uses'=>'SignsController@getDBlank'

			));
Route::get('/sign1/seasonal',array(
			'as'=>'floral-seasonal',
			'uses'=>'SignsController@getSeasonal'

			));
Route::get('/sign1/raleys-dailies',array(
			'as'=>'floral-raleys-dailies',
			'uses'=>'SignsController@getRaleysDailies'

			));
Route::get('/sign1/pct-off',array(
			'as'=>'floral-pct-off',
			'uses'=>'SignsController@getPctOff'

			));
Route::get('/sign1/local',array(
			'as'=>'floral-local',
			'uses'=>'SignsController@getLocal'

			));
Route::get('/sign1/buy-save',array(
			'as'=>'floral-buy/save',
			'uses'=>'SignsController@getBuySave'

			));
Route::get('/sign1/bogo2',array(
			'as'=>'floral-bogo2',
			'uses'=>'SignsController@getBogo2'

			));
Route::get('/sign1/b2go',array(
			'as'=>'floral-b2go',
			'uses'=>'SignsController@getB2go'

			));
Route::get('/sign1/buy-get',array(
			'as'=>'floral-buy-get',
			'uses'=>'SignsController@getBuyGet'

			));
Route::get('/sign1/buy-get-free',array(
			'as'=>'floral-buy-get-free',
			'uses'=>'SignsController@getBuyGetFree'

			));
Route::get('/sign1/special-promo',array(
			'as'=>'floral-special-promo',
			'uses'=>'SignsController@getSpecialPromo'

			));
Route::get('/sign1/percent-off',array(
			'as'=>'floral-percent-off',
			'uses'=>'SignsController@getPercentOff'

			));
Route::get('/sign1/special-off',array(
			'as'=>'floral-special-%-off',
			'uses'=>'SignsController@getSpecialOff'

			));
Route::get('/sign1/text-only',array(
			'as'=>'floral-text-only',
			'uses'=>'SignsController@getTextOnly'

			));
Route::get('/sign1/fifth-menu-sale(AD)',array(
			'as'=>'fifth-sale',
			'uses'=>'SignsController@getFifthSale'

			));
Route::get('/sign1/fifth-price-drop',array(
			'as'=>'fifth-price-drop',
			'uses'=>'SignsController@getFifthPriceDrop'

			));
Route::get('/sign1/fifth-blank',array(
			'as'=>'fifth-blank',
			'uses'=>'SignsController@geFifthBlank'

			));
Route::get('/sign1/fifth-seasonal-price-drop',array(
			'as'=>'fifth-seasonal-price-drop',
			'uses'=>'SignsController@geFifthSeasonalPriceDrop'

			));
Route::get('/sign1/fifth-raleys-dailies',array(
			'as'=>'fifth-raleys-dailies',
			'uses'=>'SignsController@geFifthRaleysDailies'

			));
Route::get('/sign1/fifth-pct-off',array(
			'as'=>'fifth-pct-off',
			'uses'=>'SignsController@geFifthPctOff'

			));
Route::get('/sign1/fifth-buy-more-save',array(
			'as'=>'fifth-buy-more-save',
			'uses'=>'SignsController@geFifthBuyMoreSave'

			));
Route::get('/sign1/fifth-percent-off',array(
			'as'=>'fifth-percent-off',
			'uses'=>'SignsController@geFifthPercentOff'

			));
Route::get('/sign1/fifth-text-only',array(
			'as'=>'fifth-text-only',
			'uses'=>'SignsController@geFifthTextOnly'

			));
Route::get('/sign1/fifth-bogo',array(
			'as'=>'fifth-bogo',
			'uses'=>'SignsController@geFifthBogo'

			));
Route::get('/sign1/fifth-buy-get',array(
			'as'=>'fifth-buy-get',
			'uses'=>'SignsController@geFifthBuyGet'

			));
Route::get('/sign1/fifth-buy-get-free',array(
			'as'=>'fifth-buy-get-free',
			'uses'=>'SignsController@getFifthBuyGetFree'

			));
Route::get('/sign1/six-sale-(AD)',array(
			'as'=>'six-sale',
			'uses'=>'SignsController@getSixSale'

			));
Route::get('/sign1/six-price-drop',array(
			'as'=>'six-price-drop',
			'uses'=>'SignsController@getSixPriceDrop'

			));
Route::get('/sign1/six-blank',array(
			'as'=>'six-blank',
			'uses'=>'SignsController@getSixBlank'

			));
Route::get('/sign1/six-local',array(
			'as'=>'six-local',
			'uses'=>'SignsController@getSixLocal'

			));
Route::get('/sign1/six-raleys-dailies',array(
			'as'=>'six-raleys-dailies',
			'uses'=>'SignsController@getSixRaleysDailies'

			));
Route::get('/sign1/six-pct-off',array(
			'as'=>'six-pct-off',
			'uses'=>'SignsController@getSixPctOff'

			));
Route::get('/sign1/six-seasonal-price-drop',array(
			'as'=>'six-seasonal-price-drop',
			'uses'=>'SignsController@getSixSeasonalPriceDrop'

			));
Route::get('/sign1/six-buy-save-more',array(
			'as'=>'six-buy-save-more',
			'uses'=>'SignsController@getSixBuySaveMore'

			));
Route::get('/sign1/six-bogo',array(
			'as'=>'six-bogo',
			'uses'=>'SignsController@getSixBogo'

			));
Route::get('/sign1/six-b2go',array(
			'as'=>'six-b2go',
			'uses'=>'SignsController@getSixB2go'

			));
Route::get('/sign1/six-buy-get',array(
			'as'=>'six-buy-get',
			'uses'=>'SignsController@getSixBuyGet'

			));
Route::get('/sign1/six-buy-get-free',array(
			'as'=>'six-buy-get-free',
			'uses'=>'SignsController@getSixBuyGetFree'

			));
Route::get('/sign1/six-special-promo',array(
			'as'=>'six-special-promo',
			'uses'=>'SignsController@getSixSpecialPromo'

			));
Route::get('/sign1/six-percent-off',array(
			'as'=>'six-percent-off',
			'uses'=>'SignsController@getSixPercentOff'

			));
Route::get('/sign1/six-text-only',array(
			'as'=>'six-text-only',
			'uses'=>'SignsController@getSixTextOnly'

			));
Route::get('/sign1/seven-sale-(AD)',array(
			'as'=>'seven-sale',
			'uses'=>'SignsController@getSevenSale'

			));
Route::get('/sign1/seven-price-drop',array(
			'as'=>'seven-price-drop',
			'uses'=>'SignsController@getSevenPriceDrop'

			));
Route::get('/sign1/seven-blank',array(
			'as'=>'seven-blank',
			'uses'=>'SignsController@getSevenBlank'

			));
Route::get('/sign1/seven-raleys-dailies',array(
			'as'=>'seven-raleys-dailies',
			'uses'=>'SignsController@getSevenRaleysDailies'

			));
Route::get('/sign1/seven-raleys',array(
			'as'=>'seven-raleys',
			'uses'=>'SignsController@getSevenRaleys'

			));
Route::get('/sign1/seven-pct-off',array(
			'as'=>'seven-pct-off',
			'uses'=>'SignsController@getSevenPctOff'

			));
Route::get('/sign1/seven-clearance',array(
			'as'=>'seven-clearance',
			'uses'=>'SignsController@getSevenClearance'

			));
Route::get('/sign1/seven-pct-off-beer',array(
			'as'=>'seven-pct-off-beer',
			'uses'=>'SignsController@getSevenPctOffBeer'

			));
Route::get('/sign1/seven-bogo',array(
			'as'=>'seven-bogo',
			'uses'=>'SignsController@getSevenBogo'

			));
Route::get('/sign1/seven-b2go',array(
			'as'=>'seven-b2go',
			'uses'=>'SignsController@getSevenB2go'

			));
Route::get('/sign1/seven-buy-save',array(
			'as'=>'seven-buy-save',
			'uses'=>'SignsController@getSevenBuySave'

			));
Route::get('/sign1/seven-25-pct-off',array(
			'as'=>'seven-25-pct-off',
			'uses'=>'SignsController@getSeven25PctOff'

			));
Route::get('/sign1/seven-50-pct-off',array(
			'as'=>'seven-50-pct-off',
			'uses'=>'SignsController@getSeven50PctOff'

			));
Route::get('/sign1/seven-75-pct-off',array(
			'as'=>'seven-75-pct-off',
			'uses'=>'SignsController@getSeven75PctOff'

			));
Route::get('/sign1/seven-blank-brands',array(
			'as'=>'seven-blank-brands',
			'uses'=>'SignsController@getSevenBlankBrands'

			));
Route::get('/sign1/seven-local',array(
			'as'=>'seven-local',
			'uses'=>'SignsController@getSevenLocal'

			));
Route::get('/sign1/seven-seasonal-price-drop',array(
			'as'=>'seven-seasonal-price-drop',
			'uses'=>'SignsController@getSevenSeasonalPriceDrop'

			));
Route::get('/sign1/Y-sale',array(
			'as'=>'Y-sale',
			'uses'=>'SignsController@getYSale'

			));
Route::get('/sign1/Y-price-drop',array(
			'as'=>'Y-price-drop',
			'uses'=>'SignsController@getYPriceDrop'

			));
Route::get('/sign1/Y-blank-brands',array(
			'as'=>'Y-blank-brands',
			'uses'=>'SignsController@getYBlankBrands'

			));
Route::get('/sign1/Y-raleys-dailys',array(
			'as'=>'Y-raleys-dailys',
			'uses'=>'SignsController@getYRaleysDailys'

			));
Route::get('/sign1/Y-raley',array(
			'as'=>'Y-raley',
			'uses'=>'SignsController@getYRaley'

			));
Route::get('/sign1/Y-clearance',array(
			'as'=>'Y-clearance',
			'uses'=>'SignsController@getYClearance'

			));
Route::get('/sign1/Y-new-item',array(
			'as'=>'Y-new-item',
			'uses'=>'SignsController@getYNewItem'

			));
Route::get('/sign1/Y-blank',array(
			'as'=>'Y-blank',
			'uses'=>'SignsController@getYBlank'

			));
Route::get('/sign1/Y-bogo',array(
			'as'=>'Y-bogo',
			'uses'=>'SignsController@getYBogo'

			));
Route::get('/sign1/Y-was-now',array(
			'as'=>'Y-was-now',
			'uses'=>'SignsController@getYWasNow'

			));
Route::get('/sign1/print-tags',array(
			'as'=>'print-tags',
			'uses'=>'SignsController@getPrintTags'

			));
Route::get('/sign1/Remotely-signs',array(
			'as'=>'Remotely-signs',
			'uses'=>'SignsController@getRemotelySigns'

			));
Route::get('/sign1/meat',array(
			'as'=>'meat',
			'uses'=>'SignsController@getMeat'

			));
Route::get('/sign1/seafood',array(
			'as'=>'seafood',
			'uses'=>'SignsController@getSeafood'

			));
Route::get('/sign1/sign1-new-signs',array(
			'as'=>'sign1-new-signs',
			'uses'=>'SignsController@getNewSigns'

			));
Route::post('/sign1/post-new-signs',array(
			'as'=>'post-new-signs',
			'uses'=>'SignsController@PostNewSigns'

			));


Route::get('/sign1/report-mod-query',array(
	    'as'=>'sign1-report-mod-query',
	    'uses'=>'SignsController@getReportModQuery'

	));
Route::any('/sign1/report-mod-browse',array(
	    'as'=>'sign1-report-mod-browse',
	    'uses'=>'SignsController@getRepModBrowser'

	));
Route::any('/sign1/report-mod-view',array(
	    'as'=>'sign1-report-mod-view',
	    'uses'=>'SignsController@getRepModView'

	));

Route::any('/sign1/report-mod-summary-query',array(
	    'as'=>'sign1-report-mod-summary-query',
	    'uses'=>'SignsController@getReportModSummaryQuery'

	));

Route::any('/sign1/report-mod-summary-browse',array(
	    'as'=>'sign1-report-mod-summary-browse',
	    'uses'=>'SignsController@getReportModSummarybrowse'

	));
Route::any('/sign1/report-mod-detail-browse',array(
	    'as'=>'sign1-report-mod-detail-browse',
	    'uses'=>'SignsController@getReportModDetailbrowse'

	    ));

Route::any('/pdf/detail-reports', array(
			'as' => 'sign1-pdf-detail-reports',
			'uses' => 'PDFGenerator@getDetailsReports'

	));



Route::any('/sign1/report-fm-query',array(
	    'as'=>'sign1-report-fm-query',
	    'uses'=>'SignsController@getReportFmQuery'

	));
Route::any('/sign1/report-fm-browse',array(
	    'as'=>'sign1-report-fm-browse',
	    'uses'=>'SignsController@getReportFmBrowser'

	));

Route::any('/sign1/report-fm-view',array(
	    'as'=>'sign1-report-fm-view',
	    'uses'=>'SignsController@getReportFmView'

	));
Route::any('/sign1/report-fm-report',array(
	    'as'=>'sign1-report-fm-report',
	    'uses'=>'SignsController@getReportFmReport'

	));
Route::any('/sign1/report-fm-report-browse',array(
	    'as'=>'sign1-report-fm-report-browse',
	    'uses'=>'SignsController@getReportFmReportBrowser'

	));



Route::any('/sign1/report-batch-query',array(
	    'as'=>'sign1-report-batch-query',
	    'uses'=>'SignsController@getReportBatchQuery'

	));
Route::any('/sign1/report-batch-browse',array(
	    'as'=>'sign1-report-batch-browse',
	    'uses'=>'SignsController@getReportBatchBrowser'

	));

Route::any('/sign1/report-batch-view',array(
	    'as'=>'sign1-report-batch-view',
	    'uses'=>'SignsController@getReportBatchView'

	));

Route::post('/sign1/report-batch-update',array(
	    'as'=>'sign1-report-batch-update',
	    'uses'=>'SignsController@getReportBatchUpdate'

	));

Route::any('/sign1/report-batch-summary',array(
	    'as'=>'sign1-report-batch-summary',
	    'uses'=>'SignsController@getReportBatchSummary'

	));


Route::any('/sign1/report-batch-summary-view',array(
	    'as'=>'sign1-report-batch-summary-view',
	    'uses'=>'SignsController@getReportBatchSummaryView'

	));
Route::get('/sign1/prnt_sh_file','SignsController@prnt_sh_file');





});