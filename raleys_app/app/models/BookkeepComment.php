<?php

class BookkeepComment extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'comment';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
  
    public $timestamps  = false;
}
