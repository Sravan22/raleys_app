<?php

class BookkeepSafecoin extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'safe_coin';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
  
    public $timestamps  = false;
}
