<?php

class SafeCoin extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'safe_coin';
	protected $SafeCoin_cols = array('pennies_box', 'pennies_roll', 'nickles_box', 'nickles_roll', 
											  'dimes_box', 'dimes_roll','quarters_box', 'quarters_roll', 
											  'misc_box', 'misc_roll', 'misc_desc', 'safe_date');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
   

}
