<?php

class Sofnctns extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sofnctns';
	protected $SafeCoin_cols = array('entry_number', 'function_code', 'description', 'active_switch', 'last_updated_by', 'last_update');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
   

}
