<?php

class Somencfg extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'somencfg';
	protected $SafeCoin_cols = array('entry_number', 'gl_dept_number', 'menu_level', 'function_code', 
											  'type_code', 'subtype_code','description', 'menu_position', 
											  'goto_pgm_id', 'active_switch', 'last_updated_by', 'last_update');
	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
   

}
