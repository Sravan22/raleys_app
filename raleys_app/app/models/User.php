<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	protected $fillable = array('firstname', 'lastname', 'email', 'username', 'password', 'password_temp', 
								'code', 'active');

	/*protected $begin_loan_cols = array('reg_num', 'food_stamps', 'ones', 'fives', 'tens', 'twenties', 'rolled_coins', 'quaters', 'nickles', 'dimes', 'pennies', 'misc', 'bl_total', 'entry_date');*/

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	//protected $table_begin_loan = 'bookkeep_begin_loan';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

}
