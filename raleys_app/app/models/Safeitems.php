<?php

class Safeitems extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'safeitems';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
  
    public $timestamps  = false;
}
