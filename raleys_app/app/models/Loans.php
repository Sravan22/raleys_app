<?php

class Loans extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'loans';
	protected $socadm00_sodsdhdr_cols = array("item_id", "item_amt", "reg_num", "source_id", "entry_date");
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
     
    public $timestamps  = false;

}
