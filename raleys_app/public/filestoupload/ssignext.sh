#!/usr/bin/ksh
# %W% - %H% - John Prochaska - 07/18/07
#
# ssignext.sh
#
#
# This script executes the program ssignext.ec to extract sign data.  It then 
# calls ssignext.prl to format it into Post Script and sends the PS file to 
# the printer.
# 
# It is normally called from ssqueprt.4go
#        
# Mod Log
# 03/24/2014 rhoffman  Add fonts.ps sign post script file to print the
#                      sign with the Noteworthy font type
# 07/09/2007 jprochas  Initial coding
#        

#PATH=$PATH:/usr/bin:/etc:/usr/sbin:/usr/ucb:$HOME/bin:/usr/bin/X11:/sbin:.
#export PATH

BIN=/home/slic/bin
TMP=/home/slic/tmp
ARCH_DIR=/home/slic/arch_tags
LOG=/home/slic/logs/ssignext_sh.log
FONTS=/home/slic/fonts
SIGN_FILE=$TMP/ssignext.$$.txt
PS_FILE=$TMP/ssignext.$$.ps
PRTFILE=$TMP/prtfile.ps


if [ $# -lt 2 -o $# -gt 7 ];then
   echo "Usage: ssignext.sh <User ID> <Sign ID>" >>$LOG
   exit 1
fi
USER_ID=$1
SIGN_ID=$2
BATCH_NO=0
if [ $# -eq 3 ];then
   PRINTED=$3
else
   PRINTED="N"
fi
if [ $# -ge 4 ];then
   PRINTED=$3
   BATCH_NO=$4
fi


START_DATE=$5
END_DATE=$6
START_SKU=$7  # Optional starting SKU number.  Used for Reprinting Tags/Signs
echo "$USER_ID $SIGN_ID $PRINTED $BATCH_NO $START_DATE $END_DATE $START_SKU" > sign.log
#echo "$LOG $BIN" > sign.log
if [ $BATCH_NO -gt 0 ];then
   if [ $USER_ID -eq 3000 ];then
      STORE_NO=`grep "^F" /StoreSystems/etc/store.config|cut -f2 -d" "`
   else 
      STORE_NO=`grep "^G" /StoreSystems/etc/store.config|cut -f2 -d" "`
   fi
   if [ "x$START_SKU" = "x" -o "x$START_SKU" = "x0" ];then
      SIGN_FILE=$ARCH_DIR/$SIGN_ID.$BATCH_NO.$STORE_NO.txt
      PS_FILE=$ARCH_DIR/$SIGN_ID.$BATCH_NO.$STORE_NO.ps
   else 
      SIGN_FILE=$TMP/$SIGN_ID.$BATCH_NO.$STORE_NO.r.txt
      PS_FILE=$TMP/$SIGN_ID.$BATCH_NO.$STORE_NO.r.ps
   fi
fi

if [ "x$PSDEST" != "x" ];then
   # if we've specified a Post Script printer (from ps2printers.sh) use it #
   LPDEST=$PSDEST
fi

if [ -s $PS_FILE ];then
  # We've already generated this file ... just print it and update the data #
  # database to indicate they've been printed                               #
  echo printing $PS_FILE to $LPDEST >>$LOG
  lprt -c -P$LPDEST -ds $PS_FILE 2>>$LOG
  mysql -u root -pWelcome1! raleysdb -e "update ssignhdr set printed_sw=\'Y\' where batch_no=$BATCH_NO and sign_id=\'$SIGN_ID\';" >/dev/null 2>&1
  exit 0
fi

echo "Processing Tags/Signs ... Please Wait"
echo " "
echo " "

echo `date`>>$LOG
echo "$BIN/ssignext $SIGN_FILE $USER_ID $SIGN_ID $PRINTED $BATCH_NO $START_DATE $END_DATE $START_SKU">>$LOG

$BIN/ssignext $SIGN_FILE $USER_ID $SIGN_ID $PRINTED $BATCH_NO $START_DATE $END_DATE $START_SKU 2>>$LOG
PROG_STAT=$?
if [ $PROG_STAT -ne 0 ];then
  echo "ssignext exited with $PROG_STAT ... program aborted">>$LOG
#  rm $SIGN_FILE 2>>/dev/null
  exit $PROG_STAT
fi

$BIN/ssignext.prl $SIGN_FILE 2>>$LOG
PROG_STAT=$?
if [ $PROG_STAT -ne 0 ];then
  echo "ssignext.prl exited with $PROG_STAT ... program aborted">>$LOG
#  rm $SIGN_FILE 2>>/dev/null
#  rm $PS_FILE 2>>/dev/null
  exit $PROG_STAT
fi

echo cat fonts.ps to sign file >>$LOG
cat $FONTS/fonts.ps > $PRTFILE
cat $PS_FILE >> $PRTFILE

echo printing to $LPDEST >>$LOG
qprt -c -P$LPDEST -ds $PRTFILE 2>>$LOG

if [ "x$START_SKU" = "x" ];then
   START_SKU=0
fi
if [ $BATCH_NO -eq 0 -o $START_SKU -gt 0 ];then
#  rm $PS_FILE 2>>$LOG
fi
#rm $SIGN_FILE 2>>$LOG
#rm $PRTFILE 2>>$LOG
